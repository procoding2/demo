﻿namespace GameEnum
{
    public enum MiniGameEnum
    {
        [StringValue("bingo")]
        Bingo,

        [StringValue("bubble")]
        Bubble,

        [StringValue("puzzle")]
        Puzzle,

        [StringValue("solitaire")]
        Solitaire,

        [StringValue("8ball")]
        EBall,

        [StringValue("dog")]
        Dog,

        [StringValue("lure")]
        Fish
    }

    public enum MiniGameModeEnum
    {
        [StringValue("Head to head")]
        HeadToHead = 1,
        [StringValue("One vs Many")]
        OneToMany,
        [StringValue("Knockout")]
        KnockOut,
        [StringValue("Round robin")]
        RoundRobin
    }

    public enum TypeLevelEnum
    {
        Range = 1,
        Min
    }
    
    public enum PlayStatusRoomEnum
    {
        [StringValue("Searching for Opponent")]
        SearchingForOpponent = 1,
        [StringValue("Waiting for Result")]
        WaitingForResult,
        [StringValue("Finish")]
        Finish
    }
    
    public enum StatusRoomEnum
    {
        [StringValue("In Progress")]
        InProgress = 2,
        [StringValue("Complete")]
        Complete
    }

    public enum MiniGameStartTypeEnum
    {
        Init,
        InitNextGame,
        InitReMatch,
        Tutorial
    }

    public enum BingoScoreTypeEnum
    {
        Init,
        Daubs,
        Bingos,
        MultiBingos,
        DoubleScores,
        Penalties
    }

    public enum BingoBoosterTypeEnum
    {
        WildDaub = 1,
        PickABall,
        DoubleScore,
        BonusTime
    }

    public enum GameFinishTypeEnum
    {
        GameOver = 1,
        TimesUp,
        Success
    }

    public enum BingoToastTypeEnum
    {
        Amazing,
        Perfect,
        Great,
        Good,
        Cool,
        Goodjob
    }

    public enum BingoActiveStarByTypeEnum
    {
        None,
        Normal,
        WildDaub,
        PickABall
    }

    public enum StatusRoomResponseTypeEnum
    {
        Playing = 2,
        Finish
    }

    public enum KORoomStatusEnum
    {
        Waiting,
        MatchLoss,
        MatchWin,
        Champion
    }

    public enum BubbleHeartChangeTypeEnum
    {
        Plus,
        Minus,
        ReFill,
    }

    public enum BubbleHeartStateEnum
    {
        Zero,
        One,
        Two,
        Three
    }

    public enum BubbleScoreTypeEnum
    {
        Init,
        Change
    }


    public enum RaceMode
    {
        Betting,
        Race,
    }
    public enum RaceState
    {
        Prepare,
        Ready,
        Play,
        End,

    }

    public enum DogAniState
    {
        Idle,
        Win,
        Lose,
        End,
    }
}