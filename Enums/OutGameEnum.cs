﻿using System;

namespace OutGameEnum
{
    public enum VariableEnum
    {
        String,
        Int
    }

    public enum LanguageEnum
    {
        eng,
        kor,
        vi
    }

    public enum SocialTypeEnum 
    {
        Guest, 
        Google,
        Apple, 
        FNCY,
        Facebook
    }

    public enum TabbarEnum
    {
        // Home
        Home,
        Home_GameMode_Bingo,
        Home_GameMode_Bubble,
        Home_GameMode_Solitaire,
        Home_GameMode_Puzzle,
        Home_GameMode_8Ball,

        // Event
        Event,
        //EventBonusSystem
        EventBonusSystem,

        // Ranking
        Ranking,

        // History
        History,
        HistoryDetail,
    }

    public enum SceneEnum
    {
        [StringValue("UILogo")]
        Logo,

        [StringValue("UIWelcome")]
        Welcome,

        [StringValue("UIHome")]
        Home,

        [StringValue("UIBingo")]
        Bingo,

        [StringValue("UIBubble")]
        Bubble,

        [StringValue("UISolitaire")]
        Solitaire,

        [StringValue("UIRace")]
        Race,

        [StringValue("UIFish")]
        Fish
    }

    public enum BonusGameEnum
    {
        [StringValue("roulette")]
        Roulette,

        [StringValue("randombox")]
        OpenBox,

        [StringValue("scratch")]
        Scratch
    }

    public enum CoinEnum
    {
        [StringValue("credit")]
        Token,

        [StringValue("gold")]
        Gold,

        [StringValue("ticket")]
        Ticket
    }

    public enum BonusGameRewardEnum
    {
        Gold = 1,
        HCToken,
        Ticket,
        Jackpot,
        SlotSpin
    }

    public enum UserRankingTypeEnum
    {
        Gold = 1,
        HCToken
    }

    public enum JackpotRankingTypeEnum
    {
        AllTime = 1,
        Weekly
    }

    public enum JackpotType
    {
        MiniGame = 1,
        DogBetting,
        FishBetting
    }

    public enum DailyPrizeTypeEnum
    {
        Gold = 1,
        HCToken,
        Ticket,
        X2XP
    }

    public enum DayTypeEnum
    {
        Day_1 = 1,
        Day_2,
        Day_3,
        Day_4,
        Day_5,
        Day_6,
        Day_7
    }

    public enum SettingSupportContentEnum
    {
        About,
        HowTo,
        Help,
        Privacy,
        Report,
        Term
    }

    public enum IconTypeEnum
    {
        Avatar = 1,
        AvtFrame,
        Cover
    }

    public enum NotificationTypeEnum
    {
        Normal = 1,
        NewEvent,
        NewDailyMission,
        LevelUp
    }
}

