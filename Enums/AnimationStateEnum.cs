﻿using System;

namespace AnimationStateEnums
{
    public enum BallonAnimationStateEnum
    {
        [StringValue("State1")]
        State1,
        [StringValue("State2")]
        State2,
        [StringValue("State3")]
        State3,
        [StringValue("State4")]
        State4,
        [StringValue("Destroy")]
        Destroy,
        [StringValue("ThreeSeconds")]
        ThreeSeconds
    }

    public enum BoosterProgressAnimationStateEnum
    {
        [StringValue("State0")]
        State_0,
        [StringValue("State50")]
        State_50,
        [StringValue("State100")]
        State_100
    }

    public enum TabItemAnimationStateEnum
    {
        [StringValue("Idle")]
        Idle,
        [StringValue("OnSelected")]
        OnSelected
    }
}

