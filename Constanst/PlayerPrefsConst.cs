﻿using System;
using UnityEngine;
public static class PlayerPrefsConst
{
	public const string PLAYER_INFO = "player_info";
	public const string LANGUAGE = "language";

    public const string PLAYER_ID = "player_id";
    public const string PLAYER_LOGIN_TIME_YMD = "player_login_time_ymd";

    public const string SOUND_BACKGROUND_VOLUME = "sound_background_volume";
    public const string SOUND_GAME_VOLUME = "sound_game_volume";

    public const string NUMBER_DAILY_MISSION = "NUMBER_DAILY_MISSION";
    public const string NUMBER_HISTORY_EVENT = "NUMBER_HISTORY_EVENT";
    public const string NUMBER_NOTIFICATION = "NUMBER_NOTIFICATION";

    public const string KEY_PREF_LEVEL_UP = "KEY_PREF_LEVEL_UP";
    public const string KEY_PREF_IS_MINI_GAME = "KEY_PREF_IS_MINI_GAME";

    public const string ASSET_VERSION = "asset_version";
    public const string IMAGE_DATA = "image_data";
    public const string IMAGE_BANNER_DATA = "image_banner_data";
}