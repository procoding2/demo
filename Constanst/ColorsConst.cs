﻿using System;
using UnityEngine;

public static class ColorsConst
{
    // Tabbar
    public static readonly Color32 TAB_ACTIVE_COLOR = new Color32(68, 172, 254, 255);
    public static readonly Color32 TAB_DEACTIVE_COLOR = new Color32(255, 255, 255, 255);

    // background bingo
    public static readonly Color32 BACKGROUND_FOCUS_COLOR = new Color32(5, 118, 125, 255);
}

