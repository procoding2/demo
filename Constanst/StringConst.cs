using System;

public static class StringConst
{
    public const string HUB_URL = "https://api.oxgames.net/serverList";
    public const string WEB_CLIENT_ID = "549181104605-imaiogfp79m94dombuskvsf9p6odroga.apps.googleusercontent.com";

    /// <summary>
    /// match-user-{gameId}
    /// </summary>
    public const string SOCKET_MATCH_USER_EVENT = "match-user-";

    /// <summary>
    /// live-score-{gameId}
    /// </summary>
    public const string SOCKET_LIVE_SCORE_EVENT = "live-score-";

    /// <summary>
    /// rematch-request-{userId}
    /// </summary>
    public const string SOCKET_REMATCH_REQUEST_EVENT = "rematch-request-";

    /// <summary>
    /// accept-rematch-{userId}
    /// </summary>
    public const string SOCKET_ACCEPT_REMATCH_EVENT = "accept-rematch-";

    /// <summary>
    /// exit-game-{gameId}
    /// </summary>
    public const string SOCKET_EXIT_GAME_EVENT = "exit-game-";

    /// <summary>
    /// join-rematch-{userId}
    /// </summary>
    public const string SOCKET_JOIN_REMATCH_EVENT = "join-rematch-";

    /// <summary>
    /// user-game-finish-{gameId}
    /// </summary>
    public const string SOCKET_USER_GAME_FINISH_EVENT = "user-game-finish-";

    public const string SOCKET_CHANGE_JACKPOT_EVENT = "change-jackpot";
    public const string SOCKET_CHANGE_JACKPOT_BETTING_DOG = "change-jackpot-betting-dog";
    public const string SOCKET_CHANGE_JACKPOT_BETTING_FISH = "change-jackpot-betting-lure";
    /// <summary>
    /// betting-result-{roomId}
    /// </summary>
    public const string SOCKET_BETTING_RESULT = "betting-result-";
    /// <summary>
    /// betting-rate-{roomId}
    /// </summary>
    public const string SOCKET_BETTING_RATE = "betting-rate-";

    public const string SOCKET_USER_RECEIVED_JACKPOT_EVENT = "user-received-jackpot";
    //Wait BE define
    public const string SOCKET_ICON_NOTIFICATION = "SOCKET_ICON_NOTIFICATION";
    /// <summary>
    /// daily-mission-{userId}
    /// </summary>
    public const string SOCKET_DAILY_MISSION = "daily-mission-";
    /// <summary>
    /// history-{userId}
    /// </summary>
    public const string SOCKET_ICON_HISTORY = "history-";

    /// <summary>
    /// user-summary-{userId}
    /// </summary>
    public const string SOCKET_USER_SUMMARY = "user-summary-";

    // Bingo score info
    public const string BINGO_SCORE_INFO_DAUBS = "daubs";
    public const string BINGO_SCORE_INFO_BINGOS = "bingos";
    public const string BINGO_SCORE_INFO_MULTI_BINGOS = "multi bingos";
    public const string BINGO_SCORE_INFO_DOUBLE_SCORES = "double score";
    public const string BINGO_SCORE_INFO_PENTALTIES = "penalties";

    // Solitaire score info
    public const string SOLITAIRE_SCORE_INFO_SCORE = "score solitaire";
    public const string SOLITAIRE_SCORE_INFO_TIMEBONUS = "score time bonus solitaire";

    // Bubble score info
    public const string BUBBLE_SCORE_INFO_BULLET = "Bullet";
    public const string BUBBLE_SCORE_INFO_PREPARE_BULLET = "PrepareBullet";
    public const string BUBBLE_SCORE_INFO_BULLET_POS_X = "BulletPosX";
    public const string BUBBLE_SCORE_INFO_BULLET_POS_Y = "BulletPosY";
    public const string BUBBLE_SCORE_INFO_SAME_COLORS = "SameColors";
    public const string BUBBLE_SCORE_INFO_SAME_ISLAND = "SameIsland";
    public const string BUBBLE_SCORE_INFO_SCORES = "Score";
    public const string BUBBLE_SCORE_INFO_BONUS_SCORES = "BonusScore";

    // INotifyPropertyChanged
    public const string PROPERTY_SOUND = "sound";
    public const string PROPERTY_LANGUAGE = "language";
    public const string PROPERTY_USER_INFOR = "userinfor";


    public const string PROPERTY_FCM_DAILY = "fcm_daily";
    public const string PROPERTY_FCM_MISSION = "fcm_mission";
    public const string PROPERTY_FCM_NORMAL = "fcm_normal";
    public const string PROPERTY_FCM_HISTORY = "fcm_history";

    public const string PROPERTY_ANTICHEAT = "anticheat";

    public const string PROPERTY_APPLE_UPDATE = "apple_update";

    // Firebase
    public const string FIREBASE_KEY_DATA = "data";
    public const string KEY_DATA_LEVEL_UP = "KEY_DATA_LEVEL_UP";
    public const string KEY_HAVE_NOTI = "KEY_HAVE_NOTI";

    // Version
    //public const string VERSION = UnityEngine.Application.version;

    public const string APPLE_STORE_URL = "https://apps.apple.com/us/app/hyperx-casual/id6475059203";
    public const string GOOGLE_STORE_URL = "market://details?id=com.oxgames.hyperx";

    public const string POLICY_URL = "https://hyperxstorage.s3.ap-southeast-1.amazonaws.com/privacy.pdf";
    public const string CONDITION_URL = "https://hyperxstorage.s3.ap-southeast-1.amazonaws.com/term.pdf";

    public const string CHECKSUM_DEF = "DEF";
}