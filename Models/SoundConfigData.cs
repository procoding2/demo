﻿using System;

public class SoundConfigData: IEquatable<SoundConfigData>
{
	public float BackgroundVolume { get; set; }
	public float GameVolume { get; set; }

    public bool Equals(SoundConfigData other)
    {
        return BackgroundVolume == other.BackgroundVolume
            && GameVolume == other.GameVolume;
    }
}

