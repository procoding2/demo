using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class ImageData
{
    public List<ImageItemData> Avatars { get; set; } = new List<ImageItemData>();
    public List<ImageItemData> AvatarFrames { get; set; } = new List<ImageItemData>();
    public List<ImageItemData> Covers { get; set; } = new List<ImageItemData>();

    public class ImageItemData
	{
		public string Path { get; set; }
		public Texture2D Texture { get; set; }
    }

    public void AvatarByUrl(string url, Action<ImageItemData> callBack)
    {
        var avt = Avatars.FirstOrDefault(x => x.Path == url);
        if (avt != null)
        {
            callBack.Invoke(avt);
            return;
        }

        Download(url, Avatars, callBack);
    }

    public void AvatarFrameByUrl(string url, Action<ImageItemData> callBack)
    {
        var avtFr = AvatarFrames.FirstOrDefault(x => x.Path == url);
        if (avtFr != null)
        {
            callBack.Invoke(avtFr);
            return;
        }

        Download(url, AvatarFrames, callBack);
    }

    public void CoverByUrl(string url, Action<ImageItemData> callBack)
    {
        var cover = Covers.FirstOrDefault(x => x.Path == url);
        if (cover != null)
        {
            callBack.Invoke(cover);
            return;
        }

        Download(url, Covers, callBack);
    }

    private void Download(string url, List<ImageItemData> lstSource, Action<ImageItemData> callBack)
    {
        Current.Ins.downloadAPI.DownloadImage(url, (isComplete, result) =>
        {
            if (!isComplete)
            {
                callBack.Invoke(lstSource.FirstOrDefault());
                return;
            }
            else
            {
                ImageItemData imgData = new ImageItemData();
                imgData.Path = url;
                imgData.Texture = result;
                callBack.Invoke(imgData);
            }
        },
        cacheFolder: Current.Ins.CACHE_USER_DIR);
    }
}

[Serializable]
public class ImageDataUrl
{
    public List<string> Avatars = new List<string>();
    public List<string> AvatarFrames = new List<string>();
    public List<string> Covers = new List<string>();

    public List<string> AllPaths
    {
        get
        {
            List<string> paths = new List<string>();

            paths.AddRange(Avatars);
            paths.AddRange(AvatarFrames);
            paths.AddRange(Covers);

            return paths;
        }
    }
}

[Serializable]
public class ImageDataEventUrl
{
    public string Roulette;
    public string Scrach;
    public string OpenBox;
}
