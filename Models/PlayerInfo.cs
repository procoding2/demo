using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OutGameEnum;

[Serializable]
public class PlayerInfo
{
    [field: SerializeField] public string Id { get; set; }
    [field: SerializeField] public string Pid { get; set; }
    [field: SerializeField] public string UserName { get; set; }
    [field: SerializeField] public string Name { get; set; }
    [field: SerializeField] public int Level { get; set; }
    [field: SerializeField] public int CurrentXP { get; set; }
    [field: SerializeField] public int NextLevelXP { get; set; }
    [field: SerializeField] public string ReferralCode { get; set; }
    [field: SerializeField] public string ReferenceCode { get; set; }
    [field: SerializeField] public string Gender { get; set; }
    [field: SerializeField] public string Avatar { get; set; }
    [field: SerializeField] public string Cover { get; set; }
    [field: SerializeField] public string AvatarFrame { get; set; }
    [field: SerializeField] public string Phone { get; set; }
    [field: SerializeField] public string Email { get; set; }
    [field: SerializeField] public string DateOfBirth { get; set; }
    [field: SerializeField] public decimal HCTokenCoin { get; set; }
    [field: SerializeField] public decimal TicketCoin { get; set; }
    [field: SerializeField] public decimal GoldCoin { get; set; }
    [field: SerializeField] public string AccessToken { get; set; }
    [field: SerializeField] public SocialTypeEnum SocialType { get; set; }
    [field: SerializeField] public bool IsDelete { get; set; }
    [field: SerializeField] public bool IsEditName { get; set; }
    [field: SerializeField] public int BingoMMR { get; set; }
    [field: SerializeField] public int SolitaireMMR { get; set; }
    [field: SerializeField] public int BubbleMMR { get; set; }
    [field: SerializeField] public int DailyRewardNotClaim { get; set; }
    [field: SerializeField] public int AssetVersion { get; set; }
}
