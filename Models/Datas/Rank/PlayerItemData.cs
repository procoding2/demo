﻿using System;
using OutGameEnum;

namespace RankTab
{
    public class PlayerItemData
    {
        public int Order { get; set; }
        public string PlayerId { get; set; }
        public string PlayerName { get; set; }
        public string PlayerAvt { get; set; }
        public string PlayerAvtFrame { get; set; }
        public int Gold { get; set; }
        public int Token { get; set; }
        public UserRankingTypeEnum RankType { get; set; }

        public bool isSearching { get; set; }
    }
}

