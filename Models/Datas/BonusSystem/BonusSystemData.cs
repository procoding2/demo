﻿using System;
using System.Collections.Generic;
using OutGameEnum;

public class BonusSystemData
{
    public GameFeeData RouletteGameFee { get; set; } = new GameFeeData();
    public GameFeeData OpenBoxGameFee { get; set; } = new GameFeeData();
    public GameFeeData ScratchGameFee { get; set; } = new GameFeeData();

    public List<GamePrizesData> RoulettePrizes { get; set; }
    public List<GamePrizesData> OpenBoxPrizes { get; set; }

    public FreeTurnData RouletteFreeTurn { get; set; } = new FreeTurnData();

    public class GameFeeData
	{
		public int TokenFee { get; set; }
        public int GoldFee { get; set; }
        public int TicketFee { get; set; }
    }

    public class GamePrizesData
    {
        public int Id { get; set; }
        public int Amount { get; set; }
        public BonusGameRewardEnum Type { get; set; }
    }

    public class FreeTurnData
    {
        public int Daily { get; set; }
        public int Bonus { get; set; }
        public double TimeRemain { get; set; }

        public int totalFreeTurn { get { return Daily + Bonus; } }
    }
}