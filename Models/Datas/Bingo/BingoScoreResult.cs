﻿using System;
using System.Collections.Generic;
using System.Linq;

public class BingoScoreResult
{
    public List<int> Daubs { get; set; } = new List<int>();
    public List<int> Bingos { get; set; } = new List<int>();
    public List<int> MultiBingos { get; set; } = new List<int>();
    public List<int> DoubleScores { get; set; } = new List<int>();
    public List<int> Penalties { get; set; } = new List<int>();

    public int DaubsSum() => Daubs.Sum(x => x);
    public int BingosSum() => Bingos.Sum(x => x);
    public int MultiBingosSum() => MultiBingos.Sum(x => x);
    public int DoubleScoresSum() => DoubleScores.Sum(x => x);
    public int PenaltiesSum() => Penalties.Sum(x => x);

    public double StartTimeMilliSecond { get; set; }
    public double EndTimeMilliSecond { get; set; }
}