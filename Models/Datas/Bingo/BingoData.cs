﻿using System;
using System.Collections.Generic;
using GameEnum;

public class BingoData: DataUIBase
{
    public int[,] board { get; set; }
    public int[] ballons { get; set; }
    public Stack<BingoBoosterTypeEnum> boosters { get; set; }
}