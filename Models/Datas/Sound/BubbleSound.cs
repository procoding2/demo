using System;
using UnityEngine;

[Serializable]
public class BubbleSound 
{
    public AudioClip Background;

    public AudioClip SpawnBubble;
    public AudioClip ShootGun;
    public AudioClip ExplodeBubble;
    public AudioClip SwapBubble;
    public AudioClip BubbleInIsLand;
}
