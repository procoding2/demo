﻿using System;
using UnityEngine;

[Serializable]
public class BonusSystemSound
{
	public AudioClip Roulette;
	public AudioClip Scratch;
    public AudioClip Scratch_Done;
	public AudioClip OpenBox;
}

