﻿using System;
using UnityEngine;

[Serializable]
public class MiniGameSound
{
    public AudioClip TimeCountDown;
    public AudioClip SuccessMission;
    public AudioClip TimeUp;
    public AudioClip GameOver;
}

