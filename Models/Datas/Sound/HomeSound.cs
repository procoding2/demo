﻿using System;
using UnityEngine;

[Serializable]
public class HomeSound
{
	public AudioClip CoinChange;
	public AudioClip Coin;
}

