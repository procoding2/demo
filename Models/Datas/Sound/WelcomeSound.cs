﻿using System;
using UnityEngine;

[Serializable]
public class WelcomeSound
{
    public AudioClip Background;
}

