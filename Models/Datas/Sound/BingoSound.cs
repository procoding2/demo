﻿using System;
using UnityEngine;

[Serializable]
public class BingoSound
{
    public AudioClip Background;

    public AudioClip BalloonPop;
    public AudioClip Correct;
    public AudioClip Wrong;
    public AudioClip Bingo;

    public AudioClip DoubleScore;
    public AudioClip WildDaub;
    public AudioClip BonusTime;
    public AudioClip PickABall;

    public AudioClip Flip;
    public AudioClip ActiveBingo;
}

