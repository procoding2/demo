﻿using System;
public class BubbleData: DataUIBase
{
    public int[,] board { get; set; }
    public int[] bubbles { get; set; }
}