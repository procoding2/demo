﻿using System;
using System.Collections.Generic;
using System.Linq;
using CodeStage.AntiCheat.ObscuredTypes;

public class BubbleScoreResult
{
    public List<ScoreInfor> ScoreInfors { get; set; } = new List<ScoreInfor>();
    public double StartTimeMilliSecond { get; set; }
    public double EndTimeMilliSecond { get; set; }

    public ObscuredInt TimeRemaining { get; set; }
    public ObscuredInt Score { get; set; }

    public class ScoreInfor
    {
        public ObscuredInt Bullet { get; set; }
        public ObscuredInt PrepareBullet { get; set; }
        public (ObscuredInt, ObscuredInt) BulletPos { get; set; }
        public ObscuredInt SameColor { get; set; }
        public ObscuredInt SameInsland { get; set; }
        public ObscuredInt Score { get; set; }
        public ObscuredInt BonusScore { get; set; }
    }
}