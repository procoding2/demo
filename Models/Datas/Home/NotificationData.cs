﻿using System;
using System.Collections.Generic;
using OutGameEnum;

namespace HomeTab
{
    public class NotificationData
    {
        public List<NotificationItemData> Items { get; set; }
    }

    public class NotificationItemData
    {
        public string Id { get; set; }
        public bool IsRead { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public double TimeAgo { get; set; }
    }
}

