﻿using System;
using System.Linq;
using System.Collections.Generic;
using GameEnum;

namespace HomeTab
{
    public class PrizeDetailData
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public MiniGameEnum Game { get; set; }
        public MiniGameModeEnum Mode { get; set; }
        public int Quantity { get; set; }
        public int Round { get { return Mode == MiniGameModeEnum.KnockOut ? (int)MathF.Log(Quantity, 2f) : 1; } }

        public int GoldFee { get; set; }
        public int TokenFee { get; set; }
        public int TicketFee { get; set; }

        public List<PrizeDetailItemData> Items { get; set; } = new List<PrizeDetailItemData>();
        public int TotalGold { get { return Items.Sum(x => x.Gold); } }
        public int TotalToken { get { return Items.Sum(x => x.Token); } }
        public int TotalTicket { get { return Items.Sum(x => x.Ticket); } }
    }

    public class PrizeDetailItemData
    {
        public int Order { get; set; }
        public int Gold { get; set; }
        public int Token { get; set; }
        public int Ticket { get; set; }
    }
}