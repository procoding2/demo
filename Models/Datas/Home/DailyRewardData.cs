﻿using System;
using System.Collections.Generic;
using OutGameEnum;

namespace HomeTab
{
    public class DailyRewardData
    {
        public List<DailyRewardItemData> Items { get; set; }
    }

    public class DailyRewardItemData
    {
        public string Name { get; set; }
        public DayTypeEnum Day { get; set; }
        public bool IsClaimed { get; set; }
        public string RewardId { get; set; }
        public List<DailyRewardItemPrizeData> Prizes { get; set; }
    }

    public class DailyRewardItemPrizeData
    {
        public DailyPrizeTypeEnum RewardType { get; set; }
        public int RewardValue { get; set; }
    }
}

