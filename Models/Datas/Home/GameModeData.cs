using System;
using System.Collections;
using System.Collections.Generic;
using GameEnum;
using OutGameEnum;

public class GameModeData 
{
    public MiniGameEnum Game { get; set; }

    public List<GameModeItemData> HeadToHeads { get; set; } = new List<GameModeItemData>();

    public List<GameModeItemData> OneToManys { get; set; } = new List<GameModeItemData>();

    public List<GameModeItemData> KnockOuts { get; set; } = new List<GameModeItemData>();

    public class GameModeItemData
    {
        public string Id { get; set; }
        public MiniGameModeEnum Mode { get; set; }
        public int GoldPrizePool { get; set; }
        public int TokenPrizePool { get; set; }
        public int TicketPrizePool { get; set; }
        public int XPPrizePool { get; set; }
        public string ItemName { get; set; }
        public int Quantity { get; set; }
        public int FromLevel { get; set; }
        public int ToLevel { get; set; }
        public double StartTime { get; set; }
        public double EndTime { get; set; }
        public bool IsActive { get; set; }
        public int TokenFee { get; set; }
        public int GoldFee { get; set; }
        public int TicketFee { get; set; }
        public TypeLevelEnum TypeLevel { get; set; }
    }
}
