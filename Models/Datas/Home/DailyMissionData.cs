﻿using System;
using System.Collections.Generic;
using OutGameEnum;

namespace HomeTab
{
    public class DailyMissionData
    {
        public List<DailyMissionItemData> Items { get; set; }
    }

    public class DailyMissionItemData
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public DailyPrizeTypeEnum RewardType { get; set; }
        public int RewardValue { get; set; }
        public int TotalProgress { get; set; }
        public int CurrentProgress { get; set; }
        public bool IsClaimed { get; set; }
        public string RewardId { get; set; }
    }
}

