﻿using System;
public class ConfirmFeePopupData
{
	public int Gold { get; private set; }
    public int Token { get; private set; }
    public int Ticket { get; private set; }

    public int TotalItem
    {
        get
        {
            int total = 0;
            if (Gold > 0) total++;
            if (Token > 0) total++;
            if (Ticket > 0) total++;

            return total;
        }
    }

    public ConfirmFeePopupData(int gold, int token, int ticket)
	{
        Gold = gold;
        Token = token;
        Ticket = ticket;
    }
}

