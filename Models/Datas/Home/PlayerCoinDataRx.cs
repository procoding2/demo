﻿using System;
using OutGameEnum;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerCoinDataRx: IEqualityComparer<PlayerCoinDataRx>
{
    public bool Equals(PlayerCoinDataRx x, PlayerCoinDataRx y)
    {
        return x.Gold == y.Gold
            && x.Token == y.Token
            && x.Ticket == y.Ticket;
    }

    public int GetHashCode(PlayerCoinDataRx obj)
    {
        return obj.GetHashCode();
    }

    public decimal Gold { get; set; }
    public decimal Token { get; set; }
    public decimal Ticket { get; set; }

    public Transform transform { get; set; }
    public Transform desTransform { get; set; }
    public List<(CoinEnum, int)> CoinTypes { get; set; } = new List<(CoinEnum, int)>();
    public Action CallBackAfterEffect { get; set; }
}

