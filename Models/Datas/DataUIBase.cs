﻿using System;
using System.Collections.Generic;
using GameEnum;

public class DataUIBase: IData
{
    public string gameId { get; set; }
    public string roomId { get; set; }
    public MiniGameModeEnum miniGameMode { get; set; }

    public bool isSync { get; set; }
    public int totalPlayer { get; set; }
    public Player User { get; set; }

    public bool isOppoentInGame { get; set; } = true;
    public Player Opponent { get; set; }

    public OpponentScore OScore { get; set; } = new();

    public class Player
    {
        public string Name { get; set; }
        public string Avt { get; set; }
        public string AvtFrame { get; set; }
    }

    public class OpponentScore
    {
        public List<ScoreData> Scores { get; set; } = new();
        public class ScoreData
        {
            public double Time { get; set; }
            public int Score { get; set; }
        }
    }
}

