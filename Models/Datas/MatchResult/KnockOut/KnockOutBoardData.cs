﻿using System;
using System.Collections.Generic;
using GameEnum;

public class KnockOutBoardData
{
	public KORoomStatusEnum roomStatus { get; set; }
    public List<KnockOutMatchData> matchs { get; set; }
    public double timeRemain { get; set; }
}