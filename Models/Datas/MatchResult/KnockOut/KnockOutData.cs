﻿using System;
public class KnockOutData
{
    public string gameId { get; set; }
    public string roomId { get; set; }

    public Player Opponent { get; set; }

    public class Player
    {
        public string Name { get; set; }
        public string Avt { get; set; }
        public string AvtFrame { get; set; }
    }
}

