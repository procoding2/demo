﻿using System;
public class KnockOutMatchData
{
    public int gameNumber { get; set; }
	public KnockOutPlayerData player_0 { get; set; }
    public KnockOutPlayerData player_1 { get; set; }
}