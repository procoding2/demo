﻿using System;
public class KnockOutPlayerData
{
    public int Id { get; set; }
    public string Avatar { get; set; }
	public string Name { get; set; }
	public int Score { get; set; }
}