﻿using System;
using System.Collections.Generic;
public class MatchOneToManyData
{
    public string gameId { get; set; }
    public string roomId { get; set; }
    public int totalPlayer { get; set; }
    public List<FinishedUserData> FinishedUserDatas { get; set; } = new List<FinishedUserData>();

    public class FinishedUserData
    {
        public int UserId { get; set; }
        public int Rank { get; set; }
    }
}