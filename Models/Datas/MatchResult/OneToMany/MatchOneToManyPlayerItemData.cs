﻿using System;
public class MatchOneToManyPlayerItemData
{
	public int Order { get; set; }
	public string PlayerId { get; set; }
	public string PlayerName { get; set; }
	public string PlayerAvt { get; set; }
	public string PlayerAvtFrame { get; set; }
	public int PlayerScore { get; set; }

	public bool isSearching { get; set; }
}

