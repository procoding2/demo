﻿public class MatchHeadToHeadData
{
    public string gameId { get; set; }
    public string roomId { get; set; }

    public bool isSync { get; set; }
    public bool isOpponentInGame { get; set; }
    public Player Opponent { get; set; }

    public class Player
    {
        public string Name { get; set; }
        public string Avt { get; set; }
        public string AvtFrame { get; set; }
    }
}

