using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum ENVIRONMENTTYPE
{
    DEV,
    STAGE,
    TEST_PRODUCT,
    PRODUCT,

}
[CreateAssetMenu(fileName = "ConfigData", menuName = "HCData/Create")]
public class ConfigData : ScriptableObject
{

    private static ConfigData _instance = null;
    public static ConfigData instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = Resources.Load<ConfigData>("ConfigData");
            }
            return _instance;
        }
    }

    [Header("Environment Key Setting")]
    [SerializeField]
    public List<CredentialData> EvnData = new List<CredentialData>();

    [Header("Current Target Stage")]
    public ENVIRONMENTTYPE currentStage;


    [Header("Current Ver")]
    public VersionData verData;


    public CredentialData CurrentStageData
    {
        get
        {
            foreach (CredentialData data in EvnData)
            {
                if (data.EnvType == currentStage)
                    return data;
            }
            return null;
        }
    }

    public static string stageName
    {
        get
        {
            switch (_instance.currentStage)
            {
                case ENVIRONMENTTYPE.DEV:
                    return "dev";
                case ENVIRONMENTTYPE.STAGE:
                    return "stg";
                case ENVIRONMENTTYPE.TEST_PRODUCT:
                    return "test_Product";
                case ENVIRONMENTTYPE.PRODUCT:
                    return "live";
                default:
                    return "none";
            }
        }
    }
}

[Serializable]
public class CredentialData
{
    [SerializeField]
    private ENVIRONMENTTYPE envType;
    public ENVIRONMENTTYPE EnvType => envType;
    [SerializeField]
    private string _HOST_URL;
    public string HOST_URL => _HOST_URL;
    [SerializeField]
    private string _DOWNLOAD_URL;
    public string DOWNLOAD_URL => _DOWNLOAD_URL;
    [SerializeField]
    private string _SOCKET_URL;
    public string SOCKET_URL => _SOCKET_URL;
}

[Serializable]
public class VersionData
{
    public int majorVersion;        //0~9
    public int minorVersion;        //0~99
    public int revisionVersion;     //0~99
    public override string ToString() => $"{ majorVersion }.{ minorVersion }.{ revisionVersion }";
}