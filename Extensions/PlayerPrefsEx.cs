﻿using UnityEngine;
using CodeStage.AntiCheat.Storage;

public static class PlayerPrefsEx
{
    public static void Set<T>(string key, T value) => ObscuredPrefs.Set(key, value);
    public static T Get<T>(string key, T defaultValue) => ObscuredPrefs.Get(key, defaultValue);
    public static void DeleteKey(string key) => ObscuredPrefs.DeleteKey(key);
    public static void DeleteAll() => ObscuredPrefs.DeleteAll();
}