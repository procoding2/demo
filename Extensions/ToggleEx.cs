﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;

public class ToggleEx : Toggle, IPointerDownHandler, IPointerUpHandler
{
    override public void OnPointerDown(PointerEventData eventData)
    {
        if (IsInteractable()) transform.DOScale(0.9f, 0.1f);
    }

    override public void OnPointerUp(PointerEventData eventData)
    {
        if (IsInteractable()) transform.DOScale(1f, 0.1f);
    }
}

