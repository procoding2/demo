﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public static class UnityExt
{
    public static void RemoveAllChild(this Transform transform)
    {
        int c = transform.childCount;
        for (int i = 0; i < c; i++)
        {
            UnityEngine.Object.Destroy(transform.GetChild(i).gameObject);
        }
    }

    public static void DisableAllChild(this Transform transform)
    {
        int c = transform.childCount;
        for (int i = 0; i < c; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
    }

    public static void ActiveOnly(this Transform transform, GameObject gameObject)
    {
        transform.DisableAllChild();
        gameObject.SetActive(true);
    }

    public static void onClickWithHCSound(this Button button, UnityAction action, bool isClearAllListener = false)
    {
        if (isClearAllListener) button.onClick.RemoveAllListeners();
        button.onClick.AddListener(() =>
        {
            try
            {
                SoundManager.instance.PlayUIEffect("se_click1");
            }
            catch { }

            action.Invoke();
        });
    }
}