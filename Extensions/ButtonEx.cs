﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;

public class ButtonEx : Button, IPointerDownHandler, IPointerUpHandler
{
    override public void OnPointerDown(PointerEventData eventData)
    {
        if (IsInteractable())
        {
            var target = transform;
            if (targetGraphic != null) target = targetGraphic.gameObject.transform;
            target.DOScale(0.9f, 0.1f);
        }
    }

    override public void OnPointerUp(PointerEventData eventData)
    {
        if (IsInteractable())
        {
            var target = transform;
            if (targetGraphic != null) target = targetGraphic.gameObject.transform;
            target.DOScale(1f, 0.1f);
        }
    }
}

