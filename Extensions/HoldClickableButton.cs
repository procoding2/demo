using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class HoldClickableButton : ButtonEx
{
    [SerializeField] private float _holdDuration { get => 0.5f; }

    public event Action OnClicked;
    public event Action OnHoldClicked;

    private bool _isHoldingButton;
    private bool _isPressButton;
    private float _elapsedTime;

    public bool IsHoldingButton { get => _isHoldingButton; }
    public bool IsPressButton { get => _isPressButton; }

    override public void OnPointerDown(PointerEventData eventData)
    {
        ToggleHoldingButton(true);
        _isPressButton = true;

        //Click();
    } 


    private void ToggleHoldingButton(bool isPointerDown)
    {
        _isHoldingButton = isPointerDown;

        if (isPointerDown)
            _elapsedTime = 0;
    }

    override public void OnPointerUp(PointerEventData eventData)
    {
        ManageButtonInteraction(true);
        ToggleHoldingButton(false);
        _isPressButton = false;
    }

    private void ManageButtonInteraction(bool isPointerUp = false)
    {
        if (!_isHoldingButton)
            return;

        if (isPointerUp)
        {
            Click();
            return;
        }

        _elapsedTime += Time.deltaTime;
        var isHoldClickDurationReached = _elapsedTime > _holdDuration;

        if (isHoldClickDurationReached)
            HoldClick();
    }

    private void Click()
    {
        OnClicked?.Invoke();
    }

    private void HoldClick()
    {
        ToggleHoldingButton(false);
        OnHoldClicked?.Invoke();
    }

    private void Update() => ManageButtonInteraction();
}