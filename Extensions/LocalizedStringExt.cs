﻿using System;
using UnityEngine.Localization;
using UnityEngine.Localization.SmartFormat.PersistentVariables;

using OutGameEnum;
using UnityEngine.Localization.Components;

public static class LocalizedStringExt
{
	public static LocalizedString UpdateValue(this LocalizedString localizedString, string name, VariableEnum type, object value)
	{
		switch (type)
		{
			case VariableEnum.Int:
				IntVariable intVal = new IntVariable();
                intVal.Value = (int)value;
				localizedString.Add(name, intVal);

				break;
			case VariableEnum.String:
                StringVariable strVal = new StringVariable();
                strVal.Value = value.ToString();
                localizedString.Add(name, strVal);
                break;
		}

		return localizedString;
	}

	public static void UpdateValue(this LocalizeStringEvent lStringEvent, string name, VariableEnum type, object value)
	{
		lStringEvent.StringReference.UpdateValue(name, type, value);
        lStringEvent.RefreshString();
    }
}

