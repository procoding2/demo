﻿
using OutGameEnum;
using System;

namespace Utils
{
    internal static class DateTimeUtil
    {
        const int k_Second = 1;
        const int k_Minute = 60 * k_Second;
        const int k_Hour = 60 * k_Minute;
        const int k_Day = 24 * k_Hour;
        const int k_Month = 30 * k_Day;

        public static string GetTimeAgo(long time)
        {
            //
            LanguageEnum language = Current.Ins.Config.GetCurrentLanguage();
            //
            var dt1970 = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Local);
            var offset = System.TimeZone.CurrentTimeZone.GetUtcOffset(dt1970);
            var dateTime = (dt1970 + TimeSpan.FromSeconds(time) + offset);
            var ts = new TimeSpan(DateTime.UtcNow.Ticks - dateTime.ToUniversalTime().Ticks);
            double delta = Math.Abs(ts.TotalSeconds);

            if (delta < 2 * k_Minute)
            {             
                switch (language)
                {
                    case LanguageEnum.eng:
                        return "a minute ago";
                    case LanguageEnum.kor:
                        return "몇 분 전";
                    case LanguageEnum.vi:
                        return "mới phút trước";
                }
            }
                

            if (delta < 45 * k_Minute)
            {
                switch (language)
                {
                    case LanguageEnum.eng:
                        return ts.Minutes + " minutes ago";
                    case LanguageEnum.kor:
                        return ts.Minutes + " 분 전";
                    case LanguageEnum.vi:
                        return ts.Minutes + " phút trước";
                }
            }           

            if (delta < 90 * k_Minute)
            {
                switch (language)
                {
                    case LanguageEnum.eng:
                        return "an hour ago";
                    case LanguageEnum.kor:
                        return "한 시간 전";
                    case LanguageEnum.vi:
                        return "một giờ trước";
                }
            }          

            if (delta < 24 * k_Hour)
            {
                switch (language)
                {
                    case LanguageEnum.eng:
                        return ts.Hours + " hours ago";
                    case LanguageEnum.kor:
                        return ts.Hours + " 시간 전";
                    case LanguageEnum.vi:
                        return ts.Hours + " giờ trước";
                }
            }
            

            if (delta < 48 * k_Hour)
            {
                switch (language)
                {
                    case LanguageEnum.eng:
                        return "yesterday";
                    case LanguageEnum.kor:
                        return "어제";
                    case LanguageEnum.vi:
                        return "Hôm qua";
                }
            }              

            if (delta < 30 * k_Day)
            {
                switch (language)
                {
                    case LanguageEnum.eng:
                        return ts.Days + " days ago";
                    case LanguageEnum.kor:
                        return ts.Days + " 일 전";
                    case LanguageEnum.vi:
                        return ts.Days + " ngày trước";
                }
            }                  

            if (delta < 12 * k_Month)
            {
                int months = Convert.ToInt32(Math.Floor((double)ts.Days / 30));
                switch (language)
                {
                    case LanguageEnum.eng:
                        return months <= 1 ? "a month ago" : months + " months ago";
                    case LanguageEnum.kor:
                        return months <= 1 ? "한 달 전" : months + " 한 달 전";
                    case LanguageEnum.vi:
                        return months <= 1 ? "một tháng trước" : months + " tháng trước";
                }
            }

            int years = Convert.ToInt32(Math.Floor((double)ts.Days / 365));
            switch (language)
            {
                case LanguageEnum.eng:
                    return years <= 1 ? "one year ago" : years + " years ago";
                case LanguageEnum.kor:
                    return years <= 1 ? "일년 전" : years + " 년 전";
                case LanguageEnum.vi:
                    return years <= 1 ? "một năm trước" : years + " năm trước";
            }
            return "A long time ago";
        }

        public static string GetRefundTime(int time)
        {
            LanguageEnum language = Current.Ins.Config.GetCurrentLanguage();
            string timeTxt = "";
            if (time < k_Hour)
                timeTxt = time / k_Minute + "m";
            else if (time < k_Day)
                timeTxt = time / k_Hour + "h";
            else if (time >= k_Day)
                timeTxt = time / k_Day + "d";

            switch (language)
            {
                case LanguageEnum.eng:
                    return "Refund in" + " " + timeTxt;
                case LanguageEnum.kor:
                    return timeTxt + " 후에 환불";
                case LanguageEnum.vi:
                    return "Hoàn sau " + timeTxt;
            }

            return "--";
        }

        public static string getTimeHistory(int time)
        {
            var dt1970 = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Local);
            var offset = System.TimeZone.CurrentTimeZone.GetUtcOffset(dt1970);
            var result = (dt1970 + TimeSpan.FromSeconds(time) + offset).ToString(@"dd/MM/yyyy-HH:mm");

            return result;
        }

        public static DayOfWeek GetDayOfWeek(double time)
        {
            var dt1970 = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Local);
            var dt = dt1970 + TimeSpan.FromSeconds(time);
            return dt.DayOfWeek;
        }

        public static string GetTimeFormat(double time, string format)
        {
            var dt1970 = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Local);
            var dt = dt1970 + TimeSpan.FromSeconds(time);
            TimeZoneInfo infos = TimeZoneInfo.Local;
            return dt.Add(infos.BaseUtcOffset).ToString(format);
        }
    }
}