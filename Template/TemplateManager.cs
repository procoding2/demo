﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using TMPro;

public enum LocalIndex
{
    en,
    ko,
    vi,

    max,
}


public class TemplateManager : Singleton<TemplateManager>
{
    Dictionary<string, TemplateLocalString> m_dicTexts = new Dictionary<string, TemplateLocalString>();

    LocalIndex localIndex = LocalIndex.ko;
    public LocalIndex LocalIndexValue
    {
        get => localIndex;
        set
        {
            localIndex = value;
            PlayerPrefs.SetInt("localIndex", (int)value);
        }
    }

    private List<LocalStringText> localStringObjList = new List<LocalStringText>();
    public List<LocalStringText> LocalStringObjList => localStringObjList;

    public void LoadTemplates()
    {
            
        SystemLanguage sl = Application.systemLanguage;
        LocalIndex index = LocalIndex.en;
        if (sl == SystemLanguage.Vietnamese)
            index = LocalIndex.vi;
        //else if (sl == SystemLanguage.Korean)//
        //    index = LocalIndex.ko;

            //index = LocalIndex.ko;

        localIndex = (LocalIndex)PlayerPrefs.GetInt("localIndex", (int)index);


        LoadLocalStringTemplate();
    }

    public void LoadLocalStringTemplate()
    {
        UnityEngine.Debug.Log("[TemplateManager::LoadLocalStringTemplate]");

        Stopwatch stopWatch = new Stopwatch();
        stopWatch.Start();

        TextAsset jsonObj = Resources.Load<TextAsset>("Templates/HCX_localString");
        string json = jsonObj.text;

        TemplateLocalStringRootObject listData = new TemplateLocalStringRootObject();
        listData = JsonUtility.FromJson<TemplateLocalStringRootObject>(json);

        m_dicTexts.Clear();
        foreach (TemplateLocalString child in listData.localString)
        {
            //child.text = child.text.Replace("\\n", "\n");

            string dicLey = $"{child.Table}:{child.Key}";
            m_dicTexts[dicLey] = child;
        }

        stopWatch.Stop();
        UnityEngine.Debug.Log("text template loading time : " + stopWatch.Elapsed);
        UnityEngine.Debug.Log("text Count : " + m_dicTexts.Count);
    }

    public void ClearLocalStringTemplate()
    {
        m_dicTexts.Clear();
    }

    public TemplateLocalString FindLocalStringTemplate(string key)
    {
        TemplateLocalString template;
        if (m_dicTexts.TryGetValue(key, out template) == true)
        {
            return template;
        }

        return null;
    }

    public string GetLocalText(string key)
    {
        TemplateLocalString template;

        string localStr = null;
        if (m_dicTexts.TryGetValue(key, out template) == true)
        {

            switch (localIndex)
            {
                case LocalIndex.ko: localStr = template.ko; break;
                case LocalIndex.en: localStr = template.en; break;
                case LocalIndex.vi: localStr = template.vi; break;
            }

            if(localStr != null)
            {
                return localStr.Replace("\\n", "\n");
            }  
        }

        return "error:" + key.ToString();
    }


    public void ReflashLocalStringObj()
    {
        localStringObjList.ForEach(obj => obj.ReflashLocalString());
    }

}



