﻿using System;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;
using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Crypto.Digests;
using Org.BouncyCastle.Crypto.Encodings;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.OpenSsl;
using System.IO;

public class EncryptHelper
{
    #region BC2
    public static string EnctyptWithBC2(string pkey, byte[] data)
    {
        byte[] byteArray = Encoding.UTF8.GetBytes(pkey);
        MemoryStream stream = new MemoryStream(byteArray);

        StreamReader sr = new StreamReader(stream);
        // Read in public key from file
        var pemReader = new PemReader(sr);
        var rsaPub = (RsaKeyParameters)pemReader.ReadObject();
        // create encrypter
        var encrypter = new OaepEncoding(new RsaEngine(), new Sha256Digest(), new Sha256Digest(), null);
        encrypter.Init(true, rsaPub);
        var cipher = encrypter.ProcessBlock(data, 0, data.Length);

        return Convert.ToBase64String(cipher);
    }

    public static string EncryptWithBC(string pkey, string stringToEncrypt)
    {
        var plain = Encoding.UTF8.GetBytes(stringToEncrypt);
        return EnctyptWithBC2(pkey, plain);
    }
    #endregion

    #region AES256
    public static string EncryptWithAES256(string plainText, string key)
    {

        var Key = Encoding.UTF8.GetBytes(key);
        var IV = new byte[16];
        Debug.Log($"SKeyEncode: {key}");

        // Check arguments.
        if (plainText == null || plainText.Length <= 0)
            throw new ArgumentNullException("plainText");
        if (Key == null || Key.Length <= 0)
            throw new ArgumentNullException("Key");
        if (IV == null || IV.Length <= 0)
            throw new ArgumentNullException("IV");
        byte[] encrypted;

        // Create an Aes object
        // with the specified key and IV.
        using (Aes aesAlg = Aes.Create())
        {
            aesAlg.Key = Key;
            aesAlg.IV = IV;
            aesAlg.Mode = CipherMode.CBC;
            aesAlg.Padding = PaddingMode.PKCS7;

            // Create an encryptor to perform the stream transform.
            ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

            // Create the streams used for encryption.
            using (MemoryStream msEncrypt = new MemoryStream())
            {
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                    {
                        //Write all data to the stream.
                        swEncrypt.Write(plainText);
                    }
                    encrypted = msEncrypt.ToArray();
                }
            }
        }

        // Return the encrypted bytes from the memory stream.
        return Convert.ToBase64String(encrypted);
    }

    public static string DecryptWithAES256(string combinedString, string keyString)
    {
        Debug.Log($"SKey: {keyString}");

        var cipherText = Convert.FromBase64String(combinedString);
        var Key = Encoding.UTF8.GetBytes(keyString);
        var IV = new byte[16];

        // Check arguments.
        if (cipherText == null || cipherText.Length <= 0)
            throw new ArgumentNullException("cipherText");
        if (Key == null || Key.Length <= 0)
            throw new ArgumentNullException("Key");
        if (IV == null || IV.Length <= 0)
            throw new ArgumentNullException("IV");

        // Declare the string used to hold
        // the decrypted text.
        string plaintext = null;

        // Create an Aes object
        // with the specified key and IV.
        using (Aes aesAlg = Aes.Create())
        {
            aesAlg.Key = Key;
            aesAlg.IV = IV;
            aesAlg.Mode = CipherMode.CBC;
            aesAlg.Padding = PaddingMode.PKCS7;

            // Create a decryptor to perform the stream transform.
            ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

            // Create the streams used for decryption.
            using (MemoryStream msDecrypt = new MemoryStream(cipherText))
            {
                using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                {
                    using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                    {

                        // Read the decrypted bytes from the decrypting stream
                        // and place them in a string.
                        plaintext = srDecrypt.ReadToEnd();
                    }
                }
            }
        }

        return plaintext;
    }
    #endregion
}