﻿using System;
using OutGameEnum;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;

public class ChangeScene : Editor
{
    [MenuItem("Open Scene/Logo #0")]
    public static void OpenLogo()
    {
        OpenScene(SceneEnum.Logo.GetStringValue());
    }

    [MenuItem("Open Scene/Welcom #1")]
    public static void OpenWelcome()
    {
        OpenScene(SceneEnum.Welcome.GetStringValue());
    }

    [MenuItem("Open Scene/Home #2")]
    public static void OpenHome()
    {
        OpenScene(SceneEnum.Home.GetStringValue());
    }

    [MenuItem("Open Scene/Bingo #3")]
    public static void OpenBingo()
    {
        OpenScene(SceneEnum.Bingo.GetStringValue());
    }

    [MenuItem("Open Scene/Solitaire #4")]
    public static void OpenSolitaire()
    {
        OpenScene(SceneEnum.Solitaire.GetStringValue());
    }

    [MenuItem("Open Scene/Bubble #5")]
    public static void OpenBubble()
    {
        OpenScene(SceneEnum.Bubble.GetStringValue());
    }

    [MenuItem("Open Scene/Race #6")]
    public static void OpenRace()
    {
        OpenScene(SceneEnum.Race.GetStringValue());
    }

    [MenuItem("Open Scene/Fish #7")]
    public static void OpenFish()
    {
        OpenScene(SceneEnum.Fish.GetStringValue());
    }

    private static void OpenScene(string sceneName)
    {
        if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
        {
            EditorSceneManager.OpenScene("Assets/Game/Scenes/" + sceneName + ".unity");
        }
    }
}
#endif

public class SceneHelper
{
	public static void LoadScene(SceneEnum scene)
	{
		var scnName = scene.GetStringValue();
        UIPopUpManager.instance.CloseAllPopUpEvent();
		SceneManager.LoadScene(scnName);
	}

	public static void ReloadScene()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}
}

