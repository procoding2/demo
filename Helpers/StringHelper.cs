﻿using System;
using System.Linq;
using System.Text;

public class StringHelper
{
	public static string NumberFormatter(object number)
	{
		var rs = string.Format("{0:#,#}", number);

		return string.IsNullOrWhiteSpace(rs) ? "0" : rs;
    }

	public static string ReplaceStringByDot(string inputStr, int maxLength)
	{
		if (inputStr.Length > maxLength)
		{
			return inputStr.Substring(0,6) + "...";
		}

		return inputStr;
	}

	public static string FloatToPercent(float value)
	{
		return (value * 100).ToString("0\\%");
	}

	public static int QuantityInput(string inputStr)
    {
		return Encoding.UTF8.GetByteCount(inputStr);
	}

    public static string RandomString(int length)
    {
        System.Random random = new System.Random();
        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        return new string(Enumerable.Repeat(chars, length)
            .Select(s => s[random.Next(s.Length)]).ToArray());
    }
}

