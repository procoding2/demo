﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using UnityEngine;

public interface IChecker
{
    public bool isRooted();
    public bool isEmulator();
}

public class IOSCheck : IChecker
{
    public bool isEmulator()
    {
        return Application.platform != RuntimePlatform.IPhonePlayer;
    }

    private static readonly List<string> KnownDangerousFiles = new List<string>
            {
                "/Applications/Cydia.app",
                "/Applications/FakeCarrier.app",
                "/Applications/Icy.app",
                "/Applications/IntelliScreen.app",
                "/Applications/MxTube.app",
                "/Applications/RockApp.app",
                "/Applications/SBSettings.app",
                "/Applications/WinterBoard.app",
                "/Applications/blackra1n.app",
                "/Library/MobileSubstrate/DynamicLibraries/LiveClock.plist",
                "/Library/MobileSubstrate/DynamicLibraries/Veency.plist",
                "/Library/MobileSubstrate/MobileSubstrate.dylib",
                "/System/Library/LaunchDaemons/com.ikey.bbot.plist",
                "/System/Library/LaunchDaemons/com.saurik.Cydia.Startup.plist",
                "/private/var/tmp/cydia.log",
                "/etc/apt",
                "/usr/bin/sshd",
            };

    private static readonly List<string> KnownDangerousFolders = new List<string>
            {
                "/private/var/mobile/Library/SBSettings/Themes",
                "/etc/apt",
                "/private/var/lib/apt",
                "/var/cache/apt",
                "/var/lib/apt",
                "/private/var/lib/cydia",
                "/var/lib/cydia",
                "/private/var/stash",
            };

    /// <summary>
    /// Return true if the device is rooted/jailbroken
    /// </summary>
    /// <returns></returns>
    public bool isRooted()
    {
        if (CheckPotentialDangerousFiles()
           || CheckPotentialDangerousFolders()
           || CheckFileWritePermission()
            )
        {
            return true;
        }

        return false;
    }

    /// <summary>
    /// Check potential files which indicate a jailbreak
    /// </summary>
    /// <returns></returns>
    private bool CheckPotentialDangerousFiles()
    {
        foreach (var path in KnownDangerousFiles)
        {
            if (File.Exists(path))
            {
                return true;
            }
        }

        return false;
    }

    /// <summary>
    ///  Check potential folders which indicate a jailbreak
    /// </summary>
    /// <returns></returns>
    private bool CheckPotentialDangerousFolders()
    {
        foreach (var path in KnownDangerousFolders)
        {
            if (Directory.Exists(path))
            {
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Normal device will not allow to write to private folder
    /// </summary>
    /// <returns></returns>
    private bool CheckFileWritePermission()
    {
        //check write permission
        try
        {
            File.WriteAllText("/private/jailbreak.txt", "This is a test.");
            return true;
        }
        catch (UnauthorizedAccessException)
        {
        }

        return false;
    }
}

public class AndroidCheck : IChecker
{
    public bool isEmulator()
    {
        if (Application.installMode == ApplicationInstallMode.DeveloperBuild) return false;

        // BlueStack
        string[] s_BlueStacksDirSigs = { "/sdcard/windows/BstSharedFolder", "/mnt/windows/BstSharedFolder" };
        if (s_BlueStacksDirSigs.Any(Directory.Exists)) return true;

        if (Application.platform == RuntimePlatform.Android)
        {
            AndroidJavaClass osBuild;
            osBuild = new AndroidJavaClass("android.os.Build");
            string fingerPrint = osBuild.GetStatic<string>("FINGERPRINT");
            string model = osBuild.GetStatic<string>("MODEL");
            string menufacturer = osBuild.GetStatic<string>("MANUFACTURER");
            string brand = osBuild.GetStatic<string>("BRAND");
            string device = osBuild.GetStatic<string>("DEVICE");
            string product = osBuild.GetStatic<string>("PRODUCT");

            return fingerPrint.Contains("generic")
                    || fingerPrint.Contains("unknown")
                    || model.Contains("google_sdk")
                    || model.Contains("Emulator")
                    || model.Contains("Android SDK built for x86")
                    || model.Contains("Subsystem for Android(TM)")
                    || menufacturer.Contains("Genymotion")
                    || (brand.Contains("generic") && device.Contains("generic"))
                    || product.Equals("google_sdk")
                    || product.Equals("unknown")
                    || product.Contains("windows");

        }

        return false;
    }

    public bool isRooted()
    {
        if (DetectRootManagementApps()
            || DetectRootCloakingApps()
            || DetectPotentiallyDangerousApps()
            || CheckForBinary(BINARY_SU)
            || CheckForBinary(BINARY_BUSYBOX)
            || CheckForBinary(BINARY_MAGISK)
            )
        {
            return true;
        }

        return false;
    }

    static readonly string BINARY_SU = "su";
    static readonly string BINARY_BUSYBOX = "busybox";
    static readonly string BINARY_MAGISK = "magisk";

    static readonly List<string> KnownRootAppsPackages = new List<string> {
            "com.noshufou.android.su",
            "com.noshufou.android.su.elite",
            "eu.chainfire.supersu",
            "com.koushikdutta.superuser",
            "com.thirdparty.superuser",
            "com.yellowes.su",
            "com.topjohnwu.magisk",
            "com.kingroot.kinguser",
            "com.kingo.root",
            "com.smedialink.oneclickroot",
            "com.zhiqupk.root.global",
            "com.alephzain.framaroot"
        };

    static readonly List<string> KnownDangerousAppsPackages = new List<string> {
            "com.koushikdutta.rommanager",
            "com.koushikdutta.rommanager.license",
            "com.dimonvideo.luckypatcher",
            "com.chelpus.lackypatch",
            "com.ramdroid.appquarantine",
            "com.ramdroid.appquarantinepro",
            "com.android.vending.billing.InAppBillingService.COIN",
            "com.android.vending.billing.InAppBillingService.LUCK",
            "com.chelpus.luckypatcher",
            "com.blackmartalpha",
            "org.blackmart.market",
            "com.allinone.free",
            "com.repodroid.app",
            "org.creeplays.hack",
            "com.baseappfull.fwd",
            "com.zmapp",
            "com.dv.marketmod.installer",
            "org.mobilism.android",
            "com.android.wp.net.log",
            "com.android.camera.update",
            "cc.madkite.freedom",
            "com.solohsu.android.edxp.manager",
            "org.meowcat.edxposed.manager",
            "com.xmodgame",
            "com.cih.game_cih",
            "com.charles.lpoqasert",
            "catch_.me_.if_.you_.can_"
        };

    static readonly List<string> KnownRootCloakingPackages = new List<string> {
            "com.devadvance.rootcloak",
            "com.devadvance.rootcloakplus",
            "de.robv.android.xposed.installer",
            "com.saurik.substrate",
            "com.zachspong.temprootremovejb",
            "com.amphoras.hidemyroot",
            "com.amphoras.hidemyrootadfree",
            "com.formyhm.hiderootPremium",
            "com.formyhm.hideroot"
        };

    // These must end with a /
    static readonly List<string> SuPaths = new List<string> {
            "/data/local/",
            "/data/local/bin/",
            "/data/local/xbin/",
            "/sbin/",
            "/su/bin/",
            "/system/bin/",
            "/system/bin/.ext/",
            "/system/bin/failsafe/",
            "/system/sd/xbin/",
            "/system/usr/we-need-root/",
            "/system/xbin/",
            "/cache/",
            "/data/",
            "/dev/"
        };

    /*
     * Using the PackageManager, check for a list of well known root apps. @link {Const.knownRootAppsPackages}
     * @param additionalRootManagementApps - array of additional packagenames to search for
     * @return true if one of the apps it's installed
     */
    private bool DetectRootManagementApps(params string[] additionalRootManagementApps)
    {
        // Create a list of package names to iterate over from constants any others provided
        if (additionalRootManagementApps != null && additionalRootManagementApps.Length > 0)
        {
            KnownRootAppsPackages.AddRange(additionalRootManagementApps);
        }

        return IsAnyPackageFromListInstalled(KnownRootAppsPackages);
    }

    /*
     * Using the PackageManager, check for a list of well known apps that require root. @link {Const.knownRootAppsPackages}
     * @param additionalDangerousApps - array of additional packagenames to search for
     * @return true if one of the apps it's installed
     */
    private bool DetectPotentiallyDangerousApps(params string[] additionalDangerousApps)
    {

        // Create a list of package names to iterate over from constants any others provided
        if (additionalDangerousApps != null && additionalDangerousApps.Length > 0)
        {
            KnownDangerousAppsPackages.AddRange(additionalDangerousApps);
        }

        return IsAnyPackageFromListInstalled(KnownDangerousAppsPackages);
    }

    /*
     * Using the PackageManager, check for a list of well known root cloak apps. @link {Const.knownRootAppsPackages}
     * @param additionalRootCloakingApps - array of additional packagenames to search for
     * @return true if one of the apps it's installed
     */
    private bool DetectRootCloakingApps(params string[] additionalRootCloakingApps)
    {

        // Create a list of package names to iterate over from constants any others provided
        if (additionalRootCloakingApps != null && additionalRootCloakingApps.Length > 0)
        {
            KnownRootCloakingPackages.AddRange(additionalRootCloakingApps);
        }

        return IsAnyPackageFromListInstalled(KnownRootCloakingPackages);
    }

    /*
     *
     * @param filename - check for this existence of this file
     * @return true if found
     */
    private bool CheckForBinary(string filename)
    {
        foreach (var path in SuPaths)
        {
            if (File.Exists(path + filename))
            {
                return true;
            }
        }

        return false;
    }

    /*
       * Check if any package in the list is installed
       * @param packages - list of packages to search for
       * @return true if any of the packages are installed
       */
    private bool IsAnyPackageFromListInstalled(List<string> packages)
    {
        foreach (var name in packages)
        {
            try
            {
                _pm.Call<dynamic>("getPackageInfo", new object[] { name, 0 });
                return true;
            }
            catch (Exception)
            {
            }
        }

        return false;
    }

    AndroidJavaObject _pm;
    public AndroidCheck()
    {
        AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject currentActivity = jc.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaObject packageManager = currentActivity.Call<AndroidJavaObject>("getPackageManager");
        _pm = packageManager;
    }
}

public class RootCheckHelper
{
    public static bool isRooted()
    {
        if (Application.installMode == ApplicationInstallMode.DeveloperBuild) return false;

        if (Application.platform == RuntimePlatform.Android)
        {
            AndroidCheck andCheck = new AndroidCheck();
            return andCheck.isRooted();
        }

        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            IOSCheck iosCheck = new IOSCheck();
            return iosCheck.isRooted();
        }

        return true;
    }

    public static bool isEmulator()
    {
        if (Application.installMode == ApplicationInstallMode.DeveloperBuild) return false;

        if (Application.platform == RuntimePlatform.Android)
        {
            AndroidCheck andCheck = new AndroidCheck();
            return andCheck.isEmulator();
        }

        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            IOSCheck iosCheck = new IOSCheck();
            return iosCheck.isEmulator();
        }

        return true;
    }
}