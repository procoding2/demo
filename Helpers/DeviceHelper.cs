﻿using UnityEngine;
using UnityEngine.Rendering.Universal;
using UnityEngine.Rendering;

public class DeviceHelper
{
    public static void OpenSetting()
    {
#if UNITY_IPHONE
        string url = MyNativeBindings.GetSettingsURL();
        Application.OpenURL(url);
#endif

#if UNITY_ANDROID
        using var unityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        using AndroidJavaObject currentActivityObject = unityClass.GetStatic<AndroidJavaObject>("currentActivity");
        string packageName = currentActivityObject.Call<string>("getPackageName");
        using var uriClass = new AndroidJavaClass("android.net.Uri");
        using AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("fromParts", "package", packageName, null);
        using var intentObject = new AndroidJavaObject("android.content.Intent", "android.settings.APPLICATION_DETAILS_SETTINGS", uriObject);
        intentObject.Call<AndroidJavaObject>("addCategory", "android.intent.category.DEFAULT");
        intentObject.Call<AndroidJavaObject>("setFlags", 0x10000000);
        currentActivityObject.Call("startActivity", intentObject);
#endif
    }

    public static void SetQuality(bool isReset = false)
    {
        UniversalRenderPipelineAsset data = GraphicsSettings.currentRenderPipeline as UniversalRenderPipelineAsset;

        if (data)
        {
            if (isReset)
            {
                data.renderScale = 1f;
                return;
            }

            if (Application.platform == RuntimePlatform.Android)
            {
                switch (SystemInfo.systemMemorySize)
                {
                    case <= 8500:
                        data.renderScale = 0.7f;
                        break;
                    default: break;
                }
            }

            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                switch (SystemInfo.systemMemorySize)
                {
                    case <= 6500:
                        data.renderScale = 0.7f;
                        break;
                    default: break;
                }
            }
        }
    }
}