﻿using System;
public class UrlHelper
{
    public static string GetHostUrlFromPath(string path, bool inCludePrefix = true)
    {
        if (!inCludePrefix) return path;
        Uri url = new Uri(Current.Ins.HostUrl);
        return (new Uri(url, path)).AbsoluteUri;
    }

    public static string GetDownloadUrlFromPath(string path)
    {
        bool isUri = Uri.TryCreate(path, UriKind.Absolute, out Uri uriResult)
                && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);

        if (isUri) return path;

        Uri url = new Uri(Current.Ins.DownloadUrl);
        return (new Uri(url, path)).AbsoluteUri;
    }
}