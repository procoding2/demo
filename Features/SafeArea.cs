using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SafeArea : MonoBehaviour
{
    RectTransform rectTransform;
    Rect safeArea;
    Vector2 minAnchor;
    Vector2 maxAnchor;

    public bool IncludeBottom;
    public bool IncludeTop;

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        safeArea = Screen.safeArea;
        minAnchor = safeArea.position;
        maxAnchor = minAnchor + safeArea.size;

        minAnchor.x /= Screen.width;

        if (!IncludeBottom) minAnchor.y /= Screen.height;
        else minAnchor.y = 0f;

        maxAnchor.x /= Screen.width;

        if (!IncludeTop) maxAnchor.y /= Screen.height;
        else maxAnchor.y = 1f;

        rectTransform.anchorMin = minAnchor;
        rectTransform.anchorMax = maxAnchor;
    }
}
