﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<T> where T : new()
{
    private static object m_Lock = new object();
    private static T m_instance;

    public static T instance
    {
        get
        {
            lock (m_Lock)
            {
                if (m_instance == null)
                {
                    m_instance = new T();
                }

                return m_instance;
            }
        }
    }
}

public class MonoBehaviourSingleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static bool isApplicationQuit = false;
    private static object m_Lock = new object();
    private static T m_instance = null;
    public static T instance
    {
        get
        {
            lock (m_Lock)
            {
                if (m_instance == null && isApplicationQuit == false)
                {
                    GameObject coreGameObject = new GameObject(typeof(T).Name);
                    if (coreGameObject != null)
                    {
                        m_instance = coreGameObject.AddComponent<T>();
                        m_instance.enabled = true;
                        DontDestroyOnLoad(coreGameObject);
                    }
                }

                return m_instance;
            }
        }
    }


    private void OnApplicationQuit()
    {
        isApplicationQuit = true;
    }


}

public class ManagerSingleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static bool isApplicationQuit = false;
    private static object m_Lock = new object();
    private static T m_RuntimeInstance = null;
    public static T instance
    {
        get
        {
            lock (m_Lock)
            {
                if (m_RuntimeInstance == null && isApplicationQuit == false)
                {
                    m_RuntimeInstance = (T)FindObjectOfType(typeof(T));
                    if (m_RuntimeInstance == null)
                    {
                        var singletonObject = new GameObject();
                        m_RuntimeInstance = singletonObject.AddComponent<T>();
                        singletonObject.name = typeof(T).ToString() + " (Singleton)";
                        //DontDestroyOnLoad(singletonObject);
                        
                    }
                }

                return m_RuntimeInstance;
            }
        }
    }


    private void OnApplicationQuit()
    {
        isApplicationQuit = true;
    }

}


public class PrefabsLoadSingleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static bool isApplicationQuit = false;
    private static object m_Lock = new object();
    private static T m_RuntimeInstance;
    public static T instance
    {
        get
        {
            lock (m_Lock)
            {
                if (m_RuntimeInstance == null && isApplicationQuit == false)
                {
                    m_RuntimeInstance = (T)FindObjectOfType(typeof(T));
                    if (m_RuntimeInstance == null)
                    {
                        var singletonObject = Instantiate(Resources.Load<T>($"Prefabs/{typeof(T)}"));
                        if (singletonObject)
                        {
                            m_RuntimeInstance = singletonObject.GetComponent<T>();
                            DontDestroyOnLoad(singletonObject);
                        }

                    }
                }

                return m_RuntimeInstance;
            }
        }
    }


    private void OnApplicationQuit()
    {
        isApplicationQuit = true;
    }

}
