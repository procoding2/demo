
using System;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_ANDROID
using Unity.Notifications.Android;
#endif

public class AndroidNotiController: MonoBehaviour
{
#if UNITY_ANDROID
    static List<string> Handled_Ids = new List<string>();
    string _Channel_Id = "notify_daily_reminder";
    string _Channel_Title = "Daily Reminders";
    string _Channel_Description = "Get daily updates to see anything you missed.";


    public void initNotiAndroid()
    {
        Debug.Log("NotificationManager: Start");

        //always remove any currently displayed notifications
        Unity.Notifications.Android.AndroidNotificationCenter.CancelAllDisplayedNotifications();

        //check if this was openened from a notification click
        var notification_intent_data = AndroidNotificationCenter.GetLastNotificationIntent();
        //if the notification intent is not null and we have not already seen this notification id, do something
        //using a static List to store already handled notification ids
        if (notification_intent_data != null && Handled_Ids.Contains(notification_intent_data.Id.ToString()) == false)
        {
            Handled_Ids.Add(notification_intent_data.Id.ToString());

            //this logic assumes only one type of notification is shown
            //open app when the user clicks the notification                
            SceneHelper.LoadScene(OutGameEnum.SceneEnum.Welcome);
            return;
        }

        //dont do anything further if the user has disabled notifications
        //this assumes you have additional ui to enabled/disable this preference
        var allow_notifications = PlayerPrefsEx.Get("notifications", "");
        if (allow_notifications?.ToLower() == "false")
        {
            Debug.Log("Notifications Disabled");
            return;
        }
        Setup_Notifications();
    }


    private void Setup_Notifications()
    {
        Debug.Log("NotificationsManager: Setup_Notifications");
        //initialize the channel
        this.Initialize();
        //schedule the next notification
        this.Schedule_Daily_Reminder();
    }


    private void Initialize()
    {
        //add our channel
        //a channel can be used by more than one notification
        //you do not have to check if the channel is already created, Android OS will take care of that logic
        var androidChannel = new AndroidNotificationChannel(this._Channel_Id, this._Channel_Title, this._Channel_Description, Importance.Default);
        AndroidNotificationCenter.RegisterNotificationChannel(androidChannel);
    }

    private void Schedule_Daily_Reminder()
    {
        //since this is the only notification I have, I will cancel any currently pending notifications
        //if I create more types of notifications, additional logic will be needed
        AndroidNotificationCenter.CancelAllScheduledNotifications();
        //create new schedule
        string title = "We Miss You!";
        string body = "Come Back! Come Back! Come Back!";

        //show at the specified time - 12:00 PM
        //you could also always set this a certain amount of hours ahead, since this code resets the schedule, this could be used to prompt the user to play again if they haven't played in a while
        DateTime delivery_time = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 12, 0, 0).AddDays(1);

        Debug.Log("Delivery Time: " + delivery_time.ToString());
        //you currently do not need the notification id
        //if you had multiple notifications, you could store this and use it to cancel a specific notification
        var scheduled_notification_id = Unity.Notifications.Android.AndroidNotificationCenter.SendNotification(
            new Unity.Notifications.Android.AndroidNotification()
            {
                Title = title,
                Text = body,
                FireTime = delivery_time,
            },
            this._Channel_Id);
    }
#endif
}

