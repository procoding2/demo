using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_ANDROID
using Unity.Notifications.Android;
#elif UNITY_IOS
using Unity.Notifications.iOS;
#endif

public class HCPushNotification
{
    private interface IHCPushNotification
    {
        void PushNotification(string id, string title, string content);
    }

#if UNITY_ANDROID
    private class HCAndroidPushNotification : IHCPushNotification
    {
        public void PushNotification(string id, string title, string content)
        {
            var channel = new AndroidNotificationChannel()
            {
                Id = id,
                Name = title,
                Importance = Importance.Default,
                Description = content,
            };

            AndroidNotificationCenter.RegisterNotificationChannel(channel);

            Unity.Notifications.Android.AndroidNotificationCenter.SendNotification(
                new Unity.Notifications.Android.AndroidNotification()
                {
                    Title = title,
                    Text = content,
                    FireTime = DateTime.Now,
                }, id);
        }
    }
#elif UNITY_IOS
    private class HCIOSPushNotification : IHCPushNotification
    {
        public void PushNotification(string id, string title, string content)
        {
            var timeTrigger = new iOSNotificationTimeIntervalTrigger()
            {
                TimeInterval = new TimeSpan(0, 0, 1),
                Repeats = false
            };

            var notiData = new iOSNotification();
            notiData.Identifier = Guid.NewGuid().ToString();
            notiData.Trigger = timeTrigger;
            notiData.ShowInForeground = true;
            notiData.Title = title;
            notiData.Body = content;

            Debug.Log("iOSNotificationCenter.ScheduleNotification(notiData);");
            Debug.Log($"Title: {title}, Content: {content}");
            iOSNotificationCenter.ScheduleNotification(notiData);
        }
    }
#endif

    private static HCPushNotification _instance;
    public static HCPushNotification Instance()
    {
        if (_instance == null) _instance = new HCPushNotification();
        return _instance;
    }

    private HCPushNotification()
    {
#if UNITY_ANDROID
        _hcNoti = new HCAndroidPushNotification();
#elif UNITY_IOS
            _hcNoti = new HCIOSPushNotification();
#endif
    }

    private IHCPushNotification _hcNoti;
    public void Push(string id, string title, string content)
        => _hcNoti.PushNotification($"HC_Noti_{id}", title, content);
}

