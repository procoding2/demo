
#if UNITY_IOS
using Unity.Notifications.iOS;
#endif

using System;
using System.Collections;
using UnityEngine;

public class IOSNotiController : MonoBehaviour
{
#if UNITY_IOS
  public IEnumerator RequestAuthorization()
    {
        var authorizationOption = AuthorizationOption.Alert | AuthorizationOption.Badge;
        using (var req = new AuthorizationRequest(authorizationOption, true))
        {
            while (!req.IsFinished)
            {
                yield return null;
            };
        }
    }

    public void initNotiIos()
    {
        //always remove any currently displayed notifications
        Unity.Notifications.iOS.iOSNotificationCenter.RemoveAllScheduledNotifications();

        //check if this was openened from a notification click
        var notification_intent_data = iOSNotificationCenter.GetLastRespondedNotification();
        //if the notification intent is not null and we have not already seen this notification id, do something
        //using a static List to store already handled notification ids
        if (notification_intent_data != null)
        {
            //this logic assumes only one type of notification is shown
            //open app when the user clicks the notification                
            SceneHelper.LoadScene(OutGameEnum.SceneEnum.Welcome);
            return;
        }

        var timeNow = DateTime.Now.AddDays(1);
        var calendarTrigger = new iOSNotificationCalendarTrigger()
        {
            Year = timeNow.Year,
            Month = timeNow.Month,
            Day = timeNow.Day,
            Hour = 12,
            Minute = 0,
            Second = 0,
            Repeats = true
        };

        var notification = new iOSNotification()
        {
            // You can specify a custom identifier which can be used to manage the notification later.
            // If you don't provide one, a unique string will be generated automatically.
            Identifier = "_notification_01",
            Title = "We Miss You!",
            Body = "Come Back! Come Back! Come Back!",
            ShowInForeground = true,
            ForegroundPresentationOption = (PresentationOption.Alert | PresentationOption.Sound),
            CategoryIdentifier = "category_a",
            ThreadIdentifier = "thread1",
            Trigger = calendarTrigger,
        };

        iOSNotificationCenter.ScheduleNotification(notification);
    }
#endif
}
