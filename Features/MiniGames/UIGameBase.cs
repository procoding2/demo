using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UniRx;

using GameEnum;
using DG.Tweening;
using System.Threading;
using OutGameEnum;

public abstract class UIGameBase<T> : UIBase<UIGameBase<T>>
{
    protected override void InitIns() => Ins = this;

    #region static
    protected static MiniGameModeEnum sGameMode;
    protected static string sGameId;
    protected static string sRoomId;

    public static ConfirmFeePopupData sFeeData;
    protected static MiniGameStartTypeEnum sStartType;

    public static void InitStaticVariable(MiniGameModeEnum gameMode, string gameId, ConfirmFeePopupData feeData)
    {
        sGameMode = gameMode;
        sGameId = gameId;
        sStartType = MiniGameStartTypeEnum.Init;

        sFeeData = feeData;
    }

    public static void InitNextGameStaticVariable(string roomId)
    {
        sRoomId = roomId;
        sStartType = MiniGameStartTypeEnum.InitNextGame;
    }

    public static void InitReMatchStaticVariable(string gameId)
    {
        sGameId = gameId;
        sStartType = MiniGameStartTypeEnum.InitReMatch;
    }

    #endregion

    [Header("MatchingView")]
    public UIMatchingView MatchingView;

    [Header("LoadingView")]
    public UILoadingView LoadingView;

    [Header("CountDownView")]
    public UICountDownView CountDownView;

    [Header("GamePlayView")]
    public GameObject gamePlayView;

    [Header("ResultView")]
    public GameObject resultView;

    [Header("CommonView")]

    public GameObject popupSpecialNotice;

    protected MiniGameModeEnum _gameMode;
    protected string _gameId;
    protected string _roomId;
    protected MiniGameStartTypeEnum _startType;

    private double _timeStartPause;
    protected bool _isInGame;

    protected bool _IsGameFinished = false;
    protected float _currentTime;

    protected string sKey = string.Empty;

    // 0
    public void Init(MiniGameModeEnum gameMode, string gameId)
    {
        _gameMode = gameMode;
        _gameId = gameId;
        _startType = MiniGameStartTypeEnum.Init;

        if (_gameMode == MiniGameModeEnum.HeadToHead)
        {
            LoadMatchingView();
        }
        else
        {
            LoadGameContent();
        }
    }

    public void InitNextGame(string roomId)
    {
        _roomId = roomId;
        _startType = MiniGameStartTypeEnum.InitNextGame;

        LoadGameContent();
    }

    public void InitReMatch(string gameId)
    {
        _gameId = gameId;
        _startType = MiniGameStartTypeEnum.InitReMatch;

        LoadGameContent();
    }

    protected void GetUserSecret(Action callBack)
    {
        if (!string.IsNullOrEmpty(sKey))
        {
            callBack.Invoke();
            return;
        }

        StartCoroutine(Current.Ins.userAPI.GetUserSecret(res =>
        {
            if (res == null || res.errorCode != 0)
            {
                OnCallBackAPIError(res);
                return;
            }

            sKey = res.data.secret;

            callBack.Invoke();
        }));
    }

    // 1
    protected virtual void LoadMatchingView()
    {
        MatchingView.LoadMatchingView();
        LoadingView.gameObject.SetActive(false);
        CountDownView.gameObject.SetActive(false);
        gamePlayView.SetActive(false);
        resultView.SetActive(false);
    }

    protected class MatchData
    {
        public IResponse Response { get; set; }
        public int GameId { get; set; }
        public int RoomId { get; set; }
        public bool IsSync { get; set; }
        public UserResponse User { get; set; }
        public Action<bool> UpdateIsSyncAction { get; set; }
        public Action<DataUIBase.OpponentScore> UpdateOpponentScoreAction { get; set; }
    }

    private IDisposable _matchDp;
    protected Subject<UserResponse> _userRx = new Subject<UserResponse>();

    protected MatchData _matchData = new MatchData();
    protected async void Matching()
    {
        if (_matchData.User != null) UpdateOpponentInfo(_matchData.User.name, _matchData.User.avatar, _matchData.User.avatarFrame);

        if (_matchData.IsSync && _matchData.User == null)
        {
            MatchingView.SetMatchCountDown(_matchData.RoomId.ToString(), onCancel: () =>
            {
                _matchDp.Dispose();

                Current.Ins.socketAPI.RemoveSocket(StringConst.SOCKET_MATCH_USER_EVENT);
                SceneHelper.LoadScene(SceneEnum.Home);
            });

            int timeRemaining = 5;

            AddJob(() => MatchingView.SetTextMatchDownWithAnimationAction($"{timeRemaining}"));

            _matchDp = Observable.CombineLatest(_userRx, Observable.Interval(TimeSpan.FromSeconds(1)),
                (user, time) =>
                {
                    if (user == null) timeRemaining--;
                    return (user, timeRemaining);
                })
                .Subscribe((result) =>
                {
                    if (result.user == null && result.timeRemaining > 0)
                    {
                        AddJob(() => MatchingView.SetTextMatchDownWithAnimationAction($"{result.timeRemaining}"));
                        return;
                    }

                    _matchDp.Dispose();
                    Debug.Log("ddd");
                    if (result.user == null && result.timeRemaining <= 0)
                    {
                        AddJob(() => LoadGameContent(_matchData.Response));
                        _matchData.UpdateIsSyncAction(false);
                        return;
                    }

                    AddJob(() =>
                    {
                        IEnumerator DelayCall()
                        {
                            yield return new WaitForSeconds(2);
                            LoadGameContent(_matchData.Response);
                        }

                        StartCoroutine(DelayCall());
                    });
                });
        }

        if (_matchData.User != null)
        {
            await Task.Delay(TimeSpan.FromSeconds(2f));

            await StartCoroutine(Current.Ins.gameAPI.GetLiveScoreGame(id: _matchData.GameId.ToString(), callBack: (res) =>
            {
                if (res != null && res.errorCode == 0)
                {
                    DataUIBase.OpponentScore oScore = new DataUIBase.OpponentScore();
                    oScore.Scores = res
                        .data
                        .Select(x =>
                        {
                            return new DataUIBase.OpponentScore.ScoreData()
                            {
                                Score = x.score,
                                Time = x.time
                            };
                        })
                        .OrderBy(x => x.Time)
                        .ThenBy(x => x.Score)
                        .ToList();

                    _matchData.UpdateOpponentScoreAction.Invoke(oScore);
                }

                LoadGameContent(_matchData.Response);
            }));
        }
    }

    protected virtual void UpdateOpponentInfoIngame(MatchUserSocketResponse opp)
    {

    }

    protected virtual void UpdateOpponentInfo(string playerName, string avt, string avtFrame)
    {
        AddJob(() => MatchingView.UpdateOpponentInfo(playerName, avt, avtFrame));
    }

    // 2
    protected virtual void LoadGameContent(IResponse response = null)
    {
        _isInGame = true;
        MatchingView.gameObject.SetActive(false);
        LoadingView.gameObject.SetActive(true);
        CountDownView.gameObject.SetActive(false);
        gamePlayView.SetActive(false);
        resultView.SetActive(false);

        LoadingView.UpdateProgresbar(0.3f);
    }

    // 3
    protected virtual async Task<bool> ParseFromAPI(IResponse response)
    {
        return await Task.Run(() => true);
    }

    protected virtual async void OnCallBackFromAPI(IResponse response)
    {
        LoadingView.UpdateProgresbar(0.5f);
        if (await ParseFromAPI(response))
        {
            await Task.Delay(TimeSpan.FromSeconds(1f));

            LoadingView.UpdateProgresbar(1f);
            OnGameContentLoaded();
        }
        else
        {
            OnCallBackAPIError(response);
        }
    }

    protected virtual void OnCallBackAPIError(IResponse response)
    {
        UIPopUpManager.instance.ShowErrorPopUpForGoHome("Request Error : OnCallBackAPIError", null, SceneEnum.Home);
    }

    // 4
    protected virtual void OnGameContentLoaded()
    {
        StartCoroutine(LoadCountDown());
    }

    // 5
    private int _countDownTime = 3;
    protected virtual IEnumerator LoadCountDown()
    {
        if (_countDownTime == 3)
        {
            yield return new WaitForSeconds(1f);
            MatchingView.gameObject.SetActive(false);
            LoadingView.gameObject.SetActive(false);
            CountDownView.gameObject.SetActive(true);
            gamePlayView.SetActive(false);
            resultView.SetActive(false);
        }

        Sequence sq = DOTween.Sequence();

        if (_countDownTime == 3)
        {
            SoundManager.instance.PlayUIEffect("se_select");
            CountDownView.CountDown_3.SetActive(true);
            sq.Join(CountDownView.CountDown_1.transform.DOScale(new Vector3(0, 0, 0), 0.3f))
                .Join(CountDownView.CountDown_2.transform.DOScale(new Vector3(0, 0, 0), 0.3f))
                .Join(CountDownView.CountDown_3.transform.DOScale(new Vector3(1, 1, 1), 0.3f))
                .OnComplete(() =>
                {
                    CountDownView.CountDown_1.SetActive(false);
                    CountDownView.CountDown_2.SetActive(false);
                });
        }
        else if (_countDownTime == 2)
        {
            SoundManager.instance.PlayUIEffect("se_select");
            CountDownView.CountDown_2.SetActive(true);
            sq.Join(CountDownView.CountDown_1.transform.DOScale(new Vector3(0, 0, 0), 0.3f))
                .Join(CountDownView.CountDown_2.transform.DOScale(new Vector3(1, 1, 1), 0.3f))
                .Join(CountDownView.CountDown_3.transform.DOScale(new Vector3(0, 0, 0), 0.3f))
                .OnComplete(() =>
                {
                    CountDownView.CountDown_1.SetActive(false);
                    CountDownView.CountDown_3.SetActive(false);
                });
        }
        else
        {
            SoundManager.instance.PlayUIEffect("se_select");
            CountDownView.CountDown_1.SetActive(true);
            sq.Join(CountDownView.CountDown_1.transform.DOScale(new Vector3(1, 1, 1), 0.3f))
                .Join(CountDownView.CountDown_2.transform.DOScale(new Vector3(0, 0, 0), 0.3f))
                .Join(CountDownView.CountDown_3.transform.DOScale(new Vector3(0, 0, 0), 0.3f))
                .OnComplete(() =>
                {
                    CountDownView.CountDown_2.SetActive(false);
                    CountDownView.CountDown_3.SetActive(false);
                });
        }

        sq.Play();

        _countDownTime--;

        yield return new WaitForSeconds(1);

        if (_countDownTime == 0)
        {
            LoadGamePlay();
        }
        else
        {
            StartCoroutine(LoadCountDown());
        }
    }

    // 6
    protected virtual void LoadGamePlay()
    {
        MatchingView.gameObject.SetActive(false);
        LoadingView.gameObject.SetActive(false);
        CountDownView.gameObject.SetActive(false);
        gamePlayView.SetActive(true);
        resultView.SetActive(false);
    }

    // 7
    protected virtual void LoadResult()
    {
        resultView.SetActive(true);
        _isInGame = false;
    }

    protected virtual void LoadOneToManyResult(StatusRoomEnum status, string roomId, GameObject matchResult, MatchOneToManyData oneToManyData, SaveScoreGameResponse res)
    {
        if (status == StatusRoomEnum.Complete)
        {
            void GetRoomResult(string roomId)
            {
                UIPopUpManager.instance.ShowLoadingCircle(true);
                StartCoroutine(Current.Ins.historyAPI.GetTournamentRoomResult(roomId, (roomres) =>
                {
                    UIPopUpManager.instance.ShowLoadingCircle(true);
                    if (roomres == null || roomres.errorCode != 0)
                    {
                        UIPopUpManager.instance.ShowErrorPopUp("Request Error : GetRoomResult", res, () => GetRoomResult(roomId));
                        return;
                    }

                    oneToManyData.FinishedUserDatas = new List<MatchOneToManyData.FinishedUserData>();
                    foreach (var user in roomres.data)
                    {
                        oneToManyData.FinishedUserDatas.Add(new MatchOneToManyData.FinishedUserData
                        {
                            UserId = user.user.id,
                            Rank = user.rank
                        });
                    }

                    matchResult.GetComponent<UIMatchResult>().ShowOneToMany(res, oneToManyData, sFeeData,
                        newMatchCallBack: () =>
                        {
                            InitStaticVariable(MiniGameModeEnum.OneToMany, _gameId, sFeeData);
                            SceneHelper.ReloadScene();
                        });
                }));
            }

            GetRoomResult(roomId);
        }
        else
        {
            matchResult.GetComponent<UIMatchResult>().ShowOneToMany(res, oneToManyData, sFeeData,
                newMatchCallBack: () =>
                {
                    InitStaticVariable(MiniGameModeEnum.OneToMany, _gameId, sFeeData);
                    SceneHelper.ReloadScene();
                });
        }
    }

    protected virtual void OnResumeGame(double pauseTime) { }




    private void OnApplicationPause(bool pause)
    {
        if (pause) _timeStartPause = TimeManager.CurrentTimeInMilliSecond;
        else OnResumeGame(TimeManager.CurrentTimeInMilliSecond - _timeStartPause);
    }

    protected override void OnDestroy()
    {
        sKey = string.Empty;
    }

    #region Tutorial
    public static void InitTutorialGameStaticVariable(MiniGameEnum game)
    {
        sStartType = MiniGameStartTypeEnum.Tutorial;
    }

    public void InitTutorialGame()
    {
        _startType = MiniGameStartTypeEnum.Tutorial;
        LoadTutorialGameContent();
    }

    protected virtual void LoadTutorialGameContent()
    {
        MatchingView.gameObject.SetActive(false);
        LoadingView.gameObject.SetActive(true);
        CountDownView.gameObject.SetActive(false);
        gamePlayView.SetActive(false);
        resultView.SetActive(false);

        LoadingView.UpdateProgresbar(0.3f);
    }

    protected virtual async void AfterMakeTutorialData()
    {
        LoadingView.UpdateProgresbar(0.5f);
        LoadingView.UpdateProgresbar(0.5f);
        await Task.Delay(TimeSpan.FromSeconds(1f));
        LoadingView.UpdateProgresbar(1f);
        OnGameContentLoaded();
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        Current.Ins.socketAPI.RemoveSocket(StringConst.SOCKET_LIVE_SCORE_EVENT);
        Current.Ins.socketAPI.RemoveSocket(StringConst.SOCKET_EXIT_GAME_EVENT);
        Current.Ins.socketAPI.RemoveSocket(StringConst.SOCKET_MATCH_USER_EVENT);
    }
    #endregion
}
