using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Localization.Components;
using TMPro;

public class ResultUserInfo : MonoBehaviour
{


    [SerializeField]
    private GameObject baseObj, winObj, loseObj, avatarFrame;

    [SerializeField]
    private List<RawImage> avatarImages = new List<RawImage>();

    [SerializeField]
    private TextMeshProUGUI nameText;

    [SerializeField]
    private GameObject pointText;
    private Animator aniCon => GetComponent<Animator>();

    // Start is called before the first frame update
    void Start()
    {
        

    }

    private void OnEnable()
    {
        if (baseObj != null) baseObj.SetActive(true);
        if (winObj != null) winObj.SetActive(false);
        if (loseObj != null) loseObj.SetActive(false);
        if (avatarFrame != null) avatarFrame.SetActive(true);

        aniCon.Play("Idle");
    }

    public void SetUserAvatar(Texture2D texture)
    {
        avatarImages.ForEach(obj => obj.texture = texture);
    }

    public void SetUserInfo(string name, int point)
    {
        nameText.text = name;
        pointText.GetComponent<TextMeshProUGUI>().text = point.ToString();//.UpdateValue("point", OutGameEnum.VariableEnum.Int, point);
    }
    public void SetUserInfo(string name, string message)
    {
        nameText.text = name;
        pointText.GetComponent<TextMeshProUGUI>().text = message;
    }

    public void SetResultAni(bool isWin)
    {
        SoundManager.instance.PlayUIEffect(isWin ? "se_drum" : "se_betting");
        baseObj.SetActive(false);
        avatarFrame.SetActive(isWin);

        winObj.SetActive(isWin);
        loseObj.SetActive(!isWin);

        aniCon.SetTrigger(isWin ? "win" : "lose");
    }


}
