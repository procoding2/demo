using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using GameEnum;
using AnimationStateEnums;
using DG.Tweening;

public class BoosterControl : MonoBehaviour
{
    public GameObject wildDaub;
    public GameObject pickBall;
    public GameObject doubleScore;
    public GameObject bonusTime;
    public GameObject questionBox;

    public Animator progressAnimator;

    private GameObject _pendingBooster;
    private Action _onClickBooster;

    public Action OnClickWhenTutorial;

    public void SetPendingBooster(BingoBoosterTypeEnum type, Action onClick)
    {
        switch (type)
        {
            case BingoBoosterTypeEnum.WildDaub:
                _pendingBooster = wildDaub;
                break;
            case BingoBoosterTypeEnum.PickABall:
                _pendingBooster = pickBall;
                break;
            case BingoBoosterTypeEnum.DoubleScore:
                _pendingBooster = doubleScore;
                break;
            case BingoBoosterTypeEnum.BonusTime:
                _pendingBooster = bonusTime;
                break;
        }

        _onClickBooster = onClick;

        ActiveObject(questionBox);

        _initTime = TimeManager.CurrentTimeInSecond;
    }

    private void ActiveObject(GameObject gameObject)
    {


        Vector3 vectorOff = new Vector3(0, 0, 0);
        Vector3 vectorOn = new Vector3(1, 1, 1);
        float duration = 0.3f;

        Sequence sq = DOTween.Sequence();
        sq.Join(wildDaub.transform.DOScale(vectorOff, duration))
          .Join(pickBall.transform.DOScale(vectorOff, duration))
          .Join(doubleScore.transform.DOScale(vectorOff, duration))
          .Join(bonusTime.transform.DOScale(vectorOff, duration))
          .Join(questionBox.transform.DOScale(vectorOff, duration))
          .OnComplete(() =>
          {
              wildDaub.SetActive(false);
              pickBall.SetActive(false);
              doubleScore.SetActive(false);
              bonusTime.SetActive(false);
              questionBox.SetActive(false);

              gameObject.SetActive(true);
              gameObject.transform.DOScale(vectorOn, duration);
          });

        sq.Play();
    }

    private BoosterProgressAnimationStateEnum _boosterState = BoosterProgressAnimationStateEnum.State_0;
    public bool IsMax()
    {
        return _boosterState == BoosterProgressAnimationStateEnum.State_100;
    }

    public int _initTime;
    public int getInitTime()
    {
        return _initTime;
    }

    public void InCreaseProgress()
    {
        switch (_boosterState)
        {
            case BoosterProgressAnimationStateEnum.State_0:
                _boosterState = BoosterProgressAnimationStateEnum.State_50;
                break;
            case BoosterProgressAnimationStateEnum.State_50:
                _boosterState = BoosterProgressAnimationStateEnum.State_100;
                ActiveObject(_pendingBooster);
                break;
            case BoosterProgressAnimationStateEnum.State_100:
                _boosterState = BoosterProgressAnimationStateEnum.State_0;
                break;
        }

        progressAnimator.SetTrigger(_boosterState.GetStringValue());
    }

    public void OnClick()
    {
        var bingoUI = (UIBingo)UIBingo.Ins;

        if (_boosterState != BoosterProgressAnimationStateEnum.State_100)
            return;



        if (bingoUI.IsCanPlay == false)
        {
            SoundManager.instance.PlayUIEffect("se_cancel");
            return;
        }


        SoundManager.instance.PlayUIEffect("se_combo10_01");

        _onClickBooster();
        bingoUI.getNextBooster(this);
        InCreaseProgress();
        StartCoroutine(CallBackTutorial());
    }

    public void RemoveActionClick()
    {
        wildDaub.GetComponent<Button>().interactable = false;
        pickBall.GetComponent<Button>().interactable = false;
        doubleScore.GetComponent<Button>().interactable = false;
        bonusTime.GetComponent<Button>().interactable = false;
    }

    private IEnumerator CallBackTutorial()
    {
        yield return new WaitForSeconds(0.5f);
        OnClickWhenTutorial?.Invoke();
    }
}
