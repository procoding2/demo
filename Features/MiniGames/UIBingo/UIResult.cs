using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

using GameEnum;
using TMPro;

public class UIResult : UIBase<UIResult>
{
    protected override void InitIns() => Ins = this;

    public GameObject TimesUpTitle;
    public GameObject GameOverTitle;
    public GameObject SuccessMissionTitle;

    public GameObject Daubs;
    public GameObject Bingos;
    public GameObject MultiBingos;
    public GameObject DoubleScore;
    public GameObject Penalties;

    public TextMeshProUGUI FinalScores;

    public GameObject submitBtn;

    public void Init(BingoScoreResult result,
        GameFinishTypeEnum finishType,
        string gameId,
        string key,
        Action<SaveScoreGameResponse> afterSubmitCallBack)
    {
        switch (finishType)
        {
            case GameFinishTypeEnum.TimesUp:
                TimesUpTitle.SetActive(true);
                GameOverTitle.SetActive(false);
                SuccessMissionTitle.SetActive(false);
                break;
            case GameFinishTypeEnum.GameOver:
                TimesUpTitle.SetActive(false);
                GameOverTitle.SetActive(true);
                SuccessMissionTitle.SetActive(false);
                break;
            case GameFinishTypeEnum.Success:
                TimesUpTitle.SetActive(false);
                GameOverTitle.SetActive(false);
                SuccessMissionTitle.SetActive(true);
                break;
        }

        var daubScore = result.DaubsSum();
        Daubs.GetComponent<DetailItemResult>().
            Init
            (
                result.Daubs.Count.ToString(),
                daubScore.ToString()
            );

        var bingoScore = result.BingosSum();
        Bingos.GetComponent<DetailItemResult>().
            Init
            (
                result.Bingos.Count.ToString(),
                bingoScore.ToString()
            );

        var multiBingosScore = result.MultiBingosSum();
        MultiBingos.GetComponent<DetailItemResult>().
            Init
            (
                result.MultiBingos.Count.ToString(),
                multiBingosScore.ToString()
            );

        var doubleScores = result.DoubleScoresSum();
        DoubleScore.GetComponent<DetailItemResult>().
            Init
            (
                result.DoubleScores.Count.ToString(),
                doubleScores.ToString()
            );

        var penaltiesScore = result.PenaltiesSum();
        Penalties.GetComponent<DetailItemResult>().
            Init
            (
                result.Penalties.Count.ToString(),
                penaltiesScore.ToString()
            );

        var totalScore = daubScore + bingoScore + multiBingosScore + doubleScores + penaltiesScore;
        totalScore = totalScore >= 0 ? totalScore : 0;
        FinalScores.text = totalScore.ToString();

        SaveScoreGameRequest req = new SaveScoreGameRequest();
        req.key = key;
        req.status = (int)finishType;
        req.score = totalScore;
        req.scoreInfo = new SaveScoreGameRequest.ScoreInfo[5];
        req.scoreInfo[0] = new SaveScoreGameRequest.ScoreInfo()
        {
            name = StringConst.BINGO_SCORE_INFO_DAUBS,
            quantity = result.Daubs.Count,
            score = daubScore
        };

        req.scoreInfo[1] = new SaveScoreGameRequest.ScoreInfo()
        {
            name = StringConst.BINGO_SCORE_INFO_BINGOS,
            quantity = result.Bingos.Count,
            score = bingoScore
        };

        req.scoreInfo[2] = new SaveScoreGameRequest.ScoreInfo()
        {
            name = StringConst.BINGO_SCORE_INFO_MULTI_BINGOS,
            quantity = result.MultiBingos.Count,
            score = multiBingosScore
        };

        req.scoreInfo[3] = new SaveScoreGameRequest.ScoreInfo()
        {
            name = StringConst.BINGO_SCORE_INFO_DOUBLE_SCORES,
            quantity = result.DoubleScores.Count,
            score = doubleScores
        };

        req.scoreInfo[4] = new SaveScoreGameRequest.ScoreInfo()
        {
            name = StringConst.BINGO_SCORE_INFO_PENTALTIES,
            quantity = result.Penalties.Count,
            score = penaltiesScore
        };

        req.time = result.EndTimeMilliSecond - result.StartTimeMilliSecond;

        UIPopUpManager.instance.ShowLoadingCircle(true);
        StartCoroutine(Current.Ins.gameAPI.SaveScoreGame<SaveScoreGameResponse>(gameId, req, (res) =>
        {
            UIPopUpManager.instance.ShowLoadingCircle(false);
            if (res == null || res.errorCode != 0)
            {
                if (res != null && res.errorCode == 2)
                {
                    GetGameScoreWhenError(gameId, key, afterSubmitCallBack);
                    return;
                }
                UIPopUpManager.instance.ShowErrorPopUp("Request Error : SaveScoreGame", res, () => submitBtn.GetComponent<Button>().onClick?.Invoke());
                return;
            }

            res.dataDecode = JsonUtility.FromJson<SaveScoreGameResponse.DataDecode>(EncryptHelper.DecryptWithAES256(res.data.info, key));
            afterSubmitCallBack.Invoke(res);
        }));
    }

    private void GetGameScoreWhenError(string gameId, string key, Action<SaveScoreGameResponse> afterSubmitCallBack)
    {
        UIPopUpManager.instance.ShowLoadingCircle(true);
        StartCoroutine(Current.Ins.gameAPI.GetGameScore(gameId, (res) =>
        {
            UIPopUpManager.instance.ShowLoadingCircle(false);
            if (res == null || res.errorCode != 0)
            {
                UIPopUpManager.instance.ShowErrorPopUp("Request Error : GetGameScoreWhenError", res, () => GetGameScoreWhenError(gameId, key, afterSubmitCallBack));
                return;
            }

            res.dataDecode = JsonUtility.FromJson<SaveScoreGameResponse.DataDecode>(EncryptHelper.DecryptWithAES256(res.data.info, key));

            afterSubmitCallBack.Invoke(res);
        }));
    }
}
