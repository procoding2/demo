using DG.Tweening;
using System;
using TMPro;
using UniRx;
using UnityEngine;
using CodeStage.AntiCheat.ObscuredTypes;

public class WildDaubBooster : UIBase<WildDaubBooster>
{
    protected override void InitIns() => Ins = this;

    public TMP_Text wildDaubTime;

    private Action _onStopCallback;
    public void StartBooster(Action onStopCallBack, bool isTutorial)
    {
        _onStopCallback = onStopCallBack;

        if (!isTutorial)
        {
            bags.Add(wildDaubTimerRx());
            _timerRx.OnNext(10);
            bags.Add(TimeInterval());
        }

        gameObject.transform.localScale = new Vector3(0, 0, 0);
        gameObject.SetActive(true);
        gameObject.transform.DOScale(new Vector3(1, 1, 1), 0.3f);
    }

    public void StopBooster()
    {
        gameObject.transform.DOScale(new Vector3(0, 0, 0), 0.3f)    
            .OnComplete(() =>
            {
                gameObject.SetActive(false);
                gameObject.transform.localScale = new Vector3(1, 1, 1);
            });
        bags.ForEach(d => d.Dispose());
    }

    /// <summary>
    /// WidlDaub Timer Rx
    /// </summary>
    private Subject<ObscuredFloat> _timerRx = new Subject<ObscuredFloat>();
    private IDisposable wildDaubTimerRx()
    {
        return _timerRx
            .Scan((pre, curr) =>
            {
                float nextTime = pre - curr;
                return nextTime <= 0 ? 0 : nextTime;
            })
            .DistinctUntilChanged()
            .Subscribe(value =>
            {
                int nValue = (int)value;
                if (nValue <= 0)
                {
                    _onStopCallback.Invoke();
                    return;
                }

                wildDaubTime.text = (nValue - 1).ToString();
            });
    }

    private IDisposable TimeInterval()
    {
        return Observable
            .Interval(TimeSpan.FromSeconds(1))
            .Subscribe(x =>
            {
                _timerRx.OnNext(1);
            });
    }

    private double _timeStartPause;
    private void OnApplicationPause(bool pause)
    {
        if (pause) _timeStartPause = TimeManager.CurrentTimeInMilliSecond;
        else
        {
            double timePause = TimeManager.CurrentTimeInMilliSecond - _timeStartPause;
            _timerRx.OnNext((float)timePause / 1000f);
        }
    }
}
