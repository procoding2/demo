using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

using AnimationStateEnums;
using GameEnum;
using DG.Tweening;
using CodeStage.AntiCheat.ObscuredTypes;

public class BingoBoard : UIBase<BingoBoard>
{
    protected override void InitIns() => Ins = this;

    public BingoRow[] bingoRows;

    private UIBingo bingoUI;
    private BingoData _gameData;

    private List<BingoRow> _bingoLineLst;

    public void Init(BingoData data, Action onDone)
    {
        bingoUI = (UIBingo)UIBingo.Ins;
        _gameData = data;

        InitBoard();

        StartCoroutine(InitShowNumber(onDone));
    }

    private IEnumerator InitShowNumber(Action onDone)
    {
        //bingoUI.BubbleEffectObj.SetActive(true);
        foreach (var row in bingoRows)
        {
            foreach (var item in row.items)
            {
                if (item.Number == 0) continue;
                item.disActiveStar();
                yield return new WaitForSeconds(0.07f);
            }
        }

        onDone.Invoke();
    }

    private void InitBoard()
    {
        _bingoLineLst = new List<BingoRow>();
        BingoRow firstBingo = gameObject.AddComponent<BingoRow>();
        firstBingo.items = new BingoItem[5];
        firstBingo.items[0] = bingoRows[0].items[0];
        firstBingo.items[1] = bingoRows[0].items[4];
        firstBingo.items[2] = bingoRows[2].items[2];
        firstBingo.items[3] = bingoRows[4].items[0];
        firstBingo.items[4] = bingoRows[4].items[4];
        _bingoLineLst.Add(firstBingo);

        // Diagonal
        BingoRow bingoDiaLine1 = gameObject.AddComponent<BingoRow>();
        bingoDiaLine1.items = new BingoItem[5];
        int bingoDiaPos1 = 0;

        BingoRow bingoDiaLine2 = gameObject.AddComponent<BingoRow>();
        bingoDiaLine2.items = new BingoItem[5];
        int bingoDiaPos2 = 0;

        for (int col = 0; col <= 4; col++)
        {
            // Horizontal
            BingoRow bingoHorLine = gameObject.AddComponent<BingoRow>();
            bingoHorLine.items = new BingoItem[5];

            // Vertical
            BingoRow bingoVerLine = gameObject.AddComponent<BingoRow>();
            bingoVerLine.items = new BingoItem[5];

            for (int row = 0; row <= 4; row++)
            {
                var item = bingoRows[row].items[col];

                item.Row = row;
                item.Col = col;

                // Horizontal
                bingoHorLine.items[row] = item;

                // Vertical
                var verItem = bingoRows[col].items[row];
                verItem.Row = col;
                verItem.Col = row;
                bingoVerLine.items[row] = verItem;

                // Diagonal
                if (col == row)
                {
                    bingoDiaLine1.items[bingoDiaPos1++] = item;

                    if (bingoDiaPos1 > 4)
                    {
                        _bingoLineLst.Add(bingoDiaLine1);
                    }
                }

                bool isCenterItem = col == 2 && row == 2;
                if (isCenterItem || row + col == 4)
                {
                    bingoDiaLine2.items[bingoDiaPos2++] = item;

                    if (bingoDiaPos2 > 4)
                    {
                        _bingoLineLst.Add(bingoDiaLine2);
                    }
                }

                if (isCenterItem)
                {
                    item.Number = 0;
                    item.star.SetActive(true);
                    item.TypeActive = BingoActiveStarByTypeEnum.Normal;
                    continue;
                }

                item.Number = _gameData.board[row, col];

                item.transform.gameObject
                    .GetComponent<Button>()
                    .onClick
                    .AddListener(() => onClickBingoItem(item));
            }

            _bingoLineLst.Add(bingoHorLine);
            _bingoLineLst.Add(bingoVerLine);
        }
    }

    private void onClickBingoItem(BingoItem item)
    {
        if (!bingoUI.IsCanPlay) return;
        if (item.isOpened() || item.isStar()) return;
        if (bingoUI.IsWaitForWildDaubTutorial) return;

        Action afterActive = () =>
        {
            // Check bingo valid
            if (GetBingoLines().Count > 0)
            {
                // Enable bingoBtn
                bingoUI.updateBingoBtn(true);
            }

            // Booster
            bingoUI.boosterIncrease();

            if (GetUnSelectedNumber().Count == 0)
            {
                bingoUI.CompleteAll();
            }
        };
        
        // WildDaubBooster
        if (bingoUI.isBoosterWildDaubActive)
        {
            if (bingoUI.IsTutorial)
            {
                if (item.Number != bingoRows[3].items[0].Number) return;
            }

            //HCSound.Ins.PlayGameSound(HCSound.Ins.Bingo.Correct);
            SoundManager.instance.PlayEffect("se_combo10_01");
            item.activeStar(BingoActiveStarByTypeEnum.WildDaub);
            bingoUI.ShowCollectEffect(item.transform.position);
            bingoUI.onStopWildDaub();
            afterActive.Invoke();

            var score = (BingoScoreTypeEnum.Daubs, 100);
            bingoUI.updateScore(score);

            IEnumerator LazyScore()
            {
                yield return new WaitForSeconds(0.3f);
                bingoUI.ShowScoreEffect(score.Item2, item.transform.position);
            }

            StartCoroutine(LazyScore());
            
            return;
        }

        // PickABallBooster
        if (bingoUI.isBoosterPickABallActive)
        {
            //HCSound.Ins.PlayGameSound(HCSound.Ins.Bingo.Correct);
            SoundManager.instance.PlayEffect("se_combo10_01");
            item.activeStar(BingoActiveStarByTypeEnum.PickABall);
            bingoUI.ShowCollectEffect(item.transform.position);
            afterActive.Invoke();

            var score = (BingoScoreTypeEnum.Daubs, 100);
            bingoUI.updateScore(score);
            bingoUI.ShowScoreEffect(score.Item2, item.transform.position);

            return;
        }

        // Check match
        var children = bingoUI
            .ballonLayout
            .GetComponentsInChildren<ControlBalloonPrefab>()
            .Where(c => c.Number == item.Number)
            .ToList();

        if (children.Count == 0)
        {
            if (!bingoUI.isBoosterPickABallActive)
            {
                bingoUI.Penalty(item.gameObject);
            }

            return;
        }

        bool isActiveValue = children.Select(x => x.Number).Contains(bingoUI.currentBalloonValue);

        Action callback = () =>
        {
            // Score
            var score = (BingoScoreTypeEnum.Daubs, 100);
            if (isActiveValue)
            {
                var toast = bingoUI.Toast.GetComponent<BingoToast>();

                float x = 300f - bingoUI.BalloonTimeDistance * 100f;
                float ratio = 1 - (x / 300f);
                int sc = 150 - (int)(x * 50f / 300f);

                Debug.Log($"x: {x}, sc: {sc}, ratio: {ratio}");

                if (sc < 100) sc = 100;

                score = (BingoScoreTypeEnum.Daubs, sc);

                if (ratio >= 0.8f)
                {
                    toast.ShowToast(BingoToastTypeEnum.Amazing);
                }
                else if (ratio >= 0.6 && ratio < 0.8)
                {
                    toast.ShowToast(BingoToastTypeEnum.Perfect);
                }
                else if (ratio >= 0.4 && ratio < 0.6)
                {
                    toast.ShowToast(BingoToastTypeEnum.Great);
                }
                else if (ratio >= 0.2 && ratio < 0.4)
                {
                    toast.ShowToast(BingoToastTypeEnum.Good);
                }
                else
                {
                    toast.ShowToast(BingoToastTypeEnum.Cool);
                }
            }

            // DoubleScoreBooster
            if (bingoUI.isBoosterDoubleScoreActive)
            {
                score = (BingoScoreTypeEnum.DoubleScores, score.Item2 * 2);
            }

            bingoUI.updateScore(score);
            bingoUI.ShowScoreEffect(score.Item2, item.transform.position);
            //children.LastOrDefault()?.ShowScore(score.Item2);

            afterActive.Invoke();
        };

        //HCSound.Ins.PlayGameSound(HCSound.Ins.Bingo.Correct);
        SoundManager.instance.PlayEffect("se_combo10_01");
        bingoUI.ShowCollectEffect(item.transform.position);

        item.active(callback);
    }

    public List<BingoRow> GetBingoLines()
    {
        return _bingoLineLst
            .Where(line => line.items.ToList().All(item => item.isOpenOrStar()))
            .Where(line => line.items.Any(item => item.isOpenOrStarByBooster()))
            //.Where(line => line.items.Any(item => item.isOpened()))
            .ToList();
    }

    public void ChangeBingoLineToStar(List<BingoRow> lines, int linePoint, Action doneAction = null)
    {
        StartCoroutine(LazyChangeLineToStar(lines, linePoint, doneAction));
    }

    private IEnumerator LazyChangeLineToStar(List<BingoRow> lines, int linePoint, Action doneAction = null)
    {
        int i = 1;
        var score = (BingoScoreTypeEnum.Bingos, 1000);
        bingoUI.updateScore((BingoScoreTypeEnum.Bingos, lines.Count * score.Item2));
        
        foreach (var line in lines)
        {
            float diffValue = score.Item2 / line.items.Length;

            yield return new WaitForSeconds(0.1f);
            foreach (var item in line.items)
            {
                yield return new WaitForSeconds(0.1f);

                SoundManager.instance.PlayEffect($"se_combo{i}");

                item.activeStar(BingoActiveStarByTypeEnum.Normal);
                bingoUI.ShowCollectEffect(item.transform.position);
                bingoUI.ShowScoreEffect((int)diffValue, item.transform.position);
                if (++i > 10)
                {
                    i = 10;
                }
            }
        }

        yield return new WaitForSeconds(1f);
        doneAction?.Invoke();
    }

    public List<string> GetSelectedNumber()
    {
        var lstResult = new List<string>();
        foreach (var row in bingoRows)
        {
            foreach (var item in row.items)
            {
                if (item.isOpenOrStar())
                {
                    lstResult.Add(item.Number.ToString());
                }
            }
        }

        return lstResult;
    }

    public List<string> GetUnSelectedNumber()
    {
        var lstResult = new List<string>();
        foreach (var row in bingoRows)
        {
            foreach (var item in row.items)
            {
                if (!item.isOpenOrStar())
                {
                    lstResult.Add(item.Number.ToString());
                }
            }
        }

        return lstResult;
    }

    public void OpenByPickABallBooster(GameObject _ball, Action callBackPickDone)
    {
        GameObject ball = Instantiate(_ball);
        ball.transform.SetParent(transform);
        ball.transform.localScale = Vector3.one * 2.0f;
        ball.transform.position = Camera.main.ScreenToWorldPoint(_ball.transform.position);

        ObscuredInt value = _ball.GetComponent<ControlBalloonPrefab>().Number;

        BingoItem bItem = null;
        foreach (var row in bingoRows)
        {
            var it = row.items.Where(x => x.Number == value).FirstOrDefault();
            if (it != null)
            {
                bItem = it;
                break;
            }
        }

        if (bItem != null)
        {
            Sequence sq = DOTween.Sequence();
            sq.Join(ball.transform.DOMove(bItem.transform.position, 0.5f).SetEase(Ease.InOutQuart))
                .Join(ball.transform.DOScale(new Vector3(1, 1, 1), 0.5f))
                .OnComplete(() =>
                {
                    Destroy(ball);
                    onClickBingoItem(bItem);
                    callBackPickDone.Invoke();
                });

            sq.Play();
        }
        else
        {
            Destroy(ball);
            callBackPickDone.Invoke();
        }
    }
}

