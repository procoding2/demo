using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using GameEnum;
using DG.Tweening;

public class BingoToast : MonoBehaviour
{
    public GameObject Amazing;
    public GameObject Perfect;
    public GameObject Great;
    public GameObject Good;
    public GameObject Cool;
    public GameObject GoodJob;

    public GameObject charAniObj;

    private List<GameObject> gameObjects = new List<GameObject>();

    private void Awake()
    {
        gameObjects.Clear();
        gameObjects.Add(Amazing);
        gameObjects.Add(Perfect);
        gameObjects.Add(Great);
        gameObjects.Add(Good);
        gameObjects.Add(Cool);
        gameObjects.Add(GoodJob);
    }

    public void ShowToast(BingoToastTypeEnum toastType)
    {
        //if (charAniObj.activeSelf)
        //    charAniObj.SetActive(false);

        //charAniObj.SetActive(true);

        HideAll();

        GameObject obj = gameObjects[(int)toastType];
        if (obj)
        {
            obj.SetActive(true);
            obj.transform.DOScale(new Vector3(1, 1, 1), 0.3f);


            Sequence sq = DOTween.Sequence();
            sq.Join(obj.transform.DOScale(new Vector3(1.1f, 1.1f, 1.1f), 0.1f))
                .Append(obj.transform.DOScale(new Vector3(1, 1, 1), 0.2f))
                .Append(obj.transform.DOScale(new Vector3(1, 1, 1), 0.8f))
                .Append(obj.transform.DOScale(new Vector3(1.1f, 1.1f, 1.1f), 0.1f))
                .Append(obj.transform.DOScale(new Vector3(0, 0, 0), 0.3f))
                .OnComplete(() =>
                {
                    obj.SetActive(false);
                });

            sq.Play();
        }

        //switch (toastType)
        //{
        //    case BingoToastTypeEnum.Amazing:
        //        Amazing.SetActive(true);
        //        Amazing.transform
        //            .DOScale(new Vector3(1, 1, 1), 0.3f);
        //        break;

        //    case BingoToastTypeEnum.Perfect:
        //        Perfect.SetActive(true);
        //        Perfect.transform
        //           .DOScale(new Vector3(1, 1, 1), 0.3f);
        //        break;

        //    case BingoToastTypeEnum.Great:
        //        Great.SetActive(true);
        //        Great.transform
        //           .DOScale(new Vector3(1, 1, 1), 0.3f);
        //        break;

        //    case BingoToastTypeEnum.Good:
        //        Good.SetActive(true);
        //        Good.transform
        //           .DOScale(new Vector3(1, 1, 1), 0.3f);
        //        break;

        //    case BingoToastTypeEnum.Cool:
        //        Cool.SetActive(true);
        //        Cool.transform
        //           .DOScale(new Vector3(1, 1, 1), 0.3f);
        //        break;
        //    case BingoToastTypeEnum.Goodjob:
        //        GoodJob.SetActive(true);
        //        GoodJob.transform
        //           .DOScale(new Vector3(1, 1, 1), 0.3f);
        //        break;
        //}

        //StartCoroutine(WaitHideAll(1.0f));
    }

    private void HideAll()//bool withAnimation = false)
    {
        //if (withAnimation)
        //{
        //    Sequence sq = DOTween.Sequence();
        //    sq.Join(Amazing.transform.DOScale(new Vector3(0, 0, 0), 0.3f))
        //        .Join(Perfect.transform.DOScale(new Vector3(0, 0, 0), 0.3f))
        //        .Join(Great.transform.DOScale(new Vector3(0, 0, 0), 0.3f))
        //        .Join(Good.transform.DOScale(new Vector3(0, 0, 0), 0.3f))
        //        .Join(Cool.transform.DOScale(new Vector3(0, 0, 0), 0.3f))
        //        .Join(GoodJob.transform.DOScale(new Vector3(0, 0, 0), 0.3f))
        //        .OnComplete(() =>
        //        {
        //            Amazing.SetActive(false);
        //            Perfect.SetActive(false);
        //            Great.SetActive(false);
        //            Good.SetActive(false);
        //            Cool.SetActive(false);
        //            GoodJob.SetActive(false);
        //        });

        //    sq.Play();
        //}
        //else
        //{

        foreach (GameObject obj in gameObjects)
        {
            obj.SetActive(false);
            obj.transform.localScale = Vector3.zero;
        }


        //Amazing.SetActive(false);
        //Perfect.SetActive(false);
        //Great.SetActive(false);
        //Good.SetActive(false);
        //Cool.SetActive(false);
        //GoodJob.SetActive(false);

        //Amazing.transform.localScale = new Vector3(0, 0, 0);
        //Perfect.transform.localScale = new Vector3(0, 0, 0);
        //Great.transform.localScale = new Vector3(0, 0, 0);
        //Good.transform.localScale = new Vector3(0, 0, 0);
        //Cool.transform.localScale = new Vector3(0, 0, 0);
        //GoodJob.transform.localScale = new Vector3(0, 0, 0);
        //}
    }

    //private IEnumerator WaitHideAll(float time)
    //{
    //    yield return new WaitForSeconds(time);
    //    HideAll(withAnimation: true);
    //}
}
