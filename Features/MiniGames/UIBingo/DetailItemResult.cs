using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DetailItemResult : MonoBehaviour
{
    public TextMeshProUGUI Times;
    public TextMeshProUGUI Score;

    public void Init(string times, string Score)
    {
        this.Times.text = times;
        this.Score.text = StringHelper.NumberFormatter(Score);
    }
}