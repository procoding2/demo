using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using CodeStage.AntiCheat.ObscuredTypes;

public class PickABallBooster : UIBase<PickABallBooster>
{
    protected override void InitIns() => Ins = this;

    public TextMeshProUGUI timeRemainText;
    public GridLayoutGroup ballListLayout;
    public Button CloseBtn;

    public Action CallBackWhenTutorial;


    [SerializeField]
    private List<GameObject> shadowList = new List<GameObject>();

    public void ShowPickABall(
        List<GameObject> balls,
        Action<GameObject> onSelectedBall,
        Action onClose,
        bool isTutorial = false)
    {


        ObjectPoolManager.instance.HiddenAllUIPoolObjects();

        gameObject.transform.localScale = new Vector3(0, 0, 0);
        gameObject.SetActive(true);
        gameObject.transform.DOScale(new Vector3(1, 1, 1), 0.3f)
            .OnComplete(() =>
            {
                balls.ForEach(b =>
                {
                    b.transform.localScale = new Vector3(0, 0, 0);
                    b.transform.DOScale(new Vector3(2, 2, 2), 0.3f);
                });
            });

        ballListLayout.transform.RemoveAllChild();

        
        for (int i = 0; i< shadowList.Count; i++)
        {
            shadowList[i].SetActive(i < balls.Count);
        }

        int pos = 0;
        foreach (var b in balls)
        {
            b.transform.SetParent(ballListLayout.transform, false);
            b.GetComponent<Animator>().enabled = false;
            b.GetComponent<ControlBalloonPrefab>().OnHideProgress();

            if (isTutorial && pos++ != 2) continue;

            b.GetComponentInChildren<Button>().onClick.AddListener(() =>
            {
                SoundManager.instance.PlayUIEffect("se_combo10_01");
                onSelectedBall.Invoke(b);
                StartCoroutine(CallBackTutorial(isTutorial));
                StopBooster();
            });
        }

        CloseBtn.onClick.AddListener(() =>
        {
            
            gameObject.SetActive(false);
            onClose.Invoke();
        });

        if (isTutorial) return;

        bags.Add(TimerRx());
        _timerRx.OnNext(10);
        bags.Add(TimeInterval());
    }

    public void StopBooster()
    {
        //gameObject.SetActive(false);
        gameObject.transform.localScale = Vector3.zero;
        bags.ForEach(d => d.Dispose());

        //Sequence sq = DOTween.Sequence();
        //sq.AppendInterval(0.05f);
        //sq.Append(gameObject.transform.DOScale(Vector3.zero, 0.05f).SetEase(Ease.InOutBack));

        //sq.Play().OnComplete(() => { gameObject.SetActive(false); }) ;


    }

    private IEnumerator CallBackTutorial(bool isTutorial)
    {
        yield return new WaitForSeconds(0.5f);
        if (isTutorial) CallBackWhenTutorial?.Invoke();
    }

    /// <summary>
    /// Timer Rx
    /// </summary>
    private Subject<ObscuredFloat> _timerRx = new Subject<ObscuredFloat>();
    private IDisposable TimerRx()
    {
        return _timerRx
            .Scan((pre, curr) =>
            {
                float nextTime = pre - curr;
                return nextTime <= 0 ? 0 : nextTime;
            })
            .DistinctUntilChanged()
            .Subscribe(value =>
            {
                int nValue = (int)value;
                if (nValue <= 0)
                {
                    CloseBtn.onClick.Invoke();
                    return;
                }

                timeRemainText.text = $"{nValue - 1}";
            });
    }

    private IDisposable TimeInterval()
    {
        return Observable
            .Interval(TimeSpan.FromSeconds(1))
            .Subscribe(x =>
            {
                _timerRx.OnNext(1);
            });
    }

    private double _timeStartPause;
    private void OnApplicationPause(bool pause)
    {
        if (pause) _timeStartPause = TimeManager.CurrentTimeInMilliSecond;
        else
        {
            double timePause = TimeManager.CurrentTimeInMilliSecond - _timeStartPause;
            _timerRx.OnNext((float)timePause / 1000f);
        }
    }
}
