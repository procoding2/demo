using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.Localization.Components;
using OutGameEnum;
using DG.Tweening;
using System.Drawing;
using CodeStage.AntiCheat.ObscuredTypes;

public class DoubleScoreBooster : UIBase<DoubleScoreBooster>
{
    protected override void InitIns() => Ins = this;

    public LocalizeStringEvent doubleScoreText;

    private Action _onStopCallback;

    public void StartBooster(Action onStopCallBack)
    {
        _onStopCallback = onStopCallBack;

        bags.Add(TimerRx());

        _timerRx.OnNext(10);

        bags.Add(TimeInterval());

        gameObject.transform.localScale = Vector3.zero;
        //gameObject.SetActive(true);

        if (sqHide != null) sqHide.Kill();
            gameObject.transform.DOScale(Vector3.one, 0.3f);
    }

    public void IncreaseTimer()
    {
        _timerRx.OnNext(-10);
    }

    Sequence sqHide;
    public void StopBooster()
    {
        sqHide = DOTween.Sequence();
        sqHide.Join(gameObject.transform.DOScale(new Vector3(0, 0, 0), 0.3f)
            .OnComplete(() =>
            {
                //gameObject.SetActive(false);
                gameObject.transform.localScale = Vector3.zero;
            }));
        bags.ForEach(d => d.Dispose());
    }

    /// <summary>
    /// Timer Rx
    /// </summary>
    private Subject<ObscuredFloat> _timerRx = new Subject<ObscuredFloat>();
    private IDisposable TimerRx()
    {
        return _timerRx
            .Scan((pre, curr) =>
            {
                float nextTime = pre - curr;
                return nextTime <= 0 ? 0 : nextTime;
            })
            .DistinctUntilChanged()
            .Subscribe(value =>
            {
                int nValue = (int)value;
                if (nValue <= 0)
                {
                    _onStopCallback.Invoke();
                    return;
                }

                doubleScoreText.UpdateValue("x", VariableEnum.Int, nValue - 1);
            });
    }

    private IDisposable TimeInterval()
    {
        return Observable
            .Interval(TimeSpan.FromSeconds(1))
            .Subscribe(x =>
            {
                _timerRx.OnNext(1);
            });
    }

    private double _timeStartPause;
    private void OnApplicationPause(bool pause)
    {
        if (pause) _timeStartPause = TimeManager.CurrentTimeInMilliSecond;
        else
        {
            double timePause = TimeManager.CurrentTimeInMilliSecond - _timeStartPause;
            _timerRx.OnNext((float)timePause / 1000f);
        }
    }
}
