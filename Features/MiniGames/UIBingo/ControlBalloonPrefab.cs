using System;
using System.Collections;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using CodeStage.AntiCheat.ObscuredTypes;

public class ControlBalloonPrefab : MonoBehaviour
{
    public GameObject ball_b;
    public GameObject ball_i;
    public GameObject ball_n;
    public GameObject ball_g;
    public GameObject ball_o;

    private ObscuredInt _number;
    public ObscuredInt Number
    {
        get { return _number; }
        set
        {
            _number = value;
            numberTxt.text = _number.ToString();
        }
    }

    [SerializeField] private Text numberTxt;

    public TextMeshProUGUI scoreTxt;

    public GameObject backGroundProgress;
    public GameObject progress;

    public void Init(ObscuredInt number)
    {
        Number = number;

        if (number >= 1 && number <= 15)
        {
            ball_b.SetActive(true);
        }
        else if (number >= 16 && number <= 30)
        {
            ball_i.SetActive(true);
        }
        else if (number >= 31 && number <= 45)
        {
            ball_n.SetActive(true);
        }
        else if (number >= 46 && number <= 60)
        {
            ball_g.SetActive(true);
        }
        else
        {
            ball_o.SetActive(true);
        }
    }

    public Action OnStartCountDown;
    public void StartCountDown() => OnStartCountDown?.Invoke();

    public void OnClose() => Destroy(gameObject);

    public void ShowScore(int value)
    {
        scoreTxt.text = $"+ {value.ToString()}";
        scoreTxt.transform.DOScale(new Vector3(1, 1, 1), 0.3f)
            .OnComplete(() =>
            {
                StartCoroutine(HideScore());
            });
    }

    private IEnumerator HideScore()
    {
        yield return new WaitForSeconds(2f);
        if (scoreTxt.IsActive())
        {
            scoreTxt.transform.DOScale(new Vector3(0, 0, 0), 0.3f);
        }
    }

    public void OnHideProgress()
    {
        backGroundProgress.SetActive(false);
        progress.SetActive(false);
        OnCallBackOnHide?.Invoke();
    }

    public Action OnCallBackOnHide = null;
    public void OnHideAfterThreeSecond() => OnCallBackOnHide?.Invoke();
}
