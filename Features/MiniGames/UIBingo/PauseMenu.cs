using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{
    public GameObject main;
    public GameObject endMatchConfirm;

    public Button outSideBtn;
    public Button CloseBtn;

    public void ShowPauseMenu(Action cbEndNow, Action cbResume = null)
    {
        main.SetActive(true);
        endMatchConfirm.SetActive(false);

        _callBackEndNow = cbEndNow;
        _callBackResume = cbResume;

        outSideBtn
            .onClick
            .AddListener(() =>
            {
                gameObject.SetActive(false);
                main.SetActive(false);
                endMatchConfirm.SetActive(false);
                _callBackResume?.Invoke();
            });

        CloseBtn.onClick.AddListener(() => { outSideBtn.onClick.Invoke(); });
    }

    void OnClickTuturialBtn()
    {

    }

    public void OnClickQuitGametn()
    {
        main.SetActive(false);
        endMatchConfirm.SetActive(true);
    }

    private Action _callBackResume;
    public void OnClickResumeBtn()
    {
        main.SetActive(false);
        endMatchConfirm.SetActive(false);
        gameObject.SetActive(false);
        _callBackResume?.Invoke();
    }

    private Action _callBackEndNow;
    public void OnClickEndNowBtn()
    {
        main.SetActive(false);
        endMatchConfirm.SetActive(false);
        _callBackEndNow?.Invoke();
    }
}
