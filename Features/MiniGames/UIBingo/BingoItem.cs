using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using GameEnum;
using CodeStage.AntiCheat.ObscuredTypes;

public class BingoItem : MonoBehaviour
{
    public int Col;
    public int Row;

    public BingoActiveStarByTypeEnum TypeActive = BingoActiveStarByTypeEnum.None;

    private ObscuredInt _number;

    [HideInInspector]
    public ObscuredInt Number 
    {
        get { return _number; }
        set
        {
            _number = value;
            number.text = value.ToString();
        }
    }

    [SerializeField] private TextMeshProUGUI number;
    public GameObject bg_selected;
    public GameObject star;
    public GameObject MinusScoreTxt;

    public void active(Action callBack = null)
    {
        if (isOpened() || isStar()) return;

        //HCSound.Ins.PlayGameSound(HCSound.Ins.Bingo.Flip, true);
        SoundManager.instance.PlayUIEffect("se_type3");
        bg_selected.transform.localScale = new Vector3(0, 0, 0);
        bg_selected.SetActive(true);

        bg_selected.transform.DOScale(new Vector3(1, 1, 1), 0.3f);
        callBack?.Invoke();
    }

    public bool isOpened()
    {
        return bg_selected.activeSelf;
    }

    public void activeStar(BingoActiveStarByTypeEnum typeActive)
    {
        //if (isStar()) return;

        //HCSound.Ins.PlayGameSound(HCSound.Ins.Bingo.Flip, true);
        SoundManager.instance.PlayUIEffect("se_type3");
        if (TypeActive != BingoActiveStarByTypeEnum.Normal) TypeActive = typeActive;

        star.transform.localScale = new Vector3(0, 0, 0);
        star.SetActive(true);

        Sequence sq = DOTween.Sequence();
        sq.Join(bg_selected.transform.DOScale(new Vector3(0, 0, 0), 0.3f))
          .Join(star.transform.DOScale(new Vector3(1.1f, 1.1f, 1.1f), 0.05f))
          .Join(star.transform.DOScale(new Vector3(1,1,1), 0.3f))
          .OnComplete(() =>
          {
              bg_selected.SetActive(false);
          });

        if (isStar())
        {
            star.transform.DOScale(new Vector3(0.9f, 0.9f, 0.9f), 0.1f)
                .OnComplete(() => sq.Play());
        }
        else
        {
            sq.Play();
        }
    }

    public void disActiveStar()
    {
        if (!isStar()) return;

        //HCSound.Ins.PlayGameSound(HCSound.Ins.Bingo.Flip, true);
        SoundManager.instance.PlayUIEffect("se_type3");
        star.transform.DOLocalRotate(new Vector3(0,90,0), 0.25f)
            .OnComplete(() =>
            {
                star.SetActive(false);
                star.transform.localRotation = Quaternion.Euler(Vector3.zero);
            });
    }

    public bool isStar()
    {
        return star.activeSelf;
    }

    public bool isOpenOrStar()
    {
        return isOpened() || isStar();
    }

    public bool isOpenOrStarByBooster()
    {
        if (Row == 2 && Col == 2) return false;
        return isOpened() || (isStar() && (TypeActive == BingoActiveStarByTypeEnum.WildDaub || TypeActive == BingoActiveStarByTypeEnum.PickABall));
    }

    public void ShowMinusScore()
    {
        MinusScoreTxt.SetActive(false);
        MinusScoreTxt.transform.localScale = new Vector3(0, 0, 0);

        MinusScoreTxt.SetActive(true);

        StopCoroutine(nameof(StartCoroutine));
        MinusScoreTxt.transform.DOScale(new Vector3(1, 1, 1), 0.3f)
            .OnComplete(() => StartCoroutine(nameof(HideMinusScore)));
    }

    private IEnumerator HideMinusScore()
    {
        yield return new WaitForSeconds(1f);
        if (!MinusScoreTxt.activeInHierarchy) yield break;

        MinusScoreTxt.transform.DOScale(new Vector3(0, 0, 0), 0.3f)
            .OnComplete(() => MinusScoreTxt.SetActive(false));
    }
}
