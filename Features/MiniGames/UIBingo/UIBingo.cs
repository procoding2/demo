using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using GameEnum;
using AnimationStateEnums;
using System;
using System.Linq;
using UniRx;
using UnityEngine.Playables;
using DG.Tweening;
using System.Threading.Tasks;
using OutGameEnum;
using TMPro;
using CodeStage.AntiCheat.ObscuredTypes;

public class UIBingo : UIGameBase<UIBingo>
{
    protected override void InitIns() => Ins = this;

    [Header("Bingo")]
    public GameObject ballonLayout;
    public GameObject ballonPrefab;
    public GameObject minusScorePrefab;
    public GameObject boardLayout;

    [Header("User Info")]
    public UserInfo myUserInfo, opponentUserInfo;
    
    //public RawImage userAvt;
    //public TextMeshProUGUI userName;
    //public Text userScore;

    //public GameObject oppnentView;
    //public RawImage opponentAvt;
    //public TextMeshProUGUI opponentName;
    //public Text opponentScore;

    [Header("Time View")]
    public TimeView timeView;
    public GameObject ScoreTextObj;


    public GameObject bingoBtn;
    public GameObject bingoBtn_Active;
    public GameObject bingoBtn_InActive;

    public GameObject booster_1;
    public GameObject booster_2;
    public GameObject wildDaubBooster;
    public GameObject doubleScoreBooster;
    public GameObject pickABallBooster;
    public GameObject BonusTimeBooster;

    public GameObject PauseMenu;
    public GameObject Toast;

    public GameObject matchResult;

    [SerializeField] private GameObject objTop;
    [SerializeField] private GameObject objHead;
    [SerializeField] private GameObject objBottom;
    [SerializeField] private Transform posBonusTime;

    [SerializeField]
    private GameObject collectEffectObj;

    [Header("Tutorial")]
    public GameObject HandTopPrefab;
    public GameObject HandRightPrefab;
    public GameObject HandDownPrefab;
    public GameObject HandLeftPrefab;
    public GameObject disableScreen;

    [Header("first tutorial")]
    public GameObject LockForTheCalledNumberPrefab;
    public GameObject TabItToDaubItPrefab;

    [Header("second tutorial")]
    public GameObject TheFasterYouDaubPrefab;
    public GameObject OnceItsFullyChargePrefab;
    public GameObject BoosterListPrefab;
    public Transform TopOfBottomTransform;
    public GameObject TapItToUsePrefab;
    public GameObject PickABallThirdTransform;

    [Header("third tutorial")]
    public GameObject TapToBingoPrefab;

    [Header("end tutorial")]
    public GameObject endTutorialPopup;
    public GameObject tryAgainEndTutorialPopup;
    public GameObject playNowEndTutorialPopup;
    public GameObject skipBtn;
    public GameObject parentTut;

    private readonly ObscuredFloat MAX_TIME = 120f;

    [HideInInspector]
    public BingoScoreResult result;

    [HideInInspector]
    private BingoData dataGame = new BingoData();

    [HideInInspector]
    public ObscuredBool isBoosterWildDaubActive = false;
    [HideInInspector]
    public ObscuredBool isBoosterPickABallActive = false;
    [HideInInspector]
    public ObscuredBool isBoosterDoubleScoreActive = false;

    [HideInInspector]
    public bool IsBoosterActive
    {
        get
        {
            return isBoosterWildDaubActive
                || isBoosterPickABallActive
                || isBoosterDoubleScoreActive;
        }
    }

    [HideInInspector]
    public bool IsTutorial { get { return _startType == MiniGameStartTypeEnum.Tutorial; } }

    public bool IsWaitForWildDaubTutorial = false;

    [HideInInspector]
    public bool IsCanPlay = false;

    private void ShowHideBalloonLayout(bool isShow)
    {
        _lstBallonExisted
            .Where(x => x != null)
            .ToList()
            .ForEach(b =>
            {
                b.GetComponent<Animator>().enabled = isShow;

                b.transform.DOScale(isShow ? new Vector3(1, 1, 1) : new Vector3(0, 0, 0), 0.3f);

                //b.transform.localScale = isShow
                //? new Vector3(1, 1, 1)
                //: new Vector3(0, 0, 0);
            });
    }

    private void ShowUILayout()
    {
        objTop.transform.DOScale(Vector3.one, 0.3f);
        objHead.transform.DOScale(Vector3.one, 0.3f);
        objBottom.transform.DOScale(Vector3.one, 0.3f);
    }

    private void HideUILayout()
    {
        objTop.transform.DOScale(Vector3.zero, 0.3f);
        objHead.transform.DOScale(Vector3.zero, 0.3f);
        objBottom.transform.DOScale(Vector3.zero, 0.3f);
    }

    private void SetColorBackground(Color color)
    {
        gamePlayView.GetComponent<Image>().DOColor(color, 0.3f);
    }

    private void PauseOrResumeAnimationBalloonLayout(bool isResume)
    {
        _lstBallonExisted
            .Where(x => x != null)
            .ToList()
            .ForEach(b =>
            {
                b.GetComponent<Animator>().enabled = isResume;
            });
    }

    private bool _isCreatingNewBalloon = false;
    private bool _isPlayingCountDownTimeSound = false;

    #region Rx

    /// <summary>
    /// Timer Rx
    /// </summary>
    private ObscuredBool _isTimerRunning = false;
    private Subject<ObscuredFloat> _timerRx = new Subject<ObscuredFloat>();
    private IDisposable timerRx()
    {
        return _timerRx
            .Scan((pre, curr) =>
            {
                float nextTime = pre - curr;
                return nextTime <= 0 ? 0 : nextTime;
            })
            .Select(timer =>
            {
                if (timer <= 10)
                {
                    if (!_isPlayingCountDownTimeSound)
                    {
                        _isPlayingCountDownTimeSound = true;
                        //HCSound.Ins.PlayGameSound2(HCSound.Ins.MiniGame.TimeCountDown, 10f - timer);
                        SoundManager.instance.PlayUIEffect("se_clock01");
                    }

                    if (timeView && (int)timer > 0)
                    {
                        timeView.SetCountDownText(((int)timer).ToString());
                    }
                }

                if (timer > 10 && _isPlayingCountDownTimeSound)
                {
                    _isPlayingCountDownTimeSound = false;
                    //HCSound.Ins.StopGameSound2();
                }

                return timer == 0
                ? string.Empty
                : (TimeSpan.FromSeconds(timer)).ToString(@"mm\:ss");
            })
            .DistinctUntilChanged()
            .Subscribe(text =>
            {
                timeView.SetTimeTxt(string.IsNullOrEmpty(text) ? "00:00" : text);

                if (string.IsNullOrEmpty(text))
                {
                    GameFinished(GameFinishTypeEnum.TimesUp);
                }
                else
                {
                    if (dataGame.OScore == null) return;

                    var oScore = dataGame.OScore.Scores 
                        .Where(x =>
                        {
                            var xScore = x.Time > 600000 ? "-1" : (TimeSpan.FromSeconds(MAX_TIME - x.Time / 1000f)).ToString(@"mm\:ss");
                            return text == xScore;
                        })
                        .FirstOrDefault();

                    if (oScore != null) opponentUserInfo.Score = oScore.Score;
                }
            });
    }

    private IDisposable StartTimer()
    {
        return Observable
            .Interval(TimeSpan.FromSeconds(1))
            .Subscribe(x =>
            {
                if (_isTimerRunning)
                {
                    updateTimer(1);
                }
            });
    }

    private void pauseOrResumeTimer(bool isResume)
    {
        //if (_isPlayingCountDownTimeSound)
        //{
        //    if (isResume) HCSound.Ins.FxSoundGame2.UnPause();
        //    else HCSound.Ins.FxSoundGame2.Pause();
        //}

        _isTimerRunning = isResume;

        timeView.SetSecAni(_isTimerRunning);

    }

    public void updateTimer(float time)
    {
        _timerRx.OnNext(time);
    }

    /// <summary>
    /// Score RX
    /// </summary>
    private Subject<(BingoScoreTypeEnum, ObscuredInt)> _addScoreRx = new Subject<(BingoScoreTypeEnum, ObscuredInt)>();
    private IDisposable increaseScoreRx()
    {
        return _addScoreRx
            .Scan((pre, curr) =>
            {
                int nextScore = pre.Item2 + curr.Item2;

                var type = curr.Item1;
                switch (type)
                {
                    case BingoScoreTypeEnum.Init:
                        break;
                    case BingoScoreTypeEnum.Daubs:
                        result.Daubs.Add(curr.Item2);
                        break;
                    case BingoScoreTypeEnum.Bingos:
                        result.Bingos.Add(curr.Item2);
                        break;
                    case BingoScoreTypeEnum.MultiBingos:
                        result.MultiBingos.Add(curr.Item2);
                        break;
                    case BingoScoreTypeEnum.DoubleScores:
                        result.DoubleScores.Add(curr.Item2);
                        break;
                    case BingoScoreTypeEnum.Penalties:
                        if (pre.Item2 != 0)
                        {
                            if (nextScore < 0)
                            {
                                result.Penalties.Add(-pre.Item2);
                            }
                            else
                            {
                                result.Penalties.Add(curr.Item2);
                            }
                        }

                        break;
                }

                return nextScore < 0 ? (curr.Item1, 0) : (curr.Item1, nextScore);
            })
            .Subscribe(score =>
            {
                if (_startType == MiniGameStartTypeEnum.Tutorial) return;
                if (string.IsNullOrWhiteSpace(sKey)) return;

                var req = new SaveScoreGameRequest();
                req.key = sKey;
                req.time = _currentTime;
                req.score = score.Item2;
                req.scoreInfo = new SaveScoreGameRequest.ScoreInfo[5];
                req.scoreInfo[0] = new SaveScoreGameRequest.ScoreInfo()
                {
                    name = StringConst.BINGO_SCORE_INFO_DAUBS,
                    quantity = result.Daubs.Count,
                    score = result.DaubsSum()
                };

                req.scoreInfo[1] = new SaveScoreGameRequest.ScoreInfo()
                {
                    name = StringConst.BINGO_SCORE_INFO_BINGOS,
                    quantity = result.Bingos.Count,
                    score = result.BingosSum()
                };

                req.scoreInfo[2] = new SaveScoreGameRequest.ScoreInfo()
                {
                    name = StringConst.BINGO_SCORE_INFO_MULTI_BINGOS,
                    quantity = result.MultiBingos.Count,
                    score = result.MultiBingosSum()
                };

                req.scoreInfo[3] = new SaveScoreGameRequest.ScoreInfo()
                {
                    name = StringConst.BINGO_SCORE_INFO_DOUBLE_SCORES,
                    quantity = result.DoubleScores.Count,
                    score = result.DoubleScoresSum()
                };

                req.scoreInfo[4] = new SaveScoreGameRequest.ScoreInfo()
                {
                    name = StringConst.BINGO_SCORE_INFO_PENTALTIES,
                    quantity = result.Penalties.Count,
                    score = result.PenaltiesSum()
                };

                StartCoroutine(Current.Ins.gameAPI.SaveLiveScoreGame<SaveScoreGameResponse>(dataGame.gameId, req, (res) => { }));
            });
    }

    public void updateScore((BingoScoreTypeEnum, ObscuredInt) score)
    {
        _addScoreRx.OnNext(score);
    }

    public void ShowScoreEffect(int score, Vector3 pos)
    {
        myUserInfo.ShowPointTailEffect(score, pos);

        GameObject obj = ObjectPoolManager.instance.GetObject(ScoreTextObj.name, true, true);
        if (obj)
        {
            TextMeshProUGUI tMesh = obj.GetComponent<TextMeshProUGUI>();
            if (tMesh)
            {
                if (score >= 0)
                {
                    tMesh.text = $"+{Mathf.Abs(score)}";
                    tMesh.color = new Color(1.0f, 0.9424f, 0.0f, 1.0f);
                }
                else
                {
                    tMesh.text = $"-{Mathf.Abs(score)}";
                    tMesh.color = new Color(1.0f, 0.3454f, 0.0f, 1.0f);
                }

            }
            obj.transform.localScale = Vector3.one;
            obj.transform.position = pos;

            Sequence sq = DOTween.Sequence();
            sq.Join(obj.transform.DOScale(1.4f, 0.3f));
            sq.Append(obj.transform.DOScale(0.95f, 0.25f));
            sq.Append(obj.transform.DOScale(1.0f, 0.1f));
            sq.AppendInterval(0.5f);
            sq.Append(obj.transform.DOScale(1.1f, 0.1f));
            sq.Append(obj.transform.DOScale(0.0f, 0.25f));

            sq.Play().OnComplete(() =>
            {
                obj.SetActive(false);
            });

        }

    }


    /// <summary>
    /// Bingo Button RX
    /// </summary>
    private Subject<bool> _isBingoRx = new Subject<bool>();
    private IDisposable bingoRx()
    {
        return _isBingoRx
            .DistinctUntilChanged()
            .Subscribe(status =>
            {
                Sequence sq = DOTween.Sequence();

                if (status == true)
                {
                    //HCSound.Ins.PlayGameSound(HCSound.Ins.Bingo.ActiveBingo, true);
                    SoundManager.instance.PlayUIEffect("se_combo10_02");
                    bingoBtn_Active.SetActive(true);
                    bingoBtn_InActive.SetActive(false);
                    //sq.Join(bingoBtn_Active.transform.DOScale(Vector3.one, 0.3f))
                    //.Join(bingoBtn_InActive.transform.DOScale(Vector3.zero, 0.3f));
                }
                else
                {
                    //sq.Join(bingoBtn_Active.transform.DOScale(Vector3.zero, 0.3f))
                    //.Join(bingoBtn_InActive.transform.DOScale(Vector3.one, 0.3f));
                    bingoBtn_Active.SetActive(false);
                    bingoBtn_InActive.SetActive(true);
                }

                sq.Play();

                bingoBtn.GetComponent<Button>().interactable = status;
            });
    }

    public void updateBingoBtn(bool status)
    {
        _isBingoRx.OnNext(status);
    }

    private void InitRx()
    {
        DisposeAllRx();

        // Score
        bags.Add(increaseScoreRx());
        _addScoreRx.OnNext((BingoScoreTypeEnum.Init, 0));

        // BingoBtn
        bags.Add(bingoRx());

        // Timer
        bags.Add(timerRx());
        updateTimer(MAX_TIME);
        pauseOrResumeTimer(true);
        bags.Add(StartTimer());
    }
    #endregion

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();

        PlayerPrefsEx.Set(MiniGameEnum.Bingo.GetStringValue(), 1);
        SoundManager.instance.PlayMusic("bgm_findUser");

        bingoBtn_Active.SetActive(false);
        bingoBtn_InActive.SetActive(true);

        if (HomeTab.UIHomeTab.DefaultView != TabbarEnum.Event)
        {
            HomeTab.UIHomeTab.DefaultView = TabbarEnum.Home_GameMode_Bingo;
        }
        
        switch (UIBingo.sStartType)
        {
            case MiniGameStartTypeEnum.Init:
                Init(UIBingo.sGameMode, UIBingo.sGameId);
                break;
            case MiniGameStartTypeEnum.InitNextGame:
                InitNextGame(UIBingo.sRoomId);
                break;
            case MiniGameStartTypeEnum.InitReMatch:
                InitReMatch(UIBingo.sGameId);
                break;
            case MiniGameStartTypeEnum.Tutorial:
                InitTutorialGame();
                break;
        }

        bingoBtn.GetComponent<Button>().onClick.AddListener(OnClickBingoBtn);

        ObjectPoolManager.instance.ReservePool(collectEffectObj, 10, true);
        ObjectPoolManager.instance.ReservePool(ScoreTextObj, 10, true);

        var screenWidth = gameObject.GetComponent<RectTransform>().rect.width;
        var ratio = screenWidth / 1080f;
        if (ratio < 1f) boardLayout.transform.localScale = new Vector3(ratio, ratio, ratio);
        if (ratio > 1f)
        {
            ratio--;
            ratio = 1f - ratio * 2f;

            boardLayout.transform.localScale = new Vector3(ratio, ratio, ratio);
            boardLayout.transform.localPosition = new Vector3(boardLayout.transform.localPosition.x, 0f, boardLayout.transform.localPosition.z);
        }
    }

    protected override void Update()
    {
        if (_isTimerRunning && !_isCreatingNewBalloon)
        {
            BalloonTimeDistance = BalloonTimeDistance - Time.deltaTime;
            if (BalloonTimeDistance < 0)
            {
                _isCreatingNewBalloon = true;

                // Create new balloon
                increaseBalloon();
            }
        }

        if (_isTimerRunning)
        {
            _currentTime += Time.deltaTime * 1000f;
            if (_currentTime >= MAX_TIME * 1000f) _currentTime = MAX_TIME * 1000f;
        }

        base.Update();
    }

    public void InitNextGameKnockOut(string roomId)
    {
        InitNextGame(roomId);
    }

    protected override void OnReloadCryptoKey()
    {
        MAX_TIME.RandomizeCryptoKey();
        isBoosterWildDaubActive.RandomizeCryptoKey();
        isBoosterPickABallActive.RandomizeCryptoKey();
        isBoosterDoubleScoreActive.RandomizeCryptoKey();
        _isTimerRunning.RandomizeCryptoKey();
        _currentBalloonPos.RandomizeCryptoKey();
        currentBalloonValue.RandomizeCryptoKey();
        BalloonTimeDistance.RandomizeCryptoKey();
    }

    protected override void LoadMatchingView()
    {
        base.LoadMatchingView();

        var req = new CreateRoomByMiniGameRequest();
        req.GameId = _gameId;

        UserResponse uSocketRes = null;
        BingoDataResponse dtRes = null;

        GetUserSecret(() =>
        {
            StartCoroutine(Current.Ins.gameAPI.CreateRoomByMiniGame<BingoDataResponse>(req, (res) =>
            {
                if (res == null || res.errorCode != 0)
                {
                    OnCallBackAPIError(res);
                    return;
                }

                dtRes = res;
                dtRes.dataDecode = JsonUtility.FromJson<BingoDataResponse.DataDecode>(EncryptHelper.DecryptWithAES256(res.data.info, sKey));
                _matchData.Response = res;
                _matchData.GameId = res.dataDecode.gameId;
                _matchData.RoomId = res.dataDecode.roomId;
                _matchData.IsSync = res.dataDecode.isSync;

                _matchData.UpdateIsSyncAction = (isSync) => res.dataDecode.isSync = isSync;
                _matchData.UpdateOpponentScoreAction = (oScore) => dataGame.OScore = oScore;
                _matchData.User = res.dataDecode.users.FirstOrDefault();
                if (uSocketRes != null)
                {
                    _matchData.User = new UserResponse();
                    _matchData.User.id = uSocketRes.id;
                    _matchData.User.name = uSocketRes.name;
                    _matchData.User.avatar = uSocketRes.avatar;
                }

                Matching();
                if (_matchData.User == null) _userRx.OnNext(null);
            }));
        });

        Current.Ins.socketAPI.AddSocket<MatchUserSocketResponse>
        (
            StringConst.SOCKET_MATCH_USER_EVENT,
            $"{StringConst.SOCKET_MATCH_USER_EVENT}{Current.Ins.player.Id}",
            (opponentRes) =>
            {
                if (_matchData.GameId != opponentRes.gameId) return;

                if (dtRes == null)
                {
                    uSocketRes = new UserResponse();
                    uSocketRes.id = opponentRes.user.id;
                    uSocketRes.name = opponentRes.user.name;
                    uSocketRes.avatar = opponentRes.user.avatar;
                    uSocketRes.avatarFrame = opponentRes.user.avatarFrame;
                }
                else
                {
                    dtRes.dataDecode.users = new List<UserResponse>()
                    {
                        new UserResponse()
                        {
                            id = opponentRes.user.id,
                            name = opponentRes.user.name,
                            avatar = opponentRes.user.avatar
                        }
                    };
                }

                UpdateOpponentInfo(opponentRes.user.name, opponentRes.user.avatar, opponentRes.user.avatarFrame);
                if (_isInGame) UpdateOpponentInfoIngame(opponentRes);

                _userRx.OnNext(opponentRes.user);
            }
        );
    }

    protected override void OnCallBackAPIError(IResponse response)
    {
        BingoDataResponse res = response as BingoDataResponse;
        if(res!= null && res.errorCode == 4)
        {
            popupSpecialNotice.SetActive(true);
        }
        else
        {
            base.OnCallBackAPIError(response);
        }
    }

    protected override void UpdateOpponentInfoIngame(MatchUserSocketResponse opp)
    {
        AddJob(() =>
        {
            dataGame.isSync = true;
            dataGame.Opponent = new();
            dataGame.Opponent.Name = opp.user.name;
            dataGame.Opponent.Avt = opp.user.avatar;
            dataGame.Opponent.AvtFrame = opp.user.avatarFrame;

            opponentUserInfo.gameObject.SetActive(true);
            opponentUserInfo.UserName.text = dataGame.Opponent.Name;

            Current.Ins.ImageData.AvatarByUrl(dataGame.Opponent.Avt, (result) =>
            {
                opponentUserInfo.UserAvt.texture = result.Texture;
            });
            //Current.Ins.ImageData.AvatarFrameByUrl(dataGame.Opponent.AvtFrame, (result) =>
            //{
            //    opponentUserInfo.UserAvtFrame.texture = result.Texture;
            //});
            // 
        });
    }

    protected override void LoadGameContent(IResponse response)
    {
        result = new BingoScoreResult();

        base.LoadGameContent(response);

        var req = new CreateRoomByMiniGameRequest();

        if (response != null)
        {
            OnCallBackFromAPI(response);
            return;
        }

        GetUserSecret(() =>
        {
            switch (_startType)
            {
                case MiniGameStartTypeEnum.Init:
                    req.GameId = _gameId;
                    StartCoroutine(Current.Ins.gameAPI.CreateRoomByMiniGame<BingoDataResponse>(req, OnCallBackFromAPI));

                    break;
                case MiniGameStartTypeEnum.InitNextGame:
                    req.RoomId = _roomId;
                    StartCoroutine(Current.Ins.gameAPI.GetNextGame<BingoDataResponse>(req, OnCallBackFromAPI));

                    break;
                case MiniGameStartTypeEnum.InitReMatch:
                    req.GameId = _gameId;
                    StartCoroutine(Current.Ins.gameAPI.GetGameScript<BingoDataResponse>(req, OnCallBackFromAPI));
                    break;
            }
        });
    }

    public void OnClickMenuBtn()
    {
        if (_startType == MiniGameStartTypeEnum.Tutorial) return;

        PauseMenu.SetActive(true);
        var pMenu = PauseMenu.GetComponent<PauseMenu>();

        Action callBackEndNow = () =>
        {
            GameFinished(GameFinishTypeEnum.GameOver);
        };

        pMenu.ShowPauseMenu(callBackEndNow);
        pMenu.OnClickQuitGametn();
    }

    protected async override Task<bool> ParseFromAPI(IResponse responseData)
    {
        var res = (BingoDataResponse)responseData;

        if (res == null || res.errorCode != 0)
        {
            return false;
        }

        res.dataDecode = JsonUtility.FromJson<BingoDataResponse.DataDecode>(EncryptHelper.DecryptWithAES256(res.data.info, sKey));
        var gameInfo = res.dataDecode.gameInfo;
        dataGame.gameId = res.dataDecode.gameId.ToString();
        dataGame.roomId = res.dataDecode.roomId.ToString();

        // Booster
        dataGame.boosters = new Stack<BingoBoosterTypeEnum>();
        gameInfo.booster.ForEach(b => dataGame.boosters.Push((BingoBoosterTypeEnum)b));

        // Balloons
        dataGame.ballons = gameInfo.bingo.ToArray();

        // Board
        dataGame.board = ArrayHelper.Make2DArray<int>(gameInfo.numbers.ToArray(), 5, 5);

        // Game Mode
        var gameMode = res.dataDecode.mode;
        dataGame.miniGameMode = gameMode;

        if (gameMode == MiniGameModeEnum.HeadToHead)
        {
            dataGame.isSync = res.dataDecode.isSync;
        }

        // totalPlayer
        dataGame.totalPlayer = res.dataDecode.quantityUser;

        // User and Opponent
        dataGame.User = new BingoData.Player();
        dataGame.User.Name = Current.Ins.player.Name;
        dataGame.User.Avt = Current.Ins.player.Avatar;
        dataGame.User.AvtFrame = Current.Ins.player.AvatarFrame;

        myUserInfo.UserName.text = dataGame.User.Name;
        //getAvt
        Current.Ins.ImageData.AvatarByUrl(dataGame.User.Avt, (result) =>
        {
            myUserInfo.UserAvt.texture = result.Texture;
        });
        //Current.Ins.ImageData.AvatarFrameByUrl(dataGame.User.AvtFrame, (result) =>
        //{
        //    myUserInfo.UserAvtFrame.texture = result.Texture;
        //});

        UserResponse opponentInfo = null;
        switch (gameMode)
        {
            case MiniGameModeEnum.HeadToHead:
                opponentInfo = res.dataDecode.users.FirstOrDefault();

                if (opponentInfo != null)
                {
                    dataGame.Opponent = new BingoData.Player();
                    dataGame.Opponent.Name = opponentInfo.name;
                    dataGame.Opponent.Avt = opponentInfo.avatar;
                    dataGame.Opponent.AvtFrame = opponentInfo.avatarFrame;

                    opponentUserInfo.gameObject.SetActive(true);
                    opponentUserInfo.UserName.text = dataGame.Opponent.Name;
                    
                    Current.Ins.ImageData.AvatarByUrl(dataGame.Opponent.Avt, (result) =>
                    {
                        opponentUserInfo.UserAvt.texture = result.Texture;
                    });
                    //Current.Ins.ImageData.AvatarFrameByUrl(dataGame.Opponent.AvtFrame, (result) =>
                    //{
                    //    opponentUserInfo.UserAvtFrame.texture = result.Texture;
                    //});
                }
                else
                {
                    opponentUserInfo.gameObject.SetActive(false);
                }

                Current.Ins.socketAPI.AddSocket<LiveScoreSocketResponse>
                (
                    StringConst.SOCKET_LIVE_SCORE_EVENT,
                    $"{StringConst.SOCKET_LIVE_SCORE_EVENT}{dataGame.gameId}",
                    onSocketCallBackScore
                );

                break;

            case MiniGameModeEnum.OneToMany:
                opponentUserInfo.gameObject.SetActive(false);
                //oppnentView.SetActive(false);
                break;

            case MiniGameModeEnum.KnockOut:
                opponentInfo = res.dataDecode.users.FirstOrDefault();

                if (!dataGame.isSync)
                {
                    opponentUserInfo.gameObject.SetActive(false);
                    //oppnentView.SetActive(false);
                }

                if (opponentInfo != null)
                {
                    dataGame.Opponent = new BingoData.Player();
                    dataGame.Opponent.Name = opponentInfo.name;
                    dataGame.Opponent.Avt = opponentInfo.avatar;
                    dataGame.Opponent.AvtFrame = opponentInfo.avatarFrame;
                    opponentUserInfo.UserName.text = dataGame.Opponent.Name;
                    //getAvt
                    Current.Ins.ImageData.AvatarByUrl(dataGame.Opponent.Avt, (result) =>
                    {
                        opponentUserInfo.UserAvt.texture = result.Texture;
                    });
                    //Current.Ins.ImageData.AvatarFrameByUrl(dataGame.Opponent.AvtFrame, (result) =>
                    //{
                    //    opponentUserInfo.UserAvtFrame.texture = result.Texture;
                    //});
                }

                break;

            case MiniGameModeEnum.RoundRobin:
                opponentUserInfo.gameObject.SetActive(false);
                //oppnentView.SetActive(false);
                break;
        }

        Current.Ins.socketAPI.AddSocket<ExitGameSocketResponse>
            (
                StringConst.SOCKET_EXIT_GAME_EVENT,
                $"{StringConst.SOCKET_EXIT_GAME_EVENT}{dataGame.gameId}",
                (exitGameRes) =>
                {
                    dataGame.isOppoentInGame = false;
                });

        return await Task.Run(() => true);
    }

    private void onSocketCallBackScore(LiveScoreSocketResponse uLiveScore)
    {
        if (uLiveScore.userId.ToString() == Current.Ins.player.Id) return;

        var oTime = uLiveScore.time;

        if (_currentTime - oTime < -2000f)
        {
            DataUIBase.OpponentScore.ScoreData add = new()
            {
                Time = uLiveScore.time,
                Score = uLiveScore.score
            };

            if (dataGame.OScore == null) dataGame.OScore = new();
            dataGame.OScore.Scores.Add(add);
        }
        else
        {
            AddJob(() => opponentUserInfo.Score = uLiveScore.score);
        }
    }

    protected override void LoadGamePlay()
    {
        //HCSound.Ins.PlayBackground(HCSound.Ins.Bingo.Background);

        SoundManager.instance.PlayMusic("bgm_bingo");

        base.LoadGamePlay();

        BingoBoard.Ins.Init(dataGame, onDone: () =>
        {
            if (_IsGameFinished) return;

            result.StartTimeMilliSecond = TimeManager.CurrentTimeInMilliSecond;

            IsCanPlay = true;

            getNextBooster();

            InitBalloon();

            InitRx();

            if (_startType == MiniGameStartTypeEnum.Tutorial)
            {
                StartCoroutine(LoadGameplayTutorial());
            }
        });
    }

    protected override void OnResumeGame(double pauseTime)
    {
        if (!_isTimerRunning) return;
        if (isBoosterPickABallActive || isBoosterWildDaubActive) return;
        if (_lstBallonExisted == null) return;

        float pTime = (float)pauseTime / 1000f;

        // Time
        BalloonTimeDistance -= pTime;

        // Sound
        _isPlayingCountDownTimeSound = false;

        updateTimer(pTime);
    }

    private void OnClickBingoBtn()
    {

        if (IsCanPlay == false)
        {
            SoundManager.instance.PlayUIEffect("se_cancel");
            return;
        }

        var lstBingo = BingoBoard.Ins.GetBingoLines();
        if (lstBingo.Count > 0)
        {
            //HCSound.Ins.PlayGameSound(HCSound.Ins.Bingo.Bingo, true);
            SoundManager.instance.PlayUIEffect("se_combo10_02");
            //for (int i = 0; i < lstBingo.Count; i++)
            //{

            //    var itemRowList = lstBingo[i];
            //    int length = itemRowList.items.Length;
            //    for (int j = 0; j < length; j++)
            //    {
            //        updateScore((BingoScoreTypeEnum.Bingos, 1000 / length), itemRowList.items[j].transform.position);
            //    }

                
            //}

            Action doneAction = null;
            if (BingoBoard.Ins.GetUnSelectedNumber().Count == 0)
            {
                PauseOrResumeAnimationBalloonLayout(false);
                pauseOrResumeTimer(false);
                //DisposeAllRx();

                booster_1.GetComponent<BoosterControl>().RemoveActionClick();
                booster_2.GetComponent<BoosterControl>().RemoveActionClick();

                doneAction = () => GameFinished(GameFinishTypeEnum.Success);
            }

            // Multi
            var lstMulti = lstBingo
                //.Where(x => x.items.Count(x => x.TypeActive == BingoActiveStarByTypeEnum.WildDaub || x.TypeActive == BingoActiveStarByTypeEnum.PickABall) < 4)
                .ToList();

            if (lstMulti.Count > 1)
            {
                var score = (BingoScoreTypeEnum.MultiBingos, 1000);
                updateScore(score);
                ShowScoreEffect(score.Item2, bingoBtn.transform.position);
            }

            // Change to star
            _isWaitAni = true;
            if (doneAction == null)
            {
                doneAction = () =>
                {
                    _isWaitAni = false;
                    if (_isWaitTimesUp) GameFinished(GameFinishTypeEnum.TimesUp);
                };
            }

            BingoBoard.Ins.ChangeBingoLineToStar(lstBingo, 1000, doneAction);
        }

        updateBingoBtn(false);
    }

    public void ShowCollectEffect(Vector3 pos)
    {
        GameObject eff = ObjectPoolManager.instance.GetObject(collectEffectObj.name);
        if (eff)
        {
            eff.transform.position = pos;
        }
    }

    public void Penalty(GameObject item)
    {
        //HCSound.Ins.PlayGameSound(HCSound.Ins.Bingo.Wrong, true);
        SoundManager.instance.PlayUIEffect("se_cancel");
        var score = (BingoScoreTypeEnum.Penalties, -25);
        updateScore(score);
        ShowScoreEffect(score.Item2, item.transform.position);
        //var pntScore = Instantiate(minusScorePrefab, transform, false);
        //pntScore.transform.localScale = Vector3.zero;
        //pntScore.transform.position = item.transform.position;

        //pntScore.transform.DOScale(Vector3.one, 0.3f)
        //    .OnComplete(() =>
        //    {
        //        pntScore.transform.DOMove(userScore.transform.position, 0.5f)
        //        .OnComplete(() =>
        //        {
        //            pntScore.transform.DOScale(Vector3.zero, 0.3f).OnComplete(() =>
        //            {
        //                updateScore((BingoScoreTypeEnum.Penalties, -25));
        //                Destroy(pntScore);
        //            });
        //        });
        //    });
    }

    public void CompleteAll()
    {
        if (BingoBoard.Ins.GetBingoLines().Count > 0)
        {
            bingoBtn.GetComponent<Button>().onClick.Invoke();
            return;
        }

        DisposeAllRx();
        StartCoroutine(SetFinishCoroutine());
    }

    private IEnumerator SetFinishCoroutine()
    {
        yield return new WaitForSeconds(1f);
        GameFinished(GameFinishTypeEnum.Success);
    }

    private bool _isWaitTimesUp = false;
    private bool _isWaitAni = false;
    public void GameFinished(GameFinishTypeEnum finishType)
    {
        _IsGameFinished = true;

        IsCanPlay = false;
        pauseOrResumeTimer(false);

        if (finishType == GameFinishTypeEnum.TimesUp)
        {
            if (_isWaitAni)
            {
                _isWaitTimesUp = true;
                return;
            }
        }

        if (opponentUserInfo.gameObject.activeSelf)
        {
            int? lastScore = dataGame.OScore.Scores.LastOrDefault()?.Score;
            if (lastScore != null)
            {
                opponentUserInfo.Score = lastScore.Value;
            }
        }

        DisposeAllRx();
        PauseMenu.SetActive(false);

        SoundManager.instance.PlayMusic("bgm_result");
        SoundManager.instance.PlayUIEffect("se_useClock");

        result.EndTimeMilliSecond = result.StartTimeMilliSecond + MAX_TIME;

        if (result.StartTimeMilliSecond == 0)
        {
            result.StartTimeMilliSecond = result.EndTimeMilliSecond;
        }

        Action<SaveScoreGameResponse> afterSubmitCallBack = (res) =>
        {
            this.resultView.transform.DOScale(Vector3.one, 0.1f).SetEase(Ease.InOutBack);
            this.resultView.GetComponent<UIResult>()
                    .submitBtn.GetComponent<Button>()
                        .onClickWithHCSound(() =>
                        {
                            this.resultView.transform.DOScale(Vector3.zero, 0.1f).OnComplete(() => this.matchResult.SetActive(true));
                        });
            switch (dataGame.miniGameMode)
            {
                case MiniGameModeEnum.HeadToHead:
                    var headToHeadData = new MatchHeadToHeadData();
                    headToHeadData.gameId = dataGame.gameId;
                    headToHeadData.isSync = dataGame.isSync;
                    headToHeadData.isOpponentInGame = dataGame.isOppoentInGame;
                    headToHeadData.roomId = dataGame.roomId;

                    if (dataGame.Opponent != null)
                    {
                        headToHeadData.Opponent = new MatchHeadToHeadData.Player();
                        headToHeadData.Opponent.Name = dataGame.Opponent.Name;
                        headToHeadData.Opponent.Avt = dataGame.Opponent.Avt;
                        headToHeadData.Opponent.AvtFrame = dataGame.Opponent.AvtFrame;
                    }

                    matchResult.GetComponent<UIMatchResult>().ShowHeadToHead(res, headToHeadData, sFeeData,
                        acceptReMatchCallBack: (gameId) =>
                        {
                            InitReMatchStaticVariable(gameId);
                            SceneHelper.ReloadScene();
                        },
                        newMatchCallBack: () =>
                        {
                            InitStaticVariable(MiniGameModeEnum.HeadToHead, _gameId, sFeeData);
                            SceneHelper.ReloadScene();
                        });

                    break;

                case MiniGameModeEnum.OneToMany:
                    var oneToManyData = new MatchOneToManyData();
                    oneToManyData.totalPlayer = dataGame.totalPlayer;
                    oneToManyData.gameId = dataGame.gameId;
                    oneToManyData.roomId = dataGame.roomId;

                    LoadOneToManyResult(res.dataDecode.status, dataGame.roomId, matchResult, oneToManyData, res);

                    break;

                case MiniGameModeEnum.KnockOut:
                    var knockOutdata = new KnockOutData();
                    knockOutdata.roomId = dataGame.roomId;
                    knockOutdata.gameId = dataGame.gameId;

                    if (dataGame.Opponent != null)
                    {
                        knockOutdata.Opponent = new KnockOutData.Player();
                        knockOutdata.Opponent.Name = dataGame.Opponent.Name;
                        knockOutdata.Opponent.Avt = dataGame.Opponent.Avt;
                        knockOutdata.Opponent.AvtFrame = dataGame.Opponent.AvtFrame;
                        knockOutdata.Opponent.Name = dataGame.Opponent.Name;
                    }

                    matchResult.GetComponent<UIMatchResult>().ShowKnockOut(res, knockOutdata, () =>
                    {
                        InitNextGameStaticVariable(dataGame.roomId);
                        SceneHelper.ReloadScene();
                    });

                    break;
            }
        };
        LoadResult();
        resultView
            .GetComponent<UIResult>()
            .Init(result, finishType, dataGame.gameId, sKey, afterSubmitCallBack);
    }

    #region Balloon

    /// <summary>
    /// Balloon
    /// </summary>
    private List<GameObject> _lstBallonExisted;
    private ObscuredInt _currentBalloonPos = 0;
    [HideInInspector]
    public ObscuredInt currentBalloonValue = 0;
    [HideInInspector]
    public ObscuredFloat BalloonTimeDistance = 3f;

    private void InitBalloon()
    {
        _lstBallonExisted = new List<GameObject>();
        ballonLayout.transform.RemoveAllChild();
        increaseBalloon();
    }

    private void increaseBalloon()
    {
        // Destroy
        if (_lstBallonExisted.Count == 6) _lstBallonExisted.RemoveAt(5);

        // Move next
        if (_lstBallonExisted.Count > 0)
        {
            for (int pos = 0; pos < _lstBallonExisted.Count; pos++)
            {
                var item = _lstBallonExisted[pos].GetComponent<Animator>();

                switch (pos)
                {
                    case 0:
                        item.SetTrigger(BallonAnimationStateEnum.State1.GetStringValue());
                        break;
                    case 1:
                        item.SetTrigger(BallonAnimationStateEnum.State2.GetStringValue());
                        break;
                    case 2:
                        item.SetTrigger(BallonAnimationStateEnum.State3.GetStringValue());
                        break;
                    case 3:
                        item.SetTrigger(BallonAnimationStateEnum.State4.GetStringValue());
                        break;
                    case 4:
                        item.SetTrigger(BallonAnimationStateEnum.Destroy.GetStringValue());
                        break;
                    default: break;
                }
            }
        }

        // Add new
        if (_currentBalloonPos > dataGame.ballons.Count() - 1)
        {
            return;
        }

        AddNewBalloon();
    }

    private GameObject AddNewBalloon()
    {
        SoundManager.instance.PlayUIEffect($"se_bubblePop{UnityEngine.Random.Range(1, 6):00}");

        var ballon = Instantiate(ballonPrefab);
        ballon.transform.localScale = 0.9f * Vector3.one;
        var balloonIns = ballon.GetComponent<ControlBalloonPrefab>();
        balloonIns.OnStartCountDown = () =>
        {
            BalloonTimeDistance = 3f;
            _isCreatingNewBalloon = false;
        };

        var lstSelected = BingoBoard.Ins.GetSelectedNumber();

        currentBalloonValue = dataGame.ballons[_currentBalloonPos++];

        while (lstSelected.Contains(currentBalloonValue.ToString()))
        {
            currentBalloonValue = dataGame.ballons[_currentBalloonPos++];
        }

        balloonIns.Init(currentBalloonValue);
        _lstBallonExisted.Insert(0, ballon);

        ballon.transform.SetParent(ballonLayout.transform, false);

        return ballon;
    }

    #endregion

    #region Booster

    /// <summary>
    /// Booster
    /// </summary>
    public void getNextBooster(BoosterControl booster = null)
    {
        if (dataGame.boosters.Count == 0)
        {
            return;
        }

        Action<BingoBoosterTypeEnum> action = (boosterType) =>
        {
            //if (IsBoosterActive) return;

            switch (boosterType)
            {
                case BingoBoosterTypeEnum.WildDaub:
                    //HCSound.Ins.PlayGameSound(HCSound.Ins.Bingo.WildDaub, true);
                    SoundManager.instance.PlayUIEffect("se_combo10_02");
                    StartWildDaub();
                    break;

                case BingoBoosterTypeEnum.PickABall:
                    //HCSound.Ins.PlayGameSound(HCSound.Ins.Bingo.PickABall, true);
                    SoundManager.instance.PlayUIEffect("se_combo10_02");
                    StartPickABall();
                    break;

                case BingoBoosterTypeEnum.DoubleScore:
                    //HCSound.Ins.PlayGameSound(HCSound.Ins.Bingo.DoubleScore, true);
                    SoundManager.instance.PlayUIEffect("se_combo10_02");
                    StartDoubleScore();
                    break;

                case BingoBoosterTypeEnum.BonusTime:
                    //HCSound.Ins.PlayGameSound(HCSound.Ins.Bingo.BonusTime, true);
                    SoundManager.instance.PlayUIEffect("se_combo10_02");
                    SoundManager.instance.PlayUIEffect("se_clock01");
                    BonusTimeBooster.transform.localScale = new Vector3(0, 0, 0);
                    BonusTimeBooster.transform.DOScale(new Vector3(1, 1, 1), 0.3f)
                    .OnComplete(() =>
                    {
                        StopCoroutine(nameof(HideBonusTimeBooster));
                        StartCoroutine(nameof(HideBonusTimeBooster));
                    });
                    updateTimer(-10);
                    break;
            }
        };

        var boosterVl1 = dataGame.boosters.Pop();

        if (booster == null)
        {
            var bs1 = booster_1.GetComponent<BoosterControl>();
            bs1.SetPendingBooster(boosterVl1, () => action(boosterVl1));

            if (dataGame.boosters.Count == 0)
            {
                return;
            }

            var bs2 = booster_2.GetComponent<BoosterControl>();
            var boosterVl2 = dataGame.boosters.Pop();
            bs2.SetPendingBooster(boosterVl2, () => action(boosterVl2));

            return;
        }

        booster.SetPendingBooster(boosterVl1, () => action(boosterVl1));
    }

    public void boosterIncrease()
    {
        var bs1 = booster_1.GetComponent<BoosterControl>();
        var bs2 = booster_2.GetComponent<BoosterControl>();

        if (bs1.IsMax() && bs2.IsMax()) return;

        if (bs1.IsMax())
        {
            bs2.InCreaseProgress();
        }
        else if (bs2.IsMax())
        {
            bs1.InCreaseProgress();
        }
        else if (bs1.getInitTime() > bs2.getInitTime())
        {
            bs2.InCreaseProgress();
        }
        else
        {
            bs1.InCreaseProgress();
        }
    }

    /// <summary>
    /// WildDaub
    /// </summary>
    public void StartWildDaub()
    {
        pauseOrResumeTimer(false);

        // Hide balloon and pause animation
        ShowHideBalloonLayout(false);

        // Active WildDaub
        wildDaubBooster
            .GetComponent<WildDaubBooster>()
            .StartBooster(() => onStopWildDaub(), _startType == MiniGameStartTypeEnum.Tutorial);

        HideUILayout();
        SetColorBackground(ColorsConst.BACKGROUND_FOCUS_COLOR);

        isBoosterWildDaubActive = true;
    }

    public void onStopWildDaub()
    {
        pauseOrResumeTimer(true);

        // Show balloon and continue animation
        ShowHideBalloonLayout(true);

        wildDaubBooster
            .GetComponent<WildDaubBooster>()
            .StopBooster();

        ShowUILayout();
        SetColorBackground(Color.white);

        isBoosterWildDaubActive = false;

        if (_startType == MiniGameStartTypeEnum.Tutorial)
        {
            StartCoroutine(AfterWildDaub());
        }
    }

    /// <summary>
    /// DoubleScore
    /// </summary>
    public void StartDoubleScore()
    {
        if (isBoosterDoubleScoreActive)
        {
            doubleScoreBooster.GetComponent<DoubleScoreBooster>().IncreaseTimer();
        }
        else
        {
            // Active WildDaub
            doubleScoreBooster.GetComponent<DoubleScoreBooster>().StartBooster(
                onStopCallBack: () =>
                {
                    doubleScoreBooster.GetComponent<DoubleScoreBooster>().StopBooster();
                    isBoosterDoubleScoreActive = false;
                });

            isBoosterDoubleScoreActive = true;
        }
    }

    /// <summary>
    /// PickABall
    /// </summary>
    public void StartPickABall()
    {
        pauseOrResumeTimer(false);
        ShowHideBalloonLayout(false);

        List<ObscuredInt> lstNumber = new List<ObscuredInt>();

        var lstRow = BingoBoard.Ins.bingoRows.ToList();
        foreach (var row in lstRow)
        {
            var items = row.items.ToList();

            foreach (var item in items)
            {
                if (item.isOpenOrStar()) continue;

                lstNumber.Add(item.Number);
            }
        }
 
        var lstItemRnd = lstNumber
            .OrderBy(x => (new System.Random()).Next())
            .Take(4)
            .Select(number =>
            {
                var ball = Instantiate(ballonPrefab);
                ball.transform.localScale = Vector3.zero;
                var ballCtrl = ball.GetComponent<ControlBalloonPrefab>();
                ballCtrl.Init(number);
                return ball;
            })
            .ToList();

        if (_startType == MiniGameStartTypeEnum.Tutorial)
        {
            var number10 = BingoBoard.Ins.bingoRows[1].items[0].GetComponent<BingoItem>().Number;
            var ball = Instantiate(ballonPrefab);
            ball.transform.localScale = Vector3.zero;
            var ballCtrl = ball.GetComponent<ControlBalloonPrefab>();
            ballCtrl.Init(number10);
            lstItemRnd[2] = ball;
        }

        Action<GameObject> onSelectedBall = (ball) =>
        {
            BingoBoard.Ins.OpenByPickABallBooster(ball, OnStopPickABall);
        };

        pickABallBooster
            .GetComponent<PickABallBooster>()
            .ShowPickABall(lstItemRnd, onSelectedBall, OnStopPickABall, _startType == MiniGameStartTypeEnum.Tutorial);

        isBoosterPickABallActive = true;
    }

    private IEnumerator HideBonusTimeBooster()
    {
        yield return new WaitForSeconds(2f);
        var posDefault = BonusTimeBooster.transform.localPosition;
        
        Sequence tween = DOTween.Sequence();
        tween.Join(BonusTimeBooster.transform.DOMove(posBonusTime.position, 0.3f))
             .Join(BonusTimeBooster.transform.DOScale(new Vector3(0, 0, 0), 0.3f))
             .OnComplete(() => BonusTimeBooster.transform.localPosition = posDefault);

        tween.Play();
    }

    public void OnStopPickABall()
    {
        ShowHideBalloonLayout(true);
        pauseOrResumeTimer(true);

        isBoosterPickABallActive = false;
    }

    #endregion

    #region Tutorial
    protected override void LoadTutorialGameContent()
    {
        result = new BingoScoreResult();
        base.LoadTutorialGameContent();
        MakeTutorialData();
        AfterMakeTutorialData();
    }

    private void MakeTutorialData()
    {
        dataGame = new BingoData();

        myUserInfo.UserName.text = Current.Ins.player.Name;
        //getAvt
        Current.Ins.UserAvatar((result) =>
        {
            myUserInfo.UserAvt.texture = result.Texture;
        });

        //Current.Ins.UserAvatarFrame((result) =>
        //{
        //    myUserInfo.UserAvtFrame.texture = result.Texture;
        //});

        opponentUserInfo.gameObject.SetActive(false);       

        // Booster
        dataGame.boosters = new Stack<BingoBoosterTypeEnum>();
        for (int i = 0; i < 12; i++)
        {
            if (i == 10)
            {
                dataGame.boosters.Push(BingoBoosterTypeEnum.WildDaub);
                continue;
            }

            if (i == 11)
            {
                dataGame.boosters.Push(BingoBoosterTypeEnum.PickABall);
                continue;
            }

            dataGame.boosters.Push((BingoBoosterTypeEnum)UnityEngine.Random.Range(1, 4));
        }
 
        // balloon
        dataGame.ballons = new int[24];
        int posBalloon = 0;

        // board
        dataGame.board = new int[5, 5];
        var lstData = new List<List<int>>
        {
            new List<int>{ 1, 20, 33, 47, 61 },
            new List<int>{ 2, 16, 34, 48, 63 },
            new List<int>{ 3, 17, 35, 49, 64 },
            new List<int>{ 4, 18, 36, 50, 65 },
            new List<int>{ 5, 19, 37, 51, 66 },
        };

        for (int col = 0; col <= 4; col++)
        {
            for (int row = 0; row <= 4; row++)
            {
                int value = lstData[row][col];
                dataGame.board[row, col] = value;

                if (row != 2 || col != 2)
                {
                    dataGame.ballons[posBalloon++] = value;
                }
            }
        }

        dataGame.ballons[0] = dataGame.board[2, 0];
        dataGame.ballons[1] = 70;
    }

    private IEnumerator LoadGameplayTutorial()
    {
        disableScreen.SetActive(true);
        GameObject a = Instantiate(skipBtn, parentTut.transform);
        a.transform.position = opponentUserInfo.transform.position;
        //a.transform.position = oppnentView.transform.position;
        a.transform.localScale = Vector3.zero;
        a.GetComponent<Button>().onClick
            .AddListener(() =>
            {
                endTutorialPopup.SetActive(true);
                if (parentTut.transform.childCount < 2) return;
                for (int child = 1; child < parentTut.transform.childCount; child++)
                {
                    parentTut.transform.GetChild(child).gameObject.SetActive(false);
                }               
            });


        yield return new WaitForSeconds(2f);

        a.transform.DOScale(Vector3.one, 0.5f).SetEase(Ease.OutElastic);

        pauseOrResumeTimer(false);
        PauseOrResumeAnimationBalloonLayout(false);

        BingoBoard.Ins.bingoRows[0].items[0].active();
        yield return new WaitForSeconds(0.2f);

        BingoBoard.Ins.bingoRows[4].items[0].active();
        yield return new WaitForSeconds(0.2f);

        // handtop
        var item20Pos = BingoBoard.Ins.bingoRows[2].items[0].transform.position;
        var hTop = Instantiate(HandTopPrefab, parentTut.transform, false);
        hTop.transform.localScale = Vector3.zero;
        hTop.transform.position = item20Pos;
        hTop.transform.DOScale(Vector3.one, 0.5f);

        // Tab it to Daub it
        var item31Pos = BingoBoard.Ins.bingoRows[3].items[1].transform.position;
        var tabItToDaubItDialog = Instantiate(TabItToDaubItPrefab, parentTut.transform, false);
        tabItToDaubItDialog.transform.localScale = Vector3.zero;
        tabItToDaubItDialog.transform.position = item31Pos;
        tabItToDaubItDialog.transform.DOScale(Vector3.one, 0.5f);

        // Lock for the called number
        var ballonLayoutPos = ballonLayout.transform.position;
        var lockForTheCalledNumberDialog = Instantiate(LockForTheCalledNumberPrefab, parentTut.transform, false);
        lockForTheCalledNumberDialog.transform.localScale = Vector3.zero;
        lockForTheCalledNumberDialog.transform.position = ballonLayoutPos;
        lockForTheCalledNumberDialog.transform.DOScale(Vector3.one, 0.5f);

        // Enable tap screen
        disableScreen.SetActive(false);
        BingoBoard.Ins.bingoRows[2].items[0].GetComponent<Button>().onClick.AddListener(() => OnClickItem20(() =>
        {
            Destroy(hTop);
            Destroy(tabItToDaubItDialog);
            Destroy(lockForTheCalledNumberDialog);
        }));
    }

    private void OnClickItem20(Action callBack)
    {
        SoundManager.instance.PlayUIEffect("se_click1");

        BingoBoard.Ins.bingoRows[2].items[0].GetComponent<Button>().onClick.RemoveAllListeners();

        callBack.Invoke();

        StartCoroutine(AfterDaub());
    }

    private IEnumerator AfterDaub()
    {
        pauseOrResumeTimer(true);
        PauseOrResumeAnimationBalloonLayout(true);

        yield return new WaitForSeconds(1f);

        pauseOrResumeTimer(false);
        PauseOrResumeAnimationBalloonLayout(false);
        disableScreen.SetActive(true);

        // Booster
        var boosterList = Instantiate(BoosterListPrefab, parentTut.transform, false);
        boosterList.GetComponent<BoosterListScript>().BoosterList.SetActive(false);
        boosterList.GetComponent<BoosterListScript>().BoosterList.transform.localScale = Vector3.zero;

        // The faster you daub
        var ballonLayoutPos = ballonLayout.transform.position;
        var theFasterYouDaub = Instantiate(TheFasterYouDaubPrefab, parentTut.transform, false);
        theFasterYouDaub.transform.localScale = Vector3.zero;
        theFasterYouDaub.transform.position = ballonLayoutPos;
        theFasterYouDaub.transform.DOScale(Vector3.one, 0.5f);

        // Once it's fully charge
        var onceItsFullyCharge = Instantiate(OnceItsFullyChargePrefab, parentTut.transform, false);
        onceItsFullyCharge.transform.localScale = Vector3.zero;
        onceItsFullyCharge.transform.position = TopOfBottomTransform.position;
        onceItsFullyCharge.transform.DOScale(Vector3.one, 0.5f);

        boosterList.GetComponent<Button>().onClick.AddListener(() =>
        {
            SoundManager.instance.PlayUIEffect("se_click1");
            Destroy(theFasterYouDaub);
            Destroy(onceItsFullyCharge);

            boosterList.GetComponent<BoosterListScript>().BoosterList.SetActive(true);
            boosterList.GetComponent<BoosterListScript>().BoosterList.transform.DOScale(Vector3.one, 0.3f);

            boosterList.GetComponent<Button>().onClick.RemoveAllListeners();
            boosterList.GetComponent<BoosterListScript>().ContinueBtn.onClick.AddListener(() =>
            {
                SoundManager.instance.PlayUIEffect("se_click1");
                Destroy(boosterList);
                boosterIncrease();

                var TapItToUse = Instantiate(TapItToUsePrefab, parentTut.transform, false);
                TapItToUse.transform.localScale = Vector3.zero;
                TapItToUse.transform.position = TopOfBottomTransform.position;
                TapItToUse.transform.DOScale(Vector3.one, 0.5f);

                // hand right
                var hRight = Instantiate(HandRightPrefab, parentTut.transform, false);
                hRight.transform.localScale = Vector3.zero;
                hRight.transform.position = booster_1.transform.position;
                hRight.transform.DOScale(Vector3.one, 0.5f);

                disableScreen.SetActive(false);
                booster_1.GetComponent<BoosterControl>().OnClickWhenTutorial = () =>
                {
                    booster_1.GetComponent<BoosterControl>().OnClickWhenTutorial = null;
                    Destroy(TapItToUse);
                    Destroy(hRight);

                    // handtop
                    var hTop = Instantiate(HandTopPrefab, pickABallBooster.transform, false);
                    hTop.transform.localScale = Vector3.zero;
                    hTop.transform.position = PickABallThirdTransform.transform.position;
                    hTop.transform.DOScale(Vector3.one, 0.5f);

                    pickABallBooster.GetComponent<PickABallBooster>().CallBackWhenTutorial = () =>
                    {
                        Destroy(hTop);
                        StartCoroutine(AfterPickABallBooster());
                    };
                };
            });
        });

        yield return new WaitForSeconds(2f);
    }

    private GameObject _hLeft;
    private GameObject _tabItToDaubItDialog;
    private IEnumerator AfterPickABallBooster()
    {
        yield return new WaitForSeconds(1f);

        pauseOrResumeTimer(false);
        PauseOrResumeAnimationBalloonLayout(false);
        disableScreen.SetActive(true);

        var TapItToUse = Instantiate(TapItToUsePrefab, parentTut.transform, false);
        TapItToUse.transform.localScale = Vector3.zero;
        TapItToUse.transform.position = TopOfBottomTransform.position;
        TapItToUse.transform.DOScale(Vector3.one, 0.5f);

        boosterIncrease();

        // hand left
        _hLeft = Instantiate(HandLeftPrefab, parentTut.transform, false);
        _hLeft.transform.localScale = Vector3.zero;
        _hLeft.transform.position = booster_2.transform.position;
        _hLeft.transform.DOScale(Vector3.one, 0.5f);

        disableScreen.SetActive(false);

        IsWaitForWildDaubTutorial = true;

        booster_2.GetComponent<BoosterControl>().OnClickWhenTutorial = () =>
        {
            booster_2.GetComponent<BoosterControl>().OnClickWhenTutorial = null;
            Destroy(TapItToUse);

            // hand left
            var item30Pos = BingoBoard.Ins.bingoRows[3].items[0].transform.position;
            _hLeft.transform.DOMove(item30Pos, 0.3f);

            // Tab it to Daub it
            var item31Pos = BingoBoard.Ins.bingoRows[3].items[1].transform.position;
            _tabItToDaubItDialog = Instantiate(TabItToDaubItPrefab, parentTut.transform, false);
            _tabItToDaubItDialog.transform.localScale = Vector3.zero;
            _tabItToDaubItDialog.transform.position = item31Pos;
            _tabItToDaubItDialog.transform.DOScale(Vector3.one, 0.5f);

            IsWaitForWildDaubTutorial = false;
        };
    }

    private IEnumerator AfterWildDaub()
    {
        Destroy(_hLeft);
        Destroy(_tabItToDaubItDialog);

        yield return new WaitForSeconds(0.5f);

        pauseOrResumeTimer(false);
        PauseOrResumeAnimationBalloonLayout(false);
        disableScreen.SetActive(true);

        // Tap to Bingo
        var ballonLayoutPos = ballonLayout.transform.position;
        var taptoBingo = Instantiate(TapToBingoPrefab, parentTut.transform, false);
        taptoBingo.transform.localScale = Vector3.zero;
        taptoBingo.transform.position = ballonLayoutPos;
        taptoBingo.transform.DOScale(Vector3.one, 0.5f);

        // hand right
        var hRight = Instantiate(HandRightPrefab, parentTut.transform, false);
        hRight.transform.localScale = Vector3.zero;
        hRight.transform.position = bingoBtn.transform.position;
        hRight.transform.DOScale(Vector3.one, 0.5f);

        disableScreen.SetActive(false);
        bingoBtn.GetComponent<Button>().onClickWithHCSound(() =>
        {
            OnClickBingoBtnTutorial(() =>
            {
                Destroy(taptoBingo);
                Destroy(hRight);
            });
        });
    }

    private void OnClickBingoBtnTutorial(Action callBack)
    {
        bingoBtn.GetComponent<Button>().onClick.RemoveAllListeners();
        StartCoroutine(AfterBingoTutorial());
        callBack.Invoke();
    }

    private IEnumerator AfterBingoTutorial()
    {
        Toast.GetComponent<BingoToast>().ShowToast(BingoToastTypeEnum.Goodjob);
        yield return new WaitForSeconds(2f);

        parentTut.transform.GetChild(parentTut.transform.childCount-1).gameObject.SetActive(false);
        endTutorialPopup.SetActive(true);
    }

    #endregion
}
