using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using OutGameEnum;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIMatchingView : MonoBehaviour
{
    [Header("MatchingView")]
    public GameObject iconGame;
    public GameObject Player1;
    public GameObject Player1_In;
    public TextMeshProUGUI PlayerName1;
    public RawImage Player1_Avt;
    //public RawImage Player1_AvtFrame;
    public GameObject Player2;
    public GameObject Player2_In;
    public TextMeshProUGUI PlayerName2;
    public RawImage Player2_Avt;
    //public RawImage Player2_AvtFrame;
    public GameObject matchCountDown;
    public TextMeshProUGUI matchCountDownText;
    public GameObject matchFound;

    public GameObject CancelBtn;
    public GameObject CancelText;
    public GameObject CancelingText;

    private string _roomId;
    private Action _onCancel;

    private void Start()
    {
        CancelBtn.SetActive(false);
        CancelText.SetActive(true);
        CancelingText.SetActive(false);

        CancelBtn.GetComponent<Button>().onClickWithHCSound(() =>
        {
            CancelBtn.GetComponent<Button>().interactable = false;
            CancelText.SetActive(false);
            CancelingText.SetActive(true);
            StartCoroutine(Current.Ins.gameAPI.CancelRoom(_roomId, (res) => { }));
            _onCancel?.Invoke();
        });
    }

    public void LoadMatchingView()
    {
        gameObject.SetActive(true);
        
        Sequence sq = DOTween.Sequence();
        sq.Join(iconGame.transform.DOScale(new Vector3(1, 1, 1), 0.5f))
            .Join(Player1.transform.DOMove(Player1_In.transform.position, 0.5f))
            .Join(Player2.transform.DOMove(Player2_In.transform.position, 0.5f));

        sq.Play();

        PlayerName1.text = Current.Ins.player.Name;
        //getAvt
        Current.Ins.UserAvatar((result) =>
        {
            Player1_Avt.texture = result.Texture;
        });
        //Current.Ins.UserAvatarFrame((result) =>
        //{
        //    Player1_AvtFrame.texture = result.Texture;
        //});
    }

    public void SetMatchCountDown(string roomId, Action onCancel)
    {
        _roomId = roomId;
        _onCancel = onCancel;

        CancelBtn.SetActive(true);
        matchCountDown.SetActive(true);
        matchCountDown.transform.DOScale(new Vector3(1, 1, 1), 0.3f);
    }

    public void SetTextMatchDownWithAnimationAction(string text)
    {
        matchCountDownText.transform.DOScale(new Vector3(0, 0, 0), 0.1f)
        .OnComplete(() =>
        {
            matchCountDownText.text = text;
            matchCountDownText.transform.DOScale(new Vector3(1, 1, 1), 0.3f);
        });
    }

    public void UpdateOpponentInfo(string playerName, string avt, string avtFrame)
    {
        CancelBtn.SetActive(false);

        PlayerName2.text = playerName;

        Current.Ins.ImageData.AvatarByUrl(avt, (result) =>
        {
            Player2_Avt.texture = result.Texture;
            Player2_Avt.gameObject.SetActive(true);
        });

        //Current.Ins.ImageData.AvatarFrameByUrl(avtFrame, (result) =>
        //{
        //    Player2_AvtFrame.texture = result.Texture;
        //    Player2_AvtFrame.gameObject.SetActive(true);
        //});

        matchCountDown.transform.DOScale(new Vector3(0, 0, 0), 0.3f)
        .OnComplete(() =>
        {
            matchCountDown.SetActive(false);
            matchFound.SetActive(true);
            matchFound.transform.DOScale(new Vector3(1, 1, 1), 0.3f);
        });
    }
}
