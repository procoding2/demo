﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;

public class SolitaireAnimOfScore : MonoBehaviour
{
    GameObject scoreTxt;
    TMP_Text txt;
    string score;

    private void Awake()
    {
        scoreTxt = GameObject.FindGameObjectWithTag("scoreTxt");
        txt = GetComponent<TMP_Text>();
    }
    private void OnEnable()
    {
        score = txt.text;
        if (score[0] == '+') 
        {
            AnimationAddPoint();
            Debug.Log("+++++++++++++++++");
        }
        else
        {
            AnimationMinusPoint();
            Debug.Log("-----------------");
        }
       
    }

    void AnimationAddPoint()
    {
        Vector3 originalPosition = transform.localPosition;
        this.transform.localScale = Vector3.one * 1.2f;
        transform.DOLocalMoveY(transform.localPosition.y - 30, 0.5f);
        transform.DOScale(Vector3.one * 1.5f, 0.5f).OnComplete(() =>
        {
            transform.DOMove(scoreTxt.transform.position, 0.5f);
            transform.DOScale(Vector3.zero, 0.5f).OnComplete(() =>
            {
                this.gameObject.SetActive(false);
                transform.localPosition = originalPosition;
                this.transform.localScale = Vector3.one;
            });
        });
    }

    void AnimationMinusPoint()
    {
        GameObject parent = this.transform.parent.gameObject;
        Vector3 originalPosition = transform.localPosition;
        this.transform.parent = scoreTxt.transform;
        transform.position = scoreTxt.transform.position;
        transform.localScale = Vector3.one;
        transform.DOLocalMoveX(transform.position.x + 60, 0.5f);
        transform.DOScale(Vector3.one * 1.5f, 0.5f).OnComplete(() =>
        {
            transform.DOScale(Vector3.zero, 0.25f).OnComplete(() =>
            {
                this.gameObject.SetActive(false);
                this.transform.parent = parent.transform;
                transform.localPosition = originalPosition;
                this.transform.localScale = Vector3.one;
            });
        });
    }
}
