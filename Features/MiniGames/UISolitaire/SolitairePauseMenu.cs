using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SolitairePauseMenu : MonoBehaviour
{
    [Header("GameObj")]
    public GameObject main;
    public GameObject endMatchConfirm;
    [Header("Button")]
    public Button outSideBtn;
    public Button CloseBtn;
    public Button QuitGame;
    public Button EndBtn;
    public Button ResumeBtn;

    private void Awake()
    {
        QuitGame.onClick.AddListener(OnClickQuitGametn);
        EndBtn.onClick.AddListener(OnClickEndNowBtn);
        ResumeBtn.onClick.AddListener(OnClickResumeBtn);
    }

    public void ShowPauseMenu(Action cbEndNow)
    {
        main.SetActive(true);
        endMatchConfirm.SetActive(false);

        _callBackEndNow = cbEndNow;

        outSideBtn
            .onClick
            .AddListener(() =>
            {
                gameObject.SetActive(false);
                main.SetActive(false);
                endMatchConfirm.SetActive(true);
            });

        CloseBtn.onClick.AddListener(() => { outSideBtn.onClick.Invoke(); });
    }

    public void OnClickQuitGametn()
    {
        main.SetActive(false);
        endMatchConfirm.SetActive(true);
    }

    public void OnClickResumeBtn()
    {
        main.SetActive(false);
        endMatchConfirm.SetActive(false);
        gameObject.SetActive(false);
    }

    private Action _callBackEndNow;
    public void OnClickEndNowBtn()
    {
        main.SetActive(false);
        endMatchConfirm.SetActive(false);
        _callBackEndNow?.Invoke();
    }
}
