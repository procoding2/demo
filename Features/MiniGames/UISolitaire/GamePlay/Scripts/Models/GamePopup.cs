using Solitaire.Helpers;
using UniRx;

namespace Solitaire.Models
{
    public class GamePopup : StateModel<SolitaireGame.Popup>
    {
        public ReactiveCommand OptionsCommand { get; private set; }
        public ReactiveCommand LeaderboardCommand { get; private set; }
        public ReactiveCommand MatchCommand { get; private set; }

        public GamePopup(GameState gameState) : base(SolitaireGame.Popup.None)
        {
            // Options popup can be opened from home and while playing
            OptionsCommand = new ReactiveCommand(gameState.State.Select(s => s == SolitaireGame.State.Home || s == SolitaireGame.State.Playing));
            OptionsCommand.Subscribe(_ =>
            {
                if (gameState.State.Value == SolitaireGame.State.Playing)
                {
                    gameState.State.Value = SolitaireGame.State.Paused;
                }

                State.Value = SolitaireGame.Popup.Options;
            }).AddTo(this);

            // Leaderboard popup can be opened from home and while playing
            LeaderboardCommand = new ReactiveCommand(gameState.State.Select(s => s == SolitaireGame.State.Home || s == SolitaireGame.State.Playing));
            LeaderboardCommand.Subscribe(_ =>
            {
                if (gameState.State.Value == SolitaireGame.State.Playing)
                {
                    gameState.State.Value = SolitaireGame.State.Paused;
                }

                State.Value = SolitaireGame.Popup.Leaderboard;
            }).AddTo(this);

            // Match popup can only be opened while playing
            MatchCommand = new ReactiveCommand(gameState.State.Select(s => s == SolitaireGame.State.Playing));
            MatchCommand.Subscribe(_ =>
            {
                gameState.State.Value = SolitaireGame.State.Paused;
                State.Value = SolitaireGame.Popup.Match;
            }).AddTo(this);
        }
    }
}
