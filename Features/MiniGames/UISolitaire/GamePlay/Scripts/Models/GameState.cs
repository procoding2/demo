using Solitaire.Helpers;

namespace Solitaire.Models
{
    public class GameState : StateModel<SolitaireGame.State>
    {
        public GameState() : base(SolitaireGame.State.Home)
        {

        }
    }
}
