﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Solitaire.Models;
using Zenject;
using UniRx;
using Solitaire.Presenters;
using UnityEngine.Networking;
using Solitaire.Services;
using DG.Tweening;

public class GamePlaySolitaire : MonoBehaviour
{
    private static GamePlaySolitaire instance;
	public Button _buttonUndo;
	[Inject] readonly GameControls _gameControls;

    [Header("List card")]
    [SerializeField] GameObject cardPool;
    [SerializeField] List<GameObject> lstCardPool;
    [SerializeField] List<GameObject> lstCardRemove;
    [Header("List toast")]
    public GameObject Amazing;
    public GameObject Perfect;
    public GameObject Great;
    public GameObject Good;
    public GameObject Cool;
    public GameObject GoodJob;
    public GameObject Solitaire;
    public GameObject Foundation;
    public GameObject Stack_Waste;
    public GameObject Tableau;
    public GameObject Stock;
    public static GamePlaySolitaire Instance { get => instance; set => instance = value; }
    public List<GameObject> LstCardPool { get => lstCardPool; set => lstCardPool = value; }
    public GameObject CardPool { get => cardPool; set => cardPool = value; }
    public List<GameObject> LstCardRemove { get => lstCardRemove; set => lstCardRemove = value; }

    public bool CheckTut = false;

    [Inject] readonly SolitaireGame _game;
    int countBtnUndo=0;

    public float ratio = 0;

    void PointUndo()
    {
        _buttonUndo.interactable = false;
        if (++countBtnUndo > 10) countBtnUndo = 0;
        (UISolitaire.Ins as UISolitaire).updateScore(-10);
        (UISolitaire.Ins as UISolitaire).ShowScoreEffect(-10, _buttonUndo.transform.position + new Vector3(0f, 0.1f * countBtnUndo, 0));
        DOVirtual.DelayedCall(1.25f, delegate
        {
            if ((UISolitaire.Ins as UISolitaire).GetScoreSolitaire() == 0) GamePlaySolitaire.Instance._buttonUndo.interactable = false;
            else GamePlaySolitaire.Instance._buttonUndo.interactable = true;
        });
    }

    private void Awake()
    {
        instance = this;
        //
        float screen = (float)Screen.height / (float)Screen.width;
        screen = Mathf.Round(screen * 100f) / 100f;
        if (Mathf.Approximately(screen, 1.33f))
        {
            Solitaire.transform.localScale = Vector3.one * 1.3f;
        }
        //
        //
        if (!CheckTut)
        {
            this.SetPos();
        }
        Solitaire.SetActive(true);
        this.LstCardPool = new();
        this.LstCardRemove = new();

        var screenWidth = gameObject.GetComponent<RectTransform>().rect.width;
        ratio = screenWidth / 1080f;
        

    }

    float CalculateSafeArea()
    {
        Rect safeArea;
        Vector2 minAnchor;
        Vector2 maxAnchor;
        safeArea = Screen.safeArea;
        minAnchor = safeArea.position;
        maxAnchor = minAnchor + safeArea.size;

        maxAnchor.y /= Screen.height;

        float height = (1f - maxAnchor.y) * Screen.height;
        return height/100;
    }

    void SetPos()
    {
        //Rect safeArea;
        //safeArea = Screen.safeArea;
        float y_safeArea = CalculateSafeArea();
        //
        float screen = (float)Screen.height / (float)Screen.width;
        screen = screen / 1f;
        if (Mathf.Approximately(Mathf.Round(screen * 100f) / 100f, 1.78f))
        {
            Foundation.transform.position = new Vector3(0, 1.5f-y_safeArea, 0);
            Stack_Waste.transform.position = new Vector3(0, 1.5f-y_safeArea, 0);
            Tableau.transform.position = new Vector3(0, 1.5f - y_safeArea, 0);
        }
        else if (Mathf.Approximately(Mathf.Round(screen * 10f) / 10f, 2.0f))
        {
            Foundation.transform.position = new Vector3(0, 2f-y_safeArea, 0);
            Stack_Waste.transform.position = new Vector3(0, 2f - y_safeArea, 0);
            Tableau.transform.position = new Vector3(0, 2f - y_safeArea, 0);
        }

        else if (Mathf.Approximately(Mathf.Round(screen * 10f) / 10f, 2.1f) || (Mathf.Approximately(Mathf.Round(screen * 10f) / 10f, 2.2f)))
        {
            Foundation.transform.position = new Vector3(0, 3f - y_safeArea, 0);
            Stack_Waste.transform.position = new Vector3(0, 3f - y_safeArea, 0);
            Tableau.transform.position = new Vector3(0, 3f - y_safeArea, 0);
        }

        //else if (Mathf.Approximately(Mathf.Round(screen * 10f) / 10f, 2.2f))
        //{
        //    Foundation.transform.position = new Vector3(0, 3f - y_safeArea, 0);
        //    Stack_Waste.transform.position = new Vector3(0, 3f - y_safeArea, 0);
        //    Tableau.transform.position = new Vector3(0, 3f - y_safeArea, 0);
        //}
    }

    private void Start()
	{
		_gameControls.UndoCommand.BindTo(_buttonUndo).AddTo(this);
        _buttonUndo.onClick.AddListener(PointUndo);

        float screen = (float)Screen.height / (float)Screen.width;
        screen = Mathf.Round(screen * 100f) / 100f;
        if (Mathf.Approximately(screen, 1.33f))
        {
            return;
        }
        Solitaire.transform.DOScale(Vector3.one * ratio, 0f);
        GameObject.Find("CardPool").transform.DOScale(Vector3.one * ratio, 0f);
    }

    public void AddListCardPool()
    {
        cardPool = GameObject.Find("CardPool");
        for(int i = 0; i<52 ; i++)
        {
            lstCardPool.Add(cardPool.transform.GetChild(i).gameObject);
        }
    }

    public void SetActiveCard(bool active)
    {
        foreach(var card in lstCardPool)
        {
            card.GetComponent<BoxCollider2D>().enabled = active;
        }
    }

    public int GetCardObj(Card card)
    {
        foreach(GameObject obj in lstCardPool)
        {
            if (obj.GetComponent<CardPresenter>().Card == card) 
            { 
                return lstCardPool.IndexOf(obj);
            }
        }
        return -1;
    }

    public void AddCardRemove(Solitaire.Models.Card card)
    {
        foreach (var item in lstCardPool)
        {
            if (item.activeSelf == true && item.GetComponent<CardPresenter>().Card == card)
            {
                lstCardRemove.Add(item.gameObject);
            }
        }
    }

    public void RefillStock()
    {
        lstCardRemove.Clear();
        foreach (var item in lstCardPool)
        {
            item.gameObject.SetActive(true);
        }
    }

    public void CheckCardWaste()
    {
        foreach (var item in lstCardRemove)
        {
            item.gameObject.SetActive(false);
        }
        lstCardRemove.Clear();
    }

    public void CheckListRemoveWaste(Solitaire.Models.Card card)
    {
        for(int index = 0; index < lstCardRemove.Count; index++)
        {
            if (lstCardRemove[index].GetComponent<CardPresenter>().Card == card)
            {
                this.lstCardRemove.RemoveAt(index);
            }
        }
    }

    public void ShowToast(int n)
    {
        if(n == 2)
        {
            this.Cool.SetActive(true);
        }
        else if(n == 3)
        {
            this.Good.SetActive(true);
        }
        else if( n == 4)
        {
            this.Great.SetActive(true);
        }
        else if (n == 5)
        {
            this.Perfect.SetActive(true);
        }
        else if (n >= 6)
        {
            this.Amazing.SetActive(true);
        }
    }
}
