using Solitaire.Models;
using Solitaire.Services;
using System;
using System.Collections.Generic;
using Zenject;
using UnityEngine;

namespace Solitaire.Commands
{
    public class DrawCardCommand : ICommand, IDisposable, IPoolable<Pile, Pile, IMemoryPool> 
    {
        [Inject] readonly SolitaireGame _game;
        readonly List<Card> _cards = new(3);
        Pile _pileStock;
        Pile _pileWaste;
        IMemoryPool _pool;

        public void Execute(GameObject a)
        {
            //GamePlaySolitaire.Instance.CheckCardWaste();
            int count;
            if (_game.onTutorial) count = 3;
            else count = 1;

            for (int i = 0; i < count; i++)
            {
                Card card = _pileStock.TopCard();

                if (card == null)
                {
                    break;
                }

                card.Flip();
                _pileWaste.AddCard(card);
                _cards.Add(card);
                GamePlaySolitaire.Instance.AddCardRemove(card);
            }

            //_audioService.PlaySfx(Audio.SfxDraw, 1f);
            _game._PlaySoundEffectDelegate("se_sol_Draw");
        }

        public void Undo()
        {
            for (int i = _cards.Count - 1; i >= 0; i--)
            {
                Card card = _cards[i];
                card.Flip();
                _pileStock.AddCard(card);
                _cards.RemoveAt(i);
            }

            //_audioService.PlaySfx(Audio.SfxDraw, 1f);
            _game._PlaySoundEffectDelegate("se_sol_Draw");
            //GamePlaySolitaire.Instance._buttonUndo.interactable = false;
            if ((UISolitaire.Ins as UISolitaire).GetScoreSolitaire() == 0) GamePlaySolitaire.Instance._buttonUndo.interactable = false;
            else GamePlaySolitaire.Instance._buttonUndo.interactable = true;
        }

        public void Dispose()
        {
            _pool.Despawn(this);
        }

        public void OnDespawned()
        {
            _pileStock = null;
            _pileWaste = null;
            _pool = null;
        }

        public void OnSpawned(Pile pileStock, Pile pileWaste, IMemoryPool pool)
        {
            _pileStock = pileStock;
            _pileWaste = pileWaste;
            _pool = pool;
            _cards.Clear();
        }

        public class Factory : PlaceholderFactory<Pile, Pile, DrawCardCommand>
        {
        }
    }
}
