using UnityEngine;
namespace Solitaire.Commands
{
    public interface ICommand
    {
        void Execute(GameObject a);
        void Undo();
    }
}
