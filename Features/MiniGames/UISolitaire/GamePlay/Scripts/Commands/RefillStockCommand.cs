using Solitaire.Models;
using System;
using Zenject;
using UnityEngine;

namespace Solitaire.Commands
{
    public class RefillStockCommand : ICommand, IDisposable, IPoolable<Pile, Pile, IMemoryPool>
    {
        [Inject] readonly SolitaireGame _game;
        [Inject] readonly Options _options;

        Pile _pileStock;
        Pile _pileWaste;
        IMemoryPool _pool;
        int _points;

        public void Execute(GameObject a)
        {
            Card topCard;

            while ((topCard = _pileWaste.TopCard()) != null)
            {
                topCard.Flip();
                _pileStock.AddCard(topCard);
            }

            _game._PlaySoundEffectDelegate("se_sol_Draw");
        }

        public void Undo()
        {
            Card topCard;

            while ((topCard = _pileStock.TopCard()) != null)
            {
                topCard.Flip();
                _pileWaste.AddCard(topCard);
            }

            if(_game.StockTimes > 3 && !_options.DrawThree.Value)
            {
                _game.StockTimes--;
                (UISolitaire.Ins as UISolitaire).updateScore(50);
                (UISolitaire.Ins as UISolitaire).ShowScoreEffect(50, GamePlaySolitaire.Instance.Stock.transform.position);
            }

            _game._PlaySoundEffectDelegate("se_sol_Draw");

            if ((UISolitaire.Ins as UISolitaire).GetScoreSolitaire() == 0) GamePlaySolitaire.Instance._buttonUndo.interactable = false;
            else GamePlaySolitaire.Instance._buttonUndo.interactable = true;
        }

        public void Dispose()
        {
            _pool.Despawn(this);
        }

        public void OnDespawned()
        {
            _pileStock = null;
            _pileWaste = null;
            _pool = null;
        }

        public void OnSpawned(Pile pileStock, Pile pileWaste, IMemoryPool pool)
        {
            _pileStock = pileStock;
            _pileWaste = pileWaste;
            _pool = pool;
            _points = 0;
        }

        public class Factory : PlaceholderFactory<Pile, Pile, RefillStockCommand>
        {
        }
    }
}
