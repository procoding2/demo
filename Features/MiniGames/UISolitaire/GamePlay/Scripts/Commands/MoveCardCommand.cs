using Solitaire.Models;
using Solitaire.Presenters;
using System;
using UnityEngine;
using Zenject;

namespace Solitaire.Commands
{
    public class MoveCardCommand : ICommand, IDisposable, IPoolable<Card, Pile, Pile, IMemoryPool>
    {
        [Inject] readonly SolitaireGame.Config _gameConfig;
        [Inject] readonly SolitaireGame _game;
        [Inject] readonly GameState _gameState;

        Card _card;
        Pile _pileSource;
        Pile _pileTarget;
        IMemoryPool _pool;

        int undoScoreBack = 0;

        private bool _wasTopCardFlipped;
        public void Execute(GameObject a)
        {
            if (_pileSource.TopCard() == _card)
            {
                // Single card
                _pileTarget.AddCard(_card);
            }
            else
            {
                // Multiple cards
                var cards = _pileSource.SplitAt(_card);
                _pileSource.RemoveCards(cards);
                _pileTarget.AddCards(cards);
            }

            // Scoring
            if (_pileSource.IsWaste)
            {
                if (_pileTarget.IsTableau)
                {
                    Setscore(a, _gameConfig.PointsWasteToTableau);
                }
                else if (_pileTarget.IsFoundation)
                {
                    Setscore(a, ScoreToFoundation(_gameConfig.PointsWasteToFoundation));
                }
            }
            else if (_pileSource.IsTableau && _pileTarget.IsFoundation)
            {
                Setscore(a, ScoreToFoundation(_gameConfig.PointsTableauToFoundation));
            }
            else if (_pileSource.IsFoundation && _pileTarget.IsTableau)
            {
                Setscore(a, ScoreBackTableaus(_gameConfig.PointsFoundationToTableau));
            }

            _game._PlaySoundEffectDelegate("se_sol_Draw");
            // Reveal card below if needed
            Card cardBelow = _pileSource.TopCard();
            int index = GamePlaySolitaire.Instance.GetCardObj(cardBelow);
            GameObject scoreFaceUP = null;
            if (index != -1)
                scoreFaceUP = GamePlaySolitaire.Instance.LstCardPool[index];
            
            if (_pileSource.IsTableau &&
                cardBelow != null && !cardBelow.IsFaceUp.Value)
            {
                cardBelow.Flip();
                _wasTopCardFlipped = true;

                if(scoreFaceUP != null)
                    Setscore(scoreFaceUP.GetComponent<CardPresenter>().canvasScore, _gameConfig.PointsTurnOverTableauCard);
            }

            if ((UISolitaire.Ins as UISolitaire).GetScoreSolitaire() == 0 || _gameState.State.Value == SolitaireGame.State.Win) GamePlaySolitaire.Instance._buttonUndo.interactable = false;
            else GamePlaySolitaire.Instance._buttonUndo.interactable = true;
        }

        int ScoreToFoundation(int df) 
        {
            if (_game.comboToFoundation > 1)
            {
                int final = df + (_game.comboToFoundation-1) * 10;
                undoScoreBack = _game.comboToFoundation;
                return final;
            }
            return df;
        }

        int ScoreBackTableaus(int df)
        {
            if (_game.comboBackTableaus > 1)
            {
                int final = df - (_game.comboBackTableaus - 1) * 10;
                return final;
            }
            return df;
        }

        void Setscore(GameObject scoreGameObj, int score)
        {
            (UISolitaire.Ins as UISolitaire).updateScore(score);
            if (scoreGameObj == null)
            {
                (UISolitaire.Ins as UISolitaire).ShowScoreEffect(score, GamePlaySolitaire.Instance._buttonUndo.transform.position);
                return;
            }
            (UISolitaire.Ins as UISolitaire).ShowScoreEffect(score, scoreGameObj.transform.position);
        }
        public void Undo()
        {
            // Hide top card of the source tableau pile
            Card cardTop = _pileSource.TopCard();

            if (_pileSource.IsTableau && _wasTopCardFlipped &&
                cardTop != null && cardTop.IsFaceUp.Value)
            {
                cardTop.Flip();
                Setscore(cardTop.objCard, -_gameConfig.PointsTurnOverTableauCard);
            }

            // Scoring
            if (_pileSource.IsWaste)
            {
                if (_pileTarget.IsTableau)
                {
                    Setscore(_pileTarget.TopCard().objCard, -_gameConfig.PointsWasteToTableau);
                }
                else if (_pileTarget.IsFoundation)
                {
                    Setscore(_pileTarget.TopCard().objCard, -ScoreToFoundation(_gameConfig.PointsWasteToFoundation));
                }
            }
            else if (_pileSource.IsTableau && _pileTarget.IsFoundation)
            {

                if (undoScoreBack < 0) undoScoreBack = 0;
                Setscore(_pileTarget.TopCard().objCard, -(undoScoreBack--*10 + _gameConfig.PointsTableauToFoundation));
            }
            else if (_pileSource.IsFoundation && _pileTarget.IsTableau)
            {
                Setscore(_pileTarget.TopCard().objCard, -_gameConfig.PointsFoundationToTableau);
            }

            _game._PlaySoundEffectDelegate("se_sol_Draw");
            if (_pileTarget.TopCard() == _card)
            {
                // Single card
                _pileSource.AddCard(_card);
            }
            else
            {
                // Multiple cards
                var cards = _pileTarget.SplitAt(_card);
                _pileTarget.RemoveCards(cards);
                _pileSource.AddCards(cards);
            }
			// ButtonUndo

            _game.comboToFoundation = 0;
            if(_game.comboBackTableaus >0)
            {
                _game.comboBackTableaus--;
            }

            if ((UISolitaire.Ins as UISolitaire).GetScoreSolitaire() == 0) GamePlaySolitaire.Instance._buttonUndo.interactable = false;
            else GamePlaySolitaire.Instance._buttonUndo.interactable = true;
        }

        public void Dispose()
        {
            _pool.Despawn(this);
        }

        public void OnDespawned()
        {
            _card = null;
            _pileSource = null;
            _pileTarget = null;
            _pool = null;
        }

        public void OnSpawned(Card card, Pile pileSource, Pile pileTarget, IMemoryPool pool)
        {
            _card = card;
            _pileSource = pileSource;
            _pileTarget = pileTarget;
            _pool = pool;
            _wasTopCardFlipped = false;
        }

        public class Factory : PlaceholderFactory<Card, Pile, Pile, MoveCardCommand>
        {
        }
    }
}
