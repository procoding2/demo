using Solitaire.Models;
using Solitaire.Services;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

namespace Solitaire.Helpers
{
    [RequireComponent(typeof(Selectable))]
    public class InteractableSfx : MonoBehaviour, IPointerUpHandler
    {
        //[Inject] readonly IAudioService _audioService;
        [Inject] readonly SolitaireGame _game;
        [SerializeField]Selectable _selectable;

        private void Awake()
        {
            _selectable = GetComponent<Selectable>();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (_selectable.IsInteractable())
            {
                //_audioService.PlaySfx(Audio.SfxClick, 1.0f);
                _game._PlaySoundEffectDelegate("se_sol_Click");
            }
        }
    }
}
