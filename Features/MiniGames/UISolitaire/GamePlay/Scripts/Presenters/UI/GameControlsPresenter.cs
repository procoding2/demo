using Solitaire.Models;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Solitaire.Presenters
{
    public class GameControlsPresenter : OrientationAwarePresenter
    {
        [SerializeField] Button _buttonHome;
        [SerializeField] Button _buttonUndo;
        [SerializeField] Button _buttonHint;

        [Inject] readonly GameControls _gameControls;


        protected override void Start()
        {
            base.Start();
            _gameControls.UndoCommand.BindTo(_buttonUndo).AddTo(this);
            _gameControls.HintCommand.BindTo(_buttonHint).AddTo(this);
        }

        protected override void OnOrientationChanged(bool isLandscape)
        {
        }
    }
}
