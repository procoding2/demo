﻿using DG.Tweening;
using Solitaire.Models;
using Solitaire.Services;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace Solitaire.Presenters
{
    public class PilePresenter : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] Pile.PileType Type;
        [SerializeField] Pile.CardArrangement Arrangement;
        [SerializeField] Vector3 PosPortrait;
        [SerializeField] Vector3 PosLandscape;
        [SerializeField] GameObject refillStock;
        [SerializeField] GameObject refillStockScore;

        [Inject] readonly Pile _pile;
        [Inject] readonly SolitaireGame _game;
        [Inject] readonly IDragAndDropHandler _dndHandler;
        [Inject] readonly OrientationState _orientation;

        public Pile Pile => _pile;

        public BoxCollider2D _collider => GetComponent<BoxCollider2D>();

        [SerializeField] Sprite replay50;
        [SerializeField] SpriteRenderer replay;
        void Awake()
        {
            PosPortrait = this.transform.position;
            _pile.Init(Type, Arrangement, transform.position);
        }

        void Start()
        {
            // Update layout on orientation change
            _orientation.State.Subscribe(UpdateLayout).AddTo(this);

            _pile.pilePresenter = gameObject;
        }

        //public void OnDrop(PointerEventData eventData)
        //{
        //    //if (eventData == null || eventData.pointerDrag == null)
        //    //{
        //    //    return;
        //    //}

        //    //if (eventData.pointerDrag.TryGetComponent(out CardPresenter cardPresenter) &&
        //    //    _pile.CanAddCard(cardPresenter.Card))
        //    //{
        //    //    _dndHandler.Drop();
        //    //    _game.MoveCard(cardPresenter.Card, _pile, cardPresenter.canvasScore);
        //    //}
        //    //else
        //    //{
        //    //    _game.PlayErrorSfx();
        //    //}
        //}

        public void OnPointerClick(PointerEventData eventData)
        {
            if (eventData?.clickCount == 1 && _pile.IsStock)
            {
                _game.StockTimes++;
                GamePlaySolitaire.Instance.RefillStock();
                _game.RefillStock(refillStock);                
                //if (_game.StockTimes > 3 && _game.refillStock == false)
                //{
                //    this.refillStockScore.SetActive(true);                  
                //}
                if (_game.StockTimes == 3 && replay.sprite != replay50 && _game.refillStock == false)
                {
                    DOVirtual.DelayedCall(1f, delegate
                    {
                        replay.sprite = replay50;
                    });
                }
            }
        }

        private void UpdateLayout(Orientation orientation)
        {
            Vector3 position = orientation == Orientation.Landscape ?
                PosLandscape : PosPortrait;

            transform.position = position;
            _pile.UpdatePosition(position);
        }
    }
}
