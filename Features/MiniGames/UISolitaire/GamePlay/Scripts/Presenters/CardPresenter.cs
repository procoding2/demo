﻿using CodeStage.AntiCheat.ObscuredTypes;
using DG.Tweening;
using Solitaire.Models;
using Solitaire.Services;
using System;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace Solitaire.Presenters
{
    [RequireComponent(typeof(BoxCollider2D))]
    public class CardPresenter : MonoBehaviour, 
        IPoolable<Card.Suits, Card.Types, IMemoryPool>, IDisposable, 
        IBeginDragHandler, IDragHandler, IEndDragHandler,IPointerClickHandler
    {
        [Header("Sprites")]
        [SerializeField] SpriteRenderer _back;
        [SerializeField] SpriteRenderer _front;
        [SerializeField] SpriteRenderer _type;
        [SerializeField] SpriteRenderer _suit1;
        [SerializeField] SpriteRenderer _suit2;
        [Header("Score")]
        public GameObject canvasScore;

        [Inject] readonly SolitaireGame _game;
        [Inject] readonly Card _card;
        [Inject] readonly Card.Config _config;
        [Inject] readonly IDragAndDropHandler _dndHandler;

        Tweener _tweenScale;
        Tweener _tweenMove;
        BoxCollider2D _collider;
        Transform _transform;
        IMemoryPool _pool;

        public Card Card => _card;

        const float MoveEpsilon = 0.00001f;
        const int AnimOrder = 100;

        public ObscuredBool moveAlone = false;
        public ObscuredBool antiSpam = false;
        public ObscuredVector3 scale = Vector3.one;

        void Awake()
        {
            moveAlone = false;
            //
            scale = Vector3.one;
        }

        void Start()
        {
            _collider = GetComponent<BoxCollider2D>();
            _transform = transform;

            _card.Alpha.Subscribe(UpdateAlpha).AddTo(this);
            _card.Order.Subscribe(UpdateOrder).AddTo(this);
            _card.IsVisible.Subscribe(UpdateVisiblity).AddTo(this);
            _card.IsInteractable.Subscribe(UpdateInteractability).AddTo(this);
            _card.IsFaceUp.Where(CanFlip).Subscribe(AnimateFlip).AddTo(this);
            _card.Position.Where(CanMove).Subscribe(AnimateMove).AddTo(this);

            float screen = (float)Screen.height / (float)Screen.width;
            screen = Mathf.Round(screen * 100f) / 100f;
            if (Mathf.Approximately(screen, 1.33f))
            {
                scale = Vector3.one * GamePlaySolitaire.Instance.ratio;
            }

            _card.cardPresenter = gameObject;
            //_collider.size = new Vector2(_collider.size.x * scale.x, _collider.size.y * scale.y);
        }

        bool CanFlip(bool isFaceUp)
        {
            return (isFaceUp && !_front.gameObject.activeSelf) || (!isFaceUp && !_back.gameObject.activeSelf);
        }

        void AnimateFlip(bool isFaceUp)
        {
            // Scale X from 1 to 0 then back to 1 again,
            // switching between front and back sprites in the middle.
            // This gives the illusion of flipping the card in 2D.
            if (_tweenScale == null)
            {
                _tweenScale = _transform.DOScaleX(0f, _config.AnimationDuration / 2f)
                    .SetLoops(2, LoopType.Yoyo)
                    .SetEase(Ease.Linear)
                    .SetAutoKill(false)
                    .OnStepComplete(() => Flip(_card.IsFaceUp.Value))
                    .OnComplete(() => _transform.localScale = scale);
            }
            else
            {
                _tweenScale.Restart();
            }
        }

        bool CanMove(Vector3 position)
        {
            return Vector3.SqrMagnitude(position - _transform.position) > MoveEpsilon;
        }
        public void AnimateMove(Vector3 position)
        {
            if (_card.IsDragged)
            {
                // Update position instantly while the card is being dragged
                _transform.position = position;
            }
            else
            {
                // Move card over time to the target position while changing
                // order at the start and end so the cards are overlaid correctly.
                if (_tweenMove == null)
                {
                    _tweenMove = _transform.DOLocalMove(position, _config.AnimationDuration)
                        .SetEase(Ease.OutQuad)
                        .SetAutoKill(false)
                        .OnRewind(() =>
                        {
                            _card.OrderToRestore = _card.IsInPile ? _card.Pile.Cards.IndexOf(_card) : _card.Order.Value;
                            _card.Order.Value = AnimOrder + _card.OrderToRestore;
                        })
                        .OnComplete(() =>
                        {
                            _card.Order.Value = _card.OrderToRestore;
                            antiSpam = false;
                            moveAlone = false;
                        });
                }
                else
                {
                    _tweenMove.ChangeEndValue(position, true).Restart();
                }
            }
        }

        void UpdateOrder(int order)
        {
            // Update the sorting order of each sprite
            int sortingOrder = order * 10;
            _back.sortingOrder = sortingOrder;
            _front.sortingOrder = sortingOrder;
            _type.sortingOrder = sortingOrder + 1;
            _suit1.sortingOrder = sortingOrder + 1;
            _suit2.sortingOrder = sortingOrder + 1;
        }
        
        void UpdateAlpha(float alpha)
        {
            Color color = _back.color;
            color.a = alpha;
            _back.color = color;

            color = _front.color;
            color.a = alpha;
            _front.color = color;

            color = _type.color;
            color.a = alpha;
            _type.color = color;

            color = _suit1.color;
            color.a = alpha;
            _suit1.color = color;

            color = _suit2.color;
            color.a = alpha;
            _suit2.color = color;
        }

        void UpdateVisiblity(bool isVisible)
        {
            _back.enabled = isVisible;
            _front.enabled = isVisible;
            _type.enabled = isVisible;
            _suit1.enabled = isVisible;
            _suit2.enabled = isVisible;
            //this.canvasScore.SetActive(isVisible);
        }

        void UpdateInteractability(bool isInteractable)
        {
            _collider.enabled = isInteractable;
        }

        private Sprite FindSpriteByNameInArray(string name)
        {
            foreach (Sprite sprite in _config.CardSuitType)
            {
                if (sprite.name == name)
                {
                    return sprite;
                }
            }
            return null;
        }

        void Initialize()
        {
            name = $"Card_{_card.Suit}_{_card.Type}";
            _front.sprite = FindSpriteByNameInArray(name.ToString());
            _card.objCard = this.canvasScore;
            // Update suit sprites
   //         Sprite spriteSuit = _config.SuitSprites[(int)_card.Suit];
   //         _suit1.sprite = spriteSuit;
            

   //         if(_card.Type == Card.Types.Jack || _card.Type == Card.Types.Queen || _card.Type == Card.Types.King)
   //         {
   //             switch(_card.Suit)
   //             {
   //                 case Card.Suits.Spade:
   //                 case Card.Suits.Club:
   //                     if(_card.Type == Card.Types.Jack)
   //                     {
			//				spriteSuit = _config.SuitSprites[4];
			//			}
   //                     else if(_card.Type == Card.Types.Queen)
   //                     {
   //                         spriteSuit = _config.SuitSprites[6];
			//			}
   //                     else
   //                     {
   //                         spriteSuit = _config.SuitSprites[8];
			//			}
   //                     break;

			//		case Card.Suits.Diamond:
			//		case Card.Suits.Heart:
			//			if (_card.Type == Card.Types.Jack)
			//			{
			//				spriteSuit = _config.SuitSprites[5];
			//			}
			//			else if (_card.Type == Card.Types.Queen)
			//			{
			//				spriteSuit = _config.SuitSprites[7];
			//			}
			//			else
			//			{
			//				spriteSuit = _config.SuitSprites[9];
			//			}
			//			break;
			//	}
   //             _suit2.transform.localPosition = new Vector3(0.05f, -0.18f, 0);

			//}
			//_suit2.sprite = spriteSuit;

			//// Update type color and sprite
			//Color color = _config.Colors[(int)_card.Suit];
   //         Sprite spriteType = _config.TypeSprites[(int)_card.Type];
   //         _type.sprite = spriteType;
   //         _type.color = color;
        }

        public void Flip(bool isFaceUp)
        {
            _back.gameObject.SetActive(!isFaceUp);
            _front.gameObject.SetActive(isFaceUp);
        }

        #region IEventSystemHandlers

        private void OnMouseDown()
        {
            foreach(GameObject a in GamePlaySolitaire.Instance.LstCardPool)
            {
                a.GetComponent<CardPresenter>().moveAlone = false;
            }
            moveAlone = true;          
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
			if (_card.IsMoveable && moveAlone)
            {
                _dndHandler.BeginDrag(eventData, _card.Pile.SplitAt(_card));
                _card.IsInteractable.Value = false;                
            }
        }

        public void OnDrag(PointerEventData eventData)
        {
			if (_card.IsMoveable && moveAlone)
            {
                _dndHandler.Drag(eventData);
            }            
        }

        private bool CheckDropAndMoveCard(CardPresenter handOnCard, Pile pile)
        {
            BoxCollider2D col1 = handOnCard._collider;

            Bounds bounds1, bounds2;

            Card topCard = pile.TopCard();
            if (topCard != null && topCard.objCard && topCard.cardPresenter.TryGetComponent(out CardPresenter topCardObj))
            {

                Vector3 center1 = new Vector3(col1.bounds.center.x, col1.bounds.center.y, 0);
                Vector3 center2 = new Vector3(topCardObj._collider.bounds.center.x, topCardObj._collider.bounds.center.y, 0);
                bounds1 = new Bounds(center1, topCardObj._collider.bounds.size);
                bounds2 = new Bounds(center2, topCardObj._collider.bounds.size);

                if (bounds1.Intersects(bounds2) && pile.CanAddCard(handOnCard.Card))
                {
                    _dndHandler.Drop();
                    _game.MoveCard(handOnCard.Card, pile, canvasScore);
                    return true;
                }
            }
            else if (topCard == null && pile.pilePresenter.TryGetComponent(out PilePresenter pileObj))
            {
                Vector3 center1 = new Vector3(col1.bounds.center.x, col1.bounds.center.y, 0);
                Vector3 center2 = new Vector3(pileObj._collider.bounds.center.x, pileObj._collider.bounds.center.y, 0);
                bounds1 = new Bounds(center1, pileObj._collider.bounds.size);
                bounds2 = new Bounds(center2, pileObj._collider.bounds.size);

                if (bounds1.Intersects(bounds2) && pile.CanAddCard(handOnCard.Card))
                {
                    _dndHandler.Drop();
                    _game.MoveCard(handOnCard.Card, pile, canvasScore);
                    return true;
                }
            }

            return false;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            //onDropFunc
            if (eventData != null && eventData.pointerDrag != null && eventData.pointerDrag.TryGetComponent(out CardPresenter cardPresenter))
            {
                BoxCollider2D col1 = cardPresenter._collider;
                bool isExist = false;
                foreach (Pile pile in _game.PileTableaus)
                {
                    if (isExist = CheckDropAndMoveCard(cardPresenter, pile))
                        break;
                }
                if (isExist == false)
                {
                    foreach (Pile pile in _game.PileFoundations)
                    {
                        if (isExist = CheckDropAndMoveCard(cardPresenter, pile))
                            break;
                    }
                }

                if (isExist == false)
                {
                    _game.PlayErrorSfx();
                }
                moveAlone = false;
            }

            if (_card.IsMoveable)
            {
                _dndHandler.EndDrag();
                _card.IsInteractable.Value = true;
                moveAlone = false;
            }           
        }

   //     public void OnDrop(PointerEventData eventData)
   //     {
			////if (eventData == null || eventData.pointerDrag == null)
   ////         {
   ////             return;
   ////         }

   ////         if (eventData.pointerDrag.TryGetComponent(out CardPresenter cardPresenter) &&
   ////             _card.Pile.CanAddCard(cardPresenter.Card))
   ////         {
   ////             _dndHandler.Drop();
   ////             _game.MoveCard(cardPresenter.Card, _card.Pile, canvasScore);
   ////         }
   ////         else
   ////         {
   ////             _game.PlayErrorSfx();
   ////         }

   ////         moveAlone = false;
   //     }

        public void OnPointerClick(PointerEventData eventData)
        {
            moveAlone = false;
            if (eventData == null)
            {
                return;
            }

            if (_card.IsDrawable)
            {
                _game.DrawCard(null);
                if ((UISolitaire.Ins as UISolitaire).GetScoreSolitaire() == 0) GamePlaySolitaire.Instance._buttonUndo.interactable = false;
                else GamePlaySolitaire.Instance._buttonUndo.interactable = true;
            }
            else if (!_game.moveCard && antiSpam == false)
            {
                
                _game.moveCard = true;
                if (_card.IsMoveable)
                {
                    antiSpam = true;
                    _game.MoveCard(_card, null, canvasScore);               
                }
                else
                {
                    _game.PlayErrorSfx();
                }

                DOVirtual.DelayedCall(0.15f, delegate
                {
                    _game.moveCard = false;
                });
            }
        }

        #endregion IEventSystemHandlers

        #region IPoolable

        public void OnSpawned(Card.Suits suit, Card.Types type, IMemoryPool pool)
        {
            // Init model
            _pool = pool;
            _card.Init(suit, type);
            Initialize();
        }

        public void OnDespawned()
        {
            // Reset model
            _pool = null;
            _card.Reset(Vector3.zero);
        }

        #endregion IPoolable

        #region IDisposable

        public void Dispose()
        {
            _pool.Despawn(this);
        }

        #endregion IDisposable

        public class Factory : PlaceholderFactory<Card.Suits, Card.Types, CardPresenter>
        {
        }
    }
}
