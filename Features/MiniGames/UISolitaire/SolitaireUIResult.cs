using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using GameEnum;
using Solitaire.Services;
using Zenject;
using UnityEngine.SocialPlatforms;
using DG.Tweening;

public class SolitaireUIResult : UIBase<SolitaireUIResult>
{
    protected override void InitIns() => Ins = this;

    public GameObject TimesUpTitle;
    public GameObject GameOverTitle;
    public GameObject SuccessMissionTitle;
    public GameObject Score;
    public GameObject TimeBonus;
    public GameObject FinalScores;
    public GameObject submitBtn;

    public void Init(SolitaireScoreResult result,
        GameFinishTypeEnum finishType,
        string gameId,
        string key,
        Action<SaveScoreGameResponse> afterSubmitCallBack)
    {
        switch (finishType)
        {
            case GameFinishTypeEnum.TimesUp:
                TimesUpTitle.SetActive(true);
                GameOverTitle.SetActive(false);
                SuccessMissionTitle.SetActive(false);
                break;
            case GameFinishTypeEnum.GameOver:
                TimesUpTitle.SetActive(false);
                GameOverTitle.SetActive(true);
                SuccessMissionTitle.SetActive(false);
                break;
            case GameFinishTypeEnum.Success:
                TimesUpTitle.SetActive(false);
                GameOverTitle.SetActive(false);
                SuccessMissionTitle.SetActive(true);
                break;
        }
        //
        Score.GetComponent<DetailItemResult>().
            Init
            (
                "",
                result.ScoreSolitaire.ToString()
            );

        TimeBonus.GetComponent<DetailItemResult>().
            Init
            (
                "",
                result.ScoreTimeBonusSolitaire.ToString()
            );

        var totalScore = result.ScoreSolitaire + result.ScoreTimeBonusSolitaire;
        totalScore = totalScore >= 0 ? totalScore : 0;
        FinalScores.GetComponent<Text>().text = totalScore.ToString();

        SaveScoreGameRequest req = new SaveScoreGameRequest();
        req.key = key;
        req.status = (int)finishType;
        req.score = totalScore;
        req.scoreInfo = new SaveScoreGameRequest.ScoreInfo[2];
        req.scoreInfo[0] = new SaveScoreGameRequest.ScoreInfo()
        {
            name = StringConst.SOLITAIRE_SCORE_INFO_SCORE,
            quantity = 0,
            score = result.ScoreSolitaire
        };

        req.scoreInfo[1] = new SaveScoreGameRequest.ScoreInfo()
        {
            name = StringConst.SOLITAIRE_SCORE_INFO_TIMEBONUS,
            quantity = 0,
            score = result.ScoreTimeBonusSolitaire
        };

        req.time = result.EndTimeMilliSecond - result.StartTimeMilliSecond;

        UIPopUpManager.instance.ShowLoadingCircle(true);
        StartCoroutine(Current.Ins.gameAPI.SaveScoreGame<SaveScoreGameResponse>(gameId, req, (res) =>
        {
            UIPopUpManager.instance.ShowLoadingCircle(false);
            if (res == null || res.errorCode != 0)
            {
                if (res != null && res.errorCode == 2)
                {
                    GetGameScoreWhenError(gameId, key, afterSubmitCallBack);
                    return;
                }

                UIPopUpManager.instance.ShowErrorPopUp("Request Error : SaveScoreGame", res, () => submitBtn.GetComponent<Button>().onClick?.Invoke());
                return;
            }

            res.dataDecode = JsonUtility.FromJson<SaveScoreGameResponse.DataDecode>(EncryptHelper.DecryptWithAES256(res.data.info, key));
            afterSubmitCallBack.Invoke(res);
        }));
    }

    private void GetGameScoreWhenError(string gameId, string key, Action<SaveScoreGameResponse> afterSubmitCallBack)
    {
        UIPopUpManager.instance.ShowLoadingCircle(true);
        StartCoroutine(Current.Ins.gameAPI.GetGameScore(gameId, (res) =>
        {
            UIPopUpManager.instance.ShowLoadingCircle(false);
            if (res == null || res.errorCode != 0)
            {
                UIPopUpManager.instance.ShowErrorPopUp("Request Error : GetGameScoreWhenError", res, () => GetGameScoreWhenError(gameId, key, afterSubmitCallBack));
                return;
            }

            afterSubmitCallBack.Invoke(res);
        }));
    }
}
