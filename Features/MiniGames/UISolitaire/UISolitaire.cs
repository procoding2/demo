using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using GameEnum;
using System;
using System.Linq;
using UniRx;
using UnityEngine.Playables;
using DG.Tweening;
using System.Threading.Tasks;
using OutGameEnum;
using Solitaire.Models;
using Zenject;
using TMPro;
using CodeStage.AntiCheat.ObscuredTypes;

public class UISolitaire : UIGameBase<UISolitaire>
{
    public SolitaireDataResponse.GameInfo gameInfoCards { get; set; }
    protected override void InitIns() => Ins = this;
    
    [Header("UISolitaire")]
    [Header("GameObj")]
    [SerializeField] GameObject pauseMenu;
    public GameObject matchResult;
    
    public GameObject gamePlaySolitaire;
    public GameObject endTutorialPopup;
    [Header("Button")]
    [SerializeField] GameObject tutorialbtn;
    [SerializeField] GameObject menuBtn;
    [SerializeField] GameObject skipTut;
    [Header("User Info")]
    public UserInfo myUserInfo, oppnentUserInfo;
    [Header("Time")]
    public TimeView timeView;

    public GameObject ScoreTextObj;

    [Header("Music")]
    public AudioClip SolitaireBackgroundMusic;

    [Header("Data")]
    private ObscuredBool _isPlayingCountDownTimeSound = false;
    private ObscuredBool _isTimerRunning = false;
    DataUIBase dataGame = new DataUIBase();
    public Subject<ObscuredFloat> _timerRx = new Subject<ObscuredFloat>();
    public SolitaireScoreResult result;
    [Inject] readonly GameState _gameState;
    //
    public MiniGameStartTypeEnum type;

    private readonly ObscuredFloat MAX_TIME = 180f;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();

        PlayerPrefsEx.Set(MiniGameEnum.Solitaire.GetStringValue(), 1);
        SoundManager.instance.PlayMusic("bgm_findUser");

        ObjectPoolManager.instance.ReservePool(ScoreTextObj, 5, true);

        if (HomeTab.UIHomeTab.DefaultView != TabbarEnum.Event)
        {
            HomeTab.UIHomeTab.DefaultView = TabbarEnum.Home_GameMode_Solitaire;
        }

        this.type = UISolitaire.sStartType;
        switch (UISolitaire.sStartType)
        {
            case MiniGameStartTypeEnum.Init:
                Init(UISolitaire.sGameMode, UISolitaire.sGameId);
                break;
            case MiniGameStartTypeEnum.InitNextGame:
                InitNextGame(UISolitaire.sRoomId);
                break;
            case MiniGameStartTypeEnum.InitReMatch:
                InitReMatch(UISolitaire.sGameId);
                break;
            case MiniGameStartTypeEnum.Tutorial:
                gamePlaySolitaire.GetComponent<GamePlaySolitaire>().CheckTut = true;
                InitTutorialGame();
                break;

        }
        this.OnclickButtonOnSolitaire();
    }

    private void InitRx()
    {
        DisposeAllRx();

        // Score
        bags.Add(increaseScoreRx());
        updateScore(0);

        // Timer
        bags.Add(timerRx());
        updateTimer(MAX_TIME);
        pauseOrResumeTimer(true);
        bags.Add(StartTimer());
    }

    //Event button
    void OnclickButtonOnSolitaire()
    {
        if(this.type == MiniGameStartTypeEnum.Tutorial) menuBtn.SetActive(false);
        else menuBtn.GetComponent<Button>().onClick.AddListener(OnClickMenuBtn);
    }

    public void OnClickMenuBtn()
    {
        pauseMenu.SetActive(true);
        var pMenu = pauseMenu.GetComponent<SolitairePauseMenu>();

        Action callBackEndNow = () =>
        {
            GameFinished(GameFinishTypeEnum.GameOver);
        };

        pMenu.ShowPauseMenu(callBackEndNow);
        pMenu.OnClickQuitGametn();
    }

    private void pauseOrResumeTimer(bool isResume)
    {
        //if (_isPlayingCountDownTimeSound)
        //{
        //    if (isResume) HCSound.Ins.FxSoundGame2.UnPause();
        //    else HCSound.Ins.FxSoundGame2.Pause();
        //}

        _isTimerRunning = isResume;

        timeView.SetSecAni(_isTimerRunning);
    }
    // Update is called once per frame
    protected override void Update()
    {
        base.Update();

        if (_isTimerRunning)
        {
            _currentTime += Time.deltaTime * 1000f;
            if (_currentTime >= MAX_TIME * 1000f) _currentTime = MAX_TIME * 1000f;
        }
    }

    protected override void OnReloadCryptoKey()
    {
        MAX_TIME.RandomizeCryptoKey();
        _isPlayingCountDownTimeSound.RandomizeCryptoKey();
        _isTimerRunning.RandomizeCryptoKey();
    }

    protected override void LoadMatchingView()
    {
        base.LoadMatchingView();

        var req = new CreateRoomByMiniGameRequest();
        req.GameId = _gameId;

        UserResponse uSocketRes = null;
        SolitaireDataResponse dtRes = null;

        GetUserSecret(() =>
        {
            StartCoroutine(Current.Ins.gameAPI.CreateRoomByMiniGame<SolitaireDataResponse>(req, (res) =>
            {
                if (res == null || res.errorCode != 0)
                {
                    OnCallBackAPIError(res);
                    return;
                }

                dtRes = res;
                res.dataDecode = JsonUtility.FromJson<SolitaireDataResponse.DataDecode>(EncryptHelper.DecryptWithAES256(res.data.info, sKey));
                _matchData.Response = res;
                _matchData.GameId = res.dataDecode.gameId;
                _matchData.RoomId = res.dataDecode.roomId;
                _matchData.IsSync = res.dataDecode.isSync;

                _matchData.UpdateIsSyncAction = (isSync) => res.dataDecode.isSync = isSync;
                _matchData.UpdateOpponentScoreAction = (oScore) => dataGame.OScore = oScore;
                _matchData.User = res.dataDecode.users.FirstOrDefault();
                if (uSocketRes != null)
                {
                    _matchData.User = new UserResponse();
                    _matchData.User.id = uSocketRes.id;
                    _matchData.User.name = uSocketRes.name;
                    _matchData.User.avatar = uSocketRes.avatar;
                }

                Matching();
                if (_matchData.User == null) _userRx.OnNext(null);
            }));
        });

        Current.Ins.socketAPI.AddSocket<MatchUserSocketResponse>
        (
            StringConst.SOCKET_MATCH_USER_EVENT,
            $"{StringConst.SOCKET_MATCH_USER_EVENT}{Current.Ins.player.Id}",
            (opponentRes) =>
            {
                if (_matchData.GameId != opponentRes.gameId) return;

                if (dtRes == null)
                {
                    uSocketRes = new UserResponse();
                    uSocketRes.id = opponentRes.user.id;
                    uSocketRes.name = opponentRes.user.name;
                    uSocketRes.avatar = opponentRes.user.avatar;
                    uSocketRes.avatarFrame = opponentRes.user.avatarFrame;
                }
                else
                {
                    dtRes.dataDecode.users = new List<UserResponse>()
                    {
                        new UserResponse()
                        {
                            id = opponentRes.user.id,
                            name = opponentRes.user.name,
                            avatar = opponentRes.user.avatar
                        }
                    };
                }

                UpdateOpponentInfo(opponentRes.user.name, opponentRes.user.avatar, opponentRes.user.avatarFrame);
                if (_isInGame) UpdateOpponentInfoIngame(opponentRes);

                _userRx.OnNext(opponentRes.user);
            }
        );
    }

    protected override void UpdateOpponentInfoIngame(MatchUserSocketResponse opp)
    {
        AddJob(() =>
        {
            dataGame.isSync = true;
            dataGame.Opponent = new();
            dataGame.Opponent.Name = opp.user.name;
            dataGame.Opponent.Avt = opp.user.avatar;
            dataGame.Opponent.AvtFrame = opp.user.avatarFrame;

            oppnentUserInfo.gameObject.SetActive(true);
            oppnentUserInfo.UserName.text = dataGame.Opponent.Name;

            Current.Ins.ImageData.AvatarByUrl(dataGame.Opponent.Avt, (result) =>
            {
                oppnentUserInfo.UserAvt.texture = result.Texture;
            });
            //Current.Ins.ImageData.AvatarFrameByUrl(dataGame.Opponent.AvtFrame, (result) =>
            //{
            //    oppnentUserInfo.UserAvtFrame.texture = result.Texture;
            //});
            // 
        });
    }

    protected override void LoadGameContent(IResponse response)
    {
        result = new SolitaireScoreResult();

        base.LoadGameContent(response);

        var req = new CreateRoomByMiniGameRequest();

        if (response != null)
        {
            OnCallBackFromAPI(response);
            return;
        }

        GetUserSecret(() =>
        {
            switch (_startType)
            {
                case MiniGameStartTypeEnum.Init:
                    req.GameId = _gameId;
                    StartCoroutine(Current.Ins.gameAPI.CreateRoomByMiniGame<SolitaireDataResponse>(req, OnCallBackFromAPI));

                    break;
                case MiniGameStartTypeEnum.InitNextGame:
                    req.RoomId = _roomId;
                    StartCoroutine(Current.Ins.gameAPI.GetNextGame<SolitaireDataResponse>(req, OnCallBackFromAPI));

                    break;
                case MiniGameStartTypeEnum.InitReMatch:
                    req.GameId = _gameId;
                    StartCoroutine(Current.Ins.gameAPI.GetGameScript<SolitaireDataResponse>(req, OnCallBackFromAPI));
                    break;
            }
        });
    }

    protected override void OnCallBackAPIError(IResponse response)
    {
        SolitaireDataResponse res = response as SolitaireDataResponse;
        if (res != null && res.errorCode == 4)
        {
            popupSpecialNotice.SetActive(true);
        }
        else
        {
            base.OnCallBackAPIError(response);
        }
    }

    protected async override Task<bool> ParseFromAPI(IResponse response)
    {
        // For test
        makeTestData();

        var res = (SolitaireDataResponse)response;

        if (res == null || res.errorCode != 0)
        {
            Debug.Log($"{res?.message}");
            if (Application.isEditor)
            {
                makeTestData();
                return true;
            }

            return false;
        }

        res.dataDecode = JsonUtility.FromJson<SolitaireDataResponse.DataDecode>(EncryptHelper.DecryptWithAES256(res.data.info, sKey));
        gameInfoCards = res.dataDecode.gameInfo;

        var gameInfo = res.dataDecode.gameInfo;
        dataGame.gameId = res.dataDecode.gameId.ToString();
        dataGame.roomId = res.dataDecode.roomId.ToString();

        // Game Mode
        var gameMode = res.dataDecode.mode;
        dataGame.miniGameMode = gameMode;

        if (gameMode == MiniGameModeEnum.HeadToHead)
        {
            dataGame.isSync = res.dataDecode.isSync;
        }

        // totalPlayer
        dataGame.totalPlayer = res.dataDecode.quantityUser;

        // User and Opponent
        dataGame.User = new DataUIBase.Player();
        dataGame.User.Name = Current.Ins.player.Name;
        dataGame.User.Avt = Current.Ins.player.Avatar;
        dataGame.User.AvtFrame = Current.Ins.player.AvatarFrame;
        myUserInfo.UserName.text = StringHelper.ReplaceStringByDot(dataGame.User.Name, 10);               
        //getAvt
        Current.Ins.UserAvatar((result) =>
        {
            myUserInfo.UserAvt.texture = result.Texture;
        });

        //Current.Ins.UserAvatarFrame((result) =>
        //{
        //    myUserInfo.UserAvtFrame.texture = result.Texture;
        //});
        //

        UserResponse opponentInfo = null;
        switch (gameMode)
        {
            case MiniGameModeEnum.HeadToHead:
                opponentInfo = res.dataDecode.users.FirstOrDefault();

                if (opponentInfo != null)
                {
                    dataGame.Opponent = new DataUIBase.Player();
                    dataGame.Opponent.Name = opponentInfo.name;
                    dataGame.Opponent.Avt = opponentInfo.avatar;
                    dataGame.Opponent.AvtFrame = opponentInfo.avatarFrame;

                    oppnentUserInfo.gameObject.SetActive(true);
                    oppnentUserInfo.UserName.text = StringHelper.ReplaceStringByDot(dataGame.Opponent.Name, 10);
                    Current.Ins.ImageData.AvatarByUrl(dataGame.Opponent.Avt, (result) =>
                    {
                        oppnentUserInfo.UserAvt.texture = result.Texture;
                    });
                    //Current.Ins.ImageData.AvatarFrameByUrl(dataGame.Opponent.AvtFrame, (result) =>
                    //{
                    //    oppnentUserInfo.UserAvtFrame.texture = result.Texture;
                    //});

                }
                else
                {
                    oppnentUserInfo.gameObject.SetActive(false);
                }

                Current.Ins.socketAPI.AddSocket<LiveScoreSocketResponse>
                (
                    StringConst.SOCKET_LIVE_SCORE_EVENT,
                    $"{StringConst.SOCKET_LIVE_SCORE_EVENT}{dataGame.gameId}",
                    onSocketCallBackScore
                );

                break;

            case MiniGameModeEnum.OneToMany:
                oppnentUserInfo.gameObject.SetActive(false);
                break;

            case MiniGameModeEnum.KnockOut:
                opponentInfo = res.dataDecode.users.FirstOrDefault();

                if (!dataGame.isSync)
                {
                    oppnentUserInfo.gameObject.SetActive(false);
                }

                if (opponentInfo != null)
                {
                    dataGame.Opponent = new DataUIBase.Player();
                    dataGame.Opponent.Name = opponentInfo.name;
                    dataGame.Opponent.Avt = opponentInfo.avatar;
                    dataGame.Opponent.AvtFrame = opponentInfo.avatarFrame;
                    oppnentUserInfo.UserName.text = StringHelper.ReplaceStringByDot(dataGame.Opponent.Name, 10);
                    //getAvt
                    Current.Ins.ImageData.AvatarByUrl(dataGame.Opponent.Avt, (result) =>
                    {
                        oppnentUserInfo.UserAvt.texture = result.Texture;
                    });
                    //Current.Ins.ImageData.AvatarFrameByUrl(dataGame.Opponent.AvtFrame, (result) =>
                    //{
                    //    oppnentUserInfo.UserAvtFrame.texture = result.Texture;
                    //});
                }

                break;

            case MiniGameModeEnum.RoundRobin:
                oppnentUserInfo.gameObject.SetActive(false);
                break;
        }

        Current.Ins.socketAPI.AddSocket<ExitGameSocketResponse>
            (
                StringConst.SOCKET_EXIT_GAME_EVENT,
                $"{StringConst.SOCKET_EXIT_GAME_EVENT}{dataGame.gameId}",
                (exitGameRes) => {
                    dataGame.isOppoentInGame = false;
                });

        return await Task.Run(() => true);
    }

    protected override void LoadGamePlay()
    {
        //HCSound.Ins.PlayBackground(this.SolitaireBackgroundMusic);
        SoundManager.instance.PlayMusic("bgm_solitaire");

        base.LoadGamePlay();
        result.StartTimeMilliSecond = TimeManager.CurrentTimeInMilliSecond;
        InitRx();
    }

    protected override void OnResumeGame(double pauseTime)
    {
        if (!_isTimerRunning) return;

        float pTime = (float)pauseTime / 1000f;
        // Sound
        _isPlayingCountDownTimeSound = false;

        updateTimer(pTime);
    }

    /// <summary>
    /// Test Data
    /// </summary>
    private void makeTestData()
    {
        Debug.Log("make TestData");
    }

    public void GameFinished(GameFinishTypeEnum finishType)
    {

        SoundManager.instance.PlayMusic("bgm_result");
        SoundManager.instance.PlayUIEffect("se_useClock");

        //switch (finishType)
        //{
        //    case GameFinishTypeEnum.GameOver:
        //        HCSound.Ins.PlayGameSound3(HCSound.Ins.MiniGame.GameOver);
        //        break;
        //    case GameFinishTypeEnum.TimesUp:
        //        HCSound.Ins.PlayGameSound3(HCSound.Ins.MiniGame.TimeUp);
        //        break;
        //    case GameFinishTypeEnum.Success:
        //        HCSound.Ins.PlayGameSound3(HCSound.Ins.MiniGame.SuccessMission);
        //        break;
        //}

        result.EndTimeMilliSecond = result.StartTimeMilliSecond + MAX_TIME;

        if (oppnentUserInfo.gameObject.activeSelf)
        {
            int? lastScore = dataGame.OScore.Scores.LastOrDefault()?.Score;
            if (lastScore != null)
            {
                oppnentUserInfo.Score = lastScore.Value;
            }
        }

        pauseOrResumeTimer(false);
        DisposeAllRx();

        Action<SaveScoreGameResponse> afterSubmitCallBack = (res) =>
        {
            this.resultView.transform.DOScale(Vector3.one, 0.1f).SetEase(Ease.InOutBack);
            this.resultView.GetComponent<SolitaireUIResult>()
                    .submitBtn.GetComponent<Button>()
                        .onClickWithHCSound(() => 
                        {
                            this.resultView.transform.DOScale(Vector3.zero,0.1f).OnComplete(()=> this.matchResult.SetActive(true));
                        });

            switch (dataGame.miniGameMode)
            {
                case MiniGameModeEnum.HeadToHead:
                    var headToHeadData = new MatchHeadToHeadData();
                    headToHeadData.gameId = dataGame.gameId;
                    headToHeadData.isSync = dataGame.isSync;
                    headToHeadData.isOpponentInGame = dataGame.isOppoentInGame;
                    headToHeadData.roomId = dataGame.roomId;

                    if (dataGame.Opponent != null)
                    {
                        headToHeadData.Opponent = new MatchHeadToHeadData.Player();
                        headToHeadData.Opponent.Name = dataGame.Opponent.Name;
                        headToHeadData.Opponent.Avt = dataGame.Opponent.Avt;
                        headToHeadData.Opponent.AvtFrame = dataGame.Opponent.AvtFrame;
                    }

                    matchResult.GetComponent<UIMatchResult>().ShowHeadToHead(res, headToHeadData, sFeeData,
                        acceptReMatchCallBack : (gameId) =>
                        {
                            InitReMatchStaticVariable(gameId);
                            SceneHelper.ReloadScene();
                        },
                        newMatchCallBack: () =>
                        {
                            InitStaticVariable(MiniGameModeEnum.HeadToHead, _gameId, sFeeData);
                            SceneHelper.ReloadScene();
                        });

                    break;

                case MiniGameModeEnum.OneToMany:
                    var oneToManyData = new MatchOneToManyData();
                    oneToManyData.totalPlayer = dataGame.totalPlayer;
                    oneToManyData.gameId = dataGame.gameId;
                    oneToManyData.roomId = dataGame.roomId;

                    LoadOneToManyResult(res.dataDecode.status, dataGame.roomId, matchResult, oneToManyData, res);

                    break;

                case MiniGameModeEnum.KnockOut:
                    var knockOutdata = new KnockOutData();
                    knockOutdata.roomId = dataGame.roomId;
                    knockOutdata.gameId = dataGame.gameId;

                    if (dataGame.Opponent != null)
                    {
                        knockOutdata.Opponent = new KnockOutData.Player();
                        knockOutdata.Opponent.Name = dataGame.Opponent.Name;
                        knockOutdata.Opponent.Avt = dataGame.Opponent.Avt;
                        knockOutdata.Opponent.AvtFrame = dataGame.Opponent.AvtFrame;
                    }

                    matchResult.GetComponent<UIMatchResult>().ShowKnockOut(res, knockOutdata, () =>
                    {
                        InitNextGameStaticVariable(dataGame.roomId);
                        SceneHelper.ReloadScene();
                    });

                    break;
            }
        };
        //result.ScoreTimeBonusSolitaire = this.ConvertTimeToSeconds();
        if(finishType == GameFinishTypeEnum.TimesUp) 
        {
            result.ScoreTimeBonusSolitaire = 0;
        }
        LoadResult();
        resultView
            .GetComponent<SolitaireUIResult>()
            .Init(result, finishType, dataGame.gameId, sKey, afterSubmitCallBack);
    }
    private Subject<ObscuredInt> _addScoreRx = new Subject<ObscuredInt>();

    private IDisposable increaseScoreRx()
    {
        return _addScoreRx
            .Scan((pre, curr) =>
            {
                int nextScore = pre + curr;
                result.ScoreSolitaire = nextScore < 0 ? 0 : nextScore;
                float timeRemain = (float)this.ConvertTimeToSeconds() / MAX_TIME;
                float scoreTimeBonus = nextScore * timeRemain;
                result.ScoreTimeBonusSolitaire = (int)scoreTimeBonus;
                return nextScore < 0 ? 0 : nextScore;
            })
            .Subscribe(score =>
            {
                if (this.type == MiniGameStartTypeEnum.Tutorial) return;
                if (string.IsNullOrWhiteSpace(sKey)) return;

                var req = new SaveScoreGameRequest();
                req.key = sKey;
                req.time = _currentTime;
                req.score = score;
                req.scoreInfo = new SaveScoreGameRequest.ScoreInfo[2];
                req.scoreInfo[0] = new SaveScoreGameRequest.ScoreInfo()
                {
                    name = StringConst.SOLITAIRE_SCORE_INFO_SCORE,
                    quantity = 0,
                    score = score
                };

                float timeRemain = (float)this.ConvertTimeToSeconds() / MAX_TIME;
                float scoreTimeBonus = score * timeRemain;
                req.scoreInfo[1] = new SaveScoreGameRequest.ScoreInfo()
                {
                    name = StringConst.SOLITAIRE_SCORE_INFO_TIMEBONUS,
                    quantity = 0,
                    score = (int)scoreTimeBonus
                };

                StartCoroutine(Current.Ins.gameAPI.SaveLiveScoreGame<SaveScoreGameResponse>(dataGame.gameId, req, (res) => { }));
            });
    }

    public ObscuredInt GetScoreSolitaire()
    {
        return result.ScoreSolitaire;
    }
    public void updateScore(ObscuredInt score)
    {
        _addScoreRx.OnNext(score);
    }

    private void onSocketCallBackScore(LiveScoreSocketResponse uLiveScore)
    {
        if (uLiveScore.userId.ToString() == Current.Ins.player.Id) return;

        var oTime = uLiveScore.time;

        if (_currentTime - oTime < -2000f)
        {
            DataUIBase.OpponentScore.ScoreData add = new()
            {
                Time = uLiveScore.time,
                Score = uLiveScore.score
            };

            if (dataGame.OScore == null) dataGame.OScore = new();
            dataGame.OScore.Scores.Add(add);
        }
        else
        {
            AddJob(() => oppnentUserInfo.Score = uLiveScore.score);
        }
    }

    private IDisposable timerRx()
    {
        return _timerRx
            .Scan((pre, curr) =>
            {
                if (this.type == MiniGameStartTypeEnum.Tutorial) return 300;
                if (_gameState.State.Value != SolitaireGame.State.Playing) return pre;

                float nextTime = pre - curr;
                return nextTime <= 0 ? 0 : nextTime;
            })
            .Select(timer =>
            {
                if (timer <= 10)
                {
                    if (!_isPlayingCountDownTimeSound)
                    {
                        _isPlayingCountDownTimeSound = true;
                        //HCSound.Ins.PlayGameSound2(HCSound.Ins.MiniGame.TimeCountDown, 10f - timer);
                        SoundManager.instance.PlayUIEffect("se_clock01");
                    }
                    if (timeView && (int)timer > 0)
                    {
                        timeView.SetCountDownText(((int)timer).ToString());
                        
                    }
                }

                return timer == 0
                ? string.Empty
                : (TimeSpan.FromSeconds(timer)).ToString(@"mm\:ss");
            })
            .DistinctUntilChanged()
            .Subscribe(text =>
            {
                timeView?.SetTimeTxt(string.IsNullOrEmpty(text) ? "00:00" : text);

                if (string.IsNullOrEmpty(text))
                {
                    if (_gameState.State.Value == SolitaireGame.State.Win) return;
                    GameFinished(GameFinishTypeEnum.TimesUp);
                }
                else
                {
                    if (dataGame.OScore == null) return;

                    var oScore = dataGame.OScore.Scores
                        .Where(x =>
                        {
                            var xScore = x.Time > 600000 ? "-1" : (TimeSpan.FromSeconds(MAX_TIME - x.Time / 1000f)).ToString(@"mm\:ss");
                            return text == xScore;
                        })
                        .LastOrDefault();

                    if (oScore != null) oppnentUserInfo.Score = oScore.Score;
                }
            });
    }
     
    public int ConvertTimeToSeconds()
    {
        string timeText = timeView.timeTxt.text;
        string[] timeComponents = timeText.Split(':');
        if (timeComponents.Length != 2)
        {
            Debug.LogError("Invalid time format. Expected format: 00:00");
            return 0;
        }

        int minutes, seconds;
        if (!int.TryParse(timeComponents[0], out minutes) || !int.TryParse(timeComponents[1], out seconds))
        {
            Debug.LogError("Invalid time format. Expected format: 00:00");
            return 0;
        }

        int totalSeconds = minutes * 60 + seconds;
        return totalSeconds;
    }

    private IDisposable StartTimer()
    {
        return Observable
            .Interval(TimeSpan.FromSeconds(1))
            .Subscribe(x =>
            {
                if (_isTimerRunning)
                {
                    updateTimer(1);
                }
            });
    }
    public void updateTimer(float time)
    {
        _timerRx.OnNext(time);
    }

    //
    protected override void LoadTutorialGameContent()
    {
        result = new();
        base.LoadTutorialGameContent();
        MakeTutorialData();
        AfterMakeTutorialData();
    }

    private void MakeTutorialData()
    {
        myUserInfo.UserName.text = Current.Ins.player.Name;
        //getAvt
        Current.Ins.UserAvatar((result) =>
        {
            myUserInfo.UserAvt.texture = result.Texture;
        });

        //Current.Ins.UserAvatarFrame((result) =>
        //{
        //    myUserInfo.UserAvtFrame.texture = result.Texture;
        //});
        //
        oppnentUserInfo.gameObject.SetActive(false);    
    }

    public void InstantiateSkipTut()
    {
        GameObject a = Instantiate(skipTut, this.transform);
        a.transform.position = oppnentUserInfo.transform.position;
        a.transform.localScale = Vector3.zero;
        a.GetComponent<Button>().onClick.AddListener(() => { endTutorialPopup.SetActive(true); });
        a.transform.DOScale(Vector3.one, 0.5f).SetEase(Ease.OutElastic);
    }

    public void ShowScoreEffect(int score, Vector3 pos)
    {
        myUserInfo.ShowPointTailEffect(score, pos);

        GameObject obj = ObjectPoolManager.instance.GetObject(ScoreTextObj.name, true, true);
        if (obj)
        {
            TextMeshProUGUI tMesh = obj.GetComponent<TextMeshProUGUI>();
            if (tMesh)
            {
                if (score >= 0)
                {
                    tMesh.text = $"+{Mathf.Abs(score)}";
                    tMesh.color = new Color(1.0f, 0.9424f, 0.0f, 1.0f);
                }
                else
                {
                    tMesh.text = $"-{Mathf.Abs(score)}";
                    tMesh.color = new Color(1.0f, 0.3454f, 0.0f, 1.0f);
                }
            
            }


            obj.transform.localScale = Vector3.one;
            obj.transform.position = pos;


            Sequence sq = DOTween.Sequence();
            sq.Join(obj.transform.DOScale(1.4f, 0.3f));
            sq.Append(obj.transform.DOScale(0.95f, 0.25f));
            sq.Append(obj.transform.DOScale(1.0f, 0.1f));
            sq.AppendInterval(0.5f);
            sq.Append(obj.transform.DOScale(1.1f, 0.1f));
            sq.Append(obj.transform.DOScale(0.0f, 0.25f));

            sq.Play().OnComplete(() => 
            {
                obj.SetActive(false);
            });

        }

    }
}
