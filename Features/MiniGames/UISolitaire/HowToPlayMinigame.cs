using DanielLochner.Assets.SimpleScrollSnap;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class HowToPlayMinigame : MonoBehaviour
{
    public GameObject scrollSnap;
    public GameObject nextBtn;
    public GameObject doneBtn;
    public GameObject closeBtn;

    public GameObject blackShadow;

    //public GameObject[] Titles;

    public Action OnClose;

    public void InitTut()
    {
        scrollSnap.GetComponent<SimpleScrollSnap>().OnPanelCentered.AddListener((current, before) =>
        {
            int totalPanels = scrollSnap.GetComponent<SimpleScrollSnap>().Panels.Length;
            bool isShowNextBtn = current < totalPanels - 1;
            nextBtn.SetActive(isShowNextBtn);
            doneBtn.SetActive(!isShowNextBtn);

            //Titles.ToList().ForEach(t => t.SetActive(false));
            //Titles[current].SetActive(true);
        });

        doneBtn.GetComponent<Button>().onClick.AddListener(() =>
        {
            scrollSnap.GetComponent<SimpleScrollSnap>().GoToPanel(0);
            blackShadow.SetActive(false);
            gameObject.SetActive(false);

            OnClose?.Invoke();
        });

        closeBtn.GetComponent<Button>().onClick.AddListener(() => doneBtn.GetComponent<Button>().onClick.Invoke());

        blackShadow.SetActive(true);
    }
}
