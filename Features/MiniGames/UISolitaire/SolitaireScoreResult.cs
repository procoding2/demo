using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeStage.AntiCheat.ObscuredTypes;

public class SolitaireScoreResult
{
    public ObscuredInt ScoreSolitaire;
    public ObscuredInt ScoreTimeBonusSolitaire;

    public double StartTimeMilliSecond { get; set; }
    public double EndTimeMilliSecond { get; set; }
}
