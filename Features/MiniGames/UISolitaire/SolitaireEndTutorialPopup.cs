using OutGameEnum;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SolitaireEndTutorialPopup : MonoBehaviour
{
    public GameObject tryAgainEndTutorialPopup;
    public GameObject playNowEndTutorialPopup;

    private void Awake()
    {
        tryAgainEndTutorialPopup.GetComponent<Button>().onClick.RemoveAllListeners();
        tryAgainEndTutorialPopup.GetComponent<Button>().onClickWithHCSound(() => SceneHelper.ReloadScene());

        playNowEndTutorialPopup.GetComponent<Button>().onClick.RemoveAllListeners();
        playNowEndTutorialPopup.GetComponent<Button>().onClickWithHCSound(() => SceneHelper.LoadScene(SceneEnum.Home));
    }
}
