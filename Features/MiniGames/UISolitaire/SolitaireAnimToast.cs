using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolitaireAnimToast : MonoBehaviour
{
    [SerializeField] UISolitaire uISolitaire;
    private void OnEnable()
    {
        if (uISolitaire.type == GameEnum.MiniGameStartTypeEnum.Tutorial && this.gameObject.name != "GoodJob")
        {
            this.gameObject.SetActive(false);
            return; 
        }
        transform.localScale = Vector3.zero;
        gameObject.transform
            .DOScale(Vector3.one, 0.5f)
            .OnComplete(()=> {
                gameObject.transform
                .DOScale(Vector3.zero, 0.3f)
                .OnComplete(()=>gameObject.SetActive(false));
                });
    }
}
