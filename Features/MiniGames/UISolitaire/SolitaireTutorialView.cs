using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Solitaire.Models;
using Zenject;

public class SolitaireTutorialView : MonoBehaviour
{
    [SerializeField] UISolitaire uISolitaire;
    [Header("GameObject")]
    [SerializeField] GameObject FoundationHighlight;
    [SerializeField] GameObject StackWasteHighlight;
    [SerializeField] GameObject TableauHighlight;
    [SerializeField] GameObject CardTut;
    [SerializeField] GameObject TutPlay;
    [SerializeField] GameObject HandTut;
    [Header("GameObject Card")]
    [SerializeField] GameObject Queen;
    [SerializeField] GameObject King;

    [Header("RectTransform")]
    [SerializeField] RectTransform Character;
    [SerializeField] RectTransform TutShasow;
    [SerializeField] float timeDotween;
    [Header("Button")]
    [SerializeField] Button TapToContinue;
    //
    [Inject] readonly GameControls _gameControls;

    public void StartTutorial()
    {
        //
        Queen = GameObject.Find("Card_Spade_Queen");
        King = GameObject.Find("Card_Heart_King");
        Vector2 targetSize = new(540, 897);
        Character.DOSizeDelta(targetSize, timeDotween)
            .OnComplete(() =>
            {
                TutShasow.DOScale(Vector3.one, timeDotween)
                .OnComplete(() =>
                {
                    TableauHighlight.transform.DOScale(Vector3.one, timeDotween);
                    TapToContinue.onClick.AddListener(TutorialStep1);
                    uISolitaire.InstantiateSkipTut();
                });
            });            
    }

    void TutorialStep1()
    {
        TapToContinue.onClick.RemoveAllListeners();
        TableauHighlight.transform.DOScale(Vector3.zero, timeDotween)
            .OnComplete(() =>
            {
            FoundationHighlight.transform.DOScale(Vector3.one, timeDotween);
            TapToContinue.onClick.AddListener(TutorialStep2);
            });
    }

    void TutorialStep2()
    {
        TapToContinue.onClick.RemoveAllListeners();
        FoundationHighlight.transform.DOScale(Vector3.zero, timeDotween)
            .OnComplete(() =>
            {
                StackWasteHighlight.transform.DOScale(Vector3.one, timeDotween);
                TapToContinue.onClick.AddListener(TutorialStep3);
            });              
    }


    Tween a;
    void TutorialStep3()
    {
        TapToContinue.onClick.RemoveAllListeners();
        StackWasteHighlight.transform.DOScale(Vector3.zero, timeDotween)
            .OnComplete(() =>
            {
                CardTut.transform.DOScale(Vector3.one, timeDotween);
                TutPlay.transform.DOScale(Vector3.one, timeDotween);
                HandTut.transform.position = Queen.transform.position;
                HandTut.SetActive(true);
                HandTut.transform.DOScale(Vector3.one*80, timeDotween).OnComplete(() =>
                {
                    a = HandTut.transform.DOMove(King.transform.position, 0.75f)
                        .SetLoops(-1, LoopType.Restart)
                        .OnStepComplete(() => _gameControls.HintCommand.Execute());
                });
                
                DOVirtual.DelayedCall(1f, delegate
                {
                    TapToContinue.onClick.AddListener(TutorialStep4);
                });
            });      
    }

    void TutorialStep4()
    {
        TutShasow.DOScale(Vector3.zero, 0.3f);
        TutPlay.transform.DOScale(Vector3.zero, 0.3f);
        CardTut.transform.DOScale(Vector3.zero, 0.3f);
        a.Kill();
        HandTut.transform.DOScale(Vector3.zero, 0.3f)
            .OnComplete(()=> 
            {
                SolitaireGamePresenter.instnce.EnabledCardPresenter();             
            });
        TapToContinue.onClick.RemoveAllListeners();
    }
}
