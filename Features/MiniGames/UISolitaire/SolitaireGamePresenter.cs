﻿using Solitaire.Models;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;
using Solitaire.Presenters;
using System.Collections;
using static SolitaireDataResponse;
using DG.Tweening;
using Cysharp.Threading.Tasks;
using CodeStage.AntiCheat.ObscuredTypes;
using System.ComponentModel;


//namespace Solitaire.Presenters
//{
public class SolitaireGamePresenter : MonoBehaviour
{
    public static SolitaireGamePresenter instnce;
    public SolitaireDataResponse.GameInfo gameInfoCards;
    [SerializeField] Physics2DRaycaster _cardRaycaster;
    [SerializeField] PilePresenter _pileStock;
    [SerializeField] PilePresenter _pileWaste;
    [SerializeField] PilePresenter[] _pileFoundations;
    [SerializeField] PilePresenter[] _pileTableaus;

    [Inject] readonly SolitaireGame _game;
    [Inject] readonly SolitaireGame.Config _gameConfig;
    [Inject] readonly GameState _gameState;
    [Inject] readonly OrientationState _orientation;
    //
    ObscuredBool gameWin = false;
    ObscuredBool runTutorial = false;
    IList<Card> cardsBeforShuffle;

    private UISolitaire uISolitaire;

    Camera _camera;
    int _layerInteractable;

    //
    public SolitaireTutorialView TutView;
    public GameObject EndTutorialPopup;

    private void Awake()
    {
        instnce = this;
        _camera = Camera.main;
        _layerInteractable = LayerMask.NameToLayer("Interactable");
        uISolitaire = GameObject.Find("UISolitaire").GetComponent<UISolitaire>();
        this.gameInfoCards.cards = new();
        //
        if(uISolitaire.type == GameEnum.MiniGameStartTypeEnum.Tutorial)
        {
            BuildDeckForTut();
            GamePlaySolitaire.Instance.CheckTut = true;
        }
        else
        {
            this.gameInfoCards = uISolitaire.gameInfoCards;
        }
        // Update camera on orientation change
        _orientation.State.Subscribe(AdjustCamera).AddTo(this);
    }

    private void Start()
    {
        // Handle game state change
        _gameState.State.Pairwise().Subscribe(HandleGameStateChanges).AddTo(this);
        // Initialize game
        _game.Init(_pileStock.Pile, _pileWaste.Pile,
        _pileFoundations.Select(p => p.Pile).ToList(),
        _pileTableaus.Select(p => p.Pile).ToList());

        _game._PlaySoundEffectDelegate = PlayEffectSound;
        //_game.NewMatch();
        if (this.gameInfoCards.cards == null)
        {
            _game.NewMatch();
        }
        else
        {
            this.cardsBeforShuffle = _game.Cards;
            _game.Reset();
            //BuildDeckForTut();
            ShuffleCardsOnAPI2();
            _game.DealAsync_Forget();
        }
        GamePlaySolitaire.Instance.AddListCardPool();
        SetCameraLayers(true);

        if(uISolitaire.type == GameEnum.MiniGameStartTypeEnum.Tutorial)
        {
            GamePlaySolitaire.Instance.SetActiveCard(false);
        }

        Current.Ins.PropertyChanged += OnChangeProperty;
    }

    private void OnChangeProperty(object sender, PropertyChangedEventArgs e)
    {
        if (e.PropertyName == StringConst.PROPERTY_ANTICHEAT)
        {
            _game.RandomizeCryptoKey();
            _gameConfig.RandomizeCryptoKey();
            gameWin.RandomizeCryptoKey();
            runTutorial.RandomizeCryptoKey();
        }
    }

    private void Update()
    {
        if (_gameState.State.Value == SolitaireGame.State.Playing && this.runTutorial == false && uISolitaire.type == GameEnum.MiniGameStartTypeEnum.Tutorial)
        {
            this.runTutorial = true;
            this.RunTut();
        }
        // Detect win condition
        if (_gameState.State.Value == SolitaireGame.State.Playing )
        {
            _game.DetectWinCondition();
        }
    }

    public void WinTheGame()
    {
        if (uISolitaire.type == GameEnum.MiniGameStartTypeEnum.Tutorial) 
        {
            StartCoroutine(WinTheGameTut());
            return;
        }
        DOVirtual.DelayedCall(2f, delegate
        {
            uISolitaire.GameFinished(GameEnum.GameFinishTypeEnum.Success);
        });
        
    }

    public IEnumerator WinTheGameTut()
    {
        GamePlaySolitaire.Instance.GoodJob.SetActive(true);
        yield return new WaitForSeconds(2f);
        EndTutorialPopup.SetActive(true);
    }

    private void AdjustCamera(Orientation orientation)
    {
        _camera.orthographicSize = 10.25f;

        float screen = (float)Screen.height / (float)Screen.width;
        screen = screen / 1f;
        if (Mathf.Approximately(Mathf.Round(screen * 10f) / 10f, 1.8f))
        {
            _camera.orthographicSize = 8.5f;
            //Debug.Log("16:9");

        }
        else if (Mathf.Approximately(Mathf.Round(screen * 10f) / 10f, 2.0f))
        {
            _camera.orthographicSize = 9.5f;
            //Debug.Log("18:9");
        }

        else if (Mathf.Approximately(Mathf.Round(screen * 10f) / 10f, 2.1f))
        {
            _camera.orthographicSize = 10f;
            //Debug.Log("19:9");
        }

        else if (Mathf.Approximately(Mathf.Round(screen * 10f) / 10f, 2.2f))
        {
            _camera.orthographicSize = 10.25f;
            //Debug.Log("19:9");
        }
    }

    private void HandleGameStateChanges(Pair<SolitaireGame.State> state)
    {
        if (state.Previous == SolitaireGame.State.Home)
        {
            // Render everything and play music
            SetCameraLayers(false);
        }
        else if (state.Current == SolitaireGame.State.Home)
        {
            // Cull game elements and stop music
            SetCameraLayers(true);
        }

        // Enable card interactions only while playing
        _cardRaycaster.enabled = state.Current == SolitaireGame.State.Playing;
    }

    private void SetCameraLayers(bool cullGame)
    {
        if (cullGame)
        {
            // Every layer except Interactable
            _camera.cullingMask = ~(1 << _layerInteractable);
        }
        else
        {
            // Everything
            _camera.cullingMask = ~0;
        }
    }

    void ShuffleCardsOnAPI2()
    {      
        for (int i = 0; i < gameInfoCards.cards.Count; i++)
        {
            int n = GetPosCardOnDeck(gameInfoCards.cards[i].type, gameInfoCards.cards[i].card);
            Card temp = _game.Cards[i];
            _game.Cards[i] = _game.Cards[n];
            _game.Cards[n] = temp;
        }
    }

    int GetPosCardOnDeck(int suit, int type)
    {
        for(int i = 0;i< cardsBeforShuffle.Count;i++)
        {
            if (cardsBeforShuffle[i].ToString() == GetDeckOnAPI(suit, type))
            {
                return i;
            }
        }
        return -1;
    }
    string GetDeckOnAPI(int suit, int type)
    {
        string txtSuit = "";
        string txtType = "";
        switch (suit)
        {
            case 1:
                txtSuit = "Heart";
                break;
            case 2:
                txtSuit = "Diamond";
                break;
            case 3:
                txtSuit = "Club";
                break;
            case 4:
                txtSuit = "Spade";
                break;
        }

        switch (type)
        {
            case 1:
                txtType = "Ace";
                break;
            case 2:
                txtType = "Two";
                break;
            case 3:
                txtType = "Three";
                break;
            case 4:
                txtType = "Four";
                break;
            case 5:
                txtType = "Five";
                break;
            case 6:
                txtType = "Six";
                break;
            case 7:
                txtType = "Seven";
                break;
            case 8:
                txtType = "Eight";
                break;
            case 9:
                txtType = "Nine";
                break;
            case 10:
                txtType = "Ten";
                break;
            case 11:
                txtType = "Jack";
                break;
            case 12:
                txtType = "Queen";
                break;
            case 13:
                txtType = "King";
                break;
        }

        return txtSuit + " " + txtType;
    }

    //tut
    public void BuildDeckForTut()
    {
        gameInfoCards.cards.Clear();
        // Từ lá 1 đến 3 lần lượt là: J, Q, K Diamond
        for (int i = 1; i <= 3; i++)
        {
            gameInfoCards.cards.Add(new CardInfo { card = i + 10, type = 2 });
        }

        // Từ lá 4 đến 6 lần lượt là: 8, 9, 10 Diamond
        for (int i = 8; i <= 10; i++)
        {
            gameInfoCards.cards.Add(new CardInfo { card = i, type = 2 });
        }

        // Từ lá 7 đến 9 lần lượt là: 5, 6, 7 Diamond
        for (int i = 5; i <= 7; i++)
        {
            gameInfoCards.cards.Add(new CardInfo { card = i, type = 2 });
        }

        // Từ lá 10 đến 12 lần lượt là: 2, 3, 4 Diamond
        for (int i = 2; i <= 4; i++)
        {
            gameInfoCards.cards.Add(new CardInfo { card = i, type = 2 });
        }

        // Từ lá 13 đến 15 lần lượt là: 9, 10 Spade và Ace Diamond
        gameInfoCards.cards.Add(new CardInfo { card = 9, type = 4 });
        gameInfoCards.cards.Add(new CardInfo { card = 10, type = 4 });
        gameInfoCards.cards.Add(new CardInfo { card = 1, type = 2 });

        // Từ lá 16 đến 18 lần lượt là: 6, 7, 8 Spade
        for (int i = 6; i <= 8; i++)
        {
            gameInfoCards.cards.Add(new CardInfo { card = i, type = 4 });
        }

        // Từ lá 19 đến 21 lần lượt là: 3, 4, 5 Spade
        for (int i = 3; i <= 5; i++)
        {
            gameInfoCards.cards.Add(new CardInfo { card = i, type = 4 });
        }

        // Từ lá 22 đến 24 lần lượt là: K Clubs, Ace Spade và 2 Spade
        gameInfoCards.cards.Add(new CardInfo { card = 13, type = 3 });
        gameInfoCards.cards.Add(new CardInfo { card = 1, type = 4 });
        gameInfoCards.cards.Add(new CardInfo { card = 2, type = 4 });

        // Từ lá 25 đến 31 lần lượt là: 6, 7, 8, 9, 10, Jack, Queen Clubs
        for (int i = 6; i <= 12; i++)
        {
            gameInfoCards.cards.Add(new CardInfo { card = i, type = 3 });
        }

        // Từ lá 32 đến 37 lần lượt là: 2, 3, 4, 5 Clubs và Queen, King Spade
        for (int i = 2; i <= 5; i++)
        {
            gameInfoCards.cards.Add(new CardInfo { card = i, type = 3 });
        }
        gameInfoCards.cards.Add(new CardInfo { card = 12, type = 4 });
        gameInfoCards.cards.Add(new CardInfo { card = 13, type = 4 });

        // Từ lá 38 đến 42 lần lượt là: 10, Jack, Queen Heart và Ace Clubs, King Heart
        for (int i = 10; i <= 12; i++)
        {
            gameInfoCards.cards.Add(new CardInfo { card = i, type = 1 });
        }
        gameInfoCards.cards.Add(new CardInfo { card = 1, type = 3 });
        gameInfoCards.cards.Add(new CardInfo { card = 13, type = 1 });

        // Từ lá 43 đến 46 lần lượt là: 6, 7, 8, 9 Heart
        for (int i = 6; i <= 9; i++)
        {
            gameInfoCards.cards.Add(new CardInfo { card = i, type = 1 });
        }

        // Từ lá 47 đến 49 lần lượt là: 3, 4, 5 Heart
        for (int i = 3; i <= 5; i++)
        {
            gameInfoCards.cards.Add(new CardInfo { card = i, type = 1 });
        }

        // Từ lá 50 đến 51 lần lượt là: Ace, 2 Heart
        gameInfoCards.cards.Add(new CardInfo { card = 1, type = 1 });
        gameInfoCards.cards.Add(new CardInfo { card = 2, type = 1 });

        // Lá 52 là: Jack Spade
        gameInfoCards.cards.Add(new CardInfo { card = 11, type = 4 });

        //this.gameInfoCards = deck;
    }
    public void RunTut()
    {
        _game.onTutorial = true;
        for (int i = 0; i < _game.PileTableaus.Count; i++)
        {
            for (int j = 0; j <= i; j++)
            {
                Card card = _game.PileTableaus[i].TopCard();
                if (_game.PileTableaus[i].TopCard() == null ||
                   _game.PileTableaus[i].TopCard().ToString() == "Spade Jack" ||
                   _game.PileTableaus[i].TopCard().ToString() == "Heart King" ||
                   _game.PileTableaus[i].TopCard().ToString() == "Spade Queen")
                {
                    break;
                }
                _game.MoveCard(card, null, card.objCard);
            }
        }
        MoveCardOnStock();
    }
    void MoveCardOnStock()
    {
        for (int i = 0; i < 8; i++)
        {
            _game.DrawCard(null);
            for (int j = 0; j < 3; j++)
            {
                Card card = GamePlaySolitaire.Instance.LstCardRemove[^1].GetComponent<CardPresenter>().Card;
                _game.MoveCard(card, null, card.objCard);
            }
        }
        GamePlaySolitaire.Instance._buttonUndo.transform.DOScale(Vector3.zero,0.2f)
            .OnComplete(()=> 
            { 
                GamePlaySolitaire.Instance._buttonUndo.gameObject.SetActive(false);
                GamePlaySolitaire.Instance.SetActiveCard(true);
            });
        StartCoroutine(ShowCardHide());
    }
    private IEnumerator ShowCardHide()
    {
        yield return new WaitForSeconds(0.3f);
        GameObject KingDimond, KingClup, TenSpade;
        KingDimond = GamePlaySolitaire.Instance.LstCardPool[0];
        KingClup = GamePlaySolitaire.Instance.LstCardPool[26];
        TenSpade = GamePlaySolitaire.Instance.LstCardPool[42];

        KingDimond.GetComponent<CardPresenter>().AnimateMove(KingDimond.transform.position);
        KingClup.GetComponent<CardPresenter>().AnimateMove(KingClup.transform.position);
        TenSpade.GetComponent<CardPresenter>().AnimateMove(TenSpade.transform.position);

        foreach (GameObject a in GamePlaySolitaire.Instance.LstCardPool)
        {
            a.GetComponent<CardPresenter>().enabled = false;
        }

        TutView.StartTutorial();
    }

    public void EnabledCardPresenter()
    {
        foreach (GameObject a in GamePlaySolitaire.Instance.LstCardPool)
        {
            a.GetComponent<CardPresenter>().enabled = true;
        }
    }
    public void PlayEffectSound(string fileName)
    {
        SoundManager.instance.PlayUIEffect(fileName);
    }

    private void OnDestroy()
    {
        Current.Ins.PropertyChanged -= OnChangeProperty;
    }
}
