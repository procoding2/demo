using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using GameEnum;


public enum ThrowActionType
{
    circle,
    box,
}
public class CoinThrowTable : MonoBehaviour
{
    [SerializeField]
    private GameObject chipObj;

    [SerializeField]
    private Transform centerPos;

    [SerializeField]
    private ThrowActionType actionType = ThrowActionType.circle; 


    private int curPoint = 0, maxPoint = 0;

    [SerializeField]
    private UserInfo myUserInfo;
    // Start is called before the first frame update
    void Start()
    {
        if (myUserInfo)
        {
            curPoint = myUserInfo.Score;
        }
    }

    // Update is called once per frame
    void Update()
    {

        if ((UISolitaire.Ins as UISolitaire)?.type == MiniGameStartTypeEnum.Tutorial)
            return;

        if ((UIBingo.Ins as UIBingo) != null && (UIBingo.Ins as UIBingo).IsTutorial)
            return;
        

        if (myUserInfo && myUserInfo.Score != curPoint)
        {

            if (myUserInfo.Score > maxPoint)
            {
                ThrowCoinChip();
            }
            curPoint = myUserInfo.Score;

            if (maxPoint < curPoint)
                maxPoint = curPoint;


        }
    }


    public void ThrowCoinChip()
    {
        int count = Random.Range(1, 3);

        for (int i = 0; i < count; i++)
        {
            GameObject coin = Instantiate(chipObj);
            coin.transform.parent = transform;
            coin.transform.localScale = Vector3.one;



            if (actionType == ThrowActionType.box)
                coin.transform.position = new Vector2(centerPos.position.x, centerPos.position.y - 20.0f);
            else
                coin.transform.position = GetRandomGenPos(10.0f);

            coin.transform.localEulerAngles = new Vector3(0, 0, Random.Range(0, 350));
            Sequence sq = DOTween.Sequence();

            Vector2 destPos = Vector2.zero;

            if (actionType == ThrowActionType.box)
                destPos = (Vector2)centerPos.position + new Vector2(Random.Range(-1.6f, 1.6f), Random.Range(-0.5f, 0.5f));
            else
                destPos = GetRandomDestnationPos(2.0f);


            float time = Random.Range(0.8f, 0.9f);
            sq.Join(coin.transform.DOBlendableLocalRotateBy(new Vector3(0, 0, Random.Range(-350, 350)), time)).SetEase(Ease.OutQuint);
            sq.Join(coin.transform.DOMove(destPos, time)).SetEase(Ease.OutQuint);


            sq.Play();
        }

        SoundManager.instance.PlayUIEffect("se_betting");
    }


    private Vector2 GetRandomGenPos(float radius)
    {

        Vector2 pos = Vector2.zero;
        do
        {
            pos = Random.insideUnitCircle.normalized * radius;
        }
        while (pos.y > centerPos.position.y);
        

        return pos;
    }


    public Vector2 GetRandomDestnationPos(float radius)
    {
        return (Vector2)centerPos.position + Random.insideUnitCircle * radius;
    }
}
