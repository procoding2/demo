using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemDummyTable : ManagerSingleton<GemDummyTable>
{
    [SerializeField]
    private GameObject gemItemPrefabs;

    private void Start()
    {
    }



    public void GenerateGemItem(Vector3 pos, int count = 1)
    {
        for (int i = 0; i < count; i++)
        {

            GameObject obj = Instantiate(gemItemPrefabs);
            if (obj)
            {
                obj.transform.position = pos;
                obj.transform.parent = transform;
            }
        }
    }

}
