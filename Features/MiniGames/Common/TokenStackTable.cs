using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TokenStackTable : MonoBehaviour
{

    [SerializeField]
    private UserInfo myUserInfo;

    [SerializeField]
    private List<TokenDummy> tokenDummyList = new List<TokenDummy>();


    public int curPoint = 0;
    // Start is called before the first frame update
    void Start()
    {
        if (myUserInfo)
        {
            curPoint = myUserInfo.Score;
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (myUserInfo && myUserInfo.Score != curPoint)
        {

            if (myUserInfo.Score > curPoint)
            {
                StackToken();
            }
            curPoint = myUserInfo.Score;

        }

    }


    public void StackToken()
    {

        int retryCount = 0;
        while (retryCount < 10)
        {
            TokenDummy obj = tokenDummyList[Random.Range(0, tokenDummyList.Count - 1)];
            if (obj.IsMaxStack == false)
            {
                obj.StackTokenProcess();
                break;
            }

            retryCount++;
        }
    }
}
