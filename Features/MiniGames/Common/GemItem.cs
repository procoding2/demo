using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GemItem : MonoBehaviour
{

    [SerializeField]
    private List<Sprite> gemImageList = new List<Sprite>();

    private Image gemImage => GetComponent<Image>();

    private Rigidbody2D rigid => GetComponent<Rigidbody2D>();

    float forceValue = 1.0f, rightValue = 0.0f;
    [Header("ScaleSize")]
    [SerializeField, Range(0.0f, 5.0f)]
    private float minSize = 0.5f;

    [SerializeField, Range(0.0f, 5.0f)]
    private float maxSize = 1.0f;



    // Update is called once per frame
    void Update()
    {
        if (transform.position.y < -2000)
        {
            Destroy(gameObject);
        }
    }
    private void Start()
    {

        forceValue = Random.Range(1.0f, 2.0f);
        rightValue = Random.Range(0.01f, 0.05f);

        if (Random.Range(0, 2) == 0)
            rightValue *= -1;

        if (gemImageList.Count > 0)
        {
            gemImage.sprite = gemImageList[Random.Range(0, gemImageList.Count)];
            transform.localScale = Vector3.one * Random.Range(minSize, maxSize);
            transform.rotation = Quaternion.Euler(0.0f, 0.0f, Random.Range(0, 360));


        }

    }


    private void FixedUpdate()
    {
        rigid.AddForce((transform.up * forceValue) + (transform.right * rightValue)); ;
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        SoundManager.instance.PlayEffect($"se_type{Random.Range(2,5)}");
    }
}
