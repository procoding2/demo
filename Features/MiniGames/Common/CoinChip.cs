using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinChip : MonoBehaviour
{

    private Image chipImage => GetComponent<Image>();


    [SerializeField]
    private Sprite[] chipSprList;

    // Start is called before the first frame update
    void Start()
    {
        chipImage.sprite = chipSprList[Random.Range(0, chipSprList.Length)];
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
