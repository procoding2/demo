using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using DG.Tweening;

public class TokenDummy : MonoBehaviour
{


    [SerializeField]
    private Transform botPos;
    [SerializeField]
    private Transform genPos;

    [SerializeField]
    private GameObject tokenObj;


    [SerializeField]
    private Color tokenColor = Color.white;


    [SerializeField]
    private int maxTokenSize = 5;
    private int currentTokenCount = 0;


    public bool IsMaxStack => maxTokenSize <= currentTokenCount;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void StackTokenProcess()
    {
        GameObject token = Instantiate(tokenObj);
        token.transform.parent = transform;
        token.transform.localScale = Vector3.one;
        token.transform.localPosition = genPos.localPosition;

        token.GetComponent<Image>().color = tokenColor;

        Vector2 destPos = (Vector2)botPos.localPosition + new Vector2(Random.Range(-5.0f, 5.0f), 34.0f * currentTokenCount++);

        Sequence sq = DOTween.Sequence();

        sq.Join(token.transform.DOLocalMove(destPos, 0.5f)).SetEase(Ease.InOutBack);
        sq.Append(token.transform.DOBlendableLocalRotateBy(new Vector3(0,0 ,Random.Range(-2.0f, 2.0f)), 0.01f)).SetEase(Ease.OutQuad);
        sq.Append(token.transform.DOBlendableLocalRotateBy(new Vector3(0, 0, Random.Range(-2.0f, 2.0f)), 0.01f)).SetEase(Ease.OutQuad);
        sq.Play().OnComplete(()=>
        {

            SoundManager.instance.PlayUIEffect("se_type3");
        });



    }
}
