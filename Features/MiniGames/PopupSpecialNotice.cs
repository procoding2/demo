using OutGameEnum;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupSpecialNotice : MonoBehaviour
{
    public GameObject closeBtn;

    void Start()
    {
        closeBtn.GetComponent<Button>().onClickWithHCSound(() =>
        {
            gameObject.SetActive(false);
            SceneHelper.LoadScene(SceneEnum.Home);
        });
    }
    
}
