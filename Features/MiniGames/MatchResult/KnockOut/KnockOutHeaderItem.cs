using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnockOutHeaderItem : MonoBehaviour
{
    public GameObject active;

    public void ShowActive()
    {
        active.SetActive(true);
    }
}
