using System;
using System.Linq;
using System.Collections.Generic;
using GameEnum;
using OutGameEnum;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Localization.Components;
using TMPro;

public class KnockOutResult : MonoBehaviour
{
    [Header("User")]
    public GameObject user_Completed;
    public GameObject user_YouWon;
    public GameObject user_YouLose;
    public GameObject user_Exp;
    public GameObject user_Avt;
    //public GameObject user_AvtFrame;
    public TextMeshProUGUI user_userName;
    public GameObject user_Score;

    [Header("Opponent")]
    public GameObject opponent_Won;
    public GameObject opponent_Lose;
    public GameObject opponent_Avt;
    //public GameObject opponent_AvtFrame;
    public TextMeshProUGUI opponent_userName;
    public GameObject opponent_tobeDetermined;
    public GameObject opponent_Score;
    public GameObject opponent_playingnow;

    public GameObject exitBtn;
    public GameObject boardResult;

    public void InitKnockOut(SaveScoreGameResponse scoreResponse, KnockOutData data, Action onCallBackNextGame)
    {
        var user = scoreResponse.dataDecode.scores
            .Where(x => x.user.id.ToString() == Current.Ins.player.Id)
            .First();

        var opponent = scoreResponse.dataDecode.scores
            .Where(x => x.user.id.ToString() != Current.Ins.player.Id)
            .FirstOrDefault();

        StatusRoomResponseTypeEnum statusGame;

        if (data.Opponent == null)
        {
            statusGame = StatusRoomResponseTypeEnum.Playing;
        }
        else
        {
            var opnScr = scoreResponse.dataDecode.scores.FirstOrDefault(x => x.user.id.ToString() != Current.Ins.player.Id);
            if (opnScr != null && opnScr.score >= 0)
            {
                statusGame = StatusRoomResponseTypeEnum.Finish;
            }
            else
            {
                statusGame = StatusRoomResponseTypeEnum.Playing;
            }
        }

        user_userName.text = Current.Ins.player.Name;
        user_Score
            .GetComponent<LocalizeStringEvent>()
            .UpdateValue("point", VariableEnum.Int, user.score);
        //GetAvt
        Current.Ins.UserAvatar((result) =>
        {
            user_Avt.GetComponent<RawImage>().texture = result.Texture;
        });

        //Current.Ins.UserAvatarFrame((result) =>
        //{
        //    user_AvtFrame.GetComponent<RawImage>().texture = result.Texture;
        //});

        StatusRoomResponseTypeEnum roomStatusResponse = (StatusRoomResponseTypeEnum)scoreResponse.dataDecode.status;
        KORoomStatusEnum KORoomStatus = KORoomStatusEnum.Waiting;
        
        switch (statusGame)
        {
            case StatusRoomResponseTypeEnum.Playing:

                KORoomStatus = KORoomStatusEnum.Waiting;

                user_Completed.SetActive(true);
                user_Exp.SetActive(false);

                if (data.Opponent == null)
                {
                    opponent_userName.gameObject.SetActive(false);
                    opponent_Score.SetActive(false);

                    opponent_tobeDetermined.SetActive(true);
                }
                else
                {
                    opponent_Score.SetActive(false);
                    opponent_playingnow.SetActive(true);

                    opponent_userName.text = opponent.user.name;
                    //getAvt
                    Current.Ins.ImageData.AvatarByUrl(opponent.user.avatar, (result) =>
                    {
                        opponent_Avt.GetComponent<RawImage>().texture = result.Texture;
                    });

                    //Current.Ins.ImageData.AvatarFrameByUrl(opponent.user.avatarFrame, (result) =>
                    //{
                    //    opponent_AvtFrame.GetComponent<RawImage>().texture = result.Texture;
                    //    opponent_AvtFrame.SetActive(true);
                    //});
                }

                break;

            case StatusRoomResponseTypeEnum.Finish:

                opponent_Score
                        .GetComponent<LocalizeStringEvent>()
                        .UpdateValue("point", VariableEnum.Int, opponent.score);
                opponent_userName.text = opponent.user.name;
                //getAvt
                Current.Ins.ImageData.AvatarByUrl(opponent.user.avatar, (result) =>
                {
                    opponent_Avt.GetComponent<RawImage>().texture = result.Texture;
                });

                //Current.Ins.ImageData.AvatarFrameByUrl(opponent.user.avatarFrame, (result) =>
                //{
                //    opponent_AvtFrame.GetComponent<RawImage>().texture = result.Texture;
                //    opponent_AvtFrame.SetActive(true);
                //});

                if (scoreResponse.dataDecode.scores.IndexOf(user) == 0)
                {
                    user_YouWon.SetActive(true);
                    opponent_Lose.SetActive(true);

                    user_Exp.GetComponent<TextMeshProUGUI>().text = $"+{scoreResponse.dataDecode.yourXpReceived} EXP";

                    switch (roomStatusResponse)
                    {
                        case StatusRoomResponseTypeEnum.Playing:
                            KORoomStatus = KORoomStatusEnum.MatchWin;
                            break;
                        case StatusRoomResponseTypeEnum.Finish:
                            KORoomStatus = KORoomStatusEnum.Champion;
                            break;
                    }
                }
                else
                {
                    user_YouLose.SetActive(true);
                    opponent_Won.SetActive(true);

                    user_Exp.GetComponent<TextMeshProUGUI>().text = $"+{scoreResponse.dataDecode.yourXpReceived} EXP";

                    KORoomStatus = KORoomStatusEnum.MatchLoss;
                }

                break;
        }

        exitBtn.GetComponent<Button>().onClick.RemoveAllListeners();
        exitBtn.GetComponent<Button>()
            .onClick
            .AddListener(() =>
            {
                SoundManager.instance.PlayUIEffect("se_click1");
                UIPopUpManager.instance.ShowLoadingCircle(true);
                StartCoroutine
                (
                    Current.Ins.gameAPI
                        .GetRoomTournament<RoomTournamentResponse>
                        (
                            data.roomId,
                            (res) =>
                            {
                                UIPopUpManager.instance.ShowLoadingCircle(false);

                                if (res == null || res.errorCode != 0)
                                {
                                    UIPopUpManager.instance.ShowErrorPopUp("Request Error : Lnock Out", res, () => exitBtn.GetComponent<Button>().onClick.Invoke());
                                    return;
                                }

                                onCallBackGetRoomTournament(res, KORoomStatus, onCallBackNextGame);
                            }
                        )
                );
            });
    }

    private void onCallBackGetRoomTournament(RoomTournamentResponse response, KORoomStatusEnum KORoomStatus, Action onCallBackNextGame)
    {
        boardResult.SetActive(true);
        var boardData = new KnockOutBoardData();
        boardData.matchs = new List<KnockOutMatchData>();
        boardData.roomStatus = KORoomStatus;
        boardData.timeRemain = response.data.timeRemain;

        foreach (var match in response.data.datas)
        {
            KnockOutMatchData matchData = new KnockOutMatchData();
            matchData.gameNumber = match.gameNumber;

            var player0 = match.scores.ElementAtOrDefault(0);
            var player1 = match.scores.ElementAtOrDefault(1);

            if (player0 != null)
            {
                matchData.player_0 = new KnockOutPlayerData();
                matchData.player_0.Id = player0.user.id;
                matchData.player_0.Name = player0.user.name;
                matchData.player_0.Avatar = player0.user.avatar;
                matchData.player_0.Score = player0.score;
            }

            if (player1 != null)
            {
                matchData.player_1 = new KnockOutPlayerData();
                matchData.player_1.Id = player1.user.id;
                matchData.player_1.Name = player1.user.name;
                matchData.player_1.Avatar = player1.user.avatar;
                matchData.player_1.Score = player1.score;
            }

            boardData.matchs.Add(matchData);
        }
        
        boardResult.GetComponent<KnockOutBoardResult>().Init(boardData, () =>
        {
            onCallBackNextGame.Invoke();

            boardResult.SetActive(false);
            gameObject.SetActive(false);
        });
    }
}
