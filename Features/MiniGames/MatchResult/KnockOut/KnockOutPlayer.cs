using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class KnockOutPlayer : MonoBehaviour
{
    public GameObject Avt;
    public TextMeshProUGUI Name;
    public TextMeshProUGUI Score;
    public GameObject Win;
    public GameObject myPlayer;

    public void Init(KnockOutPlayerData player, bool isMyPlayer, bool isWin = false)
    {
        if (player == null) return;

        Name.text = player.Name;
        if (player.Score >= 0) Score.text = StringHelper.NumberFormatter(player.Score);

        Current.Ins.ImageData.AvatarByUrl(player.Avatar, (result) =>
        {
            Avt.GetComponent<RawImage>().texture = result.Texture;
        });

        if (isWin)
        {
            Win.SetActive(true);
        }
        if (isMyPlayer && player.Id.ToString() == Current.Ins.player.Id)
        {
            myPlayer.SetActive(true);
        }
    }
}
