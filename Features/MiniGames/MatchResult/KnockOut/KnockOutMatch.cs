using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnockOutMatch : MonoBehaviour
{
    public KnockOutPlayer player_0;
    public KnockOutPlayer player_1;

    public void Init(KnockOutMatchData matchData, int gameNumber)
    {
        if (matchData == null) return;

        bool isPlayer0Win = (matchData.player_0 != null && matchData.player_0.Score >= 0) &&
                            (matchData.player_1 != null && matchData.player_1.Score >= 0);
        player_0.Init(matchData.player_0, matchData.gameNumber == gameNumber, isPlayer0Win);
        player_1.Init(matchData.player_1, matchData.gameNumber == gameNumber, false);
    }
}
