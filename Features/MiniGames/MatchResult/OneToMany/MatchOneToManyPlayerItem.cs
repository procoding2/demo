using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using EnhancedUI.EnhancedScroller;

public class MatchOneToManyPlayerItem : EnhancedScrollerCellView
{
    public TextMeshProUGUI order;
    public TextMeshProUGUI playerName;
    public GameObject searching;
    public GameObject playerAvt;
    //public GameObject playerAvtFrame;
    public TextMeshProUGUI playerScore;

    public GameObject backgroundYour;

    public void Init(MatchOneToManyPlayerItemData itemData)
    {
        order.text = itemData.Order.ToString();
        playerScore.text = "--";

        if (searching != null)
        {
            if (itemData.isSearching)
            {
                this.searching.SetActive(true);
                this.playerName.gameObject.SetActive(false);
                
                return;
            }

            searching.SetActive(false);
            playerName.gameObject.SetActive(true);
        }

        if (itemData.isSearching)
        {
            this.searching.SetActive(true);
            this.playerName.gameObject.SetActive(false);
            return;
        }

        playerName.text = itemData.PlayerName;

        if (itemData.PlayerScore != -1)
        {
            playerScore.text = StringHelper.NumberFormatter(itemData.PlayerScore);
        }
        //GetAvt
        Current.Ins.ImageData.AvatarByUrl(itemData.PlayerAvt, (result) =>
        {
            playerAvt.GetComponent<RawImage>().texture = result.Texture;
        });

        //Current.Ins.ImageData.AvatarFrameByUrl(itemData.PlayerAvtFrame, (result) =>
        //{
        //    playerAvtFrame.GetComponent<RawImage>().texture = result.Texture;
        //    playerAvtFrame.SetActive(true);
        //});

        if (backgroundYour != null)
        {
            backgroundYour.SetActive(itemData.PlayerId == Current.Ins.player.Id);
        }
    }
}
