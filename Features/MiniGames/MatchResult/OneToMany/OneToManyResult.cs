using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using OutGameEnum;
using GameEnum;
using System.ComponentModel;
using UnityEngine.Localization.Components;
using EnhancedUI.EnhancedScroller;

public class OneToManyResult : UIBase<OneToManyResult>, IEnhancedScrollerDelegate
{
    protected override void InitIns() => Ins = this;

    public GameObject first;
    public GameObject second;
    public GameObject third;

    public GameObject ScrollBar;
    public EnhancedScroller scroller;
    public GameObject itemPrefab;

    [Header("Button")]
    public GameObject claimBtn;
    public GameObject reMatchBtn;
    public GameObject acceptMatchBtn;
    public GameObject newMatchBtn;
    public GameObject exitBtn;

    [SerializeField] private List<MatchOneToManyPlayerItemData> _data;

    protected override void Start()
    {
        scroller.Delegate = this;
    }

    public void InitOneToMany(SaveScoreGameResponse scoreResponse,
                              MatchOneToManyData data,
                              ConfirmFeePopupData fee,
                              Action newMatchAction)
    {
        //claimBtn.GetComponent<Button>().onClick.RemoveAllListeners();
        //claimBtn.GetComponent<Button>()
        //    .onClick.AddListener(() =>
        //    {
        //        SoundManager.instance.PlayUIEffect("se_click1");
        //        ClaimReward(data.roomId);
        //    });

        //reMatchBtn.GetComponent<Button>().onClick.RemoveAllListeners();
        //reMatchBtn.GetComponent<Button>()
        //    .onClick
        //    .AddListener(() =>
        //    {
        //        Debug.Log("Rematch");
        //    });

        _data = new List<MatchOneToManyPlayerItemData>();
        newMatchBtn.GetComponent<Button>().onClick.RemoveAllListeners();
        newMatchBtn.GetComponent<Button>()
            .onClickWithHCSound(() =>
            {
                //HCSound.Ins.StopGameSound();
                //HCSound.Ins.StopGameSound2();
                //HCSound.Ins.StopGameSound3();

                SoundManager.instance.PlayUIEffect("se_click1");
                UIMatchResult.Ins.confirmFeePopup.GetComponent<ConfirmFeePopup>()
                    .Init(data: fee, confirmAction: () =>
                    {
                        if (UIMatchResult.Ins.confirmFeePopup.GetComponent<ConfirmFeePopup>().IsShowWarning(fee))
                        {
                            UIMatchResult.Ins.confirmFeePopup.GetComponent<ConfirmFeePopup>().warningNeedCoinPopup.SetActive(true);
                            return;
                        }

                        newMatchAction.Invoke();
                    });
            });

        newMatchBtn.SetActive(true);

        exitBtn.GetComponent<Button>().onClick.RemoveAllListeners();
        exitBtn.GetComponent<Button>()
            .onClick
            .AddListener(() =>
            {
                //HCSound.Ins.StopGameSound();
                //HCSound.Ins.StopGameSound2();
                //HCSound.Ins.StopGameSound3();

                SoundManager.instance.PlayUIEffect("se_click1");
                SceneHelper.LoadScene(SceneEnum.Home);
            });

        //var statusRoom = (StatusRoomResponseTypeEnum)scoreResponse.data.status;
        //switch (statusRoom)
        //{
        //    case StatusRoomResponseTypeEnum.Playing:
        //        newMatchBtn.SetActive(true);
        //        break;

        //    case StatusRoomResponseTypeEnum.Finish:
        //        GetRoomResult(data.roomId);
        //        break;
        //}

        // ReMap Rank
        foreach (var user in data.FinishedUserDatas)
        {
            var scuser = scoreResponse.dataDecode.scores.First(x => x.user.id == user.UserId);
            scuser.rank = user.Rank;
        }

        var userScores = scoreResponse.dataDecode.scores.OrderByDescending(x => x.score).ToList();

        var firstUser = userScores.ElementAtOrDefault(0);
        var firstRank = firstUser != null && firstUser.rank != -1 ? firstUser.rank : 1;
        first.GetComponent<MatchOneToManyPlayerItem>()
                .Init(getItemData(firstUser, firstRank));

        var secondUser = userScores.ElementAtOrDefault(1);
        var secondRank = secondUser != null && secondUser.rank != -1 ? secondUser.rank : 2;
        second.GetComponent<MatchOneToManyPlayerItem>()
                .Init(getItemData(secondUser, secondRank));

        var thirdUser = userScores.ElementAtOrDefault(2);
        var thirdRank = thirdUser != null && thirdUser.rank != -1 ? thirdUser.rank : 3;
        third.GetComponent<MatchOneToManyPlayerItem>()
                .Init(getItemData(thirdUser, thirdRank));

        if (data.totalPlayer <= 3) return;

        int yourOrder = 0;
        for (int i = 3; i < data.totalPlayer; i++)
        {
            var uScore = userScores.ElementAtOrDefault(i);
            var uRank = uScore != null && uScore.rank != -1 ? uScore.rank : i + 1;

            var itemData = getItemData(uScore, uRank);
            _data.Add(itemData);

            if (uScore != null && uScore.user.id.ToString() == Current.Ins.player.Id)
            {
                yourOrder = uRank;
            }
        }

        var scrollPosition = scroller.ScrollPosition;
        scroller.ReloadData();
        scroller.ScrollPosition = scrollPosition;
    }

    private MatchOneToManyPlayerItemData getItemData(SaveScoreGameResponse.Score userScore, int order)
    {
        var itemData = new MatchOneToManyPlayerItemData();
        itemData.Order = order;

        if (userScore == null)
        {
            itemData.isSearching = true;
            return itemData;
        }

        itemData.isSearching = false;
        itemData.PlayerId = userScore.user.id.ToString();
        itemData.PlayerName = userScore.user.name;
        itemData.PlayerScore = userScore.score;
        itemData.PlayerAvt = userScore.user.avatar;
        itemData.PlayerAvtFrame = userScore.user.avatarFrame;

        return itemData;
    }

    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return _data.Count;
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        return 136f;
    }

    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        MatchOneToManyPlayerItem cellView = scroller.GetCellView(itemPrefab.GetComponent<MatchOneToManyPlayerItem>()) as MatchOneToManyPlayerItem;
        cellView.Init(_data[dataIndex]);
        return cellView;
    }
}
