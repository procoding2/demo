using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using OutGameEnum;
using GameEnum;
using UnityEngine.Localization.Components;
using System.Linq;
using System.Threading.Tasks;
using TMPro;
using DG.Tweening;

public class HeadToHeadResult : UIBase<HeadToHeadResult>
{
    protected override void InitIns() => Ins = this;

    
    public GameObject title_update, title_win, title_lose, title_draw, waitOpponentObj;

    [Header("User")]
    [SerializeField]
    private ResultUserInfo myInfo;


    //public GameObject user_YouWon;
    //public GameObject user_YouLose;
    //public GameObject user_Exp;
    //public RawImage user_Avt;
    ////public RawImage user_AvtFrame;
    //public TextMeshProUGUI user_userName;
    //public GameObject user_Score;

    [Header("Opponent")]
    [SerializeField]
    private ResultUserInfo opponentInfo;
    //public GameObject opponent_Waiting;
    //public GameObject opponent_Won;
    //public GameObject opponent_Lose;
    //public RawImage opponent_Avt;
    ////public RawImage opponent_AvtFrame;
    //public TextMeshProUGUI opponent_userName;
    //public GameObject opponent_tobeDetermined;
    //public GameObject opponent_Score;
    public GameObject opponent_playingnow;

    [Header("Button")]
    public GameObject claimBtn;
    public GameObject reMatchBtn;
    public GameObject acceptMatchBtn;
    public GameObject newMatchBtn;
    public GameObject exitBtn;

    public GameObject popup;

    private MatchHeadToHeadData _data;
    private Action<string> _acceptRematchCallBack;
    private bool _invitedbyOpponent = false;

    public void InitHeadToHead(SaveScoreGameResponse scoreResponse,
                               MatchHeadToHeadData data,
                               ConfirmFeePopupData fee,
                               Action<string> acceptRematchCallBack,
                               Action newMatchAction)
    {
        newMatchBtn.GetComponent<Button>().onClick.RemoveAllListeners();
        newMatchBtn.GetComponent<Button>().onClickWithHCSound(() =>
        {
            //HCSound.Ins.StopGameSound();
            //HCSound.Ins.StopGameSound2();
            //HCSound.Ins.StopGameSound3();

            UIMatchResult.Ins.confirmFeePopup.GetComponent<ConfirmFeePopup>()
                .Init(data:fee, confirmAction : () => 
                {
                    if (UIMatchResult.Ins.confirmFeePopup.GetComponent<ConfirmFeePopup>().IsShowWarning(fee))
                    {
                        UIMatchResult.Ins.confirmFeePopup.GetComponent<ConfirmFeePopup>().warningNeedCoinPopup.SetActive(true);
                        return;
                    }
                    UIPopUpManager.instance.ShowLoadingCircle(true);
                    StartCoroutine(Current.Ins.gameAPI.ExitGame<BaseResponse>(data.gameId, (res) =>
                    {
                        UIPopUpManager.instance.ShowLoadingCircle(false);
                        if (res == null || res.errorCode != 0)
                        {
                            UIPopUpManager.instance.ShowErrorPopUp("Request Error : InitHeadToHead", res, () => newMatchBtn.GetComponent<Button>().onClick.Invoke());
                            return;
                        }

                        newMatchAction.Invoke();
                    }));
                });           
        });

        exitBtn.GetComponent<Button>().onClick.RemoveAllListeners();
        exitBtn.GetComponent<Button>().onClick.AddListener(() =>
        {
            //HCSound.Ins.StopGameSound();
            //HCSound.Ins.StopGameSound2();
            //HCSound.Ins.StopGameSound3();

            SoundManager.instance.PlayUIEffect("se_click1");
            UIPopUpManager.instance.ShowLoadingCircle(true);
            StartCoroutine(Current.Ins.gameAPI.ExitGame<BaseResponse>(data.gameId, (res) =>
            {
                UIPopUpManager.instance.ShowLoadingCircle(false);
                if (res == null || res.errorCode != 0)
                {
                    UIPopUpManager.instance.ShowErrorPopUp("Request Error : InitHeadToHead", res, () => exitBtn.GetComponent<Button>().onClick.Invoke());
                    return;
                }

                SceneHelper.LoadScene(SceneEnum.Home);
            }));
        });

        _data = data;
        _acceptRematchCallBack = acceptRematchCallBack;

        var user = scoreResponse.dataDecode.scores
            .Where(x => x.user.id.ToString() == Current.Ins.player.Id)
            .First();

        var statusRoom = (StatusRoomResponseTypeEnum)scoreResponse.dataDecode.status;


        myInfo.SetUserInfo(Current.Ins.player.Name, user.score);
        //GetAvt
        Current.Ins.UserAvatar((result) =>
        {
            myInfo.SetUserAvatar(result.Texture);
        });

        //Current.Ins.UserAvatarFrame((result) =>
        //{
        //    user_AvtFrame.texture = result.Texture;
        //});

        switch (statusRoom)
        {
            case StatusRoomResponseTypeEnum.Playing:
                newMatchBtn.SetActive(true);
                title_update.SetActive(true);
                title_win.SetActive(false);
                title_lose.SetActive(false);
                title_draw.SetActive(false);

                //user_Exp.SetActive(false);

                if (data.Opponent == null)
                {
                    waitOpponentObj.SetActive(true);
                    opponentInfo.gameObject.SetActive(false);
                }
                else
                {
                    waitOpponentObj.SetActive(false);
                    opponentInfo.gameObject.SetActive(true);
                    opponentInfo.SetUserInfo(data.Opponent.Name, "Now Playing");
                    //getAvt
                    Current.Ins.ImageData.AvatarByUrl(data.Opponent.Avt, (result) =>
                    {
                        opponentInfo.SetUserAvatar(result.Texture);
                    });
                    //Current.Ins.ImageData.AvatarFrameByUrl(data.Opponent.AvtFrame, (result) =>
                    //{
                    //    opponent_AvtFrame.texture = result.Texture;
                    //    opponent_AvtFrame.gameObject.SetActive(true);
                    //});
                }

                break;

            case StatusRoomResponseTypeEnum.Finish:
                var opponent = scoreResponse.dataDecode.scores
                   .Where(x => x.user.id.ToString() != Current.Ins.player.Id)
                   .FirstOrDefault();

                var userIds = scoreResponse.dataDecode.scores.Select(x => x.user.id).ToList();
                AddJob(() => SetGameFinish(user, opponent, fee, userIds, scoreResponse.dataDecode.isDraw, scoreResponse.dataDecode.yourXpReceived));

                break;
        }

        if (data.isSync && data.isOpponentInGame)
        {
            // Listen opponent invite rematch
            Current.Ins.socketAPI.AddSocket<ReMatchSocketResponse>
                (
                    StringConst.SOCKET_REMATCH_REQUEST_EVENT,
                    $"{StringConst.SOCKET_REMATCH_REQUEST_EVENT}{Current.Ins.player.Id}",
                    (rematchRes) =>
                    {
                        // Show Accept Rematch Button
                        Debug.Log($"rematchRes: {rematchRes}");

                        AddJob(() =>
                        {
                            _invitedbyOpponent = true;
                            reMatchBtn.SetActive(false);
                            setAcceptRematchBtn(rematchRes.gameId, fee);
                        });
                    }
                );

            // Listen opponent finish game
            if (statusRoom == StatusRoomResponseTypeEnum.Playing)
            {
                Current.Ins.socketAPI.AddSocket<UserGameFinishSocketResponse>
                    (
                        StringConst.SOCKET_USER_GAME_FINISH_EVENT,
                        $"{StringConst.SOCKET_USER_GAME_FINISH_EVENT}{data.gameId}",
                        (gameFinishResponse) =>
                        {
                            var oppRes = gameFinishResponse.scores
                            .Where(x => x.user.id.ToString() != Current.Ins.player.Id)
                            .FirstOrDefault();

                            SaveScoreGameResponse.Score opponent = new SaveScoreGameResponse.Score();
                            opponent.user = new UserResponse();
                            opponent.score = oppRes.score;
                            opponent.user.id = oppRes.user.id;
                            opponent.user.name = oppRes.user.name;
                            opponent.user.avatar = oppRes.user.avatar;

                            var userIds = gameFinishResponse.scores.Select(x => x.user.id).ToList();
                            AddJob(() => SetGameFinish(user, opponent, fee, userIds, gameFinishResponse.isDraw, scoreResponse.dataDecode.yourXpReceived));
                        }
                    );
            }

            Current.Ins.socketAPI.AddSocket<ExitGameSocketResponse>
            (
                StringConst.SOCKET_EXIT_GAME_EVENT,
                $"{StringConst.SOCKET_EXIT_GAME_EVENT}{data.gameId}",
                (exitGameRes) => {
                    AddJob(() => reMatchBtn.GetComponent<Button>().interactable = false);
                });
        }
    }

    private void SetGameFinish(SaveScoreGameResponse.Score userScore,
                              SaveScoreGameResponse.Score opponentScore,
                              ConfirmFeePopupData fee,
                              List<int> userIds,
                              bool isDraw,
                              int exp)
    {
        title_update.SetActive(false);

        newMatchBtn.SetActive(true);
        acceptMatchBtn.SetActive(false);

        waitOpponentObj.SetActive(false);
        opponentInfo.gameObject.SetActive(true);
        opponentInfo.SetUserInfo(opponentScore.user.name, opponentScore.score);

        //getAvt
        Current.Ins.ImageData.AvatarByUrl(opponentScore.user.avatar, (result) =>
        {
            opponentInfo.SetUserAvatar(result.Texture);
        });

        //draw
        if (isDraw)
        {
            title_draw.SetActive(true);
        }//win
        else if (userIds.IndexOf(userScore.user.id) == 0)
        {
            //setClaimBtn();
            title_win.SetActive(true);
            title_lose.SetActive(false);

            StartCoroutine(GameResultCoroutine(true));
            //user_Exp.GetComponent<TextMeshProUGUI>().text = $"+{exp} EXP";
        }
        else
        {
            //claimBtn.SetActive(false);
            title_win.SetActive(false);
            title_lose.SetActive(true);
            StartCoroutine(GameResultCoroutine(false));
            //user_Exp.GetComponent<TextMeshProUGUI>().text = $"+{exp} EXP";
        }

        setRematchBtn(fee);
        //Current.Ins.ReloadUserProfile(
        //        transform: claimBtn.transform,
        //        callBackAfterDoneAPI: () =>
        //        {
        //            setRematchBtn();
        //            claimBtn.SetActive(false);
        //            newMatchBtn.SetActive(true);
        //        });
    }


    private IEnumerator GameResultCoroutine(bool isWin)
    {
        yield return new WaitForSeconds(0.5f);
        myInfo.SetResultAni(isWin);
        yield return new WaitForSeconds(0.5f);
        opponentInfo.SetResultAni(!isWin);
    }

    //private void setClaimBtn()
    //{
    //    GetRoomResult(_data.roomId);
    //    claimBtn.GetComponent<Button>().onClick.RemoveAllListeners();
    //    claimBtn.GetComponent<Button>().onClick.AddListener(() =>
    //    {
    //        SoundManager.instance.PlayUIEffect("se_click1");
    //        ClaimReward();
    //    });
    //}

    //private void ClaimReward()
    //{
    //    UIMatchResult.Ins.ShowLoading(true);
    //    StartCoroutine(Current.Ins.gameAPI.ClaimReward<ClaimRewardResponse>(_data.roomId, (res) =>
    //    {
    //        UIMatchResult.Ins.ShowLoading(false);
    //        if (res == null || res.errorCode != 0)
    //        {
    //            Debug.Log("Claim Error:");
    //            UIMatchResult.Ins.ShowErrorPopup(setClaimBtn);
    //        }

    //        Current.Ins.ReloadUserProfile(
    //            transform: claimBtn.transform,
    //            callBackAfterDoneAPI: () =>
    //            {
    //                setRematchBtn();
    //                claimBtn.SetActive(false);
    //                newMatchBtn.SetActive(true);
    //            });
    //    }));
    //}

    //private void GetRoomResult(string roomId)
    //{
    //    UIMatchResult.Ins.ShowLoading(true);
    //    StartCoroutine(Current.Ins.historyAPI.GetTournamentRoomResult(roomId, (res) =>
    //    {
    //        UIMatchResult.Ins.ShowLoading(false);
    //        if (res == null || res.errorCode != 0)
    //        {
    //            UIMatchResult.Ins.ShowErrorPopup(() => GetRoomResult(roomId));
    //            return;
    //        }

    //        var userData = res.data.FirstOrDefault(x => x.user.id.ToString() == Current.Ins.player.Id);
    //        if (userData == null) return;
    //        if (userData.gold == 0 && userData.ticket == 0 && userData.token == 0) return;

    //        claimBtn.SetActive(true);
    //    }));
    //}

    private void setRematchBtn(ConfirmFeePopupData fee)
    {
        if (!_data.isSync || !_data.isOpponentInGame || _invitedbyOpponent) return;

        // Wait for opponent accept
        Current.Ins.socketAPI.AddSocket<AcceptRematchSocketResponse>
        (
            StringConst.SOCKET_ACCEPT_REMATCH_EVENT,
            $"{StringConst.SOCKET_ACCEPT_REMATCH_EVENT}{Current.Ins.player.Id}",
            (acceptRes) =>
            {
                AddJob(() => JoinRoom(acceptRes.roomId));
            }
        );

        reMatchBtn.SetActive(true);
        //reMatchBtn.GetComponent<Button>().onClick.RemoveAllListeners();
        //reMatchBtn.GetComponent<Button>().onClick.AddListener(() =>
        //{
        //    SoundManager.instance.PlayUIEffect("se_click1");
        //    UIMatchResult.Ins.ShowLoading(true);
        //    reMatchBtn.SetActive(false);
        //    // Send rematch invite
        //    StartCoroutine(Current.Ins.gameAPI.ReMatchGame<BaseResponse>(_data.gameId, (res) =>
        //    {
        //        UIMatchResult.Ins.ShowLoading(false);
        //        if (res == null || res.errorCode != 0)
        //        {
        //            reMatchBtn.SetActive(true);
        //            Debug.Log("Send rematch invite error:");
        //            UIMatchResult.Ins.ShowErrorPopup(() => reMatchBtn.GetComponent<Button>().onClick?.Invoke());
        //            return;
        //        }
        //    }));
        //});
        //
        reMatchBtn.GetComponent<Button>().onClick.RemoveAllListeners();
        reMatchBtn.GetComponent<Button>().onClick.AddListener(() =>
        {
            //
            SoundManager.instance.PlayUIEffect("se_click1");
            UIMatchResult.Ins.confirmFeePopup.GetComponent<ConfirmFeePopup>()
                .Init(data: fee, confirmAction: () =>
                {
                    SoundManager.instance.PlayUIEffect("se_click1");
                    if (UIMatchResult.Ins.confirmFeePopup.GetComponent<ConfirmFeePopup>().IsShowWarning(fee))
                    {
                        UIMatchResult.Ins.confirmFeePopup.GetComponent<ConfirmFeePopup>().warningNeedCoinPopup.SetActive(true);
                        return;
                    }
                    //
                    UIMatchResult.Ins.confirmFeePopup.transform.localScale = Vector3.zero;
                    UIPopUpManager.instance.ShowLoadingCircle(true);
                    reMatchBtn.SetActive(false);
                    // Send rematch invite
                    StartCoroutine(Current.Ins.gameAPI.ReMatchGame<BaseResponse>(_data.gameId, (res) =>
                    {
                        UIPopUpManager.instance.ShowLoadingCircle(false);
                        if (res == null || res.errorCode != 0)
                        {
                            reMatchBtn.SetActive(true);
                            Debug.Log("Send rematch invite error:");
                            UIPopUpManager.instance.ShowErrorPopUp("Request Error : Send rematch invite error", res, () => reMatchBtn.GetComponent<Button>().onClick.Invoke());
                            return;
                        }
                    }));
                });   
        });
    }

    private void JoinRoom(string roomId)
    {
        UIPopUpManager.instance.ShowLoadingCircle(true);
        // call joinroom
        StartCoroutine(Current.Ins.gameAPI.JoinRoom<JoinRoomResponse>(roomId,
            (res) =>
            {
                UIPopUpManager.instance.ShowLoadingCircle(false);
                if (res == null || res.errorCode != 0)
                {
                    Debug.Log("Rematch Error:");
                    UIPopUpManager.instance.ShowErrorPopUp("Request Error : Rematch error", res, () => JoinRoom(roomId));
                    return;
                }

                _acceptRematchCallBack.Invoke(res.data.gameId);
            }
        ));
    }

    private void setAcceptRematchBtn(string gameId, ConfirmFeePopupData fee)
    {
        bool isWait = true;
        Current.Ins.socketAPI.AddSocket<JoinRoomSocketResponse>
        (
            StringConst.SOCKET_JOIN_REMATCH_EVENT,
            $"{StringConst.SOCKET_JOIN_REMATCH_EVENT}{Current.Ins.player.Id}",
            (joinRes) =>
            {
                AddJob(() => { _acceptRematchCallBack.Invoke(joinRes.gameId); });
                isWait = false;
            }
        );

        acceptMatchBtn.SetActive(true);
        //acceptMatchBtn.GetComponent<Button>().onClick.RemoveAllListeners();
        //acceptMatchBtn.GetComponent<Button>().onClick.AddListener(() =>
        //{
        //    SoundManager.instance.PlayUIEffect("se_click1");
        //    acceptMatchBtn.SetActive(false);
        //    popup.GetComponent<HeadToHeadPopup>().ShowWaitOpponentJoinMatch();

        //    StartCoroutine(Current.Ins.gameAPI.AcceptReMatchGame<BaseResponse>(gameId, async (res) =>
        //    {
        //        if (res == null || res.errorCode != 0)
        //        {
        //            acceptMatchBtn.SetActive(true);
        //            AddJob(() => popup.GetComponent<HeadToHeadPopup>().ShowOpponentLeftMatch());
        //            return;
        //        }

        //        await Task.Run(async () =>
        //        {
        //            await Task.Delay(TimeSpan.FromSeconds(5));
        //            if (!isWait) return;
        //            AddJob(() => popup.GetComponent<HeadToHeadPopup>().ShowOpponentLeftMatch());
        //        });
        //    }));
        //});
        //
        UIMatchResult.Ins.confirmFeePopup.GetComponent<ConfirmFeePopup>()
               .Init(data: fee, confirmAction: () =>
               {
                   SoundManager.instance.PlayUIEffect("se_click1");
                   if (UIMatchResult.Ins.confirmFeePopup.GetComponent<ConfirmFeePopup>().IsShowWarning(fee))
                   {
                       UIMatchResult.Ins.confirmFeePopup.GetComponent<ConfirmFeePopup>().warningNeedCoinPopup.SetActive(true);
                       return;
                   }
                   UIMatchResult.Ins.confirmFeePopup.transform.localScale = Vector3.zero;
                   //
                   acceptMatchBtn.SetActive(false);
                   popup.GetComponent<HeadToHeadPopup>().ShowWaitOpponentJoinMatch();

                   StartCoroutine(Current.Ins.gameAPI.AcceptReMatchGame<BaseResponse>(gameId, async (res) =>
                   {
                       if (res == null || res.errorCode != 0)
                       {
                           acceptMatchBtn.SetActive(true);
                           AddJob(() => popup.GetComponent<HeadToHeadPopup>().ShowOpponentLeftMatch());
                           return;
                       }

                       await Task.Run(async () =>
                       {
                           await Task.Delay(TimeSpan.FromSeconds(5));
                           if (!isWait) return;
                           AddJob(() => popup.GetComponent<HeadToHeadPopup>().ShowOpponentLeftMatch());
                       });
                   }));
               }, false);
        acceptMatchBtn.GetComponent<Button>().onClick.RemoveAllListeners();
        acceptMatchBtn.GetComponent<Button>().onClick.AddListener(() =>
        {
            SoundManager.instance.PlayUIEffect("se_click1");
            UIMatchResult.Ins.confirmFeePopup.transform.DOScale(Vector3.one, 0.1f);
        });
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        Current.Ins.socketAPI.RemoveSocket(StringConst.SOCKET_REMATCH_REQUEST_EVENT);
        Current.Ins.socketAPI.RemoveSocket(StringConst.SOCKET_ACCEPT_REMATCH_EVENT);
        Current.Ins.socketAPI.RemoveSocket(StringConst.SOCKET_USER_GAME_FINISH_EVENT);
        Current.Ins.socketAPI.RemoveSocket(StringConst.SOCKET_EXIT_GAME_EVENT);
    }
}

