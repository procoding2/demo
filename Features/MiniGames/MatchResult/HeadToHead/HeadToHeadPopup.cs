using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeadToHeadPopup : MonoBehaviour
{
    public GameObject opponentLeftMatch;
    public GameObject btnCloseOpponentLeftMatch;

    public void ShowOpponentLeftMatch()
    {
        waitOpponentJoinMatch.SetActive(false);
        gameObject.SetActive(true);
        opponentLeftMatch.SetActive(true);

        btnCloseOpponentLeftMatch.GetComponent<Button>().onClick.RemoveAllListeners();
        btnCloseOpponentLeftMatch.GetComponent<Button>()
            .onClick
            .AddListener(() =>
            {
                SoundManager.instance.PlayUIEffect("se_click1");
                
                opponentLeftMatch.SetActive(false);
                gameObject.SetActive(false);
            });
    }

    public GameObject waitOpponentJoinMatch;
    public void ShowWaitOpponentJoinMatch()
    {
        opponentLeftMatch.SetActive(false);
        gameObject.SetActive(true);
        waitOpponentJoinMatch.SetActive(true);
    }

    public void HideWaitOpponentJoinMatch()
    {
        opponentLeftMatch.SetActive(false);
        gameObject.SetActive(false);
        waitOpponentJoinMatch.SetActive(false);
    }
}
