using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMatchResult : UIBase<UIMatchResult>
{
    protected override void InitIns() => Ins = this;

    public GameObject headToHead;
    public GameObject oneToMany;
    public GameObject knockOut;
    public GameObject confirmFeePopup;

    public GameObject errorPopup;
    public GameObject loadingPopup;





    public void ShowHeadToHead(SaveScoreGameResponse scoreResponse, MatchHeadToHeadData data, ConfirmFeePopupData fee, Action<string> acceptReMatchCallBack, Action newMatchCallBack)
    {
        headToHead
            .GetComponent<HeadToHeadResult>()
            .InitHeadToHead(scoreResponse, data, fee, acceptReMatchCallBack, newMatchCallBack);

        headToHead.SetActive(true);
    }

    public void ShowOneToMany(SaveScoreGameResponse scoreResponse, MatchOneToManyData data, ConfirmFeePopupData fee, Action newMatchCallBack)
    {
        oneToMany.SetActive(true);
        oneToMany.GetComponent<OneToManyResult>()
            .InitOneToMany(scoreResponse, data, fee, newMatchCallBack);
    }

    public void ShowKnockOut(SaveScoreGameResponse scoreResponse, KnockOutData data, Action onCallBackNextGame)
    {
        knockOut.SetActive(true);
        knockOut.GetComponent<KnockOutResult>()
            .InitKnockOut(scoreResponse, data, () =>
            {
                onCallBackNextGame.Invoke();

                knockOut.SetActive(false);
            });
    }
}
