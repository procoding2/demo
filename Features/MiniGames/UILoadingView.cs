using System.Collections;
using System.Collections.Generic;
using OutGameEnum;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Localization.Components;
using DG.Tweening;

public class UILoadingView : MonoBehaviour
{
    public GameObject loadingProgress;
    public GameObject loadingProgressText;

    public void UpdateProgresbar(float value)
    {
        loadingProgressText.GetComponent<LocalizeStringEvent>().UpdateValue("percent", VariableEnum.String, StringHelper.FloatToPercent(value));
        loadingProgress
            .GetComponent<Slider>()
            .DOValue(value, 0.3f, false);
    }
}
