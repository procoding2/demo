using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrumSound : MonoBehaviour
{
    public bool isEnable = false;

    public void PlayDrumSound(int index)
    {
        if (isEnable)
            SoundManager.instance.PlayEffectOnlyOne(index == 0 ? "se_drum" : "se_drum2");
    }
}
