using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

using GameEnum;
using TMPro;

public class UIBubbleResult : UIBase<UIBubbleResult>
{
    protected override void InitIns() => Ins = this;

    public GameObject TimesUpTitle;
    public GameObject GameOverTitle;
    public GameObject SuccessMissionTitle;

    public GameObject Score;
    public GameObject Bonus;

    public TextMeshProUGUI FinalScores;

    public GameObject submitBtn;

    public void Init(BubbleScoreResult result,
        GameFinishTypeEnum finishType,
        string gameId,
        string key,
        Action<SaveScoreGameResponse> afterSubmitCallBack)
    {
        switch (finishType)
        {
            case GameFinishTypeEnum.TimesUp:
                TimesUpTitle.SetActive(true);
                GameOverTitle.SetActive(false);
                SuccessMissionTitle.SetActive(false);
                break;
            case GameFinishTypeEnum.GameOver:
                TimesUpTitle.SetActive(false);
                GameOverTitle.SetActive(true);
                SuccessMissionTitle.SetActive(false);
                break;
            case GameFinishTypeEnum.Success:
                TimesUpTitle.SetActive(false);
                GameOverTitle.SetActive(false);
                SuccessMissionTitle.SetActive(true);
                break;
        }

        var lastScore = result.ScoreInfors.LastOrDefault() ?? new BubbleScoreResult.ScoreInfor();
        var score = lastScore.Score;
        var bonusScore = lastScore.BonusScore;
        Score.GetComponent<DetailItemResult>().Init("1", score.ToString());
        Bonus.GetComponent<DetailItemResult>().Init("1", bonusScore.ToString());

        var totalScore = score + bonusScore;
        FinalScores.text = totalScore.ToString();

        SaveScoreGameRequest req = new SaveScoreGameRequest();
        req.key = key;
        req.status = (int)finishType;
        req.score = totalScore;
        req.scoreInfo = new SaveScoreGameRequest.ScoreInfo[5];
        req.scoreInfo[0] = new SaveScoreGameRequest.ScoreInfo()
        {
            name = StringConst.BUBBLE_SCORE_INFO_SCORES,
            quantity = 1,
            score = totalScore
        };

        req.scoreInfo[2] = new SaveScoreGameRequest.ScoreInfo()
        {
            name = StringConst.BUBBLE_SCORE_INFO_BONUS_SCORES,
            quantity = 1,
            score = bonusScore
        };

        req.time = result.EndTimeMilliSecond - result.StartTimeMilliSecond;

        UIPopUpManager.instance.ShowLoadingCircle(true);
        StartCoroutine(Current.Ins.gameAPI.SaveScoreGame<SaveScoreGameResponse>(gameId, req, (res) =>
        {
            UIPopUpManager.instance.ShowLoadingCircle(false);
            if (res == null || res.errorCode != 0)
            {
                if (res != null && res.errorCode == 2)
                {
                    GetGameScoreWhenError(gameId, key, afterSubmitCallBack);
                    return;
                }

                UIPopUpManager.instance.ShowErrorPopUp("Request Error : SaveScoreGame", res, () => submitBtn.GetComponent<Button>().onClick?.Invoke());
                return;
            }

            res.dataDecode = JsonUtility.FromJson<SaveScoreGameResponse.DataDecode>(EncryptHelper.DecryptWithAES256(res.data.info, key));
            afterSubmitCallBack.Invoke(res);
        }));
    }

    private void GetGameScoreWhenError(string gameId, string key, Action<SaveScoreGameResponse> afterSubmitCallBack)
    {
        UIPopUpManager.instance.ShowLoadingCircle(true);
        StartCoroutine(Current.Ins.gameAPI.GetGameScore(gameId, (res) =>
        {
            UIPopUpManager.instance.ShowLoadingCircle(false);
            if (res == null || res.errorCode != 0)
            {
                UIPopUpManager.instance.ShowErrorPopUp("Request Error : GetGameScoreWhenError", res, () => GetGameScoreWhenError(gameId, key, afterSubmitCallBack));
                return;
            }

            res.dataDecode = JsonUtility.FromJson<SaveScoreGameResponse.DataDecode>(EncryptHelper.DecryptWithAES256(res.data.info, key));
            afterSubmitCallBack.Invoke(res);
        }));
    }
}
