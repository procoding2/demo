using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BubbleTutorial : MonoBehaviour
{
    [SerializeField] GameObject Dialog1;
    [SerializeField] GameObject Handtut1;
    [SerializeField] GameObject Dialog2;
    [SerializeField] GameObject Dialog3;
    [SerializeField] GameObject Dialog4;
    [SerializeField] GameObject bubble_transparent;
    [SerializeField] GameObject EndTutorialPopup;
    [SerializeField] Button endTutorialPopup;
    [SerializeField] Image shadow;
    public bool check = false;
    //
    public bool tut2Done = false;
    public bool tut2 = false;
    //
    public bool tut3 = false;
    public bool tut3Done = false;
    //
    public bool btnTut3 = false;
    public bool btnTut4 = false;

    private void Awake()
    {       
        endTutorialPopup.onClick.AddListener(() =>
        {
            ActiveEndTutorialPopup();
        });
    }
    private void Update()
    {
        if (tut2 == true && tut2Done == false && BubbleGun.Ins.IsDrawLine == true)
        {
            tut2Done = true;
            DOVirtual.DelayedCall(0.1f, delegate
            {
                tut3 = true;
                EndTut2();
            });
        }

        if (tut3 == true && tut3Done == false && BubbleGun.Ins.IsCanSetNewDirection == true && BubbleGun.Ins.IsDrawLine == false)
        {
            tut3Done = true;
            DOVirtual.DelayedCall(0.5f, delegate
            {
                Tut3();
            });
        }
    }

    public void BeginTut()
    {
        this.endTutorialPopup.gameObject.SetActive(true);
        Dialog1.transform.DOScale(Vector3.one, 0.3f);
        Handtut1.transform.DOScale(Vector3.one, 0.3f)
            .OnComplete(() =>
            {
                this.check = true;
                Handtut1.transform.DOMove(new Vector3(Handtut1.transform.position.x - 0.1f, Handtut1.transform.position.y - 0.1f, 0), 0.2f).SetLoops(-1,LoopType.Yoyo);
            });
    }

    public void EndTut1()
    {
        if (Dialog1.activeSelf == false) return;
        this.check = false;
        End(Handtut1);
        End(Dialog1);
        tut2 = true;
        DOVirtual.DelayedCall(1.5f, delegate
        {
            Tut2();
        });
    }

    void End(GameObject a)
    {
        DOTween.Kill(a.transform);
        a.transform.DOScale(Vector3.zero, 0.1f)
            .OnComplete(() =>
            {
                a.SetActive(false);
            });
    }

    void Tut2()
    {
        Dialog2.transform.DOScale(Vector3.one, 0.3f);
        Handtut1.SetActive(true);
        Handtut1.transform.DOScale(Vector3.one, 0.3f)
            .OnComplete(() =>
            {
                this.check = true;
                Handtut1.transform.GetChild(0).gameObject.SetActive(true);
                Handtut1.transform.GetChild(0).DOScale(Vector3.zero, 0.2f).SetLoops(-1, LoopType.Restart);
            });
    }

    void EndTut2()
    {
        End(Handtut1.transform.GetChild(0).gameObject);
        End(Handtut1);
        End(Dialog2);       
    }

    void Tut3()
    {
        Dialog3.transform.DOScale(Vector3.one, 0.3f);
        shadow.transform.SetParent(Dialog3.transform.parent.parent);
        shadow.transform.SetSiblingIndex(Dialog3.transform.parent.parent.childCount-2);
        shadow.gameObject.SetActive(true);
        shadow.DOFade(0.8f, 0.3f);
        shadow.gameObject.GetComponent<Button>().onClick.AddListener(() =>
        {
            End(Dialog3);
            this.btnTut3 = true;
            shadow.DOFade(0f, 2f);
            Dialog4.transform.DOScale(Vector3.one, 0.3f);
            //
            shadow.gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
            shadow.gameObject.GetComponent<Button>().onClick.AddListener(() =>
            {
                this.btnTut4 = true;
                End(Dialog4);
                ActiveEndTutorialPopup();
                this.check = false;
            });
        });        
        DOVirtual.DelayedCall(3f, delegate
        {
            if (this.btnTut3) return;
            End(Dialog3);
            shadow.DOFade(0f, 0.3f);
            shadow.gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
            shadow.gameObject.GetComponent<Button>().onClick.AddListener(() =>
            {
                this.btnTut4 = true;
                End(Dialog4);
                ActiveEndTutorialPopup();
                this.check = false;
            });
            Dialog4.transform.DOScale(Vector3.one, 0.3f).OnComplete(() =>
            {
                DOVirtual.DelayedCall(3f, delegate
                {
                    if (this.btnTut4) return;
                    End(Dialog4);
                    ActiveEndTutorialPopup();
                    this.check = false;
                });
                
            });
        });

    }

    public void ActiveEndTutorialPopup()
    {
        Handtut1.SetActive(false);
        EndTutorialPopup.SetActive(true);
    }
}
