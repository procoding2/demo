using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleExplode : MonoBehaviour
{
    public Action OnDoneAction;
    public void OnDoneAnimation()
    {
        OnDoneAction?.Invoke();
    }
}
