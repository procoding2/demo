using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleColors : MonoBehaviour
{
    [SerializeField]
    private List<ParticleSystem> particles;

    [SerializeField]
    private List<Color> presetColors;

    public void ChangeParticlesColor(int index)
    {
        foreach (ParticleSystem particle in particles)
        {
            var main = particle.main;
            main.startColor = presetColors[index];
        }
    }
}
