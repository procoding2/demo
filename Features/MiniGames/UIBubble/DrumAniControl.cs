using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrumAniControl : MonoBehaviour
{


    [SerializeField]
    private Animator[] animators;

    private Coroutine playAniCoroutine = null;

    // Start is called before the first frame update


    private void OnEnable()
    {
        if (playAniCoroutine != null)
        {
            StopCoroutine(playAniCoroutine);
            playAniCoroutine = null;
        }

        playAniCoroutine = StartCoroutine(NextAniPlay());
    }

    private void PlayAnis()
    {
        float aniSpeed = BubbleBoard.Ins.Items.Count / 50.0f;

        bool isIdleState = !BubbleBoard.Ins.IsDangerState();

        for (int i = 0; i < animators.Length; i++)
        {
            if (animators[i])
            {
                animators[i].SetBool("Idle", isIdleState);
                animators[i].speed = isIdleState ? 1.0f : aniSpeed;
            }
        }
    }


    IEnumerator NextAniPlay()
    {
        yield return new WaitForSeconds(2.0f);
        while (true)
        {
            PlayAnis();
            yield return new WaitForSeconds(Random.Range(0.5f, 2.0f));
        }
    }

}
