using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using OutGameEnum;
using GameEnum;
using TMPro;
using CodeStage.AntiCheat.ObscuredTypes;

public class BubbleBoard : UIBase<BubbleBoard>
{
    protected override void InitIns() => Ins = this;

    public GameObject[] evenRowItems;
    public GameObject[] oddRowItems;
    public GameObject[] firstColItems;
    public GameObject itemOut;

    public GameObject bubbleItemPrefab;
    public GameObject layout;
    public GameObject layoutBoard;
    public GameObject wallBottom;
    public GameObject scoreTxtPrefab;

    public GameObject Gun;
    //public GameObject Wall;

    public List<GameObject> Items;
    public ObscuredInt rowIndex;

    [HideInInspector]
    public float BoardWidth;

    [HideInInspector]
    public Vector2 BoardScale;

    public UIBubble _uiBubble;

    protected override void Start()
    {
        BoardWidth = gameObject.GetComponent<RectTransform>().rect.width;
        float ratio = BoardWidth / 1080f;
        BoardScale = new Vector2(1f, 1f);
        if (ratio < 1f)
        {
            BoardScale = new Vector2(ratio, ratio);
            layoutBoard.transform.localScale = BoardScale;
            var pOffset = layoutBoard.transform.localPosition.y * (1f - ratio) / 1.5f;
            layoutBoard.transform.localPosition = new Vector3(layoutBoard.transform.localPosition.x, layoutBoard.transform.localPosition.y + pOffset, 0f);
            layout.transform.localScale = BoardScale;
        }

        if (ratio > 1f)
        {
            ratio = 1f - (ratio - 1f);
            //float height = GetComponent<RectTransform>().rect.height;
            //float ratio2 = height / 1920f;
            BoardScale = new Vector2(ratio, ratio);

            layoutBoard.transform.localScale = BoardScale;
            var pOffset = layoutBoard.transform.localPosition.y * (1f - ratio) / 1.3f;
            layoutBoard.transform.localPosition = new Vector3(layoutBoard.transform.localPosition.x, layoutBoard.transform.localPosition.y + pOffset, 0f);
            layout.transform.localScale = BoardScale;

            Gun.transform.localScale = BoardScale;

            var pOffsetGun = Gun.transform.localPosition.y * (1f - ratio) / 1.5f;
            Gun.transform.localPosition = new Vector3(Gun.transform.localPosition.x, Gun.transform.localPosition.y + pOffsetGun, 0f);

            BubbleGun.Ins.UpdateWall();
        }
    }

    protected override void OnReloadCryptoKey()
    {
        rowIndex.RandomizeCryptoKey();
    }

    public IEnumerator Init(Action onDone)
    {
        _uiBubble = (UIBubble)UIBubble.Ins;
        var bubbleIns = (UIBubble)UIBubble.Ins;
        Items = new List<GameObject>();

        // Get first 9 rows to display
        // Hide row 0
        for (int row = 0; row < 9; row++)
        {
            var rowTransform = firstColItems[row].transform;
            var rowItems = (row % 2 == 0) ? evenRowItems : oddRowItems;

            //HCSound.Ins.PlayGameSound(HCSound.Ins.Bubble.SpawnBubble);
            SoundManager.instance.PlayEffect($"se_bubblePop{UnityEngine.Random.Range(1,6):00}");
            for (int col = 0; col < 11; col++)
            {
                var colTransform = rowItems[col].transform;
                var item = Instantiate(bubbleItemPrefab, new Vector3(colTransform.position.x, rowTransform.position.y), Quaternion.identity, layout.transform);
                var itemCS = item.GetComponent<BubbleBoardItemPrefab>();
                itemCS.ItemValue = bubbleIns.dataGame.board[row, col];

                item.transform.localPosition = new Vector3(item.transform.localPosition.x, item.transform.localPosition.y, 0f);
                //if (row == 0) itemCS.HideBubbleImage();
                Items.Add(item);

                itemCS.IsProtruding = row % 2 != 0;
                itemCS.Row = row;
                itemCS.Col = col;

                item.transform.localScale = Vector3.zero;
                item.transform.DOScale(Vector3.one, 0.5f);
            }

            rowIndex++;

            yield return new WaitForSeconds(0.1f);
        }

        yield return new WaitForSeconds(0.4f);

        onDone.Invoke();
    }

    public (Vector2, int, int, bool) GetPositionByRowCol(bool isProtruding, int row, int col)
    {
        try
        {
            var rowTransform = firstColItems[row].transform;
            var rowItems = isProtruding ? oddRowItems : evenRowItems;
            var colTransform = rowItems[col].transform;
            return (new Vector2(colTransform.position.x, rowTransform.position.y), row, col, isProtruding);
        }
        catch
        {
            return (new Vector2(0, 0), -1, -1, false);
        }
    }

    public IEnumerator AppendBulletToBoard(
        Vector2 pos,
        int row,
        int col,
        int value,
        int prepareValue,
        bool isProtruding,
        Action<bool> onDestroyAction)
    {
        var item = Instantiate(bubbleItemPrefab, new Vector2(pos.x, pos.y), Quaternion.identity, layout.transform);
        item.transform.localPosition = new Vector3(item.transform.localPosition.x, item.transform.localPosition.y, 0f);
        Items.Add(item);

        var itemCS = item.GetComponent<BubbleBoardItemPrefab>();
        itemCS.IsProtruding = isProtruding;
        itemCS.ItemValue = value;
        itemCS.Row = row;
        itemCS.Col = col;

        var sames = itemCS.SameColors();

        if (sames.Count > 2)
        {
            sames.ForEach(x => Items.Remove(x.gameObject));
            onDestroyAction.Invoke(true);

            int iSameColor = 0;
            foreach (var s in sames)
            {
                yield return new WaitForSeconds(0.01f);
                
                s.Explode(s.ItemValue, onComplete: () =>
                {
                    Sequence sq = DOTween.Sequence();
                    sq.Join(s.transform
                        .DOScale(0.55f, 0.1f));
                    sq.AppendCallback(() => {  ShowBubblePopEffect(s.ItemValue - 1, s.transform.position); });
                    sq.Append(s.transform
                        .DOScale(2.25f, 0.2f)
                        .SetEase(Ease.InOutFlash));

                    sq.Play().OnComplete(() =>
                    {
                        Destroy(s.gameObject);
                    });
                });

                int score = 10;

                if (iSameColor > 2 && iSameColor <= 10)
                {
                    score = 10 * (iSameColor - 1);
                }
                else if(iSameColor > 10)
                {
                    score = 100;
                }

                StartCoroutine(ShowScore(s.gameObject, score, 0));

                iSameColor++;
            }

            GetBubblesInIsland(scoreCallBack: (totalSameIsLand) =>
            {
                _uiBubble.updateScore((BubbleScoreTypeEnum.Change, new BubbleScoreResult.ScoreInfor()
                {
                    Bullet = value,
                    PrepareBullet = prepareValue,
                    BulletPos = (row, col),
                    SameColor = sames.Count,
                    SameInsland = totalSameIsLand
                }));
            });
        }
        else
        {
            itemCS.BlinkAniPlay();
            onDestroyAction.Invoke(false);
        }
    }

    public void GetBubblesInIsland(Action<int> scoreCallBack)
    {
        var lstBubble = Items.Select(x => x.GetComponent<BubbleBoardItemPrefab>())
            .ToList();

        var lstBubbleTopNull = lstBubble
            .Where(x => x.Row > 1 && (x.TopLeft == null && x.TopRight == null))
            .OrderBy(x => x.Row)
            .ThenBy(x => x.Col)
            .ToList();

        if (lstBubbleTopNull.Count == 0)
        {
            scoreCallBack.Invoke(0);
            return;
        }

        var lstSameInsLand = new List<BubbleBoardItemPrefab>();
        foreach (var bbTopNull in lstBubbleTopNull)
        {
            if (lstSameInsLand.Contains(bbTopNull)) continue;

            var lstTemp = bbTopNull.SameInsland();
            if (lstTemp.FirstOrDefault(x => x.Row == 1) == null)
            {
                lstSameInsLand.AddRange(lstTemp);
            }
        }

        scoreCallBack.Invoke(lstSameInsLand.Count);

        Sequence sq = DOTween.Sequence();
        float pos = 0;

        foreach (var item in lstSameInsLand)
        {
            SoundManager.instance.PlayEffect($"se_bubblePop{UnityEngine.Random.Range(1, 6):00}");
            StartCoroutine(ShowScore(item.gameObject, 100, (pos++ * 0.04f)));

            sq.Join(item.gameObject.transform
                .DOMoveY(-6f, 0.7f)
                .SetEase(Ease.InOutFlash)
                .OnComplete(() =>
                {
                    Items.Remove(item.gameObject);
                    Destroy(item.gameObject);
                }));
        }
    }

    private IEnumerator ShowScore(GameObject obj, int score, float after)
    {
        if (score > 0)
        {
            GemDummyTable.instance.GenerateGemItem(obj.transform.position);
        }

        yield return new WaitForSeconds(after);
        if (obj == null) yield break;

        var txt = Instantiate(scoreTxtPrefab, transform, false);
        txt.transform.localScale = Vector3.zero;
        txt.transform.position = obj.transform.position;
        txt.GetComponent<TextMeshProUGUI>().text = $"+{score}";

        float addScale = (score / 10f - 1f) * 0.1f;
        var scale = Vector3.one + new Vector3(addScale, addScale, addScale);

        txt.transform.DOScale(scale, 0.3f);
        txt.transform.DOMoveY(txt.transform.position.y + 0.5f, 1f).OnComplete(() =>
        {
            txt.transform.DOScale(Vector3.zero, 0.3f).OnComplete(() => Destroy(txt));
        });

        _uiBubble.myUserInfo.ShowPointTailEffect(score, obj.transform.position);

        
    }

    private void BubbleWave(BubbleBoardItemPrefab bb)
    {
        //Sequence sq = DOTween.Sequence();

        //var lstNear = bb.NearsByDepth(depth: 2, ignore: bb).OrderByDescending(x => x.Key);
        //float hz = 0.5f;
        //foreach (var near in lstNear)
        //{
        //    foreach (var item in near.Value)
        //    {
        //        var direction = item.gameObject.GetComponent<RectTransform>().position - bb.gameObject.GetComponent<RectTransform>().position;
        //        direction.Normalize();
        //        var newPos = item.gameObject.GetComponent<RectTransform>().localPosition + direction * hz;

        //        sq.Join(item.gameObject.transform.DOMove(newPos, 0.3f)).SetLoops(2, LoopType.Yoyo);
        //    }

        //    hz = hz * 0.5f;
        //}

        //sq.Play();
    }

    public bool CanMoveNextRow() =>
        Items.All(x => x.GetComponent<BubbleBoardItemPrefab>().canMoveNextRow());

    public bool MoveNextRow()
    {
        if (!CanMoveNextRow()) return false;

        Sequence sequence = DOTween.Sequence();
        Sequence sequenceRow0 = DOTween.Sequence();
        List<GameObject> itemsRow0 = new List<GameObject>();

        foreach (var item in Items)
        {
            var itemCS = item.GetComponent<BubbleBoardItemPrefab>();

            // Show topRows
            if (itemCS.Row == 0)
            {
                item.transform.localScale = Vector3.zero;
                itemsRow0.Add(item);
            }

            // Move next / Add to sequence move
            itemCS.Row++;

            var nextRowTransform = firstColItems[itemCS.Row].transform;
            sequence.Join(item.transform.DOMoveY(nextRowTransform.position.y, 0.5f));
        }

        // Start move all
        sequence
        .Play()
        .OnComplete(() =>
        {
            foreach (var item in itemsRow0)
            {
                item.GetComponent<BubbleBoardItemPrefab>().ShowBubbleImage();
                item.transform.DOScale(Vector3.one, 0.5f);
            }
        });

        // ReFill topRows
        ReFill();

        return true;
    }

    private void ReFill()
    {
        var itemRow1 = Items.Select(x => x.GetComponent<BubbleBoardItemPrefab>()).FirstOrDefault(x => x.Row == 1);
        bool isProtruding = !itemRow1.IsProtruding;

        var bubbleIns = (UIBubble)UIBubble.Ins;
        var rowTransform = firstColItems[0].transform;
        var rowItems = isProtruding ? oddRowItems : evenRowItems;

        for (int col = 0; col < 11; col++)
        {
            var colTransform = rowItems[col].transform;
            var item = Instantiate(bubbleItemPrefab, new Vector3(colTransform.position.x, rowTransform.position.y), Quaternion.identity, layout.transform);
            var itemCS = item.GetComponent<BubbleBoardItemPrefab>();
            item.transform.localPosition = new Vector3(item.transform.localPosition.x, item.transform.localPosition.y, 0f);
            //item.transform.localScale = BoardScale;
            Items.Add(item);

            itemCS.IsProtruding = isProtruding;
            itemCS.Row = 0;
            itemCS.Col = col;
            itemCS.ItemValue = bubbleIns.dataGame.board[rowIndex, col];
            //itemCS.HideBubbleImage();
        }

        rowIndex++;
    }

    public void ShowBubblePopEffect(int index, Vector3 pos)
    {
        _uiBubble.ShowBubblePopEffect(index, pos);
    }


    public bool IsDangerState()
    {

        foreach (GameObject obj in Items)
        {
            if (obj.GetComponent<BubbleBoardItemPrefab>()?.Row > 9)
                return true;
        }
        return false;
    }
}
