using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DG.Tweening;
using GameEnum;
using OutGameEnum;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using CodeStage.AntiCheat.ObscuredTypes;

public class UIBubble : UIGameBase<UIBubble>
{
    protected override void InitIns() => Ins = this;

    public GameObject HeartLayout;
    public GameObject BoardLayout;
    public GameObject gunLayout;

    public GameObject scorePrefab;


    public UserInfo myUserInfo, oppnentUserInfo;

    //public RawImage userAvt;
    //public TextMeshProUGUI userName;
    //public Text userScore;

    //public GameObject oppnentView;
    //public RawImage opponentAvt;
    //public TextMeshProUGUI opponentName;
    //public Text opponentScore;

    public TimeView timeView;

    public GameObject BubblePopPrefab = null;

    public GameObject MenuBtn;
    public GameObject PauseMenu;
    public GameObject Toast;
    public GameObject TutorialBtn;
    public GameObject TutorialPopup;

    public GameObject matchResult;

    [HideInInspector]
    public bool IsCanPlay = false;
    private bool _isFire;
    private float _X;
    private float _Y;

    [HideInInspector]
    public BubbleData dataGame = new BubbleData();

    [HideInInspector]
    public BubbleScoreResult result;

    private bool _isPlayingCountDownTimeSound = false;

    private readonly ObscuredFloat MAX_TIME = 180f;

    #region Rx

    /// <summary>
    /// Timer Rx
    /// </summary>
    private bool _isTimerRunning = false;
    private Subject<ObscuredFloat> _timerRx = new Subject<ObscuredFloat>();
    private IDisposable timerRx()
    {
        return _timerRx
            .Scan((pre, curr) =>
            {
                if (UIBubble.sStartType == MiniGameStartTypeEnum.Tutorial) return pre;
                float nextTime = pre - curr;

                return nextTime <= 0 ? 0 : nextTime;
            })
            .Select(timer =>
            {
                if (timer <= 10)
                {
                    if (!_isPlayingCountDownTimeSound)
                    {
                        _isPlayingCountDownTimeSound = true;
                        //HCSound.Ins.PlayGameSound2(HCSound.Ins.MiniGame.TimeCountDown, 10f - timer);
                        SoundManager.instance.PlayUIEffect("se_clock01");
                    }
                    if (timeView && (int)timer > 0)
                    {
                        timeView.SetCountDownText(((int)timer).ToString());
                    }
                }

                if (timer > 10 && _isPlayingCountDownTimeSound)
                {
                    _isPlayingCountDownTimeSound = false;
                    //HCSound.Ins.StopGameSound2();
                }

                result.TimeRemaining = (int)timer;

                return timer == 0
                ? string.Empty
                : (TimeSpan.FromSeconds(timer)).ToString(@"mm\:ss");
            })
            .DistinctUntilChanged()
            .Subscribe(text =>
            {

                timeView?.SetTimeTxt(string.IsNullOrEmpty(text) ? "00:00" : text);

                if (string.IsNullOrEmpty(text))
                {
                    GameFinished(GameFinishTypeEnum.TimesUp);
                }
                else
                {
                    if (dataGame.OScore == null) return;

                    var oScore = dataGame.OScore.Scores
                        .Where(x =>
                        {
                            var xScore = x.Time > 600000 ? "-1" : (TimeSpan.FromSeconds(MAX_TIME - x.Time / 1000f)).ToString(@"mm\:ss");
                            return text == xScore;
                        })
                        .FirstOrDefault();

                    if (oScore != null) oppnentUserInfo.Score = oScore.Score;
                }
            });
    }

    private IDisposable StartTimer()
    {
        return Observable
            .Interval(TimeSpan.FromSeconds(1))
            .Subscribe(x =>
            {
                if (_isTimerRunning)
                {
                    updateTimer(1);
                }
            });
    }

    private void pauseOrResumeTimer(bool isResume)
    {
        //if (_isPlayingCountDownTimeSound)
        //{
        //    if (isResume) HCSound.Ins.FxSoundGame2.UnPause();
        //    else HCSound.Ins.FxSoundGame2.Pause();
        //}

        _isTimerRunning = isResume;

        timeView.SetSecAni(_isTimerRunning);
    }

    public void updateTimer(float time)
    {
        _timerRx.OnNext(time);
    }

    /// <summary>
    /// Score RX
    /// </summary>
    private Subject<(BubbleScoreTypeEnum, BubbleScoreResult.ScoreInfor)> _addScoreRx = new Subject<(BubbleScoreTypeEnum, BubbleScoreResult.ScoreInfor)>();

    private IDisposable increaseScoreRx()
    {
        return _addScoreRx
            .Scan((pre, curr) =>
            {
                BubbleScoreResult.ScoreInfor nextScore = curr.Item2;

                // Score
                for (int i = 1; i <= nextScore.SameColor; i++)
                {
                    if (i <= 3)
                    {
                        nextScore.Score += 10;
                        continue;
                    }

                    if (i <= 11)
                    {
                        nextScore.Score += 10 * (i - 2);
                        continue;
                    }

                    nextScore.Score += 100;
                }

                nextScore.Score += pre.Item2.Score;

                // BonusScore
                nextScore.BonusScore += nextScore.SameInsland * 100;
                nextScore.BonusScore += pre.Item2.BonusScore;

                var type = curr.Item1;
                switch (type)
                {
                    case BubbleScoreTypeEnum.Init:
                        break;
                    case BubbleScoreTypeEnum.Change:
                        result.ScoreInfors.Add(curr.Item2);
                        break;
                }

                return (curr.Item1, nextScore);
            })
            .Subscribe(score =>
            {
                if (_startType == MiniGameStartTypeEnum.Tutorial) return;

                if (result.StartTimeMilliSecond < 1000) return;
                if (string.IsNullOrWhiteSpace(sKey)) return;

                var req = new SaveScoreGameRequest();
                req.key = sKey;
                req.time = _currentTime;
                req.score = score.Item2.Score + score.Item2.BonusScore;
                req.scoreInfo = new SaveScoreGameRequest.ScoreInfo[5];
                req.scoreInfo[0] = new SaveScoreGameRequest.ScoreInfo()
                {
                    name = StringConst.BUBBLE_SCORE_INFO_BULLET,
                    quantity = 1,
                    score = score.Item2.Bullet
                };

                req.scoreInfo[1] = new SaveScoreGameRequest.ScoreInfo()
                {
                    name = StringConst.BUBBLE_SCORE_INFO_PREPARE_BULLET,
                    quantity = 1,
                    score = score.Item2.PrepareBullet
                };

                req.scoreInfo[2] = new SaveScoreGameRequest.ScoreInfo()
                {
                    name = StringConst.BUBBLE_SCORE_INFO_BULLET_POS_X,
                    quantity = 1,
                    score = score.Item2.BulletPos.Item1
                };

                req.scoreInfo[3] = new SaveScoreGameRequest.ScoreInfo()
                {
                    name = StringConst.BUBBLE_SCORE_INFO_BULLET_POS_Y,
                    quantity = 1,
                    score = score.Item2.BulletPos.Item2
                };

                req.scoreInfo[4] = new SaveScoreGameRequest.ScoreInfo()
                {
                    name = StringConst.BUBBLE_SCORE_INFO_SAME_COLORS,
                    quantity = 1,
                    score = score.Item2.SameColor
                };

                req.scoreInfo[4] = new SaveScoreGameRequest.ScoreInfo()
                {
                    name = StringConst.BUBBLE_SCORE_INFO_SAME_ISLAND,
                    quantity = 1,
                    score = score.Item2.SameInsland
                };

                req.scoreInfo[4] = new SaveScoreGameRequest.ScoreInfo()
                {
                    name = StringConst.BUBBLE_SCORE_INFO_SCORES,
                    quantity = 1,
                    score = score.Item2.Score
                };

                StartCoroutine(Current.Ins.gameAPI.SaveLiveScoreGame<SaveScoreGameResponse>(dataGame.gameId, req, (res) => { }));
            });
    }

    public void updateScore((BubbleScoreTypeEnum, BubbleScoreResult.ScoreInfor) score)
    {
        _addScoreRx.OnNext(score);
    }

    private void InitRx()
    {
        DisposeAllRx();

        // Score
        bags.Add(increaseScoreRx());
        _addScoreRx.OnNext((BubbleScoreTypeEnum.Init, new BubbleScoreResult.ScoreInfor()));

        // Timer
        bags.Add(timerRx());
        updateTimer(MAX_TIME);
        pauseOrResumeTimer(true);
        bags.Add(StartTimer());
    }

    #endregion

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();

        PlayerPrefsEx.Set(MiniGameEnum.Bubble.GetStringValue(), 1);
        SoundManager.instance.PlayMusic("bgm_findUser");

        ObjectPoolManager.instance.ReservePool(BubblePopPrefab, 20, true);

        Application.targetFrameRate = 60;
        MenuBtn.GetComponent<Button>().onClickWithHCSound(OnClickMenuBtn);

        if (HomeTab.UIHomeTab.DefaultView != TabbarEnum.Event)
        {
            HomeTab.UIHomeTab.DefaultView = TabbarEnum.Home_GameMode_Bubble;
        }

        switch (UIBubble.sStartType)
        {
            case MiniGameStartTypeEnum.Init:
                Init(UIBubble.sGameMode, UIBubble.sGameId);
                break;
            case MiniGameStartTypeEnum.InitNextGame:
                InitNextGame(UIBubble.sRoomId);
                break;
            case MiniGameStartTypeEnum.InitReMatch:
                InitReMatch(UIBubble.sGameId);
                break;
            case MiniGameStartTypeEnum.Tutorial:
                InitTutorialGame();
                break;
        }

        //TutorialPopup.GetComponent<BingoHowToPlay>().OnClose = () =>
        //{
        //    PauseMenu.SetActive(false);

        //    pauseOrResumeTimer(true);

        //    // Show balloon and resume animation
        //    ShowHideBalloonLayout(true);
        //};

        //TutorialBtn.GetComponent<Button>().onClick.AddListener(() =>
        //{
        //    // Hide balloon and pause animation
        //    ShowHideBalloonLayout(false);

        //    pauseOrResumeTimer(false);
        //    TutorialPopup.SetActive(true);
        //});
    }

    private void OnClickMenuBtn()
    {
        if (_startType == MiniGameStartTypeEnum.Tutorial) return;

        IsCanPlay = false;
        PauseMenu.SetActive(true);
        var pMenu = PauseMenu.GetComponent<PauseMenu>();

        pMenu.ShowPauseMenu(
            cbEndNow: () => GameFinished(GameFinishTypeEnum.GameOver),
            cbResume: () => IsCanPlay = true);
        pMenu.OnClickQuitGametn();
    }

    // Update is called once per frame
    protected override void Update()
    {
        if (IsCanPlay) Touch();

        if (_isTimerRunning)
        {
            _currentTime += Time.deltaTime * 1000f;
            if (_currentTime >= MAX_TIME * 1000f) _currentTime = MAX_TIME * 1000f;
        }

        base.Update();
    }

    protected override void OnReloadCryptoKey()
    {
        MAX_TIME.RandomizeCryptoKey();

        if (result != null)
        {
            result.Score.RandomizeCryptoKey();
            result.TimeRemaining.RandomizeCryptoKey();
        }
    }

    private void Touch()
    {
        if (_IsGameFinished) return;

        if (_startType == MiniGameStartTypeEnum.Tutorial)
        {
            if (TutView.GetComponent<BubbleTutorial>().check != true) return;
            if (!BubbleGun.Ins.IsCanSetNewDirection)
            {
                TutView.GetComponent<BubbleTutorial>().EndTut1();
            }
        }
        if (!BubbleGun.Ins.IsCanSetNewDirection) return;
        if (Input.GetMouseButton(0))
        {
            _isFire = false;
            Vector2 orgPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit2D = Physics2D.Raycast(orgPos, Vector2.zero);

            if (hit2D.collider != null)
            {
                var bb = hit2D.collider.gameObject.GetComponent<BubbleBoardItemPrefab>();
                if (bb == null || (bb != null && bb.IsBullet))
                {
                    return;
                }
            }

            _X = Input.mousePosition.x;
            _Y = Input.mousePosition.y;

            var maxY = HeartLayout.transform.position.y;
            var minY = gunLayout.transform.position.y;

            if (orgPos.y <= minY || orgPos.y >= maxY)
            {
                _X = 0f;
                _Y = 0f;
                gunLayout.GetComponent<BubbleGun>().HideDirection();
                return;
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            _isFire = true;
        }

        if (_Y != 0)
        {
            gunLayout.GetComponent<BubbleGun>().SetDirection(_X, _Y, () =>
            {
                if (_isFire)
                {
                    gunLayout.GetComponent<BubbleGun>().Shot(_X, _Y);
                    _X = 0f;
                    _Y = 0f;
                    return;
                }
            });
        }
    }

    public void GameFinished(GameFinishTypeEnum finishType)
    {
        _IsGameFinished = true;

        SoundManager.instance.PlayMusic("bgm_result");
        SoundManager.instance.PlayUIEffect("se_useClock");
        IsCanPlay = false;

        if (_startType == MiniGameStartTypeEnum.Tutorial)
        {
            TutView.GetComponent<BubbleTutorial>().ActiveEndTutorialPopup();
            return;
        }

        result.EndTimeMilliSecond = result.StartTimeMilliSecond + MAX_TIME;

        if (result.StartTimeMilliSecond == 0)
        {
            result.StartTimeMilliSecond = result.EndTimeMilliSecond;
        }

        if (oppnentUserInfo.gameObject.activeSelf)
        {
            int? lastScore = dataGame.OScore.Scores.LastOrDefault()?.Score;
            if (lastScore != null)
            {
                oppnentUserInfo.Score = lastScore.Value;
            }
        }

        pauseOrResumeTimer(false);
        DisposeAllRx();
        PauseMenu.SetActive(false);

        Action<SaveScoreGameResponse> afterSubmitCallBack = (res) =>
        {
            this.resultView.transform.DOScale(Vector3.one, 0.1f).SetEase(Ease.InOutBack);
            this.resultView.GetComponent<UIBubbleResult>()
                    .submitBtn.GetComponent<Button>()
                        .onClickWithHCSound(() =>
                        {
                            this.resultView.transform.DOScale(Vector3.zero, 0.1f).OnComplete(() => this.matchResult.SetActive(true));
                        });
            switch (dataGame.miniGameMode)
            {
                case MiniGameModeEnum.HeadToHead:
                    var headToHeadData = new MatchHeadToHeadData();
                    headToHeadData.gameId = dataGame.gameId;
                    headToHeadData.isSync = dataGame.isSync;
                    headToHeadData.isOpponentInGame = dataGame.isOppoentInGame;
                    headToHeadData.roomId = dataGame.roomId;

                    if (dataGame.Opponent != null)
                    {
                        headToHeadData.Opponent = new MatchHeadToHeadData.Player();
                        headToHeadData.Opponent.Name = dataGame.Opponent.Name;
                        headToHeadData.Opponent.Avt = dataGame.Opponent.Avt;
                        headToHeadData.Opponent.AvtFrame = dataGame.Opponent.AvtFrame;
                    }

                    matchResult.GetComponent<UIMatchResult>().ShowHeadToHead(res, headToHeadData, sFeeData,
                        acceptReMatchCallBack: (gameId) =>
                        {
                            InitReMatchStaticVariable(gameId);
                            SceneHelper.ReloadScene();
                        },
                        newMatchCallBack: () =>
                        {
                            InitStaticVariable(MiniGameModeEnum.HeadToHead, _gameId, sFeeData);
                            SceneHelper.ReloadScene();
                        });

                    break;

                case MiniGameModeEnum.OneToMany:
                    var oneToManyData = new MatchOneToManyData();
                    oneToManyData.totalPlayer = dataGame.totalPlayer;
                    oneToManyData.gameId = dataGame.gameId;
                    oneToManyData.roomId = dataGame.roomId;

                    LoadOneToManyResult(res.dataDecode.status, dataGame.roomId, matchResult, oneToManyData, res);

                    break;

                case MiniGameModeEnum.KnockOut:
                    var knockOutdata = new KnockOutData();
                    knockOutdata.roomId = dataGame.roomId;
                    knockOutdata.gameId = dataGame.gameId;

                    if (dataGame.Opponent != null)
                    {
                        knockOutdata.Opponent = new KnockOutData.Player();
                        knockOutdata.Opponent.Name = dataGame.Opponent.Name;
                        knockOutdata.Opponent.Avt = dataGame.Opponent.Avt;
                        knockOutdata.Opponent.AvtFrame = dataGame.Opponent.AvtFrame;
                    }

                    matchResult.GetComponent<UIMatchResult>().ShowKnockOut(res, knockOutdata, () =>
                    {
                        InitNextGameStaticVariable(dataGame.roomId);
                        SceneHelper.ReloadScene();
                    });

                    break;
            }
        };

        LoadResult();
        resultView
            .GetComponent<UIBubbleResult>()
            .Init(result, finishType, dataGame.gameId, sKey, afterSubmitCallBack);
    }


    public void ShowBubblePopEffect(int index, Vector3 pos)
    {
        GameObject pop = ObjectPoolManager.instance.GetObject(BubblePopPrefab.name, true, true);
        if (pop)
        {
            pop.transform.localScale = Vector3.one * 150.0f;
            pop.transform.position = pos;
            pop.GetComponent<ParticleColors>()?.ChangeParticlesColor(index);
        }
    }

    protected override void LoadMatchingView()
    {
        base.LoadMatchingView();

        var req = new CreateRoomByMiniGameRequest();
        req.GameId = _gameId;

        UserResponse uSocketRes = null;
        BubbleDataResponse dtRes = null;

        GetUserSecret(() =>
        {
            StartCoroutine(Current.Ins.gameAPI.CreateRoomByMiniGame<BubbleDataResponse>(req, (res) =>
            {
                if (res == null || res.errorCode != 0)
                {
                    OnCallBackAPIError(res);
                    return;
                }

                dtRes = res;
                dtRes.dataDecode = JsonUtility.FromJson<BubbleDataResponse.DataDecode>(EncryptHelper.DecryptWithAES256(res.data.info, sKey));
                _matchData.Response = res;
                _matchData.GameId = res.dataDecode.gameId;
                _matchData.RoomId = res.dataDecode.roomId;
                _matchData.IsSync = res.dataDecode.isSync;

                _matchData.UpdateIsSyncAction = (isSync) => res.dataDecode.isSync = isSync;
                _matchData.UpdateOpponentScoreAction = (oScore) => dataGame.OScore = oScore;
                _matchData.User = res.dataDecode.users.FirstOrDefault();
                if (uSocketRes != null)
                {
                    _matchData.User = new UserResponse();
                    _matchData.User.id = uSocketRes.id;
                    _matchData.User.name = uSocketRes.name;
                    _matchData.User.avatar = uSocketRes.avatar;
                }

                Matching();
                if (_matchData.User == null) _userRx.OnNext(null);
            }));
        });

        Current.Ins.socketAPI.AddSocket<MatchUserSocketResponse>
        (
            StringConst.SOCKET_MATCH_USER_EVENT,
            $"{StringConst.SOCKET_MATCH_USER_EVENT}{Current.Ins.player.Id}",
            (opponentRes) =>
            {
                if (_matchData.GameId != opponentRes.gameId) return;

                if (dtRes == null)
                {
                    uSocketRes = new UserResponse();
                    uSocketRes.id = opponentRes.user.id;
                    uSocketRes.name = opponentRes.user.name;
                    uSocketRes.avatar = opponentRes.user.avatar;
                    uSocketRes.avatarFrame = opponentRes.user.avatarFrame;
                }
                else
                {
                    dtRes.dataDecode.users = new List<UserResponse>()
                    {
                        new UserResponse()
                        {
                            id = opponentRes.user.id,
                            name = opponentRes.user.name,
                            avatar = opponentRes.user.avatar
                        }
                    };
                }

                UpdateOpponentInfo(opponentRes.user.name, opponentRes.user.avatar, opponentRes.user.avatarFrame);
                if (_isInGame) UpdateOpponentInfoIngame(opponentRes);

                _userRx.OnNext(opponentRes.user);
            }
        );
    }

    protected override void UpdateOpponentInfoIngame(MatchUserSocketResponse opp)
    {
        AddJob(() =>
        {
            dataGame.isSync = true;
            dataGame.Opponent = new();
            dataGame.Opponent.Name = opp.user.name;
            dataGame.Opponent.Avt = opp.user.avatar;
            dataGame.Opponent.AvtFrame = opp.user.avatarFrame;

            oppnentUserInfo.gameObject.SetActive(true);
            oppnentUserInfo.UserName.text = dataGame.Opponent.Name;

            Current.Ins.ImageData.AvatarByUrl(dataGame.Opponent.Avt, (result) =>
            {
                oppnentUserInfo.UserAvt.texture = result.Texture;
            });
            //Current.Ins.ImageData.AvatarFrameByUrl(dataGame.Opponent.AvtFrame, (result) =>
            //{
            //    oppnentUserInfo.UserAvtFrame.texture = result.Texture;
            //});
            // 
        });
    }

    protected override void LoadGameContent(IResponse response)
    {
        result = new BubbleScoreResult();

        base.LoadGameContent(response);

        var req = new CreateRoomByMiniGameRequest();

        if (response != null)
        {
            OnCallBackFromAPI(response);
            return;
        }

        GetUserSecret(() =>
        {
            switch (_startType)
            {
                case MiniGameStartTypeEnum.Init:
                    req.GameId = _gameId;
                    StartCoroutine(Current.Ins.gameAPI.CreateRoomByMiniGame<BubbleDataResponse>(req, OnCallBackFromAPI));

                    break;
                case MiniGameStartTypeEnum.InitNextGame:
                    req.RoomId = _roomId;
                    StartCoroutine(Current.Ins.gameAPI.GetNextGame<BubbleDataResponse>(req, OnCallBackFromAPI));

                    break;
                case MiniGameStartTypeEnum.InitReMatch:
                    req.GameId = _gameId;
                    StartCoroutine(Current.Ins.gameAPI.GetGameScript<BubbleDataResponse>(req, OnCallBackFromAPI));
                    break;
            }
        });
    }

    protected override void OnCallBackAPIError(IResponse response)
    {
        BubbleDataResponse res = response as BubbleDataResponse;
        if (res != null && res.errorCode == 4)
        {
            popupSpecialNotice.SetActive(true);
        }
        else
        {
            base.OnCallBackAPIError(response);
        }
    }

    protected async override Task<bool> ParseFromAPI(IResponse responseData)
    {
        var res = (BubbleDataResponse)responseData;

        if (res == null || res.errorCode != 0)
        {
            return false;
        }

        res.dataDecode = JsonUtility.FromJson<BubbleDataResponse.DataDecode>(EncryptHelper.DecryptWithAES256(res.data.info, sKey));
        var gameInfo = res.dataDecode.gameInfo;
        dataGame.gameId = res.dataDecode.gameId.ToString();
        dataGame.roomId = res.dataDecode.roomId.ToString();

        // Bullets
        dataGame.bubbles = res.dataDecode.gameInfo.balls.ToArray();

        // Board
        res.dataDecode.gameInfo.tables.AddRange(res.dataDecode.gameInfo.lines);
        int height = res.dataDecode.gameInfo.tables.Count / 11;
        dataGame.board = ArrayHelper.Make2DArray<int>(res.dataDecode.gameInfo.tables.ToArray(), height, 11);

        // Game Mode
        var gameMode = res.dataDecode.mode;
        dataGame.miniGameMode = gameMode;

        if (gameMode == MiniGameModeEnum.HeadToHead)
        {
            dataGame.isSync = res.dataDecode.isSync;
        }

        // totalPlayer
        dataGame.totalPlayer = res.dataDecode.quantityUser;

        // User and Opponent
        dataGame.User = new BubbleData.Player();
        dataGame.User.Name = Current.Ins.player.Name;
        dataGame.User.Avt = Current.Ins.player.Avatar;
        dataGame.User.AvtFrame = Current.Ins.player.AvatarFrame;

        myUserInfo.UserName.text = dataGame.User.Name;
        //Getavt
        Current.Ins.ImageData.AvatarByUrl(dataGame.User.Avt, (result) =>
        {
            myUserInfo.UserAvt.texture = result.Texture;
        });
        //Current.Ins.ImageData.AvatarFrameByUrl(dataGame.User.AvtFrame, (result) =>
        //{
        //    myUserInfo.UserAvtFrame.texture = result.Texture;
        //});

        UserResponse opponentInfo = null;
        switch (gameMode)
        {
            case MiniGameModeEnum.HeadToHead:
                opponentInfo = res.dataDecode.users.FirstOrDefault();

                if (opponentInfo != null)
                {
                    dataGame.Opponent = new BubbleData.Player();
                    dataGame.Opponent.Name = opponentInfo.name;
                    dataGame.Opponent.Avt = opponentInfo.avatar;
                    dataGame.Opponent.AvtFrame = opponentInfo.avatarFrame;

                    oppnentUserInfo.gameObject.SetActive(true);
                    oppnentUserInfo.UserName.text = dataGame.Opponent.Name;
                    //Getavt
                    Current.Ins.ImageData.AvatarByUrl(dataGame.Opponent.Avt, (result) =>
                    {
                        oppnentUserInfo.UserAvt.texture = result.Texture;
                    });
                    //Current.Ins.ImageData.AvatarFrameByUrl(dataGame.Opponent.AvtFrame, (result) =>
                    //{
                    //    oppnentUserInfo.UserAvtFrame.texture = result.Texture;
                    //});
                }
                else
                {
                    oppnentUserInfo.gameObject.SetActive(false);
                }

                Current.Ins.socketAPI.AddSocket<LiveScoreSocketResponse>
                (
                    StringConst.SOCKET_LIVE_SCORE_EVENT,
                    $"{StringConst.SOCKET_LIVE_SCORE_EVENT}{dataGame.gameId}",
                    onSocketCallBackScore
                );

                break;

            case MiniGameModeEnum.OneToMany:
                oppnentUserInfo.gameObject.SetActive(false);
                break;

            case MiniGameModeEnum.KnockOut:
                opponentInfo = res.dataDecode.users.FirstOrDefault();

                if (!dataGame.isSync)
                {
                    oppnentUserInfo.gameObject.SetActive(false);
                }

                if (opponentInfo != null)
                {
                    dataGame.Opponent = new BubbleData.Player();
                    dataGame.Opponent.Name = opponentInfo.name;
                    dataGame.Opponent.Avt = opponentInfo.avatar;
                    dataGame.Opponent.AvtFrame = opponentInfo.avatarFrame;

                    oppnentUserInfo.UserName.text = dataGame.Opponent.Name;
                    //getAvt
                    Current.Ins.ImageData.AvatarByUrl(dataGame.Opponent.Avt, (result) =>
                    {
                        oppnentUserInfo.UserAvt.texture = result.Texture;
                    });
                    //Current.Ins.ImageData.AvatarFrameByUrl(dataGame.Opponent.AvtFrame, (result) =>
                    //{
                    //    oppnentUserInfo.UserAvtFrame.texture = result.Texture;
                    //});
                }

                break;

            case MiniGameModeEnum.RoundRobin:
                oppnentUserInfo.gameObject.SetActive(false);
                break;
        }

        Current.Ins.socketAPI.AddSocket<ExitGameSocketResponse>
            (
                StringConst.SOCKET_EXIT_GAME_EVENT,
                $"{StringConst.SOCKET_EXIT_GAME_EVENT}{dataGame.gameId}",
                (exitGameRes) =>
                {
                    dataGame.isOppoentInGame = false;
                });

        return await Task.Run(() => true);
    }

    private void onSocketCallBackScore(LiveScoreSocketResponse uLiveScore)
    {
        if (uLiveScore.userId.ToString() == Current.Ins.player.Id) return;

        var oTime = uLiveScore.time;

        if (_currentTime - oTime < -2000f)
        {
            DataUIBase.OpponentScore.ScoreData add = new()
            {
                Time = uLiveScore.time,
                Score = uLiveScore.score
            };

            if (dataGame.OScore == null) dataGame.OScore = new();
            dataGame.OScore.Scores.Add(add);
        }
        else
        {
            AddJob(() => oppnentUserInfo.Score = uLiveScore.score);
        }
    }

    protected override void LoadGamePlay()
    {
        SoundManager.instance.PlayMusic("bgm_bubble");

        base.LoadGamePlay();

        StartCoroutine(BoardLayout.GetComponent<BubbleBoard>().Init(onDone: () =>
        {
            if (_IsGameFinished) return;

            InitRx();
            result.StartTimeMilliSecond = TimeManager.CurrentTimeInMilliSecond;
            IsCanPlay = true;
            if (_startType == MiniGameStartTypeEnum.Tutorial)
            {
                TutView.GetComponent<BubbleTutorial>().BeginTut();
            }
        }));

        gunLayout.GetComponent<BubbleGun>().Init(
            heartAction: () =>
            {
                HeartLayout.GetComponent<BubbleHeart>().setHeart(callBack: (isRefill) =>
                {
                    if (isRefill)
                    {
                        if (!BubbleBoard.Ins.MoveNextRow())
                        {
                            IsCanPlay = false;
                            StartCoroutine(LazyGameOver(GameFinishTypeEnum.GameOver));
                        }
                    }
                });
            },
            endGameAction: (finishType) =>
            {
                IsCanPlay = false;
                StartCoroutine(LazyGameOver(finishType));
            });
    }

    protected override void OnResumeGame(double pauseTime)
    {
        if (!_isTimerRunning) return;

        float pTime = (float)pauseTime / 1000f;
        // Sound
        _isPlayingCountDownTimeSound = false;

        updateTimer(pTime);
    }

    private IEnumerator LazyGameOver(GameFinishTypeEnum finishType)
    {
        yield return new WaitForSeconds(1f);
        GameFinished(finishType);
    }

    /// <summary>
    /// Test Data
    /// </summary>
    private void makeTestData()
    {
        Debug.Log("make TestData");

        dataGame = new BubbleData();

        // Board data
        dataGame.board = new int[100, 11];
        for (int col = 0; col < 11; col++)
        {
            for (int row = 0; row < 100; row++)
            {
                int value = UnityEngine.Random.Range(0, 5);
                dataGame.board[row, col] = value;
            }
        }

        // Bubble data
        dataGame.bubbles = new int[200];
        for (int idx = 0; idx < 200; idx++)
        {
            dataGame.bubbles[idx] = UnityEngine.Random.Range(0, 5);
        }
    }
    #region Tutorial
    public GameObject TutView;
    protected override void LoadTutorialGameContent()
    {
        result = new();
        base.LoadTutorialGameContent();
        MakeTutorialData();
        AfterMakeTutorialData();
    }
    private void MakeTutorialData()
    {
        dataGame = new();

        myUserInfo.UserName.text = Current.Ins.player.Name;
        oppnentUserInfo.gameObject.SetActive(false);
        //getAvt
        Current.Ins.UserAvatar((result) =>
        {
            myUserInfo.UserAvt.texture = result.Texture;
        });

        //Current.Ins.UserAvatarFrame((result) =>
        //{
        //    myUserInfo.UserAvtFrame.texture = result.Texture;
        //});
        MenuBtn.SetActive(false);

        // Board Bullets
        List<int> BulletsList = new();
        List<int> BoardList = new();
        int index = 1;
        for (int i = 0; i < 28; i++)
        {
            for (int j = 0; j < 11; j++)
            {
                BulletsList.Add(index);
            }
            index++;
            if (index == 7) index = 1;
        }
        dataGame.board = ArrayHelper.Make2DArray<int>(BulletsList.ToArray(), 28, 11);
        for (int i = 0; i < 28; i++)
        {
            if (i == 0) continue;
            BoardList.Add(dataGame.board[27 - i, 10]);
        }
        dataGame.bubbles = BoardList.ToArray();
    }
    #endregion
}
