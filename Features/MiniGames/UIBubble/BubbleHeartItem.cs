using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleHeartItem : MonoBehaviour
{
    public GameObject heartOn;
    public GameObject heartOff;

    public void SetHeartState(bool isOn)

    {
        heartOn.GetComponent<Animator>().SetBool("isShow", isOn);
    }
}
