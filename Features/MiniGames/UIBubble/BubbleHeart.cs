using UnityEngine;
using GameEnum;
using System;
using DG.Tweening;
using CodeStage.AntiCheat.ObscuredTypes;
using System.ComponentModel;

public class BubbleHeart : MonoBehaviour
{
    public GameObject heartItem1;
    public GameObject heartItem2;
    public GameObject heartItem3;

    private void Start()
    {
        Current.Ins.PropertyChanged += OnChangeProperty;
    }

    private void OnChangeProperty(object sender, PropertyChangedEventArgs e)
    {
        if (e.PropertyName == StringConst.PROPERTY_ANTICHEAT)
        {
            _headStateInt.RandomizeCryptoKey();
        }
    }

    private ObscuredInt _headStateInt = 3;
    private BubbleHeartStateEnum _heartState
    {
        get
        {
            if (_headStateInt == 3) return BubbleHeartStateEnum.Three;
            else if (_headStateInt == 2) return BubbleHeartStateEnum.Two;
            else if (_headStateInt == 1) return BubbleHeartStateEnum.One;
            else return BubbleHeartStateEnum.Zero;
        }
        set
        {
            _headStateInt = (int)value;
        }
    }

    public void setHeart(Action<bool> callBack)
    {
        switch (_heartState)
        {
            case BubbleHeartStateEnum.Zero:
                _heartState = BubbleHeartStateEnum.Three;
                updateHearState(_heartState);
                callBack.Invoke(true);
                break;
            case BubbleHeartStateEnum.One:
            case BubbleHeartStateEnum.Two:
            case BubbleHeartStateEnum.Three:
                updateHearState(--_heartState);
                callBack.Invoke(false);
                break;
        }
    }

    private void updateHearState(BubbleHeartStateEnum state)
    {
        Sequence sq = DOTween.Sequence();

        switch (state)
        {
            case BubbleHeartStateEnum.Zero:
                heartItem1.GetComponent<BubbleHeartItem>().SetHeartState(false);
                heartItem2.GetComponent<BubbleHeartItem>().SetHeartState(false);
                heartItem3.GetComponent<BubbleHeartItem>().SetHeartState(false);
                break;
            case BubbleHeartStateEnum.One:
                heartItem1.GetComponent<BubbleHeartItem>().SetHeartState(true);
                heartItem2.GetComponent<BubbleHeartItem>().SetHeartState(false);
                heartItem3.GetComponent<BubbleHeartItem>().SetHeartState(false);
                break;
            case BubbleHeartStateEnum.Two:
                heartItem1.GetComponent<BubbleHeartItem>().SetHeartState(true);
                heartItem2.GetComponent<BubbleHeartItem>().SetHeartState(true);
                heartItem3.GetComponent<BubbleHeartItem>().SetHeartState(false);
                break;
            case BubbleHeartStateEnum.Three:
                heartItem1.GetComponent<BubbleHeartItem>().SetHeartState(true);
                heartItem2.GetComponent<BubbleHeartItem>().SetHeartState(true);
                heartItem3.GetComponent<BubbleHeartItem>().SetHeartState(true);
                break;
        }
    }

    private void OnDestroy()
    {
        Current.Ins.PropertyChanged -= OnChangeProperty;
    }
}
