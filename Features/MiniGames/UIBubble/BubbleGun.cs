using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using GameEnum;
using CodeStage.AntiCheat.ObscuredTypes;

public class BubbleGun : UIBase<BubbleGun>
{
    protected override void InitIns() => Ins = this;

    public GameObject bubbleReady;
    public ObscuredFloat bubbleSpeed;
    public GameObject bubblePrepare;
    public GameObject gunObject;
    public GameObject gunImgObject;
    public Button swapBtn;


    public GameObject bubble;
    public GameObject bubblePrefab;
    public GameObject linePrefab;

    public GameObject smallBulletLayout;

    public GameObject wallLeft;
    public GameObject wallRight;
    public GameObject wallTop;

    public GameObject GunAnimation;


    private bool _isSwaping = false;
    private Sequence _sqNextBullet;

    // View
    private float _widthView;
    private Vector2 _initBubbleReadyPOS = new Vector2(0, 0);
    private Vector2 _initBubblePreparePOS = new Vector2(0, 0);

    // Shot
    private bool _isMoving = false;
    private float _degreeBubbleReady = 0f;
    private Vector2 _bubbleHoriDirection = new Vector2(0, 0);
    private float _bubbleReadyAdjacentConnerRad = 0f;
    private BubbleBoardItemPrefab _bubbleReadyTemp;
    private BubbleBoardItemPrefab _bubblePrepareTemp;

    // Line
    private bool _isDrawLine = false;
    private readonly float _maxWidthLine = 5.7f; // Unit;
    private GameObject _simuBall;

    public Action _heartAction;
    public Action<GameFinishTypeEnum> _endGameAction;

    //Gear
    public GameObject GearObj;
    private float GearRotateRemainValue = 0.0f;

    [HideInInspector]
    public bool IsCanSetNewDirection { get { return !_isMoving && !_isSwaping; } }

    public void Init(Action heartAction, Action<GameFinishTypeEnum> endGameAction)
    {
        _heartAction = heartAction;
        _endGameAction = endGameAction;

        _widthView = Mathf.Abs(wallLeft.transform.position.x * 2);

        StartCoroutine(InitBullet(onDone: () => 
        {
            swapBtn.onClick.AddListener(SwapBubble);
            gunImgObject.GetComponent<Button>().onClick.AddListener(SwapBubble);
        }));
    }

    public void UpdateWall()
    {
        _widthView = Mathf.Abs(wallLeft.transform.position.x * 2);
    }

    protected override void Start()
    {
        GunAnimation.SetActive(false);
        InitLineObjects();
    }
    override protected void Update()
    {
        if (_isDrawLine)
        {
            DrawLine(isDrawSimuBall: true);
        }

        if (_isMoving && _bubbleReadyTemp != null)
        {
            if (BubbleBoard.Ins._uiBubble.IsCanPlay) BubbleMove();
        }

        if (Mathf.Abs(GearRotateRemainValue) < 0.01f)
        {
            GearRotateRemainValue = 0.0f;
        }
        else
        {
            if (GearRotateRemainValue > 0)
            {
                GearRotateRemainValue -= Time.deltaTime;

                GearObj.transform.eulerAngles = new Vector3(GearObj.transform.eulerAngles.x,
                                                            GearObj.transform.eulerAngles.y,
                                                            Mathf.LerpAngle(GearObj.transform.eulerAngles.z, GearObj.transform.eulerAngles.z + 10.0f, Time.deltaTime * 20.0f));
            }
            else
            {
                GearRotateRemainValue += Time.deltaTime;
                GearObj.transform.eulerAngles = new Vector3(GearObj.transform.eulerAngles.x,
                                                            GearObj.transform.eulerAngles.y,
                                                            Mathf.LerpAngle(GearObj.transform.eulerAngles.z, GearObj.transform.eulerAngles.z - 10.0f, Time.deltaTime * 20.0f));
            }
        }

        base.Update();
    }


    private ObscuredInt _bubbleIdx = 0;

    protected override void OnReloadCryptoKey()
    {
        bubbleSpeed.RandomizeCryptoKey();
        _bubbleIdx.RandomizeCryptoKey();
    }

    public IEnumerator InitBullet(Action onDone)
    {
        var bubbleIns = (UIBubble)UIBubble.Ins;

        _bubbleReadyTemp = Instantiate(bubblePrefab, new Vector3(bubbleReady.transform.position.x, bubbleReady.transform.position.y, bubbleReady.transform.position.z), Quaternion.identity, bubble.transform).GetComponent<BubbleBoardItemPrefab>();
        _bubbleReadyTemp.InitBullet(bubbleIns.dataGame.bubbles[_bubbleIdx++]);
        _bubbleReadyTemp.transform.localScale = Vector3.zero;
        _bubbleReadyTemp.transform.DOScale(Vector3.one, 0.5f);

        _bubblePrepareTemp = Instantiate(bubblePrefab, new Vector3(bubblePrepare.transform.position.x, bubblePrepare.transform.position.y, bubblePrepare.transform.position.z), Quaternion.identity, bubble.transform).GetComponent<BubbleBoardItemPrefab>();
        _bubblePrepareTemp.InitBullet(bubbleIns.dataGame.bubbles[_bubbleIdx++]);
        _bubblePrepareTemp.transform.parent = bubblePrepare.transform;
        _bubblePrepareTemp.transform.localScale = Vector3.zero;
        _bubblePrepareTemp.transform.DOScale(Vector3.one, 0.5f);

        yield return new WaitForSeconds(1.5f);
        onDone.Invoke();
    }

    private void GetNextBubble()
    {
        var bubbleIns = (UIBubble)UIBubble.Ins;

        _isSwaping = true;
        _isMoving = false;

        int readyValue = _bubblePrepareTemp.ItemValue;
        int prepareValue = 0;

        if (_bubbleIdx >= bubbleIns.dataGame.bubbles.Length - 1) _bubbleIdx = 0;

        prepareValue = bubbleIns.dataGame.bubbles[_bubbleIdx++];

        #region Remove clear game logic
        //var values = BubbleBoard.Ins.Items
        //    .Select(x => x.GetComponent<BubbleBoardItemPrefab>())
        //    .Where(x => x.Row > 0)
        //    .Select(x => x.ItemValue)
        //    .Distinct()
        //    .ToList();
        //var total = values.Count();

        //if (_bubbleIdx >= bubbleIns.dataGame.bubbles.Length - 1)
        //    _bubbleIdx = 0;

        //if (total > 3) prepareValue = bubbleIns.dataGame.bubbles[_bubbleIdx++];
        //else
        //{
        //    if (total == 0) prepareValue = bubbleIns.dataGame.bubbles[_bubbleIdx++];
        //    else if (total < 2)
        //    {
        //        readyValue = values[0];
        //        prepareValue = bubbleIns.dataGame.bubbles[_bubbleIdx++];
        //    }
        //    else
        //    {
        //        readyValue = values[0];
        //        prepareValue = values[1];
        //    }
        //}
        #endregion

        _bubbleReadyTemp = Instantiate(bubblePrefab, new Vector3(bubbleReady.transform.position.x, bubbleReady.transform.position.y, bubbleReady.transform.position.z), Quaternion.identity, bubble.transform).GetComponent<BubbleBoardItemPrefab>();
        _bubbleReadyTemp.gameObject.transform.localScale = Vector3.zero;
        _bubbleReadyTemp.InitBullet(readyValue);

        _sqNextBullet = DOTween.Sequence();
        _sqNextBullet
            .Join(_bubbleReadyTemp.gameObject.transform.DOScale(Vector3.one, 0.15f))
            .Join(_bubblePrepareTemp.gameObject.transform.DOScale(Vector3.zero, 0.15f))
            .OnComplete(() =>
            {
                Destroy(_bubblePrepareTemp.gameObject);
                _bubblePrepareTemp = Instantiate(bubblePrefab, new Vector3(bubblePrepare.transform.position.x, bubblePrepare.transform.position.y, bubblePrepare.transform.position.z), Quaternion.identity, bubble.transform).GetComponent<BubbleBoardItemPrefab>();
                _bubblePrepareTemp.transform.parent = bubblePrepare.transform;
                _bubblePrepareTemp.gameObject.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                _bubblePrepareTemp.InitBullet(prepareValue);
                _bubblePrepareTemp.gameObject.transform.DOScale(Vector3.one, 0.1f).OnComplete(() => _isSwaping = false);
            });
    }

    private void OnBulletTouch(GameObject obj)
    {
        if (_isSwaping || !_isMoving) return;

        _isMoving = false;
        var value = _bubbleReadyTemp.GetComponent<BubbleBoardItemPrefab>().ItemValue;
        
        obj.GetComponent<BubbleBoardItemPrefab>().GetBubbleByCollision(_bubbleReadyTemp.gameObject, (des) =>
        {

            SoundManager.instance.PlayEffect("se_bubblePop03");

            var prepareBulletValue = _bubblePrepareTemp.GetComponent<BubbleBoardItemPrefab>().ItemValue;
            StartCoroutine(
              BubbleBoard.Ins.AppendBulletToBoard(
                pos: des.Item1,
                row: des.Item2,
                col: des.Item3,
                isProtruding: des.Item4,
                value: value,
                prepareValue: prepareBulletValue,
                onDestroyAction: (isDestroyed) =>
                {
                    if (!isDestroyed) _heartAction?.Invoke();

                    Destroy(_bubbleReadyTemp.gameObject);

                    GetNextBubble();

                    if (!BubbleBoard.Ins.CanMoveNextRow())
                    {
                        _endGameAction?.Invoke(GameFinishTypeEnum.GameOver);
                        return;
                    }

                    var bbs = BubbleBoard.Ins.Items
                    .Select(x => x.GetComponent<BubbleBoardItemPrefab>())
                    .Where(x => x.Row > 0);

                    if (bbs.Count() == 0)
                    {
                        _endGameAction?.Invoke(GameFinishTypeEnum.Success);
                        return;
                    }
                }));
        });
    }

    private void BubbleMove()
    {
        if (_simuBall != null) _simuBall.SetActive(false);// Destroy(_simuBall);
        _smallBubbles.ForEach(x => x.SetActive(false));

        var distance = Time.deltaTime;
        var deltaTimeX = bubbleSpeed * distance * Mathf.Cos(_bubbleReadyAdjacentConnerRad);
        var deltaTimeY = bubbleSpeed * distance * Mathf.Sin(Mathf.Abs(_bubbleReadyAdjacentConnerRad));

        float nextX = _bubbleReadyTemp.transform.position.x + _bubbleHoriDirection.x * deltaTimeX;
        float nextY = _bubbleReadyTemp.transform.position.y + deltaTimeY;

        if (nextX < -_widthView / 2f)
        {
            _bubbleHoriDirection = Vector2.right;
            float dtsY = (Mathf.Abs(nextX) - (_widthView / 2f)) * Mathf.Tan(_bubbleReadyAdjacentConnerRad);
            nextX = -_widthView / 2f;
            nextY = nextY - Mathf.Abs(dtsY);
        }
        else if (nextX > _widthView / 2f)
        {
            _bubbleHoriDirection = Vector2.left;
            float dtsY = (Mathf.Abs(nextX) - (_widthView / 2f)) * Mathf.Tan(_bubbleReadyAdjacentConnerRad);
            nextX = _widthView / 2f;
            nextY = nextY - Mathf.Abs(dtsY);
        }

        _bubbleReadyTemp.transform.position = new Vector3(nextX, nextY);

        var r = _bubbleReadyTemp.GetComponent<CircleCollider2D>().radius * BubbleBoard.Ins.BoardScale.x / 100f;

        foreach (var item in BubbleBoard.Ins.Items)
        {
            if (Vector2.Distance(_bubbleReadyTemp.transform.position, item.transform.position) <= r)
            {
                OnBulletTouch(item);
                return;
            }
        }

        //for (int i = 0; i < _smallBubbles.Count; i++)
        //{
        //    if (i % 2 == 0)
        //    {
        //        var item = _smallBubbles[i];
        //        if (_bubbleReadyTemp == null) break;
        //        if (Vector2.Distance(_bubbleReadyTemp.transform.position, item.transform.position) <= r)
        //        {
        //            item.GetComponent<BubbleBoardItemPrefab>().LazyShowAndHideSmallBubble(() =>
        //            {
        //                _smallBubbles.Remove(item);
        //                //Destroy(item);
        //            });

        //            break;
        //        }
        //    }
        //}
    }

    [HideInInspector]
    public float BubbleDirectionX { get; private set; }
    [HideInInspector]
    public float BubbleDirectionY { get; private set; }

    [HideInInspector]
    public float GunDirectionX { get; private set; }
    [HideInInspector]
    public float GunDirectionY { get; private set; }
    public bool IsDrawLine { get => _isDrawLine;}

    public void HideDirection()
    {
        _isDrawLine = false;

        if (_simuBall != null) _simuBall.SetActive(false);// Destroy(_simuBall);
        _smallBubbles.ForEach(x => x.SetActive(false));
    }

    public void SetDirection(float x, float y, TweenCallback callBack = null)
    {
        if (_isSwaping || _isMoving) return;


        GearRotateRemainValue += (GunDirectionX - x) * 0.0025f;


        GunDirectionX = x;
        GunDirectionY = y;

        gunObject.transform
            .DORotateQuaternion(Quaternion.AngleAxis(getAngleDegree(gunObject), Vector3.back), 0.01f)
            .OnComplete(() =>
            {
                _isDrawLine = true;
                callBack?.Invoke();
            });
    }

    public void Shot(float x, float y)
    {

        _isDrawLine = false;
        if (_isSwaping || _isMoving) return;

        //HCSound.Ins.PlayGameSound(HCSound.Ins.Bubble.ShootGun);
        SoundManager.instance.PlayEffect("se_smoke");
        GunDirectionX = x;
        GunDirectionY = y;
        _initBubbleReadyPOS = _bubbleReadyTemp.transform.position;
        _initBubblePreparePOS = bubblePrepare.transform.position;

        _degreeBubbleReady = getAngleDegree(_bubbleReadyTemp.gameObject);

        _bubbleReadyAdjacentConnerRad = Mathf.Deg2Rad * (90f - Mathf.Abs(_degreeBubbleReady));
        _bubbleHoriDirection = _degreeBubbleReady <= 0 ? Vector2.left : Vector2.right;


        if (GunAnimation.activeSelf)
            GunAnimation.SetActive(false);

        GunAnimation.SetActive(true);
        //GunAnimation.GetComponent<Animator>().speed = 10f;
        //GunAnimation.GetComponent<BubbleExplode>().OnDoneAction = () => GunAnimation.SetActive(false);
        DrawLine();
        //StartCoroutine(DrawLine());
        //_smallBubbles.ForEach(x => x.GetComponent<BubbleBoardItemPrefab>().HideBubbleImage());

        Sequence sq = DOTween.Sequence();
        sq.Join(gunImgObject.transform.DOScale(1.1f, 0.1f));
        sq.Append(gunImgObject.transform.DOScale(0.95f, 0.05f));
        sq.Append(gunImgObject.transform.DOScale(1.0f, 0.05f));
        sq.Append(gunImgObject.transform.DOScale(1.05f, 0.025f));
        sq.Append(gunImgObject.transform.DOScale(1.0f, 0.015f));
        sq.Play();


        _isMoving = true;
    }

    public void SwapBubble()
    {
        if (_isSwaping || _isMoving) return;

        //HCSound.Ins.PlayGameSound(HCSound.Ins.Bubble.SwapBubble);
        SoundManager.instance.PlayUIEffect("se_ether");
        _isSwaping = true;

        var readyValue = _bubbleReadyTemp.ItemValue;
        var prepareValue = _bubblePrepareTemp.ItemValue;
        if (_bubbleReadyTemp == null || _bubbleReadyTemp.gameObject == null || _bubblePrepareTemp == null || _bubblePrepareTemp.gameObject == null) return;

        Destroy(_bubbleReadyTemp.gameObject);
        Destroy(_bubblePrepareTemp.gameObject);

        _bubbleReadyTemp = Instantiate(bubblePrefab, new Vector3(bubblePrepare.transform.position.x, bubblePrepare.transform.position.y, bubblePrepare.transform.position.z), Quaternion.identity, bubble.transform).GetComponent<BubbleBoardItemPrefab>();
        _bubbleReadyTemp.InitBullet(prepareValue);

        _bubblePrepareTemp = Instantiate(bubblePrefab, new Vector3(bubbleReady.transform.position.x, bubbleReady.transform.position.y, bubbleReady.transform.position.z), Quaternion.identity, bubble.transform).GetComponent<BubbleBoardItemPrefab>();
        _bubblePrepareTemp.InitBullet(readyValue);

        Sequence sq = DOTween.Sequence();

        sq.Join(_bubbleReadyTemp.gameObject.transform.DOMove(_bubblePrepareTemp.gameObject.transform.position, 0.2f))
            .Join(_bubblePrepareTemp.gameObject.transform.DOMove(_bubbleReadyTemp.gameObject.transform.position, 0.2f))
            .OnComplete(() =>
            {
                _bubbleReadyTemp.transform.parent = bubbleReady.transform;
                _bubblePrepareTemp.transform.parent = bubblePrepare.transform;
                _isSwaping = false;
            });

        sq.Play();
    }

    private List<GameObject> _smallBubbles = new List<GameObject>();
    private float _oldAngleDegree = 0f;
    private readonly float _distanceMiniBall = 0.125f;
    private readonly int _totalSmallBalls = 52;
    //private IEnumerator DrawLine(bool isDrawSimuBall = false)


    private void InitLineObjects()
    {
        _smallBubbles.Clear();
        for (int i = 0; i < _totalSmallBalls; i++)
        {
            GameObject prevBall = Instantiate(linePrefab);
            prevBall.transform.SetParent(smallBulletLayout.transform);
            prevBall.transform.localScale = Vector3.one * 0.75f;
            _smallBubbles.Add(prevBall);
            prevBall.SetActive(false);
        }
    }


    private void DrawLine(bool isDrawSimuBall = false)
    {
        if (_bubbleReadyTemp == null) 
            return; // yield break;

        var angleDegree = getAngleDegree(_bubbleReadyTemp.gameObject);
        if (Mathf.Approximately(_oldAngleDegree, angleDegree)) 
            return;// yield break;

        _oldAngleDegree = angleDegree;

        var angleForMiniBall = angleDegree;

        var halfWidth = _widthView / 2f;

        var adjacentConnerRad = Mathf.Deg2Rad * (90f - Mathf.Abs(angleForMiniBall));

        if (_simuBall != null) _simuBall.SetActive(false);////Destroy(_simuBall);
        //_smallBubbles.ForEach(x => Destroy(x));
        //_smallBubbles.Clear();
        //_smallBubbles = new List<GameObject>();

        //GameObject prevBall = Instantiate(bubblePrefab, new Vector3(bubbleReady.transform.position.x, bubbleReady.transform.position.y, bubbleReady.transform.position.z), Quaternion.identity, smallBulletLayout.transform);
        //prevBall.GetComponent<BubbleBoardItemPrefab>().InitSmallBullet(_bubbleReadyTemp.ItemValue);
        //_smallBubbles.Add(prevBall);

        _smallBubbles.ForEach(x => x.SetActive(false));
        GameObject prevBall = _smallBubbles[0];
        prevBall.transform.position = bubbleReady.transform.position;

        int countSmalllBubbleNewWay = 0;

        for (int i = 1; i < _totalSmallBalls; i++)
        {
            if (countSmalllBubbleNewWay >= 12) return;// yield break;
            if (countSmalllBubbleNewWay > 0) countSmalllBubbleNewWay++;

            //GameObject smlBall = Instantiate(bubblePrefab, new Vector3(bubbleReady.transform.position.x, bubbleReady.transform.position.y, bubbleReady.transform.position.z), Quaternion.identity, smallBulletLayout.transform);
            //smlBall.GetComponent<BubbleBoardItemPrefab>().InitSmallBullet(_bubbleReadyTemp.ItemValue);


            GameObject smlBall = _smallBubbles[i];
            smlBall.SetActive(true);
            //smlBall.transform.localPosition = new Vector3(smlBall.transform.localPosition.x, smlBall.transform.localPosition.y, 0f);

            //_smallBubbles.Add(smlBall);
            float x = (angleForMiniBall <= 0 ? -1 : 1) * _distanceMiniBall * Mathf.Cos(adjacentConnerRad) + prevBall.transform.position.x;
            float y = _distanceMiniBall * Mathf.Sin(adjacentConnerRad) + prevBall.transform.position.y;

            smlBall.transform.position = new Vector2(x, y);
            prevBall.GetComponent<Image>().color = _bubbleReadyTemp.GetBubbleColor;
            prevBall = smlBall;

            float exDistanceX = Mathf.Abs(x) - halfWidth;
            if (exDistanceX > 0)
            {
                var smlBallPos = smlBall.transform.position;
                float addBallX = smlBallPos.x < 0 ? -halfWidth : halfWidth;
                float dtsY = exDistanceX * Mathf.Tan(adjacentConnerRad);
                float addBallY = smlBallPos.y - dtsY;
                smlBall.transform.position = new Vector3(addBallX, addBallY, smlBallPos.z);

                angleForMiniBall = -angleForMiniBall;
                countSmalllBubbleNewWay++;
            }
            else if (exDistanceX < 0 && i % 2 == 0)
            {
                smlBall.SetActive(false);
                //smlBall.GetComponent<BubbleBoardItemPrefab>().HideBubbleImage();
            }

            foreach (var item in BubbleBoard.Ins.Items)
            {
                var radius = item.GetComponent<CircleCollider2D>().radius * BubbleBoard.Ins.BoardScale.x / 100f;
                var dts = Vector2.Distance(smlBall.transform.position, item.transform.position);
                if (dts <= radius)
                {
                    if (isDrawSimuBall)
                    {
                        smlBall.SetActive(false);// GetComponent<BubbleBoardItemPrefab>().HideBubbleImage();
                        item.GetComponent<BubbleBoardItemPrefab>().GetBubbleByCollision(smlBall, (des) =>
                        {

                            if (_simuBall == null)
                            {
                                _simuBall = Instantiate(bubblePrefab, smlBall.transform.position, Quaternion.identity, bubble.transform);
                                
                            }
                            _simuBall.SetActive(true);
                            _simuBall.GetComponent<BubbleBoardItemPrefab>().InitSimu(_bubbleReadyTemp.GetBubbleColor);

                            _simuBall.transform.DOMove(new Vector3(des.Item1.x, des.Item1.y, smlBall.transform.position.z), 0.4f).SetEase(Ease.OutQuint);
                            //_simuBall.transform.position = new Vector3(des.Item1.x, des.Item1.y, smlBall.transform.position.z);
                        });
                    }

                    return;//yield break;
                }
            }
        }
    }

    //private void DrawMiniBall(GameObject item, float radius, float angleForMiniBall, float adjacentConnerRad)
    //{
    //    GameObject ballA = _smallBubbles[_smallBubbles.Count - 2];
    //    GameObject ballB = _smallBubbles[_smallBubbles.Count - 1];

    //    float xA = ballA.transform.position.x;
    //    float yA = ballA.transform.position.y;
    //    float zA = ballA.transform.position.z;

    //    float xB = ballB.transform.position.x;
    //    float yB = ballB.transform.position.y;

    //    float dis = _distanceMiniBall / 2;

    //    float xPre = xA;
    //    float yPre = yA;
    //    Tuple<Vector2, float> minDis = new(new Vector2(xB, yB), _distanceMiniBall);

    //    for (int i = 0; i < 2; i++)
    //    {
    //        float x = (angleForMiniBall <= 0 ? -1 : 1) * dis * Mathf.Cos(adjacentConnerRad) + xPre;
    //        float y = dis * Mathf.Sin(adjacentConnerRad) + yPre;

    //        Vector2 vt = new (x, y);
    //        // check mini ball
    //        float d = Vector2.Distance(vt, item.transform.position);
    //        if (d <= radius)
    //        {
    //            float dA = Vector2.Distance(vt, ballA.transform.position);

    //            if (dA < minDis.Item2) minDis = new (vt, dA);
    //        }

    //        xPre = x;
    //        yPre = y;
    //    }

    //    _simuBall = Instantiate(bubblePrefab, new Vector3(minDis.Item1.x, minDis.Item1.y, zA), Quaternion.identity, bubble.transform);
    //    _simuBall.GetComponent<BubbleBoardItemPrefab>().InitSimu();

    //    item.GetComponent<BubbleBoardItemPrefab>().GetBubbleByCollision(_simuBall, (des) =>
    //    {
    //        _simuBall.transform.position = new Vector3(des.Item1.x, des.Item1.y, zA);
    //    });

    //    ballB.GetComponent<BubbleBoardItemPrefab>().HideBubbleImage();
    //}

    private float getAngleDegree(GameObject obj)
    {
        var objPos = Camera.main.WorldToScreenPoint(obj.transform.position);
        return 90f - (180f * Mathf.Atan2(GunDirectionY - objPos.y, GunDirectionX - objPos.x) / Mathf.PI);
    }
}