using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using CodeStage.AntiCheat.ObscuredTypes;
using System.ComponentModel;

public class BubbleBoardItemPrefab : MonoBehaviour
{

    [HideInInspector]
    public bool IsBullet;

    [HideInInspector]
    public bool IsSmallBullet;

    [SerializeField]
    private GameObject transparentObj;
    public Image TransparentImage => transparentObj.GetComponent<Image>();

    [SerializeField]
    private List<Sprite> bubbleSprites;
    [SerializeField]
    private List<Sprite> openEyeSprites;
    [SerializeField]
    private List<Sprite> closeEyeSprites;

    [SerializeField]
    private Image bodyImage, eyeImage;

    public List<Color> BubbleColors = new List<Color>();

    public Action<GameObject> OnTouched;

    [HideInInspector]
    public bool IsDestroyed { get; set; }

    private void Start()
    {
        Current.Ins.PropertyChanged += OnChangeProperty;
    }

    private void OnChangeProperty(object sender, PropertyChangedEventArgs e)
    {
        if (e.PropertyName == StringConst.PROPERTY_ANTICHEAT)
        {
            _itemValue.RandomizeCryptoKey();
            ItemValue.RandomizeCryptoKey();
            Row.RandomizeCryptoKey();
            Col.RandomizeCryptoKey();
            IsProtruding.RandomizeCryptoKey();
        }
    }

    public Color GetBubbleColor => BubbleColors[_itemValue - 1];
    private IEnumerator FacialMoving()
    {
        while (true)
        {
            

            if (UnityEngine.Random.Range(0, 6) == 0)
            {
                eyeImage.transform.DOLocalMove(new Vector2(UnityEngine.Random.Range(-10.0f, 10.0f), UnityEngine.Random.Range(-10.0f, 10.0f)), UnityEngine.Random.Range(0.2f, 0.5f)).SetEase(Ease.InOutFlash);
            }
            if (UnityEngine.Random.Range(0, 4) == 0)
            {
                StartCoroutine(BlinkAni());
            }

            yield return new WaitForSeconds(UnityEngine.Random.Range(4.0f, 8.0f));
        }
    }
    IEnumerator BlinkAni()
    {
        eyeImage.sprite = closeEyeSprites[ItemValue - 1];
        yield return new WaitForSeconds(0.3f);
        eyeImage.sprite = openEyeSprites[ItemValue - 1];
    }

    public void BlinkAniPlay()
    {
        StartCoroutine(BlinkAni());
        


        Sequence sq = DOTween.Sequence();
        sq.Join(transform.DOScale(0.8f, 0.1f)).SetEase(Ease.InOutFlash);
        sq.Append(transform.DOScale(1.2f, 0.05f)).SetEase(Ease.InOutFlash);
        sq.Append(transform.DOScale(1.0f, 0.025f)).SetEase(Ease.InOutFlash);

        sq.Play();

    }


    public void InitBullet(int itemValue)
    {
        ItemValue = itemValue;
        IsBullet = true;
    }

    public void InitSmallBullet(int itemValue)
    {
        ItemValue = itemValue;
        IsSmallBullet = true;
        transform.localScale = Vector3.one * 0.2f;
    }
    public void InitSimu(Color color)
    {
        ItemValue = -1;
        TransparentImage.color = color;
    }
    public void Explode(int itemValue, Action onComplete)
    {
        //HCSound.Ins.PlayGameSound(HCSound.Ins.Bubble.ExplodeBubble, true);

        SoundManager.instance.PlayEffect($"se_bubblePop{UnityEngine.Random.Range(1, 6):00}");


        onComplete.Invoke();
    }

    //public void ExplodeBottom(Action onComplete)
    //{
    //    HideBubbleImage();
    //    explode_bottom.SetActive(true);
    //    explode_bottom.GetComponent<Animator>().speed = 8f;
    //    explode_bottom.GetComponent<BubbleExplode>().OnDoneAction = onComplete;
    //}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!IsBullet) return;



        var ball = collision.gameObject.GetComponent<BubbleBoardItemPrefab>();
        if (ball == null || ball.IsBullet || ball.IsSmallBullet)
            return;


        //if (collision.gameObject.GetComponent<BubbleBoardItemPrefab>() == null) return;
        //if (collision.gameObject.GetComponent<BubbleBoardItemPrefab>().IsBullet) return;
        //if (collision.gameObject.GetComponent<BubbleBoardItemPrefab>().IsSmallBullet) return;

        if (IsBullet)
        {
            OnTouched?.Invoke(collision.gameObject);
        }
    }



    private ObscuredInt _itemValue;
    public ObscuredInt ItemValue
    {
        get { return _itemValue; }
        set
        {
            _itemValue = value;

            if (_itemValue == -1)
            {
                transparentObj.SetActive(true);
                bodyImage.gameObject.SetActive(false);
                eyeImage.gameObject.SetActive(false);
                bodyImage.sprite = bubbleSprites[0];

            }
            else
            {
                transparentObj.SetActive(false);
                bodyImage.gameObject.SetActive(true);
                eyeImage.gameObject.SetActive(true);
                bodyImage.sprite = bubbleSprites[_itemValue - 1];
                eyeImage.sprite = openEyeSprites[_itemValue - 1];

                StopAllCoroutines();
                StartCoroutine(FacialMoving());
            }


            //switch (value)
            //{
            //    case -1:
            //        bubble_Simu.SetActive(true);
            //        break;
            //    case 1:
            //        bubble_0.SetActive(true);
            //        break;
            //    case 2:
            //        bubble_1.SetActive(true);
            //        break;
            //    case 3:
            //        bubble_2.SetActive(true);
            //        break;
            //    case 4:
            //        bubble_3.SetActive(true);
            //        break;
            //    case 5:
            //        bubble_4.SetActive(true);
            //        break;
            //    case 6:
            //        bubble_5.SetActive(true);
            //        break;
            //    default: break;
            //}
        }
    }

    public void HideBubbleImage()
    {
        transparentObj.SetActive(false);
        bodyImage.gameObject.SetActive(false);
        eyeImage.gameObject.SetActive(false);
    }

    Sequence sqShowHideSmallBubble;
    public void LazyShowAndHideSmallBubble(Action onComplete)
    {
        sqShowHideSmallBubble = DOTween.Sequence();
        ItemValue = _itemValue;
        transform.localScale = Vector3.one * 0.6f;

        sqShowHideSmallBubble.Join(transform.DOScale(0f, 0.3f).OnComplete(() => onComplete()));
    }

    public void ShowBubbleImage()
    {
        ItemValue = _itemValue;
    }

    public ObscuredInt Row;
    public ObscuredInt Col;
    public ObscuredBool IsProtruding;

    public bool canMoveNextRow()
    {
        return Row < 14;
    }

    public (int, int) TopLeftCoordinates { get { return !IsProtruding ? (Row - 1, Col) : (Row - 1, Col - 1); } }
    public BubbleBoardItemPrefab TopLeft
    {
        get
        {
            return BubbleBoard.Ins.Items.Select(x => x.GetComponent<BubbleBoardItemPrefab>())
                .FirstOrDefault(x => x.Row == TopLeftCoordinates.Item1 && x.Col == TopLeftCoordinates.Item2);
        }
    }

    public (int, int) TopRightCoordinates { get { return !IsProtruding ? (Row - 1, Col + 1) : (Row - 1, Col); } }
    public BubbleBoardItemPrefab TopRight
    {
        get
        {
            return BubbleBoard.Ins.Items.Select(x => x.GetComponent<BubbleBoardItemPrefab>())
                .FirstOrDefault(x => x.Row == TopRightCoordinates.Item1 && x.Col == TopRightCoordinates.Item2);
        }
    }

    public (int, int) LeftCoordinates { get { return (Row, Col - 1); } }
    public BubbleBoardItemPrefab Left
    {
        get
        {
            return BubbleBoard.Ins.Items.Select(x => x.GetComponent<BubbleBoardItemPrefab>())
                .FirstOrDefault(x => x.Row == LeftCoordinates.Item1 && x.Col == LeftCoordinates.Item2);
        }
    }

    public (int, int) RightCoordinates { get { return (Row, Col + 1); } }
    public BubbleBoardItemPrefab Right
    {
        get
        {
            return BubbleBoard.Ins.Items.Select(x => x.GetComponent<BubbleBoardItemPrefab>())
                .FirstOrDefault(x => x.Row == RightCoordinates.Item1 && x.Col == RightCoordinates.Item2);
        }
    }

    public (int, int) BottomLeftCoordinates { get { return !IsProtruding ? (Row + 1, Col) : (Row + 1, Col - 1); } }
    public BubbleBoardItemPrefab BottomLeft
    {
        get 
        {
            return BubbleBoard.Ins.Items.Select(x => x.GetComponent<BubbleBoardItemPrefab>())
                .FirstOrDefault(x => x.Row == BottomLeftCoordinates.Item1 && x.Col == BottomLeftCoordinates.Item2);
        }
    }

    public (int, int) BottomRightCoordinates { get { return !IsProtruding ? (Row + 1, Col + 1) : (Row + 1, Col); } }
    public BubbleBoardItemPrefab BottomRight
    {
        get
        {
            return BubbleBoard.Ins.Items.Select(x => x.GetComponent<BubbleBoardItemPrefab>())
                .FirstOrDefault(x => x.Row == BottomRightCoordinates.Item1 && x.Col == BottomRightCoordinates.Item2);
        }
    }

    public void GetBubbleByCollision(GameObject bullet, Action<(Vector2, int, int, bool)> callBack)
    {
        //Debug.Log($"AttachTo: {Row}, {Col}");
        var lstVicinityAvailable = new List<(Vector2, int, int, bool)>();
        var lstBb = BubbleBoard.Ins.Items.Select(x => x.GetComponent<BubbleBoardItemPrefab>()).ToList();

        #region Top

        if (bullet.transform.position.y > transform.position.y)
        {
            if (Row > 1)
            {
                // Top Left
                if (TopLeft == null)
                {
                    //Debug.Log($"TopLeftNull: {TopLeftCoordinates.Item1}, {TopLeftCoordinates.Item2}");
                    var p = BubbleBoard.Ins.GetPositionByRowCol(!IsProtruding, TopLeftCoordinates.Item1, TopLeftCoordinates.Item2);
                    if (p.Item2 != -1)
                    {
                        lstVicinityAvailable.Add(p);
                    }
                }
                // Top Right
                if (TopRight == null && TopRightCoordinates.Item2 < 11)
                {
                    //Debug.Log($"TopRightNull: {TopRightCoordinates.Item1}, {TopRightCoordinates.Item2}");
                    var p = BubbleBoard.Ins.GetPositionByRowCol(!IsProtruding, TopRightCoordinates.Item1, TopRightCoordinates.Item2);
                    if (p.Item2 != -1)
                    {
                        lstVicinityAvailable.Add(p);
                    }
                }
            }
        }
        
        #endregion

        #region Same Row
        // Left
        if (Col > 0 && Left == null)
        {
            var p = BubbleBoard.Ins.GetPositionByRowCol(IsProtruding, LeftCoordinates.Item1, LeftCoordinates.Item2);
            bool isAdd = false;
            if (Col == 10)
            {
                //Debug.Log($"bulletX: {bullet.transform.position.x}, PosX: {transform.position.x}");
                if (bullet.transform.position.x < transform.position.x)
                {
                    isAdd = true;
                }
            }
            else
            {
                //Debug.Log($"LeftNull: {LeftCoordinates.Item1}, {LeftCoordinates.Item2}");
                isAdd = true;
            }

            if (isAdd)
            {
                if (p.Item2 != -1)
                {
                    lstVicinityAvailable.Add(p);
                }
            }
        }

        // Right
        if (Col < 10 && Right == null)
        {
            var p = BubbleBoard.Ins.GetPositionByRowCol(IsProtruding, RightCoordinates.Item1, RightCoordinates.Item2);
            bool isAdd = false;

            if (Col == 0)
            {
                //Debug.Log($"bulletX: {bullet.transform.position.x}, PosX: {transform.position.x}");
                if (bullet.transform.position.x > transform.position.x)
                {
                    isAdd = true;
                }
            }
            else
            {
                isAdd = true;
            }

            if (isAdd)
            {
                //Debug.Log($"RightNull: {RightCoordinates.Item1}, {RightCoordinates.Item2}");
                if (p.Item2 != -1)
                {
                    lstVicinityAvailable.Add(p);
                }
            }
        }
        #endregion

        #region Bottom
        if (canMoveNextRow())
        {
            // Bottom Left
            if (BottomLeft == null)
            {
                //Debug.Log($"BottomLeftNull: {BottomLeftCoordinates.Item1}, {BottomLeftCoordinates.Item2}");
                var p = BubbleBoard.Ins.GetPositionByRowCol(!IsProtruding, BottomLeftCoordinates.Item1, BottomLeftCoordinates.Item2);
                if (p.Item2 != -1)
                {
                    lstVicinityAvailable.Add(p);
                }
            }

            // Bottom Right
            if (BottomRight == null && BottomRightCoordinates.Item2 < 11)
            {
                //Debug.Log($"BottomRightNull: {BottomRightCoordinates.Item1}, {BottomRightCoordinates.Item2}");
                var p = BubbleBoard.Ins.GetPositionByRowCol(!IsProtruding, BottomRightCoordinates.Item1, BottomRightCoordinates.Item2);
                if (p.Item2 != -1)
                {
                    lstVicinityAvailable.Add(p);
                }
            }
        }

        #endregion

        if (lstVicinityAvailable.Count == 0)
        {
            if (Col <= 0)
            {
                BottomRight.GetBubbleByCollision(bullet, callBack);
                return;
            }
            else
            {
                BottomLeft.GetBubbleByCollision(bullet, callBack);
                return;
            }
        }

        float minDis = 0f;
        Tuple<Vector2, int, int, bool> position = null;
        foreach (var item in lstVicinityAvailable)
        {
            var ds = Vector2.Distance(bullet.transform.position, item.Item1);
            if (position == null)
            {
                position = new Tuple<Vector2, int, int, bool>(item.Item1, item.Item2, item.Item3, item.Item4);
                minDis = ds;
                continue;
            }

            if (ds < minDis)
            {
                position = new Tuple<Vector2, int, int, bool>(item.Item1, item.Item2, item.Item3, item.Item4);
                minDis = ds;
            }
        }

        bullet.transform.position = position.Item1;
        //Debug.Log($"PosAttach: {position.Item2}, {position.Item3}, {position.Item4}");
        callBack.Invoke((position.Item1, position.Item2, position.Item3, position.Item4));
    }

    public List<BubbleBoardItemPrefab> SameColors(List<BubbleBoardItemPrefab> lst = null)
    {
        if (lst == null)
        {
            lst = new List<BubbleBoardItemPrefab>();
        }
        lst.Add(this);


        if (TopLeft != null && TopLeft.Row > 0 && TopLeft.ItemValue == ItemValue && !lst.Contains(TopLeft))
        {
            TopLeft.SameColors(lst);
        }

        if (TopRight != null && TopRight.Row > 0 && TopRight.ItemValue == ItemValue && !lst.Contains(TopRight))
        {
            TopRight.SameColors(lst);
        }

        if (Left != null && Left.ItemValue == ItemValue && !lst.Contains(Left))
        {
            Left.SameColors(lst);
        }

        if (Right != null && Right.ItemValue == ItemValue && !lst.Contains(Right))
        {
            Right.SameColors(lst);
        }

        if (BottomLeft != null && BottomLeft.ItemValue == ItemValue && !lst.Contains(BottomLeft))
        {
            BottomLeft.SameColors(lst);
        }

        if (BottomRight != null && BottomRight.ItemValue == ItemValue && !lst.Contains(BottomRight))
        {
            BottomRight.SameColors(lst);
        }

        return lst;
    }

    public List<BubbleBoardItemPrefab> SameInsland(List<BubbleBoardItemPrefab> lst = null)
    {
        if (lst == null) lst = new List<BubbleBoardItemPrefab>();
        //if ((TopLeft != null && TopLeft.Row == 1) || (TopRight != null && TopRight.Row == 1)) return lst;

        lst.Add(this);

        if (TopLeft != null && !lst.Contains(TopLeft))
        {
            TopLeft.SameInsland(lst);
        }

        if (TopRight != null && !lst.Contains(TopRight))
        {
            TopRight.SameInsland(lst);
        }

        if (Left != null && !lst.Contains(Left))
        {
            Left.SameInsland(lst);
        }

        if (Right != null && !lst.Contains(Right))
        {
            Right.SameInsland(lst);
        }

        if (BottomLeft != null && !lst.Contains(BottomLeft))
        {
            BottomLeft.SameInsland(lst);
        }

        if (BottomRight != null && !lst.Contains(BottomRight))
        {
            BottomRight.SameInsland(lst);
        }

        return lst;
    }

    public Dictionary<int, List<BubbleBoardItemPrefab>> NearsByDepth(int depth, BubbleBoardItemPrefab ignore, Dictionary<int, List<BubbleBoardItemPrefab>> dic = null)
    {
        if (dic == null)
        {
            dic = new Dictionary<int, List<BubbleBoardItemPrefab>>();
        }

        if (depth <= 0) return dic;

        var lstByDepth = new List<BubbleBoardItemPrefab>();
        if (dic.ContainsKey(depth))
        {
            lstByDepth = dic[depth];
        }
        else
        {
            dic.Add(depth, lstByDepth);
        }

        if (TopLeft != null && !lstByDepth.Contains(TopLeft) && TopLeft != ignore)
        {
            lstByDepth.Add(TopLeft);
            TopLeft.NearsByDepth(depth - 1, ignore, dic);
        }

        if (TopRight != null && !lstByDepth.Contains(TopRight) && TopRight != ignore)
        {
            lstByDepth.Add(TopRight);
            TopRight.NearsByDepth(depth - 1, ignore, dic);
        }

        if (Left != null && !lstByDepth.Contains(Left) && Left != ignore)
        {
            lstByDepth.Add(Left);
            Left.NearsByDepth(depth - 1, ignore, dic);
        }

        if (Right != null && !lstByDepth.Contains(Right) && Right != ignore)
        {
            lstByDepth.Add(Right);
            Right.NearsByDepth(depth - 1, ignore, dic);
        }

        if (BottomLeft != null && !lstByDepth.Contains(BottomLeft) && BottomLeft != ignore)
        {
            lstByDepth.Add(BottomLeft);
            BottomLeft.NearsByDepth(depth - 1, ignore, dic);
        }

        if (BottomRight != null && !lstByDepth.Contains(BottomRight) && BottomRight != ignore)
        {
            lstByDepth.Add(BottomRight);
            BottomRight.NearsByDepth(depth - 1, ignore, dic);
        }

        return dic;
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
        if (sqShowHideSmallBubble != null) sqShowHideSmallBubble.Kill();
        Current.Ins.PropertyChanged -= OnChangeProperty;
    }




}