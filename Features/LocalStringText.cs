using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LocalStringText : MonoBehaviour
{

    [SerializeField]
    private string uniqKey;
    [SerializeField]
    private bool isUpdateChange = true;

    private void OnEnable()
    {
        if(isUpdateChange)
            TemplateManager.instance.LocalStringObjList.Add(this);

        ReflashLocalString();
    }
    private void OnDisable()
    {
        if (isUpdateChange)
            TemplateManager.instance.LocalStringObjList.Remove(this);
    }

    public void ReflashLocalString()
    {
        if (GetComponent<TextMeshProUGUI>() != null)
            GetComponent<TextMeshProUGUI>().text = TemplateManager.instance.GetLocalText(uniqKey);
        else if(GetComponent<Text>() != null)
            GetComponent<Text>().text = TemplateManager.instance.GetLocalText(uniqKey);


    }
}
