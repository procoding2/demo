﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class SoundManager : MonoBehaviourSingleton<SoundManager>
{
    private AudioSource m_audioSource;
    private Dictionary<string, AudioClip> m_dicSoundClips;

    public bool isSoundEffectIgnore = false;
        


    // music
    private AudioSource m_musicSource;

    private Dictionary<string, int> soundLimitDic = new Dictionary<string, int>();
    private WaitForSeconds interval_05 = new WaitForSeconds(0.5f);

    public AudioSource musicSource => m_musicSource;


    private IEnumerator ResetSoundLimit()
    {

        while (true)
        {

            yield return interval_05;

            if (soundLimitDic.Keys.Count > 0)
            {

                for (int i = 0; i < soundLimitDic.Keys.Count; i++)
                {
                    string key = soundLimitDic.Keys.ToList()[i];
                    if (soundLimitDic[key] > 0)
                    {
                        soundLimitDic[key]--;
                    }
                }

            }
        }

    }

    private void Awake()
    {

        //PlayerPrefs.DeleteAll();

        m_audioSource = gameObject.AddComponent<AudioSource>();
        m_dicSoundClips = new Dictionary<string, AudioClip>();

        m_musicSource = gameObject.AddComponent<AudioSource>();

        m_musicSource.volume = PlayerPrefsEx.Get("bgmVol", 0.2f);
        m_audioSource.volume = PlayerPrefsEx.Get("seVol", 0.3f);





        BGM_On(Current.Ins.Config.SoundConfig.BackgroundVolume == 1f);
        SE_On(Current.Ins.Config.SoundConfig.GameVolume == 1f);
        StartCoroutine(ResetSoundLimit());
    }


    private void OnDisable()
    {
        isSoundEffectIgnore = false;
        if (gameObject.scene.isLoaded == false)
            return;
    }

    public float BGMVolume
    {
        get => m_musicSource.volume;
        set
        {
            m_musicSource.volume = value;
            PlayerPrefsEx.Set("bgmVol", value);
        }
    }
    public float SEVolume
    {
        get => m_audioSource.volume;
        set
        {
            m_audioSource.volume = value;
            PlayerPrefsEx.Set("seVol", value);
        }
    }

    public void BGM_On(bool isOn)
    {
        m_musicSource.enabled = isOn;
        //m_musicSource.volume = isOn ? PlayerPrefs.GetFloat("bgmVol") : 0.0f;
    }
    public void SE_On(bool isOn)
    {
        m_audioSource.enabled = isOn;
        //m_audioSource.volume = isOn ? PlayerPrefs.GetFloat("seVol") : 0.0f;
    }

    public AudioClip Load(string filename)
    {
        // 뒤에 확장자는 지워야 함.
        //filename = filename.Replace(".wav", "");

        if (m_dicSoundClips.ContainsKey(filename) == false)
        {
            string path = string.Format("Sound/SE/{0}", filename);
            AudioClip audioClip = Resources.Load(path) as AudioClip;
            if (audioClip == null)
            {
                Debug.LogWarning("not found sound resource. sound name : " + filename);
                return null;
            }

            m_dicSoundClips.Add(filename, audioClip);
            return audioClip;
        }
        else
        {
            return m_dicSoundClips[filename];
        }
    }
    public void PlayUIEffect(string filename)
    {
        PlayEffect(filename, false);
    }

    public void PlayEffect(string filename, bool isLimit = true)
    {
        if (isSoundEffectIgnore)
            return;

        if (isLimit)
        {
            if (soundLimitDic.ContainsKey(filename) == false)
            {
                soundLimitDic.Add(filename, 1);
            }
            else
            {
                if (soundLimitDic[filename] > 5)
                    return;
                else
                    ++soundLimitDic[filename];
            }
        }


        AudioClip audioClip = Load(filename);
        if (audioClip)
        {
            m_audioSource.PlayOneShot(audioClip);
        }
    }
    public void PlayEffectOnlyOne(string filename)
    {
        if (isSoundEffectIgnore)
            return;
        AudioClip audioClip = Load(filename);
        if (audioClip)
        {
            m_audioSource.clip = audioClip;
            m_audioSource.Play();
        }
    }

    public void PlayAudioClip(AudioClip audioClip)
    {

        string filename = audioClip.name;
        PlayEffect(filename);
    }
    public void PlayMusic(string fileName, bool isLoop = true)
    {

        string path = string.Format("Sound/BGM/{0}", fileName);
        AudioClip musicClip = Resources.Load(path) as AudioClip;
        if (musicClip && (m_musicSource.clip != musicClip || m_musicSource.isPlaying == false))
        {
            m_musicSource.loop = isLoop;
            m_musicSource.clip = musicClip;
            m_musicSource.Play();
        }
    }

    public void StopMusic()
    {
        m_musicSource.Stop();
    }
        
}

