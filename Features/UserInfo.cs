using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class UserInfo : MonoBehaviour
{
    [SerializeField]
    private RawImage userAvt;
    public RawImage UserAvt => userAvt;
    
    [SerializeField]
    private TextMeshProUGUI userName;
    public TextMeshProUGUI UserName => userName;
    [SerializeField]
    private Text userScore;
    public Text UserScore => userScore;

    [SerializeField]
    private GameObject PointTailPrefabs = null;


    private int score, score_ex;
    public int Score
    {
        get => score;
        set
        {

            if (score != value)
            {
                Sequence sq = DOTween.Sequence();

                sq.Join(userScore.transform.DOScale(1.65f, 0.25f).SetEase(Ease.InOutFlash));
                sq.AppendCallback(() => { userScore.color = Color.white; });
                sq.Append(userScore.transform.DOScale(0.95f, 0.25f).SetEase(Ease.InOutFlash));
                sq.AppendCallback(() => { userScore.color = originColor; });
                sq.Append(userScore.transform.DOScale(1.0f, 0.1f).SetEase(Ease.InOutFlash));


                sq.Play();
            }
            score = value;
            if (score < 0) score = 0;
        }
    }

    private Color originColor;
    public Color originColorDefault;
    private void Start()
    {
        if(PointTailPrefabs)
            ObjectPoolManager.instance.ReservePool(PointTailPrefabs, 10);

        originColor = originColorDefault;
    }



    // Update is called once per frame
    void Update()
    {
        if (score != score_ex)
        {
            int diff = Mathf.Abs(score - score_ex);
            int mul = score > score_ex ? 1 : -1;
            if (diff > 1000)
            {
                score_ex += 1000 * mul;
            }
            else if (diff > 100)
            {
                score_ex += 100 * mul;
            }
            else if (diff > 8)
            {
                score_ex += 8 * mul;
            }
            else
            {
                score_ex += mul;
            }

            if (score_ex < 0)
                score_ex = 0;

            if (userScore)
            {
                userScore.text = StringHelper.NumberFormatter(score_ex);
            }
        }
    }


    public void ShowPointTailEffect(int point, Vector2 startPos)
    {
        GameObject obj = ObjectPoolManager.instance.GetObject(PointTailPrefabs.name, true, true);
        obj.transform.position = startPos;
        obj.transform.localScale = Vector3.one;

        Sequence sq = DOTween.Sequence();
        sq.Join(obj.transform.DOJump(userScore.transform.position, UnityEngine.Random.Range(-4.0f, 4.0f), 1, Random.Range(0.75f, 1.25f)).SetEase(Ease.InOutSine));
        sq.Play().OnComplete(() => {
            obj.SetActive(false);
            Score += point;

            SoundManager.instance.PlayEffect($"se_lamp{Random.Range(1, 4)}");
        });
    }

    public void ShowPointTailEffectAndSet(int point, Vector2 startPos)
    {
        GameObject obj = ObjectPoolManager.instance.GetObject(PointTailPrefabs.name, true, true);
        obj.transform.position = startPos;
        obj.transform.localScale = Vector3.one;

        Sequence sq = DOTween.Sequence();
        sq.Join(obj.transform.DOJump(userScore.transform.position, UnityEngine.Random.Range(-4.0f, 4.0f), 1, Random.Range(0.75f, 1.25f)).SetEase(Ease.InOutSine));
        sq.Play().OnComplete(() =>
        {
            obj.SetActive(false);
            Score = point;

            SoundManager.instance.PlayEffect($"se_lamp{Random.Range(1, 4)}");
        });
    }
}
