using System;
using System.Collections.Generic;
using UnityEngine;
using CodeStage.AntiCheat.ObscuredTypes;
using CodeStage.AntiCheat.Detectors;
using CodeStage.AntiCheat.Storage;
using System.ComponentModel;

public abstract class UIBase<T> : MonoBehaviour where T: class
{
    public static T Ins;

    private void Awake() => InitIns();

    protected abstract void InitIns();

    protected List<IDisposable> bags = new List<IDisposable>();

    protected Queue<Action> jobs = new Queue<Action>();
    protected void AddJob(Action job) => jobs.Enqueue(job);

    protected virtual void Start()
    {
        //return; // 클라이언트의 Anticheat은 큰 의미없으므로 지향할 것 - Brandon

        ObscuredCheatingDetector.StartDetection(OnCheaterDetected);
        ObscuredPrefs.NotGenuineDataDetected += OnCheaterDetected;
        SpeedHackDetector.StartDetection(OnCheaterDetected);

        Current.Ins.PropertyChanged += OnChangeProperty;
    }

    protected virtual void Update()
    {
        while (jobs.Count > 0) jobs.Dequeue().Invoke();
    }

    private void OnChangeProperty(object sender, PropertyChangedEventArgs e)
    {
        if (e.PropertyName == StringConst.PROPERTY_ANTICHEAT) OnReloadCryptoKey();
    }

    protected virtual void OnReloadCryptoKey()
    {
    }

    protected virtual void OnCheaterDetected()
    {
        Application.Quit();
    }

    protected void DisposeAllRx()
    {
        bags.ForEach(d => d.Dispose());
        bags = new List<IDisposable>();
    }

    protected virtual void OnDisable()
    {
        DisposeAllRx();
        ObscuredPrefs.NotGenuineDataDetected -= OnCheaterDetected;
        Current.Ins.PropertyChanged -= OnChangeProperty;
    }

    protected virtual void OnDestroy()
    {
        DisposeAllRx();
        ObscuredPrefs.NotGenuineDataDetected -= OnCheaterDetected;
        Current.Ins.PropertyChanged -= OnChangeProperty;
    }
}
