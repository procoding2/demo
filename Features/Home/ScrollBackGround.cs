using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollBackGround : MonoBehaviour
{
    [SerializeField] private RawImage _img;
    [SerializeField] private float _scrollSpeed = 0.01f;
    [SerializeField] private float _scrollLimit = 0.3f;

    private float _currentOffset = 0.0f;
    private float _direction = 1.0f; // 1 for positive, -1 for negative

    private void Update()
    {
        _currentOffset += _scrollSpeed * Time.deltaTime * _direction;

        if (_currentOffset > _scrollLimit || _currentOffset < -_scrollLimit)
        {
            _direction *= -1;
        }

        _img.uvRect = new Rect(_currentOffset, _currentOffset/2, _img.uvRect.width, _img.uvRect.height);
    }
}
