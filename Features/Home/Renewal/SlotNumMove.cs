using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

public class SlotNumMove : MonoBehaviour
{

    [SerializeField]
    private GameObject movingObj = null;

    [SerializeField]
    private TextMeshProUGUI[] numTexts;

    private Vector3 originPos;

    // Start is called before the first frame update
    void Start()
    {
        originPos = movingObj.transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private int GetOneDigitNumber(int num)
    {
        int number = 0;
        if (num > 0)
        {
            number = num % 10;
        }
        else if (num < 0)
        {
            number = 10 - (Mathf.Abs(num) % 10);
        }

        return number; 
    }


    private void SetNumberText(int currentNum, Color color)
    {
        for (int k = 0; k < 4; k++)
        {
            int number = GetOneDigitNumber(currentNum + k - 1);
            numTexts[k].text = number.ToString();
            numTexts[k].color = color;
        }
    }

    public void SpinStart(int lastNumber, float delayTime, int count, Color lastColor)
    {
        int startnum = 0;
        SetNumberText(startnum, Color.white);


        Sequence sq = DOTween.Sequence();
        for (int i = 0; i < count; i++)
        {
            sq.Append(movingObj.transform.DOLocalMoveY(30.0f, delayTime));//.SetEase(Ease.InBounce);
            sq.AppendCallback(() => {
                startnum = GetOneDigitNumber(startnum + 4);
                SetNumberText(startnum, Color.white);
                movingObj.transform.localPosition = originPos;

                SoundManager.instance.PlayUIEffect("se_muted");
                //SoundManager.instance.PlayUIEffect("se_type3");
            });
        }
        sq.Play().OnComplete(() =>
        {
            SetNumberText(lastNumber, lastColor);
            Sequence sq2 = DOTween.Sequence();
            sq2.Append(movingObj.transform.DOLocalMoveY(5.0f, delayTime));
            sq2.Append(movingObj.transform.DOLocalMoveY(originPos.y, 0.2f).SetEase(Ease.InBounce));
            sq2.Play();

            SoundManager.instance.PlayUIEffect("se_coinBounce");
        });

    }
}
