using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using OutGameEnum;

public class TabMenuEvent : MonoBehaviour
{
    [SerializeField]
    private TabbarEnum currentTab;

    private Toggle toggle => GetComponent<Toggle>();

    [SerializeField]
    private RedDotNum redDotNum = null;

    public int CurrentDotNum { get { return redDotNum.Number; } }

    // Start is called before the first frame update
    void Start()
    {
        toggle.onValueChanged.AddListener(SelectToggleAction);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void SelectToggleAction(bool isOn)
    {
        if (isOn)
        {
            SoundManager.instance.PlayUIEffect("se_select");
            UIHome.Ins.ShowMainTapView(currentTab);
        }
    }

    public void showDotNoti(int number, bool isShow)
    {
        redDotNum.gameObject.SetActive(isShow);
        redDotNum.Number = number;
    }
    public void EnableDisableAllButton(bool isEnable)
    {
        toggle.interactable = isEnable;
    }
}
