using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FlipNumSlot : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI[] frontTexts;
    [SerializeField]
    private TextMeshProUGUI[] backTexts;


    private Animator ani_con => GetComponent<Animator>();


    private int currentNumber = 0;
    public int CurrentNumber
    {
        get => currentNumber;
        set
        {
            currentNumber = (value % 10);
            backTexts[0].text = currentNumber.ToString();

            ani_con.SetTrigger("Flip");
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void FlipMidAction()
    {
        backTexts[1].text = frontTexts[1].text;
        frontTexts[1].text = currentNumber.ToString();
    }

    public void FilpEndAction()
    {
        frontTexts[0].text = frontTexts[1].text = currentNumber.ToString();
        backTexts[0].text = backTexts[1].text = currentNumber.ToString();

        ani_con.ResetTrigger("Flip");
    }


    public void FlipTest()
    {
        CurrentNumber = Random.Range(0, 10);
    }
}
