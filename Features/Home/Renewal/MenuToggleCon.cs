using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuToggleCon : MonoBehaviour
{

    private ToggleGroup toggleGroup => GetComponent<ToggleGroup>();

    private List<Toggle> toggleList = new List<Toggle>();


    // Start is called before the first frame update
    void Start()
    {
        toggleList.Clear();
        for (int i = 0; i < transform.childCount; i++)
        {
            Toggle obj = transform.GetChild(i).GetComponent<Toggle>();
            if(obj)
            {
                obj.group = toggleGroup;
                obj.onValueChanged.AddListener(ActiveSelectToggle);
                toggleList.Add(obj);
            }
        }
        toggleList[0].isOn = true;
        toggleList[0].GetComponent<Animator>().Play("ToggleOn");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void ActiveSelectToggle(bool isOn)
    {
        foreach (Toggle toggle in toggleList)
        {
            toggle.GetComponent<Animator>().Play(toggle.isOn ? "ToggleOn" : "ToggleOff");
        }
    }
}
