using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class RedDotNum : MonoBehaviour
{

    private TextMeshProUGUI numText => GetComponentInChildren<TextMeshProUGUI>();

    private int _number = 0;
    public int Number { get => _number;
        set
        {
            if (value >= 99)
                _number = 99;
            else if (value < 0)
                _number = 0;
            else
                _number = value;

            numText.text = _number.ToString();

        }
    }
}
