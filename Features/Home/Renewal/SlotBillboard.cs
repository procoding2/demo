using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlotBillboard : MonoBehaviour
{

    [SerializeField]
    private List<SlotNumMove> slotNumList;


    private Coroutine slotCoroutine = null;

    public void SetNewSlotNumber(long number)
    {

        if (slotCoroutine != null)
            StopCoroutine(slotCoroutine);

        slotCoroutine = null;


        string newNumberStr = $"{number:000000000000}";
        char[] values = newNumberStr.ToCharArray();
        int length = values.Length;

        Color fontColor = Color.gray;
        bool isZeroStart = true;
        slotCoroutine = StartCoroutine(NewSlotNumber());
        IEnumerator NewSlotNumber()
        {
            for (int i = 0; i < length; i++)
            {
                int value = System.Convert.ToInt32(values[i].ToString());
                if (isZeroStart && value != 0)
                {
                    isZeroStart = false;
                    fontColor = Color.white;
                }

                slotNumList[i].SpinStart(value, 0.1f, 20, fontColor);

                yield return new WaitForSeconds(0.1f);
            }

        }
    }
}
