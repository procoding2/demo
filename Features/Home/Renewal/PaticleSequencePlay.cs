using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ParticleInfo
{
    public GameObject obj;
    public float time;
    public ParticleInfo() { }
}

public class PaticleSequencePlay : MonoBehaviour
{

    [SerializeField]
    private List<ParticleInfo> particleList;


    private void OnEnable()
    {
        StopAllCoroutines();

        if (particleList.Count > 0)
        {
            foreach (var x in particleList)
            {
                if (x.obj)
                    x.obj.SetActive(false);
            }
            StartCoroutine(DelaySequnece());
        }
        
    }
    IEnumerator DelaySequnece()
    {
        for (int i = 0; i < particleList.Count; i++)
        {
            yield return new WaitForSeconds(particleList[i].time);
            if (particleList[i].obj)
                particleList[i].obj.SetActive(true);
        }
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }
}
