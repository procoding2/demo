using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class GameSelectManager : MonoBehaviour
{
    private enum MovingState
    {
        ready,
        drag,
        snapMove,

    }
    [SerializeField]
    private GameObject[] showCenterImages;


    private int currentGameIndex = -1;


    [SerializeField]
    private Transform centerForm, leftForm, rightForm;

    [SerializeField]
    private RectTransform scrollRect = null;


    private List<int> gameIndexList = new List<int>();

    [SerializeField]
    private List<GameIconObj> GameListViews = new List<GameIconObj>();


    private List<GameIconObj> activeCellViews = new List<GameIconObj>();
    public List<GameIconObj> ActiveCellViews => activeCellViews;
    private List<GameIconObj> sortList = new List<GameIconObj>();


    private GameIconObj SelectGameIconObj = null;

    [SerializeField]
    private MovingState movingState = MovingState.ready;

    void Awake()
    {
        movingState = MovingState.ready;

        for (int i = 0; i < 5; i++)
        {
            gameIndexList.Add(i);
        }

        for (int i = 0; i < showCenterImages.Length; i++)
        {
            showCenterImages[i].SetActive(false);
        }
    }

    float size = 0.0f;
    float dragLimitLength = 0.0f;
    [SerializeField]
    float diffSize = 263.0f;
    // Start is called before the first frame update
    void Start()
    {


        size = (centerForm.localPosition - leftForm.localPosition).sqrMagnitude;
        dragLimitLength = (rightForm.localPosition.x - leftForm.localPosition.x) * 0.85f;

        

        for (int i = 0; i < 5; i++)
        {
            GameListViews[i].GameIndex = i;
            GameListViews[i].gameSelectManager = this;
        }
        activeCellViews.Clear();
        activeCellViews.AddRange(GameListViews.FindAll(obj => obj.gameObject.activeSelf));

        sortList.Clear();
        sortList.AddRange(activeCellViews);

        //float startPosX = GetOutSidePosX(_left: true);
        //for (int i = 0;i< activeCellViews.Count; i++)
        //{
        //    GameIconObj cell = activeCellViews[i];
        //    cell.transform.localPosition = Vector3.Lerp(cell.transform.localPosition, new Vector3(startPosX + (i * diffSize), cell.transform.localPosition.y, cell.transform.localPosition.z), Time.deltaTime * 15.0f);
        //}

        StartCoroutine(ReadySetup());
        //FlipAllAni();
    }


    IEnumerator ReadySetup()
    {
        yield return new WaitForSeconds(0.2f);
        ForceMoveFromSelectGameIndex(1);
    }

    Vector3 dragStartPos;
    // Update is called once per frame
    void Update()
    {
        activeCellViews.Sort((a, b) =>
        {
            if (a.transform.localPosition.x < b.transform.localPosition.x)
                return -1;
            else
                return 1;
        });

        float startPosX = GetOutSidePosX(_left: true);

        for (int i = 0; i < activeCellViews.Count; i++)
        {
            GameIconObj cell = activeCellViews[i];
            if (movingState == MovingState.ready)
            {
                cell.transform.localPosition = Vector3.Lerp(cell.transform.localPosition, new Vector3(startPosX + (i * diffSize), cell.transform.localPosition.y, cell.transform.localPosition.z), Time.deltaTime * 15.0f);
            }
            else if (movingState == MovingState.snapMove)
            {
                Vector3 destPos = transform.localPosition;
                if (SelectGameIconObj != null && cell.Equals(SelectGameIconObj))
                {
                    destPos = Vector3.Lerp(cell.transform.localPosition, new Vector3(centerForm.localPosition.x, cell.transform.localPosition.y, cell.transform.localPosition.z), Time.deltaTime * 15.0f);
                }
                else
                {
                    int snapIndex = i - GetCenterGameIndex(SelectGameIconObj.GameIndex);
                    float posX = SelectGameIconObj.transform.localPosition.x + (snapIndex * diffSize);

                    destPos = Vector3.Lerp(cell.transform.localPosition, new Vector3(posX, cell.transform.localPosition.y, cell.transform.localPosition.z), Time.deltaTime * 15.0f);
                    if (destPos.x < leftForm.localPosition.x)//right warp
                    {
                        float warpPosX = lastActiveCellPosX + diffSize;
                        destPos = new Vector3(warpPosX, cell.transform.localPosition.y, cell.transform.localPosition.z);
                    }
                    else if (destPos.x > rightForm.localPosition.x)//left warp
                    {
                        float warpPosX = firstActiveCellPosX - diffSize;
                        destPos = new Vector3(warpPosX, cell.transform.localPosition.y, cell.transform.localPosition.z);
                    }
                }
                cell.transform.localPosition = destPos;
            }
            float dist = (cell.transform.localPosition - centerForm.localPosition).sqrMagnitude / size;
            Vector3 currentScale = Vector3.one * Mathf.Max(0.5f, (1.0f - dist));

            cell.transform.localScale = Vector3.Lerp(cell.transform.localScale, currentScale, Time.deltaTime * 20.0f);
            cell.GameIconImage.color = new Color(cell.transform.localScale.x, cell.transform.localScale.y, cell.transform.localScale.z, 1.0f);

        }
        if (movingState == MovingState.snapMove && SelectGameIconObj != null && Mathf.Abs(SelectGameIconObj.transform.localPosition.x - centerForm.localPosition.x) < 1.0f)
        {
            movingState = MovingState.ready;
        }

        sortList.Sort((a, b) =>
        {
            if (a.transform.localScale.x > b.transform.localScale.x)
                return -1;
            else
                return 1;
        });


        if (sortList.Count > 0)
        {
            if (currentGameIndex != (sortList[0] as GameIconObj).GameIndex)
            {
                SoundManager.instance.PlayUIEffect("se_type4");
                currentGameIndex = (sortList[0] as GameIconObj).GameIndex;

                for (int j = 0; j < showCenterImages.Length; j++)
                {
                    showCenterImages[j].SetActive(currentGameIndex == j);
                }
            }
        }
    }

    private float firstActiveCellPosX => activeCellViews.Count > 0 ? activeCellViews[0].transform.localPosition.x : 0;
    private float lastActiveCellPosX => activeCellViews.Count > 0 ? activeCellViews[activeCellViews.Count - 1].transform.localPosition.x : 0;


    private float overWarpPosValue => diffSize * (0.55f * activeCellViews.Count);
    public void Drag(BaseEventData _Data)
    {
        PointerEventData pData = _Data as PointerEventData; 
        if (movingState != MovingState.drag)
        {
            movingState = MovingState.drag;
            dragStartPos = pData.position;


            activeCellViews.ForEach(x => x.originPosX = x.transform.localPosition.x);
        }
        float dragDist = Mathf.Clamp(pData.position.x - dragStartPos.x, -dragLimitLength, dragLimitLength);

        if (movingState == MovingState.drag)
        {
            foreach (GameIconObj cell in activeCellViews)
            {
                float moveDist = cell.originPosX + dragDist;
                float overPosValue = 0.0f;
                if (moveDist < leftForm.localPosition.x)
                {
                    overPosValue = rightForm.localPosition.x + overWarpPosValue;
                }
                else if (moveDist > rightForm.localPosition.x)
                {
                    overPosValue = leftForm.localPosition.x - overWarpPosValue;
                }
                

                cell.transform.localPosition = new Vector3(moveDist + overPosValue, cell.transform.localPosition.y, cell.transform.localPosition.z);

            }
        }
    }
    public void EndDrag()
    {
        ForceMoveFromSelectGameIndex(currentGameIndex);
    }
    private int GetCenterGameIndex(int _gameIndex)
    {
        int centerGameIndex = 0;

        for (int i = 0; i < activeCellViews.Count; i++)
        {
            if (activeCellViews[i].GameIndex == _gameIndex)
            {
                centerGameIndex = i;
                break;
            }
        }
        return centerGameIndex;
    }
    public void ForceMoveFromSelectGameIndex(int _gameIndex)
    {
        if (movingState == MovingState.snapMove)
            return;

        for (int i = 0; i < activeCellViews.Count; i++)
        {
            if (activeCellViews[i].GameIndex == _gameIndex)
            {
                SelectGameIconObj = activeCellViews[i];
                break;
            }
        }
        movingState = MovingState.snapMove;
    }



    private float GetOutSidePosX(bool _left)
    {
        int centerGameIndex = GetCenterGameIndex(currentGameIndex);

        return centerForm.transform.localPosition.x + (_left ? (-centerGameIndex * diffSize) : ((activeCellViews.Count - centerGameIndex - 1) * diffSize));
    }


    public void SelectGameAction()
    {
        switch (currentGameIndex )
        {
            case 0:
                HomeTab.UIHomeTab.Ins.OpenGameMode(GameEnum.MiniGameEnum.Bingo); break;
            case 1:
                HomeTab.UIHomeTab.Ins.OpenGameMode(GameEnum.MiniGameEnum.Solitaire); break;
            case 2:
                HomeTab.UIHomeTab.Ins.OpenGameMode(GameEnum.MiniGameEnum.Bubble); break;
            case 3:
                SceneHelper.LoadScene(OutGameEnum.SceneEnum.Race); break;
            case 4:
                SceneHelper.LoadScene(OutGameEnum.SceneEnum.Fish); break;
        }
    }

    private void OnDisable()
    {
        ForceMoveFromSelectGameIndex(currentGameIndex);
    }

}
