using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameIconObj : MonoBehaviour
{

    [SerializeField]
    private Image gameIconImage;


    public Image GameIconImage => gameIconImage;

    [SerializeField]
    private List<Sprite> imagesPool;

    [SerializeField]
    private int gameIndex = 0;
    public int GameIndex
    {
        get => gameIndex;
        set
        {
            gameIndex = value;
            gameIconImage.sprite = imagesPool[gameIndex];
        }
    }

    public float originPosX;

    private GameSelectManager _gameSelectManager = null;

    public GameSelectManager gameSelectManager
    {
        set => _gameSelectManager = value;
    }


    public void ForceSelectThisGame()
    {
        _gameSelectManager.ForceMoveFromSelectGameIndex(gameIndex);
    }
}
