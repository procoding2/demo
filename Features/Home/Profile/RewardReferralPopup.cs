using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RewardReferralPopup : MonoBehaviour
{
    public GameObject ButtonClaim;
    public GameObject ReferralCodeView;

    private void Start()
    {
        ButtonClaim.GetComponent<Button>().onClickWithHCSound(() =>
        {
            Current.Ins.ReloadUserProfile(ButtonClaim.gameObject.transform);
            gameObject.SetActive(false);
            ReferralCodeView.SetActive(false);
        });
    }
}
