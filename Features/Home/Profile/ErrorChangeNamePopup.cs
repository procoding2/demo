using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ErrorChangeNamePopup : MonoBehaviour
{
    public GameObject buttonClose;
    public void Start()
    {
        buttonClose.GetComponent<Button>().onClickWithHCSound(() =>
        {
            gameObject.SetActive(false);
        });
    }
}
