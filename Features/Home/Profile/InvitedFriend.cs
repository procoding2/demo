using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Localization.Components;
using TMPro;
using OutGameEnum;

public class InvitedFriend : MonoBehaviour
{
    public GameObject backButton;

    public GameObject textCode;
    public GameObject copyButton;
    public GameObject slideQuantityFriend;
    public LocalizeStringEvent textQuantityFriend;

    [Header("Quantity list")]
    public GameObject quantityRewardList;
    public GameObject quantityRewardItemPrefab;

    // Start is called before the first frame update
    void Start()
    {
        backButton.GetComponent<Button>().onClick.AddListener(() =>
        {
            SoundManager.instance.PlayUIEffect("se_click1");
            gameObject.SetActive(false);
        });

        textCode.GetComponent<TextMeshProUGUI>().SetText(Current.Ins.player.ReferralCode);
        copyButton.GetComponent<Button>().onClick.AddListener(() =>
        {
            SoundManager.instance.PlayUIEffect("se_click1");
            GUIUtility.systemCopyBuffer = textCode.GetComponent<TextMeshProUGUI>().text;
        });
    }

    public void Init(UserReferenceData[] userReferenceDatas, int quantity)
    {
        int index = userReferenceDatas.Length - 1;
        quantityRewardList.transform.RemoveAllChild();

        while (index >= 0)
        {
            bool isClaimed = index <= quantity - 1;
            var quantityFriendItem = Instantiate(quantityRewardItemPrefab, quantityRewardList.transform, false).GetComponent<QuantityItem>();
            quantityFriendItem.Init(index + 1, userReferenceDatas[index], isClaimed);
            index--;
        }

        slideQuantityFriend.GetComponent<Slider>().value = (float)quantity / userReferenceDatas.Length;
        textQuantityFriend.UpdateValue("param" , VariableEnum.Int , quantity);

        gameObject.SetActive(true);
    }
}
