using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class ProfileView : MonoBehaviour
{
    public GameObject mainHome;
    [Header("Button")]
    public GameObject backButton;
    public GameObject settingButton;
    public GameObject updateProfileButton;
    public GameObject invitedFriendButton;
    public GameObject referralCodeButton;
    public GameObject couponButton;

    [Header("View")]
    public GameObject invitedFriendView;
    public GameObject settingView;
    public GameObject updateProfileView;
    public GameObject referralCodeView;
    public GameObject textReferralCode;
    public GameObject couponView;
    public GameObject textCoupon;
    public GameObject textChangeName;

    [Header("Info Player")]
    public GameObject namePlayer;
    public GameObject pidText;
    public GameObject level;
    public GameObject currentXp;
    public RawImage avatar;
    public RawImage avatarFrame;
    public RawImage coverPhoto;

    public Action OnClose;
    Vector3 close = new Vector3 (1, 0, 1);
    // Start is called before the first frame update
    void Start()
    {
        backButton.GetComponent<Button>().onClick.AddListener(() =>
        {
            SoundManager.instance.PlayUIEffect("se_click1");
            //
            OnClose?.Invoke();
            //
            mainHome.SetActive(true);
            //
            gameObject.SetActive(false);

        });

        settingButton.GetComponent<Button>().onClick.AddListener(() =>
        {
            SoundManager.instance.PlayUIEffect("se_click1");
            settingView.SetActive(true);
        });

        updateProfileButton.GetComponent<Button>().onClick.AddListener(() =>
        {
            SoundManager.instance.PlayUIEffect("se_click1");
            textChangeName.GetComponent<TMP_InputField>().text = "";
            updateProfileView.SetActive(true);
        });

        invitedFriendButton.GetComponent<Button>().onClick.AddListener(() =>
        {
            SoundManager.instance.PlayUIEffect("se_click1");
            GetUserReference();
        });

        referralCodeButton.GetComponent<Button>().onClick.AddListener(() =>
        {
            SoundManager.instance.PlayUIEffect("se_click1");
            textReferralCode.GetComponent<TMP_InputField>().text = "";
            referralCodeView.SetActive(true);
        });

        couponButton.GetComponent<Button>().onClick.AddListener(() =>
        {
            SoundManager.instance.PlayUIEffect("se_click1");
            textCoupon.GetComponent<TMP_InputField>().text = "";
            couponView.SetActive(true);
        });

        pidText.GetComponent<Button>().onClickWithHCSound(() =>
        {
            GUIUtility.systemCopyBuffer = Current.Ins.player.Pid;
        });

        //level.GetComponent<TextMeshProUGUI>().SetText("Lv." + Current.Ins.player.Level.ToString());

        //if (Current.Ins.player.NextLevelXP == 0)
        //{
        //    currentXp.GetComponent<Slider>().value = 0f;
        //}
        //else
        //{
        //    currentXp.GetComponent<Slider>().value = (float)Current.Ins.player.CurrentXP / Current.Ins.player.NextLevelXP;
        //}

        if (Current.Ins.player.ReferenceCode != null && !Current.Ins.player.ReferenceCode.Equals(""))
        {
            referralCodeButton.GetComponent<Button>().interactable = false;
            referralCodeButton.transform.GetChild(1).gameObject.SetActive(true);
        }

        LoadUserInfor();
    }

    public void LoadUserInfor()
    {
        //GetAvt
        Current.Ins.UserAvatar((result) =>
        {
            avatar.GetComponent<RawImage>().texture = result.Texture;
        });

        Current.Ins.UserAvatarFrame((result) =>
        {
            avatarFrame.texture = result.Texture;
        });
        //
        Current.Ins.UserCover((result) =>
        {
            coverPhoto.texture = result.Texture;
        });

        namePlayer.GetComponent<TextMeshProUGUI>().SetText(Current.Ins.player.Name);
        pidText.GetComponent<TextMeshProUGUI>().SetText($"PID: {Current.Ins.player.Pid} <size= 50><sprite=0></size>");
    }

    public void GetUserReference()
    {
        UIHome.Ins.ShowHideLoadingPopup(true);

        StartCoroutine(Current.Ins.userAPI.GetUserReference((res) =>
        {
            UIHome.Ins.ShowHideLoadingPopup(false);
            if (res == null || res.errorCode != 0)
            {

                UIPopUpManager.instance.ShowErrorPopUp("Request Error : GetUserReference", res, GetUserReference);
                return;
            }

            UserReferenceData[] userReferenceGoldDatas = new UserReferenceData[res.data.prizes.Length];
            int index = 0;
            while (index < res.data.prizes.Length)
            {
                userReferenceGoldDatas[index] = new UserReferenceData();
                userReferenceGoldDatas[index].Gold = res.data.prizes[index].gold;
                userReferenceGoldDatas[index].Token = res.data.prizes[index].token;
                userReferenceGoldDatas[index].Ticket = res.data.prizes[index].ticket;
                index++;
            }

            invitedFriendView.GetComponent<InvitedFriend>().Init(userReferenceGoldDatas, res.data.quantity);
        }));
    }

    private void User_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
    {
        if (e.PropertyName == StringConst.PROPERTY_USER_INFOR && !namePlayer.GetComponent<TextMeshProUGUI>().text.Equals(Current.Ins.player.Name))
        {
            namePlayer.GetComponent<TextMeshProUGUI>().SetText(Current.Ins.player.Name);
        }
    }

    private void OnEnable()
    {
        //
        mainHome.SetActive(false);
        //
        Current.Ins.PropertyChanged += User_PropertyChanged;
    }
    private void OnDisable()
    {
        Current.Ins.PropertyChanged -= User_PropertyChanged;
    }
}
