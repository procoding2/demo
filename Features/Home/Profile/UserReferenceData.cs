using System;

public class UserReferenceData 
{
    public int Gold { get; set; }
    public int Token { get; set; }
    public int Ticket { get; set; }

    public int TotalItem
    {
        get
        {
            int total = 0;
            if (Gold > 0) total++;
            if (Token > 0) total++;
            if (Ticket > 0) total++;

            return total;
        }
    }
}
