using System;

public class CouponCodeData 
{
    public int Gold { get; set; }
    public int Token { get; set; }
    public int Ticket { get; set; }
}
