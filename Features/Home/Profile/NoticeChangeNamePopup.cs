using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class NoticeChangeNamePopup : MonoBehaviour
{
    public GameObject btnCancel;
    public GameObject btnConfirm;

    private Action _onConfirm;
    private Action _onCancel;

    void Start()
    {
        btnCancel.GetComponent<ButtonEx>().onClickWithHCSound(() =>
        {
            gameObject.SetActive(false);

            _onCancel.Invoke();
        });

        btnConfirm.GetComponent<ButtonEx>().onClickWithHCSound(() =>
        {
            gameObject.SetActive(false);

            _onConfirm.Invoke();
        });
    }

    public void Init(Action onConfirm, Action onCancel)
    {
        gameObject.SetActive(true);

        _onConfirm = onConfirm;
        _onCancel = onCancel;
    }
}
