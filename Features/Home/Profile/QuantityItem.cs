using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using OutGameEnum;
using System;

public class QuantityItem : MonoBehaviour
{
    public GameObject titleText;

    [Header("1 Item")]
    public GameObject Bg_1;
    public GameObject Complete_1;
    public GameObject Gray_1;
    public GameObject Mark_1;
    public GameObject CoinItem_1;

    [Header("2 Item")]
    public GameObject Bg_2;
    public GameObject Complete_2;
    public GameObject Gray_2;
    public GameObject Mark_2;
    public GameObject CoinItem_2_1;
    public GameObject CoinItem_2_2;

    [Header("3 Item")]
    public GameObject Bg_3;
    public GameObject Complete_3;
    public GameObject Gray_3;
    public GameObject Mark_3;
    public GameObject CoinItem_3_1;
    public GameObject CoinItem_3_2;
    public GameObject CoinItem_3_3;

    public void Init(int title, UserReferenceData data, bool isClaimed)
    {
        titleText.GetComponent<TextMeshProUGUI>().SetText(title.ToString());

        var totalItem = data.TotalItem;

        if (totalItem == 1)
        {
            Bg_1.SetActive(true);
            Bg_2.SetActive(false);
            Bg_3.SetActive(false);

            var coinType = CoinEnum.Gold;
            var coinValue = FormatNumber(data.Gold);
            if (data.Token > 0)
            {
                coinType = CoinEnum.Token;
                coinValue = FormatNumber(data.Token);
            }
            else if (data.Ticket > 0)
            {
                coinType = CoinEnum.Ticket;
                coinValue = FormatNumber(data.Ticket);
            }

            CoinItem_1.GetComponent<QuantityItemCoinItem>().Init(coinType, coinValue);

            if (isClaimed)
            {
                Complete_1.SetActive(true);
                Gray_1.SetActive(true);
                Mark_1.SetActive(true);
            }
        }
        else if (totalItem == 2)
        {
            Bg_1.SetActive(false);
            Bg_2.SetActive(true);
            Bg_3.SetActive(false);

            List<(CoinEnum, string)> coins = new List<(CoinEnum, string)>();
            if (data.Gold > 0) coins.Add((CoinEnum.Gold, FormatNumber(data.Gold)));
            if (data.Token > 0) coins.Add((CoinEnum.Token, FormatNumber(data.Token)));
            if (data.Ticket > 0) coins.Add((CoinEnum.Ticket, FormatNumber(data.Ticket)));

            CoinItem_2_1.GetComponent<QuantityItemCoinItem>().Init(coins[0].Item1, coins[0].Item2);
            CoinItem_2_2.GetComponent<QuantityItemCoinItem>().Init(coins[1].Item1, coins[1].Item2);

            if (isClaimed)
            {
                Complete_2.SetActive(true);
                Gray_2.SetActive(true);
                Mark_2.SetActive(true);
            }
        }
        else if(totalItem == 3)
        {
            Bg_1.SetActive(false);
            Bg_2.SetActive(false);
            Bg_3.SetActive(true);

            CoinItem_3_1.GetComponent<QuantityItemCoinItem>().Init(CoinEnum.Gold, FormatNumber(data.Gold));
            CoinItem_3_2.GetComponent<QuantityItemCoinItem>().Init(CoinEnum.Token, FormatNumber(data.Token));
            CoinItem_3_3.GetComponent<QuantityItemCoinItem>().Init(CoinEnum.Ticket, FormatNumber(data.Ticket));

            if (isClaimed)
            {
                Complete_3.SetActive(true);
                Gray_3.SetActive(true);
                Mark_3.SetActive(true);
            }
        }
    }

    public string FormatNumber(int quantity)
    {
        if (quantity < 1000) return quantity.ToString();

        if (quantity % 1000 == 0) return quantity / 1000 + "K";

        return quantity / 1000 + "K" + (quantity % 1000) / 100;
    }
}
