using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using TMPro;
using System.Text;

public class UpdateProfileview : MonoBehaviour
{
    public GameObject backButton;
    public GameObject updateButton;
    public TMP_InputField textName;
    public GameObject plahoderTextName;
    public GameObject profileView;
    public GameObject PopupErrorChangeName;
    public GameObject NoticeChangeNamePopup;
    //public GameObject quantityByteText;

    [Header("Edit profile")]
    public GameObject avatarButton;
    public GameObject avatarButtonDisable;
    public GameObject coverphotoButton;
    public GameObject coverphotoButtonDisable;

    [Header("info image")]
    public RawImage avatar;
    //public RawImage avatarFrame;
    public RawImage coverPhoto;

    [Header("List image")]
    public GridLayoutGroup listImage;
    public RawImage imageItemPrefab;

    private ImageData.ImageItemData _tempAvatarFrame;
    private string _tempName;
    private int _maxByteInput = 20;

    private ImageData.ImageItemData _itemAvatarSelected;
    private ImageData.ImageItemData _itemCoverSelected;

    // Start is called before the first frame update
    void Start()
    {
        avatarButtonDisable.SetActive(false);
        coverphotoButtonDisable.SetActive(true);
        avatarButton.SetActive(true);
        coverphotoButton.SetActive(false);
        textName.GetComponent<TMP_InputField>().interactable = !Current.Ins.player.IsEditName;
        LoadListImage(isAvtView: true);

        // create data word banned
        backButton.GetComponent<Button>().onClick.AddListener(() =>
        {
            SoundManager.instance.PlayUIEffect("se_click1");
            gameObject.SetActive(false);
        });

        //quantityByteText.GetComponent<TextMeshProUGUI>().SetText("0/" + _maxByteInput);
        //textName.GetComponent<TMP_InputField>().onValueChanged.AddListener((inputStr) =>
        //{
        //    quantityByteText.GetComponent<TextMeshProUGUI>().SetText((StringHelper.QuantityInput(inputStr)) + "/" + _maxByteInput);
        //});

        updateButton.GetComponent<Button>().onClick.AddListener(() =>
        {
            SoundManager.instance.PlayUIEffect("se_click1");

            _tempName = textName.GetComponent<TMP_InputField>().text;
            if (!Current.Ins.player.IsEditName && !string.IsNullOrWhiteSpace(_tempName))
            {
                NoticeChangeNamePopup.GetComponent<NoticeChangeNamePopup>().Init(
                    onCancel: () =>
                    {
                        return;
                    },
                    onConfirm: () =>
                    {
                        UpdateProfile();
                    });
            }
            else UpdateProfile();
        });

        coverphotoButtonDisable.GetComponent<Button>().onClickWithHCSound(() =>
        {
            avatarButtonDisable.SetActive(true);
            coverphotoButtonDisable.SetActive(false);
            avatarButton.SetActive(false);
            coverphotoButton.SetActive(true);
            LoadListImage(isAvtView: false);
        });

        avatarButtonDisable.GetComponent<Button>().onClickWithHCSound(() =>
        {
            avatarButtonDisable.SetActive(false);
            coverphotoButtonDisable.SetActive(true);
            avatarButton.SetActive(true);
            coverphotoButton.SetActive(false);
            LoadListImage(isAvtView: true);
        });

        //Getavt
        Current.Ins.UserAvatar((result) =>
            {
                avatar.texture = result.Texture;
            });

        //Current.Ins.UserAvatarFrame((result) =>
        //{
        //    avatarFrame.texture = result.Texture;
        //});
        //coverPhoto.texture = Current.Ins.UserCover?.Texture;

        Current.Ins.UserCover((result) =>
        {
            coverPhoto.texture = result.Texture;
        });

        plahoderTextName.GetComponent<TextMeshProUGUI>().text = Current.Ins.player.Name;
    }

    private void LoadListImage(bool isAvtView)
    {
        //delete childs in gird
        listImage.transform.RemoveAllChild();

        var lstImage = isAvtView ? Current.Ins.ImageData.Avatars : Current.Ins.ImageData.Covers;

        Action<ImageData.ImageItemData> selAction = (selected) =>
        {
            foreach (var image in lstImage)
            {
                var itemImage = Instantiate(imageItemPrefab, listImage.transform, false).gameObject.GetComponent<ImageItem>();
                itemImage.gameObject.GetComponent<RawImage>().texture = image.Texture;
                itemImage.Path = image.Path;
                itemImage.SetBorder(itemImage.Path == selected.Path);

                itemImage.gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
                itemImage.gameObject.GetComponent<Button>().onClick.AddListener(() =>
                {
                    SoundManager.instance.PlayUIEffect("se_click1");
                    listImage.GetComponentsInChildren<ImageItem>().ToList().ForEach(x => x.SetBorder(false));
                    itemImage.SetBorder(true);

                    if (isAvtView)
                    {
                        _itemAvatarSelected = image;
                        avatar.texture = _itemAvatarSelected.Texture;
                    }
                    else
                    {
                        _itemCoverSelected = image;
                        coverPhoto.texture = _itemCoverSelected.Texture;
                    }
                });
            }
        };

        if (isAvtView)
        {
            if (_itemAvatarSelected != null)
            {
                selAction.Invoke(_itemAvatarSelected);
            }
            else
            {
                Current.Ins.UserAvatar((result) =>
                {
                    _itemAvatarSelected = result;
                    selAction.Invoke(_itemAvatarSelected);
                });
            }
        }
        else
        {
            if (_itemCoverSelected != null)
            {
                selAction.Invoke(_itemCoverSelected);
            }
            else
            {
                Current.Ins.UserCover((result) =>
                {
                    _itemCoverSelected = result;
                    selAction.Invoke(_itemCoverSelected);
                });
            }
        }
    }
    private void UpdateProfile()
    {
        if (string.IsNullOrWhiteSpace(_tempName)) _tempName = Current.Ins.player.Name;

        string avtPath = _itemAvatarSelected == null ? Current.Ins.player.Avatar : _itemAvatarSelected.Path;
        string coverPath = _itemCoverSelected == null ? Current.Ins.player.Cover : _itemCoverSelected.Path;
        string avtFrPath = Current.Ins.player.AvatarFrame;

        UIHome.Ins.ShowHideLoadingPopup(true);
        StartCoroutine(Current.Ins.userAPI.UpdateUserProfile(avtPath, coverPath, avtFrPath, _tempName, (res) =>
        {
            UIHome.Ins.ShowHideLoadingPopup(false);
            if (res == null || res.errorCode != 0)
            {

                string errorMsg = "Request Error : UpdateProfile";
                if (res != null)
                {
                    if (res.errorCode == 15)
                    {
                        errorMsg += $"\nUser Name: {Current.Ins.player.Name}";
                    }
                }
                UIPopUpManager.instance.ShowErrorPopUp(errorMsg, res, UpdateProfile);

                Debug.Log("Error: Update Profile");

                return;
            }

            UIHome.Ins.ShowHideLoadingPopup(true);
            Current.Ins.ReloadUserProfile(callBackAfterDoneAPI: () =>
            {
                UIHome.Ins.ShowHideLoadingPopup(false);
                textName.GetComponent<TMP_InputField>().interactable = !Current.Ins.player.IsEditName;
                gameObject.SetActive(false);
                profileView.GetComponent<ProfileView>().LoadUserInfor();
            });

            plahoderTextName.GetComponent<TextMeshProUGUI>().text = _tempName;
            textName.GetComponent<TMP_InputField>().text = "";
        }));
    }
}
