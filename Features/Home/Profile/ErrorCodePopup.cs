using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ErrorCodePopup : MonoBehaviour
{
    public GameObject CloseBtn;

    private void Start()
    {
        CloseBtn.GetComponent<Button>().onClick.AddListener(() =>
        {
            SoundManager.instance.PlayUIEffect("se_click1");
            gameObject.SetActive(false);
        });
    }

    public void Init()
    {
        gameObject.SetActive(true);
    }
}
