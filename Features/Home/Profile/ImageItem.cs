using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageItem : MonoBehaviour
{
    public string Path;
    public GameObject OutLine;
    public void SetBorder(bool isActive) => OutLine.SetActive(isActive);
}
