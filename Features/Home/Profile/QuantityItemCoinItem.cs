using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using OutGameEnum;

public class QuantityItemCoinItem : MonoBehaviour
{
    public GameObject Gold;
    public GameObject Token;
    public GameObject Ticket;

    public TextMeshProUGUI ValueText;

    public void Init(CoinEnum coinType, string value)
    {
        Gold.SetActive(coinType == CoinEnum.Gold);
        Token.SetActive(coinType == CoinEnum.Token);
        Ticket.SetActive(coinType == CoinEnum.Ticket);
        ValueText.text = $"+{value}";
    }
}
