using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ReferralCode : MonoBehaviour
{
    [Header("Button")]
    public GameObject backButton;
    public GameObject submitButton;
    public GameObject referralCodeButton;

    [Header("View")]
    public GameObject errorCodePopup;
    public GameObject rewardPopup;
    public GameObject referralCodeText;

    // Start is called before the first frame update
    void Start()
    {
        backButton.GetComponent<Button>().onClick.AddListener(() =>
        {
            SoundManager.instance.PlayUIEffect("se_click1");
            gameObject.SetActive(false);
        });

        submitButton.GetComponent<Button>().onClick.AddListener(() =>
        {
            SoundManager.instance.PlayUIEffect("se_click1");

            if (referralCodeText.GetComponent<TextMeshProUGUI>().text.Trim().Equals("") || referralCodeText.GetComponent<TextMeshProUGUI>().text.Trim().Equals(Current.Ins.player.ReferralCode))
            {
                errorCodePopup.GetComponent<ErrorCodePopup>().Init();
                return;
            }

            CheckReferralCode();
        });
    }

    public void CheckReferralCode()
    {
        ReferralCodeRequest referralCodeRequest = new ReferralCodeRequest();
        referralCodeRequest.ReferralCode = referralCodeText.GetComponent<TextMeshProUGUI>().text.Trim();
        UIHome.Ins.ShowHideLoadingPopup(true);

        StartCoroutine(Current.Ins.userAPI.CheckReferralCode(referralCodeRequest, (res) =>
        {
            UIHome.Ins.ShowHideLoadingPopup(false);
            if (res == null || res.errorCode != 0)
            {
                errorCodePopup.GetComponent<ErrorCodePopup>().Init();
                return;
            }

            rewardPopup.SetActive(true);
            referralCodeButton.GetComponent<Button>().interactable = false;
            referralCodeButton.transform.GetChild(1).gameObject.SetActive(true);

            Current.Ins.ReloadUserProfile(gameObject.transform);
        }));
    }
}
