using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CouponView : MonoBehaviour
{
    public GameObject backButton;
    public GameObject submitButton;

    public GameObject couponText;
    public GameObject errorCodePopup;
    public GameObject successCodePopup;

    private void Start()
    {
        backButton.GetComponent<Button>().onClick.AddListener(() =>
        {
            SoundManager.instance.PlayUIEffect("se_click1");
            gameObject.SetActive(false);
        });


        submitButton.GetComponent<Button>().onClick.AddListener(() =>
        {
            SoundManager.instance.PlayUIEffect("se_click1");

            if (couponText.GetComponent<TMP_InputField>().text.Trim().Equals(""))
            {
                errorCodePopup.GetComponent<ErrorCodePopup>().Init();
                return;
            }

            CheckCouponCode();
        });
    }

    public void CheckCouponCode()
    {
        CouponRequest couponRequest = new CouponRequest();
        couponRequest.Code = couponText.GetComponent<TMP_InputField>().text.Trim();
        UIHome.Ins.ShowHideLoadingPopup(true);

        StartCoroutine(Current.Ins.userAPI.CheckCouponCode(couponRequest, (res) =>
        {
            UIHome.Ins.ShowHideLoadingPopup(false);

            if (res == null || res.errorCode != 0)
            {
                errorCodePopup.GetComponent<ErrorCodePopup>().Init();
                return;
            }

            CouponCodeData couponCodeData = new CouponCodeData();
            couponCodeData.Gold = res.data.gold;
            couponCodeData.Token = res.data.token;
            couponCodeData.Ticket = res.data.ticket;

            successCodePopup.GetComponent<SuccessCodePopup>().Init(couponCodeData);

            couponText.GetComponent<TMP_InputField>().text = "";
            Current.Ins.ReloadUserProfile(gameObject.transform);
        }));
    }
}
