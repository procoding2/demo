using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro; 

public class SuccessCodePopup : MonoBehaviour
{
    public GameObject textGold;
    public GameObject textToken;
    public GameObject textTicket;

    public GameObject buttonOK;
    // Start is called before the first frame update
    void Start()
    {
        buttonOK.GetComponent<Button>().onClick.AddListener(() =>
        {
            SoundManager.instance.PlayUIEffect("se_click1");
            gameObject.SetActive(false);
        });
    }

    public void Init(CouponCodeData couponCodeData)
    {
        textGold.GetComponent<TextMeshProUGUI>().SetText(couponCodeData.Gold.ToString());
        textToken.GetComponent<TextMeshProUGUI>().SetText(couponCodeData.Token.ToString());
        textTicket.GetComponent<TextMeshProUGUI>().SetText(couponCodeData.Ticket.ToString());
        gameObject.SetActive(true);
    }
}
