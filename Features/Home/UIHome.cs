using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using OutGameEnum;
using UnityEngine;
using UnityEngine.UI;
using HomeTab;
using DanielLochner.Assets.SimpleScrollSnap;
using TMPro;
using AnimationStateEnums;
using DG.Tweening;
using GameEnum;

public class UIHome : UIBase<UIHome>
{
    protected override void InitIns() => Ins = this;


    [SerializeField]
    private HeaderView _headerView;
    public HeaderView headerView => _headerView;

    [SerializeField, Header("TabView")]
    private GameObject homeView, eventView, rankingView, historyView;

    [SerializeField, Header("TabControls")]
    private TabMenuEvent _homeTabIcon, _eventTabIcon, _rankingTabIcon, _historyTabIcon;
    public TabMenuEvent homeTabIcon => _homeTabIcon;
    public TabMenuEvent eventTabIcon => _eventTabIcon;
    public TabMenuEvent rankingTabIcon => _rankingTabIcon;
    public TabMenuEvent historyTabIcon => _historyTabIcon;

    [SerializeField, Header("Popup")]
    private GameObject levelUpPopup;
    [SerializeField]
    private SimpleScrollSnap _scrollSnapView;
    public SimpleScrollSnap scrollSnapView => _scrollSnapView;
    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();

        Application.targetFrameRate = 60;
        //HCSound.Ins.PlayBackgrounds(HCSound.Ins.BackgroundSounds);
        SoundManager.instance.PlayMusic("bgm_home");

        scrollSnapView.GetComponent<SimpleScrollSnap>().OnPanelCentered.AddListener((current, before) =>
        {
            if (current == 0)
            {
                UIHomeTab.DefaultView = TabbarEnum.Home;
            }
            else if (current == 1)
            {
                UIHomeTab.DefaultView = TabbarEnum.Event;
                UIEventTab.Ins.Init();
            }
            else if (current == 2)
            {
                UIHomeTab.DefaultView = TabbarEnum.Ranking;
                RankTab.UIRankingTab.Ins.Init();
            }
            else if (current == 3)
            {
                UIHomeTab.DefaultView = TabbarEnum.History;
                HistoryGame.Ins.Init();
            }
        });

        FirebaseManager.Instance().PropertyChanged += UIHome_PropertyChanged;
        Current.Ins.PropertyChanged += Ins_PropertyChanged;

        // HistoryTab
        setupHistoryDot(true);

        StartCoroutine(ChangeTab());
    }

    private IEnumerator ChangeTab()
    {
        yield return new WaitForSeconds(0.1f);
        switch (UIHomeTab.DefaultView)
        {
            case TabbarEnum.Event:
                ShowMainTapView(TabbarEnum.Event);
                break;
            case TabbarEnum.Home_GameMode_Bingo:
                UIHomeTab.Ins.OpenGameMode(MiniGameEnum.Bingo);
                break;
            case TabbarEnum.Home_GameMode_Bubble:
                UIHomeTab.Ins.OpenGameMode(MiniGameEnum.Bubble);
                break;
            case TabbarEnum.Home_GameMode_Solitaire:
                UIHomeTab.Ins.OpenGameMode(MiniGameEnum.Solitaire);
                break;
        }

        if (UIHomeTab.DefaultView != TabbarEnum.Event) UIHomeTab.DefaultView = TabbarEnum.Home;
    }

    //private IEnumerator SetSizeIsOn()
    //{
    //    yield return new WaitForEndOfFrame();
    //    homeTabIcon.GetComponent<TabItem>().showDotNoti(number: 0, isShow: false);
    //    setupHistoryDot();

    //    homeTabIcon.GetComponent<TabItem>().SetAnimation(TabItemAnimationStateEnum.OnSelected);
    //    homeTabIcon.GetComponent<Toggle>().onValueChanged.AddListener((isSel) =>
    //    {
    //        homeTabIcon.GetComponent<TabItem>().showDotNoti(number: 0, isShow: false);
    //        if (isSel) SoundManager.instance.PlayUIEffect("se_click1");
    //        homeTabIcon.GetComponent<TabItem>().SetAnimation(isSel ? TabItemAnimationStateEnum.OnSelected : TabItemAnimationStateEnum.Idle);
    //    });

    //    eventTabIcon.GetComponent<Toggle>().onValueChanged.AddListener((isSel) =>
    //        {
    //            if (isSel)
    //            {

    //    homeTabIcon.GetComponent<TabItem>().showDotNoti(number: numberDaily, isShow: numberDaily > 0);
    //    SoundManager.instance.PlayUIEffect("se_click1");
    //            }
    //eventTabIcon.GetComponent<TabItem>().SetAnimation(isSel ? TabItemAnimationStateEnum.OnSelected : TabItemAnimationStateEnum.Idle);
    //        });

    //    rankingTabIcon.GetComponent<Toggle>().onValueChanged.AddListener((isSel) =>
    //    {
    //        if (isSel)
    //        {
    //            var numberDaily = PlayerPrefs.GetInt(PlayerPrefsConst.NUMBER_DAILY_MISSION, 0);
    //            homeTabIcon.GetComponent<TabItem>().showDotNoti(number: numberDaily, isShow: numberDaily > 0);
    //            SoundManager.instance.PlayUIEffect("se_click1");
    //        }
    //        rankingTabIcon.GetComponent<TabItem>().SetAnimation(isSel ? TabItemAnimationStateEnum.OnSelected : TabItemAnimationStateEnum.Idle);
    //    });

    //    historyTabIcon.GetComponent<Toggle>().onValueChanged.AddListener((isSel) =>
    //    {
    //        if (isSel)
    //        {
    //            SoundManager.instance.PlayUIEffect("se_click1");
    //            //dot daily mission
    //            var numberDaily = PlayerPrefs.GetInt(PlayerPrefsConst.NUMBER_DAILY_MISSION, 0);
    //            homeTabIcon.GetComponent<TabItem>().showDotNoti(number: numberDaily, isShow: numberDaily > 0);
    //            //dot history
    //            PlayerPrefs.SetInt(PlayerPrefsConst.NUMBER_HISTORY_EVENT, 0);
    //            historyTabIcon.GetComponent<TabItem>().showDotNoti(number: 1, isShow: false);
    //        }

    //        historyTabIcon.GetComponent<TabItem>().SetAnimation(isSel ? TabItemAnimationStateEnum.OnSelected : TabItemAnimationStateEnum.Idle);
    //    });

    //    isOn.GetComponent<RectTransform>().sizeDelta = homeTabIcon.GetComponent<RectTransform>().sizeDelta;
    //}

    private void UIHome_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
    {
        if (e.PropertyName == StringConst.KEY_DATA_LEVEL_UP)
        {
            showLevelUpPopup();
        }
    }

    private void Ins_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
    {
        if (e.PropertyName == StringConst.SOCKET_ICON_HISTORY)
        {
            setupHistoryDot();
        }

        if (e.PropertyName == StringConst.PROPERTY_USER_INFOR)
        {
            // HomeTab
            var numberDaily = PlayerPrefsEx.Get(PlayerPrefsConst.NUMBER_DAILY_MISSION, 0);
            var dailyReward = Current.Ins.player.DailyRewardNotClaim;

            SideBarMenu.Ins.setupViewDotDaily();

            if (numberDaily > 0 || dailyReward > 0)
            {
                UIHome.Ins.homeTabIcon.showDotNoti(numberDaily + dailyReward, true);
            }
            else
            {
                UIHome.Ins.homeTabIcon.showDotNoti(0, false);
            }
        }
    }

    private void setupHistoryDot(bool isStart = false)
    {

        if (isStart)
        {
            HistoryGame.Ins.Init();
        }
        else
        {
            var numberHistory = PlayerPrefsEx.Get(PlayerPrefsConst.NUMBER_HISTORY_EVENT, 0);
            if (numberHistory > 0)
            {
                numberHistory += historyTabIcon.CurrentDotNum;
                historyTabIcon.showDotNoti(numberHistory, true);
            }
        }
    }

    public void ShowHideLoadingPopup(bool isShow)
    {
        UIPopUpManager.instance.ShowLoadingCircle(isShow);
        //loadingPopup.SetActive(isShow);
    }



    public void showLevelUpPopup()
    {
        var isMiniGame = PlayerPrefsEx.Get(PlayerPrefsConst.KEY_PREF_IS_MINI_GAME, 0);
        var stringJson = PlayerPrefsEx.Get(PlayerPrefsConst.KEY_PREF_LEVEL_UP, "");
        if (isMiniGame == 1 || stringJson == "") return;
        var dataLevelUp = JsonUtility.FromJson<UserProfileResponse.Data>(stringJson);
        levelUpPopup.GetComponent<LevelUpPopup>().Init(dataLevelUp);
    }

    public void DisableTabAction(bool isEnable)
    {
        //scrollSnapView.GetComponent<SimpleScrollSnap>().Toggles.ToList().ForEach(item => item.interactable = isEnable);
        //scrollSnapView.GetComponent<SimpleScrollSnap>().UseSwipeGestures = isEnable;
        homeTabIcon.GetComponent<TabMenuEvent>().EnableDisableAllButton(isEnable);
        eventTabIcon.GetComponent<TabMenuEvent>().EnableDisableAllButton(isEnable);
        rankingTabIcon.GetComponent<TabMenuEvent>().EnableDisableAllButton(isEnable);
        historyTabIcon.GetComponent<TabMenuEvent>().EnableDisableAllButton(isEnable);
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        FirebaseManager.Instance().PropertyChanged -= UIHome_PropertyChanged;
        Current.Ins.PropertyChanged -= Ins_PropertyChanged;
    }

    public void AnimIsOn(GameObject go)
    {
        //this.isOn.transform.DOMove(go.transform.position, 0.2f).SetEase(Ease.OutQuint);
    }

    public void ShowMainTapView(TabbarEnum tapEnum)
    {
        //homeView.SetActive(false);
        //eventView.SetActive(false);
        //rankingView.SetActive(false);
        //historyView.SetActive(false);

        switch (tapEnum)
        {
            case TabbarEnum.Home:
                //homeView.SetActive(true);
                scrollSnapView.GoToPanel(0); break;
            case TabbarEnum.Event:
                //eventView.SetActive(true);
                scrollSnapView.GoToPanel(1); break;
            case TabbarEnum.Ranking:
                //rankingView.SetActive(true);
                scrollSnapView.GoToPanel(2); break;
            case TabbarEnum.History:
                //historyView.SetActive(true);
                scrollSnapView.GoToPanel(3); break;
        }

    }



}