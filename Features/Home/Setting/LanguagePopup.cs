using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using OutGameEnum;

public class LanguagePopup : MonoBehaviour
{
    public GameObject English;
    public GameObject Korean;
    public GameObject Vietnamese;

    public GameObject closeBtn;

    // Start is called before the first frame update
    void Start()
    {
        closeBtn.GetComponent<Button>().onClick.AddListener(() =>
        {
            SoundManager.instance.PlayUIEffect("se_click1");
            gameObject.SetActive(false);
        });

        English.GetComponent<Button>().onClickWithHCSound(() =>
        {
            Changelanguage(LanguageEnum.eng);
        });

        Korean.GetComponent<Button>().onClickWithHCSound(() =>
        {
            Changelanguage(LanguageEnum.kor);
        });

        Vietnamese.GetComponent<Button>().onClickWithHCSound(() =>
        {
            Changelanguage(LanguageEnum.vi);
        });

        InitLanguage();
    }

    private void Config_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
    {
        if (e.PropertyName == StringConst.PROPERTY_LANGUAGE) InitLanguage();
    }

    private void Changelanguage(LanguageEnum language) => StartCoroutine(Current.Ins.Config.ChangeLanguage(language));

    private void InitLanguage()
    {
        var lang = Current.Ins.Config.GetCurrentLanguage();
        switch (lang)
        {
            case LanguageEnum.eng:
                English.GetComponent<LanguageItem>().BG_On.SetActive(true);
                Korean.GetComponent<LanguageItem>().BG_On.SetActive(false);
                Vietnamese.GetComponent<LanguageItem>().BG_On.SetActive(false);
                break;

            case LanguageEnum.kor:
                English.GetComponent<LanguageItem>().BG_On.SetActive(false);
                Korean.GetComponent<LanguageItem>().BG_On.SetActive(true);
                Vietnamese.GetComponent<LanguageItem>().BG_On.SetActive(false);
                break;
            case LanguageEnum.vi:
                English.GetComponent<LanguageItem>().BG_On.SetActive(false);
                Korean.GetComponent<LanguageItem>().BG_On.SetActive(false);
                Vietnamese.GetComponent<LanguageItem>().BG_On.SetActive(true);
                break;
        }
    }

    private void OnEnable()
    {
        Current.Ins.Config.PropertyChanged += Config_PropertyChanged;
    }

    private void OnDisable()
    {
        Current.Ins.Config.PropertyChanged -= Config_PropertyChanged;
    }
}
