using UnityEngine;
using UnityEngine.UI;
using OutGameEnum;
using System;

public class SettingView : UIBase<SettingView>
{
    protected override void InitIns() => Ins = this;

    public GameObject profile;

    public GameObject link;
    public GameObject linkAccountTitle;
    public GameObject accountLinkedTitle;

    public GameObject deleteAccountButton;
    public GameObject recoverAccountButton;
    public GameObject logoutButton;
    public GameObject backButton;

    public SwitchToggle backgroundSound;
    public SwitchToggle gameSound;

    [Header("Popup content")]
    public GameObject socialLogout;
    public GameObject socialLogoutButton;
    public GameObject socialCloseButton;
    public GameObject socialCancelButton;

    public GameObject guestLogout;
    public GameObject linkAccountButton;
    public GameObject linkAccountPopup;
    public GameObject closeLinkAccountBtn;
    public GameObject googleLinkBtn;
    public GameObject appleLinkBtn;
    public GameObject fncyLinkBtn;
    public GameObject facebookLinkBtn;

    public GameObject guestLogoutButton;
    public GameObject guestCloseButton;
    public GameObject guestCancelButton;

    public GameObject supportContent;

    public GameObject confirmDeleteAccountPopup;
    public GameObject confirmDeleteAccountBtn;
    public GameObject closeConfirmDeleteAccountBtn;
    public GameObject cancelConfirmDeleteAccountBtn;

    public GameObject afterDeleteAccountPopup;
    public GameObject closeAfterDeleteAccountBtn;

    public GameObject linkAccountWarningPopup;
    public GameObject closeLinkAccountWarningPopup;
    public GameObject cancelLinkAccountWarningPopup;
    public GameObject confirmLinkAccountWarningPopup;

    public GameObject languageButton;
    public GameObject languageTitleEng;
    public GameObject languageTitleKor;
    public GameObject languageTitleVi;

    public GameObject languagePopup;

    public GameObject notiBtn;

    [Header("Support")]
    public GameObject aboutHCRef;
    public GameObject helpSupportRef;
    public GameObject privacyPolicyRef;
    public GameObject termConditionsRef;

    protected override void Start()
    {
        notiBtn.GetComponent<Button>().onClickWithHCSound(DeviceHelper.OpenSetting);

        backButton.GetComponent<Button>().onClickWithHCSound(OnClose);

        socialLogoutButton.GetComponent<Button>().onClickWithHCSound(logoutCurrentUser);

        guestLogoutButton.GetComponent<Button>().onClickWithHCSound(logoutCurrentUser);

        OnOffDeleteAndRecoverBtn();

        link.GetComponent<Button>().onClickWithHCSound(OnClickLinkAccount);

        linkAccountButton.GetComponent<Button>().onClickWithHCSound(OnClickLinkAccount);

        closeLinkAccountBtn.GetComponent<Button>().onClickWithHCSound(() => linkAccountPopup.SetActive(false));

        googleLinkBtn.GetComponent<Button>().onClickWithHCSound(GoogleLogin);

        facebookLinkBtn.GetComponent<Button>().onClickWithHCSound(FacebookLogin);

        appleLinkBtn.GetComponent<Button>().onClickWithHCSound(AppleLogin);

        LoadLinkAccState();

        aboutHCRef.GetComponent<Button>().onClickWithHCSound(() => supportContent.GetComponent<SupportContent>().Show(SettingSupportContentEnum.About));

        privacyPolicyRef.GetComponent<Button>().onClickWithHCSound(() => Application.OpenURL(StringConst.POLICY_URL));

        termConditionsRef.GetComponent<Button>().onClickWithHCSound(() => Application.OpenURL(StringConst.CONDITION_URL));

        logoutButton.GetComponent<Button>().onClickWithHCSound(() =>
        {
            if (Current.Ins.player.SocialType == SocialTypeEnum.Guest) guestLogout.SetActive(true);
            else socialLogout.SetActive(true);
        });

        guestCloseButton.GetComponent<Button>().onClickWithHCSound(() => guestLogout.SetActive(false));

        guestCancelButton.GetComponent<Button>().onClickWithHCSound(() => guestLogout.SetActive(false));

        socialCloseButton.GetComponent<Button>().onClickWithHCSound(() => socialLogout.SetActive(false));

        socialCancelButton.GetComponent<Button>().onClickWithHCSound(() => socialLogout.SetActive(false));

        backgroundSound.GetComponent<Toggle>().isOn = Current.Ins.Config.SoundConfig.BackgroundVolume == 1f;
        backgroundSound.OnAfterChange = (isOn) =>
        {
            SoundManager.instance.BGM_On(isOn);
            Current.Ins.Config.SoundConfig = new SoundConfigData()
            {
                BackgroundVolume = isOn ? 1f : 0f,
                GameVolume = Current.Ins.Config.SoundConfig.GameVolume
            };
        };

        gameSound.GetComponent<Toggle>().isOn = Current.Ins.Config.SoundConfig.GameVolume == 1f;
        gameSound.OnAfterChange = (isOn) =>
        {
            SoundManager.instance.SE_On(isOn);

            Current.Ins.Config.SoundConfig = new SoundConfigData()
            {
                BackgroundVolume = Current.Ins.Config.SoundConfig.BackgroundVolume,
                GameVolume = isOn ? 1f : 0f
            };
        };

        closeAfterDeleteAccountBtn.GetComponent<Button>().onClickWithHCSound(() =>
        {
            afterDeleteAccountPopup.SetActive(false);
            LogOut();
        });

        closeConfirmDeleteAccountBtn.GetComponent<Button>().onClickWithHCSound(() => confirmDeleteAccountPopup.SetActive(false));

        confirmDeleteAccountBtn.GetComponent<Button>().onClickWithHCSound(DeleteAccount);

        cancelConfirmDeleteAccountBtn.GetComponent<Button>().onClickWithHCSound(() => confirmDeleteAccountPopup.SetActive(false));

        deleteAccountButton.GetComponent<Button>().onClickWithHCSound(() =>
        {
            confirmDeleteAccountPopup.SetActive(true);
            afterDeleteAccountPopup.SetActive(false);
        });

        recoverAccountButton.GetComponent<Button>().onClickWithHCSound(RecoverAccount);

        closeLinkAccountWarningPopup.GetComponent<Button>().onClickWithHCSound(() => linkAccountWarningPopup.SetActive(false));

        cancelLinkAccountWarningPopup.GetComponent<Button>().onClickWithHCSound(() => linkAccountWarningPopup.SetActive(false));

        languageButton.GetComponent<Button>().onClickWithHCSound(() => languagePopup.SetActive(true));

        InitLanguage();
    }

    private void Config_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
    {
        if (e.PropertyName == StringConst.PROPERTY_LANGUAGE)
        {
            InitLanguage();
        }
    }

    private void InitLanguage()
    {
        var lang = Current.Ins.Config.GetCurrentLanguage();
        switch (lang)
        {
            case LanguageEnum.eng:
                languageTitleEng.SetActive(true);
                languageTitleKor.SetActive(false);
                languageTitleVi.SetActive(false);
                break;

            case LanguageEnum.kor:
                languageTitleEng.SetActive(false);
                languageTitleKor.SetActive(true);
                languageTitleVi.SetActive(false);
                break;
            case LanguageEnum.vi:
                languageTitleEng.SetActive(false);
                languageTitleKor.SetActive(false);
                languageTitleVi.SetActive(true);
                break;
        }
    }

    private void LoadLinkAccState()
    {
        if (Current.Ins.player.SocialType == SocialTypeEnum.Guest)
        {
            link.GetComponent<Button>().interactable = true;
            linkAccountTitle.SetActive(true);
            accountLinkedTitle.SetActive(false);
        }
        else
        {
            //link.GetComponent<Button>().interactable = false;
            //linkAccountTitle.SetActive(false);
            //accountLinkedTitle.SetActive(true);
            if (Current.Ins.player.SocialType == SocialTypeEnum.Google)
            {
                facebookLinkBtn.GetComponent<Button>().interactable = false;
                appleLinkBtn.GetComponent<Button>().interactable = false;
                googleLinkBtn.GetComponent<Button>().onClick.RemoveAllListeners();

                var clb = googleLinkBtn.GetComponent<Button>().colors;
                clb.pressedColor = Color.white;
                clb.selectedColor = Color.white;
                clb.highlightedColor = Color.white;
                googleLinkBtn.GetComponent<Button>().colors = clb;
            }
            else if (Current.Ins.player.SocialType == SocialTypeEnum.Apple)
            {
                facebookLinkBtn.GetComponent<Button>().interactable = false;
                googleLinkBtn.GetComponent<Button>().interactable = false;
                appleLinkBtn.GetComponent<Button>().onClick.RemoveAllListeners();

                var clb = appleLinkBtn.GetComponent<Button>().colors;
                clb.pressedColor = Color.white;
                clb.selectedColor = Color.white;
                clb.highlightedColor = Color.white;
                appleLinkBtn.GetComponent<Button>().colors = clb;
            }
            else if (Current.Ins.player.SocialType == SocialTypeEnum.Facebook)
            {
                appleLinkBtn.GetComponent<Button>().interactable = false;
                googleLinkBtn.GetComponent<Button>().interactable = false;
                facebookLinkBtn.GetComponent<Button>().onClick.RemoveAllListeners();

                var clb = facebookLinkBtn.GetComponent<Button>().colors;
                clb.pressedColor = Color.white;
                clb.selectedColor = Color.white;
                clb.highlightedColor = Color.white;
                facebookLinkBtn.GetComponent<Button>().colors = clb;
            }
        }
    }

    private void OnClickLinkAccount()
    {
        guestLogout.SetActive(false);
        linkAccountPopup.SetActive(true);
    }

    private void DeleteAccount()
    {
        UIHome.Ins.ShowHideLoadingPopup(true);
        StartCoroutine(Current.Ins.userAPI.DeleteAccount((res) =>
        {
            UIHome.Ins.ShowHideLoadingPopup(false);
            if (res == null || res.errorCode != 0)
            {
                UIPopUpManager.instance.ShowErrorPopUp("Request Error : DeleteAccount", res, DeleteAccount);
                return;
            }

            Current.Ins.ReloadUserProfile(callBackAfterDoneAPI: () =>
            {
                confirmDeleteAccountPopup.SetActive(false);
                afterDeleteAccountPopup.SetActive(true);
                //OnOffDeleteAndRecoverBtn();
            });
        }));
    }

    private void RecoverAccount()
    {
        UIHome.Ins.ShowHideLoadingPopup(true);
        StartCoroutine(Current.Ins.userAPI.RecoveryAccount((res) =>
        {
            UIHome.Ins.ShowHideLoadingPopup(false);
            if (res == null || res.errorCode != 0)
            {
                UIPopUpManager.instance.ShowErrorPopUp("Request Error : RecoverAccount", res, RecoverAccount);
                return;
            }

            Current.Ins.ReloadUserProfile(callBackAfterDoneAPI: OnOffDeleteAndRecoverBtn);
        }));
    }

    private void OnOffDeleteAndRecoverBtn()
    {
        deleteAccountButton.SetActive(!Current.Ins.player.IsDelete);
        recoverAccountButton.SetActive(Current.Ins.player.IsDelete);
    }

    private void OnClose()
    {
        profile.SetActive(true);
        gameObject.SetActive(false);
    }

    private void logoutCurrentUser()
    {
        UIHome.Ins.ShowHideLoadingPopup(true);
        StartCoroutine(Current.Ins.userAPI.Logout((res) =>
        {
            UIHome.Ins.ShowHideLoadingPopup(false);
            if (res == null || res.errorCode != 0)
            {
                UIPopUpManager.instance.ShowErrorPopUp("Request Error : logoutCurrentUser", res, logoutCurrentUser);
                return;
            }

            LogOut();
        }));
    }

    private void LogOut()
    {
        if (Current.Ins.player.SocialType == SocialTypeEnum.Google)
        {
            GoogleSignInService.Instance().SignOutGoogle();
        }
        Current.Ins.socketAPI.RemoveSocket(StringConst.SOCKET_DAILY_MISSION);
        Current.Ins.socketAPI.RemoveSocket(StringConst.SOCKET_ICON_HISTORY);
        Current.Ins.ClearPlayerDataLocal();
        SceneHelper.LoadScene(SceneEnum.Welcome);
    }

    private void GoogleLogin()
    {
        UIHome.Ins.ShowHideLoadingPopup(true);
        GoogleSignInService.Instance().SignInGoogle((isSuccess, msg, loginReq) =>
        {
            AddJob(() =>
            {
                UIHome.Ins.ShowHideLoadingPopup(false);
                if (!isSuccess) return;

                //linkAccountWarningPopup.SetActive(true);
                linkAccount(loginReq.SocialId, SocialTypeEnum.Google, loginReq.Name);
            });
        });
    }

    private void FacebookLogin()
    {
        UIHome.Ins.ShowHideLoadingPopup(true);
        Action<LoginSocialRequest, bool> job = (loginReq, isSuccess) =>
        {
            AddJob(() =>
            {
                UIHome.Ins.ShowHideLoadingPopup(false);
                if (!isSuccess) return;

                //linkAccountWarningPopup.SetActive(true);
                linkAccount(loginReq.SocialId, SocialTypeEnum.Facebook, loginReq.Name);
            });
        };

        FacebookSignInService.Ins.LoginFacebook((isSuccess, msg, loginReq) =>
        {
            job.Invoke(loginReq, isSuccess);
        });
    }

    private void AppleLogin()
    {
        UIHome.Ins.ShowHideLoadingPopup(true);
        Action<LoginSocialRequest, bool> job = (loginReq, isSuccess) =>
        {
            AddJob(() =>
            {
                UIHome.Ins.ShowHideLoadingPopup(false);
                if (!isSuccess) return;

                linkAccount(loginReq.SocialId, SocialTypeEnum.Apple, loginReq.Name);
            });
        };

        AppleSignInService.Ins.LoginApple((isSuccess, msg, loginReq) =>
        {
            job.Invoke(loginReq, isSuccess);
        });
    }

    private void linkAccount(string socialId, SocialTypeEnum socialType, string name)
    {
        UIHome.Ins.ShowHideLoadingPopup(true);
        StartCoroutine(Current.Ins.userAPI.LinkedAccount(socialId, socialType, name, (res) =>
        {
            UIHome.Ins.ShowHideLoadingPopup(false);
            if (res == null || res.errorCode != 0)
            {
                if (res != null && res.errorCode == 14)
                {
                    if (socialType == SocialTypeEnum.Google) GoogleSignInService.Instance().SignOutGoogle();

                    linkAccountWarningPopup.SetActive(true);
                    return;
                }
                UIPopUpManager.instance.ShowErrorPopUp("Request Error : UpdateProfile", res, () => linkAccount(socialId, socialType, name));



                Debug.Log("Error: LinkedAccount");

                return;
            }

            Current.Ins.ReloadUserProfile(callBackAfterDoneAPI: () =>
            {
                //linkAccountWarningPopup.SetActive(false);
                //linkAccountPopup.SetActive(false);
                LoadLinkAccState();
            });

            Current.Ins.player.Id = socialId;
            Current.Ins.player.SocialType = socialType;
            Current.Ins.SetPlayerDataLocal();
        }));
    }

    private void OnEnable()
    {
        //
        profile.SetActive(false);
        //
        Current.Ins.Config.PropertyChanged += Config_PropertyChanged;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        Current.Ins.Config.PropertyChanged -= Config_PropertyChanged;
    }
}
