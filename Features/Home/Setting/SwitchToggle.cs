using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using DG.Tweening;
using TMPro;

public class SwitchToggle : MonoBehaviour
{
    public GameObject switchSquare;
    public GameObject stateTextOn;
    public GameObject stateTextOff;
    public GameObject OnBg;
    public GameObject OffBg;

    public Action<bool> OnAfterChange;

    private Toggle _toggle;

    private Vector2 _buttonPos;
    private Vector2 _stateTextPosOn;
    private Vector2 _stateTextPosOff;

    void Awake()
    {
        _toggle = GetComponent<Toggle>();

        _buttonPos = switchSquare.GetComponent<RectTransform>().anchoredPosition;
        _stateTextPosOn = stateTextOn.GetComponent<RectTransform>().anchoredPosition;
        _stateTextPosOff = stateTextOff.GetComponent<RectTransform>().anchoredPosition;

        _toggle.onValueChanged.AddListener((isOn) =>
        {
            SoundManager.instance.PlayUIEffect("se_click1");
            OnSwitch(isOn);
        });

        OnSwitch(_toggle.isOn);
    }

    private void OnSwitch(bool isOn)
    {
        OnBg.SetActive(isOn);
        OffBg.SetActive(!isOn);
        stateTextOn.SetActive(isOn);
        stateTextOff.SetActive(!isOn);

        switchSquare.GetComponent<RectTransform>().DOAnchorPos(isOn ? _buttonPos * -1 : _buttonPos, .2f).SetEase(Ease.InOutBack);

        if (isOn) stateTextOn.GetComponent<RectTransform>().DOAnchorPos(_stateTextPosOn, .2f).SetEase(Ease.InOutBack);
        else stateTextOff.GetComponent<RectTransform>().DOAnchorPos(_stateTextPosOff, .2f).SetEase(Ease.InOutBack);

        OnAfterChange?.Invoke(isOn);
    }

    void OnDestroy()
    {
        _toggle.onValueChanged.RemoveListener(OnSwitch);
    }
}
