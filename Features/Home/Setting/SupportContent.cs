using System;
using System.Collections;
using System.Collections.Generic;
using OutGameEnum;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class SupportContent : MonoBehaviour
{
    public Button closeBtn;
    public GameObject supportContentBg;

    public GameObject AboutTitle;
    public GameObject AboutContent;

    public GameObject HowToTitle;
    public GameObject HowToContent;

    public GameObject HelpTitle;
    public GameObject HelpContent;

    public GameObject PrivacyTitle;
    public GameObject PrivacyContent;

    public GameObject ReportTitle;
    public GameObject ReportContent;

    public GameObject TermTitle;
    public GameObject TermContent;

    private GameObject _ObjectTitleActive;
    private GameObject _ObjectContentActive;

    private float _YShowSupportContent = 0;
    private float _YHideSupportContent;

    // Start is called before the first frame update
    void Start()
    {
        closeBtn.onClick.AddListener(() =>
        {
            SoundManager.instance.PlayUIEffect("se_click1");
            gameObject.transform.DOMoveY(_YHideSupportContent, 0.3f)
            .OnComplete(() =>
            {
                supportContentBg.SetActive(false);

                if (_ObjectTitleActive != null)
                {
                    _ObjectTitleActive.SetActive(false);
                }

                if (_ObjectContentActive != null)
                {
                    _ObjectContentActive.GetComponent<ScrollRect>().verticalNormalizedPosition = 1f;
                    _ObjectContentActive.SetActive(false);
                }
            });
        });

        _YHideSupportContent = gameObject.transform.position.y;
    }

    public void Show(SettingSupportContentEnum contentType)
    {
        switch (contentType)
        {
            case SettingSupportContentEnum.About:
                _ObjectTitleActive = AboutTitle;
                _ObjectContentActive = AboutContent;

                break;
            case SettingSupportContentEnum.HowTo:
                _ObjectTitleActive = HowToTitle;
                _ObjectContentActive = HowToContent;

                break;
            case SettingSupportContentEnum.Help:
                _ObjectTitleActive = HelpTitle;
                _ObjectContentActive = HelpContent;

                break;
            case SettingSupportContentEnum.Privacy:
                _ObjectTitleActive = PrivacyTitle;
                _ObjectContentActive = PrivacyContent;

                break;
            case SettingSupportContentEnum.Report:
                _ObjectTitleActive = ReportTitle;
                _ObjectContentActive = ReportContent;

                break;
            case SettingSupportContentEnum.Term:
                _ObjectTitleActive = TermTitle;
                _ObjectContentActive = TermContent;

                break;
        }

        if (_ObjectTitleActive != null)
        {
            _ObjectTitleActive.SetActive(true);
        }

        if (_ObjectContentActive != null)
        {
            _ObjectContentActive.SetActive(true);
        }

        gameObject.transform.DOMoveY(_YShowSupportContent, 0.3f);
        supportContentBg.SetActive(true);
    }
}
