using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using OutGameEnum;
using UnityEngine.Localization.Components;

public class HistoryKnockOutResult : MonoBehaviour
{
    public KnockOutMatch[] matchs;
    public ScrollRect headerScroll;
    public GameObject[] HeaderItems;
    public ScrollRect scroll;

    public GameObject timeLeft;
    public Button NextRoundBtn;
    public Button ExitBtn;

    private double? _timeRemain = null;

    private void Start()
    {
        scroll.onValueChanged.AddListener((vector) =>
        {
            headerScroll.normalizedPosition = new Vector2(vector.x, headerScroll.normalizedPosition.y);
        });
    }

    private void Update()
    {
        if (_timeRemain != null && _timeRemain >= Time.deltaTime)
        {
            _timeRemain -= Time.deltaTime;
            string timeStr = (TimeSpan.FromSeconds(_timeRemain.Value)).ToString(@"dd'd'hh'h'mm'm'ss's'");
            timeLeft.GetComponent<LocalizeStringEvent>().UpdateValue("time", VariableEnum.String, timeStr);
        }
        else
        {
            timeLeft.GetComponent<LocalizeStringEvent>().UpdateValue("time", VariableEnum.String, "time out");
        }
    }

    public void Init(KnockOutBoardData data, HistoryMatchResponse.HistoryMatch historyMatch)
    {

        try
        {
            _timeRemain = data.timeRemain;

            if (data.roomStatus == GameEnum.KORoomStatusEnum.MatchWin)
            {
                NextRoundBtn.gameObject.SetActive(true);

                NextRoundBtn.onClickWithHCSound(() =>
                {
                    if (historyMatch.game == GameEnum.MiniGameEnum.Bingo.GetStringValue())
                    {
                        UIBingo.InitNextGameStaticVariable(historyMatch.id.ToString());
                        SceneHelper.LoadScene(SceneEnum.Bingo);
                    }
                    else if (historyMatch.game == GameEnum.MiniGameEnum.Solitaire.GetStringValue())
                    {
                        UISolitaire.InitNextGameStaticVariable(historyMatch.id.ToString());
                        SceneHelper.LoadScene(SceneEnum.Solitaire);
                    }
                    else if (historyMatch.game == GameEnum.MiniGameEnum.Bubble.GetStringValue())
                    {
                        UIBubble.InitNextGameStaticVariable(historyMatch.id.ToString());
                        SceneHelper.LoadScene(SceneEnum.Bubble);
                    }

                });
            }
            else
            {
                NextRoundBtn.gameObject.SetActive(false);
            }

            ExitBtn.gameObject.SetActive(false);
            var matchNumberFocus = data.matchs
                .ToList()
                .Where(m =>
                {
                    bool isFound = false;
                    if (m.player_0 != null && m.player_0.Score >= 0)
                    {
                        isFound = m.player_0.Id.ToString() == Current.Ins.player.Id;
                    }

                    if (!isFound && m.player_1 != null && m.player_1.Score >= 0)
                    {
                        isFound = m.player_1.Id.ToString() == Current.Ins.player.Id;
                    }

                    return isFound;
                })
                .Select(m => m.gameNumber)
                .Max(x => x);
            for (int gameNumber = 0; gameNumber < matchs.Length; gameNumber++)
            {
                matchs[gameNumber].Init(getMatchData(data.matchs, gameNumber + 1), matchNumberFocus);
            }

            var matchMax = data.matchs
                .Where(m =>
                {
                    bool isFound = false;
                    if (m.player_0 != null)
                    {
                        isFound = m.player_0.Id.ToString() == Current.Ins.player.Id;
                    }

                    if (!isFound && m.player_1 != null)
                    {
                        isFound = m.player_1.Id.ToString() == Current.Ins.player.Id;
                    }

                    return isFound;
                })
                .Max(x => x.gameNumber);

            if (matchMax <= 8)
            {
                HeaderItems[0].GetComponent<KnockOutHeaderItem>().ShowActive();
            }
            else if (matchMax <= 12)
            {
                HeaderItems[1].GetComponent<KnockOutHeaderItem>().ShowActive();
            }
            else if (matchMax <= 14)
            {
                HeaderItems[2].GetComponent<KnockOutHeaderItem>().ShowActive();
            }
            else
            {
                HeaderItems[3].GetComponent<KnockOutHeaderItem>().ShowActive();
            }
            StartCoroutine(handleScroll(matchNumberFocus));
        }
        catch (Exception ex)
        {
            Debug.Log(ex.ToString());
        }
    }

    private KnockOutMatchData getMatchData(List<KnockOutMatchData> rounds, int gameNumber)
    {
        return rounds
            .Where(x => x.gameNumber == gameNumber)
            .FirstOrDefault();
    }
    private IEnumerator handleScroll(int matchNumberFocus)
    {
        yield return new WaitForSeconds(0.2f);
        scroll.GetComponent<ScrollRectEnsureVisible>()
               .CenterOnItem(matchs[matchNumberFocus - 1].GetComponent<RectTransform>());
    }

    private void OnEnable()
    {
        Current.Ins.Config.PropertyChanged += Config_PropertyChanged;
    }

    private void OnDisable()
    {
        Current.Ins.Config.PropertyChanged -= Config_PropertyChanged;
    }

    private void Config_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
    {
        if (e.PropertyName == StringConst.PROPERTY_LANGUAGE)
        {
            UIHistoryTab.Ins.IsResetView = true;
        }
    }
}