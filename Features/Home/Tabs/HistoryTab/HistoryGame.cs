using EnhancedUI.EnhancedScroller;
using RankTab;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HistoryGame : UIBase<HistoryGame>, IEnhancedScrollerDelegate
{
    public GameObject itemPrefab;
    public GameObject NotProgress;
    public EnhancedScroller scroller;
    //
    public Transform itemHolder;
    public GameObject claimBtn;
    public GameObject bgActive;
    public GameObject bgDisable;
    public GameObject inProgressBtn;
    public GameObject completeBtn;

    public GameObject iconTotalNotClaim;
    public GameObject textTotalNotClaim;

    public GameObject scrollBar;
    private bool _isLoading;
    private int _nextPage = 1;

    public int totalClaim = 0;
    public int totalRefund = 0;
    private HistoryMatchRequest historyMatchRequest = new HistoryMatchRequest();
    //
    [SerializeField] private List<HistoryMatchResponse.HistoryMatch> _data;
    protected override void InitIns() => Ins = this;

    private bool _isInitialized = false;

    public void Init()
    {
        if (_isInitialized) return;

        _isInitialized = true;

        historyMatchRequest.status = (int)GameEnum.StatusRoomEnum.InProgress;
        getHistoryMatch(historyMatchRequest);
    }

    void SetVerticalNormalizedPosition()
    {
        scroller.ClearAll();
        scroller.gameObject.GetComponent<ScrollRect>().verticalNormalizedPosition = 1f;
        scroller.ScrollPosition = 0;
    }

    // Start is called before the first frame update
    protected override void Start()
    {
        _data = new();
        scroller.Delegate = this;
        scrollBar.GetComponent<ScrollRect>().verticalNormalizedPosition = 1f;

        inProgressBtn.GetComponent<Toggle>().onValueChanged.AddListener((isChecked) =>
        {
            if (isChecked)
            {
                SoundManager.instance.PlayUIEffect("se_click1");
                //itemHolder.RemoveAllChild();
                _data.Clear();
                scrollBar.GetComponent<ScrollRect>().verticalNormalizedPosition = 1f;
                SetVerticalNormalizedPosition();
                historyMatchRequest.status = (int)GameEnum.StatusRoomEnum.InProgress;
                _nextPage = 1;
                getHistoryMatch(historyMatchRequest);
            }
        });

        completeBtn.GetComponent<Toggle>().onValueChanged.AddListener((isChecked) =>
        {
            if (isChecked)
            {
                SoundManager.instance.PlayUIEffect("se_click1");
                //itemHolder.RemoveAllChild();
                _data.Clear();
                scrollBar.GetComponent<ScrollRect>().verticalNormalizedPosition = 1f;
                SetVerticalNormalizedPosition();
                historyMatchRequest.status = (int)GameEnum.StatusRoomEnum.Complete;
                _nextPage = 1;
                getHistoryMatch(historyMatchRequest);
            }
        });

        scrollBar.GetComponent<ScrollRect>().onValueChanged.AddListener((vector) =>
        {
            if (!_isInitialized) return;
            if (_isLoading || _nextPage == 0) return;

            if (scrollBar.GetComponent<ScrollRect>().verticalNormalizedPosition <= 0.05f)
            {
                getHistoryMatch(historyMatchRequest);
            }
        });
    }

    public void SettingLanguage()
    {
        if (!_isInitialized) return;
        if (inProgressBtn.GetComponent<Toggle>().isOn)
        {
            SoundManager.instance.PlayUIEffect("se_click1");
            //itemHolder.RemoveAllChild();
            _data.Clear();
            scrollBar.GetComponent<ScrollRect>().verticalNormalizedPosition = 1f;
            historyMatchRequest.status = (int)GameEnum.StatusRoomEnum.InProgress;
            _nextPage = 1;
            getHistoryMatch(historyMatchRequest);
        }

        if (completeBtn.GetComponent<Toggle>().isOn)
        {
            SoundManager.instance.PlayUIEffect("se_click1");
            //itemHolder.RemoveAllChild();
            _data.Clear();
            scrollBar.GetComponent<ScrollRect>().verticalNormalizedPosition = 1f;
            historyMatchRequest.status = (int)GameEnum.StatusRoomEnum.Complete;
            _nextPage = 1;
            getHistoryMatch(historyMatchRequest);
        }
    }

    private void getHistoryMatch(HistoryMatchRequest historyMatchRequest)
    {
        NotProgress.SetActive(false);
        _isLoading = true;
        historyMatchRequest.limit = 10;
        historyMatchRequest.page = _nextPage;
        UIHome.Ins.ShowHideLoadingPopup(true);
        StartCoroutine(Current.Ins.historyAPI.GetHistoryMatch(request: historyMatchRequest, (historyMatchResponse) =>
        {
            UIHome.Ins.ShowHideLoadingPopup(false);
            if (historyMatchResponse == null || historyMatchResponse.errorCode != 0)
            {
                Debug.Log("Claim Error:");
                UIPopUpManager.instance.ShowErrorPopUp("Request Error : getHistoryMatch", historyMatchResponse, () => getHistoryMatch(historyMatchRequest));
                return;
            }

            List<HistoryMatchResponse.HistoryMatch> list = historyMatchResponse.data.datas.ToList();
            if (_nextPage == 1) _data.Clear();//itemHolder.RemoveAllChild();
            _isLoading = false;
            _nextPage = historyMatchResponse.data.nextPage;
            for (int i = 0; i < list.Count; i++)
            {
                //GameObject gameObject = Instantiate(itemPrefab, itemHolder, false);
                //gameObject.GetComponent<HistoryItemScript>().Init(list[i]);
                _data.Add(list[i]);
            }

            var scrollPosition = scroller.ScrollPosition;
            scroller.ReloadData();
            scroller.ScrollPosition = scrollPosition;

            var bottomMargin = 200;
            bool totalCheck = historyMatchResponse.data.totalNotClaimed >= 1 || historyMatchResponse.data.totalNotRefund >= 1;


            if (!totalCheck || historyMatchRequest.status == (int)GameEnum.StatusRoomEnum.InProgress)
            {
                bottomMargin = 20;
            }
            //
            if (_data.Count == 0)
            {
                NotProgress.SetActive(true);
            }
            //
            scrollBar.GetComponent<RectTransform>().offsetMin = new Vector2(scrollBar.GetComponent<RectTransform>().offsetMin.x, bottomMargin);

            claimBtn.SetActive(totalCheck && historyMatchRequest.status == (int)GameEnum.StatusRoomEnum.Complete);
            iconTotalNotClaim.SetActive(totalCheck);
            totalClaim = historyMatchResponse.data.totalNotClaimed;
            totalRefund = historyMatchResponse.data.totalNotRefund;

            if (totalCheck)
            {
                int total = totalClaim + totalRefund;

                textTotalNotClaim.GetComponent<TextMeshProUGUI>().text = total > 99 ? "99+" : total.ToString();

                claimBtn.GetComponent<Button>().onClick.RemoveAllListeners();
                claimBtn.GetComponent<Button>().onClickWithHCSound(() =>
                {
                    handleClaimAllReward();
                });

                UIHome.Ins.historyTabIcon.showDotNoti(total, true);
            }
            else
            {
                UIHome.Ins.historyTabIcon.showDotNoti(0, false);
                PlayerPrefsEx.Set(PlayerPrefsConst.NUMBER_HISTORY_EVENT, 0);
            }
        }));
    }

    private void handleClaimAllReward()
    {
        UIHome.Ins.ShowHideLoadingPopup(true);
        StartCoroutine(Current.Ins.historyAPI.claimAllReward<BaseResponse>((res) =>
        {
            claimBtn.SetActive(false);
            scrollBar.GetComponent<RectTransform>().offsetMin = new Vector2(scrollBar.GetComponent<RectTransform>().offsetMin.x, 20);
            UIHome.Ins.ShowHideLoadingPopup(false);

            if (res == null || res.errorCode != 0)
            {
                Debug.Log("Claim Error:");
                UIPopUpManager.instance.ShowErrorPopUp("Request Error : handleClaimAllReward", res, handleClaimAllReward);
                return;
            }

            Current.Ins.ReloadUserProfile(claimBtn.transform);
            UIHistoryTab.Ins.IsResetView = true;
            resetView();
        }));
    }

    public void resetView()
    {
        if (UIHistoryTab.Ins == null) return;
        if (!UIHistoryTab.Ins.IsResetView) return;
        UIHistoryTab.Ins.IsResetView = false;
        _nextPage = 1;
        historyMatchRequest.page = _nextPage;
        getHistoryMatch(historyMatchRequest);
    }

    private void OnEnable()
    {
        Current.Ins.Config.PropertyChanged += Config_PropertyChanged;
        resetView();
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        Current.Ins.Config.PropertyChanged -= Config_PropertyChanged;
    }

    private void Config_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
    {
        if (e.PropertyName == StringConst.PROPERTY_LANGUAGE)
        {
            SettingLanguage();
        }
    }

    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return _data.Count;
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        return 333f;
    }

    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        HistoryItemScript cellView = scroller.GetCellView(itemPrefab.GetComponent<HistoryItemScript>()) as HistoryItemScript;
        var dataItem = _data[dataIndex];
        cellView.Init(
            historyMatch: _data[dataIndex],
            claimCallBack: () =>
            {
                dataItem.isClaimed = 1;

                var scrollPosition = scroller.ScrollPosition;
                scroller.ReloadData();
                scroller.ScrollPosition = scrollPosition;

                int total = totalClaim + totalRefund;
                textTotalNotClaim.GetComponent<TextMeshProUGUI>().text = total > 99 ? "99+" : total.ToString();
                if (total > 0) UIHome.Ins.historyTabIcon.showDotNoti(total, true);
            },
            refundCallBack: () =>
            {
                dataItem.isRefund = 1;

                var scrollPosition = scroller.ScrollPosition;
                scroller.ReloadData();
                scroller.ScrollPosition = scrollPosition;

                int total = totalClaim + totalRefund;
                textTotalNotClaim.GetComponent<TextMeshProUGUI>().text = total > 99 ? "99+" : total.ToString();
                if (total > 0) UIHome.Ins.historyTabIcon.showDotNoti(total, true);
            });

        return cellView;
    }
}
