﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using DanielLochner.Assets.SimpleScrollSnap;
using EnhancedUI.EnhancedScroller;
using GameEnum;
using OutGameEnum;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HistoryItemScript : EnhancedScrollerCellView
{

    public Image gameBG;
    public GameObject statusTxt;
    public GameObject timeStatusTxt;
    public GameObject modeStatusTxt;
    public GameObject tokenDisable;

    public GameObject yourScoreMidleLayout;
    public GameObject yourScoreMidleTxt;
    public GameObject imageMidleLayout;
    public GameObject leftRightScoreLayout;
    public GameObject imageLeftLayout;
    public GameObject playingImgLeft;
    public GameObject searchOpponentImgLeft;
    public GameObject yourScoreTitle;
    public GameObject opponentScoreTitle;
    public GameObject yourScoreTxt;
    public GameObject opponentScoreTxt;
    public GameObject imageLayout;
    public GameObject playingImg;
    public GameObject searchOpponentImg;

    public GameObject titleRightTxt;
    public GameObject claimBtn;
    public GameObject bgActive;
    public GameObject bgDisable;
    public GameObject textButtonClaim;

    [Header("Reward 1, 2")]
    public GameObject contentReward;
    public GameObject coinImg;
    public GameObject tokenImg;
    public GameObject ticketImg;
    public GameObject feeTxtCoin;
    public GameObject feeTxtTicket;
    public GameObject feeTxtToken;

    [Header("Reward 3")]
    public GameObject reward_3;
    public GameObject feeTxtCoin_3;
    public GameObject feeTxtTicket_3;
    public GameObject feeTxtToken_3;

    [Header("Sprite")]
    public Sprite bingoSprite;
    public Sprite bubbleSprite;
    public Sprite solitaireSprite;

    public Sprite bingoSprite_d;
    public Sprite bubbleSprite_d;
    public Sprite solitaireSprite_d;

    public Sprite IconbingoSprite;
    public Sprite IconbubbleSprite;
    public Sprite IconsolitaireSprite;

    public Image GameResults;
    public Sprite resultVSSprite;
    public Sprite resultWinSprite;
    public Sprite resultLoseSprite;
    public Sprite resultDrawSprite;


    public Image TimerImg;
    public Sprite timerSpr, timerSpr_d;

    public Color color_yellow;

    private HistoryMatchResponse.HistoryMatch historyMatch;
    private int _statusBtn = (int)StatusButtonClaimEnum.Disable;

    private Action _claimCallBack;
    private Action _refundCallBack;



    private bool isTimeUpdate = false;
    private int createdAtTime = 0;


    private void Start()
    {
        InvokeRepeating("TimeUpdate", 0.0f, 10.0f);
    }

    private void TimeUpdate()
    {
        if (isTimeUpdate)
        {
            timeStatusTxt.GetComponent<TextMeshProUGUI>().text = Utils.DateTimeUtil.GetTimeAgo(createdAtTime);
        }
    }

    private void OnDestroy()
    {
        CancelInvoke();
    }

    public void Init(HistoryMatchResponse.HistoryMatch historyMatch, Action claimCallBack, Action refundCallBack)
    {
        _claimCallBack = claimCallBack;
        _refundCallBack = refundCallBack;

        LanguageEnum language = Current.Ins.Config.GetCurrentLanguage();
        this.historyMatch = historyMatch;
        string statusRoom = "";
        tokenDisable.SetActive(false);
        //switch (historyMatch.playStatus)
        //{
        //    //SearchingForOpponent
        //    case PlayStatusRoomEnum.SearchingForOpponent:
        //        switch (language)
        //        {
        //            case LanguageEnum.eng:
        //                statusRoom = "Waiting for Challenger";
        //                break;
        //            case LanguageEnum.kor:
        //                statusRoom = "상대를 기다리는 중";
        //                break;
        //            case LanguageEnum.vi:
        //                statusRoom = "Tìm kiếm đối thủ";
        //                break;
        //        }
        //        break;
        //    //WaitingForResult
        //    case PlayStatusRoomEnum.WaitingForResult:
        //        switch (language)
        //        {
        //            case LanguageEnum.eng:
        //                statusRoom = GameEnum.PlayStatusRoomEnum.WaitingForResult.GetStringValue();
        //                break;
        //            case LanguageEnum.kor:
        //                statusRoom = "결과 기다리는 중";
        //                break;
        //            case LanguageEnum.vi:
        //                statusRoom = "Đang chờ kết quả";
        //                break;
        //        }
        //        break;
        //    //Finish
        //    case PlayStatusRoomEnum.Finish:
        //        switch (language)
        //        {
        //            case LanguageEnum.eng:
        //                statusRoom = GameEnum.PlayStatusRoomEnum.Finish.GetStringValue();
        //                break;
        //            case LanguageEnum.kor:
        //                statusRoom = "마치다";
        //                break;
        //            case LanguageEnum.vi:
        //                statusRoom = "Hoàn thành";
        //                break;
        //        }
        //        break;
        //}
        string modeStatus = "";
        switch (historyMatch.mode)
        {
            case (int)GameEnum.MiniGameModeEnum.HeadToHead:
                //modeStatus = GameEnum.MiniGameModeEnum.HeadToHead.GetStringValue();
                modeStatus = "1 vs 1";
                setupLayoutScore(false);
                break;
            case (int)GameEnum.MiniGameModeEnum.OneToMany:
                switch (language)
                {
                    case LanguageEnum.eng:
                        modeStatus = "Tournament";
                        break;
                    case LanguageEnum.kor:
                        modeStatus = "토너먼트";
                        break;
                    case LanguageEnum.vi:
                        modeStatus = "Giải đấu";
                        break;
                }
                setupLayoutScore(true);
                break;
            case (int)GameEnum.MiniGameModeEnum.KnockOut:
                modeStatus = GameEnum.MiniGameModeEnum.KnockOut.GetStringValue();
                setupLayoutScore(false);
                break;
        }

        if (historyMatch.game == GameEnum.MiniGameEnum.Bingo.GetStringValue())
        {
            gameBG.sprite = bingoSprite;
        }
        else if (historyMatch.game == GameEnum.MiniGameEnum.Bubble.GetStringValue())
        {
            gameBG.sprite = bubbleSprite;
        }
        else if (historyMatch.game == GameEnum.MiniGameEnum.Solitaire.GetStringValue())
        {
            gameBG.sprite = solitaireSprite;
        }

        //if (gameBG.material != null)
        //    gameBG.material.SetVector("_HSLAAdjust", new Vector4(0, 0, 0, 0));
        isTimeUpdate = false;

        statusTxt.GetComponent<TextMeshProUGUI>().text = statusRoom;
        var myScore = historyMatch.games[0].scores.Find((obj) => obj.user.id.ToString() == Current.Ins.player.Id);
        var yourScore = historyMatch.games[0].scores.Find((obj) => obj.user.id.ToString() != Current.Ins.player.Id);
        if (myScore != null && myScore.createdAt != 0)
        {
            createdAtTime = myScore.createdAt;
            isTimeUpdate = true;
        }
        else if (yourScore != null && yourScore.createdAt != 0)
        {
            createdAtTime = yourScore.createdAt;
            isTimeUpdate = true;
        }
        else
        {
            createdAtTime = historyMatch.createdAt;
            isTimeUpdate = true;
        }

        if (isTimeUpdate && timeStatusTxt)
        {
            timeStatusTxt.GetComponent<TextMeshProUGUI>().text = Utils.DateTimeUtil.GetTimeAgo(createdAtTime);
        }

        modeStatusTxt.GetComponent<TextMeshProUGUI>().text = modeStatus;

        TimerImg.sprite = timerSpr;
        statusTxt.GetComponent<TextMeshProUGUI>().color = Color.white;

        yourScoreTitle.GetComponent<TextMeshProUGUI>().color = Color.white;
        opponentScoreTitle.GetComponent<TextMeshProUGUI>().color = Color.white;

        yourScoreTxt.GetComponent<TextMeshProUGUI>().color = color_yellow;
        opponentScoreTxt.GetComponent<TextMeshProUGUI>().color = color_yellow;

        setupRightLayout();
        setupAction(historyMatch);
    }






    private void setupAction(HistoryMatchResponse.HistoryMatch historyMatch)
    {
        claimBtn.GetComponent<Button>().onClick.RemoveAllListeners();
        claimBtn.GetComponent<Button>().onClickWithHCSound(() =>
        {
            if (bgDisable.activeSelf) return;

            switch (_statusBtn)
            {
                case (int)StatusButtonClaimEnum.Refund:
                    handleRefundPrize();
                    break;
                case (int)StatusButtonClaimEnum.GoToDetail:
                    goToDetailScreen();
                    break;
                case (int)StatusButtonClaimEnum.TryAgain:
                    goToMatchingGame(historyMatch);
                    break;
                case (int)StatusButtonClaimEnum.Claim:
                    handleClaimPrize();
                    break;
            }
        });

        gameObject.transform.GetComponent<Button>().onClick.RemoveAllListeners();
        gameObject.transform.GetComponent<Button>().onClickWithHCSound(() =>
        {
            switch (historyMatch.mode)
            {
                case (int)GameEnum.MiniGameModeEnum.OneToMany:
                    Load1vsManyData(historyMatch);
                    break;
                case (int)GameEnum.MiniGameModeEnum.HeadToHead:
                    goToDetailScreen();
                    break;
                case (int)GameEnum.MiniGameModeEnum.KnockOut:
                    goToKnockOutResult();
                    break;
            }
        });
    }

    private void Load1vsManyData(HistoryMatchResponse.HistoryMatch historyMatch)
    {
        UIHome.Ins.ShowHideLoadingPopup(true);
        StartCoroutine(Current.Ins.gameAPI.GetRoomTournament<RoomTournamentResponse>(roomId: historyMatch.id.ToString(), (response) =>
        {
            UIHome.Ins.ShowHideLoadingPopup(false);
            if (response == null || response.errorCode != 0)
            {
                UIPopUpManager.instance.ShowErrorPopUp("Request Error : Load1vsManyData", response, () => Load1vsManyData(historyMatch));
                return;
            }

            if (historyMatch.status == StatusRoomEnum.Complete)
            {
                void GetRoomResult(string roomId)
                {
                    UIHome.Ins.ShowHideLoadingPopup(true);
                    StartCoroutine(Current.Ins.historyAPI.GetTournamentRoomResult(roomId, (roomres) =>
                    {
                        UIHome.Ins.ShowHideLoadingPopup(false);
                        if (roomres == null || roomres.errorCode != 0)
                        {
                            UIPopUpManager.instance.ShowErrorPopUp("Request Error : Load1vsManyData", roomres, () => GetRoomResult(roomId));
                            return;
                        }

                        UIHistoryTab.Ins.oneVsManyLayout = Instantiate(UIHistoryTab.Ins.oneVsManyPrefab, UIHistoryTab.Ins.transform, false);
                        UIHistoryTab.Ins.oneVsManyLayout.GetComponent<HistoryTab.TournamentRoomPending>().Init(historyMatch, response, roomres);
                        UIHistoryTab.Ins.changeLayout(HistoryLayoutEnum.TournamentPending);
                    }));
                }

                GetRoomResult(historyMatch.id.ToString());
            }
            else
            {
                UIHistoryTab.Ins.oneVsManyLayout = Instantiate(UIHistoryTab.Ins.oneVsManyPrefab, UIHistoryTab.Ins.transform, false);
                UIHistoryTab.Ins.oneVsManyLayout.GetComponent<HistoryTab.TournamentRoomPending>().Init(historyMatch, response);
                UIHistoryTab.Ins.changeLayout(HistoryLayoutEnum.TournamentPending);
            }
        }));
    }

    private void goToKnockOutResult()
    {
        KORoomStatusEnum kORoomStatus = KORoomStatusEnum.Waiting;
        if (historyMatch.playStatus == PlayStatusRoomEnum.WaitingForResult)
        {
            kORoomStatus = KORoomStatusEnum.Waiting;
        }
        if (historyMatch.playStatus == PlayStatusRoomEnum.Finish && historyMatch.status == StatusRoomEnum.InProgress)
        {
            var indexMyUser = historyMatch.games[0].scores
           .FindIndex(x => x.user.id.ToString() == Current.Ins.player.Id);
            if (indexMyUser == 0)
            {
                kORoomStatus = KORoomStatusEnum.MatchWin;
            }
            else
            {
                kORoomStatus = KORoomStatusEnum.MatchLoss;
            }
        }
        if (historyMatch.playStatus == PlayStatusRoomEnum.Finish && historyMatch.status == StatusRoomEnum.InProgress)
        {
            kORoomStatus = KORoomStatusEnum.Champion;
        }
        StartCoroutine(Current.Ins.historyAPI.GetHistoryMatchDetail(roomId: historyMatch.id, (historyMatchResponse) =>
        {
            UIHome.Ins.ShowHideLoadingPopup(false);
            if (historyMatchResponse == null || historyMatchResponse.errorCode != 0)
            {
                Debug.Log("Claim Error:");
                UIPopUpManager.instance.ShowErrorPopUp("Request Error : goToKnockOutResult", historyMatchResponse, goToKnockOutResult);
                return;
            }
            onCallBackGetRoomTournament(historyMatchResponse.data, kORoomStatus);
        }));
    }

    private void onCallBackGetRoomTournament(HistoryMatchResponse.HistoryMatch response, KORoomStatusEnum KORoomStatus)
    {
        var boardData = new KnockOutBoardData();
        boardData.matchs = new List<KnockOutMatchData>();
        boardData.roomStatus = KORoomStatus;
        boardData.timeRemain = response.timeRefund;

        foreach (var match in response.games)
        {
            KnockOutMatchData matchData = new KnockOutMatchData();
            matchData.gameNumber = match.gameNumber;

            var player0 = match.scores.ElementAtOrDefault(0);
            var player1 = match.scores.ElementAtOrDefault(1);

            if (player0 != null)
            {
                matchData.player_0 = new KnockOutPlayerData();
                matchData.player_0.Id = player0.user.id;
                matchData.player_0.Name = player0.user.name;
                matchData.player_0.Avatar = player0.user.avatar;
                matchData.player_0.Score = player0.score;
            }

            if (player1 != null)
            {
                matchData.player_1 = new KnockOutPlayerData();
                matchData.player_1.Id = player1.user.id;
                matchData.player_1.Name = player1.user.name;
                matchData.player_1.Avatar = player1.user.avatar;
                matchData.player_1.Score = player1.score;
            }

            boardData.matchs.Add(matchData);
        }

        UIHistoryTab.Ins.goToBoardResult(boardData, response);
    }

    private void goToDetailScreen()
    {
        UIHistoryTab.Ins.changeLayout(HistoryLayoutEnum.DetailScreen);
        Sprite iconGame = null;
        if (historyMatch.game == GameEnum.MiniGameEnum.Bingo.GetStringValue())
        {
            iconGame = this.IconbingoSprite;
        }
        else if (historyMatch.game == GameEnum.MiniGameEnum.Bubble.GetStringValue())
        {
            iconGame = this.IconbubbleSprite;
        }
        else if (historyMatch.game == GameEnum.MiniGameEnum.Solitaire.GetStringValue())
        {
            iconGame = this.IconsolitaireSprite;
        }

        WinLose.Ins.Init(historyMatch, iconGame);
    }

    private void handleClaimPrize()
    {
        String roomId = historyMatch.id.ToString();

        UIHome.Ins.ShowHideLoadingPopup(true);
        StartCoroutine(Current.Ins.gameAPI.ClaimReward<ClaimRewardResponse>(roomId, (res) =>
        {
            UIHome.Ins.ShowHideLoadingPopup(false);
            if (res == null || res.errorCode != 0)
            {
                Debug.Log("Claim Error:");
                UIPopUpManager.instance.ShowErrorPopUp("Request Error : handleClaimPrize", res, handleClaimPrize);
                return;
            }

            Current.Ins.ReloadUserProfile(claimBtn.transform);
            UIHistoryTab.Ins.IsResetView = true;

            var total = HistoryGame.Ins.totalClaim + HistoryGame.Ins.totalRefund;
            if (total <= 1)
            {
                HistoryGame.Ins.claimBtn.SetActive(false);
                UIHistoryTab.Ins.IsResetView = true;
                HistoryGame.Ins.resetView();
            }
            else
            {
                HistoryGame.Ins.totalClaim--;
                _claimCallBack.Invoke();
            }

        }));
    }

    private void goToMatchingGame(HistoryMatchResponse.HistoryMatch historyMatch)
    {
        if (historyMatch.mode == (int)GameEnum.MiniGameModeEnum.OneToMany)
        {
            UIHome.Ins.scrollSnapView.GoToPanel(1);
        }
        else
        {
            UIHome.Ins.scrollSnapView.GoToPanel(0);
            if (historyMatch.game == GameEnum.MiniGameEnum.Bingo.GetStringValue())
            {
                HomeTab.UIHomeTab.Ins.OpenGameMode(MiniGameEnum.Bingo);
            }
            else if (historyMatch.game == GameEnum.MiniGameEnum.Solitaire.GetStringValue())
            {
                HomeTab.UIHomeTab.Ins.OpenGameMode(MiniGameEnum.Solitaire);
            }
            else if (historyMatch.game == GameEnum.MiniGameEnum.Bubble.GetStringValue())
            {
                HomeTab.UIHomeTab.Ins.OpenGameMode(MiniGameEnum.Bubble);
            }
        }
    }

    private void handleRefundPrize()
    {
        String roomId = historyMatch.id.ToString();

        UIHome.Ins.ShowHideLoadingPopup(true);
        StartCoroutine(Current.Ins.historyAPI.refundRoomFee<BaseResponse>(roomId, (res) =>
        {
            UIHome.Ins.ShowHideLoadingPopup(false);
            if (res == null || res.errorCode != 0)
            {
                Debug.Log("Claim Error:");
                UIPopUpManager.instance.ShowErrorPopUp("Request Error : handleRefundPrize", res, handleRefundPrize);
                return;
            }

            Current.Ins.ReloadUserProfile(claimBtn.transform);

            var total = HistoryGame.Ins.totalRefund + HistoryGame.Ins.totalClaim;
            if (total <= 1)
            {
                HistoryGame.Ins.claimBtn.SetActive(false);
                UIHistoryTab.Ins.IsResetView = true;
                HistoryGame.Ins.resetView();
            }else
            {
                HistoryGame.Ins.totalRefund--;
                _refundCallBack.Invoke();
            }
        }));
    }

    private void setupRightLayout()
    {
        String titleRight = "";
        //language
        LanguageEnum language = Current.Ins.Config.GetCurrentLanguage();
        claimBtn.SetActive(true);
        //
        reward_3.SetActive(false);
        contentReward.SetActive(true);

        tokenImg.SetActive(false);
        ticketImg.SetActive(false);
        coinImg.SetActive(false);

        titleRightTxt.SetActive(true);
        switch (language)
        {
            case LanguageEnum.eng:
                titleRight = "Prize";
                break;
            case LanguageEnum.kor:
                titleRight = "보상";
                break;
            case LanguageEnum.vi:
                titleRight = "Phần thưởng";
                break;
        }

        if (historyMatch.yourReward.token > 0
            || historyMatch.yourReward.gold > 0
            || historyMatch.yourReward.ticket > 0)
        {
            if (historyMatch.yourReward.token > 0
                && historyMatch.yourReward.gold > 0
                && historyMatch.yourReward.ticket > 0)
            {
                reward_3.SetActive(true);
                contentReward.SetActive(false);

                feeTxtTicket_3.GetComponent<TextMeshProUGUI>().text = formatNumber(historyMatch.yourReward.ticket);
                feeTxtToken_3.GetComponent<TextMeshProUGUI>().text = formatNumber(historyMatch.yourReward.token);
                feeTxtCoin_3.GetComponent<TextMeshProUGUI>().text = formatNumber(historyMatch.yourReward.gold);
            }
            else
            {
                if (historyMatch.yourReward.token > 0) tokenImg.SetActive(true);
                if (historyMatch.yourReward.gold > 0) coinImg.SetActive(true);
                if (historyMatch.yourReward.ticket > 0) ticketImg.SetActive(true);

                feeTxtTicket.GetComponent<TextMeshProUGUI>().text = formatNumber(historyMatch.yourReward.ticket);
                feeTxtToken.GetComponent<TextMeshProUGUI>().text = formatNumber(historyMatch.yourReward.token);
                feeTxtCoin.GetComponent<TextMeshProUGUI>().text = formatNumber(historyMatch.yourReward.gold);
            }
        }
        else
        {
            switch (language)
            {
                case LanguageEnum.eng:
                    titleRight = "Entry Fee";
                    break;
                case LanguageEnum.kor:
                    titleRight = "입장료";
                    break;
                case LanguageEnum.vi:
                    titleRight = "Phí cược";
                    break;
            }
            if (historyMatch.miniGame.tokenFee > 0
            || historyMatch.miniGame.goldFee > 0
            || historyMatch.miniGame.ticketFee > 0)
            {
                if (historyMatch.miniGame.tokenFee > 0) tokenImg.SetActive(true);
                if (historyMatch.miniGame.goldFee > 0) coinImg.SetActive(true);
                if (historyMatch.miniGame.ticketFee > 0) ticketImg.SetActive(true);

                feeTxtTicket.GetComponent<TextMeshProUGUI>().text = formatNumber(historyMatch.miniGame.ticketFee);
                feeTxtToken.GetComponent<TextMeshProUGUI>().text = formatNumber(historyMatch.miniGame.tokenFee);
                feeTxtCoin.GetComponent<TextMeshProUGUI>().text = formatNumber(historyMatch.miniGame.goldFee);
            }
            //
            if (historyMatch.playStatus == PlayStatusRoomEnum.Finish && historyMatch.remainMember <= 0)
            {
                tokenImg.SetActive(false);
                coinImg.SetActive(false);
                ticketImg.SetActive(false);
                titleRightTxt.SetActive(false);
                claimBtn.SetActive(false);
            }
        }

        titleRightTxt.GetComponent<TextMeshProUGUI>().text = titleRight;
        //feeTxt.GetComponent<TextMeshProUGUI>().text = feeValue.ToString();

        switch (historyMatch.playStatus)
        {
            case PlayStatusRoomEnum.SearchingForOpponent:
                bgActive.SetActive(false);
                bgDisable.SetActive(true);
                textButtonClaim.SetActive(true);
                if (historyMatch.timeRefund == 0)
                {

                    switch (language)
                    {
                        case LanguageEnum.eng:
                            textButtonClaim.GetComponent<TextMeshProUGUI>().text = "Refund";
                            break;
                        case LanguageEnum.kor:
                            textButtonClaim.GetComponent<TextMeshProUGUI>().text = "환불하다";
                            break;
                        case LanguageEnum.vi:
                            textButtonClaim.GetComponent<TextMeshProUGUI>().text = "Hoàn tiền";
                            break;
                    }
                    _statusBtn = (int)StatusButtonClaimEnum.Refund;
                    bgActive.SetActive(historyMatch.isRefund == 0);
                    bgDisable.SetActive(historyMatch.isRefund != 0);
                    claimBtn.GetComponent<Button>().interactable = false;
                }
                else
                {
                    textButtonClaim.GetComponent<TextMeshProUGUI>().text = Utils.DateTimeUtil.GetRefundTime(historyMatch.timeRefund);
                }
                break;
            case PlayStatusRoomEnum.WaitingForResult:


                //bgActive.SetActive(false);
                //bgDisable.SetActive(true);
                //textButtonClaim.SetActive(false);
                if (historyMatch.mode == (int)GameEnum.MiniGameModeEnum.KnockOut)
                {
                    claimBtn.SetActive(true);
                    _statusBtn = (int)StatusButtonClaimEnum.GoToDetail;
                    bgActive.SetActive(true);
                    bgDisable.SetActive(false);
                }
                else
                {
                    claimBtn.SetActive(false);
                }

                break;
            case PlayStatusRoomEnum.Finish:
                setupButtonClaim();
                break;
        }

    }

    void SetLanguageButtonClaim(string txtButtonClaim, string txtTitleRight, string txtStatus)
    {
        textButtonClaim.GetComponent<TextMeshProUGUI>().text = txtButtonClaim;
        //titleRightTxt.GetComponent<TextMeshProUGUI>().text = txtTitleRight;
        statusTxt.GetComponent<TextMeshProUGUI>().text = txtStatus;
    }

    private void setupButtonClaim()
    {
        LanguageEnum language = Current.Ins.Config.GetCurrentLanguage();

        if (historyMatch.remainMember > 0)
        {
            bgActive.SetActive(false);
            bgDisable.SetActive(true);
            textButtonClaim.SetActive(true);
            if (historyMatch.timeRefund == 0)
            {
                switch (language)
                {
                    case LanguageEnum.eng:
                        textButtonClaim.GetComponent<TextMeshProUGUI>().text = "Refund";
                        break;
                    case LanguageEnum.kor:
                        textButtonClaim.GetComponent<TextMeshProUGUI>().text = "환불하다";
                        break;
                    case LanguageEnum.vi:
                        textButtonClaim.GetComponent<TextMeshProUGUI>().text = "Hoàn tiền";
                        break;
                }

                GameResults.sprite = resultVSSprite;
                _statusBtn = (int)StatusButtonClaimEnum.Refund;
                bgActive.SetActive(historyMatch.isRefund == 0);
                bgDisable.SetActive(historyMatch.isRefund != 0);
            }

            return;
        }

        if (historyMatch.games.Count == 0) return;
        if (historyMatch.games[0].scores.Count < 2) return;

        bool isActive = false;
        if (historyMatch.yourReward.gold > 0
            || historyMatch.yourReward.token > 0
            || historyMatch.yourReward.ticket > 0)
        {
            isActive = historyMatch.isClaimed == 0;
        }

        bgActive.SetActive(isActive);
        bgDisable.SetActive(!isActive);

        switch (historyMatch.mode)
        {
            case (int)GameEnum.MiniGameModeEnum.HeadToHead:
                bool isYouWin = historyMatch.games[0].scores[0].user.id.ToString() == Current.Ins.player.Id;

                textButtonClaim.SetActive(true);
                
                string winLoseTextEL = isYouWin ? "You won" : "You lose";
                string winLoseTextKR = isYouWin ? "승리" : "패배";
                string winLoseTextVN = isYouWin ? "Bạn đã thắng" : "Bạn đã thua";

                GameResults.sprite = isYouWin ? resultWinSprite : resultLoseSprite;

                if (historyMatch.games[0].isDraw)
                {
                    winLoseTextEL = "You Draw";
                    winLoseTextKR = "무승부 ";
                    winLoseTextVN = "Ván hòa";
                    GameResults.sprite = resultDrawSprite;
                }

                switch (language)
                {
                    case LanguageEnum.eng:
                        SetLanguageButtonClaim("Claim", "Prize", winLoseTextEL);
                        break;
                    case LanguageEnum.kor:
                        SetLanguageButtonClaim("수령", "상", winLoseTextKR);
                        break;
                    case LanguageEnum.vi:
                        SetLanguageButtonClaim("Nhận", "Phần thưởng", winLoseTextVN);
                        break;
                }

                if (isYouWin == false && !historyMatch.games[0].isDraw)
                {
                    if (historyMatch.game == GameEnum.MiniGameEnum.Bingo.GetStringValue())
                    {
                        gameBG.sprite = bingoSprite_d;
                    }
                    else if (historyMatch.game == GameEnum.MiniGameEnum.Bubble.GetStringValue())
                    {
                        gameBG.sprite = bubbleSprite_d;
                    }
                    else if (historyMatch.game == GameEnum.MiniGameEnum.Solitaire.GetStringValue())
                    {
                        gameBG.sprite = solitaireSprite_d;
                    }

                    bool checkReward = historyMatch.yourReward.token > 0 || historyMatch.yourReward.gold > 0 || historyMatch.yourReward.ticket > 0;
                    if (!checkReward)
                    {
                        string titleRight = "";
                        titleRightTxt.SetActive(true);
                        switch (language)
                        {
                            case LanguageEnum.eng:
                                titleRight = "Prize";
                                break;
                            case LanguageEnum.kor:
                                titleRight = "보상";
                                break;
                            case LanguageEnum.vi:
                                titleRight = "Phần thưởng";
                                break;
                        }
                        titleRightTxt.GetComponent<TextMeshProUGUI>().text = titleRight;

                        tokenDisable.SetActive(true);
                    }

                }

                if (!historyMatch.games[0].isDraw)
                {
                    TimerImg.sprite = isYouWin ? timerSpr : timerSpr_d;
                    statusTxt.GetComponent<TextMeshProUGUI>().color = isYouWin ? Color.white : Color.gray;

                    yourScoreTitle.GetComponent<TextMeshProUGUI>().color = isYouWin ? Color.white : Color.gray;
                    opponentScoreTitle.GetComponent<TextMeshProUGUI>().color = isYouWin ? Color.white : Color.gray;

                    yourScoreTxt.GetComponent<TextMeshProUGUI>().color = isYouWin ? color_yellow : Color.gray;
                    opponentScoreTxt.GetComponent<TextMeshProUGUI>().color = isYouWin ? color_yellow : Color.gray;
                }
                

                //if (isYouWin == false)
                //{
                //    if (gameBG.material != null)
                //        gameBG.material.SetVector("_HSLAAdjust", new Vector4(0, -1, 0, 0));
                //}

                _statusBtn = (int)StatusButtonClaimEnum.Claim;

                break;
            case (int)GameEnum.MiniGameModeEnum.OneToMany:
                //bgActive.SetActive(historyMatch.isClaimed == 0 && historyMatch.yourRank < 4);
                //bgDisable.SetActive(historyMatch.isClaimed != 0 || historyMatch.yourRank > 3);
                textButtonClaim.SetActive(true);
                switch (language)
                {
                    case LanguageEnum.eng:
                        SetLanguageButtonClaim("Claim", "Prize", "You finished in Top" + " " + historyMatch.yourRank);
                        break;
                    case LanguageEnum.kor:
                        SetLanguageButtonClaim("수령", "상", "탑 " + historyMatch.yourRank + " 위로 마쳤습니다");
                        break;
                    case LanguageEnum.vi:
                        SetLanguageButtonClaim("Nhận", "Phần thưởng", "Bạn đã hoàn thành trong top " + historyMatch.yourRank);
                        break;
                }
                _statusBtn = (int)StatusButtonClaimEnum.Claim;
                break;
            case (int)GameEnum.MiniGameModeEnum.KnockOut:
                //bgActive.SetActive(historyMatch.isClaimed == 0 && historyMatch.yourRank < 4);
                //bgDisable.SetActive(historyMatch.isClaimed != 0 || historyMatch.yourRank > 3);
                textButtonClaim.SetActive(true);
                switch (language)
                {
                    case LanguageEnum.eng:
                        SetLanguageButtonClaim("Claim", "Prize", "You finished in Top" + " " + historyMatch.yourRank);
                        break;
                    case LanguageEnum.kor:
                        SetLanguageButtonClaim("수령", "상", "탑 " + historyMatch.yourRank + " 위로 마쳤습니다");
                        break;
                    case LanguageEnum.vi:
                        SetLanguageButtonClaim("Nhận", "Phần thưởng", "Bạn đã hoàn thành trong top " + historyMatch.yourRank);
                        break;
                }
                _statusBtn = (int)StatusButtonClaimEnum.Claim;
                break;
        }
    }

    private void setupLayoutScore(bool isModeOneToMany)
    {
        yourScoreMidleLayout.SetActive(isModeOneToMany);
        leftRightScoreLayout.SetActive(!isModeOneToMany);
        if (historyMatch.games.Count == 0) return;
        if (isModeOneToMany)
        {
            if (historyMatch.games[0].scores.Count == 0) return;

            var myScore = historyMatch.games[0].scores.FirstOrDefault(x => x.user.id.ToString() == Current.Ins.player.Id);

            if (myScore == null || myScore.score == -1)
            {
                imageMidleLayout.SetActive(true);
                yourScoreMidleTxt.SetActive(false);
            }
            else
            {
                imageMidleLayout.SetActive(false);
                yourScoreMidleTxt.SetActive(true);
                yourScoreMidleTxt.GetComponent<TextMeshProUGUI>().text = myScore.score.ToString();
            }
        }
        else
        {
            var listScore = historyMatch.games[0].scores;
            RoomTournamentResponse.Score myScore = listScore.Find((obj) => obj.user.id.ToString() == Current.Ins.player.Id);
            RoomTournamentResponse.Score yourScore = listScore.Find((obj) => obj.user.id.ToString() != Current.Ins.player.Id);
            if (myScore == null)
            {
                imageLeftLayout.SetActive(true);
                searchOpponentImgLeft.SetActive(true);
                playingImgLeft.SetActive(false);
                yourScoreTxt.SetActive(false);
            }
            else
            {
                imageLeftLayout.SetActive(myScore.score == -1);
                if (myScore.score == -1)
                {
                    searchOpponentImgLeft.SetActive(false);
                    playingImgLeft.SetActive(true);
                }
                yourScoreTxt.SetActive(myScore.score != -1);
                yourScoreTxt.GetComponent<TextMeshProUGUI>().text = myScore.score.ToString();
            }
            if (yourScore == null)
            {
                imageLayout.SetActive(true);
                opponentScoreTxt.SetActive(false);
                bool isPLaying = historyMatch.playStatus == PlayStatusRoomEnum.WaitingForResult;
                playingImg.SetActive(isPLaying);
                searchOpponentImg.SetActive(!isPLaying);
            }
            else
            {
                imageLayout.SetActive(yourScore.score == -1);
                playingImg.SetActive(yourScore.score == -1);
                searchOpponentImg.SetActive(yourScore.score != -1);
                opponentScoreTxt.SetActive(yourScore.score != -1);
                opponentScoreTxt.GetComponent<TextMeshProUGUI>().text = yourScore.score.ToString();
            }
        }
    }

    public string formatNumber(int quantity)
    {
        return quantity.ToString();


        //if (quantity < 1000) return quantity.ToString();

        //if (quantity % 1000 == 0) return quantity / 1000 + "K";

        //return quantity / 1000 + "K" + (quantity % 1000) / 100;
    }

    public enum StatusButtonClaimEnum
    {
        Disable = 0,
        Refund,
        GoToDetail,
        TryAgain,
        Claim
    }
}
