using System.Collections;
using System.Collections.Generic;
using OutGameEnum;
using UnityEngine;

public class UIHistoryTab : UIBase<UIHistoryTab>
{
    public GameObject historyLayout;
    public GameObject winLoseLayout;
    private GameObject boardResult;
    public GameObject boardResultPrefab;
    public GameObject oneVsManyPrefab;
    [HideInInspector]
    public GameObject oneVsManyLayout;
    protected override void InitIns() => Ins = this;

    [HideInInspector]
    public bool IsResetView = false;

    //private void Start()
    //{
    //    changeLayout(HistoryLayoutEnum.History);
    //}
    private void OnEnable()
    {
        ResetLayout();
    }
    public void ResetLayout()
    {
        changeLayout(HistoryLayoutEnum.History);
    }
    public void changeLayout(HistoryLayoutEnum indexLayout)
    {
        historyLayout.SetActive(indexLayout == HistoryLayoutEnum.History);
        winLoseLayout.SetActive(indexLayout == HistoryLayoutEnum.DetailScreen);

        if (indexLayout != HistoryLayoutEnum.TournamentPending)
        {
            if (oneVsManyLayout != null) Destroy(oneVsManyLayout);
        }
        
        if (indexLayout == HistoryLayoutEnum.BoardResult)
        {
            boardResult = Instantiate(boardResultPrefab, gameObject.transform, false);
        }
        else
        {
            if (boardResult != null) Destroy(boardResult);
        }
    }
    public void goToBoardResult(KnockOutBoardData boardData, HistoryMatchResponse.HistoryMatch historyMatch)
    {
        changeLayout(HistoryLayoutEnum.BoardResult);
        boardResult.GetComponent<HistoryKnockOutResult>().Init(boardData, historyMatch);
        //UIHome.Ins.historyTabIcon.GetComponent<TabItem>().OnClickIcon(() =>
        //{
        //    changeLayout(HistoryLayoutEnum.History);
        //});
    }

}

public enum HistoryLayoutEnum
{
    History = 0,
    TournamentPending,
    TourmentResutl,
    DetailScreen,
    BoardResult,
}