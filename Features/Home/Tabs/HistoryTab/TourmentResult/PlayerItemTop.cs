using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

using OutGameEnum;

namespace HistoryTab
{
    public class PlayerItemTop : MonoBehaviour
    {
        public GameObject order;
        public GameObject playerName;
        public GameObject playerAvt;
        public GameObject playerScore;

        public GameObject GoldIcon;
        public GameObject TokenIcon;

        public GameObject backgroundYour;

        public void Init(RankTab.PlayerItemData itemData)
        {
            order.GetComponent<TextMeshProUGUI>().text = itemData.Order.ToString();

            if (itemData.isSearching) return;

            playerName.GetComponent<TextMeshProUGUI>().text = itemData.PlayerName;

            if (itemData.RankType == UserRankingTypeEnum.Gold)
            {
                GoldIcon.SetActive(true);
                TokenIcon.SetActive(false);
                playerScore.GetComponent<TextMeshProUGUI>().text = StringHelper.NumberFormatter(itemData.Gold);
            }
            else
            {
                GoldIcon.SetActive(false);
                TokenIcon.SetActive(true);
                playerScore.GetComponent<TextMeshProUGUI>().text = StringHelper.NumberFormatter(itemData.Token);
            }

            if (backgroundYour != null)
            {
                backgroundYour.SetActive(itemData.PlayerId == Current.Ins.player.Id);
            }
        }
    }
}