﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using GameEnum;
using System.Linq;
using DanielLochner.Assets.SimpleScrollSnap;
using EnhancedUI.EnhancedScroller;

namespace HistoryTab
{
    public class TournamentRoomPending : MonoBehaviour, IEnhancedScrollerDelegate
    {
        public GameObject first;
        public GameObject second;
        public GameObject third;

        public GameObject scrollBar;
        public EnhancedScroller scroller;
        public GameObject itemPrefab;

        public GameObject newMatchBtn;
        public GameObject newMatchTxtBtn;
        public GameObject claimTxtBtn;
        public GameObject bgActive;
        public GameObject bgDisable;

        public GameObject closeBtn;

        [SerializeField] private List<MatchOneToManyPlayerItemData> _data;
        private void Start()
        {
            scroller.Delegate = this;
            closeBtn.GetComponent<ButtonEx>().onClickWithHCSound(() => UIHistoryTab.Ins.ResetLayout());
        }

        public void Init(HistoryMatchResponse.HistoryMatch historyMatch, RoomTournamentResponse response, RoomTournamentResultResponse roomFinishRes = null)
        {
            _data = new List<MatchOneToManyPlayerItemData>();
            LoadTournamentRoom(historyMatch, response, roomFinishRes);
        }

        private void LoadTournamentRoom(HistoryMatchResponse.HistoryMatch historyMatch, RoomTournamentResponse response, RoomTournamentResultResponse roomFinishRes = null)
        {
            // ReMap Rank
            if (roomFinishRes != null)
            {
                foreach (var user in roomFinishRes.data)
                {
                    var scuser = response.data.datas[0].scores.First(x => x.user.id == user.user.id);
                    scuser.rank = user.rank;
                }
            }

            var userScores = response.data.datas[0].scores.OrderByDescending(x => x.score).ToList();

            var firstUser = userScores.ElementAtOrDefault(0);
            var firstRank = firstUser != null && firstUser.rank != -1 ? firstUser.rank : 1;
            first.GetComponent<MatchOneToManyPlayerItem>()
                    .Init(getItemData(firstUser, firstRank));

            var secondUser = userScores.ElementAtOrDefault(1);
            var secondRank = secondUser != null && secondUser.rank != -1 ? secondUser.rank : 2;
            second.GetComponent<MatchOneToManyPlayerItem>()
                    .Init(getItemData(secondUser, secondRank));

            var thirdUser = userScores.ElementAtOrDefault(2);
            var thirdRank = thirdUser != null && thirdUser.rank != -1 ? thirdUser.rank : 3;
            third.GetComponent<MatchOneToManyPlayerItem>()
                    .Init(getItemData(thirdUser, thirdRank));

            if (historyMatch.playStatus != PlayStatusRoomEnum.Finish)
            {
                handleBtnNewMatch(historyMatch);
            }

            //if (response.data.datas[0].scores.Count() <= 3) return;

            int yourOrder = 0;
            for (int i = 3; i < historyMatch.totalMember; i++)
            {
                var uScore = userScores.ElementAtOrDefault(i);
                var uRank = uScore != null && uScore.rank != -1 ? uScore.rank : i + 1;
                var itemData = getItemData(uScore, uRank);
                _data.Add(itemData);

                if (uScore != null && uScore.user.id.ToString() == Current.Ins.player.Id)
                {
                    yourOrder = uRank;
                }
            }

            var scrollPosition = scroller.ScrollPosition;
            scroller.ReloadData();
            scroller.ScrollPosition = scrollPosition;

            if (historyMatch.playStatus == PlayStatusRoomEnum.Finish)
            {
                handleBtnClaim(historyMatch, userScores, yourOrder);
            }
        }

        private void handleBtnNewMatch(HistoryMatchResponse.HistoryMatch historyMatch)
        {
            newMatchBtn.SetActive(true);
            newMatchTxtBtn.SetActive(true);
            bgActive.SetActive(true);
            bgDisable.SetActive(false);
            newMatchBtn.GetComponent<Button>().onClick.RemoveAllListeners();
            newMatchBtn.GetComponent<Button>().onClickWithHCSound(() =>
            {
                UIHome.Ins.scrollSnapView.GoToPanel(1);
            });
        }

        private void handleBtnClaim(HistoryMatchResponse.HistoryMatch historyMatch, List<RoomTournamentResponse.Score> userScores, int yourOrder)
        {
            if (historyMatch.yourRank > 3)
            {
                newMatchBtn.SetActive(false);
                //yourScore.GetComponent<MatchOneToManyPlayerItem>().Init(getItemData(userScores.ElementAtOrDefault(yourOrder - 1), yourOrder));
            }
            else
            {
                newMatchBtn.SetActive(true);
                claimTxtBtn.SetActive(true);          
                bgActive.SetActive(historyMatch.isClaimed == 0);
                bgDisable.SetActive(historyMatch.isClaimed != 0);
                newMatchBtn.GetComponent<Button>().onClick.RemoveAllListeners();
                newMatchBtn.GetComponent<Button>().onClickWithHCSound(() =>
                {
                    if (bgDisable.activeSelf) return;
                    StartCoroutine(Current.Ins.gameAPI.ClaimReward<ClaimRewardResponse>(historyMatch.id.ToString(), (res) =>
                    {
                        if (res == null || res.errorCode != 0)
                        {
                            Debug.Log("Claim Error:");
                            return;
                        }

                        Current.Ins.ReloadUserProfile(newMatchBtn.transform);
                        UIHistoryTab.Ins.IsResetView = true;
                        bgActive.SetActive(false);
                        bgDisable.SetActive(true);
                    }));
                });
            }
        }

        private MatchOneToManyPlayerItemData getItemData(RoomTournamentResponse.Score userScore, int order)
        {
            var itemData = new MatchOneToManyPlayerItemData();
            itemData.Order = order;

            if (userScore == null)
            {
                itemData.isSearching = true;
                return itemData;
            }

            itemData.isSearching = false;
            itemData.PlayerId = userScore.user.id.ToString();
            itemData.PlayerName = userScore.user.name;
            itemData.PlayerScore = userScore.score;
            itemData.PlayerAvt = userScore.user.avatar;
            itemData.PlayerAvtFrame = userScore.user.avatarFrame;

            return itemData;
        }

        private void OnEnable()
        {
            Current.Ins.Config.PropertyChanged += Config_PropertyChanged;
        }

        private void OnDisable()
        {
            Current.Ins.Config.PropertyChanged -= Config_PropertyChanged;
        }

        private void Config_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == StringConst.PROPERTY_LANGUAGE)
            {
                UIHistoryTab.Ins.IsResetView = true;
            }
        }
        public int GetNumberOfCells(EnhancedScroller scroller)
        {
            return _data.Count;
        }

        public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
        {
            return 136f;
        }

        public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
        {
            MatchOneToManyPlayerItem cellView = scroller.GetCellView(itemPrefab.GetComponent<MatchOneToManyPlayerItem>()) as MatchOneToManyPlayerItem;
            cellView.Init(_data[dataIndex]);
            return cellView;
        }
    }
}