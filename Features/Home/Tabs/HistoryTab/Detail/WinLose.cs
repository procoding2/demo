using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GameEnum;
using OutGameEnum;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class WinLose : UIBase<WinLose>
{
    public GameObject avatarGame;
    public GameObject nameGame;
    public GameObject tournamentNameTxt;
    public GameObject timeTxt;
    public GameObject coinInfoTxt;
    public GameObject coinInfoImg;
    public GameObject tokenInfoImg;
    public GameObject ticketInfoImg;

    public GameObject resultPendingTxt;
    public GameObject winLogo;
    public GameObject loseLogo;
    public GameObject drawLogo;

    [Header("LeftLayout")]
    [SerializeField]
    private ResultUserInfo myInfo;


    [Header("RightLayout")]
    [SerializeField]
    private ResultUserInfo opponentInfo;
    [SerializeField]
    private GameObject waitOpponentObj;





    [Header("Other")]
    public GameObject receiveTxt;
    public GameObject coinLayout;
    public GameObject receiveCoinLayout;
    public GameObject coinReceiveTxt;
    public GameObject receiveTokenLayout;
    public GameObject tokenReceiveTxt;
    public GameObject receiveTicketLayout;
    public GameObject ticketReceiveTxt;

    public GameObject claimBtn;
    public GameObject bgActive;
    public GameObject bgDisable;

    public GameObject closeBtn;

    private StatusScreenEnum status = StatusScreenEnum.Pending;
    protected override void InitIns() => Ins = this;

    protected override void Start()
    {
        closeBtn.GetComponent<ButtonEx>().onClickWithHCSound(() => UIHistoryTab.Ins.ResetLayout());
    }

    public void Init(HistoryMatchResponse.HistoryMatch historyMatch, Sprite gameSprite)
    {

        if (historyMatch.playStatus == PlayStatusRoomEnum.Finish)
        {
            if (historyMatch.games.Count == 0) status = StatusScreenEnum.Pending;
            if (historyMatch.games[0].scores.Count < 2) status = StatusScreenEnum.Pending;
            bool isYouWin = historyMatch.games[0].scores[0].user.id.ToString() == Current.Ins.player.Id;
            if (isYouWin)
            {
                status = StatusScreenEnum.Win;
            }
            else
            {
                status = StatusScreenEnum.Lose;
            }

            if (historyMatch.remainMember > 0) status = StatusScreenEnum.Pending;
        }
        else
        {
            status = StatusScreenEnum.Pending;
        }
        avatarGame.GetComponent<Image>().sprite = gameSprite;
        nameGame.GetComponent<TextMeshProUGUI>().text = historyMatch.game;
        tournamentNameTxt.GetComponent<TextMeshProUGUI>().text = historyMatch.miniGame.name;
        int feeValue;
        if (historyMatch.miniGame.tokenFee > 0)
        {
            tokenInfoImg.SetActive(true);
            ticketInfoImg.SetActive(false);
            coinInfoImg.SetActive(false);
            feeValue = historyMatch.miniGame.tokenFee;
        }
        else if (historyMatch.miniGame.ticketFee > 0)
        {
            tokenInfoImg.SetActive(false);
            ticketInfoImg.SetActive(true);
            coinInfoImg.SetActive(false);
            feeValue = historyMatch.miniGame.ticketFee;
        }
        else
        {
            tokenInfoImg.SetActive(false);
            ticketInfoImg.SetActive(false);
            coinInfoImg.SetActive(true);
            feeValue = historyMatch.miniGame.goldFee;
        }
        coinInfoTxt.GetComponent<TextMeshProUGUI>().text = feeValue.ToString();

        resultPendingTxt.SetActive(status == StatusScreenEnum.Pending);
        winLogo.SetActive(status == StatusScreenEnum.Win);
        loseLogo.SetActive(status == StatusScreenEnum.Lose);
        drawLogo.SetActive(false);

        if (historyMatch.games[0].isDraw)
        {
            winLogo.SetActive(false);
            loseLogo.SetActive(false);
            drawLogo.SetActive(true);
        }
        //left layout
        if (historyMatch.games.Count == 0) return;
        RoomTournamentResponse.Score myScore = historyMatch.games[0].scores.Find((obj) => obj.user.id.ToString() == Current.Ins.player.Id);

        if (myScore != null)
        {
            Current.Ins.ImageData.AvatarByUrl(myScore.user.avatar, (result) =>
            {
                myInfo.SetUserAvatar(result.Texture);
            });

            if (myScore.score < 0)
            {
                myInfo.SetUserInfo(myScore.user.name, "Playing Now...");
            }
            else
            {
                myInfo.SetUserInfo(myScore.user.name, myScore.score);
            }


            //nameWinLeft.GetComponent<TextMeshProUGUI>().text = myScore.user.name;
            //coinWinLeft.GetComponent<TextMeshProUGUI>().text = myScore.score.ToString();
            //coinWinLeft.SetActive(myScore.score != -1);
            //playingNowImgLeft.SetActive(myScore.score == -1);
            //if (myScore.score == -1)
            //{
            //    bgWinLeft.GetComponent<Image>().sprite = bgLose;
            //}
            //else
            //{
            //    bgWinLeft.GetComponent<Image>().sprite = bgWin;
            //}
            //nameLoseLeft.GetComponent<TextMeshProUGUI>().text = myScore.user.name;
            //coinLoseLeft.GetComponent<TextMeshProUGUI>().text = myScore.score.ToString();
        }
        else
        {
            myInfo.SetUserInfo("--", "Playing Now...");

            //nameWinLeft.GetComponent<TextMeshProUGUI>().text = "--";
            //playingNowImgLeft.SetActive(true);
            //bgWinLeft.GetComponent<Image>().sprite = bgLose;
        }
        //right layout
        RoomTournamentResponse.Score opponentScore = historyMatch.games[0].scores.Find((obj) => obj.user.id.ToString() != Current.Ins.player.Id);

        if (opponentScore != null)
        {
            waitOpponentObj.SetActive(false);
            opponentInfo.gameObject.SetActive(true);

            Current.Ins.ImageData.AvatarByUrl(opponentScore.user.avatar, (result) =>
            {
                opponentInfo.SetUserAvatar(result.Texture);
            });

            if (opponentScore.score < 0)
            {
                opponentInfo.SetUserInfo(opponentScore.user.name,  "Playing Now...");
            }
            else
            {
                opponentInfo.SetUserInfo(opponentScore.user.name, opponentScore.score);
            }

        }
        else
        {
            waitOpponentObj.SetActive(true);
            opponentInfo.gameObject.SetActive(false);

        }
        if (myScore != null && myScore.createdAt != 0)
        {
            timeTxt.GetComponent<TextMeshProUGUI>().text = Utils.DateTimeUtil.getTimeHistory(myScore.createdAt);
        }
        else if (opponentScore != null && opponentScore.createdAt != 0)
        {
            timeTxt.GetComponent<TextMeshProUGUI>().text = Utils.DateTimeUtil.getTimeHistory(opponentScore.createdAt);
        }
        else
        {
            timeTxt.GetComponent<TextMeshProUGUI>().text = Utils.DateTimeUtil.getTimeHistory(historyMatch.createdAt);
        }
        receiveTxt.SetActive(historyMatch.yourReward.token > 0 || historyMatch.yourReward.gold > 0 || historyMatch.yourReward.ticket > 0);
        coinLayout.SetActive(status != StatusScreenEnum.Pending);
        receiveTokenLayout.SetActive(historyMatch.yourReward.token > 0);
        receiveCoinLayout.SetActive(historyMatch.yourReward.gold > 0);
        receiveTicketLayout.SetActive(historyMatch.yourReward.ticket > 0);

        int receiveValue = 0;
        if (historyMatch.yourReward.token > 0)
        {
            receiveTokenLayout.SetActive(true);
            receiveValue = historyMatch.yourReward.token;
            tokenReceiveTxt.GetComponent<TextMeshProUGUI>().text = receiveValue.ToString();
        }
        if (historyMatch.yourReward.ticket > 0)
        {
            ticketReceiveTxt.GetComponent<TextMeshProUGUI>().text = historyMatch.yourReward.ticket.ToString();
            receiveValue = historyMatch.yourReward.ticket;
        }
        if (historyMatch.yourReward.gold > 0)
        {
            receiveCoinLayout.SetActive(true);
            receiveValue = historyMatch.yourReward.gold;
            coinReceiveTxt.GetComponent<TextMeshProUGUI>().text = receiveValue.ToString();
        }

        claimBtn.SetActive(status != StatusScreenEnum.Pending && receiveValue != 0);
        bgActive.SetActive(historyMatch.isClaimed == 0);
        bgDisable.SetActive(historyMatch.isClaimed == 1);
        claimBtn.GetComponent<Button>().onClick.RemoveAllListeners();
        claimBtn.GetComponent<Button>().onClickWithHCSound(() =>
        {
            if (bgDisable.activeSelf) return;
            handleClaimPrize(historyMatch);
        });

        if (status != StatusScreenEnum.Pending && !historyMatch.games[0].isDraw)
        {
            StartCoroutine(GameResultCoroutine(status == StatusScreenEnum.Win));
        }
        //else if(historyMatch.games[0].isDraw)
        //{
        //    StartCoroutine(GameResultDraw());
        //}
    }

    private IEnumerator GameResultCoroutine(bool isWin)
    {
        yield return new WaitForSeconds(0.5f);
        myInfo.SetResultAni(isWin);
        yield return new WaitForSeconds(0.5f);
        opponentInfo.SetResultAni(!isWin);
    }

    //private IEnumerator GameResultDraw()
    //{
    //    yield return new WaitForSeconds(0.5f);
    //    myInfo.SetResultAni(true);
    //    opponentInfo.SetResultAni(true);
    //}

    private void handleClaimPrize(HistoryMatchResponse.HistoryMatch historyMatch)
    {
        String roomId = historyMatch.id.ToString();

        UIHome.Ins.ShowHideLoadingPopup(true);
        StartCoroutine(Current.Ins.gameAPI.ClaimReward<ClaimRewardResponse>(roomId, (res) =>
        {
            UIHome.Ins.ShowHideLoadingPopup(false);
            if (res == null || res.errorCode != 0)
            {
                Debug.Log("Claim Error:");
                UIPopUpManager.instance.ShowErrorPopUp("Request Error : handleClaimPrize", res, () => handleClaimPrize(historyMatch));
                return;
            }

            Current.Ins.ReloadUserProfile(receiveCoinLayout.transform);
            UIHistoryTab.Ins.IsResetView = true;
            bgActive.SetActive(false);
            bgDisable.SetActive(true);
        }));
    }

    private void OnEnable()
    {
        Current.Ins.Config.PropertyChanged += Config_PropertyChanged;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        Current.Ins.Config.PropertyChanged -= Config_PropertyChanged;
    }

    private void Config_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
    {
        if (e.PropertyName == StringConst.PROPERTY_LANGUAGE)
        {
            UIHistoryTab.Ins.IsResetView = true;
        }
    }

    public enum StatusScreenEnum
    {
        Pending = 1,
        Win,
        Lose
    }
}
