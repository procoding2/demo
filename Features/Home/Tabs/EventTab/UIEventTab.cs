using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using OutGameEnum;
using System;
using HomeTab;
using UniRx;
using DanielLochner.Assets.SimpleScrollSnap;
using TMPro;
using Utils;

public class UIEventTab : UIBase<UIEventTab>
{
    public GameObject eventLayout;
    public GameObject bonusSystem;
    public GameObject imageRoulette;
    public GameObject imageScratch;
    public GameObject imageOpenBox;

    [Header("Backgound")]
    public GameObject BackgoundRoulette;// active false on Toggle_game, rank, history in scene Home, btn open profile
    public SimpleScrollSnap scrollSnap;
    [SerializeField] private Toggle togglePrefab;


    public GameObject itemPrefab;
    public Transform itemHolder;
    public GameObject scrollBar;

    [Header("Tut")]
    public ButtonEx tut;
    public GameObject tutPopup;


    private bool _isLoading;

    public GameObject prizeDetail;
    public GameObject specialOffer;
    public GameObject needCoinPopup;
    public GameObject sosummaryTxt;

    private bool _isInitialized = false;
    private MiniGameRequest _miniGameRequest;
    private DayOfWeek _oldDayOfWeek;

    public List<int> idxs = new();
    public void Init()
    {
        if (_isInitialized)
        {
            if (_isLoading) return;
            if (_miniGameRequest.page == 0) _miniGameRequest.page = 1;
            GetSpecialOffer(isBackground: true);

            return;
        }

        _isInitialized = true;
        //
        if (idxs.Count == 3)
        {
            var setSpecialOffer = this.specialOffer.GetComponent<RectTransform>();
            float inset = 150f;
            float size = GetComponent<RectTransform>().sizeDelta.y - 150f;
            setSpecialOffer.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, inset, size);
        }

        tut.onClickWithHCSound(() =>
        {
            tutPopup.GetComponent<HowToPlayMinigame>().InitTut();
            tutPopup.SetActive(true);
        });

        GetSpecialOffer();
    }
    public void Remove(int index)
    {
        if (scrollSnap.NumberOfPanels > 0)
        {
            // Pagination
            DestroyImmediate(scrollSnap.Pagination.transform.GetChild(scrollSnap.NumberOfPanels - 1).gameObject);

            // Panel
            scrollSnap.Remove(index);
        }

    }

    //On Toggle_event in scene Home, btn back in profile
    public void SetActiveBackgoundRoulette()
    {
        bool eventtab = UIHome.Ins.eventTabIcon.GetComponent<Toggle>().isOn;
        if (bonusSystem.activeSelf && eventtab) BackgoundRoulette.SetActive(true);
        else BackgoundRoulette.SetActive(false);
    }
    // Start is called before the first frame update
    protected override void Start()
    {
        imageRoulette.GetComponent<Button>().onClickWithHCSound(() =>
        {
            BackgoundRoulette.SetActive(true);
            InitBonusSystem(index: 0);
        });

        imageScratch.GetComponent<Button>().onClickWithHCSound(() =>
        {
            InitBonusSystem(index: 1);
        });

        imageOpenBox.GetComponent<Button>().onClickWithHCSound(() =>
        {
            InitBonusSystem(index: 2);
        });

        _miniGameRequest = new MiniGameRequest();
        _miniGameRequest.limit = 10;
        _miniGameRequest.page = 1;
        _miniGameRequest.location = 2;

        scrollBar.GetComponent<ScrollRect>().verticalNormalizedPosition = 1f;
        scrollBar.GetComponent<ScrollRect>().onValueChanged.AddListener((vector) =>
        {
            if (!_isInitialized) return;
            if (_isLoading || _miniGameRequest.page == 0) return;

            if (scrollBar.GetComponent<ScrollRect>().verticalNormalizedPosition <= 0.05f)
            {
                GetSpecialOffer(isNextPage: true);
            }
        });

        GetEventBanner(() =>
        {
            if (idxs.Count < 3)
            {
                idxs.OrderByDescending(x => x).ToList().ForEach(x => Remove(x));

                if (idxs.Count == 2)
                {
                    scrollSnap.Pagination.gameObject.SetActive(false);
                }
            }
            else
            {
                eventLayout.SetActive(false);
            }
        });
    }

    private void GetEventBanner(Action onResult)
    {
        var jsonData = PlayerPrefsEx.Get(PlayerPrefsConst.IMAGE_BANNER_DATA, string.Empty);
        var objData = JsonUtility.FromJson<ImageDataEventUrl>(jsonData);
        if (string.IsNullOrWhiteSpace(jsonData) || objData == null)
        {
            idxs = new List<int>() { 0, 1, 2 };
            onResult.Invoke();
        }
        else
        {
            var paths = new List<string>();
            if (!string.IsNullOrWhiteSpace(objData.Roulette)) paths.Add(objData.Roulette);
            else idxs.Add(0);
            if (!string.IsNullOrWhiteSpace(objData.Scrach)) paths.Add(objData.Scrach);
            else idxs.Add(1);
            if (!string.IsNullOrWhiteSpace(objData.OpenBox)) paths.Add(objData.OpenBox);
            else idxs.Add(2);

            UIHome.Ins.ShowHideLoadingPopup(true);
            Current.Ins.downloadAPI.DownloadAllImages(
                paths: paths,
                callBack: (isCompele, textTures) =>
                {
                    if (isCompele)
                    {
                        if (!string.IsNullOrWhiteSpace(objData.Roulette)) imageRoulette.GetComponent<RawImage>().texture = textTures.FirstOrDefault(x => x.Item1 == objData.Roulette).Item2;
                        if (!string.IsNullOrWhiteSpace(objData.Scrach)) imageScratch.GetComponent<RawImage>().texture = textTures.FirstOrDefault(x => x.Item1 == objData.Scrach).Item2;
                        if (!string.IsNullOrWhiteSpace(objData.OpenBox)) imageOpenBox.GetComponent<RawImage>().texture = textTures.FirstOrDefault(x => x.Item1 == objData.OpenBox).Item2;

                        onResult.Invoke();
                    }
                    else
                    {
                        idxs = new List<int>() { 0, 1, 2 };
                        onResult.Invoke();
                    }

                }, cacheFolder: Current.Ins.CACHE_BANNER_DIR);
        }
    }

    private void GetSpecialOffer(bool isBackground = false, bool isNextPage = false)
    {
        _isLoading = true;
        Action<MiniGameResponse.Item> actionItem = (res) =>
        {
            if (res.mode == GameEnum.MiniGameModeEnum.HeadToHead)
            {
                ConfirmFeePopupData dataFee = new(res.goldFee, res.tokenFee, res.tokenFee);
                if (res.game == GameEnum.MiniGameEnum.Bingo.GetStringValue())
                {
                    UIBingo.InitStaticVariable(res.mode, res.id, dataFee);
                    SceneHelper.LoadScene(SceneEnum.Bingo);
                }
                else if (res.game == GameEnum.MiniGameEnum.Solitaire.GetStringValue())
                {
                    UISolitaire.InitStaticVariable(res.mode, res.id, dataFee);
                    SceneHelper.LoadScene(SceneEnum.Solitaire);
                }
                else if (res.game == GameEnum.MiniGameEnum.Bubble.GetStringValue())
                {
                    UIBubble.InitStaticVariable(res.mode, res.id, dataFee);
                    SceneHelper.LoadScene(SceneEnum.Bubble);
                }
            }
            else
            {
                LoadPrizeDetail(res);
            }
        };

        if (!isBackground)
        {
            UIHome.Ins.ShowHideLoadingPopup(true);
        }

        var dispose = Observable.CombineLatest(
                Observable.FromCoroutine<MiniGameResponse>((resMiniGameRx) => Current.Ins.gameAPI.GetMiniGame(_miniGameRequest, (resA) => { resMiniGameRx.OnNext(resA); })),
                Observable.FromCoroutine<SpecialOfferSummaryResponse>((resSOSumanyRx) => Current.Ins.eventAPI.GetSpecialOfferSummary((res) => { resSOSumanyRx.OnNext(res); })),
                (minigame, sosummany) => { return (minigame, sosummany); })
            .Subscribe((result) =>
            {
                UIHome.Ins.ShowHideLoadingPopup(false);

                // MiniGame List
                if (result.minigame == null || result.minigame.errorCode != 0)
                {
                    UIPopUpManager.instance.ShowErrorPopUp("Request Error : GetSpecialOffer", result.minigame, () => GetSpecialOffer(isBackground, isNextPage));
                    return;
                }

                List<MiniGameResponse.Item> list = result.minigame.data.datas.ToList();
                _isLoading = false;
                _miniGameRequest.page = result.minigame.data.nextPage;

                var dow = DateTimeUtil.GetDayOfWeek(result.minigame.currentTime);
                if (dow != _oldDayOfWeek)
                {
                    itemHolder.RemoveAllChild();
                }

                if (isNextPage || itemHolder.childCount == 0)
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        GameObject gameObject = Instantiate(itemPrefab, itemHolder, false);
                        gameObject.GetComponent<ItemSpecialOffer>().Init(list[i], actionItem);
                    }
                }

                // SpecialOffer Summary
                if (result.sosummany == null || result.sosummany.errorCode != 0)
                {
                    UIPopUpManager.instance.ShowErrorPopUp("Request Error : GetSpecialOffer", result.sosummany, () => GetSpecialOffer(isBackground, isNextPage));
                    return;
                }

                _oldDayOfWeek = dow;
                sosummaryTxt.SetActive(dow == DayOfWeek.Saturday || dow == DayOfWeek.Sunday);
                sosummaryTxt.GetComponent<TextMeshProUGUI>().text = $"{result.sosummany.data.remain} / {result.sosummany.data.total}";
            });
    }

    private void InitBonusSystem(int index)
    {
        eventLayout.SetActive(false);
        specialOffer.SetActive(false);
        bonusSystem.SetActive(true);
        UIHome.Ins.ShowHideLoadingPopup(true);

        StartCoroutine(Current.Ins.bonusAPI.GetBonusGameFee<BonusGameFeeResponse>((res) =>
        {
            UIHome.Ins.ShowHideLoadingPopup(false);
            if (res == null || res.errorCode != 0)
            {
                Debug.Log("Error: GetBonusGameFee");
                UIPopUpManager.instance.ShowErrorPopUp("Request Error : InitBonusSystem", res, () => InitBonusSystem(index));
                return;
            }

            var data = new BonusSystemData();

            var rouletteRes = res.data.ToList().Where(x => x.game == BonusGameEnum.Roulette.GetStringValue()).FirstOrDefault();
            if (rouletteRes != null)
            {
                data.RouletteGameFee.GoldFee = rouletteRes.goldFee;
                data.RouletteGameFee.TicketFee = rouletteRes.ticketFee;
                data.RouletteGameFee.TokenFee = rouletteRes.tokenFee;
            }

            var scratchRes = res.data.ToList().Where(x => x.game == BonusGameEnum.Scratch.GetStringValue()).FirstOrDefault();
            if (scratchRes != null)
            {
                data.ScratchGameFee.GoldFee = scratchRes.goldFee;
                data.ScratchGameFee.TicketFee = scratchRes.ticketFee;
                data.ScratchGameFee.TokenFee = scratchRes.tokenFee;
            }

            var openBoxRes = res.data.ToList().Where(x => x.game == BonusGameEnum.OpenBox.GetStringValue()).FirstOrDefault();
            if (openBoxRes != null)
            {
                data.OpenBoxGameFee.GoldFee = openBoxRes.goldFee;
                data.OpenBoxGameFee.TicketFee = openBoxRes.ticketFee;
                data.OpenBoxGameFee.TokenFee = openBoxRes.tokenFee;
            }

            UIHome.Ins.ShowHideLoadingPopup(true);
            StartCoroutine(Current.Ins.bonusAPI.GetBonusGamePrizes<BonusGamePrizesResponse>(BonusGameEnum.Roulette.GetStringValue(), (prizesRes) =>
            {
                UIHome.Ins.ShowHideLoadingPopup(false);
                if (prizesRes == null || prizesRes.errorCode != 0)
                {
                    Debug.Log("Error: GetBonusGamePrizes");
                    UIPopUpManager.instance.ShowErrorPopUp("Request Error : GetBonusGamePrizes", prizesRes, () => InitBonusSystem(index));
                    return;
                }

                data.RoulettePrizes = new List<BonusSystemData.GamePrizesData>();

                prizesRes.data
                .datas
                .ToList()
                .OrderBy(x => (new System.Random()).Next())
                .ToList()
                .ForEach(d =>
                {
                    data.RoulettePrizes.Add(new BonusSystemData.GamePrizesData()
                    {
                        Id = d.id,
                        Amount = d.amount,
                        Type = d.type
                    });
                });

                UIHome.Ins.ShowHideLoadingPopup(true);
                StartCoroutine(Current.Ins.bonusAPI.GetRouletteTurn<RouletteTurnResponse>((rouletteRes) =>
                {
                    UIHome.Ins.ShowHideLoadingPopup(false);
                    if (rouletteRes == null || rouletteRes.errorCode != 0)
                    {
                        Debug.Log("Error: GetRouletteTurn");
                        UIPopUpManager.instance.ShowErrorPopUp("Request Error : GetRouletteTurn", rouletteRes, () => InitBonusSystem(index));
                        return;
                    }

                    data.RouletteFreeTurn.Bonus = rouletteRes.data.bonus;
                    data.RouletteFreeTurn.Daily = rouletteRes.data.daily;
                    data.RouletteFreeTurn.TimeRemain = rouletteRes.data.timeRemain;

                    bonusSystem.GetComponent<BonusSystem>().Init(data, index, ()=> 
                    {
                        if(idxs.Count<3)
                            eventLayout.SetActive(true);
                        specialOffer.SetActive(true);
                        bonusSystem.SetActive(false);
                        
                    });
                }));
            }));
        }));
    }

    private void LoadPrizeDetail(MiniGameResponse.Item miniGameResponse)
    {
        UIHome.Ins.ShowHideLoadingPopup(true);
        StartCoroutine(Current.Ins.gameAPI.GetMiniGameDetail(miniGameResponse.id, (res) =>
        {
            var gameMode = GameEnum.MiniGameEnum.Bingo;
            if (miniGameResponse.game == GameEnum.MiniGameEnum.Bingo.GetStringValue())
            {
                gameMode = GameEnum.MiniGameEnum.Bingo;
            }
            else if (miniGameResponse.game == GameEnum.MiniGameEnum.Solitaire.GetStringValue())
            {
                gameMode = GameEnum.MiniGameEnum.Solitaire;
            }
            else if (miniGameResponse.game == GameEnum.MiniGameEnum.Bubble.GetStringValue())
            {
                gameMode = GameEnum.MiniGameEnum.Bubble;
            }
            else if (miniGameResponse.game == GameEnum.MiniGameEnum.EBall.GetStringValue())
            {
                gameMode = GameEnum.MiniGameEnum.EBall;
            }
            else
            {
                gameMode = GameEnum.MiniGameEnum.Puzzle;
            }
            UIHome.Ins.ShowHideLoadingPopup(false);
            if (res == null || res.errorCode != 0)
            {
                UIPopUpManager.instance.ShowErrorPopUp("Request Error : LoadPrizeDetail", res, () => LoadPrizeDetail(miniGameResponse));
                return;
            }

            var detailData = new PrizeDetailData();
            detailData.Title = res.data.name;
            detailData.Id = res.data.id;
            detailData.Game = gameMode;
            detailData.Mode = res.data.mode;
            detailData.Quantity = res.data.quantity;
            detailData.TokenFee = res.data.tokenFee;
            detailData.GoldFee = res.data.goldFee;
            detailData.TicketFee = res.data.ticketFee;
            detailData.Items = new List<PrizeDetailItemData>();
            foreach (var itemRes in res.data.prizes)
            {
                PrizeDetailItemData itemData = new PrizeDetailItemData();
                detailData.Items.Add(itemData);
                itemData.Order = itemRes.rank;
                itemData.Gold = itemRes.gold;
                itemData.Token = itemRes.token;
                itemData.Ticket = itemRes.ticket;
            }
            eventLayout.SetActive(false);
            specialOffer.SetActive(false);
            prizeDetail.GetComponent<PrizeDetail>().Init(
                detailData,
                onBackAction: () =>
                {
                    if (idxs.Count < 3)
                        eventLayout.SetActive(true);
                    specialOffer.SetActive(true);
                },
                errorCoinAction: null /*needCoinPopup.GetComponent<NeedCoinPopup>().Init()*/
                );

        }));
    }

    protected override void InitIns()
    {
        Ins = this;
    }
}
