using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Text.RegularExpressions;
using DG.Tweening;

public class JackpotRewardPopup : MonoBehaviour
{
    public GameObject claimBtn;
    public GameObject textJackpot;
    public GameObject popupView;
    public ParticleSystem CoinRainParticleSystem;
    public GameObject headerView;
    public GameObject JackpotImage;

    private Action _claimAction;

    private Sequence _sq;

    public void Start()
    {
        claimBtn.GetComponent<Button>().onClick.AddListener(() =>
        {
            SoundManager.instance.PlayUIEffect("se_click1");
            StartCoroutine(claim());
        });

        GetComponent<Button>().onClickWithHCSound(() =>
        {
            if (_sq != null)
            {
                _sq.Kill();
            }

            CoinRainParticleSystem.Stop();
            StopCoroutine(AnimJackpot());
            headerView.transform.localScale = Vector2.one;
            _claimAction?.Invoke();
            _claimAction = null;
            popupView.SetActive(true);
            JackpotImage.SetActive(false);
        });
    }

    public void Init(string value, Action claimAction)
    {
        headerView.transform.localScale = Vector3.zero;
        CoinRainParticleSystem.Play();

        textJackpot.GetComponent<TextMeshProUGUI>().text = StringHelper.NumberFormatter(value);
        //HCSound.Ins.PlayGameSound3(HCSound.Ins.MiniGame.SuccessMission);
        SoundManager.instance.PlayUIEffect("se_win");
        SoundManager.instance.PlayUIEffect("se_coinSpill");
        _claimAction = claimAction;
        gameObject.SetActive(true);

        _sq = DOTween.Sequence();
        JackpotImage.transform.localScale = Vector3.zero;
        JackpotImage.SetActive(true);
        _sq.Join(JackpotImage.transform.DOScale(Vector3.one, 1f).SetEase(Ease.InOutBack));
        StartCoroutine(AnimJackpot());
    }
    private IEnumerator claim(float waitTime = 0.1f)
    {
        yield return new WaitForSeconds(waitTime);
        popupView.SetActive(false);
        gameObject.SetActive(false);
    }

    private IEnumerator AnimJackpot(float waitTime = 5f)
    {
        yield return new WaitForSeconds(waitTime);
        _claimAction?.Invoke();
        _claimAction = null;
        popupView.SetActive(true);
        JackpotImage.SetActive(false);
        headerView.transform.localScale = Vector2.one;
    }
}
