using System.Collections;
using System.Collections.Generic;
using DanielLochner.Assets.SimpleScrollSnap;
using UnityEngine;
using UnityEngine.UI;

public class HowToPlayPopup : MonoBehaviour
{
    public GameObject scrollSnap;
    public GameObject nextBtn;
    public GameObject doneBtn;
    public GameObject closeBtn;

    private void Start()
    {
        if (nextBtn != null) nextBtn.SetActive(true);
        else doneBtn.SetActive(true);

        scrollSnap.GetComponent<SimpleScrollSnap>().OnPanelCentered.AddListener((current, before) =>
        {
            SoundManager.instance.PlayUIEffect("se_click1");
            int totalPanels = scrollSnap.GetComponent<SimpleScrollSnap>().Panels.Length;
            bool isShowNextBtn = current < totalPanels - 1;
            if (nextBtn != null) nextBtn.SetActive(isShowNextBtn);
            doneBtn.SetActive(!isShowNextBtn);
        });

        doneBtn.GetComponent<Button>().onClick.AddListener(() =>
        {
            SoundManager.instance.PlayUIEffect("se_click1");
            scrollSnap.GetComponent<SimpleScrollSnap>().GoToPanel(0);
            gameObject.SetActive(false);
            UIHome.Ins.headerView.EnableClick(true);
        });

        closeBtn.GetComponent<Button>().onClick.AddListener(() =>
        {
            SoundManager.instance.PlayUIEffect("se_click1");
            doneBtn.GetComponent<Button>().onClick.Invoke();
        });
    }
}
