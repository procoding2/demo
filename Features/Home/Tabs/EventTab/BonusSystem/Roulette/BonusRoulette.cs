using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using OutGameEnum;
using UnityEngine.Localization.Components;
using DG.Tweening;
using TMPro;

public class BonusRoulette : UIBase<BonusRoulette>
{
    public GameObject[] circlelayouts;
    public GameObject lightOff;
    public GameObject startSpinBtn;
    public GameObject freeSpinText;

    public GameObject spinFee;
    public GameObject spinFeeText;
    public GameObject SpinFeeGoldIcon;
    public GameObject SpinFeeTokenIcon;
    public GameObject SpinFeeTicketIcon;

    public GameObject timeRemain;

    public GameObject needCointPopup;
    public GameObject toggleAutoSpin;

    public GameObject rewardPopup;

    public GameObject tutorialBtn;
    public GameObject tutorialPopup;

    public ConfirmFeePopup confirmFeePopup;

    protected override void InitIns() => Ins = this;

    private BonusSystemData.FreeTurnData _freeTurn;
    private BonusRouletteCircleLayout _circleLayout;
    private BonusSystemData.GameFeeData _fee;

    private bool _isReloadingFreeTurn;

    public void Init(BonusSystemData.GameFeeData fee,
                     List<BonusSystemData.GamePrizesData> prizes,
                     BonusSystemData.FreeTurnData freeTurn)
    { 
        _freeTurn = freeTurn;

        UpdateUIFreeTurn();

        circlelayouts.ToList().ForEach(l => l.SetActive(false));

        var cLayout = circlelayouts
            .ToList()
            .Where(x => x.GetComponent<BonusRouletteCircleLayout>().circles.Length == prizes.Count)
            .FirstOrDefault();

        if (cLayout == null) return;

        _circleLayout = cLayout.GetComponent<BonusRouletteCircleLayout>();

        toggleAutoSpin.GetComponent<Toggle>().onValueChanged.RemoveAllListeners();
        toggleAutoSpin.GetComponent<Toggle>().onValueChanged.AddListener(_circleLayout.UpdateAutoSpin);

        setFee(fee);

        cLayout.SetActive(true);
        _circleLayout.Init(prizes);

        StartCoroutine(SetLightOnOff());

        tutorialBtn.GetComponent<Button>().onClick.RemoveAllListeners();
        tutorialBtn.GetComponent<Button>().onClickWithHCSound(OnClickTutorial);
        //
        this.confirmFeePopup = GameObject.FindGameObjectWithTag("ConfirmFeePopup").GetComponent<ConfirmFeePopup>();
    }

    private IEnumerator SetLightOnOff(bool isOn = true)
    {
        yield return new WaitForSeconds(1f);

        lightOff.SetActive(isOn);
        StartCoroutine(SetLightOnOff(!isOn));
    }

    private void setFee(BonusSystemData.GameFeeData fee)
    {
        _fee = fee;
        startSpinBtn.GetComponent<Button>().onClick.RemoveAllListeners();
        startSpinBtn.GetComponent<Button>().onClickWithHCSound(() => StartSpin(fee));

        if (fee.GoldFee > 0)
        {
            SpinFeeGoldIcon.SetActive(true);
            SpinFeeTokenIcon.SetActive(false);
            SpinFeeTicketIcon.SetActive(false);
            spinFeeText.GetComponent<TextMeshProUGUI>().text = fee.GoldFee.ToString();

            return;
        }

        if (fee.TokenFee > 0)
        {
            SpinFeeGoldIcon.SetActive(false);
            SpinFeeTokenIcon.SetActive(true);
            SpinFeeTicketIcon.SetActive(false);
            spinFeeText.GetComponent<TextMeshProUGUI>().text = fee.TokenFee.ToString();

            return;
        }

        if (fee.TicketFee > 0)
        {
            SpinFeeGoldIcon.SetActive(false);
            SpinFeeTokenIcon.SetActive(false);
            SpinFeeTicketIcon.SetActive(true);
            spinFeeText.GetComponent<TextMeshProUGUI>().text = fee.TicketFee.ToString();

            return;
        }

        freeSpinText.SetActive(true);
        spinFee.SetActive(false);
        //spinFeeText.GetComponent<TextMeshProUGUI>().text = "Free";
    }

    private void StartSpin(BonusSystemData.GameFeeData fee, bool isConfirmFeePopup = true)
    {
        if (_freeTurn.Bonus <= 0 && _freeTurn.Daily <= 0)
        {
            startSpinBtn.GetComponent<Button>().interactable = true;
            UIHome.Ins.DisableTabAction(isEnable: true);
            BonusSystem.Ins.DisableTabAction(isEnable: true);

            if (Current.Ins.player.GoldCoin < fee.GoldFee)
            {
                needCointPopup.GetComponent<NeedCoinPopup>().Show(BonusGameEnum.Roulette, CoinEnum.Gold);
                return;
            }

            if (Current.Ins.player.HCTokenCoin < fee.TokenFee)
            {
                needCointPopup.GetComponent<NeedCoinPopup>().Show(BonusGameEnum.Roulette, CoinEnum.Token);
                return;
            }

            if (Current.Ins.player.TicketCoin < fee.TicketFee)
            {
                needCointPopup.GetComponent<NeedCoinPopup>().Show(BonusGameEnum.Roulette, CoinEnum.Ticket);
                return;
            }
        }     

        Action feePopupConfirm = () => 
        {
            tutorialBtn.GetComponent<Button>().onClick.RemoveAllListeners();
            int rewardId = 0;
            long jackpotValue = 0;
            startSpinBtn.GetComponent<Button>().interactable = false;
            UIHome.Ins.DisableTabAction(isEnable: false);
            BonusSystem.Ins.DisableTabAction(isEnable: false);
            _circleLayout.StartSpin((reward, autoClaim) =>
            {
                if (reward.Type == BonusGameRewardEnum.Jackpot)
                {
                    rewardPopup.GetComponent<ReceiveRewardPopup>()
                    .Show(reward.Type, jackpotValue.ToString(), () => ClaimAction(rewardId, autoClaim));
                }
                else
                {
                    rewardPopup.GetComponent<ReceiveRewardPopup>()
                    .Show(reward.Type, reward.Amount.ToString(), () => ClaimAction(rewardId, autoClaim), autoClaim);
                }
            });

            StartCoroutine(Current.Ins.bonusAPI.CreateBonusGameReward<CreateBonusGameRewardResponse>(BonusGameEnum.Roulette, (crBRRes) =>
            {
                if (crBRRes == null || crBRRes.errorCode != 0)
                {
                    Debug.Log("Error: CreateBonusGameReward");
                    _circleLayout.StopSpin();
                    UIHome.Ins.DisableTabAction(isEnable: true);
                    BonusSystem.Ins.DisableTabAction(isEnable: true);

                    UIPopUpManager.instance.ShowErrorPopUp("Request Error : StartSpin", crBRRes, () => StartSpin(fee));
                    return;
                }

                if (_freeTurn.totalFreeTurn == 0)
                {
                    Current.Ins.UpdatePlayerCoin(-fee.GoldFee, -fee.TokenFee, -fee.TicketFee);
                }

                rewardId = crBRRes.data.id;
                jackpotValue = crBRRes.data.reward.token;
                _circleLayout.UpdateResultId(crBRRes.data.type, crBRRes.data.amount);
            }));
        };

        if (_freeTurn.Bonus > 0 || _freeTurn.Daily > 0)
        {
            feePopupConfirm.Invoke();
            return;
        }
            
        if (isConfirmFeePopup)
        {
            confirmFeePopup.Init(
                data: new ConfirmFeePopupData(fee.GoldFee, fee.TokenFee, fee.TicketFee),
                confirmAction: feePopupConfirm);
        }
        else
        {
            feePopupConfirm.Invoke();
        }
    }

    private void ClaimAction(int rewardId, bool autoClaim)
    {
        UIHome.Ins.ShowHideLoadingPopup(true);
        toggleAutoSpin.GetComponent<Toggle>().interactable = false;
        StartCoroutine(Current.Ins.bonusAPI.ClaimBonusGameReward<BaseResponse>(rewardId, (claimRes) =>
        {
            UIHome.Ins.ShowHideLoadingPopup(false);
            if (claimRes == null || claimRes.errorCode != 0)
            {
                Debug.Log("Error: ClaimBonusGameReward");

                UIPopUpManager.instance.ShowErrorPopUp("Request Error : ClaimAction", claimRes, () => ClaimAction(rewardId, autoClaim));
                return;
            }

            ReloadFreeTurn();
            Current.Ins.ReloadUserProfile(rewardPopup.transform, callBackAfterDoneEffect: () =>
            {
                if (autoClaim) StartSpin(_fee, false);
                else
                {
                    tutorialBtn.GetComponent<Button>().onClickWithHCSound(OnClickTutorial);
                    startSpinBtn.GetComponent<Button>().interactable = true;
                }

                rewardPopup.SetActive(false);
                if (toggleAutoSpin.GetComponent<Toggle>().isOn == false)
                {
                    UIHome.Ins.DisableTabAction(isEnable: true);
                    BonusSystem.Ins.DisableTabAction(isEnable: true);
                }
                toggleAutoSpin.GetComponent<Toggle>().interactable = true;
            });
        }));
    }

    protected override void Update()
    {
        base.Update();

        if (_freeTurn != null && _freeTurn.TimeRemain >= 0 && !_isReloadingFreeTurn)
        {
            if (_freeTurn.TimeRemain >= Time.deltaTime)
            {
                _freeTurn.TimeRemain -= Time.deltaTime;
                string timeStr = (TimeSpan.FromSeconds(_freeTurn.TimeRemain)).ToString(@"hh'h'mm'm'ss's'");
                timeRemain.GetComponent<LocalizeStringEvent>().UpdateValue("time", VariableEnum.String, timeStr);
            }
            else
            {
                ReloadFreeTurn();
            }
        }
    }

    private void ReloadFreeTurn()
    {
        _isReloadingFreeTurn = true;
        StartCoroutine(Current.Ins.bonusAPI.GetRouletteTurn<RouletteTurnResponse>((res) =>
        {
            if (res == null || res.errorCode != 0)
            {
                Debug.Log("Error: GetRouletteTurn");
                return;
            }

            _freeTurn.Bonus = res.data.bonus;
            _freeTurn.Daily = res.data.daily;
            _freeTurn.TimeRemain = res.data.timeRemain;

            _isReloadingFreeTurn = false;

            UpdateUIFreeTurn();
        }));
    }

    private void UpdateUIFreeTurn()
    {
        if (_freeTurn.TimeRemain >= 0)
        {
            timeRemain.SetActive(true);
        }
        else
        {
            timeRemain.SetActive(false);
        }

        if (_freeTurn.totalFreeTurn > 0)
        {
            freeSpinText.SetActive(true);
            spinFee.SetActive(false);
            freeSpinText.GetComponent<TextMeshProUGUI>().text = $"{_freeTurn.totalFreeTurn} Free Turn";
        }
        else
        {
            freeSpinText.SetActive(false);
            spinFee.SetActive(true);
        }
    }

    public void OnClickTutorial()
    {
        tutorialPopup.SetActive(true);
        UIHome.Ins.headerView.EnableClick(false);
    }
}
