using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using OutGameEnum;
using TMPro;

public class BonusRouletteCircle : MonoBehaviour
{
    public GameObject gold_icon;
    public GameObject ticket_icon;
    public GameObject token_icon;
    public GameObject jackpot_icon;
    public GameObject spin_icon;
    public GameObject disableCircle;

    public TextMeshProUGUI text;

    [HideInInspector]
    public float MinDegree { get; private set; }

    [HideInInspector]
    public float MaxDegree { get; private set; }

    [HideInInspector]
    public float DegreePart10 { get; private set; }

    [HideInInspector]
    public int Id { get; private set; }

    [HideInInspector]
    public BonusSystemData.GamePrizesData Data { get; private set; }

    public void Init(float min, float max, BonusSystemData.GamePrizesData data)
    {
        Id = data.Id;
        MinDegree = min;
        MaxDegree = max;
        DegreePart10 = (max - min) / 10f;
        Data = data;

        disableCircle.GetComponent<Image>().fillAmount = gameObject.GetComponent<Image>().fillAmount;

        switch (data.Type)
        {
            case BonusGameRewardEnum.Gold:
                gold_icon.SetActive(true);
                ticket_icon.SetActive(false);
                token_icon.SetActive(false);
                jackpot_icon.SetActive(false);
                spin_icon.SetActive(false);

                text.text = FormatNumberRewardAmount(data.Amount);
                break;

            case BonusGameRewardEnum.HCToken:
                gold_icon.SetActive(false);
                ticket_icon.SetActive(false);
                token_icon.SetActive(true);
                jackpot_icon.SetActive(false);
                spin_icon.SetActive(false);

                text.text = FormatNumberRewardAmount(data.Amount);
                break;

            case BonusGameRewardEnum.Ticket:
                gold_icon.SetActive(false);
                ticket_icon.SetActive(true);
                token_icon.SetActive(false);
                jackpot_icon.SetActive(false);
                spin_icon.SetActive(false);

                text.text = FormatNumberRewardAmount(data.Amount);
                break;

            case BonusGameRewardEnum.Jackpot:
                gold_icon.SetActive(false);
                ticket_icon.SetActive(false);
                token_icon.SetActive(false);
                jackpot_icon.SetActive(true);
                spin_icon.SetActive(false);

                text.text = FormatNumberRewardAmount(data.Amount)+"%";
                break;

            case BonusGameRewardEnum.SlotSpin:
                gold_icon.SetActive(false);
                ticket_icon.SetActive(false);
                token_icon.SetActive(false);
                jackpot_icon.SetActive(false);
                spin_icon.SetActive(true);

                text.text = "+1";
                break;
        }
    }

    public bool SameItem(BonusGameRewardEnum type, int amount)
    {
        return Data.Type == type && Data.Amount == amount;
    }

    public string FormatNumberRewardAmount(int rewardAmount)
    {
        if (rewardAmount < 10000) return rewardAmount.ToString();

        return rewardAmount / 1000 + "K";
    }
}
