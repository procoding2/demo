using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using OutGameEnum;

public class BonusRouletteCircleLayout : UIBase<BonusRouletteCircleLayout>
{
    protected override void InitIns() => Ins = this;

    public GameObject[] circles;

    private readonly float MAX_SPIN_SPEED = 5f;
    private readonly float MIN_SPIN_SPEED = 0.5f;

    private bool _isSpin = false;
    private float _spinSpeed = 0f;
    private int? _resultId = null;
    private bool _autoSpin;

    public void Init(List<BonusSystemData.GamePrizesData> prizes)
    {
        float degree = 360f / (float)prizes.Count;
        for (int i = 0; i < prizes.Count; i++)
        {
            var item = circles[i].GetComponent<BonusRouletteCircle>();
            item.Init(min: degree * (float)i,
                      max: degree * (float)(i + 1),
                      data: prizes[i]);
        }
    }

    protected override void Update()
    {
        if (_isSpin)
        {
            gameObject
                .transform
                .RotateAround(gameObject.transform.position, Vector3.forward, _spinSpeed * 150 * Time.deltaTime);

            BonusRouletteCircle bonusCurrent = null;
            float zDegree = gameObject.transform.rotation.eulerAngles.z;
            for (int i = 0; i < circles.Length; i++)
            {
                var brC = circles[i].GetComponent<BonusRouletteCircle>();
                if (zDegree >= brC.MinDegree && zDegree < brC.MaxDegree)
                {
                    brC.disableCircle.SetActive(true);
                    bonusCurrent = brC;
                }
                else
                {
                    brC.disableCircle.SetActive(false);
                }
            }

            if (_resultId != null)
            {
                _spinSpeed = _spinSpeed <= MIN_SPIN_SPEED ? MIN_SPIN_SPEED : _spinSpeed - Time.deltaTime;

                if (_spinSpeed <= 1 && bonusCurrent != null && bonusCurrent.Id == _resultId.Value)
                {
                    bool inRange = zDegree <= bonusCurrent.MaxDegree - bonusCurrent.DegreePart10;
                    if (inRange)
                    {
                        _isSpin = UnityEngine.Random.Range(0, 20) != 1;
                    }
                    else
                    {
                        _isSpin = false;
                    }

                    if (!_isSpin)
                    {
                        StopSpin();
                        onSpinFalse();
                    }
                }
            }
        }

        base.Update();
    }

    private void onSpinFalse()
    {
        var resultItem = circles.FirstOrDefault(x => x.GetComponent<BonusRouletteCircle>().Id == _resultId)?.GetComponent<BonusRouletteCircle>();
        _resultId = null;

        if (resultItem != null)
        {
            StartCoroutine(DoActionDelay(resultItem));
        }
    }

    private IEnumerator DoActionDelay(BonusRouletteCircle circle)
    {
        yield return new WaitForSeconds(0.5f);

        var type = circle.Data.Type;
        var value = circle.Data.Amount;

        _onShowReward.Invoke(new BonusSystemData.GamePrizesData() { Amount = value, Type = type }, _autoSpin);
    }

    private Action<BonusSystemData.GamePrizesData, bool> _onShowReward;
    public void StartSpin(Action<BonusSystemData.GamePrizesData, bool> onShowReward)
    {
        //HCSound.Ins.PlayGameSound2(HCSound.Ins.BonusGame.Roulette);

        _spinSpeed = MAX_SPIN_SPEED;
        _isSpin = true;

        _onShowReward = onShowReward;

        StartCoroutine(SpinSoundPlay());
    }

    private IEnumerator SpinSoundPlay()
    {
        float delayT = 0.01f;
        while (_isSpin)
        {
            SoundManager.instance.PlayUIEffect("se_oldSchoolBell");
            yield return new WaitForSeconds(delayT);
            delayT += 0.005f;
        }
    }

    public void UpdateResultId(BonusGameRewardEnum type, int amount)
    {
        var item = circles.Where(c => c.GetComponent<BonusRouletteCircle>().SameItem(type, amount)).FirstOrDefault();

        if (item != null)
        {
            _resultId = item.GetComponent<BonusRouletteCircle>().Id;
        }
    }

    public void UpdateAutoSpin(bool isAuto)
    {
        _autoSpin = isAuto;
    }

    public void StopSpin()
    {
        SoundManager.instance.PlayUIEffect("se_recovery3");
        //HCSound.Ins.StopGameSound2();
        _isSpin = false;
    }
}

