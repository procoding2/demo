using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
[CustomEditor(typeof(BonusRouletteCircleLayout))]
public class BonusRouletteCircleLayoutEditor : Editor
{
    private readonly Color32 evenColor = new Color32(120, 118, 234, 255);
    private readonly Color32 oddColor = new Color32(243, 205, 91, 255);

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        BonusRouletteCircleLayout circleLayout = (BonusRouletteCircleLayout)target;

        if (circleLayout.circles == null || circleLayout.circles.Length == 0) return;

        int totalPart = circleLayout.circles.Length;
        float amountPart = 1f / (float)totalPart;

        for (int i = 0; i < totalPart; i++)
        {
            var circleItem = circleLayout.circles[i].GetComponent<Image>();

            if (circleItem == null) continue;

            float fillAmount = 1f - amountPart * (float)i;

            circleItem.fillAmount = fillAmount;
            //circleItem.color = i % 2 == 0 ? evenColor : oddColor;
        }
    }
}
#endif
