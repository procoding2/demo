using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using OutGameEnum;
using TMPro;

public class ReceiveRewardPopup : MonoBehaviour
{
    public GameObject gold_icon;
    public GameObject ticket_icon;
    public GameObject token_icon;

    public GameObject jackpotRewardPopup;

    public TextMeshProUGUI rewardText;
    public Button claimBtn;

    private Action _claimAction;

    private void Start()
    {
        claimBtn.onClick.AddListener(() =>
        {
            SoundManager.instance.PlayUIEffect("se_click1");
            StartCoroutine(claim());
        });
    }

    public void Show(BonusGameRewardEnum type,
                     string value,
                     Action claimAction,
                     bool autoClaim = false)
    {
        switch (type)
        {
            case BonusGameRewardEnum.Gold:
                gold_icon.SetActive(true);
                ticket_icon.SetActive(false);
                token_icon.SetActive(false);

                rewardText.text = value;
                break;

            case BonusGameRewardEnum.HCToken:
                gold_icon.SetActive(false);
                ticket_icon.SetActive(false);
                token_icon.SetActive(true);

                rewardText.text = value;
                break;

            case BonusGameRewardEnum.Ticket:
                gold_icon.SetActive(false);
                ticket_icon.SetActive(true);
                token_icon.SetActive(false);

                rewardText.text = value;
                break;

            case BonusGameRewardEnum.SlotSpin:
                gold_icon.SetActive(false);
                ticket_icon.SetActive(false);
                token_icon.SetActive(false);

                rewardText.text = "+1 spin";
                break;

            case BonusGameRewardEnum.Jackpot:
                jackpotRewardPopup.GetComponent<JackpotRewardPopup>().Init(value, claimAction);
                return;
        }

        transform.localScale = Vector3.zero;
        gameObject.SetActive(true);

        _claimAction = claimAction;
        if (type != BonusGameRewardEnum.Jackpot)
            StartCoroutine(claim(0.1f));

        //if (autoClaim) StartCoroutine(claim(2f));
    }

    private IEnumerator claim(float waitTime = 0.1f)
    {
        yield return new WaitForSeconds(waitTime);
        _claimAction?.Invoke();
        _claimAction = null;
    }
}
