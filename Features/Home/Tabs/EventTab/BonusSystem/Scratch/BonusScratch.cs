using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using OutGameEnum;
using TMPro;
using HomeTab;
using DG.Tweening;

namespace BonusScratch
{
    public class BonusScratch : UIBase<BonusScratch>
    {
        protected override void InitIns() => Ins = this;

        public GameObject ItemPrefab;
        public GameObject Layout;
        public GameObject StartBtn;
        public GameObject ScratchFeeGoldIcon;
        public GameObject ScratchFeeTokenIcon;
        public GameObject ScratchFeeTicketIcon;
        public GameObject feeScratchText;
        public GameObject freeScratchText;
        public GameObject feeView;

        public GameObject timeRemain;

        public GameObject ScratchAllBtn;

        public GameObject needCointPopup;
        public GameObject rewardPopup;

        public GameObject tutorialBtn;
        public GameObject tutorialPopup;
        public ConfirmFeePopup confirmFeePopup;

        private int _rewardId;
        private BonusGameRewardEnum _rewardTypeCorrect;
        private int _valueCorrect;
        private long _jackpotValue;
        private bool _isDone;
        private BonusSystemData.GameFeeData _fee;

        protected override void Start()
        {

#if UNITY_EDITOR
            for (int i = 0; i < 9; i++)
            {
                //Items[i].GetComponent<BoardItem>().Init(i, BonusGameRewardEnum.Jackpot, 10);
            }
#endif
        }

        public void Init(BonusSystemData.GameFeeData fee)
        {
            _fee = fee;

            setFee();
            StartBtn.GetComponent<Button>().onClick.RemoveAllListeners();
            StartBtn.GetComponent<Button>().onClickWithHCSound(() => StartScratch());

            ScratchAllBtn.GetComponent<Button>().onClick.RemoveAllListeners();
            ScratchAllBtn.GetComponent<Button>().onClickWithHCSound(() => ScratchAll());

            tutorialBtn.GetComponent<Button>().onClick.RemoveAllListeners();
            tutorialBtn.GetComponent<Button>().onClickWithHCSound(OnclickTutorial);
            //
            this.confirmFeePopup = GameObject.FindGameObjectWithTag("ConfirmFeePopup").GetComponent<ConfirmFeePopup>();
        }

        private void setFee()
        {

            if (_fee.GoldFee > 0)
            {
                SetFreeText(false);
                ScratchFeeGoldIcon.SetActive(true);
                ScratchFeeTokenIcon.SetActive(false);
                ScratchFeeTicketIcon.SetActive(false);
                feeScratchText.GetComponent<TextMeshProUGUI>().text = _fee.GoldFee.ToString();

                return;
            }

            if (_fee.TokenFee > 0)
            {
                SetFreeText(false);
                ScratchFeeGoldIcon.SetActive(false);
                ScratchFeeTokenIcon.SetActive(true);
                ScratchFeeTicketIcon.SetActive(false);
                feeScratchText.GetComponent<TextMeshProUGUI>().text = _fee.TokenFee.ToString();

                return;
            }

            if (_fee.TicketFee > 0)
            {
                SetFreeText(false);
                ScratchFeeGoldIcon.SetActive(false);
                ScratchFeeTokenIcon.SetActive(false);
                ScratchFeeTicketIcon.SetActive(true);
                feeScratchText.GetComponent<TextMeshProUGUI>().text = _fee.TicketFee.ToString();

                return;
            }

            SetFreeText();
            //feeScratchText.GetComponent<TextMeshProUGUI>().text = "Free";
        }

        public void SetFreeText(bool isShow = true)
        {
            feeView.SetActive(!isShow);
            freeScratchText.SetActive(isShow);
        }

        private void StartScratch()
        {
            if (Current.Ins.player.GoldCoin < _fee.GoldFee)
            {
                needCointPopup.GetComponent<NeedCoinPopup>().Show(BonusGameEnum.Scratch, CoinEnum.Gold);
                return;
            }

            if (Current.Ins.player.HCTokenCoin < _fee.TokenFee)
            {
                needCointPopup.GetComponent<NeedCoinPopup>().Show(BonusGameEnum.Scratch, CoinEnum.Token);
                return;
            }

            if (Current.Ins.player.TicketCoin < _fee.TicketFee)
            {
                needCointPopup.GetComponent<NeedCoinPopup>().Show(BonusGameEnum.Scratch, CoinEnum.Ticket);
                return;
            }

            confirmFeePopup.Init(
               data: new ConfirmFeePopupData(_fee.GoldFee, _fee.TokenFee, _fee.TicketFee),
               confirmAction: () =>
               {
                   tutorialBtn.GetComponent<Button>().onClick.RemoveAllListeners();

                   UIHome.Ins.ShowHideLoadingPopup(true);
                   UIHome.Ins.DisableTabAction(isEnable: false);
                   BonusSystem.Ins.DisableTabAction(isEnable: false);
                   StartCoroutine(Current.Ins.bonusAPI.GetBonusGamePrizes<BonusGamePrizesResponse>(BonusGameEnum.Scratch.GetStringValue(),
                   (res) =>
                   {
                       UIHome.Ins.ShowHideLoadingPopup(false);
                       if (res == null || res.errorCode != 0)
                       {
                           UIHome.Ins.DisableTabAction(isEnable: true);
                           BonusSystem.Ins.DisableTabAction(isEnable: true);
                           UIPopUpManager.instance.ShowErrorPopUp("Request Error : StartScratch", res, () => StartBtn.GetComponent<Button>().onClick.Invoke());
                           return;
                       }

                       _rewardId = res.data.rewardId;
                       if (!LoadPrizeCorrect(res.data.datas.ToList()))
                       {
                           UIHome.Ins.DisableTabAction(isEnable: true);

                           UIPopUpManager.instance.ShowErrorPopUp("Request Error : StartScratch", res, StartScratch);
                           return;
                       }

                       Layout.transform.RemoveAllChild();

                       for (int i = 0; i < res.data.datas.ToList().Count; i++)
                       {
                           var itemData = res.data.datas.ToList()[i];
                           var itemObj = Instantiate(ItemPrefab);
                           itemObj.transform.SetParent(Layout.transform, false);
                           itemObj.GetComponent<BoardItem>().Init(itemData.type, itemData.amount, () => onItemScratchDone());
                       }

                       _isDone = false;
                       StartBtn.SetActive(false);
                       ScratchAllBtn.SetActive(true);
                       StartCoroutine(CountDownRemainTime(5));

                       Current.Ins.UpdatePlayerCoin(-_fee.GoldFee, -_fee.TokenFee, -_fee.TicketFee);
                       _jackpotValue = res.data.reward.token;
                   }));
               });        
        }

        IEnumerator CountDownRemainTime(int number)
        {
            if (_isDone) yield break;

            timeRemain.GetComponent<TextMeshProUGUI>().text = $"00:0{number}";
            if (number == 5)
            {
                timeRemain.SetActive(true);
                yield return new WaitForSeconds(1f);
                StartCoroutine(CountDownRemainTime(number - 1));
            }
            else
            {
                yield return new WaitForSeconds(1f);

                if (number == 0)
                {
                    ScratchAll();
                    yield break;
                }

                StartCoroutine(CountDownRemainTime(number - 1));
            }
        }

        private void ScratchAll()
        {
            //HCSound.Ins.PlayGameSound2(HCSound.Ins.BonusGame.Scratch_Done);
            SoundManager.instance.PlayUIEffect("se_recovery3");
            UIHome.Ins.DisableTabAction(isEnable: false);
            BonusSystem.Ins.DisableTabAction(isEnable: false);
            if (_isDone) return;

            timeRemain.SetActive(false);
            _isDone = true;

            ScratchAllBtn.GetComponent<Button>().interactable = false;
            var items = Layout.GetComponentsInChildren<BoardItem>().ToList();
            items.ForEach(x => x.UnHideItem(x.RewardType == _rewardTypeCorrect && x.Value == _valueCorrect));

            StartCoroutine(ShowReward());
        }

        IEnumerator ShowReward()
        {
            yield return new WaitForSeconds(1f);
            UIHome.Ins.DisableTabAction(isEnable: true);
            if (_rewardTypeCorrect == BonusGameRewardEnum.Jackpot)
            {
                rewardPopup.GetComponent<ReceiveRewardPopup>().Show(_rewardTypeCorrect, _jackpotValue.ToString(), () => ClaimAction());
            }
            else
            {
                rewardPopup.GetComponent<ReceiveRewardPopup>().Show(_rewardTypeCorrect, _valueCorrect.ToString(), () => ClaimAction());
            }
        }

        private void ClaimAction()
        {
            UIHome.Ins.ShowHideLoadingPopup(true);
            StartCoroutine(Current.Ins.bonusAPI.ClaimBonusGameReward<BaseResponse>(_rewardId, (claimRes) =>
            {
                UIHome.Ins.ShowHideLoadingPopup(false);
                if (claimRes == null || claimRes.errorCode != 0)
                {
                    Debug.Log("Error: ClaimBonusGameReward");
                    UIPopUpManager.instance.ShowErrorPopUp("Request Error : ClaimAction", claimRes, ClaimAction);
                    return;
                }

                Current.Ins.ReloadUserProfile(rewardPopup.transform, callBackAfterDoneEffect: () =>
                {
                    ScratchAllBtn.GetComponent<Button>().interactable = true;
                    ScratchAllBtn.SetActive(false);
                    StartBtn.SetActive(true);

                    tutorialBtn.GetComponent<Button>().onClickWithHCSound(OnclickTutorial);
                    BonusSystem.Ins.DisableTabAction(isEnable: true);
                    rewardPopup.SetActive(false);
                });
            }));
        }

        private bool LoadPrizeCorrect(List<BonusGamePrizesResponse.Prize> prizes)
        {
            var prize = prizes
                .GroupBy(p => new { p.type, p.amount })
                .ToList()
                .Where(x=>x.Count() == 3)
                .FirstOrDefault();

            if (prize != null)
            {
                _rewardTypeCorrect = prize.First().type;
                _valueCorrect = prize.First().amount;
                return true;
            }

            return false;
        }

        private void onItemScratchDone()
        {
            var items = Layout.GetComponentsInChildren<BoardItem>().ToList();
            var dones = items
                .Where(x => x.RewardType == _rewardTypeCorrect && x.Value == _valueCorrect && x.IsDone)
                .ToList();
            if (dones.Count >= 3)
            {
                ScratchAll();
            }
        }

        //public int? ItemDrawingIndex()
        //{
        //    var itemDrawing = Items.ToList().FirstOrDefault(x => x.GetComponent<BoardItem>().IsDrawing);
        //    if (itemDrawing != null)
        //    {
        //        return itemDrawing.GetComponent<BoardItem>().Index;
        //    }

        //    return null;
        //}

        //public void SetDrawOffAllItem()
        //{
        //    Items.ToList().ForEach(x => x.GetComponent<BoardItem>().IsDrawing = false);
        //}

        public void OnclickTutorial()
        {
            tutorialPopup.SetActive(true);
            UIHome.Ins.headerView.EnableClick(false);
        }
    }
}