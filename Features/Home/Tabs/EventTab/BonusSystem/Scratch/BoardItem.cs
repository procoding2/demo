using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OutGameEnum;
using TMPro;
using UnityEngine.UI;

namespace BonusScratch
{
    public class BoardItem : MonoBehaviour
    {
        public GameObject dotSpritePrefab;

        public GameObject ValueText;

        public GameObject GoldSprite;
        public GameObject TokenSprite;
        public GameObject TicketSprite;
        public GameObject JackpotSprite;

        public GameObject GoldIcon;
        public GameObject TokenIcon;
        public GameObject TicketIcon;

        public GameObject BackgroundSprite;
        public GameObject BackgroundNormal;
        public GameObject BackgroundLight;

        //[HideInInspector]
        //public bool IsDrawing;
        [HideInInspector]
        public int Index;

        [HideInInspector]
        public bool IsDone = false;

        [HideInInspector]
        public BonusGameRewardEnum RewardType;

        [HideInInspector]
        public int Value;

        private Action _onDoneAction;

        private GameObject _rewardObject;
        
        private bool _isDrawable = false;

        private List<GameObject> _dotSprites;

        private bool _isPlayingScratchSound = false;

        public void Init(BonusGameRewardEnum rewardType, int value, Action onDoneAction)
        {
            RewardType = rewardType;
            Value = value;
            _onDoneAction = onDoneAction;

            switch (rewardType)
            {
                case BonusGameRewardEnum.Gold:
                    _rewardObject = GoldSprite;
                    break;

                case BonusGameRewardEnum.HCToken:
                    _rewardObject = TokenSprite;
                    break;

                case BonusGameRewardEnum.Ticket:
                    _rewardObject = TicketSprite;
                    break;

                case BonusGameRewardEnum.Jackpot:
                    _rewardObject = JackpotSprite;
                    _rewardObject.GetComponent<TextMeshProUGUI>().text = $"{value}% Jackpot";
                    break;
            }

            ValueText.GetComponent<TextMeshProUGUI>().text = $"x{StringHelper.NumberFormatter(value)}";
            _rewardObject.SetActive(true);
            _isDrawable = true;
            _dotSprites = new List<GameObject>();
        }

        private void Update()
        {
            if (!Input.GetMouseButton(0)) return;

            if (IsDone || !_isDrawable) return;

            //if (BonusScratch.Ins.ItemDrawingIndex() != null
            //    && BonusScratch.Ins.ItemDrawingIndex().Value != Index)
            //{
            //    return;
            //}

            var mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (!gameObject.GetComponent<BoxCollider2D>().OverlapPoint(mousePos)) return;

            //IsDrawing = true;

            if (!_isPlayingScratchSound)
            {
                _isPlayingScratchSound = true;
                SoundManager.instance.PlayUIEffect("se_scratch");
            }

            var dotLine = Instantiate(dotSpritePrefab);
            dotLine.transform.SetParent(gameObject.transform, false);
            dotLine.transform.position = new Vector2(mousePos.x, mousePos.y);

            _dotSprites.Add(dotLine);
            OnAfterDrawLine();
        }

        private void OnMouseUp()
        {
            _isPlayingScratchSound = false;
            //HCSound.Ins.StopGameSound2();
        }

        public void OnAfterDrawLine()
        {
            var traces = _rewardObject.transform.GetComponentsInChildren<BoardTraceItem>();

            var mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            foreach (var traceItem in traces)
            {
                if (traceItem.IsDone) continue;

                if (traceItem.gameObject.GetComponent<BoxCollider2D>().OverlapPoint(mousePos))
                {
                    traceItem.IsDone = true;
                }
            }

            if (traces.ToList().All(x => x.IsDone))
            {
                UnHideItem();
                _onDoneAction.Invoke();
            }
        }

        public void UnHideItem(bool isReward = false)
        {
            if (RewardType != BonusGameRewardEnum.Jackpot)
            {
                _rewardObject.SetActive(false);
                BackgroundSprite.SetActive(false);
                BackgroundNormal.SetActive(true);

                if (RewardType == BonusGameRewardEnum.Gold)
                {
                    GoldIcon.SetActive(true);
                }
                else if (RewardType == BonusGameRewardEnum.HCToken)
                {
                    TokenIcon.SetActive(true);
                }
                else if (RewardType == BonusGameRewardEnum.Ticket)
                {
                    TicketIcon.SetActive(true);
                }

                ValueText.SetActive(true);
                ValueText.GetComponent<Mask>().showMaskGraphic = true;

                if (isReward)
                {
                    BackgroundLight.SetActive(true);
                }
            }
            else
            {
                _rewardObject.GetComponent<Mask>().showMaskGraphic = true;
                BackgroundNormal.SetActive(true);
                if (isReward)
                {
                    BackgroundLight.SetActive(true);
                }
            }

            _dotSprites.ForEach(x => Destroy(x));
            IsDone = true;
        }
    }
}