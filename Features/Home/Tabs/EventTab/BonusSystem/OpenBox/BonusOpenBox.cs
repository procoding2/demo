using System;
using System.Collections;
using DG.Tweening;
using OutGameEnum;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BonusOpenBox : UIBase<BonusOpenBox>
{
    public GameObject openBoxBt;
    public GameObject BoxImg;
    public GameObject needCointPopup;
    public GameObject rewardPopup;
    public GameObject tutorialBtn;
    public GameObject tutorialPopup;
    public Animator animator;
    public GameObject boxFeeText;
    public GameObject boxFeeGoldIcon;
    public GameObject boxFeeTokenIcon;
    public GameObject boxFeeTicketIcon;
    public ConfirmFeePopup confirmFeePopup;
    public GameObject freeText;
    public GameObject feeView;

    //
    [Header("In4 reward")]
    public Image ImgBoxBottom;
    public Sprite ticketSprite;
    public Sprite tokenSprite;
    public Sprite goldSprite;

    protected override void InitIns() => Ins = this;
    private bool _isReloadingFreeTurn;

    private int _rewardId = 0;
    private CreateBonusGameRewardResponse _crBRRes;
    private Action<BonusSystemData.GamePrizesData> _onShowReward;
    private bool _openBoxDone = false;

    public void Init(BonusSystemData.GameFeeData fee)
    {
        setFee(fee);
        OpenBoxBtn(fee);
        this.confirmFeePopup = GameObject.FindGameObjectWithTag("ConfirmFeePopup").GetComponent<ConfirmFeePopup>();
    }

    private void setFee(BonusSystemData.GameFeeData fee)
    {
        if (fee.GoldFee > 0)
        {
            ShowFreeText(false);
            boxFeeGoldIcon.SetActive(true);
            boxFeeTokenIcon.SetActive(false);
            boxFeeTicketIcon.SetActive(false);
            boxFeeText.GetComponent<TextMeshProUGUI>().text = fee.GoldFee.ToString();
            return;
        }

        if (fee.TokenFee > 0)
        {
            ShowFreeText(false);
            boxFeeGoldIcon.SetActive(false);
            boxFeeTokenIcon.SetActive(true);
            boxFeeTicketIcon.SetActive(false);
            boxFeeText.GetComponent<TextMeshProUGUI>().text = fee.TokenFee.ToString();
            return;
        }

        if (fee.TicketFee > 0)
        {
            ShowFreeText(false);
            boxFeeGoldIcon.SetActive(false);
            boxFeeTokenIcon.SetActive(false);
            boxFeeTicketIcon.SetActive(true);
            boxFeeText.GetComponent<TextMeshProUGUI>().text = fee.TicketFee.ToString();
            return;
        }

        ShowFreeText();
    }

    public void ShowFreeText(bool isShow = true)
    {
        feeView.SetActive(!isShow);
        freeText.SetActive(isShow);
    }

    private void OpenBoxBtn(BonusSystemData.GameFeeData fee)
    {
        Action<bool> setStateStartSpinBtn = (isEnable) => openBoxBt.GetComponent<Button>().interactable = isEnable;

        openBoxBt.GetComponent<Button>().onClick.RemoveAllListeners();
        openBoxBt.GetComponent<Button>().onClickWithHCSound(() => OpenBox(fee, setStateStartSpinBtn));

        _onShowReward = (reward) =>
        {
            rewardPopup.GetComponent<ReceiveRewardPopup>()
            .Show(reward.Type, reward.Amount.ToString(), () => ClaimAction(setStateStartSpinBtn));
        };

        tutorialBtn.GetComponent<Button>().onClick.RemoveAllListeners();
        tutorialBtn.GetComponent<Button>().onClickWithHCSound(OnClickTutorial);
    }

    private void OpenBox(BonusSystemData.GameFeeData fee, Action<bool> setStateStartSpinBtn)
    {
        if (Current.Ins.player.GoldCoin < fee.GoldFee)
        {
            needCointPopup.GetComponent<NeedCoinPopup>().Show(BonusGameEnum.OpenBox, CoinEnum.Gold);
            return;
        }

        if (Current.Ins.player.HCTokenCoin < fee.TokenFee)
        {
            needCointPopup.GetComponent<NeedCoinPopup>().Show(BonusGameEnum.OpenBox, CoinEnum.Token);
            return;
        }

        if (Current.Ins.player.TicketCoin < fee.TicketFee)
        {
            needCointPopup.GetComponent<NeedCoinPopup>().Show(BonusGameEnum.OpenBox, CoinEnum.Ticket);
            return;
        }

        confirmFeePopup.Init(
            data: new ConfirmFeePopupData(fee.GoldFee, fee.TokenFee, fee.TicketFee),
            confirmAction: () =>
            {
                setStateStartSpinBtn(false);
                UIHome.Ins.DisableTabAction(isEnable: false);
                BonusSystem.Ins.DisableTabAction(isEnable: false);
                StartCoroutine(Current.Ins.bonusAPI.CreateBonusGameReward<CreateBonusGameRewardResponse>(BonusGameEnum.OpenBox, (crBRRes) =>
                {
                    _crBRRes = crBRRes;
                    if (crBRRes == null || crBRRes.errorCode != 0)
                    {
                        Debug.Log("Error: CreateBonusGameReward");
                        setStateStartSpinBtn(true);
                        UIHome.Ins.DisableTabAction(isEnable: true);
                        BonusSystem.Ins.DisableTabAction(isEnable: true);

                        UIPopUpManager.instance.ShowErrorPopUp("Request Error : OpenBox", crBRRes, () => OpenBox(fee, setStateStartSpinBtn));

                        return;
                    }

                    switch (_crBRRes.data.type)
                    {
                        case BonusGameRewardEnum.Ticket:
                            ImgBoxBottom.sprite = ticketSprite;
                            break;
                        case BonusGameRewardEnum.HCToken:
                            ImgBoxBottom.sprite = tokenSprite;
                            break;
                        case BonusGameRewardEnum.Gold:
                            ImgBoxBottom.sprite = goldSprite;
                            break;
                    }

                    confirmFeePopup.transform.DOScale(Vector3.zero, 0f);
                    tutorialBtn.GetComponent<Button>().onClick.RemoveAllListeners();

                    Current.Ins.UpdatePlayerCoin(-fee.GoldFee, -fee.TokenFee, -fee.TicketFee);
                    animator.SetTrigger("OpenBox");
                    _openBoxDone = true;

                    StartCoroutine(OpenBoxSound());
                    IEnumerator OpenBoxSound()
                    {
                        for (int i = 0; i < 4; i++)
                        {
                            SoundManager.instance.PlayUIEffect($"se_equip{UnityEngine.Random.Range(1,3)}");
                            yield return new WaitForSeconds(0.5f);
                            
                        }
                    }
                }));
            });
    }

    public void HandleShowReward()
    {
        SoundManager.instance.PlayUIEffect("se_recovery3");

        _rewardId = _crBRRes.data.id;
        _onShowReward.Invoke(new BonusSystemData.GamePrizesData() { Amount = _crBRRes.data.amount, Type = _crBRRes.data.type });
    }

    private void ClaimAction(Action<bool> setStateStartSpinBtn)
    {
        UIHome.Ins.ShowHideLoadingPopup(true);
        StartCoroutine(Current.Ins.bonusAPI.ClaimBonusGameReward<BaseResponse>(_rewardId, (claimRes) =>
        {
            UIHome.Ins.ShowHideLoadingPopup(false);           

            if (claimRes == null || claimRes.errorCode != 0)
            {
                if (_openBoxDone)
                {
                    animator.SetTrigger("ResetBox");
                    _openBoxDone = false;
                }
                Debug.Log("Error: ClaimBonusGameReward");


                UIPopUpManager.instance.ShowErrorPopUp("Request Error : ClaimBonusGameReward", claimRes, () => ClaimAction(setStateStartSpinBtn));

                return;
            }          

            Current.Ins.ReloadUserProfile(rewardPopup.transform, callBackAfterDoneEffect: () =>
            {
                tutorialBtn.GetComponent<Button>().onClickWithHCSound(OnClickTutorial);
                BonusSystem.Ins.DisableTabAction(isEnable: true);
                rewardPopup.SetActive(false);
                if (_openBoxDone)
                {
                    animator.SetTrigger("ResetBox");
                    _openBoxDone = false;
                    setStateStartSpinBtn.Invoke(true);
                    UIHome.Ins.DisableTabAction(isEnable: true);
                }
            });
        }));
    }

    public void OnClickTutorial()
    {
        tutorialPopup.SetActive(true);
        UIHome.Ins.headerView.EnableClick(false);
    }
}
