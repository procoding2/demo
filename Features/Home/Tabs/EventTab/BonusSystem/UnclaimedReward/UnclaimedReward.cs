using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;
using OutGameEnum;

public class UnclaimedReward : MonoBehaviour
{
    [SerializeField] ScrollRectEx scrollRectParent;
    [Header("GameObj Parent")]
    [SerializeField] GameObject roulette;
    [SerializeField] GameObject scratch;
    [SerializeField] GameObject randomBox;
    [Header("GameObj Content")]
    [SerializeField] GameObject rouletteContent;
    [SerializeField] GameObject scratchContent;
    [SerializeField] GameObject randomBoxContent;
    [Header("Prefab")]
    [SerializeField] GameObject RewardCoinGold;
    [SerializeField] GameObject RewardJackpot;
    [SerializeField] GameObject RewardSpin;
    [SerializeField] GameObject RewardTicket;
    [SerializeField] GameObject RewardToken;
    [Header("Button")]
    [SerializeField] Button close;
    [SerializeField] Button claimRoulette;
    [SerializeField] Button claimScratch;
    [SerializeField] Button claimRandomBox;
    //
    [Header("In4")]
    [SerializeField] bool rouletteAcitve = false;
    [SerializeField] bool scratchAcitve = false;
    [SerializeField] bool randomBoxAcitve = false;
    [SerializeField] bool checkOnlyRewardSpin = false;
    //
    string GameType = null;
    BonusGameRewardEnum BonusType;

    private void Awake()
    {
        close.onClick.AddListener(OnClickClose);
        //claimRoulette.onClick.AddListener(() => ClaimAction(claimRoulette.gameObject));
    }

    public void Init(BonusGameRewardResponse res)
    {
        for (int i = 0; i < res.data.Length; i++)
        {
            this.GameType = res.data[i].prize.game;
            this.BonusType = res.data[i].prize.type;
            switch (this.GameType)
            {
                case "roulette":
                    rouletteAcitve = true;
                    switch (BonusType)
                    {
                        case BonusGameRewardEnum.Gold:
                            CreateBonusRewardItem(rouletteContent, RewardCoinGold, res.data[i].prize.amount);
                            this.checkOnlyRewardSpin = true;
                            break;
                        case BonusGameRewardEnum.HCToken:
                            CreateBonusRewardItem(rouletteContent, RewardToken, res.data[i].prize.amount);
                            this.checkOnlyRewardSpin = true;
                            break;
                        case BonusGameRewardEnum.Ticket:
                            CreateBonusRewardItem(rouletteContent, RewardTicket, res.data[i].prize.amount);
                            this.checkOnlyRewardSpin = true;
                            break;
                        case BonusGameRewardEnum.Jackpot:
                            CreateBonusRewardItem(rouletteContent, RewardJackpot, res.data[i].prize.amount,true);
                            this.checkOnlyRewardSpin = true;
                            break;
                        case BonusGameRewardEnum.SlotSpin:
                            CreateBonusRewardItem(rouletteContent, RewardSpin, res.data[i].prize.amount);
                            break;
                        //====================
                        default:
                            Debug.Log("UnknownBonusType roulette");
                            break;
                    }
                    break;
                case "scratch":
                    scratchAcitve = true;
                    switch (BonusType)
                    {
                        case BonusGameRewardEnum.Gold:
                            CreateBonusRewardItem(scratchContent, RewardCoinGold, res.data[i].prize.amount);
                            break;
                        case BonusGameRewardEnum.HCToken:
                            CreateBonusRewardItem(scratchContent, RewardToken, res.data[i].prize.amount);
                            break;
                        case BonusGameRewardEnum.Ticket:
                            CreateBonusRewardItem(scratchContent, RewardTicket, res.data[i].prize.amount);
                            break;
                        case BonusGameRewardEnum.Jackpot:
                            CreateBonusRewardItem(scratchContent, RewardJackpot, res.data[i].prize.amount, true);
                            break;
                        //====================
                        default:
                            Debug.Log("UnknownBonusType scratch");
                            break;
                    }
                    break;
                case "randombox":
                    randomBoxAcitve = true;
                    switch (BonusType)
                    {
                        case BonusGameRewardEnum.Gold:
                            CreateBonusRewardItem(randomBoxContent, RewardCoinGold, res.data[i].prize.amount);
                            break;
                        case BonusGameRewardEnum.HCToken:
                            CreateBonusRewardItem(randomBoxContent, RewardToken, res.data[i].prize.amount);
                            break;
                        case BonusGameRewardEnum.Ticket:
                            CreateBonusRewardItem(randomBoxContent, RewardTicket, res.data[i].prize.amount);
                            break;
                        case BonusGameRewardEnum.Jackpot:
                            CreateBonusRewardItem(randomBoxContent, RewardJackpot, res.data[i].prize.amount, true);
                            break;
                        //====================
                        default:
                            Debug.Log("UnknownBonusType randombox");
                            break;
                    }
                    break;
                //=================
                default:
                    Debug.LogError("UnknownGameType");
                    break;
            }
        }

        roulette.SetActive(rouletteAcitve);
        scratch.SetActive(scratchAcitve);
        randomBox.SetActive(randomBoxAcitve);

        if (rouletteAcitve == false || scratchAcitve == false || randomBoxAcitve == false)
        {
            scrollRectParent.vertical = false;
        }

        if (rouletteAcitve == true)
        {
            claimRoulette.onClickWithHCSound(() => ClaimAction(claimRoulette.gameObject, roulette, "roulette"));
        }

        if (scratchAcitve == true)
        {
            claimScratch.onClickWithHCSound(() => ClaimAction(claimScratch.gameObject, scratch, "scratch"));
        }

        if (randomBoxAcitve == true)
        {
            claimRandomBox.onClickWithHCSound(() => ClaimAction(claimRandomBox.gameObject, randomBox, "randombox"));
        }

        CheckScrollRect(rouletteContent);
        CheckScrollRect(scratchContent);
        CheckScrollRect(randomBoxContent);

        UIHome.Ins.headerView.EnableClick(false);
    }

    void CreateBonusRewardItem(GameObject rewardTypeParent, GameObject rewardType, int amount, bool jackpot = false)
    {
        GameObject a = Instantiate(rewardType, rewardTypeParent.transform.position, rewardTypeParent.transform.rotation, rewardTypeParent.transform);
        if(!jackpot)
            a.GetComponentInChildren<TMP_Text>().text = "x" + amount;
        else
            a.GetComponentInChildren<TMP_Text>().text = amount+"%";
    }

    void CheckScrollRect(GameObject a)
    {
        if (a.transform.childCount < 5)
            a.transform.parent.parent.GetComponent<ScrollRectEx>().horizontal = false;
    }

    void OnClickClose()
    {
        gameObject.transform.DOScale(Vector3.zero, 0.15f).OnComplete(() =>
        {
            gameObject.SetActive(false);
            UIHome.Ins.headerView.EnableClick(true);
        });
    }

    void ClaimAction(GameObject posStartAnim, GameObject parent, string gameType)
    {
        UIHome.Ins.ShowHideLoadingPopup(true);
        StartCoroutine(Current.Ins.bonusAPI.ClaimUnclaimedReward<BaseResponse>(gameType, (res) =>
        {
            UIHome.Ins.ShowHideLoadingPopup(false);
            if (res == null || res.errorCode != 0)
            {
                Debug.Log("Error: ClaimUnclaimedReward");
                UIPopUpManager.instance.ShowErrorPopUp("Request Error : ClaimAction", res, () => ClaimAction(posStartAnim, parent, gameType));
                return;
            }
            posStartAnim.GetComponent<Button>().interactable = false;
            if (!this.checkOnlyRewardSpin)
            {
                parent.transform.DOScale(Vector3.zero, 0.2f).OnComplete(() =>
                {
                    parent.gameObject.SetActive(false);
                    parent.transform.localScale = Vector3.one;
                    if (!roulette.activeSelf && !scratch.activeSelf && !randomBox.activeSelf)
                    {
                        OnClickClose();
                        return;
                    }
                    return;
                });
            }
            Current.Ins.ReloadUserProfile(posStartAnim.transform, callBackAfterDoneEffect: () =>
            {
                parent.transform.DOScale(Vector3.zero, 0.2f).OnComplete(() =>
                {
                    parent.gameObject.SetActive(false);
                    parent.transform.localScale = Vector3.one;
                    if (!roulette.activeSelf && !scratch.activeSelf && !randomBox.activeSelf)
                    {
                        OnClickClose();
                    }
                });
            });
        }));
    }
}
