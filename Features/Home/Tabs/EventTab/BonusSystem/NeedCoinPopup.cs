using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using OutGameEnum;
using UnityEngine.Localization;
using UnityEngine.Localization.Components;
using TMPro;

public class NeedCoinPopup : MonoBehaviour
{
    public GameObject Title;
    public GameObject Content;

    public GameObject Roulette;
    public GameObject Scratch;
    public GameObject OpenBox;

    public void Show(BonusGameEnum bonusType, CoinEnum bonusFeeType)
    {
        string strGameName = string.Empty;
        switch (bonusType)
        {
            case BonusGameEnum.Roulette:
                strGameName = Roulette.GetComponent<TextMeshProUGUI>().text;
                break;
            case BonusGameEnum.Scratch:
                strGameName = Scratch.GetComponent<TextMeshProUGUI>().text;
                break;
            case BonusGameEnum.OpenBox:
                strGameName = OpenBox.GetComponent<TextMeshProUGUI>().text;
                break;
        }

        Title.GetComponent<LocalizeStringEvent>().UpdateValue("name", VariableEnum.String, strGameName);
        Content.GetComponent<LocalizeStringEvent>().UpdateValue("name", VariableEnum.String, bonusFeeType.GetStringValue());

        gameObject.SetActive(true);
        UIHome.Ins.headerView.EnableClick(false);
    }

    public void OnClose()
    {
        SoundManager.instance.PlayUIEffect("se_click1");
        gameObject.SetActive(false);
        UIHome.Ins.headerView.EnableClick(true);
    }
}
