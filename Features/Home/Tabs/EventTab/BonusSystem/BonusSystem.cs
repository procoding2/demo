using System;
using UnityEngine;
using UnityEngine.UI;
using BonusScratch;
using DanielLochner.Assets.SimpleScrollSnap;
using HomeTab;
using System.Linq;

public class BonusSystem : UIBase<BonusSystem>
{
    public GameObject rouletteBtn;
    public GameObject scratchBtn;
    public GameObject openBoxBtn;

    public GameObject rouletteTitle;
    public GameObject scratchTitle;
    public GameObject openBoxTitle;
    public SimpleScrollSnap scrollSnap;
    [SerializeField] private bool _isInitialized = false;

    public GameObject rouletteLayout;
    public GameObject scratchLayout;
    public GameObject openBoxLayout;

    private BonusSystemData _data;
    public GameObject scrollSnapView;
    public GameObject avatarHeader;

    public Button backBtn;

    public void Init(BonusSystemData data, int index, Action onBackAction)
    {
        PlayerPrefsEx.Set(PlayerPrefsConst.KEY_PREF_IS_MINI_GAME, 1);
        _data = data;
        //
        if (rouletteBtn != null)
            rouletteBtn.GetComponent<Toggle>().onValueChanged.AddListener((isChecked) =>
            {
                if (isChecked)
                {
                    SoundManager.instance.PlayUIEffect("se_click1");
                    rouletteTitle.SetActive(true);
                    scratchTitle.SetActive(false);
                    openBoxTitle.SetActive(false);
                }
            });
        if (scratchBtn != null)
            scratchBtn.GetComponent<Toggle>().onValueChanged.AddListener((isChecked) =>
            {
                if (isChecked)
                {
                    SoundManager.instance.PlayUIEffect("se_click1");
                    rouletteTitle.SetActive(false);
                    scratchTitle.SetActive(true);
                    openBoxTitle.SetActive(false);
                }
            });
        if (openBoxBtn != null)
            openBoxBtn.GetComponent<Toggle>().onValueChanged.AddListener((isChecked) =>
            {
                if (isChecked)
                {
                    SoundManager.instance.PlayUIEffect("se_click1");
                    rouletteTitle.SetActive(false);
                    scratchTitle.SetActive(false);
                    openBoxTitle.SetActive(true);
                }
            });
        if (rouletteBtn != null)
            rouletteLayout.GetComponent<BonusRoulette>().Init(data.RouletteGameFee, data.RoulettePrizes, data.RouletteFreeTurn);
        if (scratchBtn != null)
            scratchLayout.GetComponent<BonusScratch.BonusScratch>().Init(data.ScratchGameFee);
        if (openBoxBtn != null)
            openBoxLayout.GetComponent<BonusOpenBox>().Init(data.OpenBoxGameFee);
        switch (index)
        {
            case 0:
                if (rouletteBtn != null)
                    rouletteBtn.GetComponent<Toggle>().isOn = true;
                break;
            case 1:
                if (scratchBtn != null)
                    scratchBtn.GetComponent<Toggle>().isOn = true;
                break;
            case 2:
                if (openBoxBtn != null)
                    openBoxBtn.GetComponent<Toggle>().isOn = true;
                break;

        }
        // Get Reward list

        backBtn.onClick.AddListener(() =>
        {
            UIEventTab.Ins.BackgoundRoulette.SetActive(false);
            SoundManager.instance.PlayUIEffect("se_click1");
            onBackAction.Invoke();
        });
        if (_isInitialized) return;
            _isInitialized = true;
        UIEventTab.Ins.idxs.OrderByDescending(x => x).ToList().ForEach(x => Remove(x));
    }

    public void Remove(int index)
    {
        //scrollSnap.Pagination.transform.GetChild(index).SetSiblingIndex(scrollSnap.NumberOfPanels - 1);
        //if (scrollSnap.NumberOfPanels > 0)
        //{
        //    // Pagination
        //     DestroyImmediate(scrollSnap.Pagination.transform.GetChild(scrollSnap.NumberOfPanels - 1).gameObject);

        //    // Panel
        //    scrollSnap.Remove(index);
        //}

        switch(index)
        {
            case 0:
                rouletteBtn.SetActive(false);               
                break; 
            case 1:
                scratchBtn.SetActive(false);
                break; 
            case 2:
                openBoxBtn.SetActive(false);
                break;
        }
        //scrollSnap.Pagination.transform.GetChild(index).gameObject.SetActive(false);
    }

    public void DisableTabAction(bool isEnable)
    {
        //scrollSnapView.GetComponent<SimpleScrollSnap>().Toggles.ToList().ForEach(item => item.interactable = isEnable);
        avatarHeader.GetComponent<Button>().interactable = isEnable;
        backBtn.GetComponent<Button>().interactable = isEnable;
    }

    protected override void InitIns()
    {
        Ins = this;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        PlayerPrefsEx.Set(PlayerPrefsConst.KEY_PREF_IS_MINI_GAME, 0);
    }
}
