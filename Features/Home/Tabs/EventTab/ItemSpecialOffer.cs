﻿using System;
using HomeTab;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Localization.Components;
using OutGameEnum;

public class ItemSpecialOffer : MonoBehaviour
{
    public GameObject bgImage;
    public GameObject nameTxt;
    public GameObject goldPrizeImg;
    public GameObject tokenPrizeImg;
    public GameObject ticketPrizeImg;
    public GameObject coinPrizeTxt;
    public GameObject numberPlayerTxt;
    public GameObject goldFeeImg;
    public GameObject tokenFeeImg;
    public GameObject ticketFeeImg;
    public GameObject coinFeeTxt;

    public Sprite bgSolitaire;
    public Sprite bgBingo;
    public Sprite bgBuble;
    public Sprite bgBia;
    public Sprite bgPuzzle;

    void SettingLanguage()
    {
        if (nameTxt.GetComponent<TextMeshProUGUI>().text == "Tournament" ||
            nameTxt.GetComponent<TextMeshProUGUI>().text == "토너먼트" ||
            nameTxt.GetComponent<TextMeshProUGUI>().text == "Giải đấu")
        {
            LanguageEnum language = Current.Ins.Config.GetCurrentLanguage();
            switch (language)
            {
                case LanguageEnum.eng:
                    nameTxt.GetComponent<TextMeshProUGUI>().text = "Tournament";
                    break;
                case LanguageEnum.kor:
                    nameTxt.GetComponent<TextMeshProUGUI>().text = "토너먼트";
                    break;
                case LanguageEnum.vi:
                    nameTxt.GetComponent<TextMeshProUGUI>().text = "Giải đấu";
                    break;
            }
        }
    }

    private void OnEnable()
    {
        SettingLanguage();
        Current.Ins.Config.PropertyChanged += Config_PropertyChanged;
    }

    private void OnDisable()
    {
        Current.Ins.Config.PropertyChanged -= Config_PropertyChanged;
    }

    private void Config_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
    {
        if (e.PropertyName == StringConst.PROPERTY_LANGUAGE)
        {
            SettingLanguage();
        }
    }
    public void Init(MiniGameResponse.Item miniGameResponse, Action<MiniGameResponse.Item> action)
    {
        if (miniGameResponse.game == GameEnum.MiniGameEnum.Bingo.GetStringValue())
        {
            bgImage.GetComponent<Image>().sprite = bgBingo;
        }
        else if (miniGameResponse.game == GameEnum.MiniGameEnum.Solitaire.GetStringValue())
        {
            bgImage.GetComponent<Image>().sprite = bgSolitaire;
        }
        else if (miniGameResponse.game == GameEnum.MiniGameEnum.Bubble.GetStringValue())
        {
            bgImage.GetComponent<Image>().sprite = bgBuble;
        }
        else if (miniGameResponse.game == GameEnum.MiniGameEnum.EBall.GetStringValue())
        {
            bgImage.GetComponent<Image>().sprite = bgBia;
        }
        else
        {
            bgImage.GetComponent<Image>().sprite = bgPuzzle;
        }
        string modeName = "";
        switch (miniGameResponse.mode)
        {
            case GameEnum.MiniGameModeEnum.HeadToHead:
                modeName = "1 vs 1";
                break;
            case GameEnum.MiniGameModeEnum.OneToMany:
                LanguageEnum language = Current.Ins.Config.GetCurrentLanguage();
                switch (language)
                {
                    case LanguageEnum.eng:
                        modeName = "Tournament";
                        break;
                    case LanguageEnum.kor:
                        modeName = "토너먼트";
                        break;
                    case LanguageEnum.vi:
                        modeName = "Giải đấu";
                        break;
                }
                break;
            case GameEnum.MiniGameModeEnum.KnockOut:
                modeName = GameEnum.MiniGameModeEnum.KnockOut.GetStringValue();
                break;
        }
        nameTxt.GetComponent<TextMeshProUGUI>().text = modeName;

        int cointPrize = 0;
        if (miniGameResponse.tokenPrize > 0)
        {
            tokenPrizeImg.SetActive(true);
            ticketPrizeImg.SetActive(false);
            goldPrizeImg.SetActive(false);
            cointPrize = miniGameResponse.tokenPrize;
        }
        else if (miniGameResponse.ticketPrize > 0)
        {
            tokenPrizeImg.SetActive(false);
            ticketPrizeImg.SetActive(true);
            goldPrizeImg.SetActive(false);
            cointPrize = miniGameResponse.ticketPrize;
        }
        else
        {
            tokenPrizeImg.SetActive(false);
            ticketPrizeImg.SetActive(false);
            goldPrizeImg.SetActive(true);
            cointPrize = miniGameResponse.goldPrize;
        }

        coinPrizeTxt.GetComponent<TextMeshProUGUI>().text = cointPrize.ToString();

        numberPlayerTxt.GetComponent<LocalizeStringEvent>().UpdateValue("n", OutGameEnum.VariableEnum.String, miniGameResponse.quantity.ToString());

        int cointFee = 0;
        if (miniGameResponse.tokenFee > 0)
        {
            tokenFeeImg.SetActive(true);
            ticketFeeImg.SetActive(false);
            goldFeeImg.SetActive(false);
            cointFee = miniGameResponse.tokenFee;
        }
        else if (miniGameResponse.ticketFee > 0)
        {
            tokenFeeImg.SetActive(false);
            ticketFeeImg.SetActive(true);
            goldFeeImg.SetActive(false);
            cointFee = miniGameResponse.ticketFee;
        }
        else
        {
            tokenFeeImg.SetActive(false);
            ticketFeeImg.SetActive(false);
            goldFeeImg.SetActive(true);
            cointFee = miniGameResponse.goldFee;
        }
        coinFeeTxt.GetComponent<TextMeshProUGUI>().text = cointFee.ToString();
        gameObject.transform.GetComponent<Button>().onClickWithHCSound(() =>
        {
            action(miniGameResponse);
        }
        );
    }
}

