using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using GameEnum;
using System;
using OutGameEnum;
using UnityEngine.Localization.Components;
using TMPro;

namespace HomeTab
{
    public class GameModeItem : MonoBehaviour
    {


        public Image gameBG;
        //public Sprite[] gameSprites;


        public Button PlayBtn;
        public TextMeshProUGUI PlayerQuantity;

        public TextMeshProUGUI EnterFee;
        public GameObject[] EnterFeeIcons;

        public TextMeshProUGUI PrizePool;
        public GameObject[] PrizePoolIcons;

        public TextMeshProUGUI gameNameTxt;




        public void Init(MiniGameEnum miniGame, GameModeData.GameModeItemData data, Action<CoinEnum> errorCoinAction)
        {
            GetComponent<Button>().interactable = true;
            gameNameTxt.GetComponent<TextMeshProUGUI>().text = data.ItemName;



            for (int i = 0; i < EnterFeeIcons.Length; i++)
            {
                EnterFeeIcons[i].SetActive(false);
            }
            for (int i = 0; i < PrizePoolIcons.Length; i++)
            {
                PrizePoolIcons[i].SetActive(false);
            }


            //if (gameBG)
            //{
            //    if (miniGame == MiniGameEnum.Bingo)
            //    {
            //        gameBG.sprite = gameSprites[0];
            //    }
            //    else if (miniGame == MiniGameEnum.Bubble)
            //    {
            //        gameBG.sprite = gameSprites[1];
            //    }
            //    else if (miniGame == MiniGameEnum.Solitaire)
            //    {
            //        gameBG.sprite = gameSprites[2];
            //    }
            //}



            PlayBtn.onClick.RemoveAllListeners();
            PlayBtn.onClickWithHCSound(() =>
            {
                SoundManager.instance.PlayUIEffect("se_click1");

                if (data.GoldFee > Current.Ins.player.GoldCoin)
                {
                    errorCoinAction.Invoke(CoinEnum.Gold);
                    return;
                }

                if (data.TokenFee > Current.Ins.player.HCTokenCoin)
                {
                    errorCoinAction.Invoke(CoinEnum.Token);
                    return;
                }

                if (data.TicketFee > Current.Ins.player.TicketCoin)
                {
                    errorCoinAction.Invoke(CoinEnum.Ticket);
                    return;
                }


                GameObject entryPop = UIPopUpManager.instance.ShowPopUpFileName("PopUp_EntryFee");
                if (entryPop.TryGetComponent<UIPopUpEntryFee>(out UIPopUpEntryFee pop))
                {
                    var feeData = new ConfirmFeePopupData(data.GoldFee, data.TokenFee, data.TicketFee);
                    pop.ShowPopUpInfo(feeData, () => StartGame(miniGame, data.Mode, data.Id, feeData));
                }
                //var confirmFeePopup = GameObject.FindGameObjectWithTag("ConfirmFeePopup").GetComponent<ConfirmFeePopup>();
                
                //confirmFeePopup.Init(
                //    data: feeData,
                //    confirmAction: () =>
                //    {
                //        ;
                //    });            
            });

            LoadPrize(data);

            PlayerQuantity.GetComponent<LocalizeStringEvent>().UpdateValue("n", VariableEnum.String, data.Quantity.ToString());

            if (data.GoldFee > 0)
            {
                EnterFeeIcons[0].SetActive(true);
                EnterFee.text = data.GoldFee.ToString();
            }
            else if (data.TokenFee > 0)
            {
                EnterFeeIcons[1].SetActive(true);
                EnterFee.text = data.TokenFee.ToString();
            }
            else if (data.TicketFee > 0)
            {
                EnterFeeIcons[2].SetActive(true);
                EnterFee.text = data.TicketFee.ToString();
            }
        }

        private void StartGame(MiniGameEnum miniGame, MiniGameModeEnum gameMode, string gameId, ConfirmFeePopupData feeData)
        {
            switch (miniGame)
            {
                case MiniGameEnum.Bingo:
                    UIBingo.InitStaticVariable(gameMode, gameId, feeData);
                    SceneHelper.LoadScene(SceneEnum.Bingo);
                    break;
                case MiniGameEnum.Bubble:
                    UIBubble.InitStaticVariable(gameMode, gameId, feeData);
                    SceneHelper.LoadScene(SceneEnum.Bubble);
                    break;
                case MiniGameEnum.Solitaire:
                    UISolitaire.InitStaticVariable(gameMode, gameId, feeData);
                    SceneHelper.LoadScene(SceneEnum.Solitaire);
                    break;
                default: break;
            }
        }

        private void LoadPrize(GameModeData.GameModeItemData data)
        {
            if (data.GoldPrizePool > 0)
            {
                PrizePoolIcons[0].SetActive(true);
                PrizePool.text = $"{data.GoldPrizePool}";
            }
            else if (data.TokenPrizePool > 0)
            {
                PrizePoolIcons[1].SetActive(true);
                PrizePool.text = $"{data.TokenPrizePool}";
            } 
            else if (data.TicketPrizePool > 0)
            {
                PrizePoolIcons[2].SetActive(true);
                PrizePool.text = $"{data.TicketPrizePool}";
            }
            else
            {
                PrizePoolIcons[3].SetActive(true);
                PrizePool.GetComponent<TextMeshProUGUI>().text = $"x100";
            }
        }
    }
}

