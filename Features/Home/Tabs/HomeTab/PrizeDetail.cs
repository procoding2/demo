using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Localization.Components;
using OutGameEnum;
using GameEnum;

namespace HomeTab
{
    public class PrizeDetail : UIBase<PrizeDetail>
    {
        protected override void InitIns() => Ins = this;

        public Button backBtn;
        public Button JoinBtn;

        public GameObject Title;
        public GameObject Round;
        public GameObject Quantity;

        //public GameObject TotalGoldItem;
        //public GameObject TotalTokenItem;
        //public GameObject TotalTicketItem;

        public GameObject TotalRewardGold;
        public GameObject TotalRewardToken;
        public GameObject TotalRewardTicket;

        public GameObject PrizeLayout;
        public GameObject PrizeItemPrefab;

        public GameObject HeadToHeadRule;
        public GameObject KnockoutRule;
        public GameObject RoundRobinRule;
        public GameObject OneVsManyRule;

        public GameObject bingoIcon;
        public GameObject bubbleIcon;
        public GameObject solitaireIcon;
        public GameObject puzzleIcon;
        public GameObject biAIcon;

        public void Init(PrizeDetailData data, Action onBackAction, Action<CoinEnum> errorCoinAction)
        {
            backBtn.onClick.RemoveAllListeners();
            backBtn.onClick.AddListener(() =>
            {
                SoundManager.instance.PlayUIEffect("se_click1");
                gameObject.SetActive(false);
                onBackAction.Invoke();
            });

            JoinBtn.onClick.RemoveAllListeners();
            JoinBtn.onClick.AddListener(() =>
            {
                SoundManager.instance.PlayUIEffect("se_click1");

                if (data.GoldFee > Current.Ins.player.GoldCoin)
                {
                    errorCoinAction.Invoke(CoinEnum.Gold);
                    return;
                }

                if (data.TokenFee > Current.Ins.player.HCTokenCoin)
                {
                    errorCoinAction.Invoke(CoinEnum.Token);
                    return;
                }

                if (data.TicketFee > Current.Ins.player.TicketCoin)
                {
                    errorCoinAction.Invoke(CoinEnum.Ticket);
                    return;
                }

                var confirmFeePopup = GameObject.FindGameObjectWithTag("ConfirmFeePopup").GetComponent<ConfirmFeePopup>();
                var feeData = new ConfirmFeePopupData(data.GoldFee, data.TokenFee, data.TicketFee);
                confirmFeePopup.Init(
                data: feeData,
                confirmAction: () =>
                {
                    StartGame(data.Game, data.Mode, data.Id, feeData);
                });
            });

            switch (data.Mode)
            {
                case MiniGameModeEnum.HeadToHead:
                    HeadToHeadRule.SetActive(true);
                    OneVsManyRule.SetActive(false);
                    KnockoutRule.SetActive(false);
                    RoundRobinRule.SetActive(false);
                    break;
                case MiniGameModeEnum.OneToMany:
                    HeadToHeadRule.SetActive(false);
                    OneVsManyRule.SetActive(true);
                    KnockoutRule.SetActive(false);
                    RoundRobinRule.SetActive(false);
                    break;
                case MiniGameModeEnum.KnockOut:
                    HeadToHeadRule.SetActive(false);
                    OneVsManyRule.SetActive(false);
                    KnockoutRule.SetActive(true);
                    RoundRobinRule.SetActive(false);
                    break;
                case MiniGameModeEnum.RoundRobin:
                    HeadToHeadRule.SetActive(false);
                    OneVsManyRule.SetActive(false);
                    KnockoutRule.SetActive(false);
                    RoundRobinRule.SetActive(true);
                    break;
            }

            gameObject.SetActive(true);
            Title.GetComponent<TextMeshProUGUI>().text = data.Title;

            //Round.GetComponent<LocalizeStringEvent>().UpdateValue("n", VariableEnum.String, data.Round);
            Quantity.GetComponent<LocalizeStringEvent>().UpdateValue("n", VariableEnum.String, data.Quantity);

            if (data.TotalGold > 0)
            {
                TotalRewardGold.GetComponent<TextMeshProUGUI>().text = StringHelper.NumberFormatter(data.TotalGold);
                TotalRewardGold.SetActive(true);
            }
            else
            {
                TotalRewardGold.SetActive(false);
            }

            if (data.TotalToken > 0)
            {
                TotalRewardToken.GetComponent<TextMeshProUGUI>().text = StringHelper.NumberFormatter(data.TotalToken);
                TotalRewardToken.SetActive(true);
            }
            else
            {
                TotalRewardToken.SetActive(false);
            }

            if (data.TotalTicket > 0)
            {
                TotalRewardTicket.GetComponent<TextMeshProUGUI>().text = StringHelper.NumberFormatter(data.TotalTicket);
                TotalRewardTicket.SetActive(true);
            }
            else
            {
                TotalRewardTicket.SetActive(false);
            }
            
            PrizeLayout.transform.RemoveAllChild();
 
            foreach (var prizeItem in data.Items)
            {
                var prz = Instantiate(PrizeItemPrefab, PrizeLayout.transform, false);
                prz.GetComponent<PrizeDetailItem>().Init(prizeItem);
            }

            bingoIcon.SetActive(data.Game == MiniGameEnum.Bingo);
            bubbleIcon.SetActive(data.Game == MiniGameEnum.Bubble);
            solitaireIcon.SetActive(data.Game == MiniGameEnum.Solitaire);
            //puzzleIcon.SetActive(data.Game == MiniGameEnum.Puzzle);
            //biAIcon.SetActive(data.Game == MiniGameEnum.EBall);
        }

        private void StartGame(MiniGameEnum miniGame, MiniGameModeEnum gameMode, string gameId, ConfirmFeePopupData feeData)
        {
            switch (miniGame)
            {
                case MiniGameEnum.Bingo:
                    UIBingo.InitStaticVariable(gameMode, gameId, feeData);
                    SceneHelper.LoadScene(SceneEnum.Bingo);
                    break;
                case MiniGameEnum.Bubble:
                    UIBubble.InitStaticVariable(gameMode, gameId, feeData);
                    SceneHelper.LoadScene(SceneEnum.Bubble);
                    break;
                case MiniGameEnum.Solitaire:
                    UISolitaire.InitStaticVariable(gameMode, gameId, feeData);
                    SceneHelper.LoadScene(SceneEnum.Solitaire);
                    break;
                default: break;
            }
        }
    }
}
