using System;
using DG.Tweening;
using OutGameEnum;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace HomeTab
{
    public class NotificationPopup : MonoBehaviour
    {
        public GameObject notificationLayout;
        public GameObject notificationItemPrefab;
        public GameObject scrollBar;
        public GameObject closeBtn;
        public GameObject outAreaBtn;

        [Header("ExtraContent")]
        public GameObject extraContent;
        public GameObject closeBtnExtraContent;
        public GameObject titleExtraContent;
        public GameObject contentExtraContent;

        private bool _isLoading;
        private int _nextPage = 1;

        private void Start()
        {
            closeBtn.GetComponent<Button>().onClick.AddListener(() =>
            {
                SoundManager.instance.PlayUIEffect("se_click1");
                transform.DOScale(Vector3.zero, 0.2f).SetEase(Ease.OutElastic).OnComplete(() =>
                {
                    gameObject.SetActive(false);
                    UIHome.Ins.headerView.EnableClick(true);
                });
            });

            outAreaBtn.GetComponent<Button>().onClick.AddListener(() =>
            {
                SoundManager.instance.PlayUIEffect("se_click1");
                transform.DOScale(Vector3.zero, 0.2f).SetEase(Ease.OutElastic).OnComplete(() =>
                {
                    gameObject.SetActive(false);
                    UIHome.Ins.headerView.EnableClick(true);
                });
            });

            closeBtnExtraContent.GetComponent<Button>().onClick.AddListener(() =>
            {
                SoundManager.instance.PlayUIEffect("se_click1");
                extraContent.transform.DOScale(Vector3.zero, 0.2f).SetEase(Ease.OutElastic).OnComplete(() =>
                {
                    extraContent.SetActive(false);
                });
            });

            scrollBar.GetComponent<ScrollRect>().onValueChanged.AddListener((vector) =>
            {
                if (_isLoading || _nextPage == 0) return;

                if (scrollBar.GetComponent<ScrollRect>().verticalNormalizedPosition <= 0.05f)
                {
                    getListNoti();
                }
            });
        }

        public void Init(NotificationData data, int nextPage)
        {
            gameObject.SetActive(true);
            notificationLayout.transform.RemoveAllChild();
            _nextPage = nextPage;
            getListNoti(data);
        }

        private void getListNoti(NotificationData data = null)
        {
            Action<NotificationData, bool> action = (data, isReload) =>
            {
                foreach (var item in data.Items)
                {
                    var notiItem = Instantiate(notificationItemPrefab, notificationLayout.transform, false);
                    notiItem.GetComponent<NotificationItem>().Init(item.Title, item.Content, item.TimeAgo);
                    notiItem.GetComponent<Button>().onClick.AddListener(() =>
                    {
                        SoundManager.instance.PlayUIEffect("se_click1");
                        OnClickItem(item);
                    });

                    if (!isReload) notiItem.transform.localScale = Vector3.zero;
                }

                if (!isReload)
                {
                    transform.localScale = Vector3.zero;
                    transform.DOScale(Vector3.one, 0.1f).SetEase(Ease.OutElastic).OnComplete(() => AnimChildren());
                }               
            };

            if (data != null)
            {
                action.Invoke(data, false);
                UIHome.Ins.headerView.EnableClick(false);
                return;
            }
            
            UIHome.Ins.ShowHideLoadingPopup(true);
            StartCoroutine(Current.Ins.userAPI.GetNotification(_nextPage, limit: 20, (res) =>
            {
                UIHome.Ins.ShowHideLoadingPopup(false);
                if (res == null || res.errorCode != 0)
                {
                    UIPopUpManager.instance.ShowErrorPopUp("Request Error : getListNoti", res, () => getListNoti());
                    Debug.Log("Error: OpenDailyMission");
                    return;
                }

                var notificationData = new NotificationData();
                notificationData.Items = new List<NotificationItemData>();

                foreach (var resItemData in res.data.datas)
                {
                    var item = new NotificationItemData();
                    notificationData.Items.Add(item);
                    item.Id = resItemData.id.ToString();
                    item.IsRead = resItemData.isRead;
                    item.TimeAgo = res.currentTime - resItemData.createdAt;

                    switch (resItemData.type)
                    {
                        case NotificationTypeEnum.Normal:
                            item.Title = resItemData.title;
                            item.Content = resItemData.content;
                            break;
                        case NotificationTypeEnum.NewEvent:
                            item.Title = resItemData.eventDetail.name;
                            item.Content = resItemData.eventDetail.content;
                            break;
                        case NotificationTypeEnum.NewDailyMission:
                            item.Title = resItemData.mission.name;
                            item.Content = resItemData.mission.description;
                            break;
                    }
                }

                if (_nextPage == 1) notificationLayout.transform.RemoveAllChild();
                _isLoading = false;

                _nextPage = res.data.nextPage;

                action.Invoke(notificationData, true);
            }));
        }

        void AnimChildren()
        {
            for (int j = 0; j < notificationLayout.transform.childCount; j++)
            {
                notificationLayout.transform.GetChild(j).DOScale(Vector3.one, 0.3f + (j * 0.15f)).SetEase(Ease.OutElastic);
            }
            scrollBar.GetComponent<ScrollRect>().verticalNormalizedPosition = 1f;
        }

        private void OnClickItem(NotificationItemData item)
        {
            if (!item.IsRead)
            {
                StartCoroutine(Current.Ins.userAPI.MarkReadUserNotification(item.Id, (res) =>
                {
                    var numberNoti = PlayerPrefsEx.Get(PlayerPrefsConst.NUMBER_NOTIFICATION, 0);
                    if (numberNoti == 0) return;
                    PlayerPrefsEx.Set(PlayerPrefsConst.NUMBER_NOTIFICATION, numberNoti - 1);
                    SideBarMenu.Ins.setupViewDotNoti();
                }));
            }

            titleExtraContent.GetComponent<TextMeshProUGUI>().text = item.Title;
            contentExtraContent.GetComponent<Text>().text = item.Content;
            extraContent.transform.localScale = Vector3.zero;
            extraContent.SetActive(true);
            extraContent.transform.DOScale(Vector3.one, 0.25f).SetEase(Ease.OutElastic);
        }

        private void OnDisable()
        {
            for (int j = 0; j < notificationLayout.transform.childCount; j++)
            {
                int currentIndex = j;
                DOTween.Kill(notificationLayout.transform.GetChild(currentIndex));
            }
        }
    }
}
