using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using OutGameEnum;
using TMPro;
using DG.Tweening;

namespace HomeTab
{
    public class DailyRewardItem : MonoBehaviour
    {
        public GameObject TodayTitle;
        public GameObject TodayBackground;

        public GameObject NextDayTitle;
        public GameObject NextDayBackground;

        public GameObject bgComplete;
        [Header("Reward_1")]
        public GameObject Reward;
        public GameObject IconGold;
        public GameObject IconToken;
        public GameObject IconTicket;
        public GameObject X2XP;
        public GameObject Value;

        [Header("Reward_2")]
        public GameObject Reward_2;
        public GameObject IconGold_2;
        public GameObject IconToken_2;
        public GameObject IconTicket_2;
        public GameObject X2XP_2;

        [Header("Reward_3")]
        public GameObject Reward_3;
        public GameObject IconGold_3;
        public GameObject IconToken_3;
        public GameObject IconTicket_3;
        public GameObject X2XP_3;
        public Button Button;

        Tween tween;

        public void TodayReal()
        {
            this.TodayTitle.SetActive(true);
            this.NextDayTitle.SetActive(false);
            if(TodayBackground != null)
                TodayBackground.SetActive(true);
            if(NextDayBackground != null)
                NextDayBackground.SetActive(false);
        }

        void CheckRewardId(bool isClaimedReward, string rewardId)
        {
            if (rewardId != "0")
            {              
                TodayTitle.SetActive(true);
                if (TodayBackground != null) TodayBackground.SetActive(true);
                Button.onClick.RemoveAllListeners();
                Button.onClick.AddListener(() =>
                {
                    SoundManager.instance.PlayUIEffect("se_click1");
                    ClaimReward(rewardId);
                });
            }
            else
            {
                NextDayTitle.SetActive(true);
                if(NextDayBackground != null)
                    NextDayBackground.SetActive(true);
            }

            if (isClaimedReward)
            {
                TodayTitle.SetActive(false);
                bgComplete.SetActive(true);
                NextDayTitle.SetActive(true);
                if (NextDayBackground != null)
                    NextDayBackground.SetActive(true);
            }
        }

        public void InitDayFrom1To6(DailyPrizeTypeEnum rewardType, int value, bool isClaimedReward, string rewardId)
        {
            CheckRewardId(isClaimedReward ,rewardId);

            IconGold.SetActive(false);
            IconToken.SetActive(false);
            IconTicket.SetActive(false);
            X2XP.SetActive(false);

            switch (rewardType)
            {
                case DailyPrizeTypeEnum.Gold:
                    IconGold.SetActive(true);
                    break;
                case DailyPrizeTypeEnum.HCToken:
                    IconToken.SetActive(true);
                    break;
                case DailyPrizeTypeEnum.Ticket:
                    IconTicket.SetActive(true);
                    break;
                case DailyPrizeTypeEnum.X2XP:
                    X2XP.SetActive(true);
                    break;
            }

            Value.GetComponent<Text>().text = value.ToString();

            this.Reward.SetActive(true);
        }

        public void InitDayFrom1To6Form2(DailyPrizeTypeEnum rewardType1, DailyPrizeTypeEnum rewardType2, int value1, int value2, bool isClaimedReward, string rewardId)
        {
            CheckRewardId(isClaimedReward, rewardId);

            switch (rewardType1)
            {
                case DailyPrizeTypeEnum.Gold:
                    IconGold_2.SetActive(true);
                    IconGold_2.GetComponentInChildren<TMP_Text>().text = value1.ToString();
                    break;
                case DailyPrizeTypeEnum.HCToken:
                    IconToken_2.SetActive(true);
                    IconToken_2.GetComponentInChildren<TMP_Text>().text = value1.ToString();
                    break;
                case DailyPrizeTypeEnum.Ticket:
                    IconTicket_2.SetActive(true);
                    IconTicket_2.GetComponentInChildren<TMP_Text>().text = value1.ToString();
                    break;
                case DailyPrizeTypeEnum.X2XP:
                    X2XP_2.SetActive(true);
                    break;
            }

            switch (rewardType2)
            {
                case DailyPrizeTypeEnum.Gold:
                    IconGold_2.SetActive(true);
                    IconGold_2.GetComponentInChildren<TMP_Text>().text = value2.ToString();
                    break;
                case DailyPrizeTypeEnum.HCToken:
                    IconToken_2.SetActive(true);
                    IconToken_2.GetComponentInChildren<TMP_Text>().text = value2.ToString();
                    break;
                case DailyPrizeTypeEnum.Ticket:
                    IconTicket_2.SetActive(true);
                    IconTicket_2.GetComponentInChildren<TMP_Text>().text = value2.ToString();
                    break;
                case DailyPrizeTypeEnum.X2XP:
                    X2XP_2.SetActive(true);
                    break;
            }

            this.Reward_2.SetActive(true);
        }

        public void InitDayFrom1To6Form3(int Gold, int Token, int Ticket, bool isClaimedReward, string rewardId)
        {
            CheckRewardId(isClaimedReward, rewardId);

            IconGold_3.GetComponentInChildren<TMP_Text>().text = Gold.ToString();
            IconToken_3.GetComponentInChildren<TMP_Text>().text = Token.ToString();
            IconTicket_3.GetComponentInChildren<TMP_Text>().text = Ticket.ToString();

            this.Reward_3.SetActive(true);
        }

        //
        [Header("Day7")]
        public GameObject Day7_1;
        public GameObject TokenValue;
        public GameObject GoldValue;
        public GameObject TicketValue;

        [Header("Day7_2")]
        public GameObject Day7_2;
        public GameObject TokenValue_2;
        public GameObject GoldValue_2;
        public GameObject TicketValue_2;
        public GameObject Plus;

        [Header("Day7_3")]
        public GameObject Day7_3;
        public GameObject TokenValue_3;
        public GameObject GoldValue_3;
        public GameObject TicketValue_3;
        public void InitDay7(int tokenValue, int goldValue, int ticketValue, bool isClaimedReward, string rewardId)
        {
            CheckRewardId(isClaimedReward, rewardId);

            TokenValue.GetComponent<TextMeshProUGUI>().text = tokenValue.ToString();
            GoldValue.GetComponent<TextMeshProUGUI>().text = goldValue.ToString();
            TicketValue.GetComponent<TextMeshProUGUI>().text = ticketValue.ToString();

            this.Day7_1.SetActive(true);
        }

        public void InitDay7_2(DailyPrizeTypeEnum rewardType1, DailyPrizeTypeEnum rewardType2, int value1, int value2, bool isClaimedReward, string rewardId)
        {
            CheckRewardId(isClaimedReward, rewardId);

            switch (rewardType1)
            {
                case DailyPrizeTypeEnum.Gold:
                    GoldValue_2.SetActive(true);
                    GoldValue_2.GetComponentInChildren<TMP_Text>().text = value1.ToString();
                    break;
                case DailyPrizeTypeEnum.HCToken:
                    TokenValue_2.SetActive(true);
                    TokenValue_2.GetComponentInChildren<TMP_Text>().text = value1.ToString();
                    break;
                case DailyPrizeTypeEnum.Ticket:
                    TicketValue_2.SetActive(true);
                    TicketValue_2.GetComponentInChildren<TMP_Text>().text = value1.ToString();
                    break;
            }

            switch (rewardType2)
            {
                case DailyPrizeTypeEnum.Gold:
                    GoldValue_2.SetActive(true);
                    GoldValue_2.GetComponentInChildren<TMP_Text>().text = value2.ToString();
                    break;
                case DailyPrizeTypeEnum.HCToken:
                    TokenValue_2.SetActive(true);
                    TokenValue_2.GetComponentInChildren<TMP_Text>().text = value2.ToString();
                    break;
                case DailyPrizeTypeEnum.Ticket:
                    TicketValue_2.SetActive(true);
                    TicketValue_2.GetComponentInChildren<TMP_Text>().text = value2.ToString();
                    break;
            }

            if(TokenValue_2.activeSelf == true) Plus.transform.SetSiblingIndex(1);
            else Plus.transform.SetSiblingIndex(2);

            this.Day7_2.SetActive(true);
        }

        public void InitDay7_3(DailyPrizeTypeEnum rewardType, int value, bool isClaimedReward, string rewardId)
        {
            CheckRewardId(isClaimedReward, rewardId);

            switch (rewardType)
            {
                case DailyPrizeTypeEnum.Gold:
                    GoldValue_3.SetActive(true);
                    GoldValue_3.GetComponentInChildren<TMP_Text>().text = value.ToString();
                    break;
                case DailyPrizeTypeEnum.HCToken:
                    TokenValue_3.SetActive(true);
                    TokenValue_3.GetComponentInChildren<TMP_Text>().text = value.ToString();
                    break;
                case DailyPrizeTypeEnum.Ticket:
                    TicketValue_3.SetActive(true);
                    TicketValue_3.GetComponentInChildren<TMP_Text>().text = value.ToString();
                    break;
            }

            this.Day7_3.SetActive(true);
        }

        private void ClaimReward(string id)
        {
            UIHome.Ins.ShowHideLoadingPopup(true);
            StartCoroutine(Current.Ins.userAPI.ClaimDailyReward(id, (res) =>
            {
                UIHome.Ins.ShowHideLoadingPopup(false);
                if (res == null || res.errorCode != 0)
                {
                    UIPopUpManager.instance.ShowErrorPopUp("Request Error : ClaimReward", res, () => ClaimReward(id));
                    return;
                }

                tween.Kill();
                transform.localScale = Vector3.one;
                Button.interactable = false;
                bgComplete.SetActive(true);

                Current.Ins.ReloadUserProfile(gameObject.transform);
            }));
        }

        public void AnimCanClaim()
        {
            if (TodayTitle.activeSelf == true && bgComplete.activeSelf == false)
            {
                tween = transform.DOScale(Vector3.one * 1.05f, 0.5f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
            }
        }
    }
}

