using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Localization.Components;
using OutGameEnum;
using TMPro;

namespace HomeTab
{
    public class NotificationItem : MonoBehaviour
    {
        public GameObject Title;
        public GameObject Content;

        public GameObject Time_Minutes;
        public GameObject Time_Hours;
        public GameObject Time_Days;
        public GameObject Time_Weeeks;
        public GameObject Time_Months;
        public GameObject Time_Years;

        /// <summary>
        /// Init
        /// </summary>
        /// <param name="title">content</param>
        /// <param name="content">content</param>
        /// <param name="time">seconds</param>
        public void Init(string title, string content, double time)
        {
            Title.GetComponent<Text>().text = title;
            Content.GetComponent<TextMeshProUGUI>().text = content;

            double minutes = time / 60f;

            if (minutes <= 60)
            {
                Time_Minutes.SetActive(true);
                Time_Minutes.GetComponent<LocalizeStringEvent>()
                    .UpdateValue("n", VariableEnum.Int, (int)minutes);
                return;
            }

            double hours = minutes / 60;
            if (hours <= 24)
            {
                Time_Hours.SetActive(true);
                Time_Hours.GetComponent<LocalizeStringEvent>()
                    .UpdateValue("n", VariableEnum.Int, (int)hours);
                return;
            }

            double days = hours / 24;
            if (days <= 7)
            {
                Time_Days.SetActive(true);
                Time_Days.GetComponent<LocalizeStringEvent>()
                    .UpdateValue("n", VariableEnum.Int, (int)days);
                return;
            }

            double weeks = days / 7;
            if (weeks <= 4)
            {
                Time_Weeeks.SetActive(true);
                Time_Weeeks.GetComponent<LocalizeStringEvent>()
                    .UpdateValue("n", VariableEnum.Int, (int)weeks);
                return;
            }

            double months = weeks / 4;
            if (months <= 12)
            {
                Time_Months.SetActive(true);
                Time_Months.GetComponent<LocalizeStringEvent>()
                    .UpdateValue("n", VariableEnum.Int, (int)months);
                return;
            }

            double years = months / 12;
            Time_Years.SetActive(true);
            Time_Years.GetComponent<LocalizeStringEvent>()
                    .UpdateValue("n", VariableEnum.Int, (int)years);
        }
    }
}

