using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using GameEnum;

public class WarningLevelPopup : MonoBehaviour
{
    public GameObject closeButton;
    public GameObject contentRange;

    public void Start()
    {
        closeButton.GetComponent<Button>().onClickWithHCSound(() =>
        {
            IsShow(false);
        });
    }
    public void IsShow(bool isShow, TypeLevelEnum typeLevel = TypeLevelEnum.Range)
    {
        if (isShow)
        {
            contentRange.SetActive(typeLevel == TypeLevelEnum.Range ? true : false);
        }

        gameObject.SetActive(isShow);

        UIHome.Ins.headerView.EnableClick(!isShow);
    }
}
