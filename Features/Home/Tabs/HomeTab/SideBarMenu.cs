using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using OutGameEnum;
using TMPro;
using UnityEngine;
using UnityEngine.Localization.Components;
using UnityEngine.UI;

namespace HomeTab
{
    public class SideBarMenu : UIBase<SideBarMenu>
    {

        public GameObject dailyMission;
        public GameObject dotDailyMission;
        public GameObject numberDailyMissionTxt;
        public GameObject dailyReward;
        public GameObject notification;
        public GameObject dotNoti;
        public GameObject dotNotiReward;
        public GameObject numberNotiTxt;

        public GameObject goldGift;
        public GameObject goldGiftTimeRemain;

        private double? _goldGiftTimeRemain = null;
        private bool _isCallingAPI = false;

        protected override void InitIns() => Ins = this;

        public void Init(Action dailyMissionAction,
                         Action dailyRewardAction,
                         Action notificationAction)
        {

            setupViewDotDaily();
            setupViewDotNoti();

            dailyMission.GetComponent<Button>().onClickWithHCSound(() =>
            {
                dailyMissionAction.Invoke();
            });

            dailyReward.GetComponent<Button>().onClickWithHCSound(() =>
            {
                SoundManager.instance.PlayUIEffect("se_click1");
                dailyRewardAction.Invoke();
            });

            notification.GetComponent<Button>().onClickWithHCSound(() =>
            {
                SoundManager.instance.PlayUIEffect("se_click1");
                notificationAction.Invoke();
            });

            dotNotiReward.SetActive(Current.Ins.player.DailyRewardNotClaim > 0);

            GetGoldGift();
            FirebaseManager.Instance().PropertyChanged += SideBarMenu_PropertyChanged;

        }

        private void SideBarMenu_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == StringConst.KEY_HAVE_NOTI)
            {
                setupViewDotNoti();
            }
        }

        //show dot daily
        public void setupViewDotDaily()
        {
            var numberDaily = PlayerPrefsEx.Get(PlayerPrefsConst.NUMBER_DAILY_MISSION, 0);
            dotDailyMission.SetActive(numberDaily > 0);
            numberDailyMissionTxt.GetComponent<TextMeshProUGUI>().text = numberDaily.ToString();
        }

        //show dot noti
        public void setupViewDotNoti()
        {
            var numberNoti = PlayerPrefsEx.Get(PlayerPrefsConst.NUMBER_NOTIFICATION, 0);
            dotNoti.SetActive(numberNoti > 0);
            numberNotiTxt.GetComponent<TextMeshProUGUI>().text = numberNoti.ToString();
        }

        private void Ins_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {

            if (e.PropertyName == StringConst.SOCKET_DAILY_MISSION)
            {
                setupViewDotDaily();
            }

            if (e.PropertyName == StringConst.PROPERTY_USER_INFOR)
            {
                dotNotiReward.SetActive(Current.Ins.player.DailyRewardNotClaim > 0);
            }
        }

        override protected void Update()
        {
            if (_goldGiftTimeRemain != null && _goldGiftTimeRemain.Value > 0 && !_isCallingAPI)
            {
                if (_goldGiftTimeRemain.Value >= Time.deltaTime)
                {
                    _goldGiftTimeRemain -= Time.deltaTime;
                    string timeStr = (TimeSpan.FromSeconds(_goldGiftTimeRemain.Value)).ToString(@"hh'h:'mm'm'");
                    goldGiftTimeRemain.GetComponent<TextMeshProUGUI>().text = timeStr;
                }
                else
                {
                    goldGiftTimeRemain.GetComponent<TextMeshProUGUI>().text = string.Empty;
                    GetGoldGift();
                }
            }
        }


        private void GetGoldGift()
        {
            goldGift.GetComponent<Button>().interactable = false;
            _isCallingAPI = true;
            UIHome.Ins.ShowHideLoadingPopup(true);
            StartCoroutine(Current.Ins.userAPI.GetGoldGift((res) =>
            {
                UIHome.Ins.ShowHideLoadingPopup(false);
                if (res == null || res.errorCode != 0)
                {
                    UIPopUpManager.instance.ShowErrorPopUp("Request Error : GetGoldGift", res, GetGoldGift);
                    return;
                }

                _isCallingAPI = false;

                _goldGiftTimeRemain = res.data.timeRemain;

                if (_goldGiftTimeRemain == 0)
                {
                    goldGift.GetComponent<Button>().interactable = true;
                    goldGift.GetComponent<Animator>().SetBool("CanReceive", true);
                }
                else
                {
                    goldGift.GetComponent<Animator>().SetBool("CanReceive", false);
                }

                goldGift.GetComponent<Button>().onClick.RemoveAllListeners();
                goldGift.GetComponent<Button>().onClickWithHCSound(() =>
                {
                    ClaimGoldGift(res.data.id.ToString());
                });
            }));
        }

        private void ClaimGoldGift(string id)
        {
            UIHome.Ins.ShowHideLoadingPopup(true);
            StartCoroutine(Current.Ins.userAPI.ClaimGoldGift(id, (res) =>
            {
                UIHome.Ins.ShowHideLoadingPopup(false);
                if (res == null || res.errorCode != 0)
                {
                    UIPopUpManager.instance.ShowErrorPopUp("Request Error : ClaimGoldGift", res, () => ClaimGoldGift(id));
                    return;
                }

                Current.Ins.ReloadUserProfile(goldGift.transform);
                GetGoldGift();
            }));
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            Current.Ins.PropertyChanged -= Ins_PropertyChanged;
            FirebaseManager.Instance().PropertyChanged -= SideBarMenu_PropertyChanged;
        }

        private void OnEnable()
        {
            Current.Ins.PropertyChanged += Ins_PropertyChanged;
        }
    }
}

