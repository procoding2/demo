using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using OutGameEnum;
using DG.Tweening;

namespace HomeTab
{
    public class JackpotPopup : MonoBehaviour
    {
        public GameObject[] JackpotValue;

        public TextMeshProUGUI AllTime;
        public TextMeshProUGUI Weekly;

        public GameObject playerAvt1;
        //public GameObject playerAvt1Frame;
        public GameObject playerName1;
        public GameObject playerValue1;

        public GameObject playerAvt2;
        //public GameObject playerAvt2Frame;
        public GameObject playerName2;
        public GameObject playerValue2;

        public GameObject playerAvt3;
        //public GameObject playerAvt3Frame;
        public GameObject playerName3;
        public GameObject playerValue3;

        public GameObject closeBtn;
        public GameObject outAreaBtn;

        private Texture _initTexture;

        private void Start()
        {
            closeBtn.GetComponent<Button>().onClick.AddListener(() =>
            {
                SoundManager.instance.PlayUIEffect("se_click1");
                transform.DOScale(Vector3.zero, 0.2f).SetEase(Ease.OutElastic).OnComplete(() =>
                {
                    gameObject.SetActive(false);
                    UIHome.Ins.headerView.EnableClick(true);
                });
            });

            AllTime.gameObject.GetComponent<Button>().onClick.AddListener(() =>
            {
                SoundManager.instance.PlayUIEffect("se_click1");
                LoadJackpotRank(JackpotRankingTypeEnum.AllTime);
            });

            Weekly.gameObject.GetComponent<Button>().onClick.AddListener(() =>
            {
                SoundManager.instance.PlayUIEffect("se_click1");
                LoadJackpotRank(JackpotRankingTypeEnum.Weekly);
            });

            outAreaBtn.GetComponent<Button>().onClick.AddListener(() =>
            {
                SoundManager.instance.PlayUIEffect("se_click1");
                transform.DOScale(Vector3.zero, 0.2f).SetEase(Ease.OutElastic).OnComplete(() =>
                {
                    gameObject.SetActive(false);
                });
            });

            _initTexture = playerAvt1.GetComponent<RawImage>().texture;
        }

        public void Init()
        {
            playerAvt1.transform.parent.localScale = Vector3.zero;
            playerAvt2.transform.parent.localScale = Vector3.zero;
            playerAvt3.transform.parent.localScale = Vector3.zero;
            transform.localScale = Vector3.zero;           
            gameObject.SetActive(true);
            //
            transform.DOScale(Vector3.one, 0.2f).SetEase(Ease.InElastic).OnComplete(() =>
            {
                playerAvt1.transform.parent.DOScale(Vector3.one, 0.4f).SetEase(Ease.OutElastic);
                playerAvt2.transform.parent.DOScale(Vector3.one, 0.4f).SetEase(Ease.OutElastic);
                playerAvt3.transform.parent.DOScale(Vector3.one, 0.4f).SetEase(Ease.OutElastic);
                AnimChildren();
            });
            //

            LoadJackpotRank(JackpotRankingTypeEnum.AllTime);

            var arr = Current.Ins.JackpotValue
               .ToString()
               .PadLeft(JackpotValue.Count(), '0')
               .Select(x => int.Parse(x.ToString()))
               .ToList();

            for (int i = 0; i < JackpotValue.Count(); i++)
            {
                var item = JackpotValue[i];
                item.GetComponent<TextMeshProUGUI>().text = arr[i].ToString();
            }

            UIHome.Ins.headerView.EnableClick(false);
        }

        void AnimChildren()
        {
            for (int j = 0; j < JackpotValue.Length; j++)
            {
                JackpotValue[j].transform.parent.localScale = Vector3.zero;
            }
            for (int j = 0; j < JackpotValue.Length; j++)
            {
                JackpotValue[j].transform.parent.DOScale(Vector3.one, 0.2f + (j * 0.05f)).SetEase(Ease.OutElastic);
            }
        }

        private Color32 _OffColor = new Color32(149, 184, 253, 255);
        private Color32 _OnColor = new Color32(255, 255, 255, 255);

        private void LoadJackpotRank(JackpotRankingTypeEnum type)
        {
            UIHome.Ins.ShowHideLoadingPopup(true);
            StartCoroutine(Current.Ins.bonusAPI.GetJackpotRanking(type, (res) =>
            {
                UIHome.Ins.ShowHideLoadingPopup(false);
                if (res == null || res.errorCode != 0)
                {
                    UIPopUpManager.instance.ShowErrorPopUp("Request Error : LoadJackpotRank", res, () => LoadJackpotRank(type));
                    return;
                }

                if (type == JackpotRankingTypeEnum.AllTime)
                {
                    AllTime.fontStyle = FontStyles.Underline;
                    Weekly.fontStyle = FontStyles.Normal;

                    AllTime.color = _OnColor;
                    Weekly.color = _OffColor;
                }
                else
                {
                    AllTime.fontStyle = FontStyles.Normal;
                    Weekly.fontStyle = FontStyles.Underline;

                    AllTime.color = _OffColor;
                    Weekly.color = _OnColor;
                }

                Reset();

                var first = res.data.ToList().ElementAtOrDefault(0);
                if (first == null) return;
                playerName1.GetComponent<TextMeshProUGUI>().text = first.user.name;
                playerValue1.GetComponent<TextMeshProUGUI>().text = StringHelper.NumberFormatter(first.token);
                Current.Ins.ImageData.AvatarByUrl(first.user.avatar, (result) =>
                {
                    playerAvt1.GetComponent<RawImage>().texture = result.Texture;
                });
                //Current.Ins.ImageData.AvatarFrameByUrl(first.user.avatarFrame, (result) =>
                //{
                //    playerAvt1Frame.GetComponent<RawImage>().texture = result.Texture;
                //    playerAvt1Frame.SetActive(true);
                //});

                var second = res.data.ToList().ElementAtOrDefault(1);
                if (second == null) return;
                playerName2.GetComponent<TextMeshProUGUI>().text = second.user.name;
                playerValue2.GetComponent<TextMeshProUGUI>().text = StringHelper.NumberFormatter(second.token);

                Current.Ins.ImageData.AvatarByUrl(second.user.avatar, (result) =>
                {
                    playerAvt2.GetComponent<RawImage>().texture = result.Texture;
                });

                //Current.Ins.ImageData.AvatarFrameByUrl(second.user.avatarFrame, (result) =>
                //{
                //    playerAvt2Frame.GetComponent<RawImage>().texture = result.Texture;
                //    playerAvt2Frame.SetActive(true);
                //});

                var third = res.data.ToList().ElementAtOrDefault(2);
                if (third == null) return;
                playerName3.GetComponent<TextMeshProUGUI>().text = third.user.name;
                playerValue3.GetComponent<TextMeshProUGUI>().text = StringHelper.NumberFormatter(third.token);

                Current.Ins.ImageData.AvatarByUrl(third.user.avatar, (result) =>
                {
                    playerAvt3.GetComponent<RawImage>().texture = result.Texture;
                });

                //Current.Ins.ImageData.AvatarFrameByUrl(third.user.avatarFrame, (result) =>
                //{
                //    playerAvt3Frame.GetComponent<RawImage>().texture = result.Texture;
                //    playerAvt3Frame.SetActive(true);
                //});
            }));
        }

        private void Reset()
        {
            playerName1.GetComponent<TextMeshProUGUI>().text = "--";
            playerValue1.GetComponent<TextMeshProUGUI>().text = "--";
            playerAvt1.GetComponent<RawImage>().texture = _initTexture;

            playerName2.GetComponent<TextMeshProUGUI>().text = "--";
            playerValue2.GetComponent<TextMeshProUGUI>().text = "--";
            playerAvt2.GetComponent<RawImage>().texture = _initTexture;

            playerName3.GetComponent<TextMeshProUGUI>().text = "--";
            playerValue3.GetComponent<TextMeshProUGUI>().text = "--";
            playerAvt3.GetComponent<RawImage>().texture = _initTexture;
        }
    }
}
