using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using OutGameEnum;
using GameEnum;
using UniRx;
using DG.Tweening;
using TMPro;

namespace HomeTab
{
    public class UIHomeTab : UIBase<UIHomeTab>
    {
        protected override void InitIns() => Ins = this;

        public static TabbarEnum DefaultView { get; set; }

        public GameObject homeContent;
        public GameObject gameMode;

        public GameObject sideBarMenu;
        public GameObject Notification;
        public GameObject DailyMission;
        public GameObject DailyReward;
        public GameObject GoldGift;

        public GameObject UnclaimedReward;

        public GameObject JackpotArea;
        public GameObject JackpotPopup;
        public Button JackpotButton;

        public GameObject NeedCoinPopup;
        public GameObject LevelPopup;

        public Button bingoBtn;
        public Button bubbleBtn;
        public Button puzzleBtn;
        public Button solitaireBtn;
        public Button eBallBtn;

        private IDisposable JackpotRx()
        {
            return Current.Ins.JackpotValueRx
                .Subscribe((vl) =>
                {
                    if (!JackpotArea.activeInHierarchy) return;
                    JackpotArea.GetComponentInChildren<SlotBillboard>().SetNewSlotNumber(vl);
                });
        }

        // Start is called before the first frame update
        protected override void Start()
        {
            sideBarMenu.GetComponent<SideBarMenu>().Init(OpenDailyMission, () => OpenDailyReward(), OpenNotification);
            
            InitJackpot();

            if (Current.Ins.IsFirstLoginOfDay)
            {
                Current.Ins.IsFirstLoginOfDay = false;
                OpenDailyReward(onClose: null);
            }

            GetUnclaimedReward();

            gameMode.SetActive(false);
        }

        void GetUnclaimedReward()
        {
            UIHome.Ins.ShowHideLoadingPopup(true);
            StartCoroutine(Current.Ins.bonusAPI.GetBonusGameReward<BonusGameRewardResponse>((res) =>
            {
                UIHome.Ins.ShowHideLoadingPopup(false);
                if (res == null || res.errorCode != 0)
                {
                    Debug.Log("Error: GetUnclaimedReward");
                    UIPopUpManager.instance.ShowErrorPopUp("Request Error : GetUnclaimedReward", res, GetUnclaimedReward);
                    return;
                }

                if (res.data.Length > 0)
                {
                    UnclaimedReward.GetComponent<UnclaimedReward>().Init(res);
                    UnclaimedReward.SetActive(true);
                }
            }));
        }

        private void OpenNotification()
        {
            UIHome.Ins.ShowHideLoadingPopup(true);
            StartCoroutine(Current.Ins.userAPI.GetNotification(1, 20, (res) =>
            {
                UIHome.Ins.ShowHideLoadingPopup(false);
                if (res == null || res.errorCode != 0)
                {
                    UIPopUpManager.instance.ShowErrorPopUp("Request Error : OpenNotification", res, OpenNotification);
                    Debug.Log("Error: OpenDailyMission");
                    return;
                }

                var notificationData = new NotificationData();
                notificationData.Items = new List<NotificationItemData>();

                foreach (var resItemData in res.data.datas)
                {
                    var item = new NotificationItemData();
                    notificationData.Items.Add(item);
                    item.Id = resItemData.id.ToString();
                    item.IsRead = resItemData.isRead;
                    item.TimeAgo = res.currentTime - resItemData.createdAt;

                    switch (resItemData.type)
                    {
                        case NotificationTypeEnum.Normal:
                            item.Title = resItemData.title;
                            item.Content = resItemData.content;
                            break;
                        case NotificationTypeEnum.NewEvent:
                            item.Title = resItemData.eventDetail.name;
                            item.Content = resItemData.eventDetail.content;
                            break;
                        case NotificationTypeEnum.NewDailyMission:
                            item.Title = resItemData.mission.name;
                            item.Content = resItemData.mission.description;
                            break;
                    }
                }

                Notification.GetComponent<NotificationPopup>().Init(notificationData, res.data.nextPage);
            }));
        }

        private void OpenDailyMission()
        {
            UIHome.Ins.ShowHideLoadingPopup(true);
            StartCoroutine(Current.Ins.userAPI.GetDailyMission((res) =>
            {
                UIHome.Ins.ShowHideLoadingPopup(false);
                if (res == null || res.errorCode != 0)
                {
                    UIPopUpManager.instance.ShowErrorPopUp("Request Error : OpenDailyMission", res, OpenDailyMission);
                    Debug.Log("Error: OpenDailyMission");
                    return;
                }

                var dailyMissionData = new DailyMissionData();
                dailyMissionData.Items = new List<DailyMissionItemData>();

                foreach (var resItemData in res.data.datas)
                {
                    if (resItemData.prizes.Count() == 0) continue;

                    var itemData = new DailyMissionItemData();
                    dailyMissionData.Items.Add(itemData);

                    itemData.Title = resItemData.name;
                    itemData.Content = resItemData.description;
                    itemData.RewardType = resItemData.prizes.First().type;
                    itemData.RewardValue = resItemData.prizes.First().amount;
                    itemData.TotalProgress = resItemData.condition;
                    itemData.CurrentProgress = resItemData.completed;
                    itemData.IsClaimed = resItemData.isClaimed;
                    itemData.RewardId = resItemData.rewardId;
                }

                DailyMission.GetComponent<DailyMissionPopup>().Init(dailyMissionData);
            }));
        }

        private void OpenDailyReward(Action onClose = null)
        {
            UIHome.Ins.ShowHideLoadingPopup(true);
            StartCoroutine(Current.Ins.userAPI.GetDailyReward((res) =>
            {
                UIHome.Ins.ShowHideLoadingPopup(false);
                if (res == null || res.errorCode != 0)
                {
                    UIPopUpManager.instance.ShowErrorPopUp("Request Error : OpenDailyReward", res, () => OpenDailyReward(onClose));
                    Debug.Log("Error: OpenDailyReward");
                    return;
                }

                var dailyRewardData = new DailyRewardData();
                dailyRewardData.Items = new List<DailyRewardItemData>();

                foreach (var resItemData in res.data.datas)
                {
                    if (resItemData.prizes.Count() == 0) continue;

                    var itemData = new DailyRewardItemData();
                    itemData.Prizes = new List<DailyRewardItemPrizeData>();
                    dailyRewardData.Items.Add(itemData);

                    itemData.Name = resItemData.name;
                    itemData.Day = resItemData.day;
                    itemData.IsClaimed = resItemData.isClaimed;
                    itemData.RewardId = resItemData.rewardId;

                    foreach (var resPrizeData in resItemData.prizes)
                    {
                        var prizeItemData = new DailyRewardItemPrizeData();
                        itemData.Prizes.Add(prizeItemData);

                        prizeItemData.RewardType = resPrizeData.type;
                        prizeItemData.RewardValue = resPrizeData.amount;
                    }
                }

                DailyReward.GetComponent<DailyRewardPopup>().Init(dailyRewardData, onClose);
            }));
        }

        private void InitJackpot()
        {
            JackpotButton.onClickWithHCSound(() =>
            {
                JackpotPopup.GetComponent<JackpotPopup>().Init();
            }, isClearAllListener: true);

            bags.Add(JackpotRx());
            //add socket change jackpot
            Current.Ins.socketAPI.AddSocket<JackpotResponse.Data>(
                socketName: StringConst.SOCKET_CHANGE_JACKPOT_EVENT,
                socketUrl: StringConst.SOCKET_CHANGE_JACKPOT_EVENT,
                callBack: (res) =>
                {
                    AddJob(() =>
                    {
                        if (!JackpotArea.activeInHierarchy) return;
                        Current.Ins.JackpotValue = res.token;
                        JackpotArea.GetComponentInChildren<SlotBillboard>().SetNewSlotNumber(res.token);
                    });
                }
            );
           
            Current.Ins.LoadJackpot();
        }

        public void OpenGameMode(MiniGameEnum miniGame)
        {
            SoundManager.instance.PlayUIEffect("se_click1");

            MiniGameRequest gameRequest = new MiniGameRequest();
            gameRequest.game = miniGame.GetStringValue();
            gameRequest.limit = 10;
            gameRequest.page = 1;

            Action<MiniGameResponse> action = (res) =>
            {
                UIHome.Ins.ShowHideLoadingPopup(false);

                if (res == null || res.errorCode != 0)
                {
                    Debug.Log("Error: MiniGameResponse");
                    UIPopUpManager.instance.ShowErrorPopUp("Request Error : OpenGameMode", res, () => OpenGameMode(miniGame));
                    return;
                }

                GameModeData data = new GameModeData();
                data.Game = miniGame;
                data.HeadToHeads = MapResponeToData(MiniGameModeEnum.HeadToHead, res);
                data.OneToManys = MapResponeToData(MiniGameModeEnum.OneToMany, res);
                data.KnockOuts = MapResponeToData(MiniGameModeEnum.KnockOut, res);

                Sequence sq = DOTween.Sequence();


                gameMode.SetActive(true);
                gameMode.GetComponent<GameMode>().Show(data,
                    errorCoinAction: null/*NeedCoinPopup.GetComponent<NeedCoinPopup>().Init*/,
                    errorLevelAction: (ToLevel) => LevelPopup.GetComponent<WarningLevelPopup>().IsShow(true, ToLevel));

                //UIHome.Ins.homeTabIcon.GetComponent<TabItem>().OnClickIcon(() =>
                //{
                //    SoundManager.instance.PlayUIEffect("se_click1");

                //    gameMode.SetActive(false);
                //    homeContent.transform.DOScale(vectorON, duration);
                //});

                //UIHome.Ins.homeView.SetActive(false);
            };

            bool isFirstTime = PlayerPrefsEx.Get(miniGame.GetStringValue(), 0).Equals(0);
            if (isFirstTime)
            {
                StartTutorialGame(miniGame);
                return;
            }

            UIHome.Ins.ShowHideLoadingPopup(true);
            StartCoroutine(Current.Ins.gameAPI.GetMiniGame(gameRequest, action));
        }

        private List<GameModeData.GameModeItemData> MapResponeToData(MiniGameModeEnum mode, MiniGameResponse response)
        {
            return response.data.datas
                .Where(d => d.mode == mode)
                .Select(itemRes => new GameModeData.GameModeItemData()
                {
                    Id = itemRes.id,
                    Mode = itemRes.mode,
                    TokenPrizePool = itemRes.tokenPrize,
                    GoldPrizePool = itemRes.goldPrize,
                    TicketPrizePool = itemRes.ticketFee,
                    ItemName = itemRes.name,
                    Quantity = itemRes.quantity,
                    FromLevel = itemRes.fromLevel,
                    ToLevel = itemRes.toLevel,
                    StartTime = itemRes.startTime,
                    EndTime = itemRes.endTime,
                    IsActive = itemRes.isActive,
                    TokenFee = itemRes.tokenFee,
                    GoldFee = itemRes.goldFee,
                    TicketFee = itemRes.ticketFee,
                    TypeLevel = itemRes.typeLevel
                })
                .ToList();
        }

        private void StartTutorialGame(MiniGameEnum miniGame)
        {
            switch (miniGame)
            {
                case MiniGameEnum.Bingo:
                    UIBingo.InitTutorialGameStaticVariable(miniGame);
                    SceneHelper.LoadScene(SceneEnum.Bingo);
                    break;
                case MiniGameEnum.Bubble:
                    UIBubble.InitTutorialGameStaticVariable(miniGame);
                    SceneHelper.LoadScene(SceneEnum.Bubble);
                    break;
                case MiniGameEnum.Solitaire:
                    UISolitaire.InitTutorialGameStaticVariable(miniGame);
                    SceneHelper.LoadScene(SceneEnum.Solitaire);
                    break;
                default: break;
            }
        }
    }
}