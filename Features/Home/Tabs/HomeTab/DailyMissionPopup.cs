using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using OutGameEnum;
using DG.Tweening;

namespace HomeTab
{
    public class DailyMissionPopup : MonoBehaviour
    {
        public GameObject dailyMissionLayout;
        public GameObject dailyMissionItemPrefab;

        public GameObject closeBtn;
        public GameObject outAreaBtn;
        public GameObject outContentBtn;

        //
        [Header("Data for animation")]
        [SerializeField] GameObject contentParent;
        [SerializeField] GameObject content;
        [SerializeField] ScrollRect layout;
        [SerializeField] float timeAnim;
        public void Init(DailyMissionData data)
        {
            dailyMissionLayout.transform.RemoveAllChild();
            foreach (var itemData in data.Items)
            {
                var missionItem = Instantiate(dailyMissionItemPrefab, dailyMissionLayout.transform, false)
                    .GetComponent<DailyMissionItem>();
                missionItem.Init(itemData);
                missionItem.transform.localScale = Vector3.zero;
            }
            transform.localScale = Vector3.one;
            contentParent.transform.localScale = new Vector3(1,0,0) ;
            //
            gameObject.SetActive(true);
            //
            contentParent.transform.DOScale(Vector3.one, timeAnim).SetEase(Ease.InElastic).OnComplete(() =>
            {
                AnimChildren();
            });

            UIHome.Ins.headerView.EnableClick(false);
        }

        void AnimChildren()
        {
            for (int j = 0; j < content.transform.childCount; j++)
            {
                int currentIndex = j;
                content.transform.GetChild(j).DOScale(Vector3.one, 0.1f + (j * 0.15f)).SetEase(Ease.OutElastic).OnComplete(() =>
                {
                    //content.transform.GetChild(currentIndex).GetComponent<DailyMissionItem>().AnimCanClaim();
                    if (currentIndex == content.transform.childCount - 1)
                    {
                        for (int i = 0; i < content.transform.childCount; i++)
                        {
                            content.transform.GetChild(i).GetComponent<DailyMissionItem>().AnimCanClaim();
                        }                       
                    }                
                });                
            }
            layout.verticalNormalizedPosition = 1f;

        }

        // Start is called before the first frame update
        void Start()
        {
            closeBtn.GetComponent<Button>().onClick.AddListener(() =>
            {
                SoundManager.instance.PlayUIEffect("se_click1");
                transform.DOScale(Vector3.zero, 0.2f).SetEase(Ease.OutElastic).OnComplete(() =>
                {
                    gameObject.SetActive(false);
                    UIHome.Ins.headerView.EnableClick(true);
                });
            });

            outAreaBtn.GetComponent<Button>().onClick.AddListener(() =>
            {
                SoundManager.instance.PlayUIEffect("se_click1");
                transform.DOScale(Vector3.zero, 0.2f).SetEase(Ease.OutElastic).OnComplete(() =>
                {
                    gameObject.SetActive(false);
                    UIHome.Ins.headerView.EnableClick(true);
                });
            });

            outContentBtn.GetComponent<Button>().onClick.AddListener(() =>
            {
                SoundManager.instance.PlayUIEffect("se_click1");
                transform.DOScale(Vector3.zero, 0.2f).SetEase(Ease.OutElastic).OnComplete(() =>
                {
                    gameObject.SetActive(false);
                    UIHome.Ins.headerView.EnableClick(true);
                });
            });
        }

        private void OnDisable()
        {
            for (int j = 0; j < content.transform.childCount; j++)
            {
                int currentIndex = j;             
                DOTween.Kill(content.transform.GetChild(currentIndex));
            }
        }
    }
}

