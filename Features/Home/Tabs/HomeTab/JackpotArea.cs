using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class JackpotArea : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI jackpotCoinNum = null;


    private long coinValue = 0, coinValue_ex = 0;
    public long CoinValue
    {
        get => coinValue;
        set => coinValue = value;
    }


    private void Update()
    {
        if (coinValue != coinValue_ex)
        {
            long diff = coinValue - coinValue_ex;
            int mul = diff > 0 ? 1 : -1;
            long addValue = Mathf.Abs(diff) switch
            {
                > 10000000 => 10000000,
                > 1000000 => 1000000,
                > 100000 => 100000,
                > 10000 => 10000,
                > 825 => 825,
                > 75 => 75,
                > 4 => 4,
                _ => 1
            };
            coinValue_ex += (addValue * mul);
            SoundManager.instance.PlayUIEffect("se_muted");
            jackpotCoinNum.text = $"{coinValue_ex:n0}";

        }
    }



}


