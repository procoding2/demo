﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using OutGameEnum;
using TMPro;
using DG.Tweening;

namespace HomeTab
{
    public class DailyMissionItem : MonoBehaviour
    {
        public GameObject Title;
        public GameObject Content;

        public GameObject IconGold;
        public GameObject IconToken;
        public GameObject IconTicket;
        public GameObject IconVMark;
        public GameObject X2XP;
        public GameObject BgComplete;

        public GameObject Value;

        public GameObject progress;

        public GameObject bg;
        public GameObject bgCanClaim;

        Tween tween;

        public void Init(DailyMissionItemData data)
        {
            bgCanClaim.SetActive(false);
            bg.SetActive(true);

            Title.GetComponent<TextMeshProUGUI>().text = data.Title;
            Content.GetComponent<Text>().text = $"{data.Content} ({data.CurrentProgress}/{data.TotalProgress})";

            progress.GetComponent<Slider>().value = (float)data.CurrentProgress / (float)data.TotalProgress;
            if (data.CurrentProgress >= data.TotalProgress)
            {
                bgCanClaim.SetActive(true);               
                bg.SetActive(false);

                SetValueReward(data);
                IconVMark.SetActive(true);

                if (!data.IsClaimed && !string.IsNullOrWhiteSpace(data.RewardId))
                {
                    IconVMark.SetActive(false);
                    gameObject.GetComponent<Button>().interactable = true;
                    gameObject.GetComponent<Button>().onClick.AddListener(() =>
                    {
                        SoundManager.instance.PlayUIEffect("se_click1");
                        ClaimReward(data.RewardId);
                    });
                }
                else
                {
                    BgComplete.SetActive(true);
                }
                
                return;
            }

            SetValueReward(data);
        }

        public void AnimCanClaim()
        {
            if (BgComplete.activeSelf == false && bgCanClaim.activeSelf == true)
            {
                tween = transform.DOScale(Vector3.one * 1.05f, 0.5f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutCirc);
            }
        }

        private void ClaimReward(string id)
        {
            UIHome.Ins.ShowHideLoadingPopup(true);
            StartCoroutine(Current.Ins.userAPI.ClaimRewardDailyMission(id, (res) =>
            {
                UIHome.Ins.ShowHideLoadingPopup(false);
                if (res == null || res.errorCode != 0)
                {
                    UIPopUpManager.instance.ShowErrorPopUp("Request Error : ClaimReward", res, () => ClaimReward(id));
                    return;
                }

                tween.Kill();
                transform.localScale = Vector3.one;
                gameObject.GetComponent<Button>().interactable = false;
                BgComplete.SetActive(true);
                IconVMark.SetActive(true);
                var numberDaily = PlayerPrefsEx.Get(PlayerPrefsConst.NUMBER_DAILY_MISSION, 0);
                PlayerPrefsEx.Set(PlayerPrefsConst.NUMBER_DAILY_MISSION, numberDaily - 1);

                Current.Ins.ReloadUserProfile(gameObject.transform);
            }));
        }

        private void SetValueReward(DailyMissionItemData data)
        {
            Value.GetComponent<TextMeshProUGUI>().text = $"x{data.RewardValue}";

            switch (data.RewardType)
            {
                case DailyPrizeTypeEnum.Gold:
                    IconGold.SetActive(true);
                    break;
                case DailyPrizeTypeEnum.HCToken:
                    IconToken.SetActive(true);
                    break;
                case DailyPrizeTypeEnum.Ticket:
                    IconTicket.SetActive(true);
                    break;
                case DailyPrizeTypeEnum.X2XP:
                    X2XP.SetActive(true);
                    Value.GetComponent<TextMeshProUGUI>().text = $"{data.RewardValue}m";
                    break;
            }
        }
    }
}

