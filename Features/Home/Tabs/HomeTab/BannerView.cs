using System.Collections;
using System.Collections.Generic;
using GameEnum;
using OutGameEnum;
using UnityEngine;
using UnityEngine.UI;

namespace HomeTab
{
    public class BannerView : MonoBehaviour
    {
        public Button bingo;
        public Button solitaire;
        public Button bubble;

        // Start is called before the first frame update
        void Start()
        {
            bingo.onClick.AddListener(() =>
            {
                SoundManager.instance.PlayUIEffect("se_click1");
                UIBingo.InitTutorialGameStaticVariable(MiniGameEnum.Bingo);
                SceneHelper.LoadScene(SceneEnum.Bingo);
            });

            solitaire.onClickWithHCSound(() =>
            {
                SoundManager.instance.PlayUIEffect("se_click1");
                UISolitaire.InitTutorialGameStaticVariable(MiniGameEnum.Solitaire);
                SceneHelper.LoadScene(SceneEnum.Solitaire);
            });

            bubble.onClickWithHCSound(() =>
            {
                SoundManager.instance.PlayUIEffect("se_click1");
                UIBubble.InitTutorialGameStaticVariable(MiniGameEnum.Bubble);
                SceneHelper.LoadScene(SceneEnum.Bubble);
            });
        }
    }
}
