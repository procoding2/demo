using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using OutGameEnum;
using DG.Tweening;

namespace HomeTab
{
    public class DailyRewardPopup : MonoBehaviour
    {
        public GameObject Day1;
        public GameObject Day2;
        public GameObject Day3;
        public GameObject Day4;
        public GameObject Day5;
        public GameObject Day6;
        public GameObject Day7;

        public GameObject CloseBtn;
        int checkTodayOf1To6 = 0;
        private Action _onClose;
        [Header("Data for animation")]
        [SerializeField] GameObject contentParent;
        [SerializeField] GameObject content;
        [SerializeField] float timeAnim;

        // Start is called before the first frame update
        void Start()
        {
            CloseBtn.GetComponent<Button>().onClick.AddListener(() =>
            {
                SoundManager.instance.PlayUIEffect("se_click1");
                transform.DOScale(Vector3.zero, 0.2f).SetEase(Ease.OutElastic).OnComplete(() =>
                {
                    gameObject.SetActive(false);
                    UIHome.Ins.headerView.EnableClick(true);
                });
                _onClose?.Invoke();
            });
        }

        public void Init(DailyRewardData data, Action onClose)
        {
            _onClose = onClose;
            checkTodayOf1To6 = 0;
            foreach (var item in data.Items)
            {
                var prizeNormalDay = item.Prizes[0];
                GameObject normalDay = null;

                switch (item.Day)
                {
                    case DayTypeEnum.Day_1:
                        normalDay = Day1;
                        break;
                    case DayTypeEnum.Day_2:
                        normalDay = Day2;
                        break;
                    case DayTypeEnum.Day_3:
                        normalDay = Day3;
                        break;
                    case DayTypeEnum.Day_4:
                        normalDay = Day4;
                        break;
                    case DayTypeEnum.Day_5:
                        normalDay = Day5;
                        break;
                    case DayTypeEnum.Day_6:
                        normalDay = Day6;
                        break;
                }

                if (normalDay != null)
                {                  
                    //
                    switch (item.Prizes.Count)
                    {
                        case 1:
                            normalDay.GetComponent<DailyRewardItem>().InitDayFrom1To6(prizeNormalDay.RewardType, prizeNormalDay.RewardValue, item.IsClaimed, item.RewardId);
                            break;
                        case 2:
                            var prizeNormalDay_Form2_2 = item.Prizes[1];
                            normalDay.GetComponent<DailyRewardItem>()
                                .InitDayFrom1To6Form2(prizeNormalDay.RewardType,
                                                      prizeNormalDay_Form2_2.RewardType,
                                                      prizeNormalDay.RewardValue,
                                                      prizeNormalDay_Form2_2.RewardValue, 
                                                      item.IsClaimed, 
                                                      item.RewardId);                                          
                            break;
                        case 3:
                            var token = item.Prizes.FirstOrDefault(x => x.RewardType == DailyPrizeTypeEnum.HCToken).RewardValue;
                            var gold = item.Prizes.FirstOrDefault(x => x.RewardType == DailyPrizeTypeEnum.Gold).RewardValue;
                            var ticket = item.Prizes.FirstOrDefault(x => x.RewardType == DailyPrizeTypeEnum.Ticket).RewardValue;
                            normalDay.GetComponent<DailyRewardItem>().InitDayFrom1To6Form3(gold, token, ticket, item.IsClaimed, item.RewardId);
                            break;
                        default:
                            Debug.LogError("InitDayFrom1To6");
                            break;
                    }
                    if (item.RewardId != "0")
                    {
                        this.checkTodayOf1To6++;
                    }
                    normalDay.transform.localScale = Vector3.zero;
                }
                else
                {                                     
                    //
                    switch (item.Prizes.Count)
                    {
                        case 1:
                            Day7.GetComponent<DailyRewardItem>().InitDay7_3(prizeNormalDay.RewardType, prizeNormalDay.RewardValue, item.IsClaimed, item.RewardId);
                            break;
                        case 2:
                            var prizeNormalDay_Form2_2 = item.Prizes[1];
                            Day7.GetComponent<DailyRewardItem>()
                                .InitDay7_2(prizeNormalDay.RewardType,
                                            prizeNormalDay_Form2_2.RewardType,
                                            prizeNormalDay.RewardValue,
                                            prizeNormalDay_Form2_2.RewardValue,
                                            item.IsClaimed,
                                            item.RewardId);
                            break;
                        case 3:
                            var token = item.Prizes.FirstOrDefault(x => x.RewardType == DailyPrizeTypeEnum.HCToken).RewardValue;
                            var gold = item.Prizes.FirstOrDefault(x => x.RewardType == DailyPrizeTypeEnum.Gold).RewardValue;
                            var ticket = item.Prizes.FirstOrDefault(x => x.RewardType == DailyPrizeTypeEnum.Ticket).RewardValue;
                            Day7.GetComponent<DailyRewardItem>().InitDay7(token, gold, ticket, item.IsClaimed, item.RewardId);
                            break;
                        default:
                            Debug.LogError("InitDay7");
                            break;
                    }
                    if (item.RewardId != "0")
                    {
                        this.checkTodayOf1To6++;
                    }
                    if (Day7.GetComponent<DailyRewardItem>().bgComplete.activeSelf == true)
                    {
                        Day7.GetComponent<DailyRewardItem>().TodayReal();
                    }                  
                }
            }

            switch (checkTodayOf1To6)
            {
                case 1:
                    Day1.GetComponent<DailyRewardItem>().TodayReal();
                    break; 
                case 2:
                    Day2.GetComponent<DailyRewardItem>().TodayReal();
                    break; 
                case 3:
                    Day3.GetComponent<DailyRewardItem>().TodayReal();
                    break;
                case 4:
                    Day4.GetComponent<DailyRewardItem>().TodayReal();
                    break;
                case 5:
                    Day5.GetComponent<DailyRewardItem>().TodayReal();
                    break;
                case 6:
                    Day6.GetComponent<DailyRewardItem>().TodayReal();
                    break;
                case 7:
                    break;
                default :
                    Debug.LogError("checkTodayOf1To6_DailyRewardPopup");
                    break;
            }
            //
            transform.localScale = Vector3.one;
            contentParent.transform.localScale = new Vector3 (0, 1, 0);
            Day7.transform.localScale = Vector3.zero;
            gameObject.SetActive(true);
            //
            contentParent.transform.DOScale(Vector3.one, 0.1f).SetEase(Ease.OutElastic).OnComplete(() =>
            {
                AnimChildren();
            });

            UIHome.Ins.headerView.EnableClick(false);
        }

        void AnimChildren()
        {
            for (int j = 0; j < content.transform.childCount; j++)
            {
                int currentIndex = j;
                content.transform.GetChild(j).DOScale(Vector3.one, 0.2f + (j * 0.1f)).SetEase(Ease.OutElastic).OnComplete(() =>
                {
                    content.transform.GetChild(currentIndex).GetComponent<DailyRewardItem>().AnimCanClaim();
                });

            }
            Day7.transform.DOScale(Vector3.one, 0.3f).SetEase(Ease.OutElastic).OnComplete(() =>
            {
                Day7.transform.GetComponent<DailyRewardItem>().AnimCanClaim();
            }); ;
        }

        private void OnDisable()
        {
            for (int j = 0; j < content.transform.childCount; j++)
            {
                int currentIndex = j;
                DOTween.Kill(content.transform.GetChild(currentIndex));
            }
        }
    }
}