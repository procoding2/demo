﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameEnum;
using OutGameEnum;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace HomeTab
{
    public class GroupModeItem : MonoBehaviour
    {
        public GameObject Header;
        public GameObject Layout;
        public GameObject ModeItemPrefab;

        void SettingLanguage()
        {
            if(Header.GetComponent<TextMeshProUGUI>().text == "Tournament" ||
               Header.GetComponent<TextMeshProUGUI>().text == "토너먼트" ||
               Header.GetComponent<TextMeshProUGUI>().text == "Giải đấu")
            {
                LanguageEnum language = Current.Ins.Config.GetCurrentLanguage();
                switch (language)
                {
                    case LanguageEnum.eng:
                        Header.GetComponent<TextMeshProUGUI>().text = "Tournament";
                        break;
                    case LanguageEnum.kor:
                        Header.GetComponent<TextMeshProUGUI>().text = "토너먼트";
                        break;
                    case LanguageEnum.vi:
                        Header.GetComponent<TextMeshProUGUI>().text = "Giải đấu";
                        break;
                }
            }
        }

        private void OnEnable()
        {
            Current.Ins.Config.PropertyChanged += Config_PropertyChanged;
        }

        private void OnDisable()
        {
            Current.Ins.Config.PropertyChanged -= Config_PropertyChanged;
        }

        private void Config_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == StringConst.PROPERTY_LANGUAGE)
            {
                SettingLanguage();
            }
        }

        public void Init(MiniGameEnum miniGame,
                         MiniGameModeEnum modeGame,
                         List<GameModeData.GameModeItemData> datas,
                         float baseHeight,
                         Action<CoinEnum> errorCoinAction,
                         Action<GameModeData.GameModeItemData> onClickRownAction)
        {
            switch (modeGame)
            {
                case MiniGameModeEnum.HeadToHead:
                    Header.GetComponent<TextMeshProUGUI>().text = "1 vs 1";
                    break;
                case MiniGameModeEnum.OneToMany:
                    LanguageEnum language = Current.Ins.Config.GetCurrentLanguage();
                    switch (language)
                    {
                        case LanguageEnum.eng:
                            Header.GetComponent<TextMeshProUGUI>().text = "Tournament";
                            break;
                        case LanguageEnum.kor:
                            Header.GetComponent<TextMeshProUGUI>().text = "토너먼트";
                            break;
                        case LanguageEnum.vi:
                            Header.GetComponent<TextMeshProUGUI>().text = "Giải đấu";
                            break;
                    }
                    break;
                case MiniGameModeEnum.KnockOut:
                    Header.GetComponent<TextMeshProUGUI>().text = "Knockout";
                    break;
            }

            var layoutRect = Layout.GetComponent<RectTransform>().rect;
            Layout.GetComponent<RectTransform>().sizeDelta = new Vector2(layoutRect.width, layoutRect.height + (datas.Count * baseHeight) + +(datas.Count * 10));

            foreach (var item in datas)
            {
                var modeItem = Instantiate(ModeItemPrefab, Layout.transform, false);
                modeItem.GetComponent<GameModeItem>().Init(miniGame, item, errorCoinAction);

                modeItem.GetComponent<Button>().onClick.RemoveAllListeners();
                modeItem.GetComponent<Button>().onClick.AddListener(() =>
                {
                    SoundManager.instance.PlayUIEffect("se_click1");
                    onClickRownAction.Invoke(item);
                });
            }
        }
    }
}