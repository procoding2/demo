using System;
using System.Collections.Generic;
using GameEnum;
using OutGameEnum;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace HomeTab
{
    public class GameMode : MonoBehaviour
    {
        public Button closeGameMode;
        public GameObject Title;
        public Image GameIcon;
        public Sprite SolitarieIcon, BubbleIcon, BingoIcon, dogRace;

        public GameObject Content;

        public GameObject GroupGameModePrefab;
        public GameObject ModeItemPrefab;

        public GameObject MainContent;
        public GameObject PrizeDetail;

        private GameModeData _data;
        private Action<CoinEnum> _errorCoinAction;
        private Action<TypeLevelEnum> _errorLevelAction;
        private float _baseHeight;

        [Header("Tut minigame")]
        public ButtonEx tutorial;
        public GameObject bingo;
        public GameObject solitaire;
        public GameObject bubble;
        [SerializeField] private GameObject gameSelect = null;

        private void Awake()
        {
            this.closeGameMode.onClickWithHCSound(CloseGameMode, true);
        }

        public void Show(GameModeData data, Action<CoinEnum> errorCoinAction, Action<TypeLevelEnum> errorLevelAction)
        {
            _data = data;
            _errorCoinAction = errorCoinAction;
            _errorLevelAction = errorLevelAction;
            MainContent.SetActive(true);
            PrizeDetail.SetActive(false);

            Content.transform.RemoveAllChild();
            gameSelect = null;
            switch (data.Game)
            {
                case MiniGameEnum.Solitaire:

                    GameIcon.sprite = SolitarieIcon;
                    Title.GetComponent<TextMeshProUGUI>().text = "Solitaire <size=75><sprite=0></size>";
                    gameSelect = solitaire;
                    break;

                case MiniGameEnum.Bubble:

                    GameIcon.sprite = BubbleIcon;
                    Title.GetComponent<TextMeshProUGUI>().text = "Bubble <size=75><sprite=0></size>";
                    gameSelect = bubble;
                    break;

                case MiniGameEnum.Bingo:

                    GameIcon.sprite = BingoIcon;
                    Title.GetComponent<TextMeshProUGUI>().text = "Bingo <size=75><sprite=0></size>";
                    gameSelect = bingo;
                    break;

                case MiniGameEnum.Puzzle:

                    GameIcon.sprite = dogRace;
                    Title.GetComponent<TextMeshProUGUI>().text = "Puzzle";

                    break;

                case MiniGameEnum.EBall:
                    Title.GetComponent<TextMeshProUGUI>().text = "8 Ball";

                    break;
            }
            //set but tut
            if (gameSelect != null)
            {               
                gameSelect.GetComponent<HowToPlayMinigame>().OnClose = () =>
                {
                    this.closeGameMode.onClickWithHCSound(CloseGameMode, true);
                };
                
                this.tutorial.onClickWithHCSound(() => 
                {
                    gameSelect.SetActive(true);
                    gameSelect.GetComponent<HowToPlayMinigame>().InitTut();
                    this.closeGameMode.onClick.RemoveAllListeners();                                     
                }, true);
            }

            _baseHeight = ModeItemPrefab.GetComponent<RectTransform>().rect.height;

            if (data.HeadToHeads.Count > 0)
            {
                var headToHead = Instantiate(GroupGameModePrefab);
                headToHead.transform.SetParent(Content.transform, false);
                InitGroup(data.Game, MiniGameModeEnum.HeadToHead, headToHead, data.HeadToHeads, OnClickRow);
            }

            if (data.OneToManys.Count > 0)
            {
                var oneVsMany = Instantiate(GroupGameModePrefab);
                oneVsMany.transform.SetParent(Content.transform, false);
                InitGroup(data.Game, MiniGameModeEnum.OneToMany, oneVsMany, data.OneToManys, OnClickRow);
            }

            if (data.KnockOuts.Count > 0)
            {
                var knockOut = Instantiate(GroupGameModePrefab);
                knockOut.transform.SetParent(Content.transform, false);
                InitGroup(data.Game, MiniGameModeEnum.KnockOut, knockOut, data.KnockOuts, OnClickRow);
            }

            UIHome.Ins.headerView.EnableClick(false);
        }

        private void InitGroup(MiniGameEnum miniGame,
                               MiniGameModeEnum modeGame,
                               GameObject group,
                               List<GameModeData.GameModeItemData> datas,
                               Action<GameModeData.GameModeItemData> onClickRowAction)
        {
            var rect = group.GetComponent<RectTransform>().rect;
            group.GetComponent<RectTransform>().sizeDelta = new Vector2(rect.width, rect.height + (datas.Count * _baseHeight) + +(datas.Count * 10));

            group.GetComponent<GroupModeItem>().Init(miniGame, modeGame, datas, _baseHeight, _errorCoinAction, onClickRowAction);
        }

        private void OnClickRow(GameModeData.GameModeItemData itemData)
        {
            if (itemData.TypeLevel == TypeLevelEnum.Min)
            {
                if (itemData.FromLevel > Current.Ins.player.Level)
                {
                    _errorLevelAction.Invoke(TypeLevelEnum.Min);
                    return;
                }
            }
            else if (itemData.TypeLevel == TypeLevelEnum.Range)
            {
                if (itemData.FromLevel > Current.Ins.player.Level || itemData.ToLevel < Current.Ins.player.Level)
                {
                    _errorLevelAction.Invoke(TypeLevelEnum.Range);
                    return;
                }
            }

            if (itemData.Mode == MiniGameModeEnum.HeadToHead) return;

            UIHome.Ins.ShowHideLoadingPopup(true);
            StartCoroutine(Current.Ins.gameAPI.GetMiniGameDetail(itemData.Id, (res) =>
            {
                UIHome.Ins.ShowHideLoadingPopup(false);
                if (res == null || res.errorCode != 0)
                {
                    UIPopUpManager.instance.ShowErrorPopUp("Request Error : GetMiniGameDetail", res, () => OnClickRow(itemData));
                    return;
                }

                var detailData = new PrizeDetailData();
                detailData.Title = res.data.name;
                detailData.Id = res.data.id;
                detailData.Game = _data.Game;
                detailData.Mode = res.data.mode;
                detailData.Quantity = res.data.quantity;
                detailData.TokenFee = res.data.tokenFee;
                detailData.GoldFee = res.data.goldFee;
                detailData.TicketFee = res.data.ticketFee;
                detailData.Items = new List<PrizeDetailItemData>();
                foreach (var itemRes in res.data.prizes)
                {
                    PrizeDetailItemData itemData = new PrizeDetailItemData();
                    detailData.Items.Add(itemData);
                    itemData.Order = itemRes.rank;
                    itemData.Gold = itemRes.gold;
                    itemData.Token = itemRes.token;
                    itemData.Ticket = itemRes.ticket;
                }

                MainContent.SetActive(false);
                PrizeDetail.GetComponent<PrizeDetail>().Init(
                    detailData,
                    onBackAction: () => MainContent.SetActive(true),
                    errorCoinAction: _errorCoinAction);

            }));
        }

        public void CloseGameMode()
        {
            SoundManager.instance.PlayUIEffect("se_click2");
            //UIHome.Ins.homeView.SetActive(true);
            this.tutorial.onClick.RemoveAllListeners();
            if (gameSelect)gameSelect.SetActive(false);
            gameObject.SetActive(false);

            UIHome.Ins.headerView.EnableClick(true);
        }
    }
}
