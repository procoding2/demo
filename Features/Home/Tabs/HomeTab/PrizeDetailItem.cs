using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

using OutGameEnum;
using UnityEngine.Localization.Components;

namespace HomeTab
{
    public class PrizeDetailItem : MonoBehaviour
    {
        public GameObject order;

        public Transform prizeForm;

        public GameObject PrizeGold;
        public GameObject PrizeToken;
        public GameObject PrizeTicket;

        public void Init(PrizeDetailItemData data)
        {
            order.GetComponent<LocalizeStringEvent>().UpdateValue("n", VariableEnum.String, data.Order);

            if (data.Gold > 0)
            {
                PrizeGold.GetComponent<TextMeshProUGUI>().text = StringHelper.NumberFormatter(data.Gold);
                PrizeGold.SetActive(true);
            }
            else
            {
                PrizeGold.SetActive(false);
            }

            if (data.Token > 0)
            {
                PrizeToken.GetComponent<TextMeshProUGUI>().text = StringHelper.NumberFormatter(data.Token);
                PrizeToken.SetActive(true);
            }
            else
            {
                PrizeToken.SetActive(false);
            }

            if (data.Ticket > 0)
            {
                PrizeTicket.GetComponent<TextMeshProUGUI>().text = StringHelper.NumberFormatter(data.Ticket);
                PrizeTicket.SetActive(true);
            }
            else
            {
                PrizeTicket.SetActive(false);
            }
            LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)prizeForm);

        }
    }
}