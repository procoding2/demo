using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

using OutGameEnum;
using EnhancedUI.EnhancedScroller;

namespace RankTab
{
    public class PlayerItem : EnhancedScrollerCellView 
    {
        public GameObject order;
        public GameObject playerName;
        public GameObject searching;
        public GameObject playerAvt;
        //public GameObject playerAvtFrame;
        public GameObject playerScore;

        public GameObject GoldIcon;
        public GameObject TokenIcon;

        public Image backgroundBG;

        public void Init(PlayerItemData itemData)
        {
            order.GetComponent<TextMeshProUGUI>().text = itemData.Order == 0 ? "--" : itemData.Order.ToString();

            if (itemData.RankType == UserRankingTypeEnum.Gold)
            {
                GoldIcon.SetActive(true);
                TokenIcon.SetActive(false);
                playerScore.GetComponent<TextMeshProUGUI>().text = itemData.Gold == 0 ? "--" : StringHelper.NumberFormatter(itemData.Gold);
            }
            else
            {
                GoldIcon.SetActive(false);
                TokenIcon.SetActive(true);
                playerScore.GetComponent<TextMeshProUGUI>().text = itemData.Token == 0 ? "--" : StringHelper.NumberFormatter(itemData.Token);
            }

            if (itemData.isSearching)
            {
                searching.SetActive(true);
                playerName.SetActive(false);
                return;
            }

            searching.SetActive(false);
            playerName.SetActive(true);

            playerName.GetComponent<TextMeshProUGUI>().text = itemData.PlayerName;
            Current.Ins.ImageData.AvatarByUrl(itemData.PlayerAvt, (result) =>
            {
                playerAvt.GetComponent<RawImage>().texture = result.Texture;
            });
        }
    }
}