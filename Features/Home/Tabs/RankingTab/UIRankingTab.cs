using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using OutGameEnum;
using TMPro;
using UnityEngine.Localization.Components;
using EnhancedUI.EnhancedScroller;

namespace RankTab
{
    public class UIRankingTab : UIBase<UIRankingTab>, IEnhancedScrollerDelegate
    {
        protected override void InitIns() => Ins = this;

        public GameObject headerItemToken;
        public GameObject headerItemGold;

        public GameObject first;
        public GameObject second;
        public GameObject third;

        public GameObject layout;
        public GameObject scrollBar;
        public GameObject itemPrefab;
        public EnhancedScroller scroller;

        public GameObject timeRemain;
        public GameObject your;
        //public Sprite bgRank1;
        //public Sprite bgRank2;
        //public Sprite bgRank3;
        //public Sprite bgRank4;

        public Sprite rankBG, myBG;

        private double? _timeRemain;
        private UserRankingTypeEnum _rankType;
        private bool _isInitialized = false;

        [SerializeField] private List<PlayerItemData> _data;

        public void Init()
        {
            if (_isInitialized) return;

            _isInitialized = true;
            LoadRank(UserRankingTypeEnum.HCToken, true);
        }

        // Start is called before the first frame update
        protected override void Start()
        {
            headerItemToken.GetComponent<Button>().onClickWithHCSound(() => LoadRank(UserRankingTypeEnum.HCToken, true));
            headerItemGold.GetComponent<Button>().onClickWithHCSound(() => LoadRank(UserRankingTypeEnum.Gold, true));

            _data = new List<PlayerItemData>();
            scroller.Delegate = this;
        }

        override protected void Update()
        {
            base.Update();

            if (_timeRemain != null)
            {
                if (_timeRemain >= Time.deltaTime)
                {
                    _timeRemain -= Time.deltaTime;
                    string timeStr = (TimeSpan.FromSeconds(_timeRemain.Value)).ToString(@"dd'd 'hh'h 'mm'm'");
                    timeRemain.GetComponent<LocalizeStringEvent>().UpdateValue("time", VariableEnum.String, timeStr);
                }
                else
                {
                    LoadRank(_rankType, true);
                }
            }
        }

        void LoadRank(UserRankingTypeEnum rankType, bool isChangeTab)
        {
            _rankType = rankType;

            if (isChangeTab)
            {
                UIHome.Ins.ShowHideLoadingPopup(true);
            }

            var dispose = Observable.CombineLatest(
                Observable.FromCoroutine<UserRankingResponse>((resUserRankingRx) => Current.Ins.userAPI.GetUserRanking(rankType, 1, (resA) => { resUserRankingRx.OnNext(resA); })),
                Observable.FromCoroutine<MyRankingResponse>((resMyRankingRx) => Current.Ins.userAPI.GetMyRanking(rankType, (res) => { resMyRankingRx.OnNext(res); })),
                (usersRanking, myRanking) => { return (usersRanking, myRanking); })
            .Subscribe((result) =>
            {
                UIHome.Ins.ShowHideLoadingPopup(false);

                if (result.usersRanking == null || result.usersRanking.errorCode != 0
                || result.myRanking == null || result.myRanking.errorCode != 0)
                {
                    Debug.Log("Error:");
                    UIPopUpManager.instance.ShowErrorPopUp("Request Error : LoadRank", result.myRanking);
                    return;
                }

                switch (rankType)
                {
                    case UserRankingTypeEnum.Gold:
                        headerItemGold.GetComponent<RankingHeaderItem>().ShowActive();
                        headerItemToken.GetComponent<RankingHeaderItem>().HideActive();

                        break;
                    case UserRankingTypeEnum.HCToken:
                        headerItemGold.GetComponent<RankingHeaderItem>().HideActive();
                        headerItemToken.GetComponent<RankingHeaderItem>().ShowActive();

                        break;
                }

                if (isChangeTab)
                {
                    first.GetComponent<PlayerItem>().Init(new PlayerItemData() { Order = 1, isSearching = true, RankType = rankType });
                    second.GetComponent<PlayerItem>().Init(new PlayerItemData() { Order = 2, isSearching = true, RankType = rankType });
                    third.GetComponent<PlayerItem>().Init(new PlayerItemData() { Order = 3, isSearching = true, RankType = rankType });

                    scroller.ClearAll();
                    scroller.gameObject.GetComponent<ScrollRect>().verticalNormalizedPosition = 1f;
                    _data.Clear();
                    scroller.ScrollPosition = 0;
                }

                _timeRemain = result.usersRanking.data.timeReset;

                foreach (var user in result.usersRanking.data.datas)
                {
                    var itemData = getItemData(user, rankType);

                    if (user.rank <= 3)
                    {
                        if (user.rank == 1)
                        {
                            first.GetComponent<PlayerItem>().Init(itemData);
                        }
                        else if (user.rank == 2)
                        {
                            second.GetComponent<PlayerItem>().Init(itemData);
                        }
                        else
                        {
                            third.GetComponent<PlayerItem>().Init(itemData);
                        }
                    }
                    else
                    {
                        _data.Add(itemData);
                    }
                }

                var scrollPosition = scroller.ScrollPosition;
                scroller.ReloadData();
                scroller.ScrollPosition = scrollPosition;

                var yourData = new UserRankingResponse.User()
                {
                    id = result.myRanking.data.id,
                    avatar = result.myRanking.data.avatar,
                    avatarFrame = result.myRanking.data.avatarFrame,
                    name = result.myRanking.data.name,
                    gold = result.myRanking.data.gold,
                    token = result.myRanking.data.token,
                    ticket = result.myRanking.data.ticket,
                    rank = result.myRanking.data.rank
                };

                PlayerItem pItem = your.GetComponent<PlayerItem>();
                if (pItem)
                {
                    pItem.backgroundBG.sprite = (yourData.rank > 0 && yourData.rank <= 3) ? rankBG : myBG;
                    pItem.Init(getItemData(yourData, rankType));
                }
            });
        }

        private PlayerItemData getItemData(UserRankingResponse.User user, UserRankingTypeEnum rankType)
        {
            var itemData = new PlayerItemData();
            itemData.Order = user.rank;

            if (user == null)
            {
                itemData.isSearching = true;
                return itemData;
            }

            itemData.isSearching = false;
            itemData.PlayerId = user.id.ToString();
            itemData.PlayerName = user.name;
            itemData.Token = user.token;
            itemData.Gold = user.gold;
            itemData.RankType = rankType;
            itemData.PlayerAvt = user.avatar;
            itemData.PlayerAvtFrame = user.avatarFrame;

            return itemData;
        }

        public int GetNumberOfCells(EnhancedScroller scroller)
        {
            return _data.Count;
        }

        public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
        {
            return 136f;
        }

        public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
        {
            PlayerItem cellView = scroller.GetCellView(itemPrefab.GetComponent<PlayerItem>()) as PlayerItem;
            cellView.Init(_data[dataIndex]);
            return cellView;
        }
    }
}