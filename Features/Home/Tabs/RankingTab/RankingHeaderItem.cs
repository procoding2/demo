using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RankTab
{
    public class RankingHeaderItem : MonoBehaviour
    {
        public GameObject bgActive;

        public void ShowActive() => bgActive.SetActive(true);
        public void HideActive() => bgActive.SetActive(false);
    }
}