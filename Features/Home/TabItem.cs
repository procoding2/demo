using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using OutGameEnum;
using System.Linq;
using AnimationStateEnums;
using TMPro;
using DG.Tweening;

public class TabItem : MonoBehaviour
{
    public GameObject Content;
    public GameObject Control;
    public GameObject Icon;
    private Action _onClick;
    [SerializeField]private Vector3 positionIconOnSelected;
    [SerializeField]private GameObject text;
    private TabItemAnimationStateEnum _state;
    
    public GameObject dotNoti;
    public GameObject numberNotiTxt;

    private void Start()
    {
        //
        this.positionIconOnSelected = Icon.transform.position;
        this.Icon.transform.position = this.transform.position;
        this.text.transform.localScale = Vector3.zero;
        //
        this.text.SetActive(false);
        Control.GetComponent<Button>().onClick.AddListener(() =>
        {
            GetComponent<Toggle>().onValueChanged.Invoke(true);
            if (_onClick == null) return;
            
            _onClick?.Invoke();
            _onClick = null;
        });
    }
    
    public void OnClickIcon(Action onClick)
    {
        _onClick = onClick;
    }
    
    public void SetAnimation(TabItemAnimationStateEnum state)
    {
        if (_state == state) return;
        
        _state = state;
        Control.GetComponent<Animator>().SetTrigger(state.GetStringValue());
        if (_state == TabItemAnimationStateEnum.OnSelected)
        {
            this.text.SetActive(true);
            UIHome.Ins.AnimIsOn(this.gameObject);
            Icon.transform.DOMove(this.positionIconOnSelected, 0.25f);
            this.text.transform.DOScale(Vector3.one, 0.25f);
        }
        else 
        { 
            this.text.SetActive(false);
            Icon.transform.DOMove(this.transform.position, 0.25f);
            this.text.transform.DOScale(Vector3.zero, 0.25f);
        }
    }
    
    public void showDotNoti(int number, bool isShow)
    {
        dotNoti.SetActive(isShow);
        if (number < 100)
        {
            numberNotiTxt.GetComponent<TextMeshProUGUI>().text = number.ToString();
        }
        else
        {
            numberNotiTxt.GetComponent<TextMeshProUGUI>().text = "99+";
        }
    }
    
    public void EnableDisableAllButton(bool isEnable)
    {
        Control.GetComponent<Button>().interactable = isEnable;
    }
}
