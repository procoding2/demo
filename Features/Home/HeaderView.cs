using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using DG.Tweening;
using TMPro;

public class HeaderView : MonoBehaviour
{
    public GameObject avatar;
    //public GameObject frame;
    public GameObject profileView;

    public GameObject CoinView;
    public Transform OutTransform;

    public TextMeshProUGUI BingoMMR;
    public TextMeshProUGUI SolitaireMMR;
    public TextMeshProUGUI BubbleMMR;

    private string _avatarPath;
    private string _avatarFramePath;

    private float _initY;

    // Start is called before the first frame update
    void Start()
    {
        if (avatar != null && profileView != null)
        {
            avatar.GetComponent<Button>().onClickWithHCSound(OnClickAvatar);
        }

        CoinView.GetComponent<CoinView>().Init(() =>
        {
            if (_isHide)
            {
                HideHeader(false);
                StartCoroutine(LazyHideHeader());
            }
        });

        Current.Ins.UserAvatar((result) =>
        {
            avatar.GetComponent<RawImage>().texture = result.Texture;
            _avatarPath = result.Path;
        });

        //Current.Ins.UserAvatarFrame((result) =>
        //{
        //    frame.GetComponent<RawImage>().texture = result.Texture;
        //    _avatarFramePath = result.Path;
        //});

        Current.Ins.PropertyChanged += User_PropertyChanged;
    }


    //todo:: �����
    public void EnableClick(bool isEnable)
    {
        //avatar.GetComponent<Button>().interactable = isEnable;
    }

    private void User_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
    {
        if (e.PropertyName == StringConst.PROPERTY_USER_INFOR)
        {
            Current.Ins.UserAvatar((result) =>
            {
                if (_avatarPath != result.Path)
                {
                    avatar.GetComponent<RawImage>().texture = result.Texture;
                    _avatarPath = result.Path;
                }
            });

            //Current.Ins.UserAvatarFrame((result) =>
            //{
            //    if (_avatarFramePath != result.Path)
            //    {
            //        frame.GetComponent<RawImage>().texture = result.Texture;
            //        _avatarFramePath = result.Path;
            //    }
            //});

            //BingoMMR.text = $"MMRBingo: {Current.Ins.player.BingoMMR}";
            //SolitaireMMR.text = $"MMRSolitaire: {Current.Ins.player.SolitaireMMR}";
            //BubbleMMR.text = $"MMRBubble: {Current.Ins.player.BubbleMMR}";
        }
    }

    private void OnDisable()
    {
        Current.Ins.PropertyChanged -= User_PropertyChanged;
    }

    private bool _isHide = false;
    private void HideHeader(bool isHide = true)
    {
        _isHide = isHide;

        if (isHide)
        {
            _initY = gameObject.transform.position.y;

            gameObject.transform
                .DOMoveY(OutTransform.position.y, 0.3f)
                .SetEase(Ease.InOutElastic);
        }
        else
        {
            gameObject.transform
                .DOMoveY(_initY, 0.3f)
                .SetEase(Ease.InOutElastic);
        }
    }

    private IEnumerator LazyHideHeader()
    {
        avatar.GetComponent<Button>().onClick.RemoveListener(OnClickAvatar);
        yield return new WaitForSeconds(3f);
        HideHeader(true);
        avatar.GetComponent<Button>().onClick.AddListener(OnClickAvatar);
    }

    private void OnClickAvatar()
    {
        if (CoinView.GetComponent<CoinView>().IsCoinMoving) return;

        profileView.SetActive(true);
        profileView.GetComponent<ProfileView>().OnClose = () => HideHeader(false);

        HideHeader(true);
    }
}
