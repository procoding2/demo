﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Localization.Components;
using Newtonsoft.Json.Linq;
using OutGameEnum;
using DG.Tweening;
using HomeTab;
using System;

public class ConfirmFeePopup : MonoBehaviour
{
    public GameObject blackPanel = null;


    public Button Confirm;
    public Button cancel;
    public LocalizeStringEvent content;
    //
    public GameObject warningNeedCoinPopup;

    private void Awake()
    {
        cancel.onClickWithHCSound(() =>
        {
            blackPanel?.SetActive(false);
            this.transform.DOScale(Vector3.zero, 0.1f);
            if (UIHome.Ins)
                UIHome.Ins.headerView.EnableClick(true);
        });
    }

    public void Init(ConfirmFeePopupData data, Action confirmAction, bool open = true)
    {
        string contentTxt = string.Empty;

        if (data.TotalItem == 0)
        {
            confirmAction.Invoke();
            return;
        }       
        Confirm.onClick.RemoveAllListeners();
        Confirm.onClickWithHCSound(() =>
        {         
            if (UIHome.Ins)
            {                
                UIHome.Ins.headerView.EnableClick(true);
            }
            blackPanel?.SetActive(false);
            transform.DOScale(Vector3.zero, 0f);
            confirmAction.Invoke();
        });

        if (data.TotalItem == 1)
        {
            if (data.Token > 0) contentTxt = $"{data.Token} Credit";
            else if (data.Gold > 0) contentTxt = $"{data.Gold} Gold";
            else contentTxt = $"{data.Ticket} Ticket";
        }
        else if (data.TotalItem == 2)
        {
            bool isFirst = true;

            if (data.Token > 0)
            {
                contentTxt = $"{data.Token} Credit";
                isFirst = false;
            }

            if (data.Gold > 0)
            {
                if (isFirst) contentTxt = $"{data.Gold} Gold";
                else contentTxt += $", {data.Gold} Gold";

                isFirst = false;
            }

            if (data.Ticket > 0) contentTxt += $", {data.Ticket} Ticket";
        }
        else
        {
            contentTxt = $"{data.Token} Credit, {data.Gold} Gold, {data.Ticket} Ticket";
        }

        content.UpdateValue("content", VariableEnum.String, contentTxt);

        if (open)
        {
            this.transform.DOScale(Vector3.one, 0.1f);
            blackPanel?.SetActive(true);
        }
            
        if (UIHome.Ins)
            UIHome.Ins.headerView.EnableClick(false);
    }

    public bool IsShowWarning(ConfirmFeePopupData fee)
    {
        return Current.Ins.player.GoldCoin < fee.Gold
            || Current.Ins.player.HCTokenCoin < fee.Token
            || Current.Ins.player.TicketCoin < fee.Ticket;
    }
}
