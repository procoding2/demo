using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UniRx;
using UnityEngine.UI;
using DG.Tweening;
using OutGameEnum;
using TMPro;

public class CoinView : MonoBehaviour
{
    public GameObject ParentGameObject;
    public GameObject tokenValue;
    public GameObject goldValue;
    public GameObject ticketValue;

    public GameObject GoldPrefab;
    public GameObject TokenPrefab;
    public GameObject TicketPrefab;

    public Transform GoldDes;
    public GameObject GoldLightDes;

    public Transform TokenDes;
    public GameObject TokenLightDes;

    public Transform TicketDes;
    public GameObject TicketLightDes;

    public GameObject valueAddGoldPrefab;
    public GameObject valueAddTokenPrefab;
    public GameObject valueAddTicketPrefab;

    public Ease easeAnimation;
    public float spread;

    private decimal _gold;
    private decimal _token;
    private decimal _ticket;

    public bool IsCoinMoving { get; private set; }

    public IDisposable playerCoinRx()
    {
        return Current.Ins.playerCoinRx
            .DistinctUntilChanged()
            .Subscribe((playerCoin) =>
            {
                GoldLightDes.SetActive(false);
                TokenLightDes.SetActive(false);
                TicketLightDes.SetActive(false);

                GoldDes.localScale = Vector3.one;
                TokenDes.localScale = Vector3.one;
                TicketDes.localScale = Vector3.one;

                if (playerCoin.transform != null && playerCoin.CoinTypes.Count > 0)
                {
                    IsCoinMoving = true;
                    StartCoroutine(CoinAnimation(
                        playerCoin.CoinTypes,
                        callBackAfterEffect: () =>
                        {
                            IsCoinMoving = false;

                            goldValue.GetComponent<TextMeshProUGUI>().text = StringHelper.NumberFormatter(playerCoin.Gold);
                            tokenValue.GetComponent<TextMeshProUGUI>().text = StringHelper.NumberFormatter(playerCoin.Token);
                            ticketValue.GetComponent<TextMeshProUGUI>().text = StringHelper.NumberFormatter(playerCoin.Ticket);

                            _gold = playerCoin.Gold;
                            _token = playerCoin.Token;
                            _ticket = playerCoin.Ticket;

                            playerCoin.CallBackAfterEffect?.Invoke();
                        },
                        callBackGold: () =>
                        {
                            goldValue.GetComponent<TextMeshProUGUI>().text = StringHelper.NumberFormatter(playerCoin.Gold);
                        },
                        callBackToken: () =>
                        {
                            tokenValue.GetComponent<TextMeshProUGUI>().text = StringHelper.NumberFormatter(playerCoin.Token);
                        },
                        callBackTicket: () =>
                        {
                            ticketValue.GetComponent<TextMeshProUGUI>().text = StringHelper.NumberFormatter(playerCoin.Ticket);
                        },
                        playerCoin.transform.position,
                        playerCoin.desTransform)); ;
                }

                else
                {
                    goldValue.GetComponent<TextMeshProUGUI>().text = StringHelper.NumberFormatter(playerCoin.Gold);
                    tokenValue.GetComponent<TextMeshProUGUI>().text = StringHelper.NumberFormatter(playerCoin.Token);
                    ticketValue.GetComponent<TextMeshProUGUI>().text = StringHelper.NumberFormatter(playerCoin.Ticket);

                    _gold = playerCoin.Gold;
                    _token = playerCoin.Token;
                    _ticket = playerCoin.Ticket;
                }
            });
    }

    private Action _onBeforeAnimation;
    private IDisposable _dp;
    public void Init(Action onBeforeAnimation)
    {
        _onBeforeAnimation = onBeforeAnimation;
        _dp = playerCoinRx();
        var userInfo = Current.Ins.player;
        tokenValue.GetComponent<TextMeshProUGUI>().text = StringHelper.NumberFormatter(userInfo.HCTokenCoin);
        goldValue.GetComponent<TextMeshProUGUI>().text = StringHelper.NumberFormatter(userInfo.GoldCoin);
        ticketValue.GetComponent<TextMeshProUGUI>().text = StringHelper.NumberFormatter(userInfo.TicketCoin);
        Current.Ins.ReloadUserProfile();
    }

    Sequence sq;
    private IEnumerator CoinAnimation(
        List<(CoinEnum, int)> coinTypes,
        Action callBackAfterEffect,
        Action callBackGold,
        Action callBackToken,
        Action callBackTicket,
        Vector3 startPos,
        Transform endPos = null)
    {
        _onBeforeAnimation.Invoke();

        yield return new WaitForSeconds(0.4f);
        SoundManager.instance.PlayUIEffect("se_ring");
        sq = DOTween.Sequence();
        int totalDone = 0;
        foreach (var coinType in coinTypes)
        {
            GameObject objPrefab = null;
            Transform desTransform = null;
            GameObject iconLight = null;

            switch (coinType.Item1)
            {
                case CoinEnum.Gold:
                    objPrefab = GoldPrefab;
                    desTransform = GoldDes;
                    iconLight = GoldLightDes;
                    break;
                case CoinEnum.Token:
                    objPrefab = TokenPrefab;
                    desTransform = TokenDes;
                    iconLight = TokenLightDes;
                    break;
                case CoinEnum.Ticket:
                    objPrefab = TicketPrefab;
                    desTransform = TicketDes;
                    iconLight = TicketLightDes;
                    break;
            }

            if (endPos != null) desTransform = endPos;
            Transform parent = ParentGameObject == null ? transform : ParentGameObject.transform;

            GameObject valuePrefab = null;
            if (coinType.Item1 == CoinEnum.Gold)
            {
                valuePrefab = valueAddGoldPrefab;
            }
            else if (coinType.Item1 == CoinEnum.Token)
            {
                valuePrefab = valueAddTokenPrefab;
            }
            else
            {
                valuePrefab = valueAddTicketPrefab;
            }

            var valueAdd = Instantiate(valuePrefab, parent, false);
            valueAdd.GetComponent<TextMeshProUGUI>().text = $"+{StringHelper.NumberFormatter(coinType.Item2)}";
            valueAdd.transform.position = startPos;
            valueAdd.transform.localScale = Vector3.zero;

            valueAdd.transform.DOScale(Vector3.one, 0.3f);
            valueAdd.transform.DOMoveY(valueAdd.transform.position.y + 0.5f, 1f).OnComplete(() =>
            {
                valueAdd.transform.DOScale(Vector3.zero, 0.3f).OnComplete(() => Destroy(valueAdd));
            });

            int valuePerCoin = 1;
            int totalCoin = coinType.Item2;
            if (coinType.Item2 > 20)
            {
                valuePerCoin = coinType.Item2 / 20;
                totalCoin = 20;
            }

            int totalDoneByCoin = 0;
            for (int i = 0; i < totalCoin; i++)
            {
                yield return new WaitForSeconds(0.05f);
                var obj = Instantiate(objPrefab, parent, false);
                obj.transform.position = startPos;

                float randX = UnityEngine.Random.Range(-0.5f, 0.5f);
                float randY = UnityEngine.Random.Range(-0.5f, 0.5f);
                float randTime = UnityEngine.Random.Range(1f, 1.5f);

                var strPos = startPos + new Vector3(randX, randY, 0f);
                obj.transform.localScale = Vector3.zero;

                obj.transform.DOMove(new Vector3(strPos.x, strPos.y, obj.transform.position.z), 0.2f)
                    .OnComplete(() =>
                    {
                        obj.transform
                        .DOMove(desTransform.position, randTime)
                        .SetEase(easeAnimation)
                        .OnComplete(() =>
                        {
                            SoundManager.instance.PlayUIEffect("se_buy");
                            if (sq != null)
                            {
                                sq.Kill();
                                sq = DOTween.Sequence();
                                desTransform.localScale = Vector3.one;
                                iconLight.transform.localScale = Vector3.zero;
                            }

                            sq.Join(iconLight.transform.DOScale(Vector3.one, 0.1f).OnComplete(() => iconLight.transform.localScale = Vector3.zero));
                            sq.Join(desTransform.DOScale(new Vector3(1.3f, 1.3f, 1.3f), 0.2f).SetLoops(2, LoopType.Yoyo));
                            Destroy(obj);

                            switch (coinType.Item1)
                            {
                                case CoinEnum.Gold:
                                    _gold += valuePerCoin;
                                    goldValue.GetComponent<TextMeshProUGUI>().text = StringHelper.NumberFormatter(_gold);
                                    break;
                                case CoinEnum.Token:
                                    _token += valuePerCoin;
                                    tokenValue.GetComponent<TextMeshProUGUI>().text = StringHelper.NumberFormatter(_token);
                                    break;
                                case CoinEnum.Ticket:
                                    _ticket += valuePerCoin;
                                    ticketValue.GetComponent<TextMeshProUGUI>().text = StringHelper.NumberFormatter(_ticket);
                                    break;
                            }

                            totalDoneByCoin++;
                            if (totalDoneByCoin == totalCoin)
                            {
                                switch (coinType.Item1)
                                {
                                    case CoinEnum.Gold:
                                        callBackGold.Invoke();
                                        break;
                                    case CoinEnum.Token:
                                        callBackToken.Invoke();
                                        break;
                                    case CoinEnum.Ticket:
                                        callBackTicket.Invoke();
                                        break;
                                }

                                totalDone++;

                                if (totalDone == coinTypes.Count)
                                {
                                    Debug.Log("Done");
                                    callBackAfterEffect?.Invoke();
                                }
                            }
                        });
                    });

                obj.transform.DOScale(1.3f, 0.5f);
            }

            yield return new WaitForSeconds(0.4f);
        }
    }

    private void OnDisable()
    {
        if (sq != null) sq.Kill();
        _dp.Dispose();
    }
}