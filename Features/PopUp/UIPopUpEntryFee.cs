using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIPopUpEntryFee : UIPopUpBaseObject
{
    [SerializeField]
    private TextMeshProUGUI[] entryFeeList;

    public void ShowPopUpInfo(ConfirmFeePopupData data, Action confirmAction)
    {
        for (int i = 0; i < 3; i++)
        {
            entryFeeList[i].gameObject.SetActive(false);
        }
        if (data.Gold > 0)
        {
            entryFeeList[0].gameObject.SetActive(true);
            entryFeeList[0].text = $"{data.Gold:n0}";
        }
        if (data.Token > 0)
        {
            entryFeeList[1].gameObject.SetActive(true);
            entryFeeList[1].text = $"{data.Token:n0}";
        }
        if (data.Ticket > 0)
        {
            entryFeeList[2].gameObject.SetActive(true);
            entryFeeList[2].text = $"{data.Ticket:n0}";
        }

        OKBtn.onClick.AddListener(() =>
        {
            if (IsShowWarning(data))
            {
                UIPopUpManager.instance.ShowPopUpBase(PopUpActionType.Btn1, TemplateManager.instance.GetLocalText("Home:dont_have_enough_coin_to_join"));
            }
            else
            {
                UIPopUpManager.instance.ShowLoadingCircle(true);
                confirmAction.Invoke();
            }
        });
    }



    private bool IsShowWarning(ConfirmFeePopupData fee)
    {
        return Current.Ins.player.GoldCoin < fee.Gold
            || Current.Ins.player.HCTokenCoin < fee.Token
            || Current.Ins.player.TicketCoin < fee.Ticket;
    }
}
