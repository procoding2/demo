using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class UIPopUpGuest : UIPopUpBaseObject
{

    [SerializeField]
    private TMP_InputField inputField;

    [SerializeField]
    private Text quantityByteText;


    private const int _maxByteInput = 20;
    // Start is called before the first frame update
    void Start()
    {


        okBtn.onClickWithHCSound(() =>
        {
            var inputTxt = inputField.text;
            UIWelcome.Ins.onClickLogin(4, inputTxt);
            UIPopUpManager.instance.ClosePopUpEvent();

        });

        quantityByteText.text = "0/" + _maxByteInput;
        inputField.text = $"Guest{TimeManager.CurrentTimeInSecond}";
    }


    public void CheckQuantity(string str)
    {
        quantityByteText.text = StringHelper.QuantityInput(str) + "/" + _maxByteInput;
        okBtn.interactable = !string.IsNullOrWhiteSpace(str);
    }


}
