﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum PopUpActionType
{
    none,
    Btn1,
    Btn2,
    Btn3,

    alram = 20,

}

public class UIPopUpManager : MonoBehaviour
{


    [SerializeField]
    private Transform centerForm = null;

    [SerializeField]
    private GameObject blackBG_prefabs = null;

    [SerializeField]
    private GameObject loadingCircle = null;


    [SerializeField]
    private Stack<GameObject> popUpObjList = new Stack<GameObject>();

    private static bool isApplicationQuit = false;
    private static object m_Lock = new object();
    private static UIPopUpManager m_instance;
    public static UIPopUpManager instance
    {
        get
        {
            lock (m_Lock)
            {
                if (m_instance == null && isApplicationQuit == false)
                {
                    GameObject coreGameObject = Instantiate(Resources.Load("UI/Canvas_PopUpManager")) as GameObject;
                    if (coreGameObject != null)
                    {
                        m_instance = coreGameObject.GetComponent<UIPopUpManager>();
                        
                    }
                }

                return m_instance;
            }
        }
    }
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        loadingCircle.SetActive(false);
    }

    private void OnApplicationQuit()
    {
        isApplicationQuit = true;
    }


    public void ShowLoadingCircle(bool isShow)
    {
        if (isShow)
            loadingCircle.transform.SetAsLastSibling();

        loadingCircle.SetActive(isShow);
    }


    public GameObject MakeBlackBGObject()
    {
        transform.SetAsLastSibling();
        GameObject blackBGObj = Instantiate(blackBG_prefabs) as GameObject;
        if (blackBGObj)
        {
            blackBGObj.transform.SetParent(transform);
            blackBGObj.transform.localScale = Vector3.one;

            RectTransform rt = blackBGObj.GetComponent<RectTransform>();
            rt.offsetMin = rt.offsetMax = Vector2.zero;
            popUpObjList.Push(blackBGObj);

        }
        return blackBGObj;
    }
    public GameObject ShowPopUpPrefabs(GameObject prefabs, string message = null)
    {

        GameObject popUpObj = Instantiate(prefabs);
        if (popUpObj)
        {
            return ShowPopUpObject(popUpObj, message);
        }
        return null;
    }
    public GameObject ShowPopUpFileName(string fileName, string message = null)
    {

        GameObject popUpObj = Instantiate(Resources.Load<GameObject>("UI/" + fileName));
        if (popUpObj)
        {
            return ShowPopUpObject(popUpObj, message);
        }
        return null;
    }
    public GameObject ShowPopUpObject(GameObject popUpObj, string message = null)
    {
        GameObject blackBGObj = MakeBlackBGObject();
        if (blackBGObj)
        {
            blackBGObj.transform.SetAsLastSibling();
            if (popUpObj)
            {
                UIPopUpBaseObject baseObj = popUpObj.GetComponent<UIPopUpBaseObject>();
                if (baseObj && message != null)
                {
                    baseObj.MainMessage.text = message;
                }


                popUpObj.transform.position = centerForm.position;
                popUpObj.transform.SetParent(blackBGObj.transform);
                popUpObj.transform.localScale = Vector3.one;

                return popUpObj;
            }

        }
        return null;
    }
    public GameObject ShowPopUpLoadObject(GameObject prefabs, string message = null)
    {
        GameObject blackBGObj = MakeBlackBGObject();
        if (blackBGObj)
        {
            blackBGObj.transform.SetAsLastSibling();

            GameObject popUpObj = Instantiate(prefabs);
            if (popUpObj)
            {
                UIPopUpBaseObject baseObj = popUpObj.GetComponent<UIPopUpBaseObject>();
                if (baseObj && message != null)
                {
                    baseObj.MainMessage.text = message;
                }


                popUpObj.transform.position = centerForm.position;
                popUpObj.transform.SetParent(blackBGObj.transform);
                popUpObj.transform.localScale = Vector3.one;

                return popUpObj;
            }

        }
        return null;
    }

    public GameObject ShowPopUpBase(PopUpActionType type, string message)
    {
        string fileName = "";

        if (type == PopUpActionType.Btn1)
        {
            fileName = "PopUp_1btn";
        }
        else if (type == PopUpActionType.Btn2)
        {
            fileName = "PopUp_2btn";
        }
        else if (type == PopUpActionType.Btn3)
        {
            fileName = "PopUp_3btn";
        }
        GameObject popUpObj = Instantiate(Resources.Load<GameObject>("UI/" + fileName));
        return ShowPopUpObject(popUpObj, message);


    }


    public APIErrorPopup ShowErrorPopUp(string errorMessage, BaseResponse response = null, Action tryAction = null, bool isCloseBtn = false)
    {
        GameObject popUpObj = Instantiate(Resources.Load<GameObject>("UI/APIErrorPopUp"));
        if (popUpObj)
        {

            if (response != null)
            {
                errorMessage += $"\nErr code: {response.errorCode}, Msg: {response.message}";
            }
            GameObject errorPop = ShowPopUpObject(popUpObj, errorMessage);
            if (errorPop)
            {
                APIErrorPopup apiErrorPop = errorPop.GetComponent<APIErrorPopup>();
                if (apiErrorPop)
                {
                    apiErrorPop.SetParam(tryAction, isCloseBtn);
                    return apiErrorPop;
                }
            }
        }

        return null;
    }
    public APIErrorPopup ShowErrorPopUpForGoHome(string errorMessage, BaseResponse response = null, OutGameEnum.SceneEnum goScene = OutGameEnum.SceneEnum.Home)
    {
        GameObject popUpObj = Instantiate(Resources.Load<GameObject>("UI/APIErrorPopUp"));
        if (popUpObj)
        {

            if (response != null)
            {
                errorMessage += $"\nErr code: {response.errorCode}, Msg: {response.message}";
            }
            GameObject errorPop = ShowPopUpObject(popUpObj, errorMessage);
            if (errorPop)
            {
                APIErrorPopup apiErrorPop = errorPop.GetComponent<APIErrorPopup>();
                if (apiErrorPop)
                {
                    apiErrorPop.SetParamGoHome(goScene);
                    return apiErrorPop;
                }
            }
        }

        return null;
    }

    public void ClosePopUpEvent()
    {

        if (popUpObjList.Count > 0)
        {
            GameObject removeObj = popUpObjList.Pop();
            Destroy(removeObj);
        }

    }
    public void CloseAllPopUpEvent()
    {
        int count = popUpObjList.Count;
        for (int i = 0; i < count; i++)
        {
            if (popUpObjList.Count > 0)
            {
                GameObject removeObj = popUpObjList.Pop();
                Destroy(removeObj);
            }
        }

        ShowLoadingCircle(false);
    }
    public bool IsPopUpObject()
    {
        if (popUpObjList.Count > 0)
            return true;

        return false;
    }


    public void OKActionEvent()
    {
        ClosePopUpEvent();
    }

    public void OK2ActionEvent()
    {
        ClosePopUpEvent();
    }


    public bool isOnPopUp() => (popUpObjList.Count > 0);
}

