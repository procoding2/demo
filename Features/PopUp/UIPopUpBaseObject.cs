﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIPopUpBaseObject : MonoBehaviour
{
    [SerializeField]
    protected TextMeshProUGUI mainMessage;
    public TextMeshProUGUI MainMessage => mainMessage;

    [SerializeField]
    protected Button closeBtn = null, okBtn = null, ok2Btn = null;

    public Button CloseBtn => closeBtn;
    public Button OKBtn => okBtn;
    public Button OK2Btn => ok2Btn;

    private void Awake()
    {

    }

    public void OkEvent()
    {
        UIPopUpManager.instance.OKActionEvent();
    }

    public void Ok2Event()
    {
        UIPopUpManager.instance.OK2ActionEvent();
    }

    public void CancelEvent()
    {
        SoundManager.instance.PlayUIEffect("se_cancel");
        UIPopUpManager.instance.ClosePopUpEvent();
    }



}

