using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TimeView : MonoBehaviour
{

    public Text timeTxt;
    public Animator timeSecAni;
    public GameObject countDownText;
    public Transform countDownPos;


    // Start is called before the first frame update
    void Start()
    {
        ObjectPoolManager.instance.ReservePool(countDownText, 1, true);
    }



    public void SetTimeTxt(string txt)
    {
        timeTxt.text = txt;
    }

    public void SetSecAni(bool isRunning)
    {
        timeSecAni.SetBool("timePlay", isRunning);
    }

    public void SetCountDownText(string txt)
    {

        GameObject obj = ObjectPoolManager.instance.GetObject(countDownText.name);
        if (obj)
        {
            obj.transform.position = countDownPos.position;
            obj.transform.localScale = Vector3.one;
            obj.GetComponent<TextMeshProUGUI>().text = txt;
            obj.SetActive(true);

            SoundManager.instance.PlayUIEffect("se_select");
        }
    }
}
