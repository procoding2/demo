using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class AutoHiddenObj : MonoBehaviour
{
    public float remainTime = 1.0f;
    public bool IsDestroy = false;
    public bool IsSmoothHidden = true;
    [Range(1, 20)]
    public float smoothHiddenSpeed = 10.0f;
    



    private void OnEnable()
    {
        SetAllComponentAlpha(1.0f);
        StartCoroutine(HiddenProcess());
    }
    private void OnDisable()
    {
        StopAllCoroutines();
    }
    IEnumerator HiddenProcess()
    {
        yield return new WaitForSeconds(remainTime);

        if (IsSmoothHidden)
        {
            float alpha = 1.0f;
            while (alpha > 0.0f)
            {
                SetAllComponentAlpha(alpha);

                alpha -= Time.deltaTime * smoothHiddenSpeed;
                yield return null;
            }
        }

        if (IsDestroy)
            Destroy(gameObject);
        else
            gameObject.SetActive(false);
    }


    void SetAllComponentAlpha(float alpha)
    {
        foreach (Text obj in GetComponents(typeof(Text)))
        {
            Color color = obj.color;
            color.a = alpha;
            obj.color = color;
        }
        foreach (TextMeshProUGUI obj in GetComponents(typeof(TextMeshProUGUI)))
        {
            Color color = obj.color;
            color.a = alpha;
            obj.color = color;
        }
        foreach (Image obj in GetComponents(typeof(Image)))
        {
            Color color = obj.color;
            color.a = alpha;
            obj.color = color;
        }
        foreach (SpriteRenderer obj in GetComponents(typeof(SpriteRenderer)))
        {
            Color color = obj.color;
            color.a = alpha;
            obj.color = color;
        }
    }
}
