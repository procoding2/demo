using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class APIErrorPopup : UIPopUpBaseObject
{
    [SerializeField]
    private Button HomeBtn;

    [SerializeField]
    private Button TryAgainBtn;

    public Action _tryAgainAction = null;

    private void Start()
    {
        //CloseBtn.GetComponent<Button>().onClickWithHCSound(() =>
        //{
        //    gameObject.SetActive(false);
        //    //_closeAction?.Invoke();
        //});

        //Close2Btn.GetComponent<Button>().onClickWithHCSound(() =>
        //{
        //    gameObject.SetActive(false);
        //    //_closeAction?.Invoke();
        //});

        //TryAgainBtn.GetComponent<Button>().onClickWithHCSound(() =>
        //{
        //    gameObject.SetActive(false);
        //    _tryAgainAction?.Invoke();
        //});
    }

    public void SetParam(Action tryAgainAction = null, bool isShowCloseBtn = true)
    {
        HomeBtn.gameObject.SetActive(false);
        TryAgainBtn.gameObject.SetActive(true);
        
        _tryAgainAction = tryAgainAction;
    }
    public void SetParamGoHome(OutGameEnum.SceneEnum goScene)
    {
        HomeBtn.gameObject.SetActive(true);
        TryAgainBtn.gameObject.SetActive(false);

        _tryAgainAction = () => SceneHelper.LoadScene((goScene == OutGameEnum.SceneEnum.Home) ? OutGameEnum.SceneEnum.Home : OutGameEnum.SceneEnum.Welcome);

    }

    public void TryAgainAction()
    {
        //_tryAgainAction?.Invoke();
        //우선 모든 닫기는 첫화면으로 이동시킴
        SceneHelper.LoadScene(OutGameEnum.SceneEnum.Welcome);

        CancelEvent();
    }

}
