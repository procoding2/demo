using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class LoadingCircle : MonoBehaviour
{
    // Update is called once per frame
    void Update() => gameObject.transform.RotateAround(gameObject.transform.position, Vector3.back, 1.2f * 360 * Time.deltaTime);
}
