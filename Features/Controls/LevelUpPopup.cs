using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LevelUpPopup : MonoBehaviour
{
    public GameObject textLevel;
    public GameObject goldRewardLayout;
    public GameObject goldTxt;
    public GameObject tokenRewardLayout;
    public GameObject tokenTxt;
    public GameObject ticketRewardLayout;
    public GameObject ticketTxt;
    public GameObject claimBtn;

    private void Start()
    {
        claimBtn.GetComponent<Button>().onClickWithHCSound(() =>
        {
            gameObject.SetActive(false);
        });
    }

    public void Init(UserProfileResponse.Data dataLevelUp)
    {
        gameObject.SetActive(true);
        textLevel.GetComponent<TextMeshProUGUI>().text = dataLevelUp.level.ToString();
        goldRewardLayout.SetActive(dataLevelUp.gold > 0);
        tokenRewardLayout.SetActive(dataLevelUp.token > 0);
        ticketRewardLayout.SetActive(dataLevelUp.ticket > 0);

        goldTxt.GetComponent<TextMeshProUGUI>().text = $"x{dataLevelUp.gold}";
        tokenTxt.GetComponent<TextMeshProUGUI>().text = $"x{dataLevelUp.token}";
        ticketTxt.GetComponent<TextMeshProUGUI>().text = $"x{dataLevelUp.ticket}";
        Debug.Log(dataLevelUp.level.ToString());
    }
}
