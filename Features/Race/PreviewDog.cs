using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreviewDog : MonoBehaviour
{

    [SerializeField]
    private Material[] dogsMaterials;

    private Renderer dogRenderer => GetComponentInChildren<Renderer>();

    private Animator aniCon => GetComponent<Animator>();



    public void SetWinnerDog(int index)
    {
        dogRenderer.material = dogsMaterials[index];
    }
}
