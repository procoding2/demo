using System;
using UnityEngine;
using TMPro;
using DG.Tweening;


public class MyPickInfo : MonoBehaviour
{
    [SerializeField]
    private GameObject[] dogSprites;

    [SerializeField]
    private TextMeshProUGUI betCoin;

    [SerializeField]
    private Transform posMove;

    public void ShowMyPickInfoView(int selectIndex, long coin)
    {
        Array.ForEach(dogSprites, obj => obj.SetActive(false));
        if (selectIndex <= dogSprites.Length)
        {
            dogSprites[selectIndex - 1].SetActive(true);
            betCoin.text =  $"{coin:n0}";


            Sequence sq = DOTween.Sequence();
            sq.Join(transform.DOMoveX(posMove.position.x, 0.5f).SetEase(Ease.InOutQuad));
            sq.Play();
        }
    }
}
