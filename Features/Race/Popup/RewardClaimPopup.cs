using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RewardClaimPopup : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI betsTxt, rateTxt, jackpotTxt, prizeTxt;

    private int roomID = 0;
    private Button historyClaimBtn;

    [SerializeField] private Button claim;

    public void ShowRewardClaimPopup(int _roomID, Button _historyClaimBtn, RaceHistoryResponse.Betting bet, RaceUserRewardResponese reward)
    {
        roomID = _roomID;
        historyClaimBtn = _historyClaimBtn;

        betsTxt.text = $"{bet.amount:n0}";
        rateTxt.text = $"x{reward.data.rate:0.00}";
        jackpotTxt.text = $"{reward.data.jackpot:n0}";
        prizeTxt.text = $"{reward.data.total:n0}";

        gameObject.SetActive(true);
    }



    public void ClaimAction()
    {
        UIPopUpManager.instance.ShowLoadingCircle(true);
        StartCoroutine(Current.Ins.raceApi.claimRewardRace(roomId: roomID, (res) =>
        {
            if (res == null || res.errorCode != 0)
            {
                Debug.Log("Claim Error:");
                UIPopUpManager.instance.ShowErrorPopUp("Request Error : ClaimAction", res, ClaimAction);
                return;
            }
            historyClaimBtn.interactable = false;
            //PickRace.Ins.changeOwnedAssets();
            UIPopUpManager.instance.ShowLoadingCircle(false);
            UIRace.Ins.ReloadUserProfile(claim.transform, () =>
            {
                ClosePop();
            });

        }));
    }

    public void ClaimActionFish()
    {
        UIPopUpManager.instance.ShowLoadingCircle(true);
        StartCoroutine(Current.Ins.fishApi.claimRewardFish(roomId: roomID, (res) =>
        {
            if (res == null || res.errorCode != 0)
            {
                UIPopUpManager.instance.ShowErrorPopUp("Request Error : ClaimActionFish", res, ClaimActionFish);

                return;
            }
            historyClaimBtn.interactable = false;
            //PickRace.Ins.changeOwnedAssets();
            UIPopUpManager.instance.ShowLoadingCircle(false);
            UIFish.Ins.ReloadUserProfile(claim.transform, () =>
            {
                ClosePop();
            });

        }));
    }

    public void ClosePop()
    {
        gameObject.SetActive(false);
    }
}
