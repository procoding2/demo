﻿using OutGameEnum;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Localization.Components;
using UnityEngine.UI;

public class WinRacePopup : MonoBehaviour
{

    [SerializeField]
    private TextMeshProUGUI raceResultTxt;

    [SerializeField]
    private TextMeshProUGUI bettingTitleTxt;

    [SerializeField]
    private TextMeshProUGUI bettingTxt;

    [SerializeField]
    private TextMeshProUGUI rateTxt;

    [SerializeField]
    private TextMeshProUGUI jackpot;

    [SerializeField]
    private TextMeshProUGUI totalTxt;

    [SerializeField]
    private TextMeshProUGUI closeTxt;
    [SerializeField]
    private LocalizeStringEvent timeClose;

    [SerializeField]
    private Button claimBtn;

    [SerializeField]
    private PreviewDog previewDog = null;

    [SerializeField]
    private GameObject infoObj, winTitleObj, loseTitleObj, renderImageObj;

    [SerializeField]
    private List<GameObject> showResultObjs;

    [SerializeField]
    private Transform resultPos, drawPos;

    private int _roomId;

    private float betValue = 0.0f, rateValue = 0.0f, jackpotValue = 0.0f, totalValue = 0.0f, exValue = 0.0f;

    public void initView(string title, RaceUserRewardResponese userReward, int roomId)
    {
        initView(title, userReward.data.amountBetting, userReward.data.amount, userReward.data.rate, userReward.data.jackpot, userReward.data.total, roomId);
    }

    public void initView(string title, int _bet, int _amount, float _rate, int _jackpot, int _total,  int roomId)
    {
        int winIndex = DogRaceManager.instance.GetLastRaceFixedWinner();
        int pickIndex = PickRace.Ins.myPickIndex;

        bool isWinGame = (winIndex + 1) == pickIndex;


        winTitleObj.SetActive(isWinGame);
        loseTitleObj.SetActive(!isWinGame);


        renderImageObj.transform.position = resultPos.position;

        //
        LanguageEnum language = Current.Ins.Config.GetCurrentLanguage();
        

        string titleWinner = "";
        string colorName = "";

        switch (language)
        {
            case LanguageEnum.eng:
                colorName = winIndex switch { 0 => "Red", 1 => "Yellow", 2 => "Green" };
                titleWinner = $"Winner is the {colorName} dog!";
                break;
            case LanguageEnum.kor:
                colorName = winIndex switch { 0 => "레드", 1 => "옐로", 2 => "그린" };
                titleWinner = $"승자는 레인 {colorName} 입니다!";
                break;
            case LanguageEnum.vi:
                colorName = winIndex switch { 0 => "Red", 1 => "Yellow", 2 => "Green" };
                titleWinner = $"Làn số {colorName} giành chiến thắng!";
                break;
        }
        raceResultTxt.text = titleWinner;

        previewDog.SetWinnerDog(winIndex);
        _roomId = roomId;


        bettingTitleTxt.text = title;

        infoObj.SetActive(true);
        claimBtn.gameObject.SetActive(false);
        closeTxt.gameObject.SetActive(false);

        showResultObjs.ForEach(obj => obj.SetActive(false));


        betValue = (float)_bet;
        rateValue = _rate;
        jackpotValue = (float)_jackpot;
        totalValue = (float)_total;


        exValue = 0.0f;

        bettingTxt.text = "0";//$"{_bet:n0}";
        jackpot.text = "0";//$"{_jackpot:n0}";
        totalTxt.text = "0";//$"{_total:n0}";

        gameObject.SetActive(true);


        StartCoroutine(ShowingResultValueUpdate(_total > 0));

    }
    public void AutoCloseView()
    {
        int winIndex = DogRaceManager.instance.GetLastRaceFixedWinner();
        LanguageEnum language = Current.Ins.Config.GetCurrentLanguage();
        string titleWinner = "";
        string colorName = "";
        switch (language)
        {
            case LanguageEnum.eng:
                colorName = winIndex switch { 0 => "Red", 1 => "Yellow", 2 => "Green" };
                titleWinner = $"Winner is the {colorName} dog!";
                break;
            case LanguageEnum.kor:
                colorName = winIndex switch { 0 => "레드", 1 => "옐로", 2 => "그린" };
                titleWinner = $"승자는 {colorName} 입니다!";
                break;
            case LanguageEnum.vi:
                colorName = winIndex switch { 0 => "Red", 1 => "Yellow", 2 => "Green" };
                titleWinner = $"Làn số {colorName} giành chiến thắng!";
                break;
        }
        raceResultTxt.text = titleWinner;

        previewDog.SetWinnerDog(winIndex);
        bettingTitleTxt.gameObject.SetActive(false);


        winTitleObj.SetActive(false);
        loseTitleObj.SetActive(false);


        infoObj.SetActive(false);
        claimBtn.gameObject.SetActive(false);
        closeTxt.gameObject.SetActive(false);


        renderImageObj.transform.position = drawPos.position;

        gameObject.SetActive(true);

        StartCoroutine(DelayClose());


    }

    private IEnumerator ShowingResultValueUpdate(bool isShowClaimBtn)
    {
        int seqIndex = 0;
        float goalValue = 0.0f;
        TextMeshProUGUI updateText = null;

        yield return new WaitForSeconds(0.5f);


        while (seqIndex < 4)
        {
            switch (seqIndex)
            {
                case 0: //bet
                    goalValue = betValue;
                    updateText = bettingTxt;
                    break;
                case 1: //rate
                    goalValue = rateValue;
                    updateText = rateTxt;
                    break;
                case 2: //jackpot
                    goalValue = jackpotValue;
                    updateText = jackpot;
                    break;
                case 3: //total
                    goalValue = totalValue;
                    updateText = totalTxt;
                    break;


            }
            exValue = 0.0f;
            showResultObjs[seqIndex].SetActive(true);

            yield return new WaitForSeconds(0.25f);
            SoundManager.instance.PlayUIEffect("se_type2");

            if (seqIndex == 1)
            {
                updateText.text = $"x{goalValue:0.00}";
            }
            else
            {
                updateText.text = $"{(int)goalValue:n0}";
            }

            seqIndex++;
        }
        yield return new WaitForSeconds(0.5f);


        claimBtn.gameObject.SetActive(isShowClaimBtn);

        SoundManager.instance.PlayUIEffect("se_type2");
        StartCoroutine(DelayClose());
    }

    private IEnumerator DelayClose()
    {
        closeTxt.gameObject.SetActive(true);

        for (int i = 8; i > 0; i--)
        {
            //closeTxt.text = $"Automatically closes after {i} seconds.";
            timeClose.UpdateValue("time", VariableEnum.Int, i);
            SoundManager.instance.PlayUIEffect("se_muted");
            yield return new WaitForSeconds(1.0f);
        }

        gameObject.SetActive(false);
        //SceneHelper.ReloadScene();
        UIRace.Ins.ReflashRaceScene();
        //UIRace.Ins.ReflashRaceScene();
        //DogRaceManager.instance.ResetRaceGame();
    }

    public void CloseThisPop()
    {
        gameObject.SetActive(false);
        UIRace.Ins.ReflashRaceScene();
        //SceneHelper.ReloadScene();
    }

    private float AddVaue(float curValue, float goalValue)
    {

        float diff = Mathf.Abs(goalValue - curValue);

        float value = diff switch
        {
            > 10000.0f => 10000.0f,
            > 1000.0f => 1000.0f,
            > 100.0f => 100.0f,
            > 45.0f => 45.0f,
            _ => 1
        };

        return value;

    }


    private void Start()
    {
        claimBtn.onClickWithHCSound(() =>
        {
            handleClaimReward();
        });
    }

    private void handleClaimReward()
    {
        UIPopUpManager.instance.ShowLoadingCircle(true);
        StartCoroutine(Current.Ins.raceApi.claimRewardRace(roomId: _roomId, (res) =>
        {
            if (res == null || res.errorCode != 0)
            {
                Debug.Log("Claim Error:");
                UIPopUpManager.instance.ShowErrorPopUp("Request Error : handleClaimReward", res, handleClaimReward);
                return;
            }
            UIPopUpManager.instance.ShowLoadingCircle(false);

            //PickRace.Ins.changeOwnedAssets();
            UIRace.Ins.ReloadUserProfile(claimBtn.transform, () =>
            {
                gameObject.SetActive(false);
                SceneHelper.ReloadScene();
            });
            //UIRace.Ins.ReflashRaceScene();
            //DogRaceManager.instance.ResetRaceGame();

        }));
    }

}

