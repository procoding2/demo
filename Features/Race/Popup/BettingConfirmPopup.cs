﻿using System;
using OutGameEnum;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BettingConfirmPopup : MonoBehaviour
{
    public GameObject closeImg;
    public GameObject bettingTitleTxt;
    public GameObject numberBettingView;
    public GameObject numberBettingTxt;
    public GameObject contentTxt;
    public GameObject cancelContentTxt;
    public GameObject actionView;
    public GameObject cancelBtn;
    public GameObject confirmBtn;
    public GameObject errorTxt;

    private Action _confirmAction;
    private Action _closeAction;

    public void initView(bool isBetting = true, string errror = "", int numberBetting = 0, Action confirmAction = null, Action closeAction = null)
    {
        gameObject.SetActive(true);
        bettingTitleTxt.SetActive(isBetting);
        numberBettingView.SetActive(isBetting);
        contentTxt.SetActive(isBetting);
        cancelContentTxt.SetActive(false);
        actionView.SetActive(isBetting);
        errorTxt.SetActive(!isBetting);
        errorTxt.GetComponent<TextMeshProUGUI>().text = errror;
        _closeAction = closeAction;
        if (!isBetting) return;
        numberBettingTxt.GetComponent<TextMeshProUGUI>().text = numberBetting.ToString("n0");
        _confirmAction = confirmAction;
    }

    public void initViewCancel(int numberBetting = 0, Action confirmAction = null)
    {
        gameObject.SetActive(true);
        bettingTitleTxt.SetActive(true);
        numberBettingView.SetActive(true);
        contentTxt.SetActive(false);
        cancelContentTxt.SetActive(true);
        actionView.SetActive(true);
        errorTxt.SetActive(false);

        LanguageEnum language = Current.Ins.Config.GetCurrentLanguage();
        string cancelTitle = "";
        switch (language)
        {
            case LanguageEnum.eng:
                cancelTitle = "Warning";
                break;
            case LanguageEnum.kor:
                cancelTitle = "경고";
                break;
            case LanguageEnum.vi:
                cancelTitle = "Cảnh báo";
                break;
        }
        bettingTitleTxt.GetComponent<TextMeshProUGUI>().text = cancelTitle;
        numberBettingTxt.GetComponent<TextMeshProUGUI>().text = numberBetting.ToString("n0");
        _confirmAction = confirmAction;
    }

    private void Start()
    {
        closeImg.GetComponent<Button>().onClickWithHCSound(() =>
        {
            _closeAction?.Invoke();
            gameObject.SetActive(false);
        });
        cancelBtn.GetComponent<Button>().onClickWithHCSound(() =>
        {
            gameObject.SetActive(false);
        });
        confirmBtn.GetComponent<Button>().onClickWithHCSound(() =>
        {
            _confirmAction?.Invoke();
        });
    }

    public void RemoveListeners_confirmBtn()
    {
        _confirmAction = null;
    }
}

