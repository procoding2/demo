using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum CamMode
{
    prepareRace,
    startRace,
    playRace,
    endRace,
    resultRace,
}

public class FollowCam : MonoBehaviour
{



    public CamMode camMode = CamMode.prepareRace;
    [Header("Prepare Mode")]
    [SerializeField]
    private Vector3 fixedVec;
    private Vector3 movePos;




    [SerializeField]
    private float followSpeed = 3.0f;
    [SerializeField]
    private Transform followTarget = null;
    public Transform FollowTarget
    {
        get => followTarget;
        set => followTarget = value;
    }
    // Start is called before the first frame update

    

    [Header("Race Start Mode")]
    [SerializeField]
    private Vector3 raceStartSetVec;

    [SerializeField]
    private Vector3 raceStartSetRot;

    [SerializeField]
    private Vector3 raceStartAddRot;

    [SerializeField]
    private Vector3 raceStartFixedVec;


    [Header("Race Play Mode")]
    [SerializeField]
    private float raceDistance = 0.0f;
    [SerializeField]
    private float raceFollowSpeed = 20.0f;
    [SerializeField]
    private Vector3 racefixedVec, raceCamRot;



    [Header("Race End Mode")]
    [SerializeField]
    private Vector3 endVec;
    [SerializeField]
    private Vector3 endCamRot;

    private Vector3 originPos, originRot;


    void Start()
    {
        originPos = transform.position;
        originRot = transform.eulerAngles;




    }

    public void ResetPosition()
    {
        transform.position = originPos;
        transform.eulerAngles = originRot;

        camMode = CamMode.prepareRace;
    }

    float raceSpeed = 0.0f;
    public void SetCamReadyPos()
    {
        followTarget = null;
        transform.position = raceStartSetVec;
        transform.rotation = Quaternion.Euler(raceStartSetRot);

        raceSpeed = 3.0f;
    }


    // Update is called once per frame
    void Update()
    {



        if (camMode == CamMode.prepareRace)
        {
            if (followTarget == null)
                return;
            movePos = new Vector3(followTarget.position.x + fixedVec.x, fixedVec.y, followTarget.position.z + fixedVec.z);



            transform.position = Vector3.Lerp(transform.position, movePos, followSpeed * Time.deltaTime);
        }
        else if (camMode == CamMode.startRace)
        {
            if (followTarget == null)
                return;

            Vector3 centerPos = new Vector3(0.0f, followTarget.position.y, followTarget.position.z);

            transform.position = Vector3.Lerp(transform.position, raceStartFixedVec, 1.25f * Time.deltaTime);
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(centerPos + raceStartAddRot - transform.position), followSpeed * Time.deltaTime);
        }
        else if (camMode == CamMode.playRace)
        {
            if (followTarget == null)
                return;


            raceDistance = Mathf.Min(DogRaceManager.instance.DiffRaceDistance > 2.0f ? DogRaceManager.instance.DiffRaceDistance - 2.0f : 0.0f, 5.0f);

            Vector3 centerPos = new Vector3(0.0f, 0.0f, followTarget.position.z);

            movePos = new Vector3(  centerPos.x + racefixedVec.x + (raceDistance * 1.8f), 
                                    centerPos.y + racefixedVec.y + (raceDistance * 1.4f), 
                                    centerPos.z + racefixedVec.z);


            if (raceSpeed < raceFollowSpeed)
                raceSpeed += Time.deltaTime;

            transform.rotation = Quaternion.Lerp(transform.rotation, /*Quaternion.Euler(raceCamRot)*/Quaternion.LookRotation(centerPos + raceStartAddRot - transform.position), raceSpeed * Time.deltaTime);


            transform.position = Vector3.Lerp(transform.position, movePos, raceSpeed * Time.deltaTime);
        }
        else if (camMode == CamMode.endRace)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(endCamRot), followSpeed * Time.deltaTime);
            transform.position = Vector3.Lerp(transform.position, endVec, followSpeed * Time.deltaTime);
        }
        else if (camMode == CamMode.resultRace)
        {

        }

    }
}
