using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;


public class FinishLineCon : MonoBehaviour
{

    [SerializeField]
    private Slider[] finishLineSliders;

    [SerializeField]
    private TextMeshProUGUI[] finishRankNumbers;


    private float[] finishValues = new float[3];

    // Start is called before the first frame update
    void Start()
    {
        InitFinishLines();
    }

    // Update is called once per frame
    void Update()
    {
            for (int i = 0; i < finishLineSliders.Length; i++)
            {
                if (finishLineSliders[i].value < finishValues[i])
                {
                    finishLineSliders[i].value += Time.deltaTime * 3.0f;
                }
            }
    }


    public void InitFinishLines()
    {
        for (int i = 0; i < finishLineSliders.Length; i++)
        {
            finishLineSliders[i].value = finishValues[i] = 0.0f;
            finishRankNumbers[i].transform.localScale = Vector3.zero;
            
        }
    }

    public void SetFinishRank(int index, int rank)
    {

        SoundManager.instance.PlayUIEffect("se_lamp2");


        float value = 1.0f;
        switch (rank)
        {
            case 0: value = 1.0f; break;
            case 1: value = 0.75f; break;
            case 2: value = 0.5f; break;
        }
        finishValues[index] = value;


        finishRankNumbers[index].text = $"{rank + 1}";


        Sequence sq = DOTween.Sequence();
        sq.Join(finishRankNumbers[index].transform.DOScale(new Vector3(-0.12f, 0.12f, 0.12f), 0.5f)).SetEase(Ease.OutQuart);
        sq.Append(finishRankNumbers[index].transform.DOScale(new Vector3(-0.1f, 0.1f, 0.1f), 0.1f)).SetEase(Ease.OutQuart);

        sq.Play();

    }
}
