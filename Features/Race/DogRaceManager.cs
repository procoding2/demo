using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using GameEnum;

public class DogRaceManager : ManagerSingleton<DogRaceManager>
{
    [SerializeField]
    private List<DogUnit> dogUnits = new List<DogUnit>();
    private List<DogUnit> raceSortList;


    [SerializeField]
    private FollowCam followCam = null;
    [SerializeField]
    private InfoView infoView = null;

    [SerializeField]
    private Transform[] startPosList, endPosList;

    [SerializeField]
    private Transform endLineForm;

    public float EndLinePosZ => endLineForm.position.z;

    [SerializeField]
    private Animator doorAniCon = null;


    [SerializeField]
    private GameObject smokeEff = null;

    [SerializeField]
    private GameObject[] startEffects;


    [SerializeField]
    private GameObject jackpotUI;

    [SerializeField]
    private FinishLineCon _finishLineCon = null;
    public FinishLineCon finishLineCon => _finishLineCon;


    public RaceState CurrentRaceState { get; private set; } = RaceState.Prepare;

    public float RaceDistance_max { get; private set; } = 100.0f;


    private Coroutine prepareCoroutine = null;


    private int selectIndex = 0;


    public float DiffRaceDistance => Mathf.Abs(raceSortList[0].transform.position.z - raceSortList[1].transform.position.z);

    public DogUnit Rank1stDog => raceSortList[0];


    private float _roomID;
    private Action _actionCallBack;

    private void Awake()
    {

        ObjectPoolManager.instance.ReservePool(smokeEff, 50);


        startEffects[0].SetActive(false);
        startEffects[1].SetActive(false);
        for (int i = 0; i < 3; i++)
        {
            dogUnits[i].StartForm = startPosList[i];
            dogUnits[i].DestnationForm = endPosList[i];
        }

        raceSortList = new List<DogUnit>(dogUnits.ToArray());

        RaceDistance_max = (startPosList[0].position - endLineForm.position).sqrMagnitude;
        UIRace.Ins.CountDownObj.SetActive(false);

        infoView.gameObject.SetActive(false);

        ChangeRaceState(RaceState.Prepare);
    }

    public void InitRankResult(List<int> ranksResult, int roomId, Action actionCallBack)
    {


        SetFixedRankResult(ranksResult);
        _roomID = roomId;

        _actionCallBack = actionCallBack;
        ChangeRaceState(RaceState.Ready);
    }
    public void SetFixedRankResult(List<int> ranksResult)
    {
        for (int i = 0; i < ranksResult.Count; i++)
        {
            dogUnits[i].FixedRanking = ranksResult.FindLastIndex((figure) => figure == i + 1);
        }
    }

    public void SetDogRateValue(float[] winRates, float[] betRates, int[] lastRanks)
    {
        infoView.gameObject.SetActive(false);
        for (int i = 0; i < dogUnits.Count; i++)
        {
            dogUnits[i].winRate = winRates[i];
            dogUnits[i].betRate = betRates[i];

            dogUnits[i].RaceRanking = lastRanks[i];
        }

        if (prepareCoroutine != null)
        {
            StopCoroutine(prepareCoroutine);
            prepareCoroutine = null;
        }
        prepareCoroutine = StartCoroutine(PrepareRaceInfo());
    }

    public void UpdateDogBetRateUpdate(float[] betRates)
    {
        for (int i = 0; i < dogUnits.Count; i++)
        {
            dogUnits[i].betRate = betRates[i];
        }
    }

    private void Update()
    {
        if (CurrentRaceState == RaceState.Play)
        {
            if (raceSortList.Count > 0)
            {
                raceSortList.Sort((unitA, unitB) =>
                {
                    if (unitA.RemainGoalDestnation < unitB.RemainGoalDestnation)
                        return -1;
                    else
                        return 1;
                });
            }
            for (int i = 0; i < raceSortList.Count; i++)
            {
                raceSortList[i].RaceRanking = i;
            }

            followCam.FollowTarget = raceSortList[0].transform;
            if (followCam.camMode == CamMode.startRace && raceSortList[0].RemainGoalDestnation < 15000.0f)
            {
                followCam.camMode = CamMode.playRace;
            }
            else if (followCam.camMode == CamMode.playRace && raceSortList[0].RemainGoalDestnation < 500.0f)
            {
                followCam.camMode = CamMode.endRace;

            }
        }
    }

    public void DrawSmokeEffect(Vector3 pos)
    {
        GameObject obj = ObjectPoolManager.instance.GetObject(smokeEff.name);
        obj.transform.position = pos;
    }
    public bool EndRace(int index)
    {
        if (CurrentRaceState == RaceState.End)
            return false;

        ChangeRaceState(RaceState.End);

        return true;
    }

    public void SelectIndexAction(int index)
    {
        selectIndex = index;
    }


    public int GetLastRaceFixedWinner()
    {
        Debug.Log($"KunHarry:GetLastRaceFixedWinner {dogUnits[0].FixedRanking}{dogUnits[1].FixedRanking}{dogUnits[2].FixedRanking}");
        for (int i = 0; i < dogUnits.Count; i++)
        {
            if (dogUnits[i].FixedRanking == 0)
                return i;
        }
        return 0;
    }

    private IEnumerator PrepareRaceInfo()
    {

        List<int> numbers = new List<int>();
        numbers.AddRange(new int[] { 0, 1, 2, 3, 4, 5 });


        List<int> rndNumbers = ShuffleList(numbers);

        for (int i = 0; i < 3; i++)
        {
            dogUnits[i].SetPrepareIdleAni(rndNumbers[i]);
        }
        infoView.gameObject.SetActive(true);

        int followIndex = 0;
        while (true)
        {

            followCam.FollowTarget = dogUnits[followIndex].transform;

            infoView.ShowRaceInfo(dogUnits[followIndex].RaceRanking == 0, followIndex, dogUnits[followIndex].winRate, dogUnits[followIndex].betRate);


            if (++followIndex >= dogUnits.Count)
                followIndex = 0;

            yield return new WaitForSeconds(5.0f);


        }
    }

    private List<T> ShuffleList<T>(List<T> list)
    {
        int random1, random2;
        T temp;

        for (int i = 0; i < list.Count; ++i)
        {
            random1 = UnityEngine.Random.Range(0, list.Count);
            random2 = UnityEngine.Random.Range(0, list.Count);

            temp = list[random1];
            list[random1] = list[random2];
            list[random2] = temp;
        }

        return list;
    }



    public void ChangeRaceState(RaceState nextState)
    {
        switch (nextState)
        {
            case RaceState.Prepare:
                {
                    SoundManager.instance.PlayMusic("bgm_dogRace");
                    UIRace.Ins.CountDownObj.SetActive(false);
                    _finishLineCon.InitFinishLines();

                }
                break;
            case RaceState.Ready:
                {
                    SoundManager.instance.PlayMusic("bgm_raceCrowdCheering", false);

                    //jackpotUI.SetActive(false);
                    infoView.gameObject.SetActive(false);
                    if (prepareCoroutine != null)
                    {
                        StopCoroutine(prepareCoroutine);
                        prepareCoroutine = null;
                    }

                    followCam.SetCamReadyPos();

                    for (int i = 0; i < 3; i++)
                    {
                        dogUnits[i].SetRaceReady();
                    }

                    StartCoroutine(RaceStart());
                    IEnumerator RaceStart()
                    {
                        yield return new WaitForSeconds(2.0f);

                        for (int i = 3; i > 0; i--)
                        {
                            SoundManager.instance.PlayUIEffect("se_useClock");
                            UIRace.Ins.ShowCountText($"{i}");
                            yield return new WaitForSeconds(1.0f);

                        }
                        startEffects[0].SetActive(true);
                        startEffects[1].SetActive(true);

                        ChangeRaceState(RaceState.Play);
                    }

                }
                break;
            case RaceState.Play:
                {
                    SoundManager.instance.PlayUIEffect("se_fireWork");
                    SoundManager.instance.PlayMusic("bgm_raceCrowdPlay", false);
                    UIRace.Ins.ShowCountText("Go!");
                    UIRace.Ins.RetryFixedRank();

                    doorAniCon.SetTrigger("openDoor");
                    followCam.camMode = CamMode.startRace;
                    for (int i = 0; i < 3; i++)
                    {
                        dogUnits[i].SetRunningStart();
                    }
                }
                break;
            case RaceState.End:
                {
                    SoundManager.instance.PlayUIEffect("se_finish");
                    UIRace.Ins.ShowCountText("Finish");


                    StartCoroutine(RaceRestart());
                    IEnumerator RaceRestart()
                    {

                        yield return new WaitForSeconds(2.0f);
                        SoundManager.instance.PlayMusic("bgm_result");
                        _actionCallBack?.Invoke();

                    }
                }
                break;

        }


        CurrentRaceState = nextState;
    }

    public void ResetRaceGame()
    {
        StopAllCoroutines();
        doorAniCon.enabled = false;
        for (int i = 0; i < dogUnits.Count; i++)
        {
            dogUnits[i].ResetPosition();
        }

        followCam.ResetPosition();
        doorAniCon.enabled = true;
        doorAniCon.Play("Ready");
        infoView.gameObject.SetActive(false);
        ChangeRaceState(RaceState.Prepare);

    }

    public void ReadyToRace()
    {
        ChangeRaceState(RaceState.Ready);
    }

    public void TestRace(GameObject obj)
    {
        obj.SetActive(false);

        for (int i = 0; i < dogUnits.Count; i++)
        {
            dogUnits[i].FixedRanking = i;
        }


        ChangeRaceState(RaceState.Ready);
    }


    public void SetWaitAniDogs()
    {
        for (int i = 0; i < dogUnits.Count; i++)
        {
            dogUnits[i].SetWaitAni();
        }
    }
}
