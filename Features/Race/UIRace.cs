using System;
using System.Text;
using System.Security.Cryptography;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;
using OutGameEnum;
using System.Collections;
using DG.Tweening;



public class UIRace : UIBase<UIRace>
{
    [Header("TopView")]
    public GameObject backBtn;
    public Text timeValueTxt;
    public GameObject timeSecObj;
    [SerializeField]
    private TextMeshProUGUI countDownText;
    public GameObject CountDownObj => countDownText.gameObject;

    public GameObject myTokenTxt;
    public GameObject[] hiddenUIObjs;

    [Header("GamePlayView")]
    public GameObject inProgressImg;
    public JackpotArea jackPotView;
    //public GameObject jackPotTopView;
    public GameObject progress1;
    public GameObject progress2;
    public GameObject progress3;
    public GameObject progress1Txt;
    public GameObject progress2Txt;
    public GameObject progress3Txt;
    public GameObject hashCodeView;
    public GameObject hashCodeTxt;

    [Header("BetView")]
    public GameObject betVeiwObj;
    public GameObject[] tabItems;
    public GameObject selectTap;

    public GameObject pickView;
    public GameObject myHistoryView;
    public GameObject recordView;


    [SerializeField]
    private BroadcastMessage broadcastMessage;

    [Header("Popup")]
    public WinRacePopup winPopup;
    public RewardClaimPopup claimPopup;

    public GameObject losePopup;
    public GameObject tutorialPopup;

    public GameObject[] JackpotValue;
    public GameObject[] JackpotTopValue;

    private RaceBettingResultResponse bettingResult;
    private bool _isPlayingCountDownTimeSound = false;
    private bool _isWaitForOpenPlayRace = false;


    [Header("History")]
    public GameObject itemPrefabHistory;
    public Transform itemHolderHistory;
    public GameObject scrollBarHistory;
    public GameObject notProgress;
    private bool _isLoadingHistory;
    private int pageHistory = 1;

    [Header("Record")]
    public GameObject itemPrefabRecord;
    public Transform itemHolderRecord;
    public GameObject scrollBarRecord;

    [Header("RaceGamePlay")]
    public GameObject bg2D;
    public GameObject content2D;
    public GameObject rawImg3D;
    [Header("RaceGamePlay")]
    public GameObject objContentUI;
    [SerializeField]
    private MyPickInfo myPickInfo;

    [Header("ForAniCoin")]
    public GameObject ParentGameObject;
    public GameObject backParentGameObject;
    public GameObject TokenPrefab;

    public Transform TokenDes;
    public GameObject TokenLightDes;

    public GameObject valueAddTokenPrefab;

    private decimal _token;

    public Ease easeAnimation;
    //----------------------------------
    private bool _isLoadingRecord;
    private int pageRecord = 1;

    private int _roomId = 0;
    public int myBetting = 0;

    protected override void InitIns() => Ins = this;

    private float[] betRateArr;
    public void SetBetRateArr(float[] arr)
    {
        betRateArr = arr;
        DogRaceManager.instance.UpdateDogBetRateUpdate(arr);
    }
    private float[] winRateArr;
    private int[] lastRankArr;
    private IDisposable dispose = null;


    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();

        ReflashRaceScene();
        addEventChangeJackpot();
        addActionTab();
        backBtn.GetComponent<Button>().onClickWithHCSound(() =>
        {
            //HCSound.Ins.StopGameSound2();
            SceneHelper.LoadScene(SceneEnum.Home);
        });
        //
        float screen = (float)Screen.height / (float)Screen.width;
        screen = Mathf.Round(screen * 10f) / 10f;
        if (Mathf.Approximately(screen, 1.3f))
        {
            objContentUI.transform.localScale = Vector3.one*0.8f;
        }
    }
    private bool isReflashRace = false;
    public void ReflashRaceScene()
    {
        StopAllCoroutines();
        DogRaceManager.instance.SetWaitAniDogs();

        StartCoroutine(ReflashSceneProcess());
        IEnumerator ReflashSceneProcess()
        {
            isReflashRace = true;
            bettingResult = null;

            yield return new WaitForSeconds(0.5f);

            myBetting = 0;
            UIPopUpManager.instance.ShowLoadingCircle(true);
            countDownText.gameObject.SetActive(false);
            hashCodeView.SetActive(false);
            getValueJackpot();

            betVeiwObj.SetActive(true);
            myPickInfo.gameObject.SetActive(false);
            jackPotView.gameObject.SetActive(true);

            broadcastMessage.gameObject.SetActive(false);
            broadcastMessage.IsLiveOn = false;

            winPopup.gameObject.SetActive(false);
            claimPopup.gameObject.SetActive(false);

            Array.ForEach(hiddenUIObjs, obj => obj.SetActive(true));

            SelectTabAction(0);

            dispose?.Dispose();
            //pickView.GetComponent<PickRace>().nowPlayingView.SetActive(true);
            dispose = Observable.CombineLatest(
                    Observable.FromCoroutine<RaceBettingSumaryResponse>((resBettingRx)
                    => Current.Ins.raceApi.getBettingSummary(
                        GameEnum.MiniGameEnum.Dog.GetStringValue(), (res) =>
                        {
                            resBettingRx.OnNext(res);
                        })),
                    Observable.FromCoroutine<RaceRoomResponse>((resRaceRoomRx)
                    => Current.Ins.raceApi.getRoomRace(GameEnum.MiniGameEnum.Dog.GetStringValue(), (res) =>
                    {
                        resRaceRoomRx.OnNext(res);
                    })),
                    Observable.FromCoroutine<RaceHistoryResponse>((resRaceHistoryRx)
                    => Current.Ins.raceApi.getRecordsRace(page: 1, (res) =>
                    {
                        resRaceHistoryRx.OnNext(res);
                    })),
                    (raceBetting, raceRoom, resRaceHistory) => { return (raceBetting, raceRoom, resRaceHistory); })
                .Subscribe((result) =>
                {
                    getBettingSummary(res: result.raceBetting);
                    getRaceRoom(res: result.raceRoom);
                    List<RaceHistoryResponse.RaceHistory> list = result.resRaceHistory.data.datas.ToList();
                    lastRankArr = new int[3];
                    lastRankArr[0] = list[0].results.First((obj) => obj.figure == 1).rank - 1;
                    lastRankArr[1] = list[0].results.First((obj) => obj.figure == 2).rank - 1;
                    lastRankArr[2] = list[0].results.First((obj) => obj.figure == 3).rank - 1;

                    DogRaceManager.instance.SetDogRateValue(winRateArr, betRateArr, lastRankArr);
                });


            if (DogRaceManager.instance.CurrentRaceState != GameEnum.RaceState.Prepare)
            {
                DogRaceManager.instance.ResetRaceGame();
                DogRaceManager.instance.ChangeRaceState(GameEnum.RaceState.Prepare);
            }



        
            yield return new WaitForSeconds(1.0f);
            isReflashRace = false;
        }

    }
    public void SelectTabAction(int index)
    {
        SoundManager.instance.PlayUIEffect("se_select");

        Sequence sq = DOTween.Sequence();

        sq.Join(selectTap.transform.DOLocalMoveX(tabItems[index].transform.localPosition.x, 0.25f).SetEase(Ease.InOutCubic));
        sq.Play();

        if (index == 0)
        {
            pickView.SetActive(true);
            myHistoryView.SetActive(false);
            recordView.SetActive(false);
        }
        else if (index == 1)
        {
            pickView.SetActive(false);
            myHistoryView.SetActive(true);
            recordView.SetActive(false);

            scrollBarHistory.GetComponent<ScrollRect>().verticalNormalizedPosition = 1f;
            pageHistory = 1;
            getHistoryRace();

        }
        else if (index == 2)
        {
            pickView.SetActive(false);
            myHistoryView.SetActive(false);
            recordView.SetActive(true);

            scrollBarRecord.GetComponent<ScrollRect>().verticalNormalizedPosition = 1f;
            pageRecord = 1;
            getRecordRace();
        }


    }



    private void addActionTab()
    {


        scrollBarHistory.GetComponent<ScrollRect>().onValueChanged.AddListener((vector) =>
        {
            if (_isLoadingHistory || pageHistory == 0) return;

            if (scrollBarHistory.GetComponent<ScrollRect>().verticalNormalizedPosition <= 0.05f)
            {
                getHistoryRace();
            }
        });

        scrollBarRecord.GetComponent<ScrollRect>().onValueChanged.AddListener((vector) =>
        {
            if (_isLoadingRecord || pageRecord == 0) return;

            if (scrollBarRecord.GetComponent<ScrollRect>().verticalNormalizedPosition <= 0.05f)
            {
                getRecordRace();
            }
        });
    }

    private void getRecordRace()
    {
        _isLoadingRecord = true;
        UIPopUpManager.instance.ShowLoadingCircle(true);
        StartCoroutine(Current.Ins.raceApi.getRecordsRace(page: pageRecord, (res) =>
        {
            if (res == null || res.errorCode != 0)
            {
                Debug.Log("Claim Error:");
                UIPopUpManager.instance.ShowErrorPopUp("Request Error : getRecordRace", res, getRecordRace);
                return;
            }
            UIPopUpManager.instance.ShowLoadingCircle(false);
            List<RaceHistoryResponse.RaceHistory> list = res.data.datas.ToList();
            if (pageRecord == 1) itemHolderRecord.RemoveAllChild();
            _isLoadingRecord = false;
            pageRecord = res.data.nextPage;
            for (int i = 0; i < list.Count; i++)
            {
                GameObject gameObject = Instantiate(itemPrefabRecord, itemHolderRecord, false);
                gameObject.GetComponent<ItemRecordRace>().Init(list[i]);
            }

        }));
    }

    

    private void getHistoryRace()
    {
        notProgress.SetActive(false);
        _isLoadingHistory = true;
        UIPopUpManager.instance.ShowLoadingCircle(true);
        StartCoroutine(Current.Ins.raceApi.getHistoryRace(page: pageHistory, (res) =>
        {
            if (res == null || res.errorCode != 0)
            {
                Debug.Log("Claim Error:");
                UIPopUpManager.instance.ShowErrorPopUp("Request Error : getHistoryRace", res, getHistoryRace);
                return;
            }
            UIPopUpManager.instance.ShowLoadingCircle(false);
            List<RaceHistoryResponse.RaceHistory> list = res.data.datas.ToList();
            //
            if(list.Count == 0)
            {
                notProgress.SetActive(true);
            }
            //
            if (pageHistory == 1) itemHolderHistory.RemoveAllChild();
            _isLoadingHistory = false;
            pageHistory = res.data.nextPage;
            for (int i = 0; i < list.Count; i++)
            {
                GameObject gameObject = Instantiate(itemPrefabHistory, itemHolderHistory, false);
                gameObject.GetComponent<ItemHistoryRace>().Init(list[i]);
            }
        }));
    }


    private void addSocketBettingRate(long roomId) => Current.Ins.socketAPI.AddSocket<RaceRoomResponse.RaceRoom>(
               socketName: StringConst.SOCKET_BETTING_RATE,
               socketUrl: $"{StringConst.SOCKET_BETTING_RATE}{roomId}",
               callBack: (res) =>
               {
                   AddJob(() =>
                   {
                       pickView.GetComponent<PickRace>().setupViewRateDog(figures: res.figures);
                   });
               }
           );

    private void addSocketBettingResult(long roomId) => Current.Ins.socketAPI.AddSocket<RaceBettingResultResponse>(
               socketName: StringConst.SOCKET_BETTING_RESULT,
               socketUrl: $"{StringConst.SOCKET_BETTING_RESULT}{roomId}",
               callBack: (res) =>
               {
                   AddJob(() =>
                   {
                       bettingResult = res;
                       hashCodeTxt.GetComponent<TextMeshProUGUI>().text = $"Hash code: {bettingResult.hash}";

                       if(_isWaitForOpenPlayRace)
                       {
                           openGamePlayRace();
                       }
                   });
               }
           );

    private void addEventChangeJackpot()
    {
        Current.Ins.socketAPI.AddSocket<JackpotResponse.Data>(
               socketName: StringConst.SOCKET_CHANGE_JACKPOT_BETTING_DOG,
               socketUrl: StringConst.SOCKET_CHANGE_JACKPOT_BETTING_DOG,
               callBack: (res) =>
               {
                   AddJob(() =>
                   {
                       handleJackpot(res.token);
                   });
               }
           );
    }

    private void getValueJackpot()
    {
        StartCoroutine(Current.Ins.bonusAPI.GetJackpot((res) =>
        {
            if (res == null || res.errorCode != 0)
            {
                Debug.Log("Error: GetJackpot");
                return;
            }
            handleJackpot(res.data.token);
        }, type: JackpotType.DogBetting));
    }

    private void getRaceRoom(RaceRoomResponse res)
    {
        if (res == null || res.errorCode != 0)
        {
            Debug.Log("Error: getRaceRoom");
            UIPopUpManager.instance.ShowLoadingCircle(false);
            UIPopUpManager.instance.ShowErrorPopUp("Request Error : getRaceRoom", res);

            return;
        }
        UIPopUpManager.instance.ShowLoadingCircle(false);
        myBetting = res.data.yourBetting;
        pickView.GetComponent<PickRace>().InitRoom(res.data);
        hashCodeTxt.GetComponent<TextMeshProUGUI>().text = $"Hash code:";
        InitRx(res.data.timeRemain);
        addSocketBettingRate(res.data.id);
        addSocketBettingResult(res.data.id);
        var myToken = (int)Current.Ins.player.HCTokenCoin;
        myTokenTxt.GetComponent<TextMeshProUGUI>().text = $"{StringHelper.NumberFormatter(myToken)}";
        _roomId = res.data.id;
    }

    private void getBettingSummary(RaceBettingSumaryResponse res)
    {
        if (res == null || res.errorCode != 0)
        {
            Debug.Log("Error: getRaceRoom");
            UIPopUpManager.instance.ShowErrorPopUp("Request Error : getBettingSummary", res);
            return;
        }
        if (res.data.Count < 3) return;
        var value1 = res.data.First((bettingSum) => bettingSum.figure == 1).rateWin;
        var value2 = res.data.First((bettingSum) => bettingSum.figure == 2).rateWin;
        var value3 = res.data.First((bettingSum) => bettingSum.figure == 3).rateWin;
        var dog1LayoutElements = progress1.GetComponent<LayoutElement>();
        dog1LayoutElements.flexibleWidth = value1;
        var dog2LayoutElements = progress2.GetComponent<LayoutElement>();
        dog2LayoutElements.flexibleWidth = value2;
        var dog3LayoutElements = progress3.GetComponent<LayoutElement>();
        dog3LayoutElements.flexibleWidth = value3;
        progress1Txt.GetComponent<TextMeshProUGUI>().text = $"{value1}%";
        progress2Txt.GetComponent<TextMeshProUGUI>().text = $"{value2}%";
        progress3Txt.GetComponent<TextMeshProUGUI>().text = $"{value3}%";
        winRateArr = new float[] { value1, value2, value3 };
    }


    #region Rx

    /// <summary>
    /// Timer Rx
    /// </summary>
    private bool _isTimerRunning = false;
    private Subject<float> _timerRx = new Subject<float>();

    private IDisposable timerRx()
    {
        return _timerRx
            .Scan((pre, curr) =>
            {
                float nextTime = pre - curr;
                return nextTime <= 0 ? 0 : nextTime;
            })
            .Select(timer =>
            {
                if (isReflashRace == false)
                {
                    if (timer <= 30 && timer > 10 && broadcastMessage.IsLiveOn == false)
                    {
                        broadcastMessage.IsLiveOn = true;
                        LanguageEnum language = Current.Ins.Config.GetCurrentLanguage();
                        string title = "";
                        switch (language)
                        {
                            case LanguageEnum.eng:
                                title = "The next race will be starting soon. Please finalize your betting.";
                                break;
                            case LanguageEnum.kor:
                                title = "다음 경주가 곧 시작됩니다. 베팅을 완료해 주세요.";
                                break;
                            case LanguageEnum.vi:
                                title = "Ván đua chuẩn bị bắt đầu. Nhanh tay hoàn tất đặt cược.";
                                break;
                        }
                        broadcastMessage.BroadcastStart(title, true);
                        SoundManager.instance.PlayUIEffect("se_score_loop");
                    }
                    else if (timer == 10 && broadcastMessage.IsLiveOn)
                    {
                        broadcastMessage.IsLiveOn = false;
                    }
                    else if (timer < 10 && broadcastMessage.IsLiveOn == false)
                    {
                        broadcastMessage.IsLiveOn = true;
                        LanguageEnum language = Current.Ins.Config.GetCurrentLanguage();
                        string title = "";
                        switch (language)
                        {
                            case LanguageEnum.eng:
                                title = "The race will be starting very soon!!";
                                break;
                            case LanguageEnum.kor:
                                title = "레이싱이 곧 시작됩니다!!";
                                break;
                            case LanguageEnum.vi:
                                title = "Ván đua chuẩn bị bắt đầu!!";
                                break;
                        }
                        broadcastMessage.BroadcastStart(title);
                        SoundManager.instance.PlayUIEffect("se_score_loop");
                    }
                }
                
                //Cannot cancel betting if time < 10s
                PickRace.Ins.setupViewButton(time: timer, yourBetting: myBetting);
                HidePopupByCondition(time: timer);

                return timer == 0
                ? string.Empty
                : (TimeSpan.FromSeconds(timer)).ToString(@"mm\:ss");
            })
            .DistinctUntilChanged()
            .Subscribe(text =>
            {
                timeValueTxt.text = string.IsNullOrEmpty(text) ? "00:00" : text;

                if (string.IsNullOrEmpty(text))
                {
                    DisposeAllRx();
                    pickView.GetComponent<PickRace>().disableView();

                    jackPotView.gameObject.SetActive(false);


                    objContentUI.SetActive(false);

                    if (myBetting > 0 && PickRace.Ins.myPickIndex > 0)
                    {
                        myPickInfo.gameObject.SetActive(true);
                        myPickInfo.ShowMyPickInfoView(PickRace.Ins.myPickIndex, myBetting);
                    }

                    if (bettingResult == null)
                    {
                        _isWaitForOpenPlayRace = true;
                        UIPopUpManager.instance.ShowLoadingCircle(true);
                    }
                    else
                    {
                        openGamePlayRace();
                    }
                }
            });
    }

    void HidePopupByCondition(float time)
    {
        if(time <5)
        {
            AddJob(() =>
            {
                tutorialPopup.SetActive(false);
                tutorialPopup.GetComponent<HowToPlayMinigame>().blackShadow.SetActive(false);
                losePopup.SetActive(false);
            });
        }
    }

    public void openGamePlayRace()
    {

        if (isReflashRace)
            return;

        _isWaitForOpenPlayRace = false;
        UIPopUpManager.instance.ShowLoadingCircle(false);

        broadcastMessage.IsLiveOn = false;
        broadcastMessage.gameObject.SetActive(false);
        claimPopup.gameObject.SetActive(false);

        timeSecObj.GetComponent<Animator>().SetBool("timePlay", false);


        Array.ForEach(hiddenUIObjs, obj => obj.SetActive(false));


        //MD5 md5Hasher = MD5.Create();
        //var hashed = md5Hasher.ComputeHash(Encoding.UTF8.GetBytes(bettingResult.hash));
        //int seed = Mathf.Abs(BitConverter.ToInt32(hashed, 0)) % 10000;
        //UnityEngine.Random.InitState(seed);
        //Debug.Log($"hash: {bettingResult.hash},  Seed : {seed}");
        Debug.Log($"openGamePlayRace: {bettingResult.ranks[0]}, {bettingResult.ranks[1]}, {bettingResult.ranks[2]}");
        DogRaceManager.instance.InitRankResult(ranksResult: bettingResult.ranks, _roomId, () =>
        {
            getUserReward();
        });

    }

    public void RetryFixedRank()
    {
        Debug.Log($"RetryFixedRank: {bettingResult.ranks[0]}, {bettingResult.ranks[1]}, {bettingResult.ranks[2]}");
        DogRaceManager.instance.SetFixedRankResult(ranksResult: bettingResult.ranks);

    }

    IEnumerator setDelayTime(Action callBack)
    {
        yield return new WaitForSeconds(2);
        callBack.Invoke();
    }

    private void getUserReward()
    {
        StartCoroutine(Current.Ins.raceApi.getUserBettingReward(_roomId, (res) =>
        {
            if (res == null || res.errorCode != 0)
            {
                Debug.Log("Error: getRaceRoom");
                return;
            }
            string title = "";
            LanguageEnum language = Current.Ins.Config.GetCurrentLanguage();
            switch (language)
            {
                case LanguageEnum.eng:
                    if (res.data.rate == 0)
                    {
                        title = "EveryOne Win";
                    }
                    else
                    {
                        title = "Congratulations";
                    }
                    break;
                case LanguageEnum.kor:
                    if (res.data.rate == 0)
                    {
                        title = "모두의 승리입니다!";
                    }
                    else
                    {
                        title = "축하합니다";
                    }
                    break;
                case LanguageEnum.vi:
                    if (res.data.rate == 0)
                    {
                        title = "Tất cả đều thắng";
                    }
                    else
                    {
                        title = "Chúc mừng";
                    }
                    break;
            }




            if (myBetting > 0)
            {
                if (res.data.id > 0)//win
                {
                    winPopup.initView(title, res, _roomId);
                }
                else //lose
                {
                    winPopup.initView(title, myBetting, 0, 0, 0, 0, _roomId);
                }
            }
            else
            {
                winPopup.AutoCloseView();
            }
            //if (res.data.id > 0)
            //{
                
            //}
            //else
            //{
            //    //"It's unfortunate.");
            //    //SceneHelper.ReloadScene();
            //}
        }));
    }

    private void handleJackpot(long value)
    {

        jackPotView.CoinValue = value;

    }

    private IDisposable StartTimer()
    {
        return Observable
            .Interval(TimeSpan.FromSeconds(1))
            .Subscribe(x =>
            {
                if (_isTimerRunning)
                {
                    updateTimer(1);
                }
            });
    }

    private void pauseOrResumeTimer(bool isResume)
    {
        //if (_isPlayingCountDownTimeSound)
        //{
        //    if (isResume) HCSound.Ins.FxSoundGame2.UnPause();
        //    else HCSound.Ins.FxSoundGame2.Pause();
        //}

        _isTimerRunning = isResume;

        timeSecObj.GetComponent<Animator>().SetBool("timePlay", _isTimerRunning);

    }

    public void updateTimer(float time)
    {
        _timerRx.OnNext(time);
    }

    private void InitRx(int timeRemain)
    {
        DisposeAllRx();

        // Timer
        bags.Add(timerRx());
        updateTimer((float)timeRemain);
        pauseOrResumeTimer(true);
        bags.Add(StartTimer());
    }
    #endregion

    public void ShowCountText(string txt)
    {
        if (countDownText.gameObject.activeSelf)
            countDownText.gameObject.SetActive(false);

        countDownText.text = txt;
        countDownText.gameObject.SetActive(true);
    }
    private void OnApplicationPause(bool pause)
    {

        if (!pause)
        {
            ReflashRaceScene();
            //SceneHelper.ReloadScene();
        }
        else
        {
            StopAllCoroutines();
        }
    }

    //
    #region TokenAnim
    Sequence sq;
    private IEnumerator CoinAnimation(
        List<(CoinEnum, int)> coinTypes,
        Action callBackAfterEffect,
        Vector3 startPos,
        Transform endPos = null)
    {

        yield return new WaitForSeconds(0.4f);
        SoundManager.instance.PlayUIEffect("se_ring");
        sq = DOTween.Sequence();
        int totalDone = 0;
        //_token = int.Parse(myTokenTxt.GetComponent<TextMeshProUGUI>().text);
        foreach (var coinType in coinTypes)
        {
            GameObject objPrefab = null;
            Transform desTransform = null;
            GameObject iconLight = null;

            objPrefab = TokenPrefab;
            desTransform = TokenDes;
            iconLight = TokenLightDes;

            if (endPos != null) desTransform = endPos;
            Transform parent = ParentGameObject == null ? transform : ParentGameObject.transform;

            GameObject valuePrefab = null;
            if (coinType.Item1 == CoinEnum.Token)
            {
                valuePrefab = valueAddTokenPrefab;
            }

            var valueAdd = Instantiate(valuePrefab, parent, false);
            valueAdd.GetComponent<TextMeshProUGUI>().text = $"+{StringHelper.NumberFormatter(coinType.Item2)}";
            valueAdd.transform.position = startPos;
            valueAdd.transform.localScale = Vector3.zero;

            valueAdd.transform.DOScale(Vector3.one, 0.3f);
            valueAdd.transform.DOMoveY(valueAdd.transform.position.y + 0.5f, 1f).OnComplete(() =>
            {
                valueAdd.transform.DOScale(Vector3.zero, 0.3f).OnComplete(() => Destroy(valueAdd));
            });

            int valuePerCoin = 1;
            int totalCoin = coinType.Item2;
            if (coinType.Item2 > 20)
            {
                valuePerCoin = coinType.Item2 / 20;
                totalCoin = 20;
            }

            int totalDoneByCoin = 0;
            for (int i = 0; i < totalCoin; i++)
            {
                yield return new WaitForSeconds(0.05f);
                var obj = Instantiate(objPrefab, parent, false);
                obj.transform.position = startPos;

                float randX = UnityEngine.Random.Range(-50f, 50f);
                float randY = UnityEngine.Random.Range(-50f, 50f);
                float randTime = UnityEngine.Random.Range(1f, 1.5f);

                var strPos = startPos + new Vector3(randX, randY, 0f);
                obj.transform.localScale = Vector3.zero;

                obj.transform.DOMove(new Vector3(strPos.x, strPos.y, obj.transform.position.z), 0.2f)
                    .OnComplete(() =>
                    {
                        obj.transform
                        .DOMove(desTransform.position, randTime)
                        .SetEase(easeAnimation)
                        .OnComplete(() =>
                        {
                            SoundManager.instance.PlayUIEffect("se_buy");
                            if (sq != null)
                            {
                                sq.Kill();
                                sq = DOTween.Sequence();
                                desTransform.localScale = Vector3.one;
                                iconLight.transform.localScale = Vector3.zero;
                            }

                            sq.Join(iconLight.transform.DOScale(Vector3.one, 0.1f).OnComplete(() => iconLight.transform.localScale = Vector3.zero));
                            sq.Join(desTransform.DOScale(new Vector3(1.3f, 1.3f, 1.3f), 0.2f).SetLoops(2, LoopType.Yoyo));
                            Destroy(obj);

                            _token += valuePerCoin;
                            myTokenTxt.GetComponent<TextMeshProUGUI>().text = $"{StringHelper.NumberFormatter(_token)}";

                            totalDoneByCoin++;
                            if (totalDoneByCoin == totalCoin)
                            {
                                totalDone++;

                                if (totalDone == coinTypes.Count)
                                {
                                    Debug.Log("Done");
                                    callBackAfterEffect?.Invoke();
                                    myTokenTxt.GetComponent<TextMeshProUGUI>().text = $"{StringHelper.NumberFormatter(Current.Ins.player.HCTokenCoin)}";
                                    _token = Current.Ins.player.HCTokenCoin;
                                    backParentGameObject.SetActive(false);
                                }
                            }
                        });
                    });

                obj.transform.DOScale(1.3f, 0.5f);
            }

            yield return new WaitForSeconds(0.4f);
        }
    }
    #endregion

    public void ReloadUserProfile(Transform transform = null, Action callBackAfterEffect = null)
    {
        _token = (int)Current.Ins.player.HCTokenCoin;
        backParentGameObject.SetActive(true);
        StartCoroutine(Current.Ins.userAPI.GetUserProfile((res) =>
        {
            if (res == null || res.errorCode != 0)
            {
                Debug.Log("Error: ReloadUserProfile");
                return;
            }

            List<(CoinEnum, int)> coinTypes = new List<(CoinEnum, int)>();

            int t = res.data.token - (int)Current.Ins.player.HCTokenCoin;
            if (t > 0)
            {
                coinTypes.Add((CoinEnum.Token, t));
            }

            // Update coin
            Current.Ins.player.HCTokenCoin = res.data.token;

            StartCoroutine(CoinAnimation(coinTypes, callBackAfterEffect, transform.position, TokenDes));
        }));
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        dispose?.Dispose();
        Current.Ins.socketAPI.RemoveSocket(StringConst.SOCKET_BETTING_RATE);
        Current.Ins.socketAPI.RemoveSocket(StringConst.SOCKET_BETTING_RESULT);
        Current.Ins.socketAPI.RemoveSocket(StringConst.SOCKET_CHANGE_JACKPOT_BETTING_DOG);
    }
}
