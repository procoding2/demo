﻿using DG.Tweening;
using GameEnum;
using OutGameEnum;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GamePlayRace : UIBase<GamePlayRace>
{
    [Header("Dog")]
    public GameObject dog1;
    public GameObject dog2;
    public GameObject dog3;

    [Header("Goal")]
    public GameObject goal;
    public GameObject no1;
    public GameObject no2;
    public GameObject no3;

    [Header("Camera")]
    public GameObject cameraMain;

    [Header("In4")]
    public bool startDogRun = false;
    public bool CamStopMove = false;
    public bool runComplate = false;
    public Transform posCam1;
    public Transform posCam2;
    public Transform posCamObject2;
    public Material raceReuslt;
    public Texture2D redYellowWin;
    public Texture2D redGreenWin;
    public Texture2D yellowRedWin;
    public Texture2D yellowGreenWin;
    public Texture2D greenRedWin;
    public Texture2D greenYellowWin;
    [Header("Animator")]
    public Animator gateAnimator;
    public Animator dog1Animator;
    public Animator dog2Animator;
    public Animator dog3Animator;
    public Animator raceResutlAnimator;
    [Header("DogWin")]
    public GameObject dog1Win;
    public GameObject dog2Win;
    public GameObject dog3Win;

    private List<int> ranks;
    private int _roomId;
    private Action _actionCallBack;
    private List<Transform> transformsDog = new();

    protected override void InitIns()
    {
        Ins = this;
        SetSizeRenderTexture();
    }

    public void SetSizeRenderTexture()
    {
        var renderTextture = cameraMain.GetComponent<Camera>();
        Vector2 size;
        size.x = UIRace.Ins.rawImg3D.GetComponent<RawImage>().texture.width;
        size.y = UIRace.Ins.rawImg3D.GetComponent<RawImage>().texture.height;
        renderTextture.targetTexture.Release();
        renderTextture.targetTexture = new RenderTexture((int)size.x, (int)size.y, 24);
        UIRace.Ins.rawImg3D.GetComponent<RawImage>().texture = renderTextture.targetTexture;
    }

    void SetRaceReuslt(Texture2D win)
    {
        raceReuslt.mainTexture = win;
    }

    public void InitRankResult(List<int> ranksResult, int roomId, Action actionCallBack)
    {
        ranks = ranksResult;
        _roomId = roomId;
        cameraMain.transform.LookAt(dog1.transform);
        transformsDog.Add(dog1.transform);
        transformsDog.Add(dog2.transform);
        transformsDog.Add(dog3.transform);
        CameraMoveStar();
        _actionCallBack = actionCallBack;
    }

    protected override void Update()
    {
        if (!CamStopMove && goal.transform.position.z + 0.5f >= dog1.transform.position.z && !startDogRun)
        {
            cameraMain.transform.LookAt(dog1.transform.position);
            //cameraMain.transform.DOLookAt(dog1.transform.position, Time.deltaTime);
            //var rotation = Quaternion.LookRotation(dog1.transform.position - cameraMain.transform.position);
            //cameraMain.transform.rotation = Quaternion.Slerp(cameraMain.transform.rotation, rotation, Time.deltaTime*100f);
        }

        if (cameraMain.transform.position.z < dog1.transform.position.z - 1f
            && !CamStopMove)
        {
            cameraMain.transform.parent = dog1.transform;
            CamStopMove = true;
        }

        if (dog1.transform.position.z > goal.transform.position.z + 0.5f)
        {
            cameraMain.transform.parent = null;
        }

        if (startDogRun)
        {
            if (ranks.Count < 3) return;
            handleScriptDogWin(transformsDog[ranks[0] - 1], transformsDog[ranks[1] - 1], transformsDog[ranks[2] - 1]);
        }
    }

    private void handleScriptDogWin(Transform dogWinTrans, Transform dog2stTrans, Transform dog3stTrans)
    {
        var speed1 = 5f;
        var speed2 = 6f;
        var speed3 = 6f;
        int randomNumber = UnityEngine.Random.Range(0, 2);
        if (randomNumber == 0)
        {
            if (dogWinTrans.position.z > 170)
            {
                speed1 = 6.5f;
                speed2 = 6;
                speed3 = 6;
            }
            if (dogWinTrans.position.z > 180)
            {
                speed1 = 6.6f;
                speed2 = 6.5f;
                speed3 = 6.7f;
            }
            if (dogWinTrans.position.z > 185)
            {
                speed1 = 7.5f;
                speed2 = 7;
                speed3 = 7;
            }
            if (dogWinTrans.position.z > 191)
            {
                speed1 = 8;
                speed2 = 7.1f;
                speed3 = 7f;
            }
        }
        else
        {
            speed1 = 7.1f;
            speed2 = 7f;
            speed3 = 6.8f;
        }

        var posX1 = Vector3.forward * Time.deltaTime * speed1;
        var posX2 = Vector3.forward * Time.deltaTime * speed2;
        var posX3 = Vector3.forward * Time.deltaTime * speed3;

        dogWinTrans.Translate(posX1);
        dog2stTrans.Translate(posX2);
        dog3stTrans.Translate(posX3);

        if (!CamStopMove && goal.transform.position.z + 0.5f >= dog1.transform.position.z)
        {
            float a=1;
            if(dogWinTrans == dog1) a = speed1;
            else if(dog2stTrans == dog1) a = speed2;
            else if(dog3stTrans == dog1) a = speed3;
            cameraMain.transform.DOLookAt(dog1.transform.position, Time.deltaTime * a);
        }

        if (dog3stTrans.position.z >= goal.transform.position.z && !this.runComplate)
        {
            handleResultRace(pos1X: dogWinTrans.transform.position.x, pos2X: dog2stTrans.transform.position.x, pos3X: dog3stTrans.transform.position.x);
        }
    }

    private void handleResultRace(float pos1X, float pos2X, float pos3X)
    {
        UIRace.Ins.hashCodeView.SetActive(true);
        no1.transform.position = new Vector3(pos1X, no1.transform.position.y, no1.transform.position.z);
        no2.transform.position = new Vector3(pos2X, no2.transform.position.y, no2.transform.position.z);
        no3.transform.position = new Vector3(pos3X, no3.transform.position.y, no3.transform.position.z);
        no1.transform.DOScale(Vector3.one * 100, 0.5f);
        no2.transform.DOScale(Vector3.one * 100, 0.5f);
        no3.transform.DOScale(Vector3.one * 100, 0.5f);
        this.runComplate = true;
        StartCoroutine(setDelayTime(time: 1, callBack: () =>
        {
            dog1Win.SetActive(ranks[0] == 1);
            dog2Win.SetActive(ranks[0] == 2);
            dog3Win.SetActive(ranks[0] == 3);
            if (ranks[0] == 1 && ranks[1] == 2)
            {
                SetRaceReuslt(redYellowWin);
            }
            else if (ranks[0] == 1 && ranks[1] == 3)
            {
                SetRaceReuslt(redGreenWin);
            }
            else if (ranks[0] == 2 && ranks[1] == 1)
            {
                SetRaceReuslt(yellowRedWin);
            }
            else if (ranks[0] == 2 && ranks[1] == 3)
            {
                SetRaceReuslt(yellowGreenWin);
            }
            else if (ranks[0] == 3 && ranks[1] == 1)
            {
                SetRaceReuslt(greenRedWin);
            }
            else if (ranks[0] == 3 && ranks[1] == 2)
            {
                SetRaceReuslt(greenYellowWin);
            }
            cameraMain.transform.DOMove(posCam2.position, 1f).SetEase(Ease.InSine);
            cameraMain.transform.DORotate(posCam2.transform.eulerAngles, 1f).SetEase(Ease.InSine).OnComplete(() =>
            {
                raceResutlAnimator.SetTrigger("ResultRace");
            });
            StartCoroutine(setDelayTime(time: 3, callBack: () =>
            {
                _actionCallBack?.Invoke();
            }));
        }));
    }

    public void CameraMoveStar()
    {
        cameraMain.transform.DOMove(posCam1.position, 1f).SetEase(Ease.InSine).OnComplete(() =>
        {
            //UIRace.Ins.countDownObj.GetComponent<TextMeshProUGUI>().text = "GO!!";
            //UIRace.Ins.countDownObj.SetActive(true);
            DOVirtual.DelayedCall(1f, () =>
            {
                this.startDogRun = true;
                gateAnimator.SetTrigger("openDoor");
                dog1Animator.SetTrigger("Run");
                dog2Animator.SetTrigger("Run");
                dog3Animator.SetTrigger("Run");
            });
        });
        //cameraMain.GetComponent<Camera>().DOFieldOfView(35, 1f);
    }

    IEnumerator setDelayTime(int time, Action callBack)
    {
        yield return new WaitForSeconds(time);
        callBack.Invoke();
    }
}