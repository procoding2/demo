using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class InfoView : MonoBehaviour
{
    [SerializeField]
    private GameObject winnerSpr = null;

    [SerializeField]
    private TextMeshProUGUI winRate, betRate;

    [SerializeField]
    private Color[] sliderColors = new Color[3];

    private float goalWinRate, goalBetRate;
    private float curWinRate, curBetRate;


    [SerializeField]
    private Slider winRateSlider;


    private bool isUpdate = false;
    // Start is called before the first frame update
    void Start()
    {
        winRateSlider.enabled = false;
        winnerSpr.SetActive(false);
        curWinRate = curBetRate = 0.0f;
        winRateSlider.value = 0.0f;
        winRate.text = "0%";
        betRate.text = "x0.00";
    }

    // Update is called once per frame
    void Update()
    {
        if (isUpdate)
        {
            if (goalBetRate <= curBetRate && goalWinRate <= curWinRate)
            {
                isUpdate = false;
                return;
            }
            if (goalWinRate > curWinRate)
            {


                curWinRate += Mathf.Abs(goalWinRate - curWinRate) > 1.0f ? 1.0f :0.1f;
                winRateSlider.value = curWinRate / 100.0f;
                winRate.text = $"{(int)curWinRate}%";
                
            }

            if (goalBetRate > curBetRate)
            {
                curBetRate += Mathf.Abs(goalBetRate - curBetRate) > 1.0f ? 1.0f: 0.01f;

                if (Mathf.Abs(goalBetRate - curBetRate) < 0.01f)
                    curBetRate = goalBetRate;

                betRate.text = $"x{curBetRate:0.00}";
            }
        }
    

    }


    public void ShowRaceInfo(bool isLastWinner, int index, float _winRate, float _betRate)
    {
        

        winnerSpr.SetActive(isLastWinner);


        goalWinRate = _winRate;
        goalBetRate = _betRate;

        curWinRate = curBetRate = 0.0f;
        winRateSlider.value = 0.0f;

        winRateSlider.fillRect.GetComponent<Image>().color = sliderColors[index];
        betRate.text = "x0.00";
        //gameObject.SetActive(true);
        isUpdate = true;

    }
}
