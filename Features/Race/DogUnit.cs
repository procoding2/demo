using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using GameEnum;


public class DogUnit : MonoBehaviour
{
    [SerializeField]
    protected int playerIndex = 0;
    [SerializeField]
    private GameObject windTails = null;
    private NavMeshAgent agent => GetComponent<NavMeshAgent>();


    private Animator aniCon => GetComponent<Animator>();


    private DogAniState currentState;
    public DogAniState CurrentState => currentState;

    public Transform StartForm { get; set; } = null;
    public Transform DestnationForm { get; set; } = null;


    public float winRate, betRate;

    public bool isRaceEnd { get; private set; } = false;

    public int RaceRanking = 0;
    [SerializeField]
    private int _fixedRanking = 0;
    public int FixedRanking
    {
        get => _fixedRanking;
        set => _fixedRanking = value;
    }


    private bool isRotation = false;
    private float nextXPos = 0.0f;


    private float[] raceMaxSpeed = {17.0f, 15.0f, 13.0f };

    private Vector3 originPos;

    public float DashSpeed => agent.speed;

    // Start is called before the first frame update
    void Start()
    {
        originPos = transform.position;

        InvokeRepeating("DashSmokeEvent", 0.0f, 0.1f);
    }
    private void OnDestroy()
    {
        CancelInvoke();
    }
    public void ResetPosition()
    {
        pastTime = 0.0f;
        agent.Warp(originPos);

        //transform.position = originPos;
        transform.rotation = Quaternion.LookRotation(DestnationForm.position - transform.position);

    }

    public void SetWaitAni()
    {
        aniCon.Play("Wait");
    }
    public void DashSound()
    {
        if (transform.position.z < DestnationForm.position.z && agent.speed > 5.0f)
            SoundManager.instance.PlayEffect($"se_walk{Random.Range(1, 5)}", false);
    }

    private void DashSmokeEvent()
    {
        if (DogRaceManager.instance.CurrentRaceState >= RaceState.Play && currentState == DogAniState.Idle && agent.speed > 5.0f)
        {
            DogRaceManager.instance.DrawSmokeEffect(transform.position);

        }
    }
    // Update is called once per frame
    void Update()
    {
        if (isRotation)
        {
            Quaternion look = Quaternion.LookRotation(DestnationForm.position - transform.position);

            if (Quaternion.Angle(look, transform.rotation) < 1.0f)
            {
                transform.rotation = look;
                isRotation = false;
            }
            else
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, look, 10.0f * Time.deltaTime);
            }
        }

        

        if (DogRaceManager.instance.CurrentRaceState == RaceState.Prepare)
        {



        }
        else if (DogRaceManager.instance.CurrentRaceState == RaceState.Ready)
        {
            if (agent.remainingDistance < 0.1f)
            {
                agent.isStopped = true;
                transform.rotation = Quaternion.LookRotation(DestnationForm.position - transform.position);
                aniCon.SetFloat("MoveSpeed", 0.0f);
            }
        }
        else if (DogRaceManager.instance.CurrentRaceState >= RaceState.Play)
        {
            if (currentState == DogAniState.Idle)
            {

                if (DogRaceManager.instance.EndLinePosZ < transform.position.z)
                {
                    ChangeState(DogRaceManager.instance.EndRace(playerIndex) ? DogAniState.Win : DogAniState.Lose);

                    DogRaceManager.instance.finishLineCon.SetFinishRank(playerIndex, RaceRanking);

                    return;
                }

                if (agent && aniCon)
                {
                    float moveSpeed = agent.velocity.magnitude / agent.speed;
                    aniCon.SetFloat("MoveSpeed", moveSpeed);
                }


                if (transform.position.z > nextXPos)
                {
                    NextMovingPos();
                }



            }
            
        }

        windTails.SetActive(DogRaceManager.instance.CurrentRaceState >= RaceState.Play && currentState == DogAniState.Idle && agent.speed > 8.0f);
        aniCon.speed = DogRaceManager.instance.CurrentRaceState >= RaceState.Play ? Mathf.Clamp(agent.speed / 12.0f, 1.0f, 1.5f) : 1.0f;
    }
    public void ChangeState(DogAniState changeState)
    {
        currentState = changeState;
        if (currentState == DogAniState.Idle)
        {
            aniCon.Play("Idle");
        }
        else if (currentState == DogAniState.Win)
        {

            isRaceEnd = true;
            aniCon.Play("Win");
        }
        else if (currentState == DogAniState.Lose)
        {

            isRaceEnd = true;
            aniCon.Play("Lose");
        }
    }
    public float RemainGoalDestnation => (DestnationForm.position - transform.position).sqrMagnitude;



    public void SetRaceReady()
    {
        ChangeState(DogAniState.Idle);
        agent.isStopped = false;
        agent.destination = StartForm.position;

        agent.speed = 2.0f;
        aniCon.SetFloat("MoveSpeed", 0.2f);
    }
    public void SetRunningReady()
    {
        aniCon.Play("Ready");
    }
    public void SetRunningStart()
    {
        nextXPos = -100.0f;
        agent.isStopped = false;
        agent.speed = 6.0f;
        agent.SetDestination(DestnationForm.position + new Vector3(0,0,1));
    }


    public void SetPrepareIdleAni(int index)
    {
        aniCon.SetInteger("ReadyIndex", index);
        aniCon.Play("Ready");
    }

    private float pastTime = 0.0f;
    public void NextMovingPos()
    {
        float minSpeed = 1.08f;
        float maxSpeed = 1.15f;
        float goalDestrate = RemainGoalDestnation / DogRaceManager.instance.RaceDistance_max;

        bool isLastSpurt = false;
        if (DogRaceManager.instance.CurrentRaceState == RaceState.Play)
        {
            if (goalDestrate < 0.2f)
            {
                isLastSpurt = true;
                //if (_fixedRanking == 0)
                //{
                //    if (RaceRanking != 0)
                //    {
                //        minSpeed = 2.48f;
                //        maxSpeed = 3.12f;
                //    }

                //}
                //else
                //{
                //    if (RaceRanking == 0 || _fixedRanking == 2)
                //    {
                //        minSpeed = -2.5f;
                //        maxSpeed = -1.5f;
                //    }
                //    else
                //    {
                //        minSpeed = -1.8f;
                //        maxSpeed = -0.4f;
                //    }
                //}


            }
            else if (goalDestrate < 0.3f)
            {
                if (_fixedRanking == 0)
                {
                    if (RaceRanking != 0)
                    {
                        minSpeed = 1.6f;
                        maxSpeed = 2.2f;
                    }
                }
                else
                {
                    if (RaceRanking == 0 || _fixedRanking == 2)
                    {
                        minSpeed = -1.5f;
                        maxSpeed = -0.5f;
                    }
                    else
                    {
                        minSpeed = -0.5f;
                        maxSpeed = -0.1f;
                    }
                }
            }
            else if (goalDestrate < 0.4f)
            {
                if (_fixedRanking == 0 && RaceRanking != 0)
                {
                    minSpeed = 1.2f;
                    maxSpeed = 1.6f;
                }
            }
        }
    


        if (agent)
        {
            if (currentState != DogAniState.Idle)
                ChangeState(DogAniState.Idle);


            if (isLastSpurt)
            {
                agent.speed = raceMaxSpeed[_fixedRanking];
            }
            else
            {
                agent.speed += Random.Range(minSpeed, maxSpeed);


                if ((RaceRanking == 2 || FixedRanking == 0) && (DogRaceManager.instance.Rank1stDog.transform.position - transform.position).sqrMagnitude > 5 && goalDestrate > 0.2f)
                {
                    Debug.Log("Speed Up!!!");
                    agent.speed = DogRaceManager.instance.Rank1stDog.DashSpeed + 1.0f;
                }
                float limitMax = Mathf.Lerp(agent.speed, raceMaxSpeed[0], 1 - goalDestrate);
                float limitMin = Mathf.Lerp(agent.speed, raceMaxSpeed[2], 1 - goalDestrate);
                agent.speed = Mathf.Clamp(agent.speed , limitMin, limitMax) ;
            }
            pastTime += Random.Range(0.03f, 0.05f);
            Vector3 nextPos = Vector3.LerpUnclamped(transform.position, DestnationForm.position, pastTime);

            nextXPos = nextPos.z;
            //agent.SetDestination(nextPos);
        }



    }
}
