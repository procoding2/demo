using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class BroadcastMessage : MonoBehaviour
{
    [SerializeField]
    private float movingSpeed = 5.0f;
    [SerializeField]
    private TextMeshProUGUI message;
    [SerializeField]
    private GameObject topObj, barObj;


    private Canvas canvas => transform.root.GetComponent<Canvas>();


    private float endPosX = 0;
    private bool isMoving = false;

    private bool isLoop = false;


    public bool IsLiveOn = false;

    private void OnEnable()
    {
        endPosX = 0;
        isMoving = isLoop = false;

    }

    // Update is called once per frame
    void Update()
    {
        if (isMoving)
        {
            if (message.transform.localPosition.x <= endPosX)
            {
                if (isLoop)
                {
                    message.transform.localPosition = new Vector2(canvas.GetComponent<RectTransform>().rect.width * 0.5f + 30, 0);
                }
                else
                {
                    isMoving = false;
                    gameObject.SetActive(false);
                }
                return;

            }

            Vector3 endPos = new Vector3(endPosX - 100, message.transform.localPosition.y, message.transform.localPosition.z);
            message.transform.localPosition = Vector3.MoveTowards(message.transform.localPosition, endPos, Time.deltaTime * movingSpeed);

        }
    }


    public void BroadcastStart(string _msg, bool _isLooping = false)
    {
        gameObject.SetActive(true);
        message.text = _msg;
        LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)transform);

        isLoop = _isLooping;

        StartCoroutine(Broadcast());
        IEnumerator Broadcast()
        {
            yield return new WaitForSeconds(1.0f);


            

            message.transform.localPosition = new Vector2(canvas.GetComponent<RectTransform>().rect.width * 0.5f + 30, 0);

            if (canvas)
            {
                endPosX = ((canvas.GetComponent<RectTransform>().rect.width * 0.5f) + GetComponent<RectTransform>().rect.width + 30) * -1;
                isMoving = true;
            }
        }
    }
}
