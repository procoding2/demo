using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class GenPos : MonoBehaviour
{

    public Color gizmosColor = Color.green;
    public float gizmosSize = 0.25f;

    private void OnDrawGizmos()
    {
#if UNITY_EDITOR

        Gizmos.color = gizmosColor;
        Gizmos.DrawSphere(transform.position, gizmosSize);
#endif
    }
}
