using CodeStage.AntiCheat.Genuine.CodeHash;
using DG.Tweening;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UILogo : MonoBehaviour
{
    public GameObject FirstLogo;
    public GameObject SecondLogo;
    // Start is called before the first frame update

    [SerializeField]
    private AndroidNotiController androidNotiController;
    [SerializeField]
    private IOSNotiController iOSNotiController;

    private readonly float SHOW_DURATION = 0.5f;
    private readonly float KEEP_DURATION = 1.5f;
    private readonly float HIDE_DURATION = 0.3f;
    void Start()
    {
        DeviceHelper.SetQuality(isReset: true);

        var firstLg = FirstLogo.GetComponent<Image>();
        var secondLg = SecondLogo.GetComponent<Image>();

        secondLg.DOFade(1f, SHOW_DURATION).OnComplete(() => StartCoroutine(LazyFadeOut(secondLg, () => SceneHelper.LoadScene(OutGameEnum.SceneEnum.Welcome))));
        //firstLg.DOFade(1f, SHOW_DURATION)
        //.OnComplete(() =>
        //{
        //    StartCoroutine(LazyFadeOut(firstLg, () =>
        //    {
        //        secondLg.DOFade(1f, SHOW_DURATION).OnComplete(() => StartCoroutine(LazyFadeOut(secondLg, () => SceneHelper.LoadScene(OutGameEnum.SceneEnum.Welcome))));
        //    }));
        //});

#if UNITY_ANDROID
        androidNotiController.initNotiAndroid();
#elif UNITY_IOS
        StartCoroutine(iOSNotiController.RequestAuthorization());
        iOSNotiController.initNotiIos();
#endif
    }

    private IEnumerator LazyFadeOut(Image img, Action callBack)
    {
        yield return new WaitForSeconds(KEEP_DURATION);
        img.DOFade(0f, HIDE_DURATION).OnComplete(() => callBack.Invoke());
    }
}