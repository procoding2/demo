using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Localization.Components;
using OutGameEnum;

public class RecoverAccountPopup : MonoBehaviour
{
    public GameObject comfirmBtn;
    public GameObject cancelBtn;
    public GameObject timeText;

    private Action _comfirmRecoverAction;
    private Action _cancelRecoverAction;

    void Start()
    {
        comfirmBtn.GetComponent<ButtonEx>().onClickWithHCSound(() =>
        {
            gameObject.SetActive(false);
            UIPopUpManager.instance.ShowLoadingCircle(true);;
            _comfirmRecoverAction.Invoke();
        });

        cancelBtn.GetComponent<ButtonEx>().onClickWithHCSound(() =>
        {
            gameObject.SetActive(false);
            _cancelRecoverAction.Invoke();
        });
    }

    public void Init(LoginResponse res, string userName, Action onCancel, Action onConfirm)
    {
        gameObject.SetActive(true);
        _cancelRecoverAction = onCancel;
        _comfirmRecoverAction = onConfirm;

        int time = res.data.user.timeRemainDelete;
        timeText.GetComponent<LocalizeStringEvent>().UpdateValue("time", VariableEnum.Int, time / 86400);

    }

}
