using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LoginView : MonoBehaviour
{
    [SerializeField]
    private Button googleBtn, appleBtn, fncyBtn, guestBtn, fbBtn;

    //public GameObject guestPopup;
    //public GameObject closeBtn;
    //public GameObject inputField;
    //public GameObject quantityByteText;

    //public GameObject applyBtn;

    //private int _maxByteInput = 20;
    // Start is called before the first frame update
    void Start()
    {
        googleBtn.onClickWithHCSound(() =>
        {
            UIWelcome.Ins?.onClickLogin(1);
        });

        appleBtn.onClickWithHCSound(() =>
        {
            UIWelcome.Ins?.onClickLogin(2);
        });

        fncyBtn.onClickWithHCSound(() =>
        {
            UIWelcome.Ins?.onClickLogin(3);
        });

        guestBtn.onClickWithHCSound(() =>
        {
            //guestPopup.SetActive(true);

            GameObject pop = Instantiate(Resources.Load<GameObject>("UI/PopUpGuest"));
            UIPopUpManager.instance.ShowPopUpObject(pop);

        });

        fbBtn.onClickWithHCSound(() =>
        {
            UIWelcome.Ins?.onClickLogin(5);
        });



    }
}