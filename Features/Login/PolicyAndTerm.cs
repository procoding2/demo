using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PolicyAndTerm : MonoBehaviour
{
    public GameObject MainContent;
    public GameObject policyBtn;
    public GameObject termBtn;

    public GameObject ExtraContent;
    public GameObject closeBtn;

    public GameObject PrivacyTitle;
    public GameObject PrivacyContent;

    public GameObject TermTitle;
    public GameObject TermContent;

    public GameObject confirmBtn;

    public GameObject uiWelcome;

    private void Start()
    {
        float initYMainContent = MainContent.transform.position.y;
        float yHideMainContent = -7;

        float initYExtraContent = ExtraContent.transform.position.y;
        float yHideExtraContent = 0;

        policyBtn.GetComponent<Button>()
            .onClick
            .AddListener(() =>
            {
                SoundManager.instance.PlayUIEffect("se_click1");

                //PrivacyTitle.SetActive(true);
                //PrivacyContent.SetActive(true);

                //TermTitle.SetActive(false);
                //TermContent.SetActive(false);

                //Sequence sq = DOTween.Sequence();
                //sq
                //.Join(MainContent.transform.DOMoveY(yHideMainContent, 0.3f))
                //.Join(ExtraContent.transform.DOMoveY(yHideExtraContent, 0.3f));

                //sq.Play();
                Application.OpenURL(StringConst.POLICY_URL);
            });

        termBtn.GetComponent<Button>()
            .onClick
            .AddListener(() =>
            {
                SoundManager.instance.PlayUIEffect("se_click1");

                //PrivacyTitle.SetActive(false);
                //PrivacyContent.SetActive(false);

                //TermTitle.SetActive(true);
                //TermContent.SetActive(true);

                //Sequence sq = DOTween.Sequence();
                //sq
                //.Join(MainContent.transform.DOMoveY(yHideMainContent, 0.3f))
                //.Join(ExtraContent.transform.DOMoveY(yHideExtraContent, 0.3f));

                //sq.Play();
                Application.OpenURL(StringConst.CONDITION_URL);
            });

        closeBtn.GetComponent<Button>()
            .onClick
            .AddListener(() =>
            {
                SoundManager.instance.PlayUIEffect("se_click1");

                PrivacyTitle.SetActive(false);
                TermTitle.SetActive(false);

                Sequence sq = DOTween.Sequence();
                sq
                .Join(MainContent.transform.DOMoveY(initYMainContent, 0.3f))
                .Join(ExtraContent.transform.DOMoveY(initYExtraContent, 0.3f));

                sq.Play();
            });

        confirmBtn.GetComponent<Button>()
            .onClick
            .AddListener(() =>
            {
                SoundManager.instance.PlayUIEffect("se_click1");
                uiWelcome.GetComponent<UIWelcome>().onClickContinueBtn();
            });
    }
}
