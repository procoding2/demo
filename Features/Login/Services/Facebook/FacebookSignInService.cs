﻿using System;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using UnityEngine;
using UnityEngine.Networking;

public class FacebookSignInService: MonoBehaviour
{
    public static FacebookSignInService Ins;
    
    private void Awake()
    {
        Ins = this;
        DontDestroyOnLoad(this);

        if (!FB.IsInitialized)
        {
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            FB.ActivateApp();
        }
    }

    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            FB.ActivateApp();
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }

    private List<string> _permsFB = new List<string>() { "public_profile" };
    public void LoginFacebook(Action<bool, string, LoginSocialRequest> callBack)
    {
        FB.LogInWithReadPermissions(_permsFB, (result) =>
        {
            if (FB.IsLoggedIn)
            {
                //// AccessToken class will have session details
                //var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
                //// Print current access token's User ID
                //Debug.Log(aToken.UserId);
                //Debug.Log(aToken.TokenString);
                //// Print current access token's granted permissions
                //foreach (string perm in aToken.Permissions)
                //{
                //    Debug.Log(perm);
                //}

                Debug.Log(result.RawResult);

                var fbResponse = JsonUtility.FromJson<FacebookResponse>(result.RawResult);

                if (string.IsNullOrWhiteSpace(fbResponse.user_id) || string.IsNullOrWhiteSpace(fbResponse.access_token))
                {
                    callBack.Invoke(false, "Error", null);
                    return;
                }

                FB.API("me?fields=name", HttpMethod.GET, (res) =>
                {
                    LoginSocialRequest loginReq = new LoginSocialRequest();
                    loginReq.SocialId = fbResponse.user_id;
                    loginReq.SocialType = 4;
                    loginReq.FCMToken = FirebaseManager.Instance().token;
                    loginReq.AToken = fbResponse.access_token;

                    if (res.Error != null)
                    {
                        loginReq.Name = $"Fb{TimeManager.CurrentTimeInSecond}";
                    }
                    else
                    {
                        loginReq.Name = res.ResultDictionary["name"].ToString();
                    }

                    callBack.Invoke(true, string.Empty, loginReq);
                });
            }
            else
            {
                Debug.Log("User cancelled login");
                callBack.Invoke(false, "Error", null);
            }
        });
    }

    [Serializable]
    private class FacebookResponse
    {
        public string user_id;
        public string access_token;
    }
}

