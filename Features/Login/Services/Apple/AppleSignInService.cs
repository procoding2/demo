using AppleAuth;
using AppleAuth.Enums;
using AppleAuth.Interfaces;
using AppleAuth.Extensions;
using AppleAuth.Native;
using UnityEngine;
using System;
using Firebase.Auth;
using Firebase.Extensions;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

public class AppleSignInService
{
    private static AppleSignInService _ins;
    public static AppleSignInService Ins
    {
        get
        {
            _ins ??= new();
            return _ins;
        }
    }

    private IAppleAuthManager appleAuthManager;
    private FederatedOAuthProvider _provider;
    private FirebaseAuth auth = FirebaseAuth.DefaultInstance;

    private bool _isRunning = false;

    private AppleSignInService()
    {
        FederatedOAuthProviderData providerData = new FederatedOAuthProviderData();
        providerData.ProviderId = "apple.com";

        // Construct a FederatedOAuthProvider for use in Auth methods.
        _provider = new FederatedOAuthProvider();
        _provider.SetProviderData(providerData);

        // If the current platform is supported
        if (AppleAuthManager.IsCurrentPlatformSupported)
        {
            // Creates a default JSON deserializer, to transform JSON Native responses to C# instances
            var deserializer = new PayloadDeserializer();
            // Creates an Apple Authentication manager with the deserializer
            this.appleAuthManager = new AppleAuthManager(deserializer);
        }

        Current.Ins.PropertyChanged += Ins_PropertyChanged;
    }

    private void Ins_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
    {
        if (e.PropertyName == StringConst.PROPERTY_APPLE_UPDATE)
        {
            if (_isRunning) appleAuthManager?.Update();
        }
    }

    public void LoginApple(Action<bool, string, LoginSocialRequest> callBack)
    {
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            _isRunning = true;
            LoginFromIOS(callBack);
        }
        else if (Application.platform == RuntimePlatform.Android)
        {
            LoginFromAndroid(callBack);
        }
        else
        {
            callBack.Invoke(false, string.Empty, null);
        }
    }

    public void LoginAppleSilent(Action<bool, string, LoginSocialRequest> callBack)
    {
        try
        {
            AfterLoginDone(auth.CurrentUser.UserId, callBack);
        }
        catch (Exception)
        {
            callBack.Invoke(false, "", null);
        }
    }

    private void LoginFromIOS(Action<bool, string, LoginSocialRequest> callBack)
    {
        var rawNonce = GenerateRandomString(32);
        var nonce = GenerateSHA256NonceFromRawNonce(rawNonce);
        var loginArgs = new AppleAuthLoginArgs(LoginOptions.IncludeFullName, nonce);
        this.appleAuthManager.LoginWithAppleId(
            loginArgs,
            credential =>
            {
                try
                {
                    // Obtained credential, cast it to IAppleIDCredential
                    var appleIdCredential = credential as IAppleIDCredential;
                    if (appleIdCredential != null)
                    {
                        Credential fbCredential = OAuthProvider.GetCredential(
                            "apple.com",
                            Encoding.UTF8.GetString(appleIdCredential.IdentityToken),
                            rawNonce,
                            Encoding.UTF8.GetString(appleIdCredential.AuthorizationCode));
                        auth.SignInAndRetrieveDataWithCredentialAsync(fbCredential).ContinueWith(sgTask =>
                        {
                            if (sgTask.IsCanceled)
                            {
                                callBack.Invoke(false, "", null);
                                return;
                            }

                            if (sgTask.IsFaulted)
                            {
                                callBack.Invoke(false, "", null);
                                return;
                            }

                            _isRunning = false;
                            AfterLoginDone(sgTask.Result.User.UserId, callBack);
                        });
                    }
                    else
                    {
                        callBack.Invoke(false, "Error", null);
                    }
                }
                catch (Exception ex)
                {
                    Debug.Log($"Error: {ex.ToString()}");
                    callBack.Invoke(false, "Error", null);
                }
            },
            error =>
            {
                // Something went wrong
                var authorizationErrorCode = error.GetAuthorizationErrorCode();
                callBack.Invoke(false, "Error", null);
            });
    }

    private void LoginFromAndroid(Action<bool, string, LoginSocialRequest> callBack)
    {
        auth.SignInWithProviderAsync(_provider).ContinueWithOnMainThread(task =>
        {
            if (task.IsCanceled)
            {
                callBack.Invoke(false, "", null);
                return;
            }
            if (task.IsFaulted)
            {
                callBack.Invoke(false, "", null);
                return;
            }

            AfterLoginDone(task.Result.User.UserId, callBack);
        });
    }

    private void AfterLoginDone(string userId, Action<bool, string, LoginSocialRequest> callBack)
    {
        auth.CurrentUser.TokenAsync(true).ContinueWith(tokenTask =>
        {
            if (tokenTask.IsCanceled)
            {
                callBack.Invoke(false, "", null);
                return;
            }

            if (tokenTask.IsFaulted)
            {
                callBack.Invoke(false, "", null);
                return;
            }

            string idTokenFirebase = tokenTask.Result;

            LoginSocialRequest loginReq = new LoginSocialRequest();
            loginReq.SocialId = userId;
            loginReq.SocialType = 2;
            loginReq.FCMToken = FirebaseManager.Instance().token;
            loginReq.Name = $"Apple{TimeManager.CurrentTimeInSecond}";
            loginReq.AToken = idTokenFirebase;

            callBack.Invoke(true, idTokenFirebase, loginReq);
        });
    }

    private static string GenerateRandomString(int length)
    {
        if (length <= 0)
        {
            throw new Exception("Expected nonce to have positive length");
        }

        const string charset = "0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._";
        var cryptographicallySecureRandomNumberGenerator = new RNGCryptoServiceProvider();
        var result = string.Empty;
        var remainingLength = length;

        var randomNumberHolder = new byte[1];
        while (remainingLength > 0)
        {
            var randomNumbers = new List<int>(16);
            for (var randomNumberCount = 0; randomNumberCount < 16; randomNumberCount++)
            {
                cryptographicallySecureRandomNumberGenerator.GetBytes(randomNumberHolder);
                randomNumbers.Add(randomNumberHolder[0]);
            }

            for (var randomNumberIndex = 0; randomNumberIndex < randomNumbers.Count; randomNumberIndex++)
            {
                if (remainingLength == 0)
                {
                    break;
                }

                var randomNumber = randomNumbers[randomNumberIndex];
                if (randomNumber < charset.Length)
                {
                    result += charset[randomNumber];
                    remainingLength--;
                }
            }
        }

        return result;
    }

    private static string GenerateSHA256NonceFromRawNonce(string rawNonce)
    {
        var sha = new SHA256Managed();
        var utf8RawNonce = Encoding.UTF8.GetBytes(rawNonce);
        var hash = sha.ComputeHash(utf8RawNonce);

        var result = string.Empty;
        for (var i = 0; i < hash.Length; i++)
        {
            result += hash[i].ToString("x2");
        }

        return result;
    }
}