using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Firebase;
using Firebase.Auth;
using System;
using Google;
using System.Threading.Tasks;

public class GoogleSignInService
{
    private static GoogleSignInService _instance;
    public static GoogleSignInService Instance()
    {
        if (_instance == null)
        {
            _instance = new GoogleSignInService();
        }

        return _instance;
    }

    private GoogleSignInService()
    {
        configuration = new GoogleSignInConfiguration()
        {
            WebClientId = StringConst.WEB_CLIENT_ID,
            RequestIdToken = true
        };

        auth = FirebaseAuth.DefaultInstance;
    }

    private GoogleSignInConfiguration configuration;
    private FirebaseAuth auth;

    public void SignInGoogle(Action<bool, string, LoginSocialRequest> callBack)
    {
        GoogleSignIn.Configuration = configuration;
        GoogleSignIn.Configuration.UseGameSignIn = false;
        GoogleSignIn.Configuration.RequestIdToken = true;
        GoogleSignIn.Configuration.RequestEmail = true;

        try
        {
            GoogleSignIn.DefaultInstance.SignIn().ContinueWith(task => onGoogleSignIn(task, callBack));
        }
        catch(Exception ex)
        {
            Debug.Log(ex.ToString());
            callBack.Invoke(false, ex.Message, null);
        }
    }

    public void SignInSilentlyGoogle(Action<bool, string, LoginSocialRequest> callBack)
    {
        GoogleSignIn.Configuration = configuration;
        GoogleSignIn.Configuration.UseGameSignIn = false;
        GoogleSignIn.Configuration.RequestIdToken = true;
        GoogleSignIn.Configuration.RequestEmail = true;

        try
        {
            GoogleSignIn.DefaultInstance.SignInSilently().ContinueWith(task => onGoogleSignIn(task, callBack));
        }
        catch(Exception ex)
        {
            Debug.Log(ex.ToString());
            callBack.Invoke(false, ex.Message, null);
        }
    }

    public void SignOutGoogle() => GoogleSignIn.DefaultInstance.SignOut();

    private void onGoogleSignIn(Task<GoogleSignInUser> googleTask, Action<bool, string, LoginSocialRequest> callBack)
    {
        if (googleTask.IsFaulted)
        {
            callBack.Invoke(false, "IsFaulted", null);
        }
        else if (googleTask.IsCanceled)
        {
            callBack.Invoke(false, "IsCanceled", null);
        }
        else
        {
            Credential credential = GoogleAuthProvider.GetCredential(googleTask.Result.IdToken, null);

            auth
                .SignInWithCredentialAsync(credential)
                .ContinueWith(credentialTask =>
                {
                    if (credentialTask.IsCanceled)
                    {
                        callBack.Invoke(false, "IsCanceled: credential", null);
                        return;
                    }

                    if (credentialTask.IsFaulted)
                    {
                        callBack.Invoke(false, "IsFaulted: credential", null);
                        return;
                    }

                    auth.CurrentUser.TokenAsync(true).ContinueWith(tokenTask =>
                    {
                        if (tokenTask.IsCanceled)
                        {
                            callBack.Invoke(false, "TokenAsync was canceled.", null);
                            return;
                        }

                        if (tokenTask.IsFaulted)
                        {
                            callBack.Invoke(false, "TokenAsync encountered an error: " + googleTask.Exception, null);
                            return;
                        }

                        string idTokenFirebase = tokenTask.Result;

                        LoginSocialRequest loginReq = new LoginSocialRequest();
                        loginReq.SocialId = credentialTask.Result.UserId;
                        loginReq.SocialType = 1;
                        loginReq.FCMToken = FirebaseManager.Instance().token;
                        loginReq.Name = auth.CurrentUser.DisplayName;
                        loginReq.AToken = idTokenFirebase;

                        callBack.Invoke(true, "", loginReq);
                    });
                });
        }
    }
}
