using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Localization.Components;
using Utils;

public class MaintaincePopup : MonoBehaviour
{
    public GameObject content;
    public void Init(double timeFrom, double timeTo)
    {
        content.GetComponent<LocalizeStringEvent>().UpdateValue("0", OutGameEnum.VariableEnum.String, DateTimeUtil.GetTimeFormat(timeFrom, "yyyy/MM/dd HH:mm"));
        content.GetComponent<LocalizeStringEvent>().UpdateValue("1", OutGameEnum.VariableEnum.String, DateTimeUtil.GetTimeFormat(timeTo, "yyyy/MM/dd HH:mm"));
    }
}