using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Localization.Components;

using OutGameEnum;
using System;
using DG.Tweening;

using TMPro;

public class UIWelcome : UIBase<UIWelcome>
{
    protected override void InitIns() => Ins = this;

    [Header("View")]
    //public GameObject splashView;
    [SerializeField]
    private Animator SplashAniCon;

    [SerializeField]
    private GameObject LogoPanel = null;

    [SerializeField]
    private GameObject beforeLoadingView;

    [SerializeField]
    private GameObject loadingView;

    [SerializeField]
    private Slider progressBar;

    [SerializeField]
    private TextMeshProUGUI progressPercent;

    public GameObject policyAndTermView;


    public LocalizeStringEvent version;

    public GameObject RecoverAccountPopup;




    [SerializeField]
    private TextMeshProUGUI stageVerString = null;

    /// <summary>
    /// Game Entry Point : 언어설정 및 로그인 관련 초기화 진행
    /// </summary>
    protected override void Start()
    {


        SoundManager.instance.StopMusic();

        base.Start();

        stageVerString.text = ConfigData.instance.currentStage == ENVIRONMENTTYPE.PRODUCT ? "" : ConfigData.instance.currentStage.ToString();

        LogoPanel.SetActive(true);
        beforeLoadingView.SetActive(false);

        loadingView.SetActive(false);
        policyAndTermView.SetActive(false);

        StartCoroutine(Current.Ins.Config.SetUpLanguage());
        version.UpdateValue("version", VariableEnum.String, Application.version);


        // 로딩 관련 순차적 실행 진행
        StartCoroutine(openBeforeLoadingView());
    }

    private IEnumerator openBeforeLoadingView()
    {
        yield return new WaitForSeconds(0.5f);
        SoundManager.instance.PlayUIEffect("se_lamp1");

        yield return new WaitForSeconds(2.7f);
        LogoPanel.SetActive(false);

        SoundManager.instance.PlayMusic("bgm_home");

        //yield return new WaitForSeconds(0.5f);
        SplashAniCon.SetTrigger("show");
        //splashView.SetActive(false);
        beforeLoadingView.SetActive(true);
        loadingView.SetActive(false);

        yield return new WaitForSeconds(1f);
        StartCoroutine(openLoadingView());
    }

    private IEnumerator openLoadingView()
    {
        yield return new WaitForSeconds(0.2f);

        beforeLoadingView.SetActive(false);
        loadingView.SetActive(true);

        yield return new WaitForSeconds(0.5f);
        //HostUrl();

        GameServerStatus();
    }

    /// <summary>
    /// VERSION에 따른 접속 대상 서버에 대한 정보를 HUB에서 얻어오기 - Editor 등에서 디버깅을 하려면 반드시 Version 을 신경써서 어느 서버로 연결되는지 체크 후 작업
    /// </summary>
    private void HostUrl()
    {
        UpdateProgresbar(0.3f);



        //Current.Ins.DownloadUrl = ConfigData.instance.CurrentStageData.DOWNLOAD_URL;
        //Current.Ins.HostUrl = ConfigData.instance.CurrentStageData.HOST_URL;
        //Current.Ins.SocketUrl = ConfigData.instance.CurrentStageData.SOCKET_URL;

        

        //StartCoroutine(Current.Ins.userAPI.GetHostUrl(StringConst.VERSION, res =>
        //{
        //    if (res == null || res.errorCode != 0)
        //    {
        //        ErrorPopup.Init(HostUrl);
        //        return;
        //    }

        //    Current.Ins.DownloadUrl = res.data.downloadUrl;
        //    Current.Ins.HostUrl = res.data.hostUrl;
        //    Current.Ins.SocketUrl = res.data.socketUrl;

            
        //}));
    }

    private void GameServerStatus()
    {
        UpdateProgresbar(0.4f);
        StartCoroutine(Current.Ins.userAPI.GetTimeMaintaince(res =>
        {
            if (res == null || res.errorCode != 0)
            {

                UIPopUpManager.instance.ShowErrorPopUp("Request Error : GameServerStatus", res, GameServerStatus);

                return;
            }

            if (res.data.status)
            {

                SoundManager.instance.PlayUIEffect("se_click2");

                string message = TemplateManager.instance.GetLocalText("Home:content_maintenance");
                message = string.Format(message, Utils.DateTimeUtil.GetTimeFormat(res.data.fromMaintenance, "yyyy/MM/dd HH:mm"), Utils.DateTimeUtil.GetTimeFormat(res.data.toMaintenance, "yyyy/MM/dd HH:mm"));
                UIPopUpManager.instance.ShowPopUpBase(PopUpActionType.Btn1, message);

                return;
            }

            GetVersion();
        }));
    }

    void GetVersion()
    {
        UpdateProgresbar(0.5f);
        StartCoroutine(Current.Ins.userAPI.GetVersion((res) =>
        {
            if (res == null || res.errorCode != 0)
            {

                UIPopUpManager.instance.ShowErrorPopUp("Request Error : GetVersion", res, GetVersion);

                return;
            }

            if (res.data.name == Application.version)
            {
                InitLogin();
                return;
            }

            string message = TemplateManager.instance.GetLocalText("Welcome:ContentUpdatePopup");
            GameObject pop = UIPopUpManager.instance.ShowPopUpBase(res.data.requireUpdate ? PopUpActionType.Btn1 : PopUpActionType.Btn2, message);
            if (pop)
            {
                UIPopUpBaseObject popObj = pop.GetComponent<UIPopUpBaseObject>();
                popObj.OKBtn.onClick.AddListener(() =>
                    {
                        string urlStore = Application.platform == RuntimePlatform.IPhonePlayer ? StringConst.APPLE_STORE_URL : StringConst.GOOGLE_STORE_URL;
                        Application.OpenURL(urlStore);
                    }
                );
                popObj.OKBtn.GetComponentInChildren<TextMeshProUGUI>().text = TemplateManager.instance.GetLocalText("Welcome:UpdateBtn");


                if (res.data.requireUpdate == false)
                {
                    popObj.CloseBtn.onClick.AddListener(() =>
                        {
                            InitLogin();
                        }
                    );
                    popObj.CloseBtn.GetComponentInChildren<TextMeshProUGUI>().text = TemplateManager.instance.GetLocalText("Welcome:NotnowBtn");
                }
            }

        }));
    }
    private IEnumerator openPolicyAndTermView()
    {
        yield return new WaitForSeconds(0.1f);
        loadingView.SetActive(false);
        policyAndTermView.SetActive(true);
    }

    private void InitLogin()
    {
        var playerInfo = Current.Ins.GetPlayerDataLocal();

        Action<bool> loginResultAction = (isLogged) =>
        {
            if (isLogged)
            {
                SceneHelper.LoadScene(SceneEnum.Home);
            }
            else
            {
                StartCoroutine(openPolicyAndTermView());
            }
        };

        // New login
        if (playerInfo == null
            || string.IsNullOrWhiteSpace(playerInfo.UserName)
            || string.IsNullOrWhiteSpace(playerInfo.Name))
        {
            UpdateProgresbar(1f);
            loginResultAction.Invoke(false);
        }
        else
        {
            // Guest login
            if (playerInfo.SocialType == SocialTypeEnum.Guest)
            {
                var req = new LoginRequest();
                req.UserName = playerInfo.UserName;
                req.FCMToken = FirebaseManager.Instance().token;
                req.Name = playerInfo.Name;

                StartCoroutine(loginGuestAction(req, req.UserName, loginResultAction));
            }
            // Social login
            else
            {
                switch (playerInfo.SocialType)
                {
                    case SocialTypeEnum.Google:
                        GoogleLogin(isNormalSignIn: false);
                        break;
                    case SocialTypeEnum.Facebook:
                        var req = new LoginSocialRequest();
                        req.SocialId = playerInfo.UserName;
                        req.SocialType = (int)SocialTypeEnum.Facebook;
                        req.FCMToken = FirebaseManager.Instance().token;
                        req.Name = playerInfo.Name;

                        StartCoroutine(loginSocialAction(req, req.SocialId, loginResultAction));
                        break;
                    case SocialTypeEnum.Apple:
                        AppleLogin(isNormalSignIn: false);
                        break;
                    default: break;
                }
            }
        }
    }

    private void UpdateProgresbar(float value)
    {
        progressPercent.text = $"Loading... {(int)(value * 100.0f)}%";// .GetComponent<LocalizeStringEvent>().UpdateValue("percent", VariableEnum.String, StringHelper.FloatToPercent(value));
        
        progressBar.DOValue(value, 0.3f, false);
    }

    /// <summary>
    /// Click to open login view
    /// </summary>
    public void onClickContinueBtn()
    {
        loadingView.SetActive(false);
        policyAndTermView.SetActive(false);


        GameObject pop = Instantiate(Resources.Load<GameObject>("UI/LoginView"));
        UIPopUpManager.instance.ShowPopUpObject(pop, null);
        
        
        //loginView.SetActive(true);
    }

    /// <summary>
    /// Click to open login
    /// 1: Google
    /// 2: Apple
    /// 3: FNCY
    /// 4: Guest
    /// 5: Facebook
    /// </summary>
    /// <param name="type">login type</param>
    /// <param name="name">name</param>
    public void onClickLogin(int type, string name = "")
    {
        switch (type)
        {
            case 1:
                GoogleLogin();
                break;

            case 2:
                AppleLogin();
                break;

            case 3:
                //SceneHelper.LoadScene(SceneEnum.Home);
                break;

            case 4:
                UIPopUpManager.instance.ShowLoadingCircle(true);
                LoginRequest loginRequest = new LoginRequest();
                loginRequest.FCMToken = FirebaseManager.Instance().token;
                loginRequest.Name = name;
                loginRequest.UserName = $"{StringHelper.RandomString(8)}{TimeManager.CurrentTimeInSecond}";

                StartCoroutine(loginGuestAction(loginRequest, loginRequest.UserName, (result) =>
                {
                    UIPopUpManager.instance.CloseAllPopUpEvent();
                    if (!result)
                    {
                        UIPopUpManager.instance.ShowErrorPopUp("Request Error : onClickLogin", tryAction: () => onClickLogin(type, name));

                        return;
                    }

                    SceneHelper.LoadScene(SceneEnum.Home);
                }));

                break;
            case 5:
                FacebookLogin();
                break;

            default: break;
        }
    }

    private void FacebookLogin()
    {
        UIPopUpManager.instance.ShowLoadingCircle(true);;
        Action<LoginSocialRequest, bool> job = (loginReq, isSuccess) =>
        {
            AddJob(() =>
            {
                UIPopUpManager.instance.CloseAllPopUpEvent();
                if (!isSuccess)
                {


                    UIPopUpManager.instance.ShowErrorPopUp("Request Error : FacebookLogin");
                    return;
                }

                UIPopUpManager.instance.ShowLoadingCircle(true);
                
                StartCoroutine(loginSocialAction(loginReq, loginReq.SocialId, (result) =>
                {
                    if (result) SceneHelper.LoadScene(SceneEnum.Home);
                    else StartCoroutine(openPolicyAndTermView());
                }));
            });
        };

        FacebookSignInService.Ins.LoginFacebook((isSuccess, msg, loginReq) =>
        {
            job.Invoke(loginReq, isSuccess);
        });
    }

    private void AppleLogin(bool isNormalSignIn = true)
    {
        UIPopUpManager.instance.ShowLoadingCircle(true);;
        Action<LoginSocialRequest, bool> job = (loginReq, isSuccess) =>
        {
            AddJob(() =>
            {
                UIPopUpManager.instance.CloseAllPopUpEvent();
                if (!isSuccess)
                {
                    UIPopUpManager.instance.ShowErrorPopUp("Request Error : AppleLogin");
                    return;
                }

                UIPopUpManager.instance.ShowLoadingCircle(true);;
                StartCoroutine(loginSocialAction(loginReq, loginReq.SocialId, (result) =>
                {
                    if (result) SceneHelper.LoadScene(SceneEnum.Home);
                    else StartCoroutine(openPolicyAndTermView());
                }));
            });
        };

        if (isNormalSignIn)
        {
            AppleSignInService.Ins.LoginApple((isSuccess, msg, loginReq) => job.Invoke(loginReq, isSuccess));
        }
        else
        {
            AppleSignInService.Ins.LoginAppleSilent((isSuccess, msg, loginReq) => job.Invoke(loginReq, isSuccess));
        }
    }

    private void GoogleLogin(bool isNormalSignIn = true)
    {
        UIPopUpManager.instance.ShowLoadingCircle(true);;
        Action<LoginSocialRequest, bool> job = (loginReq, isSuccess) =>
        {
            AddJob(() =>
            {
                UIPopUpManager.instance.CloseAllPopUpEvent();
                if (!isSuccess)
                {
                    UIPopUpManager.instance.ShowErrorPopUp("Request Error : GoogleLogin");
                    return;
                }

                UIPopUpManager.instance.ShowLoadingCircle(true);;
                StartCoroutine(loginSocialAction(loginReq, loginReq.SocialId, (result) =>
                {
                    if (result) SceneHelper.LoadScene(SceneEnum.Home);
                    else StartCoroutine(openPolicyAndTermView());
                }));
            });
        };

        if (isNormalSignIn)
        {
            GoogleSignInService.Instance().SignInGoogle((isSuccess, msg, loginReq) => job.Invoke(loginReq, isSuccess));
        }
        else
        {
            GoogleSignInService.Instance().SignInSilentlyGoogle((isSuccess, msg, loginReq) => job.Invoke(loginReq, isSuccess));
        }
    }

    private void loginAPICallBack(LoginResponse res, string userName, Action<bool> callBack)
    {
        if (res == null || res.errorCode != 0)
        {
            StartCoroutine(FullProgressbarAndCallBack(() => callBack.Invoke(false)));
        }
        else
        {
            if (res.data.user.isDelete)
            {
                UIPopUpManager.instance.ShowLoadingCircle(false);;


                //UIPopUpManager.instance.ShowPopUpBase(PopUpActionType.Btn2, TemplateManager.instance.GetLocalText(""));


                RecoverAccountPopup.GetComponent<RecoverAccountPopup>().Init(res, userName,
                    onCancel: () =>
                    {
                        if (res.data.user.socialType == SocialTypeEnum.Google)
                        {
                            GoogleSignInService.Instance().SignOutGoogle();
                        }

                        StartCoroutine(FullProgressbarAndCallBack(() => callBack.Invoke(false)));
                    },
                    onConfirm: () =>
                    {
                        Current.Ins.player.AccessToken = res.data.accessToken;

                        RecoverAccount(() =>
                        {
                            LoadCurentLogin(res, userName, callBack);
                        });
                    });

                return;
            }

            LoadCurentLogin(res, userName, callBack);
        }
    }

    public void LoadCurentLogin(LoginResponse res, string userName, Action<bool> callBack)
    {
        PlayerPrefsEx.Set(PlayerPrefsConst.NUMBER_DAILY_MISSION, res.data.user.dailyMissionNotClaim);
        PlayerPrefsEx.Set(PlayerPrefsConst.NUMBER_NOTIFICATION, res.data.user.notificationUnWatch);
        Current.Ins.setPlayerInfo(res, userName);
        Current.Ins.ListenSocket();
        LoadImage(res, callBack);
    }

    private void LoadImage(LoginResponse res, Action<bool> callBack)
    {
        UIPopUpManager.instance.ShowLoadingCircle(true);;

        Current.Ins.LoadAllImage((isComplete) =>
        {
            UIPopUpManager.instance.ShowLoadingCircle(false);;
            if (!isComplete)
            {
                UIPopUpManager.instance.ShowErrorPopUp("Request Error : LoadImage", res, () => LoadImage(res, callBack));

                return;
            }

            StartCoroutine(FullProgressbarAndCallBack(() =>
            {
                Current.Ins.SetTimeUserLogin(res.currentTime, res.data.user.id.ToString());
                callBack.Invoke(true);
            }));
        });
    }

    private void RecoverAccount(Action onDone)
    {
        UIPopUpManager.instance.ShowLoadingCircle(true);;
        StartCoroutine(Current.Ins.userAPI.RecoveryAccount((res) =>
        {
            if (res == null || res.errorCode != 0)
            {
                UIPopUpManager.instance.ShowLoadingCircle(false);;
                UIPopUpManager.instance.ShowErrorPopUp("Request Error : RecoverAccount", res, () => RecoverAccount(onDone));
                return;
            }

            onDone.Invoke();
        }));
    }

    private IEnumerator FullProgressbarAndCallBack(Action callBack)
    {
        UpdateProgresbar(1f);

        yield return new WaitForSeconds(0.5f);

        UIPopUpManager.instance.ShowLoadingCircle(false);;
        callBack.Invoke();
    }

    private IEnumerator loginGuestAction(LoginRequest req, string userName, Action<bool> callBack)
    {
        return Current.Ins.userAPI.Login(req, (res) =>
        {
            if (res == null || res.errorCode != 0)
            {
                if (res != null && res.errorCode == 15)
                {
                    UIPopUpManager.instance.ShowPopUpBase(PopUpActionType.Btn1, TemplateManager.instance.GetLocalText("ProfileSetting:NoticeContent"));
                }
                else
                {
                    UIPopUpManager.instance.ShowErrorPopUp("Request Error : loginGuestAction", res);
                }

                UIPopUpManager.instance.ShowLoadingCircle(false);;
                return;
            }

            loginAPICallBack(res, userName, callBack);
        });
    }

    private IEnumerator loginSocialAction(LoginSocialRequest req, string userName, Action<bool> callBack)
    {

        return Current.Ins.userAPI.LoginSocial(req, (res) =>
        {
            UIPopUpManager.instance.ShowLoadingCircle(false);;
            if (res == null || res.errorCode != 0)
            {
                UIPopUpManager.instance.ShowErrorPopUp("Request Error : loginSocialAction", res);
                return;
            }

            loginAPICallBack(res, userName, callBack);
        });
    }
}
