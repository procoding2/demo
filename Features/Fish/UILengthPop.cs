using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class UILengthPop : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI lengthText, resultText;

    [SerializeField]
    private Slider lengthSlider;

    [SerializeField]
    private Color[] resultColors = new Color[2];

    private float curLength, goalLength;





    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShowLentghResultPop(bool isShow)
    {
        if (isShow == false)
        {
            gameObject.SetActive(false);
            return;
        }

        Fish fish = LureGameManager.instance.lureObj.CatchedFish;
        if (fish == null)
            return;


        fish.PlayAni(true);
        gameObject.SetActive(true);
        resultText.gameObject.SetActive(false);
        lengthText.text = "0.0 cm";

        curLength = 0.0f;
        goalLength = fish.FishLength * 100.0f;


        lengthSlider.gameObject.SetActive(false);
        StopAllCoroutines();
        StartCoroutine(CheckSize());

        IEnumerator CheckSize()
        {
            yield return new WaitForSeconds(3.0f);

            fish.PlayAni(false);
            lengthSlider.gameObject.SetActive(true);
            lengthSlider.value = 0.0f;

            Vector2 startPos = Camera.main.WorldToScreenPoint(fish.FrontVector);
            Vector2 endPos = Camera.main.WorldToScreenPoint(fish.TailVector);
            Vector2 sliderPos = new Vector2(lengthSlider.transform.position.x, startPos.y);

            lengthSlider.transform.position = sliderPos;

            float width = (startPos.y - endPos.y) * (1920.0f / Screen.height);

            RectTransform rt = lengthSlider.GetComponent<RectTransform>();
            rt.sizeDelta = new Vector2(width, 50);

            fish.PlayAni(true);

            while (curLength < goalLength)
            {
                curLength += 0.25f;
                lengthText.text = $"{curLength:0.0} cm";


                if (lengthSlider.value < 1.0f)
                    lengthSlider.value += 0.01f;

                yield return new WaitForSeconds(0.01f);

                SoundManager.instance.PlayUIEffect("se_type1");
            }

            lengthText.text = $"{goalLength:0.0} cm";
            lengthSlider.value = 1.0f;

            resultText.text = goalLength > 25.0f ? "Big!!" : "Small!!";
            resultText.color = resultColors[goalLength > 25.0f ? 1 : 0];

            yield return new WaitForSeconds(0.5f);

            resultText.gameObject.SetActive(true);
            SoundManager.instance.PlayUIEffect("se_recovery3");
        }

    }



}
