using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;




public enum FishType
{
    EmperorAngelfish,
    Caranx,
    Clownfish,
    DoubleSaddle,
    GoldenFish,
}
public class Fish : MonoBehaviour
{

    //[SerializeField]
    private float swimSpeed = 2.0f;
    public float SwimSpeed { set => swimSpeed = value; }
    [SerializeField]
    private float turnSpeed = 5.0f;

    [SerializeField]
    private Transform frontForm, tailForm;


    [SerializeField]
    private FishType curFishType = FishType.EmperorAngelfish;
    public FishType CurFishType => curFishType;

    public Vector3 FrontVector => frontForm.position;

    public Vector3 TailVector => tailForm.position;

    private float stayTime = 0.0f;

    private Vector3 nextPos = Vector3.zero;
    public Vector3 NextPos
    {
        set => nextPos = value;
    }


    private float fishLength = 0.0f;
    public float FishLength => fishLength;

    private bool isDecisionFish = false;
    private int biteTryCount = 0;
    public bool IsDecisionFish
    {
        get => isDecisionFish;
        set
        {
            isDecisionFish = value;
            if (isDecisionFish)
            {
                biteTryCount = Random.Range(4,7);
            }
        }
    }

    private bool isBiteActionIng = false;
    private bool isCatchState = false;

    Coroutine BiteActionCoroutine = null;

    

    private Animator aniCon => GetComponentInChildren<Animator>();
    // Start is called before the first frame update
    void Start()
    {

    }



    public Vector3 CenterPos => Vector3.Lerp(frontForm.position, tailForm.position, 0.5f);
    private void OnEnable()
    {
        if (LureGameManager.instance.FishList.Contains(this) == false)
            LureGameManager.instance.FishList.Add(this);

        isCatchState = false;
        
        //nextPos = transform.position;


        if (BiteActionCoroutine != null)
            StopCoroutine(BiteActionCoroutine);

        BiteActionCoroutine = StartCoroutine(BiteActionProcess());



    }


    public void SetNewScale(float scale)
    {
        fishLength = scale;
        transform.localScale = Vector3.one * 0.01f;
        StartCoroutine(ScaleUpProcess(fishLength));
    }

    IEnumerator ScaleUpProcess(float value)
    {
        

        float currentValue = transform.localScale.x;

        while (transform.localScale.x < value)
        {
            currentValue += 0.01f;
            transform.localScale = Vector3.one * currentValue;

            yield return new WaitForSeconds(0.01f);
        }
    }

    IEnumerator BiteActionProcess()
    {
        yield return new WaitForSeconds(3.0f);
        while (true)
        {

            yield return new WaitForSeconds(/*isDecisionFish ?  0.3f :*/ Random.Range(0.3f, 0.6f));



            
            LureObj lureObj = null;
            Collider[] lureCols = Physics.OverlapSphere(transform.position, isDecisionFish ? 100.0f : 10.0f, LayerMask.GetMask("Lure"));
            if (lureCols.Length > 0)
            {
                for (int i = 0; i < lureCols.Length; i++)
                {
                    LureObj obj = lureCols[i].GetComponent<LureObj>();
                    if (obj && obj.MoveState == LureMoveState.InWay)
                    {
                        lureObj = obj;
                        break;
                    }
                }
            }

            if (lureObj && lureObj.IsBiteAble && (lureObj.transform.position - transform.position).sqrMagnitude < (isDecisionFish ? 3.0f : 1.0f))
            {

                SoundManager.instance.PlayEffect("se_fishTok");

                isBiteActionIng = true;
                Sequence sq = DOTween.Sequence();

                transform.rotation = GetLookFowardAngle(lureObj.transform.position);


                Vector3 oriPos = transform.position;

                sq.Join(transform.DOMove(transform.position + transform.right * -0.2f, 0.1f).SetEase(Ease.InSine));
                sq.Append(lureObj.transform.DOShakePosition(0.1f, 0.1f));
                sq.Append(transform.DOMove(oriPos, 0.1f).SetEase(Ease.InSine));
                sq.Play().OnComplete(()=>{

                    if (isDecisionFish)
                    {
                        if (--biteTryCount <= 0)
                        {
                            isCatchState = true;
                            lureObj.SetCatchFish(this);

                        }
                    }

                    isBiteActionIng = false;
                });
            }
        }
    }

    private void OnDisable()
    {
        if (LureGameManager.instance.FishList.Contains(this))
            LureGameManager.instance.FishList.Remove(this);
    }


    private Vector3 GetNextPosFromCenterGenPos()
    {

        Vector3 resultPos = Vector3.zero;
        Vector2 circlePos = UnityEngine.Random.insideUnitCircle * 15.0f;


        resultPos = LureGameManager.instance.GenCenterForm.transform.position + new Vector3(circlePos.x, 0.0f, circlePos.y);

        //Collider[] cols = Physics.OverlapSphere(transform.position, 40.0f, LayerMask.GetMask("Rock"));
        //if (cols.Length > 0)
        //{
        //    int limitCount = 0;
        //    bool isNotOK = false;
        //    do
        //    {
        //        isNotOK = false;
        //        for (int i = 0; i < cols.Length; i++)
        //        {
        //            if (cols[i].bounds.Contains(resultPos))
        //            {
        //                isNotOK = true;

        //                circlePos = UnityEngine.Random.insideUnitCircle * 15.0f;
        //                resultPos = LureGameManager.instance.GenCenterForm.transform.position + new Vector3(circlePos.x, 0.0f, circlePos.y);
        //                break;
        //            }
        //        }
        //        limitCount++;
        //    }
        //    while (isNotOK && limitCount< 100);
        //}




        return resultPos;
    }

    // Update is called once per frame
    void Update()
    {

        if (isCatchState)
        {
            return;
        }


        bool isFollowLure = false;
        Collider[] lureCols = Physics.OverlapSphere(transform.position, isDecisionFish ? 150.0f: 10.0f, LayerMask.GetMask("Lure"));

        if (lureCols.Length > 0)
        {
            for (int i = 0; i < lureCols.Length; i++)
            {
                LureObj lureObj = lureCols[i].GetComponent<LureObj>();
                if (lureObj && lureObj.MoveState == LureMoveState.InWay)
                {
                    isFollowLure = true;
                    if ((lureObj.transform.position - transform.position).sqrMagnitude > 20.0f)
                    {
                        nextPos = lureObj.transform.position + Random.insideUnitSphere * 2.0f;
                        nextPos.y = transform.position.y;
                        break;
                    }
                }
            }
        }



        
        if ((nextPos - transform.position).sqrMagnitude < 0.1f)
        {
            if (LureGameManager.instance.CurrentRaceState == GameEnum.RaceState.Play)
            {
                swimSpeed = Random.Range(2.5f, 3.5f);
            }
            else
            {
                swimSpeed = (curFishType == FishType.GoldenFish) ? 1.0f : Random.Range(1.5f, 2.5f);
            }

            nextPos = GetNextPosFromCenterGenPos();

            stayTime = (Random.Range(0, 10) < 2) ? Random.Range(1.0f, 3.0f) : 0.0f;
        }
        //else
        //{
        //    Collider[] cols = Physics.OverlapSphere(transform.position, 3.0f, LayerMask.GetMask("Rock"));
        //    if (cols.Length > 0)
        //    {
        //        Bounds fishBound = new Bounds(transform.position, Vector3.one * 3.0f);
        //        for (int i = 0; i < cols.Length; i++)
        //        {
        //            if (cols[i].bounds.Intersects(fishBound) == true)
        //            {
        //                nextPos = GetNextPosFromCenterGenPos();
        //            }
        //        }

        //    }
        //}
        


        if (stayTime > 0.0f)
        {
            stayTime -= Time.deltaTime;
        }
        else
        {


            if (isBiteActionIng == false)
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, GetLookFowardAngle(nextPos), Time.deltaTime * turnSpeed);
                transform.position = Vector3.MoveTowards(transform.position, nextPos, Time.deltaTime * swimSpeed * (isFollowLure ? 1.5f : 1.0f));
            }
        }


    }
    private Quaternion GetLookFowardAngle(Vector3 lookPos)
    {
        Quaternion viewAngle = Quaternion.LookRotation(lookPos - transform.position);
        viewAngle.eulerAngles = new Vector3(0.0f, viewAngle.eulerAngles.y + 90, viewAngle.eulerAngles.z);

        return viewAngle;
    }

    public void PlayAni(bool isPlay)
    {
        aniCon.SetBool("isStop", !isPlay);
    }


}
