﻿using DG.Tweening;
using OutGameEnum;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PickFish : UIBase<PickFish>
{
    [Header("Pick Dog")]
    public GameObject tutorialImg;

    [SerializeField]
    private Toggle[] selectFishToggles;

    private ToggleGroup toggleGroup;

    [SerializeField]
    private TextMeshProUGUI[] FishBetRates;


    [Header("BET detail")]
    public GameObject price1View;
    public HoldClickableButton price1ViewBtn;
    public GameObject price1Txt;
    public GameObject resetBtn;
    public GameObject coinFeeTxt;

    [Header("BET View")]
    [SerializeField]
    private GameObject betViewObj;
    [SerializeField]
    private Transform defaultPos, downPos;


    [SerializeField]
    private GameObject betContentsObj;
    private bool isShowBetView = false;

    [SerializeField]
    private GameObject betConfirmObj;
    private bool isShowConfirmView = false;


    [Header("BET button")]
    public Button betBtn;

    [Header("Cancel button")]
    public Button cancelBtn;





    [Header("Popup")]
    public GameObject bettingPopup;
    public GameObject nowPlayingView;

    private bool isDisableClick;
    private RaceBettingRequest bettingRequest = new RaceBettingRequest();


    public int myPickIndex => bettingRequest.figure;


    protected override void InitIns() => Ins = this;



    private void Start()
    {
        toggleGroup = selectFishToggles[0].group;
        selectFishToggles[0].onValueChanged.AddListener((isOn) => { setupViewBgDog(isOn, 1); });
        selectFishToggles[1].onValueChanged.AddListener((isOn) => { setupViewBgDog(isOn, 2); });

        price1ViewBtn.OnClicked += () =>
        {
            Actionprice1View();
        };
        resetBtn.GetComponent<Button>().onClickWithHCSound(() =>
        {
            if (isDisableClick) return;
            bettingRequest.amount = 0;
            coinFeeTxt.GetComponent<TextMeshProUGUI>().text = "0";
        });



        tutorialImg.GetComponent<Button>().onClickWithHCSound(() =>
        {
            UIFish.Ins.tutorialPopup.GetComponent<HowToPlayMinigame>().InitTut();
            UIFish.Ins.tutorialPopup.SetActive(true);
        });
    }
    private void OnEnable()
    {

        InvokeRepeating("CheckHoldingBetButton", 0.0f, 0.25f);
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        CancelInvoke();
    }

    private RaceRoomResponse.RaceRoom raceRoomData = null;

    public void InitRoom(RaceRoomResponse.RaceRoom raceRoom)
    {
        raceRoomData = raceRoom;
        bettingRequest = new RaceBettingRequest();
        gameObject.SetActive(true);
        //nowPlayingView.SetActive(false);

        betViewObj.transform.position = defaultPos.position;

        coinFeeTxt.GetComponent<TextMeshProUGUI>().text = StringHelper.NumberFormatter(raceRoom.yourBetting);
        var myToken = (int)Current.Ins.player.HCTokenCoin;
        setupViewButton(time: raceRoom.timeRemain, yourBetting: raceRoom.yourBetting);
        isDisableClick = raceRoom.yourBetting > 0;
        setupViewRateDog(raceRoom.figures);




        bettingRequest.figure = raceRoom.yourFigure;


        toggleGroup.allowSwitchOff = true;

        Array.ForEach(selectFishToggles, obj => obj.isOn = false);


        betContentsObj.SetActive(false);
        betConfirmObj.SetActive(false);


        isShowBetView = false;
        isShowConfirmView = false;
        Array.ForEach(selectFishToggles, obj => obj.interactable = true);
        if (raceRoom.yourFigure > 0)
        {
            selectFishToggles[raceRoom.yourFigure - 1].isOn = true;
            setupViewBgDog(true, raceRoom.yourFigure);
            ShowBetViewObject();
            Array.ForEach(selectFishToggles, obj => obj.interactable = false);

        }

        if (raceRoom.yourBetting > 0)
        {
            ShowBetConfirmObject();
        }
        betBtn.onClick.RemoveAllListeners();
        betBtn.onClick.AddListener(() =>
        {
            if (isDisableClick) return;


            SoundManager.instance.PlayUIEffect("se_select");
            handleBettingRace(raceRoom.id, bettingRequest);
        });
        cancelBtn.onClick.RemoveAllListeners();
        cancelBtn.onClick.AddListener(() =>
        {
            SoundManager.instance.PlayUIEffect("se_cancel");
            bettingPopup.GetComponent<BettingConfirmPopup>().initViewCancel(numberBetting: raceRoom.yourBetting, confirmAction: () =>
            {
                handleCancelBetting(raceRoom.id);
            });
        });

        tutorialImg.GetComponent<Button>().interactable = true;
    }


    private void CheckHoldingBetButton()
    {
        if (!price1ViewBtn.IsHoldingButton && price1ViewBtn.IsPressButton)
        {
            // Thực hiện hành động ở đây, ví dụ: in ra thông báo
            Actionprice1View();
        }
    }

    void Actionprice1View()
    {
        if (isDisableClick) return;
        ShowBetConfirmObject();


        if (bettingRequest.amount >= Mathf.Min((int)Current.Ins.player.HCTokenCoin, raceRoomData.limit) || bettingRequest.amount + 1000 > Current.Ins.player.HCTokenCoin)
            return;


        SoundManager.instance.PlayUIEffect("se_buy");

        bettingRequest.amount += 1000;
        coinFeeTxt.GetComponent<TextMeshProUGUI>().text = $"{bettingRequest.amount:n0}";
    }

    private IEnumerator price1ViewHoldBtn(int myToken, int limit)
    {
        while (!price1ViewBtn.IsHoldingButton && price1ViewBtn.IsPressButton)
        {
            // Thực hiện hành động ở đây, ví dụ: in ra thông báo
            Actionprice1View();
            yield return new WaitForSeconds(0.25f);
        }
    }


    private void handleCancelBetting(int id)
    {
        UIPopUpManager.instance.ShowLoadingCircle(true);
        Action<BaseResponse> callBackResponse = (res) =>
        {
            if (res == null || res.errorCode != 0)
            {

                UIPopUpManager.instance.ShowErrorPopUp("Request Error : handleCancelBetting", res, () => handleCancelBetting(id));

                return;
            }
            UIPopUpManager.instance.ShowLoadingCircle(false);

            bettingPopup.GetComponent<BettingConfirmPopup>().RemoveListeners_confirmBtn();
            UIFish.Ins.ReloadUserProfile(bettingPopup.GetComponent<BettingConfirmPopup>().confirmBtn.transform, () =>
            {
                Current.Ins.ReloadUserProfile(callBackAfterDoneAPI: () =>
                {
                    bettingPopup.GetComponent<BettingConfirmPopup>().gameObject.SetActive(false);
                    UIFish.Ins.ReflashRaceScene();
                    //SceneHelper.ReloadScene();
                });
            });
        };
        StartCoroutine(Current.Ins.fishApi.cancleBetting(id, callBackResponse));
    }

    public void setupViewButton(float time, int yourBetting)
    {
        if (time > 10)
        {
            betBtn.interactable = true;
            cancelBtn.interactable = true;
            betBtn.gameObject.SetActive(yourBetting == 0);
            cancelBtn.gameObject.SetActive(yourBetting > 0);
        }
        else
        {
            betBtn.interactable = (time > 10);
            cancelBtn.interactable = (time > 10);
        }

        if (Current.Ins.player.HCTokenCoin < 1000)
        {
            betBtn.interactable = false;
        }

        if (time < 5)
        {
            this.tutorialImg.GetComponent<Button>().interactable = false;
        }
    }



    public void ShowBetViewObject()
    {
        if (isShowBetView)
            return;

        isShowBetView = true;
        Sequence sq = DOTween.Sequence();
        sq.Join(betViewObj.transform.DOMoveY(downPos.position.y, 0.2f).SetEase(Ease.OutCubic));
        sq.Play().OnComplete(() => {
            betContentsObj.SetActive(true);
        });
    }
    public void ShowBetConfirmObject()
    {
        if (isShowConfirmView)
            return;

        isShowConfirmView = true;

        betConfirmObj.SetActive(true);
    }

    private void handleBettingRace(int roomId, RaceBettingRequest bettingRequest)
    {
        LanguageEnum language = Current.Ins.Config.GetCurrentLanguage();
        string emptyFigure = "";
        string emptyAmount = "";
        switch (language)
        {
            case LanguageEnum.eng:
                emptyFigure = "You must pick dog!";
                emptyAmount = "You must bet!";
                break;
            case LanguageEnum.kor:
                emptyFigure = "개를 골라야 해요!";
                emptyAmount = "내기를 해야 해!";
                break;
            case LanguageEnum.vi:
                emptyFigure = "Bạn chưa chọn con chó";
                emptyAmount = "Bạn chưa đặt cược!";
                break;
        }
        if (bettingRequest.figure == 0)
        {
            bettingPopup.GetComponent<BettingConfirmPopup>().initView(isBetting: false, errror: emptyFigure);
            return;
        }
        if (bettingRequest.amount == 0)
        {
            bettingPopup.GetComponent<BettingConfirmPopup>().initView(isBetting: false, errror: emptyAmount);
            return;
        }
        bettingPopup.GetComponent<BettingConfirmPopup>().initView(isBetting: true, numberBetting: bettingRequest.amount, confirmAction: () =>
        {
            confirmBettingRace(roomId, bettingRequest);
        });
    }

    private void confirmBettingRace(int roomId, RaceBettingRequest bettingRequest)
    {
        UIPopUpManager.instance.ShowLoadingCircle(true);
        Action<BaseResponse> callBackResponse = (res) =>
        {
            if (res == null || res.errorCode != 0)
            {
                UIPopUpManager.instance.ShowErrorPopUp("Request Error : confirmBettingRace", res, () => handleBettingRace(roomId, bettingRequest));

                return;
            }

            Array.ForEach(selectFishToggles, obj => obj.interactable = false);

            bettingPopup.GetComponent<BettingConfirmPopup>().gameObject.SetActive(false);
            disableView();
            UIFish.Ins.myBetting = bettingRequest.amount;

            betBtn.gameObject.SetActive(false);
            cancelBtn.gameObject.SetActive(true);

            cancelBtn.GetComponent<Button>().onClickWithHCSound(() =>
            {
                bettingPopup.GetComponent<BettingConfirmPopup>().initViewCancel(numberBetting: bettingRequest.amount, confirmAction: () =>
                {
                    handleCancelBetting(roomId);
                });
            });
            changeOwnedAssets();
            UIPopUpManager.instance.ShowLoadingCircle(false);
        };
        StartCoroutine(Current.Ins.fishApi.bettingRaceFish<BaseResponse>(roomId, bettingRequest, callBackResponse));
    }

    public void changeOwnedAssets()
    {
        Current.Ins.ReloadUserProfile(callBackAfterDoneAPI: () =>
        {
            var myToken = (int)Current.Ins.player.HCTokenCoin;
            UIFish.Ins.myTokenTxt.GetComponent<TextMeshProUGUI>().text = $"{StringHelper.NumberFormatter(myToken)}";
        });
    }
    public void setupViewRateDog(List<RaceRoomResponse.Figure> figures)
    {
        if (figures.Count < 2) return;

        for (int i = 0; i < 2; i++)
        {
            FishBetRates[i].text = $"x{figures[i].rate:0.00}";
        }
        UIFish.Ins.SetBetRateArr(new float[] { figures[0].rate, figures[1].rate});
    }

    public void disableView()
    {
        isDisableClick = true;
    }

    public void setupViewBgDog(bool isOn, int figure)
    {
        if (isOn)
        {
            SoundManager.instance.PlayUIEffect("se_select");
            bettingRequest.figure = figure;
            toggleGroup.allowSwitchOff = false;

            ShowBetViewObject();
        }
    }
}
