using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public enum FishingRodState
{
    ready,
    throwLure,
    realing,
    hit,
    grab
}



public class FishingRod : MonoBehaviour
{
    private Animator aniCon => GetComponent<Animator>();
    private LineRenderer lineRenderer => GetComponent<LineRenderer>();

    private FishingRodState currentState = FishingRodState.ready;

    private Vector3 originPos = Vector3.zero, originRot = Vector3.zero;
    private bool isRotate = false;
    private float lookRotY = 0.0f;

    private bool isRealingAction = false;
    public bool IsRealingAction => isRealingAction;

    [SerializeField]
    private Transform[] rodPoints;
    [SerializeField]
    private Transform lurePoint, hookPoint;




    [SerializeField]
    private float minDir = 100.0f, maxDir = 115.0f, distValue = 35.0f;

    public Transform RodEndPoint => rodPoints[rodPoints.Length - 1];



    // Start is called before the first frame update
    void Start()
    {
        if (LureGameManager.instance.lureObj)
            LureGameManager.instance.lureObj.gameObject.SetActive(false);
        originPos = transform.position;
        originRot = transform.eulerAngles;
    }



    private void Awake()
    {
        LureGameManager.instance.fishingRod = this;
    }


    // Update is called once per frame
    void Update()
    {
        if (LureGameManager.instance.lureObj && LureGameManager.instance.lureObj.MoveState <= LureMoveState.DragFish)
        {
            lineRenderer.positionCount = rodPoints.Length + (LureGameManager.instance.lureObj.gameObject.activeSelf ? 1 : 0);

            for (int i = 0; i < rodPoints.Length; i++)
            {
                lineRenderer.SetPosition(i, rodPoints[i].position);
            }


            if (LureGameManager.instance.lureObj && LureGameManager.instance.lureObj.gameObject.activeSelf)
                lineRenderer.SetPosition(rodPoints.Length, lurePoint.position);
        }
        else
        {
            lineRenderer.positionCount = 0;
        }



        if (isRotate)
        {

            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(transform.eulerAngles.x, lookRotY, transform.eulerAngles.z), Time.deltaTime * 3.0f);
            if (Mathf.Abs(lookRotY - transform.eulerAngles.y) < 1.0f)
            {
                isRotate = false;
            }
        }


        switch (currentState)
        {
            case FishingRodState.ready:
                { }
                break;
            case FishingRodState.throwLure:
                {
                    
                }
                break;
            case FishingRodState.realing:
                {



                
                }
                break;
            case FishingRodState.hit:
                { }
                break;
            case FishingRodState.grab:
                { }
                break;
        }

    }

    public void ChangeCurrentState(FishingRodState lastState)
    {

        switch (lastState)
        {
            case FishingRodState.ready:
                {
                    aniCon.Play("Ready");
                    aniCon.SetBool("Stay", false);

                    transform.position = originPos;
                    transform.eulerAngles = originRot;
                    lookRotY = originRot.y;
                    isRotate = false;
                }
                break;
            case FishingRodState.throwLure:
                {
                    LureObj lureObj = LureGameManager.instance.lureObj;
                    if (lureObj)
                    {
                        lureObj.transform.position = RodEndPoint.position;
                        lureObj.gameObject.SetActive(true);

                        Vector3 destPos = LureGameManager.GetDegreeDistVector(transform.position, Random.Range(minDir, maxDir), distValue);
                        destPos.y = -8.0f;
                        lureObj.ShootLure(destPos);

                        Quaternion lookQua = Quaternion.LookRotation(destPos - transform.position);
                        lookRotY = lookQua.eulerAngles.y + 90;
                        isRotate = true;
                    }

                }
                break;
            case FishingRodState.realing:
                {
                    if (LureGameManager.instance.lureObj)
                        lineDist = (transform.position - LureGameManager.instance.lureObj.transform.position).magnitude;

                    aniCon.SetTrigger("Realing");

                    


                }
                break;
            case FishingRodState.hit:
                {
                    aniCon.SetTrigger("Hit");
                    SoundManager.instance.PlayEffect("se_fishHit");
                    StartCoroutine(HitRealingRepeat(6, 0.5f));
                    IEnumerator HitRealingRepeat(int count, float delay)
                    {
                        for (int i = 0; i < count; i++)
                        {
                            SoundManager.instance.PlayEffect("se_fishRealing");
                            yield return new WaitForSeconds(delay);
                        }
                    
                    }
                }
                break;
            case FishingRodState.grab:
                { }
                break;
        }
        currentState = lastState;
    }

    float lineDist = 0.0f;
    Coroutine ThrowLureCoroutine = null;
    public void ShootRod()
    {

        

        aniCon.Play("Ready");
        aniCon.SetTrigger("Throw");
        aniCon.SetBool("Stay", false);
        if (LureGameManager.instance.lureObj)
            LureGameManager.instance.lureObj.gameObject.SetActive(false);

        //isForwardMoving = true;
        Vector3 forwardPos = transform.position + (transform.right * -0.35f);
        Vector3 backPos = transform.position + (transform.right * 2.5f);
        Sequence sq = DOTween.Sequence();

        sq.Join(transform.DOMove(forwardPos, 0.5f).SetEase(Ease.InOutSine));
        //sq.AppendInterval(1.0f);
        sq.Append(transform.DOMove(backPos, 0.8f).SetEase(Ease.InOutSine));
        sq.Append(transform.DOMove(originPos, 0.5f).SetEase(Ease.InOutSine));

        sq.Play();

        //LureGameManager.instance.SetDicisionFish();

        isRotate = false;

        //transform.eulerAngles = new Vector3(transform.eulerAngles.x, originRotY, transform.eulerAngles.z);
        if (ThrowLureCoroutine != null)
            StopCoroutine(ThrowLureCoroutine);


        ThrowLureCoroutine = StartCoroutine(ThrowLure());
        IEnumerator ThrowLure()
        {
            yield return new WaitForSeconds(0.8f);

            SoundManager.instance.PlayEffect("se_fishRodShoot");

            ChangeCurrentState(FishingRodState.throwLure);
            isRealingAction = true;
            for (int i = 0; i < 3; i++)
            {
                float delayStay = Random.Range(3.0f, 4.0f);
                yield return new WaitForSeconds(delayStay);
                isRealingAction = false;
                aniCon.SetBool("Stay", true);

                yield return new WaitForSeconds(2.0f);
                isRealingAction = true;
                aniCon.SetBool("Stay", false);
            }
        }
    }
}
