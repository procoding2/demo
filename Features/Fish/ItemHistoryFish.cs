﻿using OutGameEnum;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ItemHistoryFish : MonoBehaviour
{
    [SerializeField]
    private GameObject[] bgArr;

    [SerializeField]
    private TextMeshProUGUI timeTxt;

    [SerializeField]
    private GameObject[] crownImages, pickImages;

    [SerializeField]
    private Image[] fishImages;

    [SerializeField]
    private Sprite[] smallFishSprites, bigFishSprites;

    [SerializeField]
    private Button copyBtn, claimBtn;

    [SerializeField]
    private GameObject prizeObj;

    [SerializeField]
    private TextMeshProUGUI hashCodeTxt, myBetTxt, prizeTxt;

    [SerializeField]
    private TextMeshProUGUI[] rateTxts = new TextMeshProUGUI[2];

    [SerializeField]
    private GameObject jackpotInfo;
    [SerializeField]
    private TextMeshProUGUI jackpotValue;


    public void Init(RaceHistoryResponse.RaceHistory raceHistory)
    {
        var isWin = raceHistory.reward.amount > 0;
        Array.ForEach(crownImages, obj => obj.SetActive(false));

        bgArr[0].SetActive(isWin);
        bgArr[1].SetActive(!isWin);
        prizeObj.SetActive(isWin);
        jackpotInfo.SetActive(false);

        int fishIndex = LureGameManager.GetIndexFromHashCode(raceHistory.hash, 2) == 8 ? (int)FishType.GoldenFish : LureGameManager.GetIndexFromHashCodeMod(raceHistory.hash);

        fishImages[0].sprite = smallFishSprites[fishIndex];
        fishImages[1].sprite = bigFishSprites[fishIndex];

        timeTxt.GetComponent<TextMeshProUGUI>().text = Utils.DateTimeUtil.getTimeHistory(raceHistory.endTime);

        if (raceHistory.results.Count > 0)
        {
            var figure = raceHistory.results.FindLast((result) => result.figure == 1);
            setupViewNumber(0, figure.rank, figure.rate);
        }
        if (raceHistory.results.Count > 1)
        {
            var figure = raceHistory.results.FindLast((result) => result.figure == 2);
            setupViewNumber(1, figure.rank, figure.rate);
        }
        //if (raceHistory.results.Count > 2)
        //{
        //    var figure = raceHistory.results.FindLast((result) => result.figure == 3);
        //    setupViewNumber(2, figure.rank, figure.rate);
        //}

        if (raceHistory.reward.jackpot > 0 && isWin)
        {
            jackpotInfo.SetActive(true);
            jackpotValue.text = $"{raceHistory.reward.jackpot:n0}";

            fishImages[0].sprite = smallFishSprites[(int)FishType.GoldenFish];
            fishImages[1].sprite = bigFishSprites[(int)FishType.GoldenFish];
        }

        //
        LanguageEnum language = Current.Ins.Config.GetCurrentLanguage();
        string hashCode = "";
        string Bet = "";
        string Prize = "";
        switch (language)
        {
            case LanguageEnum.eng:
                hashCode = $"Hash code: {raceHistory.hash}";
                Bet = "Bet";
                Prize = "Prize";
                break;
            case LanguageEnum.kor:
                hashCode = $"해시 코드:  {raceHistory.hash}";
                Bet = "베팅";
                Prize = "보상";
                break;
            case LanguageEnum.vi:
                hashCode = $"Hash code:  {raceHistory.hash}";
                Bet = "Cược";
                Prize = "Giải thưởng";
                break;
        }
        hashCodeTxt.text = hashCode;
        //

        Array.ForEach(pickImages, obj => obj.SetActive(false));

        if (raceHistory.betting.figure <= pickImages.Length)
            pickImages[raceHistory.betting.figure - 1].SetActive(true);

        myBetTxt.text = $"{Bet}: {StringHelper.NumberFormatter(raceHistory.betting.amount)}";
        prizeTxt.text = $"{Prize}: {StringHelper.NumberFormatter(raceHistory.reward.total)}";
        claimBtn.gameObject.SetActive(isWin);
        claimBtn.interactable = (isWin && raceHistory.reward.isClaimed == 0);

        copyBtn.onClick.AddListener(() =>
        {
            GUIUtility.systemCopyBuffer = raceHistory.hash;
        });

        claimBtn.onClickWithHCSound(() => getUserBettingReward(raceHistory.id, claimBtn, raceHistory.betting));
    }

    private void getUserBettingReward(int id, Button claimBtn, RaceHistoryResponse.Betting bet)
    {
        UIPopUpManager.instance.ShowLoadingCircle(true);
        StartCoroutine(Current.Ins.fishApi.getUserBettingReward(id, (res) =>
        {
            if (res == null || res.errorCode != 0)
            {
                Debug.Log("Claim Error:");

                UIPopUpManager.instance.ShowErrorPopUp("Request Error : getUserBettingReward", res, () => getUserBettingReward(id, claimBtn, bet));

                return;
            }

            UIFish.Ins.claimPopup.ShowRewardClaimPopup(id, claimBtn, bet, res);
            UIPopUpManager.instance.ShowLoadingCircle(false);
        }));
    }

    private void handleClaimReward(int id)
    {
        UIPopUpManager.instance.ShowLoadingCircle(true);
        StartCoroutine(Current.Ins.fishApi.claimRewardFish(roomId: id, (res) =>
        {
            if (res == null || res.errorCode != 0)
            {

                UIPopUpManager.instance.ShowErrorPopUp("Request Error : handleClaimReward", res, () => handleClaimReward(id));

                return;
            }
            claimBtn.interactable = false;
            PickRace.Ins.changeOwnedAssets();
            UIPopUpManager.instance.ShowLoadingCircle(false);
        }));
    }

    private void setupViewNumber(int index, int rank, float rate)
    {

        rateTxts[index].text = $"x{rate:0.00}";
        crownImages[index].SetActive(rank == 1);
        fishImages[index].color = (rank == 1) ? Color.white : Color.gray;
    }
}
