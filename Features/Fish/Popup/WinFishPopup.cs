﻿using OutGameEnum;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Localization.Components;
using System.Linq;
using Unity.VisualScripting;

public class WinFishPopup : MonoBehaviour
{


    [SerializeField]
    private TextMeshProUGUI bettingTxt;

    [SerializeField]
    private TextMeshProUGUI rateTxt;

    [SerializeField]
    private TextMeshProUGUI jackpot;

    [SerializeField]
    private TextMeshProUGUI totalTxt;

    [SerializeField]
    private TextMeshProUGUI closeTxt;
    [SerializeField]
    private LocalizeStringEvent timeClose;

    [SerializeField]
    private Button claimBtn;


    [SerializeField]
    private GameObject infoObj, winTitleObj, loseTitleObj;

    [SerializeField]
    private List<GameObject> showResultObjs;


    private int _roomId;

    private float betValue = 0.0f, rateValue = 0.0f, jackpotValue = 0.0f, totalValue = 0.0f, exValue = 0.0f;

    public void initView(RaceUserRewardResponese userReward, int roomId)
    {
        initView(userReward.data.amountBetting, userReward.data.amount, userReward.data.rate, userReward.data.jackpot, userReward.data.total, roomId);
    }

    public void initView(int _bet, int _amount, float _rate, int _jackpot, int _total, int roomId)
    {
        //int winIndex = DogRaceManager.instance.GetLastRaceFixedWinner();
        //0 == Win -> get index

        int winIndex = 0;// UIFish.Ins.BettingResult.ranks.First();
        int pickIndex = 0;// PickFish.Ins.myPickIndex;

        bool isWinGame = winIndex == pickIndex;


        winTitleObj.SetActive(_total>0);
        loseTitleObj.SetActive(_total==0);



        _roomId = roomId;

        infoObj.SetActive(true);
        claimBtn.gameObject.SetActive(false);
        closeTxt.gameObject.SetActive(false);

        showResultObjs.ForEach(obj => obj.SetActive(false));


        betValue = (float)_bet;
        rateValue = _rate;
        jackpotValue = (float)_jackpot;
        totalValue = (float)_total;


        exValue = 0.0f;

        bettingTxt.text = "0";//$"{_bet:n0}";
        jackpot.text = "0";//$"{_jackpot:n0}";
        totalTxt.text = "0";//$"{_total:n0}";

        gameObject.SetActive(true);


        StartCoroutine(ShowingResultValueUpdate(_total > 0));

    }
    public void AutoCloseView()
    {

        winTitleObj.SetActive(false);
        loseTitleObj.SetActive(false);


        infoObj.SetActive(false);
        claimBtn.gameObject.SetActive(false);
        closeTxt.gameObject.SetActive(false);



        gameObject.SetActive(true);

        StartCoroutine(DelayClose());


    }

    private IEnumerator ShowingResultValueUpdate(bool isShowClaimBtn)
    {
        int seqIndex = 0;
        float goalValue = 0.0f;
        TextMeshProUGUI updateText = null;

        yield return new WaitForSeconds(0.5f);


        while (seqIndex < 4)
        {
            switch (seqIndex)
            {
                case 0: //bet
                    goalValue = betValue;
                    updateText = bettingTxt;
                    break;
                case 1: //rate
                    goalValue = rateValue;
                    updateText = rateTxt;
                    break;
                case 2: //jackpot
                    goalValue = jackpotValue;
                    updateText = jackpot;
                    break;
                case 3: //total
                    goalValue = totalValue;
                    updateText = totalTxt;
                    break;


            }
            exValue = 0.0f;
            showResultObjs[seqIndex].SetActive(true);

            yield return new WaitForSeconds(0.25f);
            SoundManager.instance.PlayUIEffect("se_type2");

            if (seqIndex == 1)
            {
                updateText.text = $"x{goalValue:0.00}";
            }
            else
            {
                updateText.text = $"{(int)goalValue:n0}";
            }

            seqIndex++;
        }
        yield return new WaitForSeconds(0.5f);


        claimBtn.gameObject.SetActive(isShowClaimBtn);

        SoundManager.instance.PlayUIEffect("se_type2");
        StartCoroutine(DelayClose());
    }

    private IEnumerator DelayClose()
    {
        closeTxt.gameObject.SetActive(true);

        for (int i = 8; i > 0; i--)
        {
            //closeTxt.text = $"Automatically closes after {i} seconds.";
            timeClose.UpdateValue("time", VariableEnum.Int, i);
            SoundManager.instance.PlayUIEffect("se_muted");
            yield return new WaitForSeconds(1.0f);
        }

        gameObject.SetActive(false);
        UIFish.Ins.ReflashRaceScene();
    }

    public void CloseThisPop()
    {
        gameObject.SetActive(false);
        UIFish.Ins.ReflashRaceScene();
        //SceneHelper.ReloadScene();
    }

    private float AddVaue(float curValue, float goalValue)
    {

        float diff = Mathf.Abs(goalValue - curValue);

        float value = diff switch
        {
            > 10000.0f => 10000.0f,
            > 1000.0f => 1000.0f,
            > 100.0f => 100.0f,
            > 45.0f => 45.0f,
            _ => 1
        };

        return value;

    }


    private void Start()
    {
        claimBtn.onClickWithHCSound(() =>
        {
            handleClaimReward();
        });
    }

    private void handleClaimReward()
    {
        UIPopUpManager.instance.ShowLoadingCircle(true);
        StartCoroutine(Current.Ins.fishApi.claimRewardFish(roomId: _roomId, (res) =>
        {
            if (res == null || res.errorCode != 0)
            {

                UIPopUpManager.instance.ShowErrorPopUp("Request Error : handleClaimReward", res, handleClaimReward);


                return;
            }
            UIPopUpManager.instance.ShowLoadingCircle(false);

            //PickRace.Ins.changeOwnedAssets();
            UIFish.Ins.ReloadUserProfile(claimBtn.transform, () =>
            {
                gameObject.SetActive(false);
                SceneHelper.ReloadScene();
            });
            //UIFish.Ins.ReflashRaceScene();
            //DogRaceManager.instance.ResetRaceGame();

        }));
    }
}
