using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;



public enum LureMoveState
{
    InWay,
    OutWay,
    CatchFish,
    DragFish,
    LiftFish,
}


public class LureObj : MonoBehaviour
{

    [SerializeField]
    private Transform hookForm;

    [SerializeField]
    private Transform effectForm;

    [SerializeField]
    private GameObject splashEff, ripplesEff;

    private LureMoveState moveState = LureMoveState.InWay;
    public LureMoveState MoveState => moveState;


    private bool isBiteAble = false;
    public bool IsBiteAble => isBiteAble;

    [SerializeField]
    float lureSpeed = 5.0f;

    Vector3 startPos, endPos, centerPos;
    float startTime;



    private Fish catchedFish = null;
    public Fish CatchedFish => catchedFish;

    public float dist = 0.01f;

    // Start is called before the first frame update
    void Start()
    {
    }

    private void Awake()
    {
        LureGameManager.instance.lureObj = this;
    }
    private void OnDisable()
    {
        moveState = LureMoveState.InWay;
        isBiteAble = false;
    }

    private void OnDestroy()
    {
        CancelInvoke();
    }
    // Update is called once per frame
    void Update()
    {
        if (gameObject.activeSelf == false)
            return;

        effectForm.position = GetLineAtWaterGround();

        Vector3 splashPos = Vector3.Lerp(transform.position, Camera.main.transform.position, dist);
        splashPos.y = -6.0f;

        splashEff.transform.position = splashPos;
        if (moveState <= LureMoveState.CatchFish)
        {
            Vector3 viewPos = endPos - transform.position;
            if (viewPos.Equals(Vector3.zero) == false)
            {
                Quaternion viewAngle = Quaternion.LookRotation(viewPos);
                viewAngle.eulerAngles = new Vector3(viewAngle.eulerAngles.x, viewAngle.eulerAngles.y - 90, viewAngle.eulerAngles.z);
                transform.rotation = Quaternion.Lerp(transform.rotation, viewAngle, Time.deltaTime * 5.0f);
            }

        }


        if (moveState == LureMoveState.InWay)
        {
            if (LureGameManager.instance.fishingRod.IsRealingAction)
                transform.position = Vector3.MoveTowards(transform.position, endPos, Time.deltaTime * lureSpeed);

            ripplesEff.SetActive(true);
            splashEff.SetActive(false);


        }
        else if (moveState == LureMoveState.OutWay)
        {
            //ripplesTail.SetActive(false);
            float fracComplete = (Time.time - startTime) / 1.0f;
            transform.position = Vector3.Slerp(startPos - centerPos, endPos - centerPos, fracComplete);
            transform.position += centerPos;

            if ((endPos - transform.position).sqrMagnitude < 2.1f)
            {
                SoundManager.instance.PlayEffect("se_lurePong");
                LureGameManager.instance.ShowBigSplash(transform.position);

                moveState = LureMoveState.InWay;
                StartCoroutine(OutWayEnd());
            }

            ripplesEff.SetActive(false);
            splashEff.SetActive(false);
        }
        else if (moveState == LureMoveState.CatchFish)
        {
            if ((endPos - transform.position).sqrMagnitude < 1.0f)
            {
                //LureGameManager.instance.LastBigSplash(transform.position);// Random.Range(2.5f, 3.5f));
                Vector2 circlePos = Random.insideUnitCircle * 1.25f;

                endPos = transform.position + new Vector3(circlePos.x, 0.0f, circlePos.y);

                //hookForm.DORotate(new Vector3(0.0f, Random.Range(10.0f, 310.0f), Random.Range(10.0f, 160.0f)), 0.1f).SetEase(Ease.InSine);                
            }
            ripplesEff.SetActive(false);
            splashEff.SetActive(true);

            Vector3 veiwVec = Quaternion.LookRotation(endPos - transform.position).eulerAngles;
            veiwVec.y -= 90.0f;

            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(veiwVec), Time.deltaTime * 4.0f);
            transform.position = Vector3.Lerp(transform.position, endPos, Time.deltaTime * 4.0f);
        }
        else if (moveState == LureMoveState.DragFish)
        {
            if ((endPos - transform.position).sqrMagnitude < 1.0f)
            {
                //LureGameManager.instance.ShowBigSplash(transform.position, Random.Range(0.5f, 1.0f));
                Vector3 RodPoint = LureGameManager.instance.fishingRod.RodEndPoint.position;
                RodPoint.y = -8.0f;

                endPos = Vector3.Lerp(transform.position, RodPoint, 0.2f);

                //hookForm.DORotate(new Vector3(0.0f, Random.Range(10.0f, 310.0f), Random.Range(10.0f, 160.0f)), 0.1f).SetEase(Ease.InSine);                
            }
            ripplesEff.SetActive(false);
            splashEff.SetActive(true);

            Vector3 veiwVec = Quaternion.LookRotation(endPos - transform.position).eulerAngles;
            veiwVec.y -= 90.0f;
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(veiwVec), Time.deltaTime * 4.0f);
            transform.position = Vector3.Lerp(transform.position, endPos, Time.deltaTime * 4.0f);
        }
        else// rift fish
        {
            ripplesEff.SetActive(false);
            splashEff.SetActive(false);
        }
    }

    private IEnumerator OutWayEnd()
    {
        yield return new WaitForSeconds(0.3f);
        endPos = new Vector3(-60.0f, -8.0f, -44.0f);
        LureGameManager.instance.fishingRod?.ChangeCurrentState(FishingRodState.realing);

        yield return new WaitForSeconds(Random.Range(1.5f, 2.5f));
        isBiteAble = true;
    }


    public void ShootLure(Vector3 _destPos)
    {
        if (catchedFish)
        {
            catchedFish.transform.SetParent(ObjectPoolManager.instance.transform, true);
            catchedFish.gameObject.SetActive(false);

            LureGameManager.instance.GenFishObjects();

            catchedFish = null;
        }


        startPos = transform.position;
        endPos = _destPos;

        centerPos = (startPos + endPos) * 0.5f;
        centerPos -= new Vector3(0, 1, 0);

        startTime = Time.time;

        isBiteAble = false;
        moveState = LureMoveState.OutWay;

    }


    private Vector3 GetLineAtWaterGround()
    {
        Vector3 pos = Vector3.zero;
        if (LureGameManager.instance.fishingRod)
            pos = Vector3.Lerp(transform.position, LureGameManager.instance.fishingRod.RodEndPoint.position, 0.25f);

        pos.y = -7.0f;
        return pos;
    }

    public void SetCatchFish(Fish _fish)
    {
        isBiteAble = false;



        if (catchedFish)
        {
            catchedFish.transform.SetParent(ObjectPoolManager.instance.transform, true);
        }

        catchedFish = _fish;
        if (catchedFish == null)
        {
            moveState = LureMoveState.InWay;
            return;
        }

        
        moveState = LureMoveState.CatchFish;

        UIFish.Ins.ShowCountText("Hit!!");
        SoundManager.instance.PlayUIEffect("se_fishHitSound");

        catchedFish.transform.SetParent(hookForm, true);

        catchedFish.transform.localRotation = Quaternion.Euler(0, 180, 0);
        catchedFish.transform.localPosition = new Vector3(-0.3f, -0.3f, 0.03f);

        Vector3 RodPoint = LureGameManager.instance.fishingRod.RodEndPoint.position;
        RodPoint.y = -8.0f;

        endPos = Vector3.Lerp(transform.position, RodPoint, 0.2f);

        LureGameManager.instance.fishingRod.ChangeCurrentState(FishingRodState.hit);



        StartCoroutine(NextEndFishingDelay());

        IEnumerator NextEndFishingDelay()
        {
            //yield return new WaitForSeconds(0.2f);
            //LureGameManager.instance.LastBigSplash(transform.position, 3.0f);
            yield return new WaitForSeconds(2.0f);

            moveState = LureMoveState.DragFish;

            yield return new WaitForSeconds(2.0f);

            LureGameManager.instance.ChangeRaceState(GameEnum.RaceState.End);
        }
    }

    public void ResultFishing(Transform form)
    {
        moveState = LureMoveState.LiftFish;

        catchedFish.transform.SetParent(form, true);
        catchedFish.transform.localPosition = Vector3.zero;
        catchedFish.transform.localRotation = Quaternion.Euler(Vector3.zero);

        Animator aniCon = form.GetComponent<Animator>();
        if (aniCon)
            aniCon.SetTrigger("Action");

    }
}
