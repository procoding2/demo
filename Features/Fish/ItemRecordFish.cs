using OutGameEnum;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ItemRecordFish : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI timeTxt;


    [SerializeField]
    private GameObject[] crownImages;

    [SerializeField]
    private Image[] fishImages;

    [SerializeField]
    private Sprite[] smallFishSprites, bigFishSprites;
    [SerializeField]
    private TextMeshProUGUI[] rateTxts;

    [SerializeField]
    private Button copyBtn;
    [SerializeField]
    private TextMeshProUGUI hashCodeTxt;

    [SerializeField]
    private GameObject jackpotInfo;
    [SerializeField]
    private TextMeshProUGUI jackpotValue;

    public void Init(RaceHistoryResponse.RaceHistory raceHistory)
    {
        timeTxt.text = Utils.DateTimeUtil.getTimeHistory(raceHistory.endTime);
        Array.ForEach(crownImages, obj => obj.SetActive(false));
        jackpotInfo.SetActive(false);

        int fishIndex = LureGameManager.GetIndexFromHashCode(raceHistory.hash, 2) == 8 ? (int)FishType.GoldenFish : LureGameManager.GetIndexFromHashCodeMod(raceHistory.hash);


        fishImages[0].sprite = smallFishSprites[fishIndex];
        fishImages[1].sprite = bigFishSprites[fishIndex];

        if (raceHistory.results.Count > 0)
        {
            var figure = raceHistory.results.FindLast((result) => result.figure == 1);
            setupViewNumber(0, figure.rank, figure.rate);
        }
        if (raceHistory.results.Count > 1)
        {
            var figure = raceHistory.results.FindLast((result) => result.figure == 2);
            setupViewNumber(1, figure.rank, figure.rate);
        }
        //if (raceHistory.results.Count > 2)
        //{
        //    var figure = raceHistory.results.FindLast((result) => result.figure == 3);
        //    setupViewNumber(2, figure.rank, figure.rate);
        //}

        if (raceHistory.jackpot > 0)
        {
            jackpotInfo.SetActive(true);
            jackpotValue.text = $"{raceHistory.jackpot:n0}";
        }

        //
        LanguageEnum language = Current.Ins.Config.GetCurrentLanguage();
        string hashCode = "";
        switch (language)
        {
            case LanguageEnum.eng:
                hashCode = $"Hash code: {raceHistory.hash}";
                break;
            case LanguageEnum.kor:
                hashCode = $"�ؽ� �ڵ�:  {raceHistory.hash}";
                break;
            case LanguageEnum.vi:
                hashCode = $"Hash code:  {raceHistory.hash}";
                break;
        }
        hashCodeTxt.GetComponent<TextMeshProUGUI>().text = hashCode;

        copyBtn.onClick.AddListener(() =>
        {
            GUIUtility.systemCopyBuffer = raceHistory.hash;
        });
    }

    private void setupViewNumber(int index, int rank, float rate)
    {


        rateTxts[index].text = $"x{rate:0.00}";
        crownImages[index].SetActive(rank == 1);

        fishImages[index].color = (rank == 1) ? Color.white : Color.gray;
    }
}

