
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameEnum;




public class LureGameManager : ManagerSingleton<LureGameManager>
{
    [SerializeField]
    private List<Fish> _fishList = new List<Fish>();
    public List<Fish> FishList => _fishList;

    [SerializeField]
    private List<GameObject> fishObjList = new List<GameObject>();
    [SerializeField]
    private GameObject goldenFishObj = null;

    [SerializeField]
    private GameObject bigSplash = null;

    [SerializeField]
    private Transform genCenterForm = null, resultForm = null;
    private Vector3 catchFishVector = Vector3.zero;


    [SerializeField]
    private Vector3 resultPosOffset;

    public Transform GenCenterForm => genCenterForm;


    public FishingRod fishingRod = null;
    public LureObj lureObj = null;


    private RaceState currentRaceState = RaceState.Prepare;
    public RaceState CurrentRaceState => currentRaceState;


    Camera mainCamera = null;


    [SerializeField]
    private Transform originCamForm, resultCamForm;


    [SerializeField]
    private float cameraMovSpeed = 10.0f;


    public float height = 0.0f;

    private Coroutine prepareCoroutine = null;


    private float _roomID;
    private Action _actionCallBack;

    private void Awake()
    {
        for (int i = 0; i < fishObjList.Count; i++)
        {
            ObjectPoolManager.instance.ReservePool(fishObjList[i], 10);
        }
        ObjectPoolManager.instance.ReservePool(goldenFishObj, 1);
        ObjectPoolManager.instance.ReservePool(bigSplash, 10);

        mainCamera = Camera.main;
        originCamForm.position = mainCamera.transform.position;
        originCamForm.rotation = mainCamera.transform.rotation;
        ChangeRaceState(RaceState.Prepare);




    }

    private void Start()
    {
    }

    private void OnEnable()
    {

    }

    private void OnDisable()
    {
        ClearFishList();
    }

    private void OnDestroy()
    {
    }

    private IEnumerator GenDelayProcess(int count)
    {
        for (int i = 0; i < count; i++)
        {
            GenFishObjects();
            yield return new WaitForSeconds(0.01f);
        }

        GenNewGoldenFishObject(UnityEngine.Random.Range(0, 2) == 0);

    }

    private void ClearFishList()
    {
        foreach (Fish fish in _fishList.ToArray())
        {
            fish.gameObject.SetActive(false);
        }

        _fishList.Clear();
    }





    // Update is called once per frame
    void Update()
    {
        switch (currentRaceState)
        {
            case RaceState.Prepare:
                {
                    mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, originCamForm.position, Time.deltaTime * cameraMovSpeed);
                    mainCamera.transform.rotation = Quaternion.Lerp(mainCamera.transform.rotation, originCamForm.rotation, Time.deltaTime * cameraMovSpeed);
                }
                break;
            case RaceState.Ready:
                {



                }
                break;
            case RaceState.Play:
                {


                }
                break;
            case RaceState.End:
                {
                    mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, catchFishVector, Time.deltaTime * cameraMovSpeed);
                    mainCamera.transform.rotation = Quaternion.Lerp(mainCamera.transform.rotation, resultCamForm.rotation, Time.deltaTime * cameraMovSpeed);
                }
                break;
        }
    }


    public void ChangeRaceState(RaceState changeState)
    {
        switch (changeState)
        {
            case RaceState.Prepare:
                {
                    SoundManager.instance.PlayMusic("bgm_ocean");
                    UIFish.Ins.CountDownObj.SetActive(false);
                    if (prepareCoroutine != null)
                    {
                        StopCoroutine(prepareCoroutine);
                        prepareCoroutine = null;
                    }
                    ClearFishList();
                    prepareCoroutine = StartCoroutine(GenDelayProcess(20));


                }
                break;
            case RaceState.Ready:
                {

                    //SoundManager.instance.PlayMusic("bgm_ocean", false); ;

                    UIFish.Ins.betVeiwObj.SetActive(false);
                    UIFish.Ins.jackPotView.gameObject.SetActive(false);

                    if (prepareCoroutine != null)
                    {
                        StopCoroutine(prepareCoroutine);
                        prepareCoroutine = null;
                    }



                    StartCoroutine(RaceStart());
                    IEnumerator RaceStart()
                    {
                        yield return new WaitForSeconds(0.5f);

                        for (int i = 3; i > 0; i--)
                        {
                            SoundManager.instance.PlayUIEffect("se_useClock");
                            UIFish.Ins.ShowCountText($"{i}");
                            yield return new WaitForSeconds(1.0f);

                        }

                        ChangeRaceState(RaceState.Play);
                    }
                }
                break;
            case RaceState.Play:
                {
                    _fishList.ForEach(fish =>
                    {
                        fish.SwimSpeed = 3.0f;
                        fish.NextPos = fish.transform.position;
                    });
                    UIFish.Ins.ShowCountText("Casting!");
                    UIFish.Ins.RetryFixedRank();
                    fishingRod.ShootRod();
                    
                }
                break;
            case RaceState.End:
                {

                    SoundManager.instance.PlayEffect("se_fishLanding");
                    fishingRod.ChangeCurrentState(FishingRodState.ready);
                    lureObj.ResultFishing(resultForm);

                    UIFish.Ins.lengthPop.ShowLentghResultPop(true);

                    catchFishVector = lureObj.CatchedFish.CenterPos + resultPosOffset;

                    StartCoroutine(RaceEnd());
                    IEnumerator RaceEnd()
                    {
                        yield return new WaitForSeconds(7.0f);
                        _actionCallBack?.Invoke();
                        //UIFish.Ins.lengthPop.ShowLentghResultPop(false);
                        //UIFish.Ins.TestResultPop();
                    }



                }
                break;
        }

        currentRaceState = changeState;
    }

    public void ResetRaceGame()
    {
        Animator aniCon = resultForm.GetComponent<Animator>();
        if (aniCon)
            aniCon.ResetTrigger("Action");


        fishingRod.ChangeCurrentState(FishingRodState.ready);
        lureObj.SetCatchFish(null);
        lureObj.gameObject.SetActive(false);

        ChangeRaceState(RaceState.Prepare);


    }

    public void GenFishObjects() => GenNewFishObject(UnityEngine.Random.Range(0, fishObjList.Count), UnityEngine.Random.Range(0, 2) == 0);
    public Fish GenNewFishObject(int index, bool isSmall)
    {
        GameObject rndFish = fishObjList[index];
        return GenFishObjectFromName(rndFish.name, isSmall);
    }
    public Fish GenNewGoldenFishObject(bool isSmall) => GenFishObjectFromName(goldenFishObj.name, isSmall);
    public Fish GenFishObjectFromName(string fishName, bool isSmall)
    {
        GameObject fish = ObjectPoolManager.instance.GetObject(fishName);


        Vector2 circlePos = Vector2.zero;
        do
        {
            circlePos = UnityEngine.Random.insideUnitCircle * 25.0f;
        }
        while (circlePos.y > 0.0f);
        Vector3 genPos = genCenterForm.position + new Vector3(circlePos.x, 0.0f, circlePos.y);

        fish.transform.position = genPos;

        Fish fishScript = fish.GetComponent<Fish>();
        if (fishScript)
        {
            fishScript.NextPos = genPos;
            float scale = 0.25f;


            if (isSmall)
                scale = UnityEngine.Random.Range(0.21f, 0.23f);
            else
                scale = UnityEngine.Random.Range(0.26f, 0.28f);

            fishScript.SetNewScale(scale);

        }

        return fishScript;
    }

    private Vector2 GetRandomGenPos(float radius)
    {


        Vector2 pos = Vector2.zero;
        do
        {
            pos = UnityEngine.Random.insideUnitCircle.normalized * radius;
        }
        while (pos.y > genCenterForm.position.y);


        return pos;
    }

    public void ShowBigSplash(Vector3 pos, float scale = 3.0f)
    {
        Vector3 lastBigSplashPos = pos;
        lastBigSplashPos.y = height;

        GameObject splash = ObjectPoolManager.instance.GetObject(bigSplash.name);
        splash.transform.position = lastBigSplashPos;
        splash.transform.localScale = Vector3.one * scale;

    }

    


    private void SetDicisionFish(bool isSmallFish, int fishIndex)
    {
        _fishList.ForEach(obj =>
        {
            obj.IsDecisionFish = false;
        });



        List<Fish> tempList = new List<Fish>();

        tempList.AddRange(_fishList.FindAll(fish => (((isSmallFish && fish.FishLength < 0.25f) || (isSmallFish == false && fish.FishLength > 0.25f)) && (int)fish.CurFishType == fishIndex)));

        if (tempList.Count == 0)
        {
            Fish findFish = null;
            tempList.AddRange(_fishList.FindAll(obj => (int)obj.CurFishType == fishIndex));
            if (tempList.Count == 0)
            {
                findFish = fishIndex == (int)FishType.GoldenFish ? GenNewGoldenFishObject(isSmallFish) : GenNewFishObject(fishIndex, isSmallFish);
                findFish.IsDecisionFish = true;
                return;
            }
            findFish = tempList[UnityEngine.Random.Range(0, tempList.Count)];
            float scale = 0.25f;
            if (isSmallFish)
                scale = UnityEngine.Random.Range(0.21f, 0.23f);
            else
                scale = UnityEngine.Random.Range(0.26f, 0.28f);
            findFish.SetNewScale(scale);
            findFish.IsDecisionFish = true;
        }
        else
        {
            Fish decisionFish = tempList[UnityEngine.Random.Range(0, tempList.Count)];
            if (decisionFish)
                decisionFish.IsDecisionFish = true;
        }
        tempList.Clear();
    }

    public void OpenGameStart(GameObject btn)
    {
        btn.SetActive(false);

        ChangeRaceState(RaceState.Ready);

    }


    public void InitRankResult(RaceBettingResultResponse ranksResult, int roomId, Action actionCallBack)
    {

        SetFixedRankResult(ranksResult);

        _roomID = roomId;

        _actionCallBack = actionCallBack;
        ChangeRaceState(RaceState.Ready);
    }


    public void SetFixedRankResult(RaceBettingResultResponse ranksResult)
    {
        bool isSamllFish = ranksResult.ranks[0] == 1;
        int fishIndex = 0; 

        bool isJackpot = GetIndexFromHashCode(ranksResult.hash, 2) == 8;
        if (isJackpot)
            fishIndex = (int)FishType.GoldenFish;
        else
            fishIndex = GetIndexFromHashCodeMod(ranksResult.hash);


        Debug.Log($"Dicision Fish : {(isSamllFish ? "Small" : "Big")}, FishIndex : {fishIndex}");
        SetDicisionFish(isSamllFish, fishIndex);
    }


    static public Vector3 GetDegreeDistVector(Vector3 originPos, float degreeVal, float distVal)
    {
        float radian = degreeVal * Mathf.Deg2Rad;
        float x = Mathf.Cos(radian);
        float y = Mathf.Sin(radian);
        return originPos + (new Vector3(x, 0, y) * distVal);
    }

    static public int GetIndexFromHashCodeMod(string hashCode, int index = 12, int modNum = 4)
    {
        return GetIndexFromHashCode(hashCode, index) % modNum;
    }

    static public int GetIndexFromHashCode(string hashCode, int index)
    {
        char[] values = hashCode.ToCharArray();
        int decideNum = Convert.ToInt32(Convert.ToString(values[index]), 16);


        return decideNum;
    }

}
