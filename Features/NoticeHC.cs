using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class NoticeHC : MonoBehaviour
{
    public GameObject jackpotNoticeLayout;
    public GameObject noticeLayout;
    public GameObject jackpotNoticeTxt;

    private Queue<string> _userNameQueue = new Queue<string>();
    private bool _isShow = false;

    private void Start()
    {
        //add socket user received jackpot
        Current.Ins.socketAPI.AddSocket<UserResponse>(
           socketName: StringConst.SOCKET_USER_RECEIVED_JACKPOT_EVENT,
           socketUrl: StringConst.SOCKET_USER_RECEIVED_JACKPOT_EVENT,
           callBack: (res) =>
           {
               UnityThread.executeInUpdate(() =>
               {
                   _userNameQueue.Enqueue(res.name);

                   if (_isShow) return;

                   _isShow = true;
                   ShowMsg();
               });
           }
       );
    }

    private void ShowMsg()
    {
        jackpotNoticeLayout.SetActive(true);
        jackpotNoticeTxt.GetComponent<TextMeshProUGUI>().text = $"Congratulations to {_userNameQueue.Dequeue()} for winning a Jackpot with a very high value. Well done!";
        noticeLayout.transform.position = new Vector3(11f, noticeLayout.transform.position.y, noticeLayout.transform.position.z);
        noticeLayout.transform.DOMoveX(-11f, 11f).SetLoops(3, LoopType.Restart).OnComplete(() =>
        {
            if (_userNameQueue.Count > 0)
            {
                ShowMsg();
                return;
            }

            _isShow = false;
            jackpotNoticeLayout.SetActive(false);
        });
    }

    private void OnDisable()
    {
        Current.Ins.socketAPI.RemoveSocket(StringConst.SOCKET_USER_RECEIVED_JACKPOT_EVENT);
    }
}
