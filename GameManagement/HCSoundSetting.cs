using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class HCSound
{
    public WelcomeSound Welcome;
    public HomeSound Home;
    public BingoSound Bingo;
    public BubbleSound Bubble;
    public MiniGameSound MiniGame;
    public BonusSystemSound BonusGame;
}