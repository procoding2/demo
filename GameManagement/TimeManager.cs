﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    public static TimeManager Ins;

    [HideInInspector]
    public int time;

    [HideInInspector]
    public int maxTime = 180;

    [HideInInspector]
    public int oldDay;

    [HideInInspector]
    public int newDay;


    private void Awake()
    {
        DontDestroyOnLoad(this);
        if (Ins == null) Ins = this;
    }

    private void OnEnable()
    {
        //ReLoad();
    }

    //public void ReLoad()
    //{
    //    if (!PlayerPrefs.HasKey("time_play"))
    //    {
    //        PlayerPrefs.SetInt("time_play", 0);
    //    }
    //    time = PlayerPrefs.GetInt("time_play");

    //    if (!PlayerPrefs.HasKey("old_day"))
    //    {
    //        oldDay = GetNowDay();
    //        PlayerPrefs.SetInt("old_day", oldDay);
    //    }
    //    oldDay = PlayerPrefs.GetInt("old_day");
    //    newDay = GetNowDay();

    //    if (newDay > oldDay)
    //    {
    //        PlayerPrefs.SetInt("old_day", newDay);
    //        oldDay = newDay;
    //        time = 0;
    //        PlayerPrefs.SetInt("time_play", 0);
    //    }
    //    else
    //    {
    //        if (time >= maxTime)
    //        {
    //            CallPopupTimeOut();
    //            return;
    //        }
    //    }
    //    StartCoroutine(Ie_UpdateTime());
    //}

    public int GetNowDay()
    {
        var day = CurrentTimeInSecond;
        return day / 86400;
    }

    public static int CurrentTimeInSecond
    {
        get
        {
            return (int)(DateTime.Now - new DateTime(1970, 1, 1)).TotalSeconds;
        }
    }

    public static double CurrentTimeInMilliSecond
    {
        get
        {
            var distance = (DateTime.Now - new DateTime(1970, 1, 1)).TotalMilliseconds;
            return Math.Round(distance);
        }
    }

    // Update is called once per frame
    //IEnumerator Ie_UpdateTime()
    //{
    //    while (true)
    //    {
    //        if (time < maxTime)
    //        {
    //            time++;
    //            yield return Yielders.Get(1f);
    //        }
    //        else
    //        {
    //            CallPopupTimeOut();
    //            yield break;
    //        }
    //    }
    //}

    //public void CallPopupTimeOut()
    //{
    //    UIManager.Ins.TimeOut.SetActive(true);
    //}

    //public void OnApplicationPause(bool pause)
    //{
    //    if (pause)
    //    {
    //        Save();
    //    }
    //    else
    //    {
    //        ReLoad();
    //    }
    //}

    public void OnApplicationQuit()
    {
        Save();
    }

    public void Save()
    {
        StopAllCoroutines();
        PlayerPrefs.SetInt("time_play", time);
        PlayerPrefs.SetInt("old_day", newDay);
    }
}
