using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class HCSound : MonoBehaviour
{
    public static HCSound Ins;

    [HideInInspector]
    public AudioSource FxSoundGame;

    [HideInInspector]
    public AudioSource FxSoundGame2;

    [HideInInspector]
    public AudioSource FxSoundGame3;

    [HideInInspector]
    public AudioSource FxSoundBackground;

    [Header("Game Sound")]
    public AudioClip ButtonClick;

    [Header("Background Sound")]
    public List<AudioClip> BackgroundSounds;

    private SoundConfigData _soundConfig;

    private void Awake()
    {
        DontDestroyOnLoad(this);
        if (Ins == null) Ins = this;

        LoadSoundConfig(Current.Ins.Config.SoundConfig);
        Current.Ins.Config.PropertyChanged += (obj, e) =>
        {
            if (e.PropertyName == StringConst.PROPERTY_SOUND)
            {
                LoadSoundConfig(((Config)obj).SoundConfig);
            }
        };
    }

    private void LoadSoundConfig(SoundConfigData config)
    {
        _soundConfig = config;

        if (FxSoundGame == null)
        {
            FxSoundGame = gameObject.AddComponent<AudioSource>();
        }

        if (FxSoundGame2 == null)
        {
            FxSoundGame2 = gameObject.AddComponent<AudioSource>();
        }

        if (FxSoundGame3 == null)
        {
            FxSoundGame3 = gameObject.AddComponent<AudioSource>();
        }

        if (FxSoundBackground == null)
        {
            FxSoundBackground = gameObject.AddComponent<AudioSource>();
            FxSoundBackground.loop = true;
        }

        FxSoundGame.volume = config.GameVolume;
        FxSoundGame2.volume = config.GameVolume;
        FxSoundGame3.volume = config.GameVolume;
        FxSoundBackground.volume = config.BackgroundVolume;
    }

    public void PlayGameSound(AudioClip audioClip, bool isOverlap = false)
    {
        if (FxSoundGame.isPlaying && !isOverlap) return;
        FxSoundGame.PlayOneShot(audioClip);
    }

    public void StopGameSound()
    {
        FxSoundGame.Stop();
        FxSoundGame.clip = null;
    }

    public void PlayGameSound2(AudioClip audioClip, float? time = null)
    {
        if (time != null && time.Value <= audioClip.length) FxSoundGame2.time = time.Value;

        FxSoundGame2.PlayOneShot(audioClip);
    }

    public void StopGameSound2()
    {
        FxSoundGame2.Stop();
        FxSoundGame2.clip = null;
    }

    public void PlayGameSound3(AudioClip audioClip) => FxSoundGame3.PlayOneShot(audioClip);
    public void StopGameSound3()
    {
        FxSoundGame3.Stop();
        FxSoundGame3.clip = null;
    }

    public void PlayBackground(AudioClip clip)
    {
        CancelInvoke(nameof(PlayNextTrack));
        CancelInvoke(nameof(ReplayBackground));

        FxSoundBackground.clip = clip;
        FxSoundBackground.Play();
    }

    private List<AudioClip> _clips = new List<AudioClip>();
    private int _iClip = 0;
    public void PlayBackgrounds(List<AudioClip> clips = null)
    {
        if (clips != null) _clips = clips;

        _iClip = 0;
        PlayBackground(_clips[0]);

        if (clips.Count <= 1) return;

        float timeDelay = clips[0].length;
        for (int i = 1; i <= clips.Count - 1; i++)
        {
            Invoke(nameof(PlayNextTrack), timeDelay);
            timeDelay += clips[i].length;
        }
    }

    private void ReplayBackground()
    {
        PlayBackgrounds();
    }

    private void PlayNextTrack()
    {
        _iClip++;

        FxSoundBackground.clip = _clips[_iClip];
        FxSoundBackground.Play();

        if (_iClip == _clips.Count - 1)
        {
            Invoke(nameof(ReplayBackground), _clips[_iClip].length);
        }
    }

    public void PauseBackground() => FxSoundBackground.Pause();
    public void UnPauseBackground() => FxSoundBackground.UnPause();
    public void StopBackground() => FxSoundBackground.Stop();
}
