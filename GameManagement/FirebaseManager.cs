using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Messaging;
using System;
using OutGameEnum;
using System.ComponentModel;
using System.Threading.Tasks;

public class FirebaseManager : INotifyPropertyChanged
{
    [Serializable]
    private class FirebaseMessageReceived
    {
        public int id;
        public string title;
        public string content;
        public bool isRead;
        public NotificationTypeEnum type;
        public NotificationResponse.MissionItem mission;
        public NotificationResponse.EventItem eventDetail;
        public double createdAt;
    }

    #region INotifyPropertyChanged
    public event PropertyChangedEventHandler PropertyChanged;

    protected void OnPropertyChanged(PropertyChangedEventArgs e)
    {
        PropertyChangedEventHandler handler = PropertyChanged;
        if (handler != null) handler(this, e);
    }

    protected void OnPropertyChanged(string propertyName) => OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
    #endregion

    private static FirebaseManager _instance;
    public static FirebaseManager Instance()
    {
        if (_instance == null) _instance = new FirebaseManager();
        return _instance;
    }

    public string token { get; private set; } = "token";

    private FirebaseApp app;
    private FirebaseManager()
    {
        _listIdPushed = new List<string>();
    }

    public Task Init()
    {
        if (Application.isEditor)
        {
            return Task.CompletedTask;
        }

        return FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            var dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                // Create and hold a reference to your FirebaseApp,
                // where app is a Firebase.FirebaseApp property of your application class.
                app = FirebaseApp.DefaultInstance;

                // Set a flag here to indicate whether Firebase is ready to use by your app.
                Firebase.Messaging.FirebaseMessaging.TokenReceived += OnTokenReceived;
                Firebase.Messaging.FirebaseMessaging.MessageReceived += OnMessageReceived;
            }
            else
            {
                UnityEngine.Debug.LogError(System.String.Format(
                  "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                // Firebase Unity SDK is not safe to use here.
            }
        });
    }

    private List<string> _listIdPushed = new List<string>();
    private void OnMessageReceived(object sender, MessageReceivedEventArgs e)
    {
        Debug.Log("OnMessageReceived:");
        if (!e.Message.Data.ContainsKey(StringConst.FIREBASE_KEY_DATA)) return;

        var data = JsonUtility.FromJson<FirebaseMessageReceived>(e.Message.Data[StringConst.FIREBASE_KEY_DATA]);
        if (data == null) return;

        if (_listIdPushed.Contains(e.Message.MessageId)) return;
        _listIdPushed.Add(e.Message.MessageId);

        var numberNoti = PlayerPrefsEx.Get(PlayerPrefsConst.NUMBER_NOTIFICATION, 0);
        try
        {
            switch (data.type)
            {
                case NotificationTypeEnum.LevelUp:
                    Debug.Log("LevelUp");
                    HCPushNotification.Instance().Push(e.Message.MessageId, data.title, data.content);
                    PlayerPrefsEx.Set(PlayerPrefsConst.KEY_PREF_LEVEL_UP, e.Message.Data[StringConst.FIREBASE_KEY_DATA]);
                    OnPropertyChanged(StringConst.KEY_DATA_LEVEL_UP);
                    break;
                case NotificationTypeEnum.NewDailyMission:
                    Debug.Log("NewDailyMission");
                    PlayerPrefsEx.Set(PlayerPrefsConst.NUMBER_NOTIFICATION, numberNoti + 1);
                    OnPropertyChanged(StringConst.KEY_HAVE_NOTI);
                    HCPushNotification.Instance().Push(e.Message.MessageId, data.mission.name, data.mission.description);
                    break;
                case NotificationTypeEnum.NewEvent:
                    Debug.Log("NewEvent");
                    PlayerPrefsEx.Set(PlayerPrefsConst.NUMBER_NOTIFICATION, numberNoti + 1);
                    OnPropertyChanged(StringConst.KEY_HAVE_NOTI);
                    HCPushNotification.Instance().Push(e.Message.MessageId, data.eventDetail.name, data.eventDetail.content);
                    break;
                case NotificationTypeEnum.Normal:
                    Debug.Log("Normal");
                    PlayerPrefsEx.Set(PlayerPrefsConst.NUMBER_NOTIFICATION, numberNoti + 1);
                    OnPropertyChanged(StringConst.KEY_HAVE_NOTI);
                    HCPushNotification.Instance().Push(e.Message.MessageId, data.title, data.content);
                    break;
            }

            //OnPropertyChanged(new PropertyChangedEventArgs(StringConst.PROPERTY_FCM));
        }
        catch (Exception ex)
        {
            Debug.Log($"ErrorFirebase: {ex.ToString()}");
        }
    }

    private void OnTokenReceived(object sender, TokenReceivedEventArgs token)
    {
        Debug.Log("NewToken: " + token.Token);
        this.token = token.Token;
    }

    public Task DeleteToken()
    {
        Debug.Log("deletetoken");
        return Firebase.Messaging.FirebaseMessaging.DeleteTokenAsync();
    }

    public Task<string> GetNewToken()
    {
        Debug.Log("GetNewToken");
        return Firebase.Messaging.FirebaseMessaging.GetTokenAsync();
    }

    public void SendFirebaseLog(string content)
    {
        try
        {
            Firebase.Analytics.FirebaseAnalytics.LogEvent
            (
                Firebase.Analytics.FirebaseAnalytics.EventLevelStart,
                new Firebase.Analytics.Parameter("LogContent", content)
            );
        }
        catch { }
    }
}
