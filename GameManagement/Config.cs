using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Localization.Settings;

using OutGameEnum;
using System.ComponentModel;

public class Config : INotifyPropertyChanged
{
    public event PropertyChangedEventHandler PropertyChanged;

    protected void OnPropertyChanged(PropertyChangedEventArgs e)
    {
        PropertyChangedEventHandler handler = PropertyChanged;
        if (handler != null)
            handler(this, e);
    }

    protected void OnPropertyChanged(string propertyName)
        => OnPropertyChanged(new PropertyChangedEventArgs(propertyName));

    private SoundConfigData _soundConfig;
    public SoundConfigData SoundConfig
    {
        get
        {
            if (_soundConfig == null)
            {
                _soundConfig = new SoundConfigData();
                _soundConfig.BackgroundVolume = PlayerPrefsEx.Get(PlayerPrefsConst.SOUND_BACKGROUND_VOLUME, 1f);
                _soundConfig.GameVolume = PlayerPrefsEx.Get(PlayerPrefsConst.SOUND_GAME_VOLUME, 1f);
            }

            return _soundConfig;
        }
        set
        {
            if (!_soundConfig.Equals(value))
            {
                _soundConfig = value;
                PlayerPrefsEx.Set(PlayerPrefsConst.SOUND_BACKGROUND_VOLUME, _soundConfig.BackgroundVolume);
                PlayerPrefsEx.Set(PlayerPrefsConst.SOUND_GAME_VOLUME, _soundConfig.GameVolume);

                OnPropertyChanged(StringConst.PROPERTY_SOUND);
            }
        }
    }

    #region Language

    public LanguageEnum GetCurrentLanguage()
    {
        var defLanguage = 0;
        switch (Application.systemLanguage)
        {
            //case SystemLanguage.Korean:
            //    defLanguage = 1; break;
            case SystemLanguage.Vietnamese:
                defLanguage = 2; break;
        }

        return (LanguageEnum)PlayerPrefsEx.Get(PlayerPrefsConst.LANGUAGE, defLanguage);
    }

    /// <summary>
    /// Init language;
    /// </summary>
    /// <returns></returns>
    public IEnumerator SetUpLanguage()
    {
        yield return LocalizationSettings.InitializationOperation;

        var defLanguage = 0;
        switch (Application.systemLanguage)
        {
            //case SystemLanguage.Korean:
            //    defLanguage = 1; break;
            case SystemLanguage.Vietnamese:
                defLanguage = 2; break;
        }

        var currLang = PlayerPrefsEx.Get(PlayerPrefsConst.LANGUAGE, defLanguage);
        LocalizationSettings.SelectedLocale = LocalizationSettings.AvailableLocales.Locales[currLang];
    }

    /// <summary>
    /// Change Language.
    /// Example:
    /// StartCoroutine(Current.Ins.Config.ChangeLanguage(LanguageEnum.kor));
    /// </summary>
    /// <param name="l"></param>
    /// <returns></returns>
    public IEnumerator ChangeLanguage(LanguageEnum l)
    {
        yield return LocalizationSettings.InitializationOperation;

        var lang = (int)l;
        LocalizationSettings.SelectedLocale = LocalizationSettings.AvailableLocales.Locales[lang];
        PlayerPrefsEx.Set(PlayerPrefsConst.LANGUAGE, lang);
        OnPropertyChanged(StringConst.PROPERTY_LANGUAGE);
    }

    #endregion
}
