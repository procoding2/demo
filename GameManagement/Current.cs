using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using OutGameEnum;
using System;
using System.Linq;
using System.ComponentModel;
using System.IO;
using System.Globalization;
using System.Threading;
using CodeStage.AntiCheat.ObscuredTypes;
using CodeStage.AntiCheat.Genuine.CodeHash;
using System.Threading.Tasks;
using Firebase.Extensions;

/// <summary>
/// Entry Point : Current 클래스를 진입포인트로 삼아서, 각 객체 생성하며 게임내 필요한 초기화 진행 - Brandon
/// </summary>
public class Current : MonoBehaviour, INotifyPropertyChanged
{
    #region INotifyPropertyChanged
    public event PropertyChangedEventHandler PropertyChanged;

    protected void OnPropertyChanged(PropertyChangedEventArgs e)
    {
        PropertyChangedEventHandler handler = PropertyChanged;
        if (handler != null) handler(this, e);
    }

    protected void OnPropertyChanged(string propertyName) => OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
    #endregion

    public static Current Ins;

    private void Awake()
    {
        Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("en-US");

        DontDestroyOnLoad(this);
        if (Ins == null)
        {
            Ins = this;


            //if (RootCheckHelper.isEmulator()) Application.Quit();


            TemplateManager.instance.LoadTemplates();

            CacheInit();
            //AnticheatInit();
            StartCoroutine(SendNotiChangeCryptoKey());
            StartCoroutine(AppleUpdate());
            InitFirebaseToken();
        }
    }

    public UserAPI userAPI = new UserAPI();     // HTTP Call 을 통한 통신 관련 객체 초기화
    public GameAPI gameAPI = new GameAPI();     // HTTP Call 을 통한 게임관련 통신 처리 객체 초기화
    public BonusAPI bonusAPI = new BonusAPI();
    public SocketAPI socketAPI = new SocketAPI();   // 서버에서 수동 이벤트로 데이터를 받기 위해서 소켓 통신을 관리해주는 객체 - Serverless에서 불필요한 아키텍쳐로 판단됨
    public EventAPI eventAPI = new EventAPI();
    public HistoryAPI historyAPI = new HistoryAPI();
    public DownloadAPI downloadAPI = new DownloadAPI();
    public RaceAPI raceApi = new RaceAPI();
    public FishAPI fishApi = new FishAPI();

    private Config _config;
    public Config Config
    {
        get
        {
            if (_config == null) _config = new Config();
            return _config;
        }
    }

    private ImageData _imageData;
    public ImageData ImageData
    {
        get
        {
            if (_imageData == null) _imageData = new ImageData();
            return _imageData;
        }
    }

    public void UserAvatar(Action<ImageData.ImageItemData> callBack) => _imageData.AvatarByUrl(player.Avatar, callBack);
    public void UserAvatarFrame(Action<ImageData.ImageItemData> callBack) => _imageData.AvatarFrameByUrl(player.AvatarFrame, callBack);
    public void UserCover(Action<ImageData.ImageItemData> callBack) => _imageData.CoverByUrl(player.Cover, callBack);
    //public ImageData.ImageItemData UserCover { get { return _imageData.Covers.FirstOrDefault(x => x.Path == player.Cover) ?? _imageData.Covers.FirstOrDefault(); } }

    public PlayerInfo player { get; private set; } = new PlayerInfo();

    public void setPlayerInfo(LoginResponse res, string userName)
    {
        var user = res.data.user;
        player = new PlayerInfo();
        player.Id = user.id.ToString();
        player.Pid = user.pid;
        player.UserName = userName;
        player.Name = user.name;
        player.Avatar = user.avatar;
        player.Cover = user.cover;
        player.AvatarFrame = user.avatarFrame;
        player.GoldCoin = user.gold;
        player.HCTokenCoin = user.token;
        player.TicketCoin = user.ticket;
        player.IsDelete = user.isDelete;
        player.IsEditName = user.isEditName;

        player.DailyRewardNotClaim = user.dailyRewardNotClaim;

        player.AccessToken = res.data.accessToken;
        player.SocialType = (SocialTypeEnum)res.data.user.socialType; // response: 0 (int)
        player.AssetVersion = user.assetVersion;

        SetPlayerDataLocal();
    }

    public void SetPlayerDataLocal()
    {
        string jsonData = JsonUtility.ToJson(player);
        PlayerPrefsEx.Set(PlayerPrefsConst.PLAYER_INFO, jsonData);
    }

    public PlayerInfo GetPlayerDataLocal()
    {
        string jsonData = PlayerPrefsEx.Get(PlayerPrefsConst.PLAYER_INFO, "");
        Debug.Log(jsonData);
        var playerData = JsonUtility.FromJson<PlayerInfo>(jsonData);
        return playerData;
    }

    public void ClearPlayerDataLocal()
    {
        PlayerPrefsEx.DeleteKey(PlayerPrefsConst.PLAYER_INFO);
    }

    [HideInInspector]
    public bool IsFirstLoginOfDay { get; set; }
    public void SetTimeUserLogin(double timestamp, string userId)
    {
        var userIdPref = PlayerPrefsEx.Get(PlayerPrefsConst.PLAYER_ID, string.Empty);
        var loginTimeYMDPref = PlayerPrefsEx.Get(PlayerPrefsConst.PLAYER_LOGIN_TIME_YMD, string.Empty);

        var dt1970 = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
        var loginTimeYMDBE = (dt1970 + (TimeSpan.FromSeconds(timestamp))).ToString(@"yyyyMMdd");

        if (userIdPref != userId || loginTimeYMDPref != loginTimeYMDBE)
        {
            IsFirstLoginOfDay = true;
        }

        PlayerPrefsEx.Set(PlayerPrefsConst.PLAYER_ID, userId);
        PlayerPrefsEx.Set(PlayerPrefsConst.PLAYER_LOGIN_TIME_YMD, loginTimeYMDBE);
    }

    public void LoadAllImage(Action<bool> callBack)
    {
        var dispose = Observable.CombineLatest(
            Observable.FromCoroutine<bool>((isDoneRx) => GetImageUser((isDone) => isDoneRx.OnNext(isDone))),
            Observable.FromCoroutine<bool>((isDoneRx) => GetEventBanner((isDone) => isDoneRx.OnNext(isDone))),
            (imgUser, imgEvent) => { return (imgUser, imgEvent); })
            .Subscribe((result) =>
            {
                if (!result.imgUser) callBack.Invoke(false);
                else callBack.Invoke(true);
            });
    }

    private IEnumerator GetImageUser(Action<bool> callBack)
    {
        yield return new WaitForSeconds(0.01f);
        int currentAssetVersion = PlayerPrefsEx.Get(PlayerPrefsConst.ASSET_VERSION, 0);
        int userAssetVersion = player.AssetVersion;

        Action<ImageDataUrl, Action> downloadImgAction = (imageDataUrl, onComplete) =>
        {
            var paths = imageDataUrl.AllPaths;

            downloadAPI.DownloadAllImages(paths, (isComplete, rs) =>
            {
                if (isComplete)
                {
                    if (_imageData == null) _imageData = new ImageData();

                    _imageData.Avatars = new List<ImageData.ImageItemData>();
                    _imageData.AvatarFrames = new List<ImageData.ImageItemData>();
                    _imageData.Covers = new List<ImageData.ImageItemData>();

                    foreach (var item in rs)
                    {
                        try
                        {
                            if (imageDataUrl.Avatars.Contains(item.Item1))
                            {
                                _imageData.Avatars.Add(new ImageData.ImageItemData() { Path = item.Item1, Texture = item.Item2 });
                            }

                            if (imageDataUrl.AvatarFrames.Contains(item.Item1))
                            {
                                _imageData.AvatarFrames.Add(new ImageData.ImageItemData() { Path = item.Item1, Texture = item.Item2 });
                            }

                            if (imageDataUrl.Covers.Contains(item.Item1))
                            {
                                _imageData.Covers.Add(new ImageData.ImageItemData() { Path = item.Item1, Texture = item.Item2 });
                            }
                        }
                        catch
                        {
                            Debug.Log($"Error: {item.Item1}");
                        }
                    }

                    PlayerPrefsEx.Set(PlayerPrefsConst.ASSET_VERSION, userAssetVersion);
                }
                else
                {
                    PlayerPrefsEx.Set(PlayerPrefsConst.ASSET_VERSION, 0);
                }

                onComplete.Invoke();
            },
            cacheFolder: CACHE_USER_DIR);
        };

        Action<Action> loadUrlFromAPIAndDownload = (onComplete) =>
        {
            // Clear old data
            PlayerPrefsEx.DeleteKey(PlayerPrefsConst.IMAGE_DATA);
            var dirCache = $"{CachedFolder}/{CACHE_USER_DIR}";
            if (Directory.Exists(dirCache)) Directory.Delete(dirCache, true);

            var avtsRx = Observable.FromCoroutine<(IconTypeEnum, IconResponse)>((rx) => Current.Ins.userAPI.GetIcon(IconTypeEnum.Avatar, (res) =>
            {
                rx.OnNext((IconTypeEnum.Avatar, res));
                rx.OnCompleted();
            }));

            var framesRx = Observable.FromCoroutine<(IconTypeEnum, IconResponse)>((rx) => Current.Ins.userAPI.GetIcon(IconTypeEnum.AvtFrame, (res) =>
            {
                rx.OnNext((IconTypeEnum.AvtFrame, res));
                rx.OnCompleted();
            }));

            var coversRx = Observable.FromCoroutine<(IconTypeEnum, IconResponse)>((rx) => Current.Ins.userAPI.GetIcon(IconTypeEnum.Cover, (res) =>
            {
                rx.OnNext((IconTypeEnum.Cover, res));
                rx.OnCompleted();
            }));

            var dp = Observable
                .WhenAll(avtsRx, framesRx, coversRx)
                .Retry(2)
                .DoOnError((error) =>
                {
                    Debug.Log(error.Message);
                    callBack.Invoke(false);
                })
                .Subscribe(res =>
                {
                    var avtImgData = res.Where(x => x.Item1 == IconTypeEnum.Avatar).First();
                    var frameImgData = res.Where(x => x.Item1 == IconTypeEnum.AvtFrame).First();
                    var coverImgData = res.Where(x => x.Item1 == IconTypeEnum.Cover).First();

                    ImageDataUrl imgDataUrl = new ImageDataUrl();

                    if (avtImgData.Item2 != null && avtImgData.Item2.errorCode == 0)
                    {
                        imgDataUrl.Avatars.AddRange(avtImgData.Item2.data.datas.Select(x => x.url));
                    }

                    if (frameImgData.Item2 != null && frameImgData.Item2.errorCode == 0)
                    {
                        imgDataUrl.AvatarFrames.AddRange(frameImgData.Item2.data.datas.Select(x => x.url));
                    }

                    if (coverImgData.Item2 != null && coverImgData.Item2.errorCode == 0)
                    {
                        imgDataUrl.Covers.AddRange(coverImgData.Item2.data.datas.Select(x => x.url));
                    }

                    var jsonData = JsonUtility.ToJson(imgDataUrl);
                    PlayerPrefsEx.Set(PlayerPrefsConst.IMAGE_DATA, jsonData);

                    downloadImgAction.Invoke(imgDataUrl, () => callBack.Invoke(true));
                });
        };

        ImageDataUrl imgDataUrl = null;
        if (currentAssetVersion == userAssetVersion)
        {
            if (_imageData == null)
            {
                string jsonData = PlayerPrefsEx.Get(PlayerPrefsConst.IMAGE_DATA, "");
                Debug.Log(jsonData);
                imgDataUrl = JsonUtility.FromJson<ImageDataUrl>(jsonData);
            }

            if (imgDataUrl == null
                || imgDataUrl.Avatars == null || imgDataUrl.Avatars.Count == 0
                || imgDataUrl.Covers == null || imgDataUrl.Covers.Count == 0)
            {
                loadUrlFromAPIAndDownload.Invoke(() => callBack.Invoke(true));
            }
            else
            {
                downloadImgAction.Invoke(imgDataUrl, () => callBack.Invoke(true));
            }
        }
        else
        {
            loadUrlFromAPIAndDownload.Invoke(() => callBack.Invoke(true));
        }
    }

    private IEnumerator GetEventBanner(Action<bool> onComplete)
    {
        return Current.Ins.eventAPI.GetEventBanner<EventBannerResponse>(locationEvent: 2, (res) =>
        {
            if (res == null || res.errorCode != 0)
            {
                onComplete.Invoke(false);
                return;
            }

            var roulette = res.data.FirstOrDefault(x => x.game == BonusGameEnum.Roulette.GetStringValue());
            var scratch = res.data.FirstOrDefault(x => x.game == BonusGameEnum.Scratch.GetStringValue());
            var openBox = res.data.FirstOrDefault(x => x.game == BonusGameEnum.OpenBox.GetStringValue());

            var paths = new List<string>();
            if (roulette != null) paths.Add(roulette.thumbnail);
            if (scratch != null) paths.Add(scratch.thumbnail);
            if (openBox != null) paths.Add(openBox.thumbnail);

            Current.Ins.downloadAPI.DownloadAllImages(
                paths: paths,
                callBack: (isCompele, textTures) =>
                {
                    if (isCompele)
                    {
                        ImageDataEventUrl imgDataEventUrl = new ImageDataEventUrl();

                        if (roulette != null) imgDataEventUrl.Roulette = textTures.FirstOrDefault(x => x.Item1 == roulette.thumbnail).Item1;
                        if (scratch != null) imgDataEventUrl.Scrach = textTures.FirstOrDefault(x => x.Item1 == scratch.thumbnail).Item1;
                        if (openBox != null) imgDataEventUrl.OpenBox = textTures.FirstOrDefault(x => x.Item1 == openBox.thumbnail).Item1;

                        var jsonData = JsonUtility.ToJson(imgDataEventUrl);
                        PlayerPrefsEx.Set(PlayerPrefsConst.IMAGE_BANNER_DATA, jsonData);

                        onComplete.Invoke(true);
                    }
                    else onComplete.Invoke(false);

                }, cacheFolder: Current.Ins.CACHE_BANNER_DIR);
        });
    }

    #region Rx
    public Subject<PlayerCoinDataRx> playerCoinRx = new Subject<PlayerCoinDataRx>();
    public void UpdatePlayerCoin(
        int gold,
        int token,
        int ticket,
        Transform transform = null,
        Transform desTransform = null,
        Action callBackAfterDoneEffect = null)
    {
        player.GoldCoin += gold;
        player.HCTokenCoin += token;
        player.TicketCoin += ticket;

        List<(CoinEnum, int)> coinTypes = new List<(CoinEnum, int)>();

        if (token != 0)
        {
            coinTypes.Add((CoinEnum.Token, token));
        }

        if (gold != 0)
        {
            coinTypes.Add((CoinEnum.Gold, gold));
        }

        if (ticket != 0)
        {
            coinTypes.Add((CoinEnum.Ticket, ticket));
        }

        var dataRx = new PlayerCoinDataRx();
        dataRx.Gold = player.GoldCoin;
        dataRx.Token = player.HCTokenCoin;
        dataRx.Ticket = player.TicketCoin;
        dataRx.CoinTypes = coinTypes;
        dataRx.transform = transform;
        dataRx.desTransform = desTransform;
        dataRx.CallBackAfterEffect = callBackAfterDoneEffect;

        playerCoinRx.OnNext(dataRx);
    }

    public void ReloadUserProfile(
        Transform transform = null,
        Transform desTransform = null,
        Action callBackAfterDoneEffect = null,
        Action callBackAfterDoneAPI = null)
    {
        StartCoroutine(userAPI.GetUserProfile((res) =>
        {
            if (res == null || res.errorCode != 0)
            {
                Debug.Log("Error: ReloadUserProfile");
                return;
            }

            List<(CoinEnum, int)> coinTypes = new List<(CoinEnum, int)>();

            int t = res.data.token - (int)player.HCTokenCoin;
            if (t > 0)
            {
                coinTypes.Add((CoinEnum.Token, t));
            }

            int g = res.data.gold - (int)player.GoldCoin;
            if (g > 0)
            {
                coinTypes.Add((CoinEnum.Gold, g));
            }

            int tk = res.data.ticket - (int)player.TicketCoin;
            if (tk > 0)
            {
                coinTypes.Add((CoinEnum.Ticket, tk));
            }

            player.IsDelete = res.data.isDelete;
            player.IsEditName = res.data.isEditName;
            player.SocialType = res.data.socialType;

            player.Avatar = res.data.avatar;
            player.Cover = res.data.cover;
            player.AvatarFrame = res.data.avatarFrame;
            player.Name = res.data.name;

            // Update coin
            player.GoldCoin = res.data.gold;
            player.HCTokenCoin = res.data.token;
            player.TicketCoin = res.data.ticket;

            // Update level, xp player
            player.Level = res.data.level;
            player.CurrentXP = res.data.currentXP;
            player.NextLevelXP = res.data.nextLevelXP;

            // Update referral code, referenceCode
            player.ReferralCode = res.data.referralCode;
            player.ReferenceCode = res.data.referenceCode;

            // MMR
            player.BingoMMR = res.data.mmrs.FirstOrDefault(x => x.game == "bingo")?.point ?? 0;
            player.BubbleMMR = res.data.mmrs.FirstOrDefault(x => x.game == "bubble")?.point ?? 0;
            player.SolitaireMMR = res.data.mmrs.FirstOrDefault(x => x.game == "solitaire")?.point ?? 0;

            //dailyRewardNotClaim
            player.DailyRewardNotClaim = res.data.dailyRewardNotClaim;

            var dataRx = new PlayerCoinDataRx();
            dataRx.Gold = player.GoldCoin;
            dataRx.Token = player.HCTokenCoin;
            dataRx.Ticket = player.TicketCoin;
            dataRx.CoinTypes = coinTypes;
            dataRx.transform = transform;
            dataRx.desTransform = desTransform;
            dataRx.CallBackAfterEffect = callBackAfterDoneEffect;

            OnPropertyChanged(StringConst.PROPERTY_USER_INFOR);

            callBackAfterDoneAPI?.Invoke();
            if (dataRx.CoinTypes.Count == 0) callBackAfterDoneEffect?.Invoke();

            playerCoinRx.OnNext(dataRx);
        }));
    }

    [HideInInspector]
    public long JackpotValue;
    public Subject<long> JackpotValueRx = new Subject<long>();
    public void LoadJackpot()
    {
        StartCoroutine(bonusAPI.GetJackpot((res) =>
        {
            if (res == null || res.errorCode != 0)
            {
                Debug.Log("Error: GetJackpot");
                return;
            }

            JackpotValue = res.data.token;
            JackpotValueRx.OnNext(JackpotValue);
        }));
    }

    #endregion

    /// <summary>
    /// 네트워크 소켓 통신이 이 정도 값 전달을 위해서 사용된다면 불필요한 아키텍쳐로 보임 - Brandon
    /// </summary>
    public void ListenSocket()
    {
        //add socket daily mission
        Current.Ins.socketAPI.AddSocket<DailyMissionSocketResponse>(
            socketName: StringConst.SOCKET_DAILY_MISSION,
                        $"{StringConst.SOCKET_DAILY_MISSION}{player.Id}",
            callBack: (res) =>
            {
                UnityThread.executeInUpdate(() =>
                {
                    PlayerPrefsEx.Set(PlayerPrefsConst.NUMBER_DAILY_MISSION, res.totalNotClaim);
                    OnPropertyChanged(StringConst.SOCKET_DAILY_MISSION);
                });
            }
        );

        //add socket history
        Current.Ins.socketAPI.AddSocket<HistorySocketResponese>(
            socketName: StringConst.SOCKET_ICON_HISTORY,
                        $"{StringConst.SOCKET_ICON_HISTORY}{player.Id}",
            callBack: (res) =>
            {
                UnityThread.executeInUpdate(() =>
                {
                    PlayerPrefsEx.Set(PlayerPrefsConst.NUMBER_HISTORY_EVENT, res.totalNotWatch);
                    OnPropertyChanged(StringConst.SOCKET_ICON_HISTORY);
                });
            }
        );

        // add socket credit change
        Current.Ins.socketAPI.AddSocket<UserSummarySocketResponse>(
            socketName: StringConst.SOCKET_USER_SUMMARY,
            socketUrl: $"{StringConst.SOCKET_USER_SUMMARY}{player.Id}",
            callBack: (res) => UnityThread.executeInUpdate(() => ReloadUserProfile()));
    }

    #region Cached
    public string CachedFolder;
    public readonly string CACHE_USER_DIR = "user";
    public readonly string CACHE_BANNER_DIR = "banner";
    private Cache _cache;
    private void CacheInit()
    {
        // Cache Init
        CachedFolder = Path.Combine(Application.persistentDataPath, "Cached");
        Debug.Log($"CachedFolder: {CachedFolder}");
        if (!Directory.Exists(CachedFolder)) Directory.CreateDirectory(CachedFolder);

        _cache = Caching.AddCache(CachedFolder);
        // 1Gb
        _cache.maximumAvailableStorageSpace = 1073741824;
    }

    public string GetUrlFromCached(string name, string subFolder)
    {
        var dir = string.IsNullOrWhiteSpace(subFolder) ? $"{_cache.path}" : Path.Combine(_cache.path, subFolder);
        if (!Directory.Exists(dir)) return null;

        DirectoryInfo dirInfo = new DirectoryInfo(dir);
        var file = dirInfo.GetFiles().Where(file => file.Name == name).Select(file => file.FullName).FirstOrDefault();

        if (file == null) return null;
        return Path.Combine(dir, file);
    }

    public void SetCache(string name, string subFolder, byte[] data)
    {
        string dir = string.IsNullOrWhiteSpace(subFolder) ? $"{CachedFolder}" : Path.Combine(_cache.path, subFolder);
        if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);

        string path = string.IsNullOrWhiteSpace(subFolder) ? Path.Combine(CachedFolder, name) : Path.Combine(CachedFolder, subFolder, name);

        try
        {
            if (File.Exists(path)) File.Delete(path);

            using (var fs = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                fs.Write(data, 0, data.Length);
            }
        }
        catch (Exception ex)
        {
            Debug.Log($"SetCacheError: {ex.ToString()}");
        }
    }
    #endregion

    #region AntiCheat

    public ObscuredString CheckSum { get { return "DEF"; } } 

    private IEnumerator SendNotiChangeCryptoKey()
    {
        yield return new WaitForSeconds(5f);

        CheckSum.RandomizeCryptoKey();

        OnPropertyChanged(StringConst.PROPERTY_ANTICHEAT);
        StartCoroutine(SendNotiChangeCryptoKey());
    }

    //private async void AnticheatInit()
    //{
    //    // Start: Validate client
    //    //if (RootCheckHelper.isEmulator() || RootCheckHelper.isRooted()) Application.Quit();

    //    Task generateTask = null;
    //    Current.Ins.CheckSum = StringConst.CHECKSUM_DEF;
    //    //if (Application.installMode == ApplicationInstallMode.DeveloperBuild || Application.isEditor || Application.platform == RuntimePlatform.IPhonePlayer)
    //    //{
    //    //    Current.Ins.CheckSum = StringConst.CHECKSUM_DEF;
    //    //}
    //    //else if (string.IsNullOrEmpty(Current.Ins.CheckSum))
    //    //{
    //    //    async Task HashTask()
    //    //    {
    //    //        var hash = await CodeHashGenerator.GenerateAsync();
    //    //        if (hash == null) Application.Quit();
    //    //        Current.Ins.CheckSum = hash.SummaryHash;
    //    //    }

    //    //    generateTask = HashTask();
    //    //}
    //    // End: Validate client

    //    async Task FbTask()
    //    {
    //        try
    //        {
    //            await FirebaseManager.Instance().Init();
    //            await FirebaseManager.Instance().DeleteToken();
    //            await FirebaseManager.Instance().GetNewToken();
    //        }
    //        catch { }
    //    }
    //    var firebaseTask = FbTask();

    //    if (generateTask != null) await Task.WhenAll(generateTask, firebaseTask);
    //    else await firebaseTask;
    //}

    private async void InitFirebaseToken()
    {
        try
        {
            await FirebaseManager.Instance().Init();
            await FirebaseManager.Instance().DeleteToken();
            await FirebaseManager.Instance().GetNewToken();
        }
        catch { }
    }

    #endregion

    private IEnumerator AppleUpdate()
    {
        yield return new WaitForSeconds(0.2f);
        OnPropertyChanged(StringConst.PROPERTY_APPLE_UPDATE);
        StartCoroutine(AppleUpdate());
    }

    #region HOST URL
    public string HostUrl { get => ConfigData.instance.CurrentStageData.HOST_URL; }
    public string DownloadUrl { get => ConfigData.instance.CurrentStageData.DOWNLOAD_URL; }
    public string SocketUrl { get => ConfigData.instance.CurrentStageData.SOCKET_URL; }
    #endregion

    private void OnApplicationQuit() => Current.Ins.socketAPI.RemoveAllSocket();
}